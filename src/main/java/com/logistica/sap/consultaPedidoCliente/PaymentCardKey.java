
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentCardKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentCardKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://sap.com/xi/AP/Common/GDT}PaymentCardID"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}PaymentCardTypeCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentCardKey", namespace = "http://sap.com/xi/AP/FO/PaymentCard/Global", propOrder = {
    "id",
    "typeCode"
})
public class PaymentCardKey {

    @XmlElement(name = "ID", required = true)
    protected PaymentCardID id;
    @XmlElement(name = "TypeCode", required = true)
    protected PaymentCardTypeCode typeCode;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardID }
     *     
     */
    public PaymentCardID getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardID }
     *     
     */
    public void setID(PaymentCardID value) {
        this.id = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentCardTypeCode }
     *     
     */
    public PaymentCardTypeCode getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentCardTypeCode }
     *     
     */
    public void setTypeCode(PaymentCardTypeCode value) {
        this.typeCode = value;
    }

}
