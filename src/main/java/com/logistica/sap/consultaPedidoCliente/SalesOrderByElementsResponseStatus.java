
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponseStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentAuthorisationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}AuthorisationStatusCode" minOccurs="0"/>
 *         &lt;element name="PaymentAuthorisationStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListCancellationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CancellationStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListCancellationStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListCustomerOrderLifeCycleStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerOrderLifeCycleStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListCustomerOrderLifeCycleStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListFulfilmentProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListFulfilmentProcessingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListPlanningReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListPlanningReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListExecutionReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListExecutionReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ConfirmationIssuingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}IssuingStatusCode" minOccurs="0"/>
 *         &lt;element name="ConfirmationIssuingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListInvoiceProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListInvoiceProcessingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ApprovalStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ApprovalStatusCode" minOccurs="0"/>
 *         &lt;element name="ApprovalStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ItemListProductAvailabilityConfirmationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProductAvailabilityConfirmationStatusCode" minOccurs="0"/>
 *         &lt;element name="ItemListProductAvailabilityConfirmationStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ConsistencyStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ConsistencyStatusCode" minOccurs="0"/>
 *         &lt;element name="ConsistencyStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="GeneralDataCompletenessStatusCode" type="{http://sap.com/xi/AP/Common/GDT}DataCompletenessStatusCode" minOccurs="0"/>
 *         &lt;element name="GeneralDataCompletenessStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="CancellationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CancellationStatusCode" minOccurs="0"/>
 *         &lt;element name="CancellationStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="InvoicingBlockingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}BlockingStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoicingBlockingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="FulfilmentBlockingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}BlockingStatusCode" minOccurs="0"/>
 *         &lt;element name="FulfilmentBlockingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="CustomerRequestReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="CustomerRequestReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="ReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseStatus", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "paymentAuthorisationStatusCode",
    "paymentAuthorisationStatusName",
    "itemListCancellationStatusCode",
    "itemListCancellationStatusName",
    "itemListCustomerOrderLifeCycleStatusCode",
    "itemListCustomerOrderLifeCycleStatusName",
    "itemListFulfilmentProcessingStatusCode",
    "itemListFulfilmentProcessingStatusName",
    "itemListPlanningReleaseStatusCode",
    "itemListPlanningReleaseStatusName",
    "itemListExecutionReleaseStatusCode",
    "itemListExecutionReleaseStatusName",
    "confirmationIssuingStatusCode",
    "confirmationIssuingStatusName",
    "itemListInvoiceProcessingStatusCode",
    "itemListInvoiceProcessingStatusName",
    "approvalStatusCode",
    "approvalStatusName",
    "itemListProductAvailabilityConfirmationStatusCode",
    "itemListProductAvailabilityConfirmationStatusName",
    "consistencyStatusCode",
    "consistencyStatusName",
    "generalDataCompletenessStatusCode",
    "generalDataCompletenessStatusName",
    "cancellationStatusCode",
    "cancellationStatusName",
    "invoicingBlockingStatusCode",
    "invoicingBlockingStatusName",
    "fulfilmentBlockingStatusCode",
    "fulfilmentBlockingStatusName",
    "customerRequestReleaseStatusCode",
    "customerRequestReleaseStatusName",
    "releaseStatusCode",
    "releaseStatusName"
})
public class SalesOrderByElementsResponseStatus {

    @XmlElement(name = "PaymentAuthorisationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String paymentAuthorisationStatusCode;
    @XmlElement(name = "PaymentAuthorisationStatusName")
    protected Name paymentAuthorisationStatusName;
    @XmlElement(name = "ItemListCancellationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListCancellationStatusCode;
    @XmlElement(name = "ItemListCancellationStatusName")
    protected Name itemListCancellationStatusName;
    @XmlElement(name = "ItemListCustomerOrderLifeCycleStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListCustomerOrderLifeCycleStatusCode;
    @XmlElement(name = "ItemListCustomerOrderLifeCycleStatusName")
    protected Name itemListCustomerOrderLifeCycleStatusName;
    @XmlElement(name = "ItemListFulfilmentProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListFulfilmentProcessingStatusCode;
    @XmlElement(name = "ItemListFulfilmentProcessingStatusName")
    protected Name itemListFulfilmentProcessingStatusName;
    @XmlElement(name = "ItemListPlanningReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListPlanningReleaseStatusCode;
    @XmlElement(name = "ItemListPlanningReleaseStatusName")
    protected Name itemListPlanningReleaseStatusName;
    @XmlElement(name = "ItemListExecutionReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListExecutionReleaseStatusCode;
    @XmlElement(name = "ItemListExecutionReleaseStatusName")
    protected Name itemListExecutionReleaseStatusName;
    @XmlElement(name = "ConfirmationIssuingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String confirmationIssuingStatusCode;
    @XmlElement(name = "ConfirmationIssuingStatusName")
    protected Name confirmationIssuingStatusName;
    @XmlElement(name = "ItemListInvoiceProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListInvoiceProcessingStatusCode;
    @XmlElement(name = "ItemListInvoiceProcessingStatusName")
    protected Name itemListInvoiceProcessingStatusName;
    @XmlElement(name = "ApprovalStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String approvalStatusCode;
    @XmlElement(name = "ApprovalStatusName")
    protected Name approvalStatusName;
    @XmlElement(name = "ItemListProductAvailabilityConfirmationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemListProductAvailabilityConfirmationStatusCode;
    @XmlElement(name = "ItemListProductAvailabilityConfirmationStatusName")
    protected Name itemListProductAvailabilityConfirmationStatusName;
    @XmlElement(name = "ConsistencyStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String consistencyStatusCode;
    @XmlElement(name = "ConsistencyStatusName")
    protected Name consistencyStatusName;
    @XmlElement(name = "GeneralDataCompletenessStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String generalDataCompletenessStatusCode;
    @XmlElement(name = "GeneralDataCompletenessStatusName")
    protected Name generalDataCompletenessStatusName;
    @XmlElement(name = "CancellationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String cancellationStatusCode;
    @XmlElement(name = "CancellationStatusName")
    protected Name cancellationStatusName;
    @XmlElement(name = "InvoicingBlockingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String invoicingBlockingStatusCode;
    @XmlElement(name = "InvoicingBlockingStatusName")
    protected Name invoicingBlockingStatusName;
    @XmlElement(name = "FulfilmentBlockingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fulfilmentBlockingStatusCode;
    @XmlElement(name = "FulfilmentBlockingStatusName")
    protected Name fulfilmentBlockingStatusName;
    @XmlElement(name = "CustomerRequestReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String customerRequestReleaseStatusCode;
    @XmlElement(name = "CustomerRequestReleaseStatusName")
    protected Name customerRequestReleaseStatusName;
    @XmlElement(name = "ReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String releaseStatusCode;
    @XmlElement(name = "ReleaseStatusName")
    protected Name releaseStatusName;

    /**
     * Gets the value of the paymentAuthorisationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentAuthorisationStatusCode() {
        return paymentAuthorisationStatusCode;
    }

    /**
     * Sets the value of the paymentAuthorisationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentAuthorisationStatusCode(String value) {
        this.paymentAuthorisationStatusCode = value;
    }

    /**
     * Gets the value of the paymentAuthorisationStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getPaymentAuthorisationStatusName() {
        return paymentAuthorisationStatusName;
    }

    /**
     * Sets the value of the paymentAuthorisationStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setPaymentAuthorisationStatusName(Name value) {
        this.paymentAuthorisationStatusName = value;
    }

    /**
     * Gets the value of the itemListCancellationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListCancellationStatusCode() {
        return itemListCancellationStatusCode;
    }

    /**
     * Sets the value of the itemListCancellationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListCancellationStatusCode(String value) {
        this.itemListCancellationStatusCode = value;
    }

    /**
     * Gets the value of the itemListCancellationStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListCancellationStatusName() {
        return itemListCancellationStatusName;
    }

    /**
     * Sets the value of the itemListCancellationStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListCancellationStatusName(Name value) {
        this.itemListCancellationStatusName = value;
    }

    /**
     * Gets the value of the itemListCustomerOrderLifeCycleStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListCustomerOrderLifeCycleStatusCode() {
        return itemListCustomerOrderLifeCycleStatusCode;
    }

    /**
     * Sets the value of the itemListCustomerOrderLifeCycleStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListCustomerOrderLifeCycleStatusCode(String value) {
        this.itemListCustomerOrderLifeCycleStatusCode = value;
    }

    /**
     * Gets the value of the itemListCustomerOrderLifeCycleStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListCustomerOrderLifeCycleStatusName() {
        return itemListCustomerOrderLifeCycleStatusName;
    }

    /**
     * Sets the value of the itemListCustomerOrderLifeCycleStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListCustomerOrderLifeCycleStatusName(Name value) {
        this.itemListCustomerOrderLifeCycleStatusName = value;
    }

    /**
     * Gets the value of the itemListFulfilmentProcessingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListFulfilmentProcessingStatusCode() {
        return itemListFulfilmentProcessingStatusCode;
    }

    /**
     * Sets the value of the itemListFulfilmentProcessingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListFulfilmentProcessingStatusCode(String value) {
        this.itemListFulfilmentProcessingStatusCode = value;
    }

    /**
     * Gets the value of the itemListFulfilmentProcessingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListFulfilmentProcessingStatusName() {
        return itemListFulfilmentProcessingStatusName;
    }

    /**
     * Sets the value of the itemListFulfilmentProcessingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListFulfilmentProcessingStatusName(Name value) {
        this.itemListFulfilmentProcessingStatusName = value;
    }

    /**
     * Gets the value of the itemListPlanningReleaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListPlanningReleaseStatusCode() {
        return itemListPlanningReleaseStatusCode;
    }

    /**
     * Sets the value of the itemListPlanningReleaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListPlanningReleaseStatusCode(String value) {
        this.itemListPlanningReleaseStatusCode = value;
    }

    /**
     * Gets the value of the itemListPlanningReleaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListPlanningReleaseStatusName() {
        return itemListPlanningReleaseStatusName;
    }

    /**
     * Sets the value of the itemListPlanningReleaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListPlanningReleaseStatusName(Name value) {
        this.itemListPlanningReleaseStatusName = value;
    }

    /**
     * Gets the value of the itemListExecutionReleaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListExecutionReleaseStatusCode() {
        return itemListExecutionReleaseStatusCode;
    }

    /**
     * Sets the value of the itemListExecutionReleaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListExecutionReleaseStatusCode(String value) {
        this.itemListExecutionReleaseStatusCode = value;
    }

    /**
     * Gets the value of the itemListExecutionReleaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListExecutionReleaseStatusName() {
        return itemListExecutionReleaseStatusName;
    }

    /**
     * Sets the value of the itemListExecutionReleaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListExecutionReleaseStatusName(Name value) {
        this.itemListExecutionReleaseStatusName = value;
    }

    /**
     * Gets the value of the confirmationIssuingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationIssuingStatusCode() {
        return confirmationIssuingStatusCode;
    }

    /**
     * Sets the value of the confirmationIssuingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationIssuingStatusCode(String value) {
        this.confirmationIssuingStatusCode = value;
    }

    /**
     * Gets the value of the confirmationIssuingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getConfirmationIssuingStatusName() {
        return confirmationIssuingStatusName;
    }

    /**
     * Sets the value of the confirmationIssuingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setConfirmationIssuingStatusName(Name value) {
        this.confirmationIssuingStatusName = value;
    }

    /**
     * Gets the value of the itemListInvoiceProcessingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListInvoiceProcessingStatusCode() {
        return itemListInvoiceProcessingStatusCode;
    }

    /**
     * Sets the value of the itemListInvoiceProcessingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListInvoiceProcessingStatusCode(String value) {
        this.itemListInvoiceProcessingStatusCode = value;
    }

    /**
     * Gets the value of the itemListInvoiceProcessingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListInvoiceProcessingStatusName() {
        return itemListInvoiceProcessingStatusName;
    }

    /**
     * Sets the value of the itemListInvoiceProcessingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListInvoiceProcessingStatusName(Name value) {
        this.itemListInvoiceProcessingStatusName = value;
    }

    /**
     * Gets the value of the approvalStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalStatusCode() {
        return approvalStatusCode;
    }

    /**
     * Sets the value of the approvalStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalStatusCode(String value) {
        this.approvalStatusCode = value;
    }

    /**
     * Gets the value of the approvalStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getApprovalStatusName() {
        return approvalStatusName;
    }

    /**
     * Sets the value of the approvalStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setApprovalStatusName(Name value) {
        this.approvalStatusName = value;
    }

    /**
     * Gets the value of the itemListProductAvailabilityConfirmationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemListProductAvailabilityConfirmationStatusCode() {
        return itemListProductAvailabilityConfirmationStatusCode;
    }

    /**
     * Sets the value of the itemListProductAvailabilityConfirmationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemListProductAvailabilityConfirmationStatusCode(String value) {
        this.itemListProductAvailabilityConfirmationStatusCode = value;
    }

    /**
     * Gets the value of the itemListProductAvailabilityConfirmationStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getItemListProductAvailabilityConfirmationStatusName() {
        return itemListProductAvailabilityConfirmationStatusName;
    }

    /**
     * Sets the value of the itemListProductAvailabilityConfirmationStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setItemListProductAvailabilityConfirmationStatusName(Name value) {
        this.itemListProductAvailabilityConfirmationStatusName = value;
    }

    /**
     * Gets the value of the consistencyStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsistencyStatusCode() {
        return consistencyStatusCode;
    }

    /**
     * Sets the value of the consistencyStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsistencyStatusCode(String value) {
        this.consistencyStatusCode = value;
    }

    /**
     * Gets the value of the consistencyStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getConsistencyStatusName() {
        return consistencyStatusName;
    }

    /**
     * Sets the value of the consistencyStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setConsistencyStatusName(Name value) {
        this.consistencyStatusName = value;
    }

    /**
     * Gets the value of the generalDataCompletenessStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralDataCompletenessStatusCode() {
        return generalDataCompletenessStatusCode;
    }

    /**
     * Sets the value of the generalDataCompletenessStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralDataCompletenessStatusCode(String value) {
        this.generalDataCompletenessStatusCode = value;
    }

    /**
     * Gets the value of the generalDataCompletenessStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getGeneralDataCompletenessStatusName() {
        return generalDataCompletenessStatusName;
    }

    /**
     * Sets the value of the generalDataCompletenessStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setGeneralDataCompletenessStatusName(Name value) {
        this.generalDataCompletenessStatusName = value;
    }

    /**
     * Gets the value of the cancellationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationStatusCode() {
        return cancellationStatusCode;
    }

    /**
     * Sets the value of the cancellationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationStatusCode(String value) {
        this.cancellationStatusCode = value;
    }

    /**
     * Gets the value of the cancellationStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getCancellationStatusName() {
        return cancellationStatusName;
    }

    /**
     * Sets the value of the cancellationStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setCancellationStatusName(Name value) {
        this.cancellationStatusName = value;
    }

    /**
     * Gets the value of the invoicingBlockingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingBlockingStatusCode() {
        return invoicingBlockingStatusCode;
    }

    /**
     * Sets the value of the invoicingBlockingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingBlockingStatusCode(String value) {
        this.invoicingBlockingStatusCode = value;
    }

    /**
     * Gets the value of the invoicingBlockingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getInvoicingBlockingStatusName() {
        return invoicingBlockingStatusName;
    }

    /**
     * Sets the value of the invoicingBlockingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setInvoicingBlockingStatusName(Name value) {
        this.invoicingBlockingStatusName = value;
    }

    /**
     * Gets the value of the fulfilmentBlockingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfilmentBlockingStatusCode() {
        return fulfilmentBlockingStatusCode;
    }

    /**
     * Sets the value of the fulfilmentBlockingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfilmentBlockingStatusCode(String value) {
        this.fulfilmentBlockingStatusCode = value;
    }

    /**
     * Gets the value of the fulfilmentBlockingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getFulfilmentBlockingStatusName() {
        return fulfilmentBlockingStatusName;
    }

    /**
     * Sets the value of the fulfilmentBlockingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setFulfilmentBlockingStatusName(Name value) {
        this.fulfilmentBlockingStatusName = value;
    }

    /**
     * Gets the value of the customerRequestReleaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRequestReleaseStatusCode() {
        return customerRequestReleaseStatusCode;
    }

    /**
     * Sets the value of the customerRequestReleaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRequestReleaseStatusCode(String value) {
        this.customerRequestReleaseStatusCode = value;
    }

    /**
     * Gets the value of the customerRequestReleaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getCustomerRequestReleaseStatusName() {
        return customerRequestReleaseStatusName;
    }

    /**
     * Sets the value of the customerRequestReleaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setCustomerRequestReleaseStatusName(Name value) {
        this.customerRequestReleaseStatusName = value;
    }

    /**
     * Gets the value of the releaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseStatusCode() {
        return releaseStatusCode;
    }

    /**
     * Sets the value of the releaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseStatusCode(String value) {
        this.releaseStatusCode = value;
    }

    /**
     * Gets the value of the releaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getReleaseStatusName() {
        return releaseStatusName;
    }

    /**
     * Sets the value of the releaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setReleaseStatusName(Name value) {
        this.releaseStatusName = value;
    }

}
