
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsQuerySelectionByElements complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQuerySelectionByElements">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SelectionByID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByBuyerPartyID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByPartyID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByItemListCustomerOrderLifeCycleStatusCode" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByName" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByName" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByBuyerID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByPostingDate" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByDateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByDataOriginTypeCode" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByDataOriginTypeCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByItemListExecutionReleaseStatusCode" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByReleaseStatusCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByApprovalStatusCode" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByApprovalStatusCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByEmployeeResponsiblePartyID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByPartyID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionBySalesUnitPartyID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByPartyID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionBySalesAndServiceBusinessAreaSalesOrganisationID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByOrganisationalCentreID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionBySalesAndServiceBusinessAreaDistributionChannelCode" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByDistributionChannelCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByServicePerformerPartyID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByPartyID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByServiceExecutionTeamPartyID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByPartyID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByProductID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByProductID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByProductRequirementSpecificationID" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SelectionByLastChangedDate" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByDateTime" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8FAEA8DBFC7B542B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD7EF9E77604B9E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD7FBFABEA64BAA"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD804CEC07A8BF2"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD858E3B8BCED17"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA8E8EC05F573C"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA92B87175173D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA96943306173F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAA671698ED788"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAAAFC60FC178B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAAFAE2E83578D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAB40DC12FD790"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAB8A341D8D7F0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADABEB8C4E117F2"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAC3113A7457F8"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAF3DB9E8B98A6"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BDEA6EC7FDE47B2C"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA81F9BFA2D76A696D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA81FB7DF5AB7532A0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQuerySelectionByElements", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "selectionByID",
    "selectionByBuyerPartyID",
    "selectionByItemListCustomerOrderLifeCycleStatusCode",
    "selectionByName",
    "selectionByBuyerID",
    "selectionByPostingDate",
    "selectionByDataOriginTypeCode",
    "selectionByItemListExecutionReleaseStatusCode",
    "selectionByApprovalStatusCode",
    "selectionByEmployeeResponsiblePartyID",
    "selectionBySalesUnitPartyID",
    "selectionBySalesAndServiceBusinessAreaSalesOrganisationID",
    "selectionBySalesAndServiceBusinessAreaDistributionChannelCode",
    "selectionByServicePerformerPartyID",
    "selectionByServiceExecutionTeamPartyID",
    "selectionByProductID",
    "selectionByProductRequirementSpecificationID",
    "selectionByLastChangedDate",
    "servicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "consignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "ubicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "ubicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "ubicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "fechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "fechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "fechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "fechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "guaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "guaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "nmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "pzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "shipper4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "tipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "requierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "ndeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "consignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q",
    "consignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q"
})
public class SalesOrderByElementsQuerySelectionByElements {

    @XmlElement(name = "SelectionByID")
    protected List<SalesOrderByElementsQuerySelectionByID> selectionByID;
    @XmlElement(name = "SelectionByBuyerPartyID")
    protected List<SalesOrderByElementsQuerySelectionByPartyID> selectionByBuyerPartyID;
    @XmlElement(name = "SelectionByItemListCustomerOrderLifeCycleStatusCode")
    protected List<SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode> selectionByItemListCustomerOrderLifeCycleStatusCode;
    @XmlElement(name = "SelectionByName")
    protected List<SalesOrderByElementsQuerySelectionByName> selectionByName;
    @XmlElement(name = "SelectionByBuyerID")
    protected List<SalesOrderByElementsQuerySelectionByID> selectionByBuyerID;
    @XmlElement(name = "SelectionByPostingDate")
    protected List<SalesOrderByElementsQuerySelectionByDateTime> selectionByPostingDate;
    @XmlElement(name = "SelectionByDataOriginTypeCode")
    protected List<SalesOrderByElementsQuerySelectionByDataOriginTypeCode> selectionByDataOriginTypeCode;
    @XmlElement(name = "SelectionByItemListExecutionReleaseStatusCode")
    protected List<SalesOrderByElementsQuerySelectionByReleaseStatusCode> selectionByItemListExecutionReleaseStatusCode;
    @XmlElement(name = "SelectionByApprovalStatusCode")
    protected List<SalesOrderByElementsQuerySelectionByApprovalStatusCode> selectionByApprovalStatusCode;
    @XmlElement(name = "SelectionByEmployeeResponsiblePartyID")
    protected List<SalesOrderByElementsQuerySelectionByPartyID> selectionByEmployeeResponsiblePartyID;
    @XmlElement(name = "SelectionBySalesUnitPartyID")
    protected List<SalesOrderByElementsQuerySelectionByPartyID> selectionBySalesUnitPartyID;
    @XmlElement(name = "SelectionBySalesAndServiceBusinessAreaSalesOrganisationID")
    protected List<SalesOrderByElementsQuerySelectionByOrganisationalCentreID> selectionBySalesAndServiceBusinessAreaSalesOrganisationID;
    @XmlElement(name = "SelectionBySalesAndServiceBusinessAreaDistributionChannelCode")
    protected List<SalesOrderByElementsQuerySelectionByDistributionChannelCode> selectionBySalesAndServiceBusinessAreaDistributionChannelCode;
    @XmlElement(name = "SelectionByServicePerformerPartyID")
    protected List<SalesOrderByElementsQuerySelectionByPartyID> selectionByServicePerformerPartyID;
    @XmlElement(name = "SelectionByServiceExecutionTeamPartyID")
    protected List<SalesOrderByElementsQuerySelectionByPartyID> selectionByServiceExecutionTeamPartyID;
    @XmlElement(name = "SelectionByProductID")
    protected List<SalesOrderByElementsQuerySelectionByProductID> selectionByProductID;
    @XmlElement(name = "SelectionByProductRequirementSpecificationID")
    protected List<SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID> selectionByProductRequirementSpecificationID;
    @XmlElement(name = "SelectionByLastChangedDate")
    protected List<SalesOrderByElementsQuerySelectionByDateTime> selectionByLastChangedDate;
    @XmlElement(name = "ServicioFLAM_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText servicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "Consignatarios_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText consignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "UbicacinDestino_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText ubicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "UbicacionOrigen_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText ubicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "UbicacinCruce_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText ubicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "FechaCarga1_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByDate fechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "FechaCruce1_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByDate fechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "FechaDestino1_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByDate fechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "FechadeAgenteAduanal1_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByDate fechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "GuaHouse_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText guaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "GuaMaster_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText guaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "Nmerodeequipodecarga_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText nmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "PzsPBPC_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText pzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "Shipper_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText shipper4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "Tipodeequipo_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText tipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "RequierePOD_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText requierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "Ndeservicio1_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText ndeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "ConsignatarioDestino_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText consignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    @XmlElement(name = "ConsignatarioCruce_4UQSPAEZD9D7ZKNHZI3SQZD9Q", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected ExtSelectionByLongText consignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q;

    /**
     * Gets the value of the selectionByID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByID> getSelectionByID() {
        if (selectionByID == null) {
            selectionByID = new ArrayList<SalesOrderByElementsQuerySelectionByID>();
        }
        return this.selectionByID;
    }

    /**
     * Gets the value of the selectionByBuyerPartyID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByBuyerPartyID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByBuyerPartyID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByPartyID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByPartyID> getSelectionByBuyerPartyID() {
        if (selectionByBuyerPartyID == null) {
            selectionByBuyerPartyID = new ArrayList<SalesOrderByElementsQuerySelectionByPartyID>();
        }
        return this.selectionByBuyerPartyID;
    }

    /**
     * Gets the value of the selectionByItemListCustomerOrderLifeCycleStatusCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByItemListCustomerOrderLifeCycleStatusCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByItemListCustomerOrderLifeCycleStatusCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode> getSelectionByItemListCustomerOrderLifeCycleStatusCode() {
        if (selectionByItemListCustomerOrderLifeCycleStatusCode == null) {
            selectionByItemListCustomerOrderLifeCycleStatusCode = new ArrayList<SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode>();
        }
        return this.selectionByItemListCustomerOrderLifeCycleStatusCode;
    }

    /**
     * Gets the value of the selectionByName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByName }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByName> getSelectionByName() {
        if (selectionByName == null) {
            selectionByName = new ArrayList<SalesOrderByElementsQuerySelectionByName>();
        }
        return this.selectionByName;
    }

    /**
     * Gets the value of the selectionByBuyerID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByBuyerID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByBuyerID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByID> getSelectionByBuyerID() {
        if (selectionByBuyerID == null) {
            selectionByBuyerID = new ArrayList<SalesOrderByElementsQuerySelectionByID>();
        }
        return this.selectionByBuyerID;
    }

    /**
     * Gets the value of the selectionByPostingDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByPostingDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByPostingDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByDateTime }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByDateTime> getSelectionByPostingDate() {
        if (selectionByPostingDate == null) {
            selectionByPostingDate = new ArrayList<SalesOrderByElementsQuerySelectionByDateTime>();
        }
        return this.selectionByPostingDate;
    }

    /**
     * Gets the value of the selectionByDataOriginTypeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByDataOriginTypeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByDataOriginTypeCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByDataOriginTypeCode }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByDataOriginTypeCode> getSelectionByDataOriginTypeCode() {
        if (selectionByDataOriginTypeCode == null) {
            selectionByDataOriginTypeCode = new ArrayList<SalesOrderByElementsQuerySelectionByDataOriginTypeCode>();
        }
        return this.selectionByDataOriginTypeCode;
    }

    /**
     * Gets the value of the selectionByItemListExecutionReleaseStatusCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByItemListExecutionReleaseStatusCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByItemListExecutionReleaseStatusCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByReleaseStatusCode }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByReleaseStatusCode> getSelectionByItemListExecutionReleaseStatusCode() {
        if (selectionByItemListExecutionReleaseStatusCode == null) {
            selectionByItemListExecutionReleaseStatusCode = new ArrayList<SalesOrderByElementsQuerySelectionByReleaseStatusCode>();
        }
        return this.selectionByItemListExecutionReleaseStatusCode;
    }

    /**
     * Gets the value of the selectionByApprovalStatusCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByApprovalStatusCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByApprovalStatusCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByApprovalStatusCode }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByApprovalStatusCode> getSelectionByApprovalStatusCode() {
        if (selectionByApprovalStatusCode == null) {
            selectionByApprovalStatusCode = new ArrayList<SalesOrderByElementsQuerySelectionByApprovalStatusCode>();
        }
        return this.selectionByApprovalStatusCode;
    }

    /**
     * Gets the value of the selectionByEmployeeResponsiblePartyID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByEmployeeResponsiblePartyID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByEmployeeResponsiblePartyID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByPartyID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByPartyID> getSelectionByEmployeeResponsiblePartyID() {
        if (selectionByEmployeeResponsiblePartyID == null) {
            selectionByEmployeeResponsiblePartyID = new ArrayList<SalesOrderByElementsQuerySelectionByPartyID>();
        }
        return this.selectionByEmployeeResponsiblePartyID;
    }

    /**
     * Gets the value of the selectionBySalesUnitPartyID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionBySalesUnitPartyID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionBySalesUnitPartyID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByPartyID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByPartyID> getSelectionBySalesUnitPartyID() {
        if (selectionBySalesUnitPartyID == null) {
            selectionBySalesUnitPartyID = new ArrayList<SalesOrderByElementsQuerySelectionByPartyID>();
        }
        return this.selectionBySalesUnitPartyID;
    }

    /**
     * Gets the value of the selectionBySalesAndServiceBusinessAreaSalesOrganisationID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionBySalesAndServiceBusinessAreaSalesOrganisationID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionBySalesAndServiceBusinessAreaSalesOrganisationID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByOrganisationalCentreID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByOrganisationalCentreID> getSelectionBySalesAndServiceBusinessAreaSalesOrganisationID() {
        if (selectionBySalesAndServiceBusinessAreaSalesOrganisationID == null) {
            selectionBySalesAndServiceBusinessAreaSalesOrganisationID = new ArrayList<SalesOrderByElementsQuerySelectionByOrganisationalCentreID>();
        }
        return this.selectionBySalesAndServiceBusinessAreaSalesOrganisationID;
    }

    /**
     * Gets the value of the selectionBySalesAndServiceBusinessAreaDistributionChannelCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionBySalesAndServiceBusinessAreaDistributionChannelCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionBySalesAndServiceBusinessAreaDistributionChannelCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByDistributionChannelCode }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByDistributionChannelCode> getSelectionBySalesAndServiceBusinessAreaDistributionChannelCode() {
        if (selectionBySalesAndServiceBusinessAreaDistributionChannelCode == null) {
            selectionBySalesAndServiceBusinessAreaDistributionChannelCode = new ArrayList<SalesOrderByElementsQuerySelectionByDistributionChannelCode>();
        }
        return this.selectionBySalesAndServiceBusinessAreaDistributionChannelCode;
    }

    /**
     * Gets the value of the selectionByServicePerformerPartyID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByServicePerformerPartyID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByServicePerformerPartyID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByPartyID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByPartyID> getSelectionByServicePerformerPartyID() {
        if (selectionByServicePerformerPartyID == null) {
            selectionByServicePerformerPartyID = new ArrayList<SalesOrderByElementsQuerySelectionByPartyID>();
        }
        return this.selectionByServicePerformerPartyID;
    }

    /**
     * Gets the value of the selectionByServiceExecutionTeamPartyID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByServiceExecutionTeamPartyID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByServiceExecutionTeamPartyID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByPartyID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByPartyID> getSelectionByServiceExecutionTeamPartyID() {
        if (selectionByServiceExecutionTeamPartyID == null) {
            selectionByServiceExecutionTeamPartyID = new ArrayList<SalesOrderByElementsQuerySelectionByPartyID>();
        }
        return this.selectionByServiceExecutionTeamPartyID;
    }

    /**
     * Gets the value of the selectionByProductID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByProductID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByProductID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByProductID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByProductID> getSelectionByProductID() {
        if (selectionByProductID == null) {
            selectionByProductID = new ArrayList<SalesOrderByElementsQuerySelectionByProductID>();
        }
        return this.selectionByProductID;
    }

    /**
     * Gets the value of the selectionByProductRequirementSpecificationID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByProductRequirementSpecificationID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByProductRequirementSpecificationID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID> getSelectionByProductRequirementSpecificationID() {
        if (selectionByProductRequirementSpecificationID == null) {
            selectionByProductRequirementSpecificationID = new ArrayList<SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID>();
        }
        return this.selectionByProductRequirementSpecificationID;
    }

    /**
     * Gets the value of the selectionByLastChangedDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByLastChangedDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByLastChangedDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsQuerySelectionByDateTime }
     * 
     * 
     */
    public List<SalesOrderByElementsQuerySelectionByDateTime> getSelectionByLastChangedDate() {
        if (selectionByLastChangedDate == null) {
            selectionByLastChangedDate = new ArrayList<SalesOrderByElementsQuerySelectionByDateTime>();
        }
        return this.selectionByLastChangedDate;
    }

    /**
     * Gets the value of the servicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getServicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return servicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the servicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setServicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.servicioFLAM4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the consignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getConsignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return consignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the consignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setConsignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.consignatarios4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the ubicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getUbicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return ubicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the ubicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setUbicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.ubicacinDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the ubicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getUbicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return ubicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the ubicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setUbicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.ubicacionOrigen4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the ubicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getUbicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return ubicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the ubicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setUbicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.ubicacinCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the fechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public ExtSelectionByDate getFechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return fechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the fechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public void setFechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByDate value) {
        this.fechaCarga14UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the fechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public ExtSelectionByDate getFechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return fechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the fechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public void setFechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByDate value) {
        this.fechaCruce14UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the fechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public ExtSelectionByDate getFechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return fechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the fechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public void setFechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByDate value) {
        this.fechaDestino14UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the fechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public ExtSelectionByDate getFechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return fechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the fechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByDate }
     *     
     */
    public void setFechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByDate value) {
        this.fechadeAgenteAduanal14UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the guaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getGuaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return guaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the guaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setGuaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.guaHouse4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the guaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getGuaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return guaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the guaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setGuaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.guaMaster4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the nmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getNmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return nmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the nmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setNmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.nmerodeequipodecarga4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the pzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getPzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return pzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the pzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setPzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.pzsPBPC4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the shipper4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getShipper4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return shipper4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the shipper4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setShipper4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.shipper4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the tipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getTipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return tipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the tipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setTipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.tipodeequipo4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the requierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getRequierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return requierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the requierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setRequierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.requierePOD4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the ndeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getNdeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return ndeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the ndeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setNdeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.ndeservicio14UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the consignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getConsignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return consignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the consignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setConsignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.consignatarioDestino4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

    /**
     * Gets the value of the consignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @return
     *     possible object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public ExtSelectionByLongText getConsignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q() {
        return consignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q;
    }

    /**
     * Sets the value of the consignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtSelectionByLongText }
     *     
     */
    public void setConsignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q(ExtSelectionByLongText value) {
        this.consignatarioCruce4UQSPAEZD9D7ZKNHZI3SQZD9Q = value;
    }

}
