
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="MajorLevelOrdinalNumberValue" type="{http://sap.com/xi/AP/Common/GDT}OrdinalNumberValue" minOccurs="0"/>
 *         &lt;element name="MinorLevelOrdinalNumberValue" type="{http://sap.com/xi/AP/Common/GDT}OrdinalNumberValue" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}PriceSpecificationElementTypeCode" minOccurs="0"/>
 *         &lt;element name="CategoryCode" type="{http://sap.com/xi/AP/Common/GDT}PriceSpecificationElementCategoryCode" minOccurs="0"/>
 *         &lt;element name="PurposeCode" type="{http://sap.com/xi/AP/Common/GDT}PriceSpecificationElementPurposeCode" minOccurs="0"/>
 *         &lt;element name="Rate" type="{http://sap.com/xi/AP/Common/GDT}Rate" minOccurs="0"/>
 *         &lt;element name="RateBaseQuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="CalculationBasis" type="{http://sap.com/xi/AP/Common/GDT}PriceComponentCalculationBasis" minOccurs="0"/>
 *         &lt;element name="CalculatedAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="RoundingDifferenceAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="EffectiveIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="GroupedIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ManuallyChangedIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="DescriptionManuallyChangedIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FixationCode" type="{http://sap.com/xi/AP/Common/GDT}PriceComponentFixationCode" minOccurs="0"/>
 *         &lt;element name="InactivityReasonCode" type="{http://sap.com/xi/AP/Common/GDT}PriceComponentInactivityReasonCode" minOccurs="0"/>
 *         &lt;element name="OriginCode" type="{http://sap.com/xi/AP/Common/GDT}PriceComponentOriginCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "uuid",
    "description",
    "majorLevelOrdinalNumberValue",
    "minorLevelOrdinalNumberValue",
    "typeCode",
    "categoryCode",
    "purposeCode",
    "rate",
    "rateBaseQuantityTypeCode",
    "calculationBasis",
    "calculatedAmount",
    "roundingDifferenceAmount",
    "effectiveIndicator",
    "groupedIndicator",
    "manuallyChangedIndicator",
    "descriptionManuallyChangedIndicator",
    "fixationCode",
    "inactivityReasonCode",
    "originCode"
})
public class SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "Description")
    protected SHORTDescription description;
    @XmlElement(name = "MajorLevelOrdinalNumberValue")
    protected Integer majorLevelOrdinalNumberValue;
    @XmlElement(name = "MinorLevelOrdinalNumberValue")
    protected Integer minorLevelOrdinalNumberValue;
    @XmlElement(name = "TypeCode")
    protected PriceSpecificationElementTypeCode typeCode;
    @XmlElement(name = "CategoryCode")
    protected PriceSpecificationElementCategoryCode categoryCode;
    @XmlElement(name = "PurposeCode")
    protected PriceSpecificationElementPurposeCode purposeCode;
    @XmlElement(name = "Rate")
    protected Rate rate;
    @XmlElement(name = "RateBaseQuantityTypeCode")
    protected QuantityTypeCode rateBaseQuantityTypeCode;
    @XmlElement(name = "CalculationBasis")
    protected PriceComponentCalculationBasis calculationBasis;
    @XmlElement(name = "CalculatedAmount")
    protected Amount calculatedAmount;
    @XmlElement(name = "RoundingDifferenceAmount")
    protected Amount roundingDifferenceAmount;
    @XmlElement(name = "EffectiveIndicator")
    protected Boolean effectiveIndicator;
    @XmlElement(name = "GroupedIndicator")
    protected Boolean groupedIndicator;
    @XmlElement(name = "ManuallyChangedIndicator")
    protected Boolean manuallyChangedIndicator;
    @XmlElement(name = "DescriptionManuallyChangedIndicator")
    protected Boolean descriptionManuallyChangedIndicator;
    @XmlElement(name = "FixationCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fixationCode;
    @XmlElement(name = "InactivityReasonCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String inactivityReasonCode;
    @XmlElement(name = "OriginCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String originCode;

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setDescription(SHORTDescription value) {
        this.description = value;
    }

    /**
     * Gets the value of the majorLevelOrdinalNumberValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMajorLevelOrdinalNumberValue() {
        return majorLevelOrdinalNumberValue;
    }

    /**
     * Sets the value of the majorLevelOrdinalNumberValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMajorLevelOrdinalNumberValue(Integer value) {
        this.majorLevelOrdinalNumberValue = value;
    }

    /**
     * Gets the value of the minorLevelOrdinalNumberValue property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMinorLevelOrdinalNumberValue() {
        return minorLevelOrdinalNumberValue;
    }

    /**
     * Sets the value of the minorLevelOrdinalNumberValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMinorLevelOrdinalNumberValue(Integer value) {
        this.minorLevelOrdinalNumberValue = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link PriceSpecificationElementTypeCode }
     *     
     */
    public PriceSpecificationElementTypeCode getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceSpecificationElementTypeCode }
     *     
     */
    public void setTypeCode(PriceSpecificationElementTypeCode value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the categoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link PriceSpecificationElementCategoryCode }
     *     
     */
    public PriceSpecificationElementCategoryCode getCategoryCode() {
        return categoryCode;
    }

    /**
     * Sets the value of the categoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceSpecificationElementCategoryCode }
     *     
     */
    public void setCategoryCode(PriceSpecificationElementCategoryCode value) {
        this.categoryCode = value;
    }

    /**
     * Gets the value of the purposeCode property.
     * 
     * @return
     *     possible object is
     *     {@link PriceSpecificationElementPurposeCode }
     *     
     */
    public PriceSpecificationElementPurposeCode getPurposeCode() {
        return purposeCode;
    }

    /**
     * Sets the value of the purposeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceSpecificationElementPurposeCode }
     *     
     */
    public void setPurposeCode(PriceSpecificationElementPurposeCode value) {
        this.purposeCode = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link Rate }
     *     
     */
    public Rate getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Rate }
     *     
     */
    public void setRate(Rate value) {
        this.rate = value;
    }

    /**
     * Gets the value of the rateBaseQuantityTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getRateBaseQuantityTypeCode() {
        return rateBaseQuantityTypeCode;
    }

    /**
     * Sets the value of the rateBaseQuantityTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setRateBaseQuantityTypeCode(QuantityTypeCode value) {
        this.rateBaseQuantityTypeCode = value;
    }

    /**
     * Gets the value of the calculationBasis property.
     * 
     * @return
     *     possible object is
     *     {@link PriceComponentCalculationBasis }
     *     
     */
    public PriceComponentCalculationBasis getCalculationBasis() {
        return calculationBasis;
    }

    /**
     * Sets the value of the calculationBasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceComponentCalculationBasis }
     *     
     */
    public void setCalculationBasis(PriceComponentCalculationBasis value) {
        this.calculationBasis = value;
    }

    /**
     * Gets the value of the calculatedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getCalculatedAmount() {
        return calculatedAmount;
    }

    /**
     * Sets the value of the calculatedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setCalculatedAmount(Amount value) {
        this.calculatedAmount = value;
    }

    /**
     * Gets the value of the roundingDifferenceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getRoundingDifferenceAmount() {
        return roundingDifferenceAmount;
    }

    /**
     * Sets the value of the roundingDifferenceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setRoundingDifferenceAmount(Amount value) {
        this.roundingDifferenceAmount = value;
    }

    /**
     * Gets the value of the effectiveIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEffectiveIndicator() {
        return effectiveIndicator;
    }

    /**
     * Sets the value of the effectiveIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEffectiveIndicator(Boolean value) {
        this.effectiveIndicator = value;
    }

    /**
     * Gets the value of the groupedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGroupedIndicator() {
        return groupedIndicator;
    }

    /**
     * Sets the value of the groupedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGroupedIndicator(Boolean value) {
        this.groupedIndicator = value;
    }

    /**
     * Gets the value of the manuallyChangedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isManuallyChangedIndicator() {
        return manuallyChangedIndicator;
    }

    /**
     * Sets the value of the manuallyChangedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setManuallyChangedIndicator(Boolean value) {
        this.manuallyChangedIndicator = value;
    }

    /**
     * Gets the value of the descriptionManuallyChangedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDescriptionManuallyChangedIndicator() {
        return descriptionManuallyChangedIndicator;
    }

    /**
     * Sets the value of the descriptionManuallyChangedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDescriptionManuallyChangedIndicator(Boolean value) {
        this.descriptionManuallyChangedIndicator = value;
    }

    /**
     * Gets the value of the fixationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFixationCode() {
        return fixationCode;
    }

    /**
     * Sets the value of the fixationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFixationCode(String value) {
        this.fixationCode = value;
    }

    /**
     * Gets the value of the inactivityReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInactivityReasonCode() {
        return inactivityReasonCode;
    }

    /**
     * Sets the value of the inactivityReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInactivityReasonCode(String value) {
        this.inactivityReasonCode = value;
    }

    /**
     * Gets the value of the originCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginCode() {
        return originCode;
    }

    /**
     * Sets the value of the originCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginCode(String value) {
        this.originCode = value;
    }

}
