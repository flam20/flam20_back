
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SalesOrderByElementsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="BuyerID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="PostingDate" type="{http://sap.com/xi/BASIS/Global}GLOBAL_DateTime" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://sap.com/xi/AP/Common/GDT}EXTENDED_Name" minOccurs="0"/>
 *         &lt;element name="ChangeStateID" type="{http://sap.com/xi/AP/Common/GDT}ChangeStateID" minOccurs="0"/>
 *         &lt;element name="DataOriginTypeCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerTransactionDocumentDataOriginTypeCode" minOccurs="0"/>
 *         &lt;element name="FulfillmentBlockingReasonCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerTransactionDocumentFulfilmentBlockingReasonCode" minOccurs="0"/>
 *         &lt;element name="ServiceConfirmationCreationCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerTransactionDocumentServiceConfirmationCreationCode" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseStatus" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReference" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseBusinessTransactionDocumentReference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SalesAndServiceBusinessArea" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseSalesAndServiceBusinessArea" minOccurs="0"/>
 *         &lt;element name="BillToParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="AccountParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="PayerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="ProductRecipientParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="SalesPartnerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="FreightForwarderParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="EmployeeResponsibleParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="SellerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="SalesUnitParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="ServiceExecutionTeamParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="ServicePerformerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="SalesEmployeeParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="BillFromParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="RequestedFulfillmentPeriodPeriodTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms" minOccurs="0"/>
 *         &lt;element name="DeliveryTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="PricingTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePricingTerms" minOccurs="0"/>
 *         &lt;element name="SalesTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseSalesTerms" minOccurs="0"/>
 *         &lt;element name="InvoiceTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseInvoiceTerms" minOccurs="0"/>
 *         &lt;element name="Item" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CashDiscountTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseCashDiscountTerms" minOccurs="0"/>
 *         &lt;element name="PaymentControl" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePaymentControl" minOccurs="0"/>
 *         &lt;element name="PriceAndTaxCalculation" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculation" minOccurs="0"/>
 *         &lt;element name="TextCollection" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseTextCollection" minOccurs="0"/>
 *         &lt;element name="AttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}AccessAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="CroatiaBusinessSpaceID" type="{http://sap.com/xi/AP/Globalization}GLOBusinessSpaceID" minOccurs="0"/>
 *         &lt;element name="CroatiaBillingMachineID" type="{http://sap.com/xi/AP/Globalization}HR_BillingMachineID" minOccurs="0"/>
 *         &lt;element name="CroatiaPaymentMethodCode" type="{http://sap.com/xi/AP/Globalization}CroatiaPaymentMethodCode" minOccurs="0"/>
 *         &lt;element name="CroatiaPartyTaxID" type="{http://sap.com/xi/AP/Common/GDT}PartyTaxID" minOccurs="0"/>
 *         &lt;element name="CroatiaParagonBillMark" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_Text" minOccurs="0"/>
 *         &lt;element name="CroatiaSpecialNote" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_Text" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8FAEA7207B08D420"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD7EE3623884B9E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD7F982CAB54BA9"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD80309BB8ECBF1"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD8578D0DD98D16"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA8D3F8642D731"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA918F0E01973D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA95671812973F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAA5406C8F5788"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAA9BD1B27D78A"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAAE562E75978D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAB2C4F340178F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAB6C3904597E5"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADABD58AEF297F2"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAC1ECF3BBD7F5"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAEFB8218A58A3"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BDEA6D0363CA9AE8"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA81F9AB7674FB8905"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA81FB7CBC610ED2A0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponse", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "id",
    "uuid",
    "buyerID",
    "postingDate",
    "name",
    "changeStateID",
    "dataOriginTypeCode",
    "fulfillmentBlockingReasonCode",
    "serviceConfirmationCreationCode",
    "status",
    "businessTransactionDocumentReference",
    "salesAndServiceBusinessArea",
    "billToParty",
    "accountParty",
    "payerParty",
    "productRecipientParty",
    "salesPartnerParty",
    "freightForwarderParty",
    "employeeResponsibleParty",
    "sellerParty",
    "salesUnitParty",
    "serviceExecutionTeamParty",
    "servicePerformerParty",
    "salesEmployeeParty",
    "billFromParty",
    "requestedFulfillmentPeriodPeriodTerms",
    "deliveryTerms",
    "pricingTerms",
    "salesTerms",
    "invoiceTerms",
    "item",
    "cashDiscountTerms",
    "paymentControl",
    "priceAndTaxCalculation",
    "textCollection",
    "attachmentFolder",
    "croatiaBusinessSpaceID",
    "croatiaBillingMachineID",
    "croatiaPaymentMethodCode",
    "croatiaPartyTaxID",
    "croatiaParagonBillMark",
    "croatiaSpecialNote",
    "servicioFLAM",
    "consignatarios",
    "ubicacinDestino",
    "ubicacionOrigen",
    "ubicacinCruce",
    "fechaCarga1",
    "fechaCruce1",
    "fechaDestino1",
    "fechadeAgenteAduanal1",
    "guaHouse",
    "guaMaster",
    "nmerodeequipodecarga",
    "pzsPBPC",
    "shipper",
    "tipodeequipo",
    "requierePOD",
    "ndeservicio1",
    "consignatarioDestino",
    "consignatarioCruce"
})
public class SalesOrderByElementsResponse {

    @XmlElement(name = "ID")
    protected BusinessTransactionDocumentID id;
    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "BuyerID")
    protected BusinessTransactionDocumentID buyerID;
    @XmlElement(name = "PostingDate")
    protected XMLGregorianCalendar postingDate;
    @XmlElement(name = "Name")
    protected EXTENDEDName name;
    @XmlElement(name = "ChangeStateID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String changeStateID;
    @XmlElement(name = "DataOriginTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String dataOriginTypeCode;
    @XmlElement(name = "FulfillmentBlockingReasonCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fulfillmentBlockingReasonCode;
    @XmlElement(name = "ServiceConfirmationCreationCode")
    protected CustomerTransactionDocumentServiceConfirmationCreationCode serviceConfirmationCreationCode;
    @XmlElement(name = "Status")
    protected SalesOrderByElementsResponseStatus status;
    @XmlElement(name = "BusinessTransactionDocumentReference")
    protected List<SalesOrderByElementsResponseBusinessTransactionDocumentReference> businessTransactionDocumentReference;
    @XmlElement(name = "SalesAndServiceBusinessArea")
    protected SalesOrderByElementsResponseSalesAndServiceBusinessArea salesAndServiceBusinessArea;
    @XmlElement(name = "BillToParty")
    protected SalesOrderByElementsResponseParty billToParty;
    @XmlElement(name = "AccountParty")
    protected SalesOrderByElementsResponseParty accountParty;
    @XmlElement(name = "PayerParty")
    protected SalesOrderByElementsResponseParty payerParty;
    @XmlElement(name = "ProductRecipientParty")
    protected SalesOrderByElementsResponseParty productRecipientParty;
    @XmlElement(name = "SalesPartnerParty")
    protected SalesOrderByElementsResponseParty salesPartnerParty;
    @XmlElement(name = "FreightForwarderParty")
    protected SalesOrderByElementsResponseParty freightForwarderParty;
    @XmlElement(name = "EmployeeResponsibleParty")
    protected SalesOrderByElementsResponseParty employeeResponsibleParty;
    @XmlElement(name = "SellerParty")
    protected SalesOrderByElementsResponseParty sellerParty;
    @XmlElement(name = "SalesUnitParty")
    protected SalesOrderByElementsResponseParty salesUnitParty;
    @XmlElement(name = "ServiceExecutionTeamParty")
    protected SalesOrderByElementsResponseParty serviceExecutionTeamParty;
    @XmlElement(name = "ServicePerformerParty")
    protected SalesOrderByElementsResponseParty servicePerformerParty;
    @XmlElement(name = "SalesEmployeeParty")
    protected SalesOrderByElementsResponseParty salesEmployeeParty;
    @XmlElement(name = "BillFromParty")
    protected SalesOrderByElementsResponseParty billFromParty;
    @XmlElement(name = "RequestedFulfillmentPeriodPeriodTerms")
    protected SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms requestedFulfillmentPeriodPeriodTerms;
    @XmlElement(name = "DeliveryTerms")
    protected SalesOrderByElementsResponseDeliveryTerms deliveryTerms;
    @XmlElement(name = "PricingTerms")
    protected SalesOrderByElementsResponsePricingTerms pricingTerms;
    @XmlElement(name = "SalesTerms")
    protected SalesOrderByElementsResponseSalesTerms salesTerms;
    @XmlElement(name = "InvoiceTerms")
    protected SalesOrderByElementsResponseInvoiceTerms invoiceTerms;
    @XmlElement(name = "Item")
    protected List<SalesOrderByElementsResponseItem> item;
    @XmlElement(name = "CashDiscountTerms")
    protected SalesOrderByElementsResponseCashDiscountTerms cashDiscountTerms;
    @XmlElement(name = "PaymentControl")
    protected SalesOrderByElementsResponsePaymentControl paymentControl;
    @XmlElement(name = "PriceAndTaxCalculation")
    protected SalesOrderByElementsResponsePriceAndTaxCalculation priceAndTaxCalculation;
    @XmlElement(name = "TextCollection")
    protected SalesOrderByElementsResponseTextCollection textCollection;
    @XmlElement(name = "AttachmentFolder")
    protected AccessAttachmentFolder attachmentFolder;
    @XmlElement(name = "CroatiaBusinessSpaceID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String croatiaBusinessSpaceID;
    @XmlElement(name = "CroatiaBillingMachineID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String croatiaBillingMachineID;
    @XmlElement(name = "CroatiaPaymentMethodCode")
    protected CroatiaPaymentMethodCode croatiaPaymentMethodCode;
    @XmlElement(name = "CroatiaPartyTaxID")
    protected PartyTaxID croatiaPartyTaxID;
    @XmlElement(name = "CroatiaParagonBillMark")
    protected String croatiaParagonBillMark;
    @XmlElement(name = "CroatiaSpecialNote")
    protected String croatiaSpecialNote;
    @XmlElement(name = "ServicioFLAM", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String servicioFLAM;
    @XmlElement(name = "Consignatarios", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarios;
    @XmlElement(name = "UbicacinDestino", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestino;
    @XmlElement(name = "UbicacionOrigen", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacionOrigen;
    @XmlElement(name = "UbicacinCruce", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCruce;
    @XmlElement(name = "FechaCarga1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCarga1;
    @XmlElement(name = "FechaCruce1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCruce1;
    @XmlElement(name = "FechaDestino1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaDestino1;
    @XmlElement(name = "FechadeAgenteAduanal1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechadeAgenteAduanal1;
    @XmlElement(name = "GuaHouse", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHouse;
    @XmlElement(name = "GuaMaster", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMaster;
    @XmlElement(name = "Nmerodeequipodecarga", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipodecarga;
    @XmlElement(name = "PzsPBPC", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pzsPBPC;
    @XmlElement(name = "Shipper", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String shipper;
    @XmlElement(name = "Tipodeequipo", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeequipo;
    @XmlElement(name = "RequierePOD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String requierePOD;
    @XmlElement(name = "Ndeservicio1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ndeservicio1;
    @XmlElement(name = "ConsignatarioDestino", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioDestino;
    @XmlElement(name = "ConsignatarioCruce", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioCruce;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setID(BusinessTransactionDocumentID value) {
        this.id = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the buyerID property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBuyerID() {
        return buyerID;
    }

    /**
     * Sets the value of the buyerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBuyerID(BusinessTransactionDocumentID value) {
        this.buyerID = value;
    }

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostingDate(XMLGregorianCalendar value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link EXTENDEDName }
     *     
     */
    public EXTENDEDName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link EXTENDEDName }
     *     
     */
    public void setName(EXTENDEDName value) {
        this.name = value;
    }

    /**
     * Gets the value of the changeStateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeStateID() {
        return changeStateID;
    }

    /**
     * Sets the value of the changeStateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeStateID(String value) {
        this.changeStateID = value;
    }

    /**
     * Gets the value of the dataOriginTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataOriginTypeCode() {
        return dataOriginTypeCode;
    }

    /**
     * Sets the value of the dataOriginTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataOriginTypeCode(String value) {
        this.dataOriginTypeCode = value;
    }

    /**
     * Gets the value of the fulfillmentBlockingReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentBlockingReasonCode() {
        return fulfillmentBlockingReasonCode;
    }

    /**
     * Sets the value of the fulfillmentBlockingReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentBlockingReasonCode(String value) {
        this.fulfillmentBlockingReasonCode = value;
    }

    /**
     * Gets the value of the serviceConfirmationCreationCode property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerTransactionDocumentServiceConfirmationCreationCode }
     *     
     */
    public CustomerTransactionDocumentServiceConfirmationCreationCode getServiceConfirmationCreationCode() {
        return serviceConfirmationCreationCode;
    }

    /**
     * Sets the value of the serviceConfirmationCreationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerTransactionDocumentServiceConfirmationCreationCode }
     *     
     */
    public void setServiceConfirmationCreationCode(CustomerTransactionDocumentServiceConfirmationCreationCode value) {
        this.serviceConfirmationCreationCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseStatus }
     *     
     */
    public SalesOrderByElementsResponseStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseStatus }
     *     
     */
    public void setStatus(SalesOrderByElementsResponseStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the businessTransactionDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessTransactionDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessTransactionDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponseBusinessTransactionDocumentReference }
     * 
     * 
     */
    public List<SalesOrderByElementsResponseBusinessTransactionDocumentReference> getBusinessTransactionDocumentReference() {
        if (businessTransactionDocumentReference == null) {
            businessTransactionDocumentReference = new ArrayList<SalesOrderByElementsResponseBusinessTransactionDocumentReference>();
        }
        return this.businessTransactionDocumentReference;
    }

    /**
     * Gets the value of the salesAndServiceBusinessArea property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseSalesAndServiceBusinessArea }
     *     
     */
    public SalesOrderByElementsResponseSalesAndServiceBusinessArea getSalesAndServiceBusinessArea() {
        return salesAndServiceBusinessArea;
    }

    /**
     * Sets the value of the salesAndServiceBusinessArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseSalesAndServiceBusinessArea }
     *     
     */
    public void setSalesAndServiceBusinessArea(SalesOrderByElementsResponseSalesAndServiceBusinessArea value) {
        this.salesAndServiceBusinessArea = value;
    }

    /**
     * Gets the value of the billToParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getBillToParty() {
        return billToParty;
    }

    /**
     * Sets the value of the billToParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setBillToParty(SalesOrderByElementsResponseParty value) {
        this.billToParty = value;
    }

    /**
     * Gets the value of the accountParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getAccountParty() {
        return accountParty;
    }

    /**
     * Sets the value of the accountParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setAccountParty(SalesOrderByElementsResponseParty value) {
        this.accountParty = value;
    }

    /**
     * Gets the value of the payerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getPayerParty() {
        return payerParty;
    }

    /**
     * Sets the value of the payerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setPayerParty(SalesOrderByElementsResponseParty value) {
        this.payerParty = value;
    }

    /**
     * Gets the value of the productRecipientParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getProductRecipientParty() {
        return productRecipientParty;
    }

    /**
     * Sets the value of the productRecipientParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setProductRecipientParty(SalesOrderByElementsResponseParty value) {
        this.productRecipientParty = value;
    }

    /**
     * Gets the value of the salesPartnerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getSalesPartnerParty() {
        return salesPartnerParty;
    }

    /**
     * Sets the value of the salesPartnerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setSalesPartnerParty(SalesOrderByElementsResponseParty value) {
        this.salesPartnerParty = value;
    }

    /**
     * Gets the value of the freightForwarderParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getFreightForwarderParty() {
        return freightForwarderParty;
    }

    /**
     * Sets the value of the freightForwarderParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setFreightForwarderParty(SalesOrderByElementsResponseParty value) {
        this.freightForwarderParty = value;
    }

    /**
     * Gets the value of the employeeResponsibleParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getEmployeeResponsibleParty() {
        return employeeResponsibleParty;
    }

    /**
     * Sets the value of the employeeResponsibleParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setEmployeeResponsibleParty(SalesOrderByElementsResponseParty value) {
        this.employeeResponsibleParty = value;
    }

    /**
     * Gets the value of the sellerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getSellerParty() {
        return sellerParty;
    }

    /**
     * Sets the value of the sellerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setSellerParty(SalesOrderByElementsResponseParty value) {
        this.sellerParty = value;
    }

    /**
     * Gets the value of the salesUnitParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getSalesUnitParty() {
        return salesUnitParty;
    }

    /**
     * Sets the value of the salesUnitParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setSalesUnitParty(SalesOrderByElementsResponseParty value) {
        this.salesUnitParty = value;
    }

    /**
     * Gets the value of the serviceExecutionTeamParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getServiceExecutionTeamParty() {
        return serviceExecutionTeamParty;
    }

    /**
     * Sets the value of the serviceExecutionTeamParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setServiceExecutionTeamParty(SalesOrderByElementsResponseParty value) {
        this.serviceExecutionTeamParty = value;
    }

    /**
     * Gets the value of the servicePerformerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getServicePerformerParty() {
        return servicePerformerParty;
    }

    /**
     * Sets the value of the servicePerformerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setServicePerformerParty(SalesOrderByElementsResponseParty value) {
        this.servicePerformerParty = value;
    }

    /**
     * Gets the value of the salesEmployeeParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getSalesEmployeeParty() {
        return salesEmployeeParty;
    }

    /**
     * Sets the value of the salesEmployeeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setSalesEmployeeParty(SalesOrderByElementsResponseParty value) {
        this.salesEmployeeParty = value;
    }

    /**
     * Gets the value of the billFromParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getBillFromParty() {
        return billFromParty;
    }

    /**
     * Sets the value of the billFromParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setBillFromParty(SalesOrderByElementsResponseParty value) {
        this.billFromParty = value;
    }

    /**
     * Gets the value of the requestedFulfillmentPeriodPeriodTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms }
     *     
     */
    public SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms getRequestedFulfillmentPeriodPeriodTerms() {
        return requestedFulfillmentPeriodPeriodTerms;
    }

    /**
     * Sets the value of the requestedFulfillmentPeriodPeriodTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms }
     *     
     */
    public void setRequestedFulfillmentPeriodPeriodTerms(SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms value) {
        this.requestedFulfillmentPeriodPeriodTerms = value;
    }

    /**
     * Gets the value of the deliveryTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseDeliveryTerms }
     *     
     */
    public SalesOrderByElementsResponseDeliveryTerms getDeliveryTerms() {
        return deliveryTerms;
    }

    /**
     * Sets the value of the deliveryTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseDeliveryTerms }
     *     
     */
    public void setDeliveryTerms(SalesOrderByElementsResponseDeliveryTerms value) {
        this.deliveryTerms = value;
    }

    /**
     * Gets the value of the pricingTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePricingTerms }
     *     
     */
    public SalesOrderByElementsResponsePricingTerms getPricingTerms() {
        return pricingTerms;
    }

    /**
     * Sets the value of the pricingTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePricingTerms }
     *     
     */
    public void setPricingTerms(SalesOrderByElementsResponsePricingTerms value) {
        this.pricingTerms = value;
    }

    /**
     * Gets the value of the salesTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseSalesTerms }
     *     
     */
    public SalesOrderByElementsResponseSalesTerms getSalesTerms() {
        return salesTerms;
    }

    /**
     * Sets the value of the salesTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseSalesTerms }
     *     
     */
    public void setSalesTerms(SalesOrderByElementsResponseSalesTerms value) {
        this.salesTerms = value;
    }

    /**
     * Gets the value of the invoiceTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseInvoiceTerms }
     *     
     */
    public SalesOrderByElementsResponseInvoiceTerms getInvoiceTerms() {
        return invoiceTerms;
    }

    /**
     * Sets the value of the invoiceTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseInvoiceTerms }
     *     
     */
    public void setInvoiceTerms(SalesOrderByElementsResponseInvoiceTerms value) {
        this.invoiceTerms = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponseItem }
     * 
     * 
     */
    public List<SalesOrderByElementsResponseItem> getItem() {
        if (item == null) {
            item = new ArrayList<SalesOrderByElementsResponseItem>();
        }
        return this.item;
    }

    /**
     * Gets the value of the cashDiscountTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseCashDiscountTerms }
     *     
     */
    public SalesOrderByElementsResponseCashDiscountTerms getCashDiscountTerms() {
        return cashDiscountTerms;
    }

    /**
     * Sets the value of the cashDiscountTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseCashDiscountTerms }
     *     
     */
    public void setCashDiscountTerms(SalesOrderByElementsResponseCashDiscountTerms value) {
        this.cashDiscountTerms = value;
    }

    /**
     * Gets the value of the paymentControl property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePaymentControl }
     *     
     */
    public SalesOrderByElementsResponsePaymentControl getPaymentControl() {
        return paymentControl;
    }

    /**
     * Sets the value of the paymentControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePaymentControl }
     *     
     */
    public void setPaymentControl(SalesOrderByElementsResponsePaymentControl value) {
        this.paymentControl = value;
    }

    /**
     * Gets the value of the priceAndTaxCalculation property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculation }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculation getPriceAndTaxCalculation() {
        return priceAndTaxCalculation;
    }

    /**
     * Sets the value of the priceAndTaxCalculation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculation }
     *     
     */
    public void setPriceAndTaxCalculation(SalesOrderByElementsResponsePriceAndTaxCalculation value) {
        this.priceAndTaxCalculation = value;
    }

    /**
     * Gets the value of the textCollection property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseTextCollection }
     *     
     */
    public SalesOrderByElementsResponseTextCollection getTextCollection() {
        return textCollection;
    }

    /**
     * Sets the value of the textCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseTextCollection }
     *     
     */
    public void setTextCollection(SalesOrderByElementsResponseTextCollection value) {
        this.textCollection = value;
    }

    /**
     * Gets the value of the attachmentFolder property.
     * 
     * @return
     *     possible object is
     *     {@link AccessAttachmentFolder }
     *     
     */
    public AccessAttachmentFolder getAttachmentFolder() {
        return attachmentFolder;
    }

    /**
     * Sets the value of the attachmentFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessAttachmentFolder }
     *     
     */
    public void setAttachmentFolder(AccessAttachmentFolder value) {
        this.attachmentFolder = value;
    }

    /**
     * Gets the value of the croatiaBusinessSpaceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaBusinessSpaceID() {
        return croatiaBusinessSpaceID;
    }

    /**
     * Sets the value of the croatiaBusinessSpaceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaBusinessSpaceID(String value) {
        this.croatiaBusinessSpaceID = value;
    }

    /**
     * Gets the value of the croatiaBillingMachineID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaBillingMachineID() {
        return croatiaBillingMachineID;
    }

    /**
     * Sets the value of the croatiaBillingMachineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaBillingMachineID(String value) {
        this.croatiaBillingMachineID = value;
    }

    /**
     * Gets the value of the croatiaPaymentMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link CroatiaPaymentMethodCode }
     *     
     */
    public CroatiaPaymentMethodCode getCroatiaPaymentMethodCode() {
        return croatiaPaymentMethodCode;
    }

    /**
     * Sets the value of the croatiaPaymentMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CroatiaPaymentMethodCode }
     *     
     */
    public void setCroatiaPaymentMethodCode(CroatiaPaymentMethodCode value) {
        this.croatiaPaymentMethodCode = value;
    }

    /**
     * Gets the value of the croatiaPartyTaxID property.
     * 
     * @return
     *     possible object is
     *     {@link PartyTaxID }
     *     
     */
    public PartyTaxID getCroatiaPartyTaxID() {
        return croatiaPartyTaxID;
    }

    /**
     * Sets the value of the croatiaPartyTaxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyTaxID }
     *     
     */
    public void setCroatiaPartyTaxID(PartyTaxID value) {
        this.croatiaPartyTaxID = value;
    }

    /**
     * Gets the value of the croatiaParagonBillMark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaParagonBillMark() {
        return croatiaParagonBillMark;
    }

    /**
     * Sets the value of the croatiaParagonBillMark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaParagonBillMark(String value) {
        this.croatiaParagonBillMark = value;
    }

    /**
     * Gets the value of the croatiaSpecialNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaSpecialNote() {
        return croatiaSpecialNote;
    }

    /**
     * Sets the value of the croatiaSpecialNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaSpecialNote(String value) {
        this.croatiaSpecialNote = value;
    }

    /**
     * Gets the value of the servicioFLAM property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicioFLAM() {
        return servicioFLAM;
    }

    /**
     * Sets the value of the servicioFLAM property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicioFLAM(String value) {
        this.servicioFLAM = value;
    }

    /**
     * Gets the value of the consignatarios property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarios() {
        return consignatarios;
    }

    /**
     * Sets the value of the consignatarios property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarios(String value) {
        this.consignatarios = value;
    }

    /**
     * Gets the value of the ubicacinDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestino() {
        return ubicacinDestino;
    }

    /**
     * Sets the value of the ubicacinDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestino(String value) {
        this.ubicacinDestino = value;
    }

    /**
     * Gets the value of the ubicacionOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacionOrigen() {
        return ubicacionOrigen;
    }

    /**
     * Sets the value of the ubicacionOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacionOrigen(String value) {
        this.ubicacionOrigen = value;
    }

    /**
     * Gets the value of the ubicacinCruce property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCruce() {
        return ubicacinCruce;
    }

    /**
     * Sets the value of the ubicacinCruce property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCruce(String value) {
        this.ubicacinCruce = value;
    }

    /**
     * Gets the value of the fechaCarga1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCarga1() {
        return fechaCarga1;
    }

    /**
     * Sets the value of the fechaCarga1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCarga1(XMLGregorianCalendar value) {
        this.fechaCarga1 = value;
    }

    /**
     * Gets the value of the fechaCruce1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCruce1() {
        return fechaCruce1;
    }

    /**
     * Sets the value of the fechaCruce1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCruce1(XMLGregorianCalendar value) {
        this.fechaCruce1 = value;
    }

    /**
     * Gets the value of the fechaDestino1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestino1() {
        return fechaDestino1;
    }

    /**
     * Sets the value of the fechaDestino1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestino1(XMLGregorianCalendar value) {
        this.fechaDestino1 = value;
    }

    /**
     * Gets the value of the fechadeAgenteAduanal1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechadeAgenteAduanal1() {
        return fechadeAgenteAduanal1;
    }

    /**
     * Sets the value of the fechadeAgenteAduanal1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechadeAgenteAduanal1(XMLGregorianCalendar value) {
        this.fechadeAgenteAduanal1 = value;
    }

    /**
     * Gets the value of the guaHouse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHouse() {
        return guaHouse;
    }

    /**
     * Sets the value of the guaHouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHouse(String value) {
        this.guaHouse = value;
    }

    /**
     * Gets the value of the guaMaster property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMaster() {
        return guaMaster;
    }

    /**
     * Sets the value of the guaMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMaster(String value) {
        this.guaMaster = value;
    }

    /**
     * Gets the value of the nmerodeequipodecarga property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipodecarga() {
        return nmerodeequipodecarga;
    }

    /**
     * Sets the value of the nmerodeequipodecarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipodecarga(String value) {
        this.nmerodeequipodecarga = value;
    }

    /**
     * Gets the value of the pzsPBPC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPzsPBPC() {
        return pzsPBPC;
    }

    /**
     * Sets the value of the pzsPBPC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPzsPBPC(String value) {
        this.pzsPBPC = value;
    }

    /**
     * Gets the value of the shipper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipper() {
        return shipper;
    }

    /**
     * Sets the value of the shipper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipper(String value) {
        this.shipper = value;
    }

    /**
     * Gets the value of the tipodeequipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeequipo() {
        return tipodeequipo;
    }

    /**
     * Sets the value of the tipodeequipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeequipo(String value) {
        this.tipodeequipo = value;
    }

    /**
     * Gets the value of the requierePOD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequierePOD() {
        return requierePOD;
    }

    /**
     * Sets the value of the requierePOD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequierePOD(String value) {
        this.requierePOD = value;
    }

    /**
     * Gets the value of the ndeservicio1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNdeservicio1() {
        return ndeservicio1;
    }

    /**
     * Sets the value of the ndeservicio1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNdeservicio1(String value) {
        this.ndeservicio1 = value;
    }

    /**
     * Gets the value of the consignatarioDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioDestino() {
        return consignatarioDestino;
    }

    /**
     * Sets the value of the consignatarioDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioDestino(String value) {
        this.consignatarioDestino = value;
    }

    /**
     * Gets the value of the consignatarioCruce property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioCruce() {
        return consignatarioCruce;
    }

    /**
     * Sets the value of the consignatarioCruce property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioCruce(String value) {
        this.consignatarioCruce = value;
    }

}
