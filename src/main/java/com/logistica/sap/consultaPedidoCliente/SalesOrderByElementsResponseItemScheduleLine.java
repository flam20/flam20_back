
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponseItemScheduleLine complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseItemScheduleLine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemScheduleLineID" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemScheduleLineTypeCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="QuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="QuantityTypeName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="MeasureUnitName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="MeasureUnitCommonName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="DateTimePeriod" type="{http://sap.com/xi/AP/Common/GDT}UPPEROPEN_LOCALNORMALISED_DateTimePeriod" minOccurs="0"/>
 *         &lt;element name="ProductAvailabilityConfirmationCommitmentCode" type="{http://sap.com/xi/AP/Common/GDT}ProductAvailabilityConfirmationCommitmentCode" minOccurs="0"/>
 *         &lt;element name="RelatedID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemScheduleLineID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseItemScheduleLine", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "id",
    "typeCode",
    "quantity",
    "quantityTypeCode",
    "quantityTypeName",
    "measureUnitName",
    "measureUnitCommonName",
    "dateTimePeriod",
    "productAvailabilityConfirmationCommitmentCode",
    "relatedID"
})
public class SalesOrderByElementsResponseItemScheduleLine {

    @XmlElement(name = "ID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;
    @XmlElement(name = "TypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String typeCode;
    @XmlElement(name = "Quantity")
    protected Quantity quantity;
    @XmlElement(name = "QuantityTypeCode")
    protected QuantityTypeCode quantityTypeCode;
    @XmlElement(name = "QuantityTypeName")
    protected Name quantityTypeName;
    @XmlElement(name = "MeasureUnitName")
    protected Name measureUnitName;
    @XmlElement(name = "MeasureUnitCommonName")
    protected Name measureUnitCommonName;
    @XmlElement(name = "DateTimePeriod")
    protected UPPEROPENLOCALNORMALISEDDateTimePeriod dateTimePeriod;
    @XmlElement(name = "ProductAvailabilityConfirmationCommitmentCode")
    protected ProductAvailabilityConfirmationCommitmentCode productAvailabilityConfirmationCommitmentCode;
    @XmlElement(name = "RelatedID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String relatedID;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCode(String value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setQuantity(Quantity value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the quantityTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getQuantityTypeCode() {
        return quantityTypeCode;
    }

    /**
     * Sets the value of the quantityTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setQuantityTypeCode(QuantityTypeCode value) {
        this.quantityTypeCode = value;
    }

    /**
     * Gets the value of the quantityTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getQuantityTypeName() {
        return quantityTypeName;
    }

    /**
     * Sets the value of the quantityTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setQuantityTypeName(Name value) {
        this.quantityTypeName = value;
    }

    /**
     * Gets the value of the measureUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getMeasureUnitName() {
        return measureUnitName;
    }

    /**
     * Sets the value of the measureUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setMeasureUnitName(Name value) {
        this.measureUnitName = value;
    }

    /**
     * Gets the value of the measureUnitCommonName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getMeasureUnitCommonName() {
        return measureUnitCommonName;
    }

    /**
     * Sets the value of the measureUnitCommonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setMeasureUnitCommonName(Name value) {
        this.measureUnitCommonName = value;
    }

    /**
     * Gets the value of the dateTimePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod getDateTimePeriod() {
        return dateTimePeriod;
    }

    /**
     * Sets the value of the dateTimePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public void setDateTimePeriod(UPPEROPENLOCALNORMALISEDDateTimePeriod value) {
        this.dateTimePeriod = value;
    }

    /**
     * Gets the value of the productAvailabilityConfirmationCommitmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link ProductAvailabilityConfirmationCommitmentCode }
     *     
     */
    public ProductAvailabilityConfirmationCommitmentCode getProductAvailabilityConfirmationCommitmentCode() {
        return productAvailabilityConfirmationCommitmentCode;
    }

    /**
     * Sets the value of the productAvailabilityConfirmationCommitmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductAvailabilityConfirmationCommitmentCode }
     *     
     */
    public void setProductAvailabilityConfirmationCommitmentCode(ProductAvailabilityConfirmationCommitmentCode value) {
        this.productAvailabilityConfirmationCommitmentCode = value;
    }

    /**
     * Gets the value of the relatedID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedID() {
        return relatedID;
    }

    /**
     * Sets the value of the relatedID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedID(String value) {
        this.relatedID = value;
    }

}
