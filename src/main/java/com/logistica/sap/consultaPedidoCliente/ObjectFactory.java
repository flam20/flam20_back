
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.logistica.integracionsap.ws.consultaPedidoCliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SalesOrderByElementsResponseSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "SalesOrderByElementsResponse_sync");
    private final static QName _SalesOrderByElementsQuerySync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "SalesOrderByElementsQuery_sync");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.logistica.integracionsap.ws.consultaPedidoCliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CroatiaPaymentMethodCode }
     * 
     */
    public CroatiaPaymentMethodCode createCroatiaPaymentMethodCode() {
        return new CroatiaPaymentMethodCode();
    }

    /**
     * Create an instance of {@link ExtSelectionByDecimal }
     * 
     */
    public ExtSelectionByDecimal createExtSelectionByDecimal() {
        return new ExtSelectionByDecimal();
    }

    /**
     * Create an instance of {@link ExtSelectionByShortText }
     * 
     */
    public ExtSelectionByShortText createExtSelectionByShortText() {
        return new ExtSelectionByShortText();
    }

    /**
     * Create an instance of {@link ExtSelectionByLongText }
     * 
     */
    public ExtSelectionByLongText createExtSelectionByLongText() {
        return new ExtSelectionByLongText();
    }

    /**
     * Create an instance of {@link ExtSelectionByDate }
     * 
     */
    public ExtSelectionByDate createExtSelectionByDate() {
        return new ExtSelectionByDate();
    }

    /**
     * Create an instance of {@link ExtSelectionByIdentifier }
     * 
     */
    public ExtSelectionByIdentifier createExtSelectionByIdentifier() {
        return new ExtSelectionByIdentifier();
    }

    /**
     * Create an instance of {@link ExtSelectionByAmount }
     * 
     */
    public ExtSelectionByAmount createExtSelectionByAmount() {
        return new ExtSelectionByAmount();
    }

    /**
     * Create an instance of {@link ExtSelectionByIndicator }
     * 
     */
    public ExtSelectionByIndicator createExtSelectionByIndicator() {
        return new ExtSelectionByIndicator();
    }

    /**
     * Create an instance of {@link ExtSelectionByText }
     * 
     */
    public ExtSelectionByText createExtSelectionByText() {
        return new ExtSelectionByText();
    }

    /**
     * Create an instance of {@link WithholdingTaxIncomeTypeCode }
     * 
     */
    public WithholdingTaxIncomeTypeCode createWithholdingTaxIncomeTypeCode() {
        return new WithholdingTaxIncomeTypeCode();
    }

    /**
     * Create an instance of {@link TaxTypeCode }
     * 
     */
    public TaxTypeCode createTaxTypeCode() {
        return new TaxTypeCode();
    }

    /**
     * Create an instance of {@link WithholdingTax }
     * 
     */
    public WithholdingTax createWithholdingTax() {
        return new WithholdingTax();
    }

    /**
     * Create an instance of {@link ProductTax }
     * 
     */
    public ProductTax createProductTax() {
        return new ProductTax();
    }

    /**
     * Create an instance of {@link DatePeriod }
     * 
     */
    public DatePeriod createDatePeriod() {
        return new DatePeriod();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentID }
     * 
     */
    public BusinessTransactionDocumentID createBusinessTransactionDocumentID() {
        return new BusinessTransactionDocumentID();
    }

    /**
     * Create an instance of {@link PhoneNumber }
     * 
     */
    public PhoneNumber createPhoneNumber() {
        return new PhoneNumber();
    }

    /**
     * Create an instance of {@link EXTENDEDName }
     * 
     */
    public EXTENDEDName createEXTENDEDName() {
        return new EXTENDEDName();
    }

    /**
     * Create an instance of {@link PartyTaxID }
     * 
     */
    public PartyTaxID createPartyTaxID() {
        return new PartyTaxID();
    }

    /**
     * Create an instance of {@link ProductInternalID }
     * 
     */
    public ProductInternalID createProductInternalID() {
        return new ProductInternalID();
    }

    /**
     * Create an instance of {@link AddressRepresentationCode }
     * 
     */
    public AddressRepresentationCode createAddressRepresentationCode() {
        return new AddressRepresentationCode();
    }

    /**
     * Create an instance of {@link PaymentCardHolderAuthenticationID }
     * 
     */
    public PaymentCardHolderAuthenticationID createPaymentCardHolderAuthenticationID() {
        return new PaymentCardHolderAuthenticationID();
    }

    /**
     * Create an instance of {@link DeviceID }
     * 
     */
    public DeviceID createDeviceID() {
        return new DeviceID();
    }

    /**
     * Create an instance of {@link SHORTDescription }
     * 
     */
    public SHORTDescription createSHORTDescription() {
        return new SHORTDescription();
    }

    /**
     * Create an instance of {@link PaymentCardVerificationValueAvailabilityCode }
     * 
     */
    public PaymentCardVerificationValueAvailabilityCode createPaymentCardVerificationValueAvailabilityCode() {
        return new PaymentCardVerificationValueAvailabilityCode();
    }

    /**
     * Create an instance of {@link ProjectElementID }
     * 
     */
    public ProjectElementID createProjectElementID() {
        return new ProjectElementID();
    }

    /**
     * Create an instance of {@link ProductTaxEventTypeCode }
     * 
     */
    public ProductTaxEventTypeCode createProductTaxEventTypeCode() {
        return new ProductTaxEventTypeCode();
    }

    /**
     * Create an instance of {@link TaxRateTypeCode }
     * 
     */
    public TaxRateTypeCode createTaxRateTypeCode() {
        return new TaxRateTypeCode();
    }

    /**
     * Create an instance of {@link ResourceID }
     * 
     */
    public ResourceID createResourceID() {
        return new ResourceID();
    }

    /**
     * Create an instance of {@link PriceSpecificationElementCategoryCode }
     * 
     */
    public PriceSpecificationElementCategoryCode createPriceSpecificationElementCategoryCode() {
        return new PriceSpecificationElementCategoryCode();
    }

    /**
     * Create an instance of {@link ObjectID }
     * 
     */
    public ObjectID createObjectID() {
        return new ObjectID();
    }

    /**
     * Create an instance of {@link TextCollectionTextTypeCode }
     * 
     */
    public TextCollectionTextTypeCode createTextCollectionTextTypeCode() {
        return new TextCollectionTextTypeCode();
    }

    /**
     * Create an instance of {@link LogItem }
     * 
     */
    public LogItem createLogItem() {
        return new LogItem();
    }

    /**
     * Create an instance of {@link UUID }
     * 
     */
    public UUID createUUID() {
        return new UUID();
    }

    /**
     * Create an instance of {@link CancellationReasonCode }
     * 
     */
    public CancellationReasonCode createCancellationReasonCode() {
        return new CancellationReasonCode();
    }

    /**
     * Create an instance of {@link ServiceWorkingConditionsCode }
     * 
     */
    public ServiceWorkingConditionsCode createServiceWorkingConditionsCode() {
        return new ServiceWorkingConditionsCode();
    }

    /**
     * Create an instance of {@link CustomerTransactionDocumentServiceConfirmationCreationCode }
     * 
     */
    public CustomerTransactionDocumentServiceConfirmationCreationCode createCustomerTransactionDocumentServiceConfirmationCreationCode() {
        return new CustomerTransactionDocumentServiceConfirmationCreationCode();
    }

    /**
     * Create an instance of {@link ClearingHouseAccountID }
     * 
     */
    public ClearingHouseAccountID createClearingHouseAccountID() {
        return new ClearingHouseAccountID();
    }

    /**
     * Create an instance of {@link TaxExemption }
     * 
     */
    public TaxExemption createTaxExemption() {
        return new TaxExemption();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link PaymentBlockingReasonCode }
     * 
     */
    public PaymentBlockingReasonCode createPaymentBlockingReasonCode() {
        return new PaymentBlockingReasonCode();
    }

    /**
     * Create an instance of {@link TaxJurisdictionCode }
     * 
     */
    public TaxJurisdictionCode createTaxJurisdictionCode() {
        return new TaxJurisdictionCode();
    }

    /**
     * Create an instance of {@link ExchangeRate }
     * 
     */
    public ExchangeRate createExchangeRate() {
        return new ExchangeRate();
    }

    /**
     * Create an instance of {@link PriceSpecificationElementTypeCode }
     * 
     */
    public PriceSpecificationElementTypeCode createPriceSpecificationElementTypeCode() {
        return new PriceSpecificationElementTypeCode();
    }

    /**
     * Create an instance of {@link Incoterms }
     * 
     */
    public Incoterms createIncoterms() {
        return new Incoterms();
    }

    /**
     * Create an instance of {@link MEDIUMName }
     * 
     */
    public MEDIUMName createMEDIUMName() {
        return new MEDIUMName();
    }

    /**
     * Create an instance of {@link QueryProcessingConditions }
     * 
     */
    public QueryProcessingConditions createQueryProcessingConditions() {
        return new QueryProcessingConditions();
    }

    /**
     * Create an instance of {@link NOCONVERSIONProductID }
     * 
     */
    public NOCONVERSIONProductID createNOCONVERSIONProductID() {
        return new NOCONVERSIONProductID();
    }

    /**
     * Create an instance of {@link PaymentCardHolderAuthenticationResultCode }
     * 
     */
    public PaymentCardHolderAuthenticationResultCode createPaymentCardHolderAuthenticationResultCode() {
        return new PaymentCardHolderAuthenticationResultCode();
    }

    /**
     * Create an instance of {@link ProductStandardID }
     * 
     */
    public ProductStandardID createProductStandardID() {
        return new ProductStandardID();
    }

    /**
     * Create an instance of {@link Rate }
     * 
     */
    public Rate createRate() {
        return new Rate();
    }

    /**
     * Create an instance of {@link PaymentCardID }
     * 
     */
    public PaymentCardID createPaymentCardID() {
        return new PaymentCardID();
    }

    /**
     * Create an instance of {@link Description }
     * 
     */
    public Description createDescription() {
        return new Description();
    }

    /**
     * Create an instance of {@link MEDIUMNote }
     * 
     */
    public MEDIUMNote createMEDIUMNote() {
        return new MEDIUMNote();
    }

    /**
     * Create an instance of {@link PaymentBlock }
     * 
     */
    public PaymentBlock createPaymentBlock() {
        return new PaymentBlock();
    }

    /**
     * Create an instance of {@link TaxExemptionReasonCode }
     * 
     */
    public TaxExemptionReasonCode createTaxExemptionReasonCode() {
        return new TaxExemptionReasonCode();
    }

    /**
     * Create an instance of {@link RegionCode }
     * 
     */
    public RegionCode createRegionCode() {
        return new RegionCode();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentTypeCode }
     * 
     */
    public BusinessTransactionDocumentTypeCode createBusinessTransactionDocumentTypeCode() {
        return new BusinessTransactionDocumentTypeCode();
    }

    /**
     * Create an instance of {@link OrganisationName }
     * 
     */
    public OrganisationName createOrganisationName() {
        return new OrganisationName();
    }

    /**
     * Create an instance of {@link WithholdingTaxationCharacteristicsCode }
     * 
     */
    public WithholdingTaxationCharacteristicsCode createWithholdingTaxationCharacteristicsCode() {
        return new WithholdingTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link DocumentTypeCode }
     * 
     */
    public DocumentTypeCode createDocumentTypeCode() {
        return new DocumentTypeCode();
    }

    /**
     * Create an instance of {@link DistributionChannelCode }
     * 
     */
    public DistributionChannelCode createDistributionChannelCode() {
        return new DistributionChannelCode();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentReference }
     * 
     */
    public BusinessTransactionDocumentReference createBusinessTransactionDocumentReference() {
        return new BusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link LocationInternalID }
     * 
     */
    public LocationInternalID createLocationInternalID() {
        return new LocationInternalID();
    }

    /**
     * Create an instance of {@link WithholdingTaxEventTypeCode }
     * 
     */
    public WithholdingTaxEventTypeCode createWithholdingTaxEventTypeCode() {
        return new WithholdingTaxEventTypeCode();
    }

    /**
     * Create an instance of {@link PartyID }
     * 
     */
    public PartyID createPartyID() {
        return new PartyID();
    }

    /**
     * Create an instance of {@link PaymentCardPaymentAuthorisationPartyIDV1 }
     * 
     */
    public PaymentCardPaymentAuthorisationPartyIDV1 createPaymentCardPaymentAuthorisationPartyIDV1() {
        return new PaymentCardPaymentAuthorisationPartyIDV1();
    }

    /**
     * Create an instance of {@link LogItemCategoryCode }
     * 
     */
    public LogItemCategoryCode createLogItemCategoryCode() {
        return new LogItemCategoryCode();
    }

    /**
     * Create an instance of {@link CLOSEDDatePeriod }
     * 
     */
    public CLOSEDDatePeriod createCLOSEDDatePeriod() {
        return new CLOSEDDatePeriod();
    }

    /**
     * Create an instance of {@link QuantityTypeCode }
     * 
     */
    public QuantityTypeCode createQuantityTypeCode() {
        return new QuantityTypeCode();
    }

    /**
     * Create an instance of {@link FulfilmentPartyCategoryCode }
     * 
     */
    public FulfilmentPartyCategoryCode createFulfilmentPartyCategoryCode() {
        return new FulfilmentPartyCategoryCode();
    }

    /**
     * Create an instance of {@link LocationID }
     * 
     */
    public LocationID createLocationID() {
        return new LocationID();
    }

    /**
     * Create an instance of {@link TaxIdentificationNumberTypeCode }
     * 
     */
    public TaxIdentificationNumberTypeCode createTaxIdentificationNumberTypeCode() {
        return new TaxIdentificationNumberTypeCode();
    }

    /**
     * Create an instance of {@link URI }
     * 
     */
    public URI createURI() {
        return new URI();
    }

    /**
     * Create an instance of {@link LogItemNotePlaceholderSubstitutionList }
     * 
     */
    public LogItemNotePlaceholderSubstitutionList createLogItemNotePlaceholderSubstitutionList() {
        return new LogItemNotePlaceholderSubstitutionList();
    }

    /**
     * Create an instance of {@link BankAccountInternalID }
     * 
     */
    public BankAccountInternalID createBankAccountInternalID() {
        return new BankAccountInternalID();
    }

    /**
     * Create an instance of {@link DataOriginTypeCode }
     * 
     */
    public DataOriginTypeCode createDataOriginTypeCode() {
        return new DataOriginTypeCode();
    }

    /**
     * Create an instance of {@link Measure }
     * 
     */
    public Measure createMeasure() {
        return new Measure();
    }

    /**
     * Create an instance of {@link CashDiscountTermsCode }
     * 
     */
    public CashDiscountTermsCode createCashDiscountTermsCode() {
        return new CashDiscountTermsCode();
    }

    /**
     * Create an instance of {@link ProductTaxationCharacteristicsCode }
     * 
     */
    public ProductTaxationCharacteristicsCode createProductTaxationCharacteristicsCode() {
        return new ProductTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link NamespaceURI }
     * 
     */
    public NamespaceURI createNamespaceURI() {
        return new NamespaceURI();
    }

    /**
     * Create an instance of {@link RequirementSpecificationID }
     * 
     */
    public RequirementSpecificationID createRequirementSpecificationID() {
        return new RequirementSpecificationID();
    }

    /**
     * Create an instance of {@link TaxExemptionCertificateID }
     * 
     */
    public TaxExemptionCertificateID createTaxExemptionCertificateID() {
        return new TaxExemptionCertificateID();
    }

    /**
     * Create an instance of {@link ProductTaxStandardClassificationSystemCode }
     * 
     */
    public ProductTaxStandardClassificationSystemCode createProductTaxStandardClassificationSystemCode() {
        return new ProductTaxStandardClassificationSystemCode();
    }

    /**
     * Create an instance of {@link ResponseProcessingConditions }
     * 
     */
    public ResponseProcessingConditions createResponseProcessingConditions() {
        return new ResponseProcessingConditions();
    }

    /**
     * Create an instance of {@link LONGName }
     * 
     */
    public LONGName createLONGName() {
        return new LONGName();
    }

    /**
     * Create an instance of {@link PaymentCardTypeCode }
     * 
     */
    public PaymentCardTypeCode createPaymentCardTypeCode() {
        return new PaymentCardTypeCode();
    }

    /**
     * Create an instance of {@link ProductTaxStandardClassificationCode }
     * 
     */
    public ProductTaxStandardClassificationCode createProductTaxStandardClassificationCode() {
        return new ProductTaxStandardClassificationCode();
    }

    /**
     * Create an instance of {@link IndustrialSectorCode }
     * 
     */
    public IndustrialSectorCode createIndustrialSectorCode() {
        return new IndustrialSectorCode();
    }

    /**
     * Create an instance of {@link ProductID }
     * 
     */
    public ProductID createProductID() {
        return new ProductID();
    }

    /**
     * Create an instance of {@link LOCALNORMALISEDDateTime }
     * 
     */
    public LOCALNORMALISEDDateTime createLOCALNORMALISEDDateTime() {
        return new LOCALNORMALISEDDateTime();
    }

    /**
     * Create an instance of {@link Log }
     * 
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link SystemAdministrativeData }
     * 
     */
    public SystemAdministrativeData createSystemAdministrativeData() {
        return new SystemAdministrativeData();
    }

    /**
     * Create an instance of {@link PriceSpecificationElementPurposeCode }
     * 
     */
    public PriceSpecificationElementPurposeCode createPriceSpecificationElementPurposeCode() {
        return new PriceSpecificationElementPurposeCode();
    }

    /**
     * Create an instance of {@link Quantity }
     * 
     */
    public Quantity createQuantity() {
        return new Quantity();
    }

    /**
     * Create an instance of {@link PriceComponentCalculationBasis }
     * 
     */
    public PriceComponentCalculationBasis createPriceComponentCalculationBasis() {
        return new PriceComponentCalculationBasis();
    }

    /**
     * Create an instance of {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     * 
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod createUPPEROPENLOCALNORMALISEDDateTimePeriod() {
        return new UPPEROPENLOCALNORMALISEDDateTimePeriod();
    }

    /**
     * Create an instance of {@link TaxJurisdictionSubdivisionCode }
     * 
     */
    public TaxJurisdictionSubdivisionCode createTaxJurisdictionSubdivisionCode() {
        return new TaxJurisdictionSubdivisionCode();
    }

    /**
     * Create an instance of {@link FormOfAddressCode }
     * 
     */
    public FormOfAddressCode createFormOfAddressCode() {
        return new FormOfAddressCode();
    }

    /**
     * Create an instance of {@link EmailURI }
     * 
     */
    public EmailURI createEmailURI() {
        return new EmailURI();
    }

    /**
     * Create an instance of {@link Name }
     * 
     */
    public Name createName() {
        return new Name();
    }

    /**
     * Create an instance of {@link TaxDeductibilityCode }
     * 
     */
    public TaxDeductibilityCode createTaxDeductibilityCode() {
        return new TaxDeductibilityCode();
    }

    /**
     * Create an instance of {@link TaxJurisdictionSubdivisionTypeCode }
     * 
     */
    public TaxJurisdictionSubdivisionTypeCode createTaxJurisdictionSubdivisionTypeCode() {
        return new TaxJurisdictionSubdivisionTypeCode();
    }

    /**
     * Create an instance of {@link ProductAvailabilityConfirmationCommitmentCode }
     * 
     */
    public ProductAvailabilityConfirmationCommitmentCode createProductAvailabilityConfirmationCommitmentCode() {
        return new ProductAvailabilityConfirmationCommitmentCode();
    }

    /**
     * Create an instance of {@link LOCALNORMALISEDDateTime2 }
     * 
     */
    public LOCALNORMALISEDDateTime2 createLOCALNORMALISEDDateTime2() {
        return new LOCALNORMALISEDDateTime2();
    }

    /**
     * Create an instance of {@link Code }
     * 
     */
    public Code createCode() {
        return new Code();
    }

    /**
     * Create an instance of {@link PriceAndTaxCalculationItemStatus }
     * 
     */
    public PriceAndTaxCalculationItemStatus createPriceAndTaxCalculationItemStatus() {
        return new PriceAndTaxCalculationItemStatus();
    }

    /**
     * Create an instance of {@link PaymentCardKey }
     * 
     */
    public PaymentCardKey createPaymentCardKey() {
        return new PaymentCardKey();
    }

    /**
     * Create an instance of {@link AccessAttachmentFolderDocument }
     * 
     */
    public AccessAttachmentFolderDocument createAccessAttachmentFolderDocument() {
        return new AccessAttachmentFolderDocument();
    }

    /**
     * Create an instance of {@link AccessAttachmentFolderDocumentProperty }
     * 
     */
    public AccessAttachmentFolderDocumentProperty createAccessAttachmentFolderDocumentProperty() {
        return new AccessAttachmentFolderDocumentProperty();
    }

    /**
     * Create an instance of {@link AccessAttachmentFolder }
     * 
     */
    public AccessAttachmentFolder createAccessAttachmentFolder() {
        return new AccessAttachmentFolder();
    }

    /**
     * Create an instance of {@link AccessAttachmentFolderDocumentPropertyPropertyValue }
     * 
     */
    public AccessAttachmentFolderDocumentPropertyPropertyValue createAccessAttachmentFolderDocumentPropertyPropertyValue() {
        return new AccessAttachmentFolderDocumentPropertyPropertyValue();
    }

    /**
     * Create an instance of {@link SalesOrderPartyStandardIdentifier }
     * 
     */
    public SalesOrderPartyStandardIdentifier createSalesOrderPartyStandardIdentifier() {
        return new SalesOrderPartyStandardIdentifier();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseParty }
     * 
     */
    public SalesOrderByElementsResponseParty createSalesOrderByElementsResponseParty() {
        return new SalesOrderByElementsResponseParty();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent createSalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByPartyID }
     * 
     */
    public SalesOrderByElementsQuerySelectionByPartyID createSalesOrderByElementsQuerySelectionByPartyID() {
        return new SalesOrderByElementsQuerySelectionByPartyID();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent createSalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails createSalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms }
     * 
     */
    public SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms createSalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms() {
        return new SalesOrderByElementsResponseRequestedFulfillmentPeriodPeriodTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQueryMessageSync }
     * 
     */
    public SalesOrderByElementsQueryMessageSync createSalesOrderByElementsQueryMessageSync() {
        return new SalesOrderByElementsQueryMessageSync();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePaymentControl }
     * 
     */
    public SalesOrderByElementsResponsePaymentControl createSalesOrderByElementsResponsePaymentControl() {
        return new SalesOrderByElementsResponsePaymentControl();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByDataOriginTypeCode }
     * 
     */
    public SalesOrderByElementsQuerySelectionByDataOriginTypeCode createSalesOrderByElementsQuerySelectionByDataOriginTypeCode() {
        return new SalesOrderByElementsQuerySelectionByDataOriginTypeCode();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressPostalAddress }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressPostalAddress createSalesOrderMaintainRequestPartyAddressPostalAddress() {
        return new SalesOrderMaintainRequestPartyAddressPostalAddress();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationItem }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItem createSalesOrderByElementsResponsePriceAndTaxCalculationItem() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationItem();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemProduct }
     * 
     */
    public SalesOrderByElementsResponseItemProduct createSalesOrderByElementsResponseItemProduct() {
        return new SalesOrderByElementsResponseItemProduct();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails createSalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePricingTerms }
     * 
     */
    public SalesOrderByElementsResponsePricingTerms createSalesOrderByElementsResponsePricingTerms() {
        return new SalesOrderByElementsResponsePricingTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms createSalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponse }
     * 
     */
    public SalesOrderByElementsResponse createSalesOrderByElementsResponse() {
        return new SalesOrderByElementsResponse();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressEmail }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressEmail createSalesOrderMaintainRequestPartyAddressEmail() {
        return new SalesOrderMaintainRequestPartyAddressEmail();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms createSalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseSalesTerms }
     * 
     */
    public SalesOrderByElementsResponseSalesTerms createSalesOrderByElementsResponseSalesTerms() {
        return new SalesOrderByElementsResponseSalesTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemLocation }
     * 
     */
    public SalesOrderByElementsResponseItemLocation createSalesOrderByElementsResponseItemLocation() {
        return new SalesOrderByElementsResponseItemLocation();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemServiceTerms }
     * 
     */
    public SalesOrderByElementsResponseItemServiceTerms createSalesOrderByElementsResponseItemServiceTerms() {
        return new SalesOrderByElementsResponseItemServiceTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByReleaseStatusCode }
     * 
     */
    public SalesOrderByElementsQuerySelectionByReleaseStatusCode createSalesOrderByElementsQuerySelectionByReleaseStatusCode() {
        return new SalesOrderByElementsQuerySelectionByReleaseStatusCode();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePartyContactParty }
     * 
     */
    public SalesOrderByElementsResponsePartyContactParty createSalesOrderByElementsResponsePartyContactParty() {
        return new SalesOrderByElementsResponsePartyContactParty();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseSalesAndServiceBusinessArea }
     * 
     */
    public SalesOrderByElementsResponseSalesAndServiceBusinessArea createSalesOrderByElementsResponseSalesAndServiceBusinessArea() {
        return new SalesOrderByElementsResponseSalesAndServiceBusinessArea();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID }
     * 
     */
    public SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID createSalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID() {
        return new SalesOrderByElementsQuerySelectionByProductRequirementSpecificationKeyRequirementSpecificationID();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePaymentControlCreditCardPayment }
     * 
     */
    public SalesOrderByElementsResponsePaymentControlCreditCardPayment createSalesOrderByElementsResponsePaymentControlCreditCardPayment() {
        return new SalesOrderByElementsResponsePaymentControlCreditCardPayment();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressFascmile }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressFascmile createSalesOrderMaintainRequestPartyAddressFascmile() {
        return new SalesOrderMaintainRequestPartyAddressFascmile();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseInvoiceTerms }
     * 
     */
    public SalesOrderByElementsResponseInvoiceTerms createSalesOrderByElementsResponseInvoiceTerms() {
        return new SalesOrderByElementsResponseInvoiceTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseDeliveryTerms }
     * 
     */
    public SalesOrderByElementsResponseDeliveryTerms createSalesOrderByElementsResponseDeliveryTerms() {
        return new SalesOrderByElementsResponseDeliveryTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemDeliveryTerms }
     * 
     */
    public SalesOrderByElementsResponseItemDeliveryTerms createSalesOrderByElementsResponseItemDeliveryTerms() {
        return new SalesOrderByElementsResponseItemDeliveryTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQueryRequestedElements }
     * 
     */
    public SalesOrderByElementsQueryRequestedElements createSalesOrderByElementsQueryRequestedElements() {
        return new SalesOrderByElementsQueryRequestedElements();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressDisplayName }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressDisplayName createSalesOrderMaintainRequestPartyAddressDisplayName() {
        return new SalesOrderMaintainRequestPartyAddressDisplayName();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByDistributionChannelCode }
     * 
     */
    public SalesOrderByElementsQuerySelectionByDistributionChannelCode createSalesOrderByElementsQuerySelectionByDistributionChannelCode() {
        return new SalesOrderByElementsQuerySelectionByDistributionChannelCode();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails createSalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressWeb }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressWeb createSalesOrderMaintainRequestPartyAddressWeb() {
        return new SalesOrderMaintainRequestPartyAddressWeb();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByApprovalStatusCode }
     * 
     */
    public SalesOrderByElementsQuerySelectionByApprovalStatusCode createSalesOrderByElementsQuerySelectionByApprovalStatusCode() {
        return new SalesOrderByElementsQuerySelectionByApprovalStatusCode();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculation }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculation createSalesOrderByElementsResponsePriceAndTaxCalculation() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculation();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemStatus }
     * 
     */
    public SalesOrderByElementsResponseItemStatus createSalesOrderByElementsResponseItemStatus() {
        return new SalesOrderByElementsResponseItemStatus();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails createSalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseCashDiscountTerms }
     * 
     */
    public SalesOrderByElementsResponseCashDiscountTerms createSalesOrderByElementsResponseCashDiscountTerms() {
        return new SalesOrderByElementsResponseCashDiscountTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemSalesTerms }
     * 
     */
    public SalesOrderByElementsResponseItemSalesTerms createSalesOrderByElementsResponseItemSalesTerms() {
        return new SalesOrderByElementsResponseItemSalesTerms();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent createSalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddress }
     * 
     */
    public SalesOrderMaintainRequestPartyAddress createSalesOrderMaintainRequestPartyAddress() {
        return new SalesOrderMaintainRequestPartyAddress();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByProductID }
     * 
     */
    public SalesOrderByElementsQuerySelectionByProductID createSalesOrderByElementsQuerySelectionByProductID() {
        return new SalesOrderByElementsQuerySelectionByProductID();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePaymentControlCreditCardPaymentCreditCardPaymentAuthorisation }
     * 
     */
    public SalesOrderByElementsResponsePaymentControlCreditCardPaymentCreditCardPaymentAuthorisation createSalesOrderByElementsResponsePaymentControlCreditCardPaymentCreditCardPaymentAuthorisation() {
        return new SalesOrderByElementsResponsePaymentControlCreditCardPaymentCreditCardPaymentAuthorisation();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseMessageSync }
     * 
     */
    public SalesOrderByElementsResponseMessageSync createSalesOrderByElementsResponseMessageSync() {
        return new SalesOrderByElementsResponseMessageSync();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressName }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressName createSalesOrderMaintainRequestPartyAddressName() {
        return new SalesOrderMaintainRequestPartyAddressName();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItem }
     * 
     */
    public SalesOrderByElementsResponseItem createSalesOrderByElementsResponseItem() {
        return new SalesOrderByElementsResponseItem();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseStatus }
     * 
     */
    public SalesOrderByElementsResponseStatus createSalesOrderByElementsResponseStatus() {
        return new SalesOrderByElementsResponseStatus();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByID }
     * 
     */
    public SalesOrderByElementsQuerySelectionByID createSalesOrderByElementsQuerySelectionByID() {
        return new SalesOrderByElementsQuerySelectionByID();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByDateTime }
     * 
     */
    public SalesOrderByElementsQuerySelectionByDateTime createSalesOrderByElementsQuerySelectionByDateTime() {
        return new SalesOrderByElementsQuerySelectionByDateTime();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByElements }
     * 
     */
    public SalesOrderByElementsQuerySelectionByElements createSalesOrderByElementsQuerySelectionByElements() {
        return new SalesOrderByElementsQuerySelectionByElements();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseBusinessTransactionDocumentReference }
     * 
     */
    public SalesOrderByElementsResponseBusinessTransactionDocumentReference createSalesOrderByElementsResponseBusinessTransactionDocumentReference() {
        return new SalesOrderByElementsResponseBusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQueryRequestedElementsSalesOrder }
     * 
     */
    public SalesOrderByElementsQueryRequestedElementsSalesOrder createSalesOrderByElementsQueryRequestedElementsSalesOrder() {
        return new SalesOrderByElementsQueryRequestedElementsSalesOrder();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressTelephone }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressTelephone createSalesOrderMaintainRequestPartyAddressTelephone() {
        return new SalesOrderMaintainRequestPartyAddressTelephone();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode }
     * 
     */
    public SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode createSalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode() {
        return new SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePaymentControlExternalPayment }
     * 
     */
    public SalesOrderByElementsResponsePaymentControlExternalPayment createSalesOrderByElementsResponsePaymentControlExternalPayment() {
        return new SalesOrderByElementsResponsePaymentControlExternalPayment();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent }
     * 
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent createSalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent() {
        return new SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByOrganisationalCentreID }
     * 
     */
    public SalesOrderByElementsQuerySelectionByOrganisationalCentreID createSalesOrderByElementsQuerySelectionByOrganisationalCentreID() {
        return new SalesOrderByElementsQuerySelectionByOrganisationalCentreID();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemScheduleLine }
     * 
     */
    public SalesOrderByElementsResponseItemScheduleLine createSalesOrderByElementsResponseItemScheduleLine() {
        return new SalesOrderByElementsResponseItemScheduleLine();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseTextCollectionText }
     * 
     */
    public SalesOrderByElementsResponseTextCollectionText createSalesOrderByElementsResponseTextCollectionText() {
        return new SalesOrderByElementsResponseTextCollectionText();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseTextCollection }
     * 
     */
    public SalesOrderByElementsResponseTextCollection createSalesOrderByElementsResponseTextCollection() {
        return new SalesOrderByElementsResponseTextCollection();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsResponseItemAccountingCodingBlockDistribution }
     * 
     */
    public SalesOrderByElementsResponseItemAccountingCodingBlockDistribution createSalesOrderByElementsResponseItemAccountingCodingBlockDistribution() {
        return new SalesOrderByElementsResponseItemAccountingCodingBlockDistribution();
    }

    /**
     * Create an instance of {@link SalesOrderByElementsQuerySelectionByName }
     * 
     */
    public SalesOrderByElementsQuerySelectionByName createSalesOrderByElementsQuerySelectionByName() {
        return new SalesOrderByElementsQuerySelectionByName();
    }

    /**
     * Create an instance of {@link StandardFaultMessage }
     * 
     */
    public StandardFaultMessage createStandardFaultMessage() {
        return new StandardFaultMessage();
    }

    /**
     * Create an instance of {@link ExchangeFaultData }
     * 
     */
    public ExchangeFaultData createExchangeFaultData() {
        return new ExchangeFaultData();
    }

    /**
     * Create an instance of {@link StandardFaultMessageExtension }
     * 
     */
    public StandardFaultMessageExtension createStandardFaultMessageExtension() {
        return new StandardFaultMessageExtension();
    }

    /**
     * Create an instance of {@link SelectionByValue }
     * 
     */
    public SelectionByValue createSelectionByValue() {
        return new SelectionByValue();
    }

    /**
     * Create an instance of {@link SelectionByIndicator }
     * 
     */
    public SelectionByIndicator createSelectionByIndicator() {
        return new SelectionByIndicator();
    }

    /**
     * Create an instance of {@link ProjectTaskKey }
     * 
     */
    public ProjectTaskKey createProjectTaskKey() {
        return new ProjectTaskKey();
    }

    /**
     * Create an instance of {@link RequirementSpecificationKey }
     * 
     */
    public RequirementSpecificationKey createRequirementSpecificationKey() {
        return new RequirementSpecificationKey();
    }

    /**
     * Create an instance of {@link ProductKey }
     * 
     */
    public ProductKey createProductKey() {
        return new ProductKey();
    }

    /**
     * Create an instance of {@link SelectionByAmount }
     * 
     */
    public SelectionByAmount createSelectionByAmount() {
        return new SelectionByAmount();
    }

    /**
     * Create an instance of {@link ExchangeLogData }
     * 
     */
    public ExchangeLogData createExchangeLogData() {
        return new ExchangeLogData();
    }

    /**
     * Create an instance of {@link SelectionByIdentifier }
     * 
     */
    public SelectionByIdentifier createSelectionByIdentifier() {
        return new SelectionByIdentifier();
    }

    /**
     * Create an instance of {@link SelectionByDate }
     * 
     */
    public SelectionByDate createSelectionByDate() {
        return new SelectionByDate();
    }

    /**
     * Create an instance of {@link SelectionByName }
     * 
     */
    public SelectionByName createSelectionByName() {
        return new SelectionByName();
    }

    /**
     * Create an instance of {@link SelectionByCode }
     * 
     */
    public SelectionByCode createSelectionByCode() {
        return new SelectionByCode();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderByElementsResponseMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "SalesOrderByElementsResponse_sync")
    public JAXBElement<SalesOrderByElementsResponseMessageSync> createSalesOrderByElementsResponseSync(SalesOrderByElementsResponseMessageSync value) {
        return new JAXBElement<SalesOrderByElementsResponseMessageSync>(_SalesOrderByElementsResponseSync_QNAME, SalesOrderByElementsResponseMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderByElementsQueryMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "SalesOrderByElementsQuery_sync")
    public JAXBElement<SalesOrderByElementsQueryMessageSync> createSalesOrderByElementsQuerySync(SalesOrderByElementsQueryMessageSync value) {
        return new JAXBElement<SalesOrderByElementsQueryMessageSync>(_SalesOrderByElementsQuerySync_QNAME, SalesOrderByElementsQueryMessageSync.class, null, value);
    }

}
