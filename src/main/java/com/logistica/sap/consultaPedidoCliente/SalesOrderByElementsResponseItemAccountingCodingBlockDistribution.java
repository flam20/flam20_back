
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponseItemAccountingCodingBlockDistribution complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseItemAccountingCodingBlockDistribution">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountingCodingBlockAssignmentProjectTaskKey" type="{http://sap.com/xi/AP/Common/Global}ProjectTaskKey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseItemAccountingCodingBlockDistribution", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "accountingCodingBlockAssignmentProjectTaskKey"
})
public class SalesOrderByElementsResponseItemAccountingCodingBlockDistribution {

    @XmlElement(name = "AccountingCodingBlockAssignmentProjectTaskKey")
    protected ProjectTaskKey accountingCodingBlockAssignmentProjectTaskKey;

    /**
     * Gets the value of the accountingCodingBlockAssignmentProjectTaskKey property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectTaskKey }
     *     
     */
    public ProjectTaskKey getAccountingCodingBlockAssignmentProjectTaskKey() {
        return accountingCodingBlockAssignmentProjectTaskKey;
    }

    /**
     * Sets the value of the accountingCodingBlockAssignmentProjectTaskKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectTaskKey }
     *     
     */
    public void setAccountingCodingBlockAssignmentProjectTaskKey(ProjectTaskKey value) {
        this.accountingCodingBlockAssignmentProjectTaskKey = value;
    }

}
