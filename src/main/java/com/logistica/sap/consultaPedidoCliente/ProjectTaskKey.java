
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProjectTaskKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProjectTaskKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaskID" type="{http://sap.com/xi/AP/Common/GDT}ProjectElementID"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProjectTaskKey", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "taskID"
})
public class ProjectTaskKey {

    @XmlElement(name = "TaskID", required = true)
    protected ProjectElementID taskID;

    /**
     * Gets the value of the taskID property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectElementID }
     *     
     */
    public ProjectElementID getTaskID() {
        return taskID;
    }

    /**
     * Sets the value of the taskID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectElementID }
     *     
     */
    public void setTaskID(ProjectElementID value) {
        this.taskID = value;
    }

}
