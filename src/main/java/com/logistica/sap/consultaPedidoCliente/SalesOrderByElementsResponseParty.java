
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponseParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartyID" type="{http://sap.com/xi/AP/Common/GDT}PartyID" minOccurs="0"/>
 *         &lt;element name="StandardID" type="{http://sap.com/xi/A1S/Global}SalesOrderPartyStandardIdentifier" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AddressHostUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyAddress" minOccurs="0"/>
 *         &lt;element name="ContactParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePartyContactParty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseParty", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "partyID",
    "standardID",
    "addressHostUUID",
    "address",
    "contactParty"
})
public class SalesOrderByElementsResponseParty {

    @XmlElement(name = "PartyID")
    protected PartyID partyID;
    @XmlElement(name = "StandardID")
    protected List<SalesOrderPartyStandardIdentifier> standardID;
    @XmlElement(name = "AddressHostUUID")
    protected UUID addressHostUUID;
    @XmlElement(name = "Address")
    protected SalesOrderMaintainRequestPartyAddress address;
    @XmlElement(name = "ContactParty")
    protected List<SalesOrderByElementsResponsePartyContactParty> contactParty;

    /**
     * Gets the value of the partyID property.
     * 
     * @return
     *     possible object is
     *     {@link PartyID }
     *     
     */
    public PartyID getPartyID() {
        return partyID;
    }

    /**
     * Sets the value of the partyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyID }
     *     
     */
    public void setPartyID(PartyID value) {
        this.partyID = value;
    }

    /**
     * Gets the value of the standardID property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the standardID property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStandardID().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderPartyStandardIdentifier }
     * 
     * 
     */
    public List<SalesOrderPartyStandardIdentifier> getStandardID() {
        if (standardID == null) {
            standardID = new ArrayList<SalesOrderPartyStandardIdentifier>();
        }
        return this.standardID;
    }

    /**
     * Gets the value of the addressHostUUID property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getAddressHostUUID() {
        return addressHostUUID;
    }

    /**
     * Sets the value of the addressHostUUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setAddressHostUUID(UUID value) {
        this.addressHostUUID = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyAddress }
     *     
     */
    public SalesOrderMaintainRequestPartyAddress getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyAddress }
     *     
     */
    public void setAddress(SalesOrderMaintainRequestPartyAddress value) {
        this.address = value;
    }

    /**
     * Gets the value of the contactParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePartyContactParty }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePartyContactParty> getContactParty() {
        if (contactParty == null) {
            contactParty = new ArrayList<SalesOrderByElementsResponsePartyContactParty>();
        }
        return this.contactParty;
    }

}
