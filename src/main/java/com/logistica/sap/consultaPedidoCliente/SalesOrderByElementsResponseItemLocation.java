
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponseItemLocation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseItemLocation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LocationID" type="{http://sap.com/xi/AP/Common/GDT}LocationID" minOccurs="0"/>
 *         &lt;element name="UsedAddress" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyAddress" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseItemLocation", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "locationID",
    "usedAddress"
})
public class SalesOrderByElementsResponseItemLocation {

    @XmlElement(name = "LocationID")
    protected LocationID locationID;
    @XmlElement(name = "UsedAddress")
    protected SalesOrderMaintainRequestPartyAddress usedAddress;

    /**
     * Gets the value of the locationID property.
     * 
     * @return
     *     possible object is
     *     {@link LocationID }
     *     
     */
    public LocationID getLocationID() {
        return locationID;
    }

    /**
     * Sets the value of the locationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationID }
     *     
     */
    public void setLocationID(LocationID value) {
        this.locationID = value;
    }

    /**
     * Gets the value of the usedAddress property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyAddress }
     *     
     */
    public SalesOrderMaintainRequestPartyAddress getUsedAddress() {
        return usedAddress;
    }

    /**
     * Sets the value of the usedAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyAddress }
     *     
     */
    public void setUsedAddress(SalesOrderMaintainRequestPartyAddress value) {
        this.usedAddress = value;
    }

}
