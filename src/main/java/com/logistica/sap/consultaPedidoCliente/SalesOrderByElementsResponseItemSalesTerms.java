
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponseItemSalesTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseItemSalesTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IndustrialSectorCode" type="{http://sap.com/xi/AP/Common/GDT}IndustrialSectorCode" minOccurs="0"/>
 *         &lt;element name="CancellationReasonCode" type="{http://sap.com/xi/AP/Common/GDT}CancellationReasonCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseItemSalesTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "industrialSectorCode",
    "cancellationReasonCode"
})
public class SalesOrderByElementsResponseItemSalesTerms {

    @XmlElement(name = "IndustrialSectorCode")
    protected IndustrialSectorCode industrialSectorCode;
    @XmlElement(name = "CancellationReasonCode")
    protected CancellationReasonCode cancellationReasonCode;

    /**
     * Gets the value of the industrialSectorCode property.
     * 
     * @return
     *     possible object is
     *     {@link IndustrialSectorCode }
     *     
     */
    public IndustrialSectorCode getIndustrialSectorCode() {
        return industrialSectorCode;
    }

    /**
     * Sets the value of the industrialSectorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndustrialSectorCode }
     *     
     */
    public void setIndustrialSectorCode(IndustrialSectorCode value) {
        this.industrialSectorCode = value;
    }

    /**
     * Gets the value of the cancellationReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link CancellationReasonCode }
     *     
     */
    public CancellationReasonCode getCancellationReasonCode() {
        return cancellationReasonCode;
    }

    /**
     * Sets the value of the cancellationReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancellationReasonCode }
     *     
     */
    public void setCancellationReasonCode(CancellationReasonCode value) {
        this.cancellationReasonCode = value;
    }

}
