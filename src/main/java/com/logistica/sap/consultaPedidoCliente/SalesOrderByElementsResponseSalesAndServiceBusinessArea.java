
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponseSalesAndServiceBusinessArea complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseSalesAndServiceBusinessArea">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesOrganisationID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *         &lt;element name="SalesGroupID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *         &lt;element name="SalesOfficeID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *         &lt;element name="DistributionChannelCode" type="{http://sap.com/xi/AP/Common/GDT}DistributionChannelCode" minOccurs="0"/>
 *         &lt;element name="ServiceOrganisationID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseSalesAndServiceBusinessArea", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "salesOrganisationID",
    "salesGroupID",
    "salesOfficeID",
    "distributionChannelCode",
    "serviceOrganisationID"
})
public class SalesOrderByElementsResponseSalesAndServiceBusinessArea {

    @XmlElement(name = "SalesOrganisationID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesOrganisationID;
    @XmlElement(name = "SalesGroupID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesGroupID;
    @XmlElement(name = "SalesOfficeID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesOfficeID;
    @XmlElement(name = "DistributionChannelCode")
    protected DistributionChannelCode distributionChannelCode;
    @XmlElement(name = "ServiceOrganisationID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String serviceOrganisationID;

    /**
     * Gets the value of the salesOrganisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOrganisationID() {
        return salesOrganisationID;
    }

    /**
     * Sets the value of the salesOrganisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOrganisationID(String value) {
        this.salesOrganisationID = value;
    }

    /**
     * Gets the value of the salesGroupID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesGroupID() {
        return salesGroupID;
    }

    /**
     * Sets the value of the salesGroupID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesGroupID(String value) {
        this.salesGroupID = value;
    }

    /**
     * Gets the value of the salesOfficeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOfficeID() {
        return salesOfficeID;
    }

    /**
     * Sets the value of the salesOfficeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOfficeID(String value) {
        this.salesOfficeID = value;
    }

    /**
     * Gets the value of the distributionChannelCode property.
     * 
     * @return
     *     possible object is
     *     {@link DistributionChannelCode }
     *     
     */
    public DistributionChannelCode getDistributionChannelCode() {
        return distributionChannelCode;
    }

    /**
     * Sets the value of the distributionChannelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistributionChannelCode }
     *     
     */
    public void setDistributionChannelCode(DistributionChannelCode value) {
        this.distributionChannelCode = value;
    }

    /**
     * Gets the value of the serviceOrganisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOrganisationID() {
        return serviceOrganisationID;
    }

    /**
     * Sets the value of the serviceOrganisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOrganisationID(String value) {
        this.serviceOrganisationID = value;
    }

}
