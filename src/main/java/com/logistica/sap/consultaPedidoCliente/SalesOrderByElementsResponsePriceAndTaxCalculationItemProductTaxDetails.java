
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ProductTax" type="{http://sap.com/xi/AP/Common/GDT}ProductTax" minOccurs="0"/>
 *         &lt;element name="TransactionCurrencyProductTax" type="{http://sap.com/xi/AP/Common/GDT}ProductTax" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "uuid",
    "productTax",
    "transactionCurrencyProductTax"
})
public class SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "ProductTax")
    protected ProductTax productTax;
    @XmlElement(name = "TransactionCurrencyProductTax")
    protected ProductTax transactionCurrencyProductTax;

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the productTax property.
     * 
     * @return
     *     possible object is
     *     {@link ProductTax }
     *     
     */
    public ProductTax getProductTax() {
        return productTax;
    }

    /**
     * Sets the value of the productTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTax }
     *     
     */
    public void setProductTax(ProductTax value) {
        this.productTax = value;
    }

    /**
     * Gets the value of the transactionCurrencyProductTax property.
     * 
     * @return
     *     possible object is
     *     {@link ProductTax }
     *     
     */
    public ProductTax getTransactionCurrencyProductTax() {
        return transactionCurrencyProductTax;
    }

    /**
     * Sets the value of the transactionCurrencyProductTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTax }
     *     
     */
    public void setTransactionCurrencyProductTax(ProductTax value) {
        this.transactionCurrencyProductTax = value;
    }

}
