
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsQueryMessage_sync complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQueryMessage_sync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesOrderSelectionByElements" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQuerySelectionByElements" minOccurs="0"/>
 *         &lt;element name="ProcessingConditions" type="{http://sap.com/xi/AP/Common/GDT}QueryProcessingConditions" minOccurs="0"/>
 *         &lt;element name="RequestedElements" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQueryRequestedElements" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQueryMessage_sync", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "salesOrderSelectionByElements",
    "processingConditions",
    "requestedElements"
})
public class SalesOrderByElementsQueryMessageSync {

    @XmlElement(name = "SalesOrderSelectionByElements")
    protected SalesOrderByElementsQuerySelectionByElements salesOrderSelectionByElements;
    @XmlElement(name = "ProcessingConditions")
    protected QueryProcessingConditions processingConditions;
    @XmlElement(name = "RequestedElements")
    protected SalesOrderByElementsQueryRequestedElements requestedElements;

    /**
     * Gets the value of the salesOrderSelectionByElements property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsQuerySelectionByElements }
     *     
     */
    public SalesOrderByElementsQuerySelectionByElements getSalesOrderSelectionByElements() {
        return salesOrderSelectionByElements;
    }

    /**
     * Sets the value of the salesOrderSelectionByElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsQuerySelectionByElements }
     *     
     */
    public void setSalesOrderSelectionByElements(SalesOrderByElementsQuerySelectionByElements value) {
        this.salesOrderSelectionByElements = value;
    }

    /**
     * Gets the value of the processingConditions property.
     * 
     * @return
     *     possible object is
     *     {@link QueryProcessingConditions }
     *     
     */
    public QueryProcessingConditions getProcessingConditions() {
        return processingConditions;
    }

    /**
     * Sets the value of the processingConditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryProcessingConditions }
     *     
     */
    public void setProcessingConditions(QueryProcessingConditions value) {
        this.processingConditions = value;
    }

    /**
     * Gets the value of the requestedElements property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsQueryRequestedElements }
     *     
     */
    public SalesOrderByElementsQueryRequestedElements getRequestedElements() {
        return requestedElements;
    }

    /**
     * Sets the value of the requestedElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsQueryRequestedElements }
     *     
     */
    public void setRequestedElements(SalesOrderByElementsQueryRequestedElements value) {
        this.requestedElements = value;
    }

}
