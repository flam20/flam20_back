
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SalesOrderByElementsResponseCashDiscountTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseCashDiscountTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountTermsCode" minOccurs="0"/>
 *         &lt;element name="PaymentBaselineDate" type="{http://sap.com/xi/BASIS/Global}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseCashDiscountTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "code",
    "paymentBaselineDate"
})
public class SalesOrderByElementsResponseCashDiscountTerms {

    @XmlElement(name = "Code")
    protected CashDiscountTermsCode code;
    @XmlElement(name = "PaymentBaselineDate")
    protected XMLGregorianCalendar paymentBaselineDate;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link CashDiscountTermsCode }
     *     
     */
    public CashDiscountTermsCode getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashDiscountTermsCode }
     *     
     */
    public void setCode(CashDiscountTermsCode value) {
        this.code = value;
    }

    /**
     * Gets the value of the paymentBaselineDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentBaselineDate() {
        return paymentBaselineDate;
    }

    /**
     * Sets the value of the paymentBaselineDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentBaselineDate(XMLGregorianCalendar value) {
        this.paymentBaselineDate = value;
    }

}
