
package com.logistica.sap.consultaPedidoCliente;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "QuerySalesOrderIn", targetNamespace = "http://sap.com/xi/A1S/Global")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface QuerySalesOrderIn {


    /**
     * 
     * @param salesOrderByElementsQuerySync
     * @return
     *     returns com.logistica.integracionsap.ws.consultaPedidoCliente.SalesOrderByElementsResponseMessageSync
     * @throws StandardFaultMessage_Exception
     */
    @WebMethod(operationName = "FindByElements", action = "http://sap.com/xi/A1S/Global/QuerySalesOrderIn/FindByElementsRequest")
    @WebResult(name = "SalesOrderByElementsResponse_sync", targetNamespace = "http://sap.com/xi/SAPGlobal20/Global", partName = "SalesOrderByElementsResponse_sync")
    public SalesOrderByElementsResponseMessageSync findByElements(
        @WebParam(name = "SalesOrderByElementsQuery_sync", targetNamespace = "http://sap.com/xi/SAPGlobal20/Global", partName = "SalesOrderByElementsQuery_sync")
        SalesOrderByElementsQueryMessageSync salesOrderByElementsQuerySync)
        throws StandardFaultMessage_Exception
    ;

}
