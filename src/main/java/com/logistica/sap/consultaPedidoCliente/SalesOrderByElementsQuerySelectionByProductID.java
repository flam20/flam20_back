
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsQuerySelectionByProductID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQuerySelectionByProductID">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InclusionExclusionCode" type="{http://sap.com/xi/AP/Common/GDT}InclusionExclusionCode" minOccurs="0"/>
 *         &lt;element name="IntervalBoundaryTypeCode" type="{http://sap.com/xi/AP/Common/GDT}IntervalBoundaryTypeCode"/>
 *         &lt;element name="LowerBoundaryProductID" type="{http://sap.com/xi/AP/Common/GDT}NOCONVERSION_ProductID" minOccurs="0"/>
 *         &lt;element name="UpperBoundaryProductID" type="{http://sap.com/xi/AP/Common/GDT}NOCONVERSION_ProductID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQuerySelectionByProductID", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "inclusionExclusionCode",
    "intervalBoundaryTypeCode",
    "lowerBoundaryProductID",
    "upperBoundaryProductID"
})
public class SalesOrderByElementsQuerySelectionByProductID {

    @XmlElement(name = "InclusionExclusionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String inclusionExclusionCode;
    @XmlElement(name = "IntervalBoundaryTypeCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String intervalBoundaryTypeCode;
    @XmlElement(name = "LowerBoundaryProductID")
    protected NOCONVERSIONProductID lowerBoundaryProductID;
    @XmlElement(name = "UpperBoundaryProductID")
    protected NOCONVERSIONProductID upperBoundaryProductID;

    /**
     * Gets the value of the inclusionExclusionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInclusionExclusionCode() {
        return inclusionExclusionCode;
    }

    /**
     * Sets the value of the inclusionExclusionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInclusionExclusionCode(String value) {
        this.inclusionExclusionCode = value;
    }

    /**
     * Gets the value of the intervalBoundaryTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntervalBoundaryTypeCode() {
        return intervalBoundaryTypeCode;
    }

    /**
     * Sets the value of the intervalBoundaryTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntervalBoundaryTypeCode(String value) {
        this.intervalBoundaryTypeCode = value;
    }

    /**
     * Gets the value of the lowerBoundaryProductID property.
     * 
     * @return
     *     possible object is
     *     {@link NOCONVERSIONProductID }
     *     
     */
    public NOCONVERSIONProductID getLowerBoundaryProductID() {
        return lowerBoundaryProductID;
    }

    /**
     * Sets the value of the lowerBoundaryProductID property.
     * 
     * @param value
     *     allowed object is
     *     {@link NOCONVERSIONProductID }
     *     
     */
    public void setLowerBoundaryProductID(NOCONVERSIONProductID value) {
        this.lowerBoundaryProductID = value;
    }

    /**
     * Gets the value of the upperBoundaryProductID property.
     * 
     * @return
     *     possible object is
     *     {@link NOCONVERSIONProductID }
     *     
     */
    public NOCONVERSIONProductID getUpperBoundaryProductID() {
        return upperBoundaryProductID;
    }

    /**
     * Sets the value of the upperBoundaryProductID property.
     * 
     * @param value
     *     allowed object is
     *     {@link NOCONVERSIONProductID }
     *     
     */
    public void setUpperBoundaryProductID(NOCONVERSIONProductID value) {
        this.upperBoundaryProductID = value;
    }

}
