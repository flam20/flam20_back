
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for PriceAndTaxCalculationItemStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceAndTaxCalculationItemStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CalculationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CalculationStatusCode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceAndTaxCalculationItemStatus", namespace = "http://sap.com/xi/AP/FO/PriceAndTax/Global", propOrder = {
    "calculationStatusCode"
})
public class PriceAndTaxCalculationItemStatus {

    @XmlElement(name = "CalculationStatusCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String calculationStatusCode;

    /**
     * Gets the value of the calculationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculationStatusCode() {
        return calculationStatusCode;
    }

    /**
     * Sets the value of the calculationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculationStatusCode(String value) {
        this.calculationStatusCode = value;
    }

}
