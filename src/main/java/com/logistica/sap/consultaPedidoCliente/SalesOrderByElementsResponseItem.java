
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SalesOrderByElementsResponseItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;element name="BuyerID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;element name="ProcessingTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemProcessingTypeCode" minOccurs="0"/>
 *         &lt;element name="PostingDate" type="{http://sap.com/xi/BASIS/Global}GLOBAL_DateTime" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="FulfilmentPartyCategoryCode" type="{http://sap.com/xi/AP/Common/GDT}FulfilmentPartyCategoryCode" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemStatus" minOccurs="0"/>
 *         &lt;element name="ItemBusinessTransactionDocumentReference" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseBusinessTransactionDocumentReference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemProduct" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemProduct" minOccurs="0"/>
 *         &lt;element name="ItemDeliveryTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="ItemSalesTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemSalesTerms" minOccurs="0"/>
 *         &lt;element name="ItemServiceTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemServiceTerms" minOccurs="0"/>
 *         &lt;element name="ItemScheduleLine" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemScheduleLine" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProductRecipientItemParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="VendorItemParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="ServicePerformerItemParty" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseParty" minOccurs="0"/>
 *         &lt;element name="ShipFromItemLocation" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemLocation" minOccurs="0"/>
 *         &lt;element name="ItemAccountingCodingBlockDistribution" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseItemAccountingCodingBlockDistribution" minOccurs="0"/>
 *         &lt;element name="ItemAttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}AccessAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="ItemTextCollection" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponseTextCollection" minOccurs="0"/>
 *         &lt;element name="PriceAndTaxCalculationItem" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItem" minOccurs="0"/>
 *         &lt;element name="ParentID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8C980F3879223D5C"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8EB37A0EC9665D5B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8EB3BCDD1A8C1EC3"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8EB3C80C330ADF10"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8E959D4B71B8D3CD"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB45FCBEE1602A7"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB47B9319A54359"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB4924C386D43CA"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB4BA38368DC52B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB4D4826CDEC5B2"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB4E9D673338624"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8EB503F715F706B4"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8FA6740C9F314689"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8FA69C5268CA8776"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EEA8FA6B252B8358808"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "id",
    "buyerID",
    "processingTypeCode",
    "postingDate",
    "description",
    "fulfilmentPartyCategoryCode",
    "status",
    "itemBusinessTransactionDocumentReference",
    "itemProduct",
    "itemDeliveryTerms",
    "itemSalesTerms",
    "itemServiceTerms",
    "itemScheduleLine",
    "productRecipientItemParty",
    "vendorItemParty",
    "servicePerformerItemParty",
    "shipFromItemLocation",
    "itemAccountingCodingBlockDistribution",
    "itemAttachmentFolder",
    "itemTextCollection",
    "priceAndTaxCalculationItem",
    "parentID",
    "facturado",
    "ubicacinorigenPD",
    "ubicacinDestinoPD",
    "ubicacinCrucePD",
    "pruebaDetalle",
    "consignatarioDestinoPD",
    "tipodeEquipoPD",
    "nmerodeequipoPD",
    "guaHousePD",
    "guaMasterPD",
    "requierePODPD",
    "servicio",
    "fechaCrucePD2",
    "fechaCargaETDPD2",
    "fechaDestinoETAPD2"
})
public class SalesOrderByElementsResponseItem {

    @XmlElement(name = "ID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String id;
    @XmlElement(name = "BuyerID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String buyerID;
    @XmlElement(name = "ProcessingTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String processingTypeCode;
    @XmlElement(name = "PostingDate")
    protected XMLGregorianCalendar postingDate;
    @XmlElement(name = "Description")
    protected SHORTDescription description;
    @XmlElement(name = "FulfilmentPartyCategoryCode")
    protected FulfilmentPartyCategoryCode fulfilmentPartyCategoryCode;
    @XmlElement(name = "Status")
    protected SalesOrderByElementsResponseItemStatus status;
    @XmlElement(name = "ItemBusinessTransactionDocumentReference")
    protected List<SalesOrderByElementsResponseBusinessTransactionDocumentReference> itemBusinessTransactionDocumentReference;
    @XmlElement(name = "ItemProduct")
    protected SalesOrderByElementsResponseItemProduct itemProduct;
    @XmlElement(name = "ItemDeliveryTerms")
    protected SalesOrderByElementsResponseItemDeliveryTerms itemDeliveryTerms;
    @XmlElement(name = "ItemSalesTerms")
    protected SalesOrderByElementsResponseItemSalesTerms itemSalesTerms;
    @XmlElement(name = "ItemServiceTerms")
    protected SalesOrderByElementsResponseItemServiceTerms itemServiceTerms;
    @XmlElement(name = "ItemScheduleLine")
    protected List<SalesOrderByElementsResponseItemScheduleLine> itemScheduleLine;
    @XmlElement(name = "ProductRecipientItemParty")
    protected SalesOrderByElementsResponseParty productRecipientItemParty;
    @XmlElement(name = "VendorItemParty")
    protected SalesOrderByElementsResponseParty vendorItemParty;
    @XmlElement(name = "ServicePerformerItemParty")
    protected SalesOrderByElementsResponseParty servicePerformerItemParty;
    @XmlElement(name = "ShipFromItemLocation")
    protected SalesOrderByElementsResponseItemLocation shipFromItemLocation;
    @XmlElement(name = "ItemAccountingCodingBlockDistribution")
    protected SalesOrderByElementsResponseItemAccountingCodingBlockDistribution itemAccountingCodingBlockDistribution;
    @XmlElement(name = "ItemAttachmentFolder")
    protected AccessAttachmentFolder itemAttachmentFolder;
    @XmlElement(name = "ItemTextCollection")
    protected SalesOrderByElementsResponseTextCollection itemTextCollection;
    @XmlElement(name = "PriceAndTaxCalculationItem")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationItem priceAndTaxCalculationItem;
    @XmlElement(name = "ParentID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String parentID;
    @XmlElement(name = "Facturado", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected Boolean facturado;
    @XmlElement(name = "UbicacinorigenPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinorigenPD;
    @XmlElement(name = "UbicacinDestinoPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestinoPD;
    @XmlElement(name = "UbicacinCrucePD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCrucePD;
    @XmlElement(name = "PruebaDetalle", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pruebaDetalle;
    @XmlElement(name = "ConsignatarioDestinoPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioDestinoPD;
    @XmlElement(name = "TipodeEquipoPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeEquipoPD;
    @XmlElement(name = "NmerodeequipoPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipoPD;
    @XmlElement(name = "GuaHousePD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHousePD;
    @XmlElement(name = "GuaMasterPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMasterPD;
    @XmlElement(name = "RequierePODPD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String requierePODPD;
    @XmlElement(name = "Servicio", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String servicio;
    @XmlElement(name = "FechaCrucePD2", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCrucePD2;
    @XmlElement(name = "FechaCargaETDPD2", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCargaETDPD2;
    @XmlElement(name = "FechaDestinoETAPD2", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaDestinoETAPD2;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the buyerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerID() {
        return buyerID;
    }

    /**
     * Sets the value of the buyerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerID(String value) {
        this.buyerID = value;
    }

    /**
     * Gets the value of the processingTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessingTypeCode() {
        return processingTypeCode;
    }

    /**
     * Sets the value of the processingTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessingTypeCode(String value) {
        this.processingTypeCode = value;
    }

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostingDate(XMLGregorianCalendar value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setDescription(SHORTDescription value) {
        this.description = value;
    }

    /**
     * Gets the value of the fulfilmentPartyCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link FulfilmentPartyCategoryCode }
     *     
     */
    public FulfilmentPartyCategoryCode getFulfilmentPartyCategoryCode() {
        return fulfilmentPartyCategoryCode;
    }

    /**
     * Sets the value of the fulfilmentPartyCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfilmentPartyCategoryCode }
     *     
     */
    public void setFulfilmentPartyCategoryCode(FulfilmentPartyCategoryCode value) {
        this.fulfilmentPartyCategoryCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemStatus }
     *     
     */
    public SalesOrderByElementsResponseItemStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemStatus }
     *     
     */
    public void setStatus(SalesOrderByElementsResponseItemStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the itemBusinessTransactionDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemBusinessTransactionDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemBusinessTransactionDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponseBusinessTransactionDocumentReference }
     * 
     * 
     */
    public List<SalesOrderByElementsResponseBusinessTransactionDocumentReference> getItemBusinessTransactionDocumentReference() {
        if (itemBusinessTransactionDocumentReference == null) {
            itemBusinessTransactionDocumentReference = new ArrayList<SalesOrderByElementsResponseBusinessTransactionDocumentReference>();
        }
        return this.itemBusinessTransactionDocumentReference;
    }

    /**
     * Gets the value of the itemProduct property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemProduct }
     *     
     */
    public SalesOrderByElementsResponseItemProduct getItemProduct() {
        return itemProduct;
    }

    /**
     * Sets the value of the itemProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemProduct }
     *     
     */
    public void setItemProduct(SalesOrderByElementsResponseItemProduct value) {
        this.itemProduct = value;
    }

    /**
     * Gets the value of the itemDeliveryTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemDeliveryTerms }
     *     
     */
    public SalesOrderByElementsResponseItemDeliveryTerms getItemDeliveryTerms() {
        return itemDeliveryTerms;
    }

    /**
     * Sets the value of the itemDeliveryTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemDeliveryTerms }
     *     
     */
    public void setItemDeliveryTerms(SalesOrderByElementsResponseItemDeliveryTerms value) {
        this.itemDeliveryTerms = value;
    }

    /**
     * Gets the value of the itemSalesTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemSalesTerms }
     *     
     */
    public SalesOrderByElementsResponseItemSalesTerms getItemSalesTerms() {
        return itemSalesTerms;
    }

    /**
     * Sets the value of the itemSalesTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemSalesTerms }
     *     
     */
    public void setItemSalesTerms(SalesOrderByElementsResponseItemSalesTerms value) {
        this.itemSalesTerms = value;
    }

    /**
     * Gets the value of the itemServiceTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemServiceTerms }
     *     
     */
    public SalesOrderByElementsResponseItemServiceTerms getItemServiceTerms() {
        return itemServiceTerms;
    }

    /**
     * Sets the value of the itemServiceTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemServiceTerms }
     *     
     */
    public void setItemServiceTerms(SalesOrderByElementsResponseItemServiceTerms value) {
        this.itemServiceTerms = value;
    }

    /**
     * Gets the value of the itemScheduleLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemScheduleLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemScheduleLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponseItemScheduleLine }
     * 
     * 
     */
    public List<SalesOrderByElementsResponseItemScheduleLine> getItemScheduleLine() {
        if (itemScheduleLine == null) {
            itemScheduleLine = new ArrayList<SalesOrderByElementsResponseItemScheduleLine>();
        }
        return this.itemScheduleLine;
    }

    /**
     * Gets the value of the productRecipientItemParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getProductRecipientItemParty() {
        return productRecipientItemParty;
    }

    /**
     * Sets the value of the productRecipientItemParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setProductRecipientItemParty(SalesOrderByElementsResponseParty value) {
        this.productRecipientItemParty = value;
    }

    /**
     * Gets the value of the vendorItemParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getVendorItemParty() {
        return vendorItemParty;
    }

    /**
     * Sets the value of the vendorItemParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setVendorItemParty(SalesOrderByElementsResponseParty value) {
        this.vendorItemParty = value;
    }

    /**
     * Gets the value of the servicePerformerItemParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public SalesOrderByElementsResponseParty getServicePerformerItemParty() {
        return servicePerformerItemParty;
    }

    /**
     * Sets the value of the servicePerformerItemParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseParty }
     *     
     */
    public void setServicePerformerItemParty(SalesOrderByElementsResponseParty value) {
        this.servicePerformerItemParty = value;
    }

    /**
     * Gets the value of the shipFromItemLocation property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemLocation }
     *     
     */
    public SalesOrderByElementsResponseItemLocation getShipFromItemLocation() {
        return shipFromItemLocation;
    }

    /**
     * Sets the value of the shipFromItemLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemLocation }
     *     
     */
    public void setShipFromItemLocation(SalesOrderByElementsResponseItemLocation value) {
        this.shipFromItemLocation = value;
    }

    /**
     * Gets the value of the itemAccountingCodingBlockDistribution property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseItemAccountingCodingBlockDistribution }
     *     
     */
    public SalesOrderByElementsResponseItemAccountingCodingBlockDistribution getItemAccountingCodingBlockDistribution() {
        return itemAccountingCodingBlockDistribution;
    }

    /**
     * Sets the value of the itemAccountingCodingBlockDistribution property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseItemAccountingCodingBlockDistribution }
     *     
     */
    public void setItemAccountingCodingBlockDistribution(SalesOrderByElementsResponseItemAccountingCodingBlockDistribution value) {
        this.itemAccountingCodingBlockDistribution = value;
    }

    /**
     * Gets the value of the itemAttachmentFolder property.
     * 
     * @return
     *     possible object is
     *     {@link AccessAttachmentFolder }
     *     
     */
    public AccessAttachmentFolder getItemAttachmentFolder() {
        return itemAttachmentFolder;
    }

    /**
     * Sets the value of the itemAttachmentFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessAttachmentFolder }
     *     
     */
    public void setItemAttachmentFolder(AccessAttachmentFolder value) {
        this.itemAttachmentFolder = value;
    }

    /**
     * Gets the value of the itemTextCollection property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponseTextCollection }
     *     
     */
    public SalesOrderByElementsResponseTextCollection getItemTextCollection() {
        return itemTextCollection;
    }

    /**
     * Sets the value of the itemTextCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponseTextCollection }
     *     
     */
    public void setItemTextCollection(SalesOrderByElementsResponseTextCollection value) {
        this.itemTextCollection = value;
    }

    /**
     * Gets the value of the priceAndTaxCalculationItem property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItem }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItem getPriceAndTaxCalculationItem() {
        return priceAndTaxCalculationItem;
    }

    /**
     * Sets the value of the priceAndTaxCalculationItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItem }
     *     
     */
    public void setPriceAndTaxCalculationItem(SalesOrderByElementsResponsePriceAndTaxCalculationItem value) {
        this.priceAndTaxCalculationItem = value;
    }

    /**
     * Gets the value of the parentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentID() {
        return parentID;
    }

    /**
     * Sets the value of the parentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentID(String value) {
        this.parentID = value;
    }

    /**
     * Gets the value of the facturado property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFacturado() {
        return facturado;
    }

    /**
     * Sets the value of the facturado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFacturado(Boolean value) {
        this.facturado = value;
    }

    /**
     * Gets the value of the ubicacinorigenPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinorigenPD() {
        return ubicacinorigenPD;
    }

    /**
     * Sets the value of the ubicacinorigenPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinorigenPD(String value) {
        this.ubicacinorigenPD = value;
    }

    /**
     * Gets the value of the ubicacinDestinoPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestinoPD() {
        return ubicacinDestinoPD;
    }

    /**
     * Sets the value of the ubicacinDestinoPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestinoPD(String value) {
        this.ubicacinDestinoPD = value;
    }

    /**
     * Gets the value of the ubicacinCrucePD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCrucePD() {
        return ubicacinCrucePD;
    }

    /**
     * Sets the value of the ubicacinCrucePD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCrucePD(String value) {
        this.ubicacinCrucePD = value;
    }

    /**
     * Gets the value of the pruebaDetalle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPruebaDetalle() {
        return pruebaDetalle;
    }

    /**
     * Sets the value of the pruebaDetalle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPruebaDetalle(String value) {
        this.pruebaDetalle = value;
    }

    /**
     * Gets the value of the consignatarioDestinoPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioDestinoPD() {
        return consignatarioDestinoPD;
    }

    /**
     * Sets the value of the consignatarioDestinoPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioDestinoPD(String value) {
        this.consignatarioDestinoPD = value;
    }

    /**
     * Gets the value of the tipodeEquipoPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeEquipoPD() {
        return tipodeEquipoPD;
    }

    /**
     * Sets the value of the tipodeEquipoPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeEquipoPD(String value) {
        this.tipodeEquipoPD = value;
    }

    /**
     * Gets the value of the nmerodeequipoPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipoPD() {
        return nmerodeequipoPD;
    }

    /**
     * Sets the value of the nmerodeequipoPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipoPD(String value) {
        this.nmerodeequipoPD = value;
    }

    /**
     * Gets the value of the guaHousePD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHousePD() {
        return guaHousePD;
    }

    /**
     * Sets the value of the guaHousePD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHousePD(String value) {
        this.guaHousePD = value;
    }

    /**
     * Gets the value of the guaMasterPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMasterPD() {
        return guaMasterPD;
    }

    /**
     * Sets the value of the guaMasterPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMasterPD(String value) {
        this.guaMasterPD = value;
    }

    /**
     * Gets the value of the requierePODPD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequierePODPD() {
        return requierePODPD;
    }

    /**
     * Sets the value of the requierePODPD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequierePODPD(String value) {
        this.requierePODPD = value;
    }

    /**
     * Gets the value of the servicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * Sets the value of the servicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicio(String value) {
        this.servicio = value;
    }

    /**
     * Gets the value of the fechaCrucePD2 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCrucePD2() {
        return fechaCrucePD2;
    }

    /**
     * Sets the value of the fechaCrucePD2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCrucePD2(XMLGregorianCalendar value) {
        this.fechaCrucePD2 = value;
    }

    /**
     * Gets the value of the fechaCargaETDPD2 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCargaETDPD2() {
        return fechaCargaETDPD2;
    }

    /**
     * Sets the value of the fechaCargaETDPD2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCargaETDPD2(XMLGregorianCalendar value) {
        this.fechaCargaETDPD2 = value;
    }

    /**
     * Gets the value of the fechaDestinoETAPD2 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestinoETAPD2() {
        return fechaDestinoETAPD2;
    }

    /**
     * Sets the value of the fechaDestinoETAPD2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestinoETAPD2(XMLGregorianCalendar value) {
        this.fechaDestinoETAPD2 = value;
    }

}
