
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponsePricingTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponsePricingTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrencyCode" type="{http://sap.com/xi/AP/Common/GDT}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="PriceDateTime" type="{http://sap.com/xi/BASIS/Global}LOCALNORMALISED_DateTime" minOccurs="0"/>
 *         &lt;element name="GrossAmountIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponsePricingTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "currencyCode",
    "priceDateTime",
    "grossAmountIndicator"
})
public class SalesOrderByElementsResponsePricingTerms {

    @XmlElement(name = "CurrencyCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String currencyCode;
    @XmlElement(name = "PriceDateTime")
    protected LOCALNORMALISEDDateTime2 priceDateTime;
    @XmlElement(name = "GrossAmountIndicator")
    protected boolean grossAmountIndicator;

    /**
     * Gets the value of the currencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the priceDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link LOCALNORMALISEDDateTime2 }
     *     
     */
    public LOCALNORMALISEDDateTime2 getPriceDateTime() {
        return priceDateTime;
    }

    /**
     * Sets the value of the priceDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link LOCALNORMALISEDDateTime2 }
     *     
     */
    public void setPriceDateTime(LOCALNORMALISEDDateTime2 value) {
        this.priceDateTime = value;
    }

    /**
     * Gets the value of the grossAmountIndicator property.
     * 
     */
    public boolean isGrossAmountIndicator() {
        return grossAmountIndicator;
    }

    /**
     * Sets the value of the grossAmountIndicator property.
     * 
     */
    public void setGrossAmountIndicator(boolean value) {
        this.grossAmountIndicator = value;
    }

}
