
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InclusionExclusionCode" type="{http://sap.com/xi/AP/Common/GDT}InclusionExclusionCode" minOccurs="0"/>
 *         &lt;element name="IntervalBoundaryTypeCode" type="{http://sap.com/xi/AP/Common/GDT}IntervalBoundaryTypeCode"/>
 *         &lt;element name="LowerBoundaryItemListCustomerOrderLifeCycleStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerOrderLifeCycleStatusCode" minOccurs="0"/>
 *         &lt;element name="UpperBoundaryItemListCustomerOrderLifeCycleStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerOrderLifeCycleStatusCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "inclusionExclusionCode",
    "intervalBoundaryTypeCode",
    "lowerBoundaryItemListCustomerOrderLifeCycleStatusCode",
    "upperBoundaryItemListCustomerOrderLifeCycleStatusCode"
})
public class SalesOrderByElementsQuerySelectionByStatusItemListCustomerOrderLifeCycleStatusCode {

    @XmlElement(name = "InclusionExclusionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String inclusionExclusionCode;
    @XmlElement(name = "IntervalBoundaryTypeCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String intervalBoundaryTypeCode;
    @XmlElement(name = "LowerBoundaryItemListCustomerOrderLifeCycleStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String lowerBoundaryItemListCustomerOrderLifeCycleStatusCode;
    @XmlElement(name = "UpperBoundaryItemListCustomerOrderLifeCycleStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String upperBoundaryItemListCustomerOrderLifeCycleStatusCode;

    /**
     * Gets the value of the inclusionExclusionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInclusionExclusionCode() {
        return inclusionExclusionCode;
    }

    /**
     * Sets the value of the inclusionExclusionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInclusionExclusionCode(String value) {
        this.inclusionExclusionCode = value;
    }

    /**
     * Gets the value of the intervalBoundaryTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntervalBoundaryTypeCode() {
        return intervalBoundaryTypeCode;
    }

    /**
     * Sets the value of the intervalBoundaryTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntervalBoundaryTypeCode(String value) {
        this.intervalBoundaryTypeCode = value;
    }

    /**
     * Gets the value of the lowerBoundaryItemListCustomerOrderLifeCycleStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowerBoundaryItemListCustomerOrderLifeCycleStatusCode() {
        return lowerBoundaryItemListCustomerOrderLifeCycleStatusCode;
    }

    /**
     * Sets the value of the lowerBoundaryItemListCustomerOrderLifeCycleStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowerBoundaryItemListCustomerOrderLifeCycleStatusCode(String value) {
        this.lowerBoundaryItemListCustomerOrderLifeCycleStatusCode = value;
    }

    /**
     * Gets the value of the upperBoundaryItemListCustomerOrderLifeCycleStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpperBoundaryItemListCustomerOrderLifeCycleStatusCode() {
        return upperBoundaryItemListCustomerOrderLifeCycleStatusCode;
    }

    /**
     * Sets the value of the upperBoundaryItemListCustomerOrderLifeCycleStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpperBoundaryItemListCustomerOrderLifeCycleStatusCode(String value) {
        this.upperBoundaryItemListCustomerOrderLifeCycleStatusCode = value;
    }

}
