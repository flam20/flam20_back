
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponseItemStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseItemStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsistencyStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ConsistencyStatusCode" minOccurs="0"/>
 *         &lt;element name="ConsistencyStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ProductAvailabilityConfirmationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProductAvailabilityConfirmationStatusCode" minOccurs="0"/>
 *         &lt;element name="ProductAvailabilityConfirmationStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="FulfilmentDataCompletenessStatusCode" type="{http://sap.com/xi/AP/Common/GDT}DataCompletenessStatusCode" minOccurs="0"/>
 *         &lt;element name="FulfilmentDataCompletenessStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="InvoicingDataCompletenessStatusCode" type="{http://sap.com/xi/AP/Common/GDT}DataCompletenessStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoicingDataCompletenessStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="PricingDataCompletenessStatusCode" type="{http://sap.com/xi/AP/Common/GDT}DataCompletenessStatusCode" minOccurs="0"/>
 *         &lt;element name="PricingDataCompletenessStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="GeneralDataCompletenessStatusCode" type="{http://sap.com/xi/AP/Common/GDT}DataCompletenessStatusCode" minOccurs="0"/>
 *         &lt;element name="GeneralDataCompletenessStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="FulfilmentProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="FulfilmentProcessingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="InvoiceProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoiceProcessingStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="CustomerOrderLifeCycleStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerOrderLifeCycleStatusCode" minOccurs="0"/>
 *         &lt;element name="CustomerOrderLifeCycleStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="CancellationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CancellationStatusCode" minOccurs="0"/>
 *         &lt;element name="CancellationStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="PlanningReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="PlanningReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ExecutionReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="ExecutionReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ApprovalStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ApprovalStatusCode" minOccurs="0"/>
 *         &lt;element name="ApprovalStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *         &lt;element name="ReleaseStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ReleaseStatusCode" minOccurs="0"/>
 *         &lt;element name="ReleaseStatusName" type="{http://sap.com/xi/AP/Common/GDT}Name" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseItemStatus", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "consistencyStatusCode",
    "consistencyStatusName",
    "productAvailabilityConfirmationStatusCode",
    "productAvailabilityConfirmationStatusName",
    "fulfilmentDataCompletenessStatusCode",
    "fulfilmentDataCompletenessStatusName",
    "invoicingDataCompletenessStatusCode",
    "invoicingDataCompletenessStatusName",
    "pricingDataCompletenessStatusCode",
    "pricingDataCompletenessStatusName",
    "generalDataCompletenessStatusCode",
    "generalDataCompletenessStatusName",
    "fulfilmentProcessingStatusCode",
    "fulfilmentProcessingStatusName",
    "invoiceProcessingStatusCode",
    "invoiceProcessingStatusName",
    "customerOrderLifeCycleStatusCode",
    "customerOrderLifeCycleStatusName",
    "cancellationStatusCode",
    "cancellationStatusName",
    "planningReleaseStatusCode",
    "planningReleaseStatusName",
    "executionReleaseStatusCode",
    "executionReleaseStatusName",
    "approvalStatusCode",
    "approvalStatusName",
    "releaseStatusCode",
    "releaseStatusName"
})
public class SalesOrderByElementsResponseItemStatus {

    @XmlElement(name = "ConsistencyStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String consistencyStatusCode;
    @XmlElement(name = "ConsistencyStatusName")
    protected Name consistencyStatusName;
    @XmlElement(name = "ProductAvailabilityConfirmationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String productAvailabilityConfirmationStatusCode;
    @XmlElement(name = "ProductAvailabilityConfirmationStatusName")
    protected Name productAvailabilityConfirmationStatusName;
    @XmlElement(name = "FulfilmentDataCompletenessStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fulfilmentDataCompletenessStatusCode;
    @XmlElement(name = "FulfilmentDataCompletenessStatusName")
    protected Name fulfilmentDataCompletenessStatusName;
    @XmlElement(name = "InvoicingDataCompletenessStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String invoicingDataCompletenessStatusCode;
    @XmlElement(name = "InvoicingDataCompletenessStatusName")
    protected Name invoicingDataCompletenessStatusName;
    @XmlElement(name = "PricingDataCompletenessStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String pricingDataCompletenessStatusCode;
    @XmlElement(name = "PricingDataCompletenessStatusName")
    protected Name pricingDataCompletenessStatusName;
    @XmlElement(name = "GeneralDataCompletenessStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String generalDataCompletenessStatusCode;
    @XmlElement(name = "GeneralDataCompletenessStatusName")
    protected Name generalDataCompletenessStatusName;
    @XmlElement(name = "FulfilmentProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fulfilmentProcessingStatusCode;
    @XmlElement(name = "FulfilmentProcessingStatusName")
    protected Name fulfilmentProcessingStatusName;
    @XmlElement(name = "InvoiceProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String invoiceProcessingStatusCode;
    @XmlElement(name = "InvoiceProcessingStatusName")
    protected Name invoiceProcessingStatusName;
    @XmlElement(name = "CustomerOrderLifeCycleStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String customerOrderLifeCycleStatusCode;
    @XmlElement(name = "CustomerOrderLifeCycleStatusName")
    protected Name customerOrderLifeCycleStatusName;
    @XmlElement(name = "CancellationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String cancellationStatusCode;
    @XmlElement(name = "CancellationStatusName")
    protected Name cancellationStatusName;
    @XmlElement(name = "PlanningReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String planningReleaseStatusCode;
    @XmlElement(name = "PlanningReleaseStatusName")
    protected Name planningReleaseStatusName;
    @XmlElement(name = "ExecutionReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String executionReleaseStatusCode;
    @XmlElement(name = "ExecutionReleaseStatusName")
    protected Name executionReleaseStatusName;
    @XmlElement(name = "ApprovalStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String approvalStatusCode;
    @XmlElement(name = "ApprovalStatusName")
    protected Name approvalStatusName;
    @XmlElement(name = "ReleaseStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String releaseStatusCode;
    @XmlElement(name = "ReleaseStatusName")
    protected Name releaseStatusName;

    /**
     * Gets the value of the consistencyStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsistencyStatusCode() {
        return consistencyStatusCode;
    }

    /**
     * Sets the value of the consistencyStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsistencyStatusCode(String value) {
        this.consistencyStatusCode = value;
    }

    /**
     * Gets the value of the consistencyStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getConsistencyStatusName() {
        return consistencyStatusName;
    }

    /**
     * Sets the value of the consistencyStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setConsistencyStatusName(Name value) {
        this.consistencyStatusName = value;
    }

    /**
     * Gets the value of the productAvailabilityConfirmationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductAvailabilityConfirmationStatusCode() {
        return productAvailabilityConfirmationStatusCode;
    }

    /**
     * Sets the value of the productAvailabilityConfirmationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductAvailabilityConfirmationStatusCode(String value) {
        this.productAvailabilityConfirmationStatusCode = value;
    }

    /**
     * Gets the value of the productAvailabilityConfirmationStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getProductAvailabilityConfirmationStatusName() {
        return productAvailabilityConfirmationStatusName;
    }

    /**
     * Sets the value of the productAvailabilityConfirmationStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setProductAvailabilityConfirmationStatusName(Name value) {
        this.productAvailabilityConfirmationStatusName = value;
    }

    /**
     * Gets the value of the fulfilmentDataCompletenessStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfilmentDataCompletenessStatusCode() {
        return fulfilmentDataCompletenessStatusCode;
    }

    /**
     * Sets the value of the fulfilmentDataCompletenessStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfilmentDataCompletenessStatusCode(String value) {
        this.fulfilmentDataCompletenessStatusCode = value;
    }

    /**
     * Gets the value of the fulfilmentDataCompletenessStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getFulfilmentDataCompletenessStatusName() {
        return fulfilmentDataCompletenessStatusName;
    }

    /**
     * Sets the value of the fulfilmentDataCompletenessStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setFulfilmentDataCompletenessStatusName(Name value) {
        this.fulfilmentDataCompletenessStatusName = value;
    }

    /**
     * Gets the value of the invoicingDataCompletenessStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingDataCompletenessStatusCode() {
        return invoicingDataCompletenessStatusCode;
    }

    /**
     * Sets the value of the invoicingDataCompletenessStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingDataCompletenessStatusCode(String value) {
        this.invoicingDataCompletenessStatusCode = value;
    }

    /**
     * Gets the value of the invoicingDataCompletenessStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getInvoicingDataCompletenessStatusName() {
        return invoicingDataCompletenessStatusName;
    }

    /**
     * Sets the value of the invoicingDataCompletenessStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setInvoicingDataCompletenessStatusName(Name value) {
        this.invoicingDataCompletenessStatusName = value;
    }

    /**
     * Gets the value of the pricingDataCompletenessStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingDataCompletenessStatusCode() {
        return pricingDataCompletenessStatusCode;
    }

    /**
     * Sets the value of the pricingDataCompletenessStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingDataCompletenessStatusCode(String value) {
        this.pricingDataCompletenessStatusCode = value;
    }

    /**
     * Gets the value of the pricingDataCompletenessStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getPricingDataCompletenessStatusName() {
        return pricingDataCompletenessStatusName;
    }

    /**
     * Sets the value of the pricingDataCompletenessStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setPricingDataCompletenessStatusName(Name value) {
        this.pricingDataCompletenessStatusName = value;
    }

    /**
     * Gets the value of the generalDataCompletenessStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralDataCompletenessStatusCode() {
        return generalDataCompletenessStatusCode;
    }

    /**
     * Sets the value of the generalDataCompletenessStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralDataCompletenessStatusCode(String value) {
        this.generalDataCompletenessStatusCode = value;
    }

    /**
     * Gets the value of the generalDataCompletenessStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getGeneralDataCompletenessStatusName() {
        return generalDataCompletenessStatusName;
    }

    /**
     * Sets the value of the generalDataCompletenessStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setGeneralDataCompletenessStatusName(Name value) {
        this.generalDataCompletenessStatusName = value;
    }

    /**
     * Gets the value of the fulfilmentProcessingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfilmentProcessingStatusCode() {
        return fulfilmentProcessingStatusCode;
    }

    /**
     * Sets the value of the fulfilmentProcessingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfilmentProcessingStatusCode(String value) {
        this.fulfilmentProcessingStatusCode = value;
    }

    /**
     * Gets the value of the fulfilmentProcessingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getFulfilmentProcessingStatusName() {
        return fulfilmentProcessingStatusName;
    }

    /**
     * Sets the value of the fulfilmentProcessingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setFulfilmentProcessingStatusName(Name value) {
        this.fulfilmentProcessingStatusName = value;
    }

    /**
     * Gets the value of the invoiceProcessingStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceProcessingStatusCode() {
        return invoiceProcessingStatusCode;
    }

    /**
     * Sets the value of the invoiceProcessingStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceProcessingStatusCode(String value) {
        this.invoiceProcessingStatusCode = value;
    }

    /**
     * Gets the value of the invoiceProcessingStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getInvoiceProcessingStatusName() {
        return invoiceProcessingStatusName;
    }

    /**
     * Sets the value of the invoiceProcessingStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setInvoiceProcessingStatusName(Name value) {
        this.invoiceProcessingStatusName = value;
    }

    /**
     * Gets the value of the customerOrderLifeCycleStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerOrderLifeCycleStatusCode() {
        return customerOrderLifeCycleStatusCode;
    }

    /**
     * Sets the value of the customerOrderLifeCycleStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerOrderLifeCycleStatusCode(String value) {
        this.customerOrderLifeCycleStatusCode = value;
    }

    /**
     * Gets the value of the customerOrderLifeCycleStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getCustomerOrderLifeCycleStatusName() {
        return customerOrderLifeCycleStatusName;
    }

    /**
     * Sets the value of the customerOrderLifeCycleStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setCustomerOrderLifeCycleStatusName(Name value) {
        this.customerOrderLifeCycleStatusName = value;
    }

    /**
     * Gets the value of the cancellationStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationStatusCode() {
        return cancellationStatusCode;
    }

    /**
     * Sets the value of the cancellationStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationStatusCode(String value) {
        this.cancellationStatusCode = value;
    }

    /**
     * Gets the value of the cancellationStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getCancellationStatusName() {
        return cancellationStatusName;
    }

    /**
     * Sets the value of the cancellationStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setCancellationStatusName(Name value) {
        this.cancellationStatusName = value;
    }

    /**
     * Gets the value of the planningReleaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanningReleaseStatusCode() {
        return planningReleaseStatusCode;
    }

    /**
     * Sets the value of the planningReleaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanningReleaseStatusCode(String value) {
        this.planningReleaseStatusCode = value;
    }

    /**
     * Gets the value of the planningReleaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getPlanningReleaseStatusName() {
        return planningReleaseStatusName;
    }

    /**
     * Sets the value of the planningReleaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setPlanningReleaseStatusName(Name value) {
        this.planningReleaseStatusName = value;
    }

    /**
     * Gets the value of the executionReleaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecutionReleaseStatusCode() {
        return executionReleaseStatusCode;
    }

    /**
     * Sets the value of the executionReleaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecutionReleaseStatusCode(String value) {
        this.executionReleaseStatusCode = value;
    }

    /**
     * Gets the value of the executionReleaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getExecutionReleaseStatusName() {
        return executionReleaseStatusName;
    }

    /**
     * Sets the value of the executionReleaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setExecutionReleaseStatusName(Name value) {
        this.executionReleaseStatusName = value;
    }

    /**
     * Gets the value of the approvalStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalStatusCode() {
        return approvalStatusCode;
    }

    /**
     * Sets the value of the approvalStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalStatusCode(String value) {
        this.approvalStatusCode = value;
    }

    /**
     * Gets the value of the approvalStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getApprovalStatusName() {
        return approvalStatusName;
    }

    /**
     * Sets the value of the approvalStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setApprovalStatusName(Name value) {
        this.approvalStatusName = value;
    }

    /**
     * Gets the value of the releaseStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseStatusCode() {
        return releaseStatusCode;
    }

    /**
     * Sets the value of the releaseStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseStatusCode(String value) {
        this.releaseStatusCode = value;
    }

    /**
     * Gets the value of the releaseStatusName property.
     * 
     * @return
     *     possible object is
     *     {@link Name }
     *     
     */
    public Name getReleaseStatusName() {
        return releaseStatusName;
    }

    /**
     * Sets the value of the releaseStatusName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Name }
     *     
     */
    public void setReleaseStatusName(Name value) {
        this.releaseStatusName = value;
    }

}
