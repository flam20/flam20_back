
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponsePriceAndTaxCalculation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponsePriceAndTaxCalculation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MainDiscount" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="MainPrice" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="MainSurcharge" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="MainTotal" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="PriceComponent" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProductTaxDetails" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TaxationTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms" minOccurs="0"/>
 *         &lt;element name="WithholdingTaxDetails" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponsePriceAndTaxCalculation", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "mainDiscount",
    "mainPrice",
    "mainSurcharge",
    "mainTotal",
    "priceComponent",
    "productTaxDetails",
    "taxationTerms",
    "withholdingTaxDetails"
})
public class SalesOrderByElementsResponsePriceAndTaxCalculation {

    @XmlElement(name = "MainDiscount")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent mainDiscount;
    @XmlElement(name = "MainPrice")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent mainPrice;
    @XmlElement(name = "MainSurcharge")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent mainSurcharge;
    @XmlElement(name = "MainTotal")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent mainTotal;
    @XmlElement(name = "PriceComponent")
    protected List<SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent> priceComponent;
    @XmlElement(name = "ProductTaxDetails")
    protected List<SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails> productTaxDetails;
    @XmlElement(name = "TaxationTerms")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms taxationTerms;
    @XmlElement(name = "WithholdingTaxDetails")
    protected List<SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails> withholdingTaxDetails;

    /**
     * Gets the value of the mainDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent getMainDiscount() {
        return mainDiscount;
    }

    /**
     * Sets the value of the mainDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public void setMainDiscount(SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent value) {
        this.mainDiscount = value;
    }

    /**
     * Gets the value of the mainPrice property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent getMainPrice() {
        return mainPrice;
    }

    /**
     * Sets the value of the mainPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public void setMainPrice(SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent value) {
        this.mainPrice = value;
    }

    /**
     * Gets the value of the mainSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent getMainSurcharge() {
        return mainSurcharge;
    }

    /**
     * Sets the value of the mainSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public void setMainSurcharge(SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent value) {
        this.mainSurcharge = value;
    }

    /**
     * Gets the value of the mainTotal property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent getMainTotal() {
        return mainTotal;
    }

    /**
     * Sets the value of the mainTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent }
     *     
     */
    public void setMainTotal(SalesOrderByElementsResponsePriceAndTaxCalculationMainPriceComponent value) {
        this.mainTotal = value;
    }

    /**
     * Gets the value of the priceComponent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the priceComponent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPriceComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent> getPriceComponent() {
        if (priceComponent == null) {
            priceComponent = new ArrayList<SalesOrderByElementsResponsePriceAndTaxCalculationPriceComponent>();
        }
        return this.priceComponent;
    }

    /**
     * Gets the value of the productTaxDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productTaxDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductTaxDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails> getProductTaxDetails() {
        if (productTaxDetails == null) {
            productTaxDetails = new ArrayList<SalesOrderByElementsResponsePriceAndTaxCalculationProductTaxDetails>();
        }
        return this.productTaxDetails;
    }

    /**
     * Gets the value of the taxationTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms getTaxationTerms() {
        return taxationTerms;
    }

    /**
     * Sets the value of the taxationTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms }
     *     
     */
    public void setTaxationTerms(SalesOrderByElementsResponsePriceAndTaxCalculationTaxationTerms value) {
        this.taxationTerms = value;
    }

    /**
     * Gets the value of the withholdingTaxDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the withholdingTaxDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWithholdingTaxDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails> getWithholdingTaxDetails() {
        if (withholdingTaxDetails == null) {
            withholdingTaxDetails = new ArrayList<SalesOrderByElementsResponsePriceAndTaxCalculationWithholdingTaxDetails>();
        }
        return this.withholdingTaxDetails;
    }

}
