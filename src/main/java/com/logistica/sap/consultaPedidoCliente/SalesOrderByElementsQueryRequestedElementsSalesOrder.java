
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsQueryRequestedElementsSalesOrder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQueryRequestedElementsSalesOrder">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="itemTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="salesAndServiceBusinessAreaTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="billToPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="accountPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="payerPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="productRecipientPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="salesPartnerPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="freightForwarderPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="employeeResponsiblePartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="sellerPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="salesUnitPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="serviceExecutionTeamPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="servicePerformerPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="salesEmployerPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="billFromPartyTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="requestedFulfillmentPeriodPeriodTerms" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="deliveryTermsTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="pricingTermsTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="salesTermsTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="invoiceTermsTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="cashDiscountTermsTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="paymentControlTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="priceAndTaxCalculationTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="textCollectionTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *       &lt;attribute name="attachmentFolderTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQueryRequestedElementsSalesOrder", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "value"
})
public class SalesOrderByElementsQueryRequestedElementsSalesOrder {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "itemTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemTransmissionRequestCode;
    @XmlAttribute(name = "salesAndServiceBusinessAreaTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesAndServiceBusinessAreaTransmissionRequestCode;
    @XmlAttribute(name = "billToPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String billToPartyTransmissionRequestCode;
    @XmlAttribute(name = "accountPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String accountPartyTransmissionRequestCode;
    @XmlAttribute(name = "payerPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String payerPartyTransmissionRequestCode;
    @XmlAttribute(name = "productRecipientPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String productRecipientPartyTransmissionRequestCode;
    @XmlAttribute(name = "salesPartnerPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesPartnerPartyTransmissionRequestCode;
    @XmlAttribute(name = "freightForwarderPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String freightForwarderPartyTransmissionRequestCode;
    @XmlAttribute(name = "employeeResponsiblePartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String employeeResponsiblePartyTransmissionRequestCode;
    @XmlAttribute(name = "sellerPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String sellerPartyTransmissionRequestCode;
    @XmlAttribute(name = "salesUnitPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesUnitPartyTransmissionRequestCode;
    @XmlAttribute(name = "serviceExecutionTeamPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String serviceExecutionTeamPartyTransmissionRequestCode;
    @XmlAttribute(name = "servicePerformerPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String servicePerformerPartyTransmissionRequestCode;
    @XmlAttribute(name = "salesEmployerPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesEmployerPartyTransmissionRequestCode;
    @XmlAttribute(name = "billFromPartyTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String billFromPartyTransmissionRequestCode;
    @XmlAttribute(name = "requestedFulfillmentPeriodPeriodTerms")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String requestedFulfillmentPeriodPeriodTerms;
    @XmlAttribute(name = "deliveryTermsTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String deliveryTermsTransmissionRequestCode;
    @XmlAttribute(name = "pricingTermsTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String pricingTermsTransmissionRequestCode;
    @XmlAttribute(name = "salesTermsTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesTermsTransmissionRequestCode;
    @XmlAttribute(name = "invoiceTermsTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String invoiceTermsTransmissionRequestCode;
    @XmlAttribute(name = "cashDiscountTermsTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String cashDiscountTermsTransmissionRequestCode;
    @XmlAttribute(name = "paymentControlTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String paymentControlTransmissionRequestCode;
    @XmlAttribute(name = "priceAndTaxCalculationTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String priceAndTaxCalculationTransmissionRequestCode;
    @XmlAttribute(name = "textCollectionTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String textCollectionTransmissionRequestCode;
    @XmlAttribute(name = "attachmentFolderTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String attachmentFolderTransmissionRequestCode;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the itemTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTransmissionRequestCode() {
        return itemTransmissionRequestCode;
    }

    /**
     * Sets the value of the itemTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTransmissionRequestCode(String value) {
        this.itemTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the salesAndServiceBusinessAreaTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesAndServiceBusinessAreaTransmissionRequestCode() {
        return salesAndServiceBusinessAreaTransmissionRequestCode;
    }

    /**
     * Sets the value of the salesAndServiceBusinessAreaTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesAndServiceBusinessAreaTransmissionRequestCode(String value) {
        this.salesAndServiceBusinessAreaTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the billToPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillToPartyTransmissionRequestCode() {
        return billToPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the billToPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillToPartyTransmissionRequestCode(String value) {
        this.billToPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the accountPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountPartyTransmissionRequestCode() {
        return accountPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the accountPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountPartyTransmissionRequestCode(String value) {
        this.accountPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the payerPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayerPartyTransmissionRequestCode() {
        return payerPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the payerPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayerPartyTransmissionRequestCode(String value) {
        this.payerPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the productRecipientPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductRecipientPartyTransmissionRequestCode() {
        return productRecipientPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the productRecipientPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductRecipientPartyTransmissionRequestCode(String value) {
        this.productRecipientPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the salesPartnerPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesPartnerPartyTransmissionRequestCode() {
        return salesPartnerPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the salesPartnerPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesPartnerPartyTransmissionRequestCode(String value) {
        this.salesPartnerPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the freightForwarderPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightForwarderPartyTransmissionRequestCode() {
        return freightForwarderPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the freightForwarderPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightForwarderPartyTransmissionRequestCode(String value) {
        this.freightForwarderPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the employeeResponsiblePartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeResponsiblePartyTransmissionRequestCode() {
        return employeeResponsiblePartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the employeeResponsiblePartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeResponsiblePartyTransmissionRequestCode(String value) {
        this.employeeResponsiblePartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the sellerPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerPartyTransmissionRequestCode() {
        return sellerPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the sellerPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerPartyTransmissionRequestCode(String value) {
        this.sellerPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the salesUnitPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesUnitPartyTransmissionRequestCode() {
        return salesUnitPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the salesUnitPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesUnitPartyTransmissionRequestCode(String value) {
        this.salesUnitPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the serviceExecutionTeamPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceExecutionTeamPartyTransmissionRequestCode() {
        return serviceExecutionTeamPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the serviceExecutionTeamPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceExecutionTeamPartyTransmissionRequestCode(String value) {
        this.serviceExecutionTeamPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the servicePerformerPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicePerformerPartyTransmissionRequestCode() {
        return servicePerformerPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the servicePerformerPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicePerformerPartyTransmissionRequestCode(String value) {
        this.servicePerformerPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the salesEmployerPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesEmployerPartyTransmissionRequestCode() {
        return salesEmployerPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the salesEmployerPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesEmployerPartyTransmissionRequestCode(String value) {
        this.salesEmployerPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the billFromPartyTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillFromPartyTransmissionRequestCode() {
        return billFromPartyTransmissionRequestCode;
    }

    /**
     * Sets the value of the billFromPartyTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillFromPartyTransmissionRequestCode(String value) {
        this.billFromPartyTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the requestedFulfillmentPeriodPeriodTerms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestedFulfillmentPeriodPeriodTerms() {
        return requestedFulfillmentPeriodPeriodTerms;
    }

    /**
     * Sets the value of the requestedFulfillmentPeriodPeriodTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestedFulfillmentPeriodPeriodTerms(String value) {
        this.requestedFulfillmentPeriodPeriodTerms = value;
    }

    /**
     * Gets the value of the deliveryTermsTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryTermsTransmissionRequestCode() {
        return deliveryTermsTransmissionRequestCode;
    }

    /**
     * Sets the value of the deliveryTermsTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryTermsTransmissionRequestCode(String value) {
        this.deliveryTermsTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the pricingTermsTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPricingTermsTransmissionRequestCode() {
        return pricingTermsTransmissionRequestCode;
    }

    /**
     * Sets the value of the pricingTermsTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPricingTermsTransmissionRequestCode(String value) {
        this.pricingTermsTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the salesTermsTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesTermsTransmissionRequestCode() {
        return salesTermsTransmissionRequestCode;
    }

    /**
     * Sets the value of the salesTermsTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesTermsTransmissionRequestCode(String value) {
        this.salesTermsTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the invoiceTermsTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceTermsTransmissionRequestCode() {
        return invoiceTermsTransmissionRequestCode;
    }

    /**
     * Sets the value of the invoiceTermsTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceTermsTransmissionRequestCode(String value) {
        this.invoiceTermsTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the cashDiscountTermsTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashDiscountTermsTransmissionRequestCode() {
        return cashDiscountTermsTransmissionRequestCode;
    }

    /**
     * Sets the value of the cashDiscountTermsTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashDiscountTermsTransmissionRequestCode(String value) {
        this.cashDiscountTermsTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the paymentControlTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentControlTransmissionRequestCode() {
        return paymentControlTransmissionRequestCode;
    }

    /**
     * Sets the value of the paymentControlTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentControlTransmissionRequestCode(String value) {
        this.paymentControlTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the priceAndTaxCalculationTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceAndTaxCalculationTransmissionRequestCode() {
        return priceAndTaxCalculationTransmissionRequestCode;
    }

    /**
     * Sets the value of the priceAndTaxCalculationTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceAndTaxCalculationTransmissionRequestCode(String value) {
        this.priceAndTaxCalculationTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the textCollectionTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextCollectionTransmissionRequestCode() {
        return textCollectionTransmissionRequestCode;
    }

    /**
     * Sets the value of the textCollectionTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextCollectionTransmissionRequestCode(String value) {
        this.textCollectionTransmissionRequestCode = value;
    }

    /**
     * Gets the value of the attachmentFolderTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttachmentFolderTransmissionRequestCode() {
        return attachmentFolderTransmissionRequestCode;
    }

    /**
     * Sets the value of the attachmentFolderTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttachmentFolderTransmissionRequestCode(String value) {
        this.attachmentFolderTransmissionRequestCode = value;
    }

}
