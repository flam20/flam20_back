
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsQuerySelectionByDistributionChannelCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQuerySelectionByDistributionChannelCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InclusionExclusionCode" type="{http://sap.com/xi/AP/Common/GDT}InclusionExclusionCode" minOccurs="0"/>
 *         &lt;element name="IntervalBoundaryTypeCode" type="{http://sap.com/xi/AP/Common/GDT}IntervalBoundaryTypeCode" minOccurs="0"/>
 *         &lt;element name="LowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode" type="{http://sap.com/xi/AP/Common/GDT}DistributionChannelCode"/>
 *         &lt;element name="UpperBoundarySalesAndServiceBusinessAreaDistributionChannelCode" type="{http://sap.com/xi/AP/Common/GDT}DistributionChannelCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQuerySelectionByDistributionChannelCode", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "inclusionExclusionCode",
    "intervalBoundaryTypeCode",
    "lowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode",
    "upperBoundarySalesAndServiceBusinessAreaDistributionChannelCode"
})
public class SalesOrderByElementsQuerySelectionByDistributionChannelCode {

    @XmlElement(name = "InclusionExclusionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String inclusionExclusionCode;
    @XmlElement(name = "IntervalBoundaryTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String intervalBoundaryTypeCode;
    @XmlElement(name = "LowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode", required = true)
    protected DistributionChannelCode lowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode;
    @XmlElement(name = "UpperBoundarySalesAndServiceBusinessAreaDistributionChannelCode")
    protected DistributionChannelCode upperBoundarySalesAndServiceBusinessAreaDistributionChannelCode;

    /**
     * Gets the value of the inclusionExclusionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInclusionExclusionCode() {
        return inclusionExclusionCode;
    }

    /**
     * Sets the value of the inclusionExclusionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInclusionExclusionCode(String value) {
        this.inclusionExclusionCode = value;
    }

    /**
     * Gets the value of the intervalBoundaryTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntervalBoundaryTypeCode() {
        return intervalBoundaryTypeCode;
    }

    /**
     * Sets the value of the intervalBoundaryTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntervalBoundaryTypeCode(String value) {
        this.intervalBoundaryTypeCode = value;
    }

    /**
     * Gets the value of the lowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode property.
     * 
     * @return
     *     possible object is
     *     {@link DistributionChannelCode }
     *     
     */
    public DistributionChannelCode getLowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode() {
        return lowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode;
    }

    /**
     * Sets the value of the lowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistributionChannelCode }
     *     
     */
    public void setLowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode(DistributionChannelCode value) {
        this.lowerBoundarySalesAndServiceBusinessAreaDistributionChannelCode = value;
    }

    /**
     * Gets the value of the upperBoundarySalesAndServiceBusinessAreaDistributionChannelCode property.
     * 
     * @return
     *     possible object is
     *     {@link DistributionChannelCode }
     *     
     */
    public DistributionChannelCode getUpperBoundarySalesAndServiceBusinessAreaDistributionChannelCode() {
        return upperBoundarySalesAndServiceBusinessAreaDistributionChannelCode;
    }

    /**
     * Sets the value of the upperBoundarySalesAndServiceBusinessAreaDistributionChannelCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistributionChannelCode }
     *     
     */
    public void setUpperBoundarySalesAndServiceBusinessAreaDistributionChannelCode(DistributionChannelCode value) {
        this.upperBoundarySalesAndServiceBusinessAreaDistributionChannelCode = value;
    }

}
