
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsResponsePriceAndTaxCalculationItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponsePriceAndTaxCalculationItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://sap.com/xi/AP/FO/PriceAndTax/Global}PriceAndTaxCalculationItemStatus" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://sap.com/xi/AP/Common/GDT}CountryCode" minOccurs="0"/>
 *         &lt;element name="TaxationCharacteristicsCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTaxationCharacteristicsCode" minOccurs="0"/>
 *         &lt;element name="TaxationCharacteristicsDeterminationMethodCode" type="{http://sap.com/xi/AP/Common/GDT}TaxationCharacteristicsDeterminationMethodCode" minOccurs="0"/>
 *         &lt;element name="TaxJurisdictionCode" type="{http://sap.com/xi/AP/Common/GDT}TaxJurisdictionCode" minOccurs="0"/>
 *         &lt;element name="TaxRegionCode" type="{http://sap.com/xi/AP/Common/GDT}RegionCode" minOccurs="0"/>
 *         &lt;element name="WithholdingTaxationCharacteristicsCode" type="{http://sap.com/xi/AP/Common/GDT}WithholdingTaxationCharacteristicsCode" minOccurs="0"/>
 *         &lt;element name="WithholdingTaxationCharacteristicsDeterminationMethodCode" type="{http://sap.com/xi/AP/Common/GDT}TaxationCharacteristicsDeterminationMethodCode" minOccurs="0"/>
 *         &lt;element name="ItemMainDiscount" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="ItemMainPrice" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="ItemMainSurcharge" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="ItemMainTotal" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent" minOccurs="0"/>
 *         &lt;element name="ItemPriceComponent" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemProductTaxDetails" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemTaxationTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms" minOccurs="0"/>
 *         &lt;element name="ItemWithholdingTaxDetails" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponsePriceAndTaxCalculationItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "status",
    "countryCode",
    "taxationCharacteristicsCode",
    "taxationCharacteristicsDeterminationMethodCode",
    "taxJurisdictionCode",
    "taxRegionCode",
    "withholdingTaxationCharacteristicsCode",
    "withholdingTaxationCharacteristicsDeterminationMethodCode",
    "itemMainDiscount",
    "itemMainPrice",
    "itemMainSurcharge",
    "itemMainTotal",
    "itemPriceComponent",
    "itemProductTaxDetails",
    "itemTaxationTerms",
    "itemWithholdingTaxDetails"
})
public class SalesOrderByElementsResponsePriceAndTaxCalculationItem {

    @XmlElement(name = "Status")
    protected PriceAndTaxCalculationItemStatus status;
    @XmlElement(name = "CountryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String countryCode;
    @XmlElement(name = "TaxationCharacteristicsCode")
    protected ProductTaxationCharacteristicsCode taxationCharacteristicsCode;
    @XmlElement(name = "TaxationCharacteristicsDeterminationMethodCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String taxationCharacteristicsDeterminationMethodCode;
    @XmlElement(name = "TaxJurisdictionCode")
    protected TaxJurisdictionCode taxJurisdictionCode;
    @XmlElement(name = "TaxRegionCode")
    protected RegionCode taxRegionCode;
    @XmlElement(name = "WithholdingTaxationCharacteristicsCode")
    protected WithholdingTaxationCharacteristicsCode withholdingTaxationCharacteristicsCode;
    @XmlElement(name = "WithholdingTaxationCharacteristicsDeterminationMethodCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String withholdingTaxationCharacteristicsDeterminationMethodCode;
    @XmlElement(name = "ItemMainDiscount")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent itemMainDiscount;
    @XmlElement(name = "ItemMainPrice")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent itemMainPrice;
    @XmlElement(name = "ItemMainSurcharge")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent itemMainSurcharge;
    @XmlElement(name = "ItemMainTotal")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent itemMainTotal;
    @XmlElement(name = "ItemPriceComponent")
    protected List<SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent> itemPriceComponent;
    @XmlElement(name = "ItemProductTaxDetails")
    protected List<SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails> itemProductTaxDetails;
    @XmlElement(name = "ItemTaxationTerms")
    protected SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms itemTaxationTerms;
    @XmlElement(name = "ItemWithholdingTaxDetails")
    protected List<SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails> itemWithholdingTaxDetails;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link PriceAndTaxCalculationItemStatus }
     *     
     */
    public PriceAndTaxCalculationItemStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceAndTaxCalculationItemStatus }
     *     
     */
    public void setStatus(PriceAndTaxCalculationItemStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the taxationCharacteristicsCode property.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxationCharacteristicsCode }
     *     
     */
    public ProductTaxationCharacteristicsCode getTaxationCharacteristicsCode() {
        return taxationCharacteristicsCode;
    }

    /**
     * Sets the value of the taxationCharacteristicsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxationCharacteristicsCode }
     *     
     */
    public void setTaxationCharacteristicsCode(ProductTaxationCharacteristicsCode value) {
        this.taxationCharacteristicsCode = value;
    }

    /**
     * Gets the value of the taxationCharacteristicsDeterminationMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxationCharacteristicsDeterminationMethodCode() {
        return taxationCharacteristicsDeterminationMethodCode;
    }

    /**
     * Sets the value of the taxationCharacteristicsDeterminationMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxationCharacteristicsDeterminationMethodCode(String value) {
        this.taxationCharacteristicsDeterminationMethodCode = value;
    }

    /**
     * Gets the value of the taxJurisdictionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public TaxJurisdictionCode getTaxJurisdictionCode() {
        return taxJurisdictionCode;
    }

    /**
     * Sets the value of the taxJurisdictionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public void setTaxJurisdictionCode(TaxJurisdictionCode value) {
        this.taxJurisdictionCode = value;
    }

    /**
     * Gets the value of the taxRegionCode property.
     * 
     * @return
     *     possible object is
     *     {@link RegionCode }
     *     
     */
    public RegionCode getTaxRegionCode() {
        return taxRegionCode;
    }

    /**
     * Sets the value of the taxRegionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionCode }
     *     
     */
    public void setTaxRegionCode(RegionCode value) {
        this.taxRegionCode = value;
    }

    /**
     * Gets the value of the withholdingTaxationCharacteristicsCode property.
     * 
     * @return
     *     possible object is
     *     {@link WithholdingTaxationCharacteristicsCode }
     *     
     */
    public WithholdingTaxationCharacteristicsCode getWithholdingTaxationCharacteristicsCode() {
        return withholdingTaxationCharacteristicsCode;
    }

    /**
     * Sets the value of the withholdingTaxationCharacteristicsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link WithholdingTaxationCharacteristicsCode }
     *     
     */
    public void setWithholdingTaxationCharacteristicsCode(WithholdingTaxationCharacteristicsCode value) {
        this.withholdingTaxationCharacteristicsCode = value;
    }

    /**
     * Gets the value of the withholdingTaxationCharacteristicsDeterminationMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWithholdingTaxationCharacteristicsDeterminationMethodCode() {
        return withholdingTaxationCharacteristicsDeterminationMethodCode;
    }

    /**
     * Sets the value of the withholdingTaxationCharacteristicsDeterminationMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWithholdingTaxationCharacteristicsDeterminationMethodCode(String value) {
        this.withholdingTaxationCharacteristicsDeterminationMethodCode = value;
    }

    /**
     * Gets the value of the itemMainDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent getItemMainDiscount() {
        return itemMainDiscount;
    }

    /**
     * Sets the value of the itemMainDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public void setItemMainDiscount(SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent value) {
        this.itemMainDiscount = value;
    }

    /**
     * Gets the value of the itemMainPrice property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent getItemMainPrice() {
        return itemMainPrice;
    }

    /**
     * Sets the value of the itemMainPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public void setItemMainPrice(SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent value) {
        this.itemMainPrice = value;
    }

    /**
     * Gets the value of the itemMainSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent getItemMainSurcharge() {
        return itemMainSurcharge;
    }

    /**
     * Sets the value of the itemMainSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public void setItemMainSurcharge(SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent value) {
        this.itemMainSurcharge = value;
    }

    /**
     * Gets the value of the itemMainTotal property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent getItemMainTotal() {
        return itemMainTotal;
    }

    /**
     * Sets the value of the itemMainTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent }
     *     
     */
    public void setItemMainTotal(SalesOrderByElementsResponsePriceAndTaxCalculationItemMainPriceComponent value) {
        this.itemMainTotal = value;
    }

    /**
     * Gets the value of the itemPriceComponent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemPriceComponent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemPriceComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent> getItemPriceComponent() {
        if (itemPriceComponent == null) {
            itemPriceComponent = new ArrayList<SalesOrderByElementsResponsePriceAndTaxCalculationItemPriceComponent>();
        }
        return this.itemPriceComponent;
    }

    /**
     * Gets the value of the itemProductTaxDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemProductTaxDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemProductTaxDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails> getItemProductTaxDetails() {
        if (itemProductTaxDetails == null) {
            itemProductTaxDetails = new ArrayList<SalesOrderByElementsResponsePriceAndTaxCalculationItemProductTaxDetails>();
        }
        return this.itemProductTaxDetails;
    }

    /**
     * Gets the value of the itemTaxationTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms }
     *     
     */
    public SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms getItemTaxationTerms() {
        return itemTaxationTerms;
    }

    /**
     * Sets the value of the itemTaxationTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms }
     *     
     */
    public void setItemTaxationTerms(SalesOrderByElementsResponsePriceAndTaxCalculationItemItemTaxationTerms value) {
        this.itemTaxationTerms = value;
    }

    /**
     * Gets the value of the itemWithholdingTaxDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemWithholdingTaxDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemWithholdingTaxDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails }
     * 
     * 
     */
    public List<SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails> getItemWithholdingTaxDetails() {
        if (itemWithholdingTaxDetails == null) {
            itemWithholdingTaxDetails = new ArrayList<SalesOrderByElementsResponsePriceAndTaxCalculationItemItemProductTaxDetails>();
        }
        return this.itemWithholdingTaxDetails;
    }

}
