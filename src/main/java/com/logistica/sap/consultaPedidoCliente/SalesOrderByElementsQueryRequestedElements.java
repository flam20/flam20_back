
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderByElementsQueryRequestedElements complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsQueryRequestedElements">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesOrder" type="{http://sap.com/xi/A1S/Global}SalesOrderByElementsQueryRequestedElementsSalesOrder" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="salesOrderTransmissionRequestCode" type="{http://sap.com/xi/AP/Common/GDT}TransmissionRequestCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsQueryRequestedElements", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "salesOrder"
})
public class SalesOrderByElementsQueryRequestedElements {

    @XmlElement(name = "SalesOrder")
    protected SalesOrderByElementsQueryRequestedElementsSalesOrder salesOrder;
    @XmlAttribute(name = "salesOrderTransmissionRequestCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String salesOrderTransmissionRequestCode;

    /**
     * Gets the value of the salesOrder property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderByElementsQueryRequestedElementsSalesOrder }
     *     
     */
    public SalesOrderByElementsQueryRequestedElementsSalesOrder getSalesOrder() {
        return salesOrder;
    }

    /**
     * Sets the value of the salesOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderByElementsQueryRequestedElementsSalesOrder }
     *     
     */
    public void setSalesOrder(SalesOrderByElementsQueryRequestedElementsSalesOrder value) {
        this.salesOrder = value;
    }

    /**
     * Gets the value of the salesOrderTransmissionRequestCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOrderTransmissionRequestCode() {
        return salesOrderTransmissionRequestCode;
    }

    /**
     * Sets the value of the salesOrderTransmissionRequestCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOrderTransmissionRequestCode(String value) {
        this.salesOrderTransmissionRequestCode = value;
    }

}
