
package com.logistica.sap.consultaPedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ExtSelectionByLongText complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExtSelectionByLongText">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SelectionByText" type="{http://sap.com/xi/AP/Common/Global}SelectionByName" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtSelectionByLongText", namespace = "http://sap.com/xi/Extensibility/Global", propOrder = {
    "selectionByText"
})
public class ExtSelectionByLongText {

    @XmlElement(name = "SelectionByText")
    protected List<SelectionByName> selectionByText;

    /**
     * Gets the value of the selectionByText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectionByText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectionByText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectionByName }
     * 
     * 
     */
    public List<SelectionByName> getSelectionByText() {
        if (selectionByText == null) {
            selectionByText = new ArrayList<SelectionByName>();
        }
        return this.selectionByText;
    }

}
