
package com.logistica.sap.consultaPedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderByElementsResponseSalesTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderByElementsResponseSalesTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CancellationReasonCode" type="{http://sap.com/xi/AP/Common/GDT}CancellationReasonCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderByElementsResponseSalesTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "cancellationReasonCode"
})
public class SalesOrderByElementsResponseSalesTerms {

    @XmlElement(name = "CancellationReasonCode")
    protected CancellationReasonCode cancellationReasonCode;

    /**
     * Gets the value of the cancellationReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link CancellationReasonCode }
     *     
     */
    public CancellationReasonCode getCancellationReasonCode() {
        return cancellationReasonCode;
    }

    /**
     * Sets the value of the cancellationReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancellationReasonCode }
     *     
     */
    public void setCancellationReasonCode(CancellationReasonCode value) {
        this.cancellationReasonCode = value;
    }

}
