
package com.logistica.sap.pedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SalesOrderMaintainRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderMaintainRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodeSenderTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="BuyerID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="PostingDate" type="{http://sap.com/xi/BASIS/Global}GLOBAL_DateTime" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://sap.com/xi/AP/Common/GDT}EXTENDED_Name" minOccurs="0"/>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="DataOriginTypeCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerTransactionDocumentDataOriginTypeCode" minOccurs="0"/>
 *         &lt;element name="FulfillmentBlockingReasonCode" type="{http://sap.com/xi/AP/Common/GDT}CustomerTransactionDocumentFulfilmentBlockingReasonCode" minOccurs="0"/>
 *         &lt;element name="ReleaseCustomerRequest" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ReleaseAllItemsToExecution" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FinishFulfilmentProcessingOfAllItems" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReference" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestBusinessTransactionDocumentReference" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SalesAndServiceBusinessArea" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestSalesAndServiceBusinessArea" minOccurs="0"/>
 *         &lt;element name="BillToParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyParty" minOccurs="0"/>
 *         &lt;element name="AccountParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyParty" minOccurs="0"/>
 *         &lt;element name="PayerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyParty" minOccurs="0"/>
 *         &lt;element name="ProductRecipientParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyParty" minOccurs="0"/>
 *         &lt;element name="SalesPartnerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyParty" minOccurs="0"/>
 *         &lt;element name="FreightForwarderParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyParty" minOccurs="0"/>
 *         &lt;element name="EmployeeResponsibleParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="SellerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="SalesUnitParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="ServiceExecutionTeamParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="ServicePerformerParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="SalesEmployeeParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="BillFromParty" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPartyIDParty" minOccurs="0"/>
 *         &lt;element name="RequestedFulfillmentPeriodPeriodTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms" minOccurs="0"/>
 *         &lt;element name="DeliveryTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="PricingTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPricingTerms" minOccurs="0"/>
 *         &lt;element name="SalesTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestSalesTerms" minOccurs="0"/>
 *         &lt;element name="InvoiceTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestInvoiceTerms" minOccurs="0"/>
 *         &lt;element name="Item" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestItem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CashDiscountTerms" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestCashDiscountTerms" minOccurs="0"/>
 *         &lt;element name="PaymentControl" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPaymentControl" minOccurs="0"/>
 *         &lt;element name="PriceAndTaxCalculation" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestPriceAndTaxCalculation" minOccurs="0"/>
 *         &lt;element name="TextCollection" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainRequestTextCollection" minOccurs="0"/>
 *         &lt;element name="AttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="ChangeStateID" type="{http://sap.com/xi/AP/Common/GDT}ChangeStateID" minOccurs="0"/>
 *         &lt;element name="CroatiaBusinessSpaceID" type="{http://sap.com/xi/AP/Globalization}GLOBusinessSpaceID" minOccurs="0"/>
 *         &lt;element name="CroatiaBillingMachineID" type="{http://sap.com/xi/AP/Globalization}HR_BillingMachineID" minOccurs="0"/>
 *         &lt;element name="CroatiaPaymentMethodCode" type="{http://sap.com/xi/AP/Globalization}CroatiaPaymentMethodCode" minOccurs="0"/>
 *         &lt;element name="CroatiaPartyTaxID" type="{http://sap.com/xi/AP/Common/GDT}PartyTaxID" minOccurs="0"/>
 *         &lt;element name="CroatiaParagonBillMark" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_Text" minOccurs="0"/>
 *         &lt;element name="CroatiaSpecialNote" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_Text" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EDA8FAEA65856C45420"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD7EDD1E10B8B9E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD7F92025588BA9"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD8028DAF7BCBF1"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BAD85709EE404D16"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA8C96C66C1731"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA912000AF573D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADA94FE5EEED73F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAA4BE34105787"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAA954C07CD789"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAAD7E5D87578B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAB2493F3A578E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAB74B8FB417F0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADABCC4119397F1"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAC13CA65997F4"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BADAEF0E77EFF8A3"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BDEA6C790A481AE8"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA81F9A91D6B1E48F6"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA81FB7C4147FD52A0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="itemListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" />
 *       &lt;attribute name="businessTransactionDocumentReferenceListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" />
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderMaintainRequest", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodeSenderTechnicalID",
    "id",
    "buyerID",
    "postingDate",
    "name",
    "uuid",
    "dataOriginTypeCode",
    "fulfillmentBlockingReasonCode",
    "releaseCustomerRequest",
    "releaseAllItemsToExecution",
    "finishFulfilmentProcessingOfAllItems",
    "businessTransactionDocumentReference",
    "salesAndServiceBusinessArea",
    "billToParty",
    "accountParty",
    "payerParty",
    "productRecipientParty",
    "salesPartnerParty",
    "freightForwarderParty",
    "employeeResponsibleParty",
    "sellerParty",
    "salesUnitParty",
    "serviceExecutionTeamParty",
    "servicePerformerParty",
    "salesEmployeeParty",
    "billFromParty",
    "requestedFulfillmentPeriodPeriodTerms",
    "deliveryTerms",
    "pricingTerms",
    "salesTerms",
    "invoiceTerms",
    "item",
    "cashDiscountTerms",
    "paymentControl",
    "priceAndTaxCalculation",
    "textCollection",
    "attachmentFolder",
    "changeStateID",
    "croatiaBusinessSpaceID",
    "croatiaBillingMachineID",
    "croatiaPaymentMethodCode",
    "croatiaPartyTaxID",
    "croatiaParagonBillMark",
    "croatiaSpecialNote",
    "servicioFLAM",
    "consignatarios",
    "ubicacinDestino",
    "ubicacionOrigen",
    "ubicacinCruce",
    "fechaCarga1",
    "fechaCruce1",
    "fechaDestino1",
    "fechadeAgenteAduanal1",
    "guaHouse",
    "guaMaster",
    "nmerodeequipodecarga",
    "pzsPBPC",
    "shipper",
    "tipodeequipo",
    "requierePOD",
    "ndeservicio1",
    "consignatarioDestino",
    "consignatarioCruce"
})
public class SalesOrderMaintainRequest {

    @XmlElement(name = "ObjectNodeSenderTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String objectNodeSenderTechnicalID;
    @XmlElement(name = "ID")
    protected BusinessTransactionDocumentID id;
    @XmlElement(name = "BuyerID")
    protected BusinessTransactionDocumentID buyerID;
    @XmlElement(name = "PostingDate")
    protected XMLGregorianCalendar postingDate;
    @XmlElement(name = "Name")
    protected EXTENDEDName name;
    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "DataOriginTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String dataOriginTypeCode;
    @XmlElement(name = "FulfillmentBlockingReasonCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String fulfillmentBlockingReasonCode;
    @XmlElement(name = "ReleaseCustomerRequest")
    protected Boolean releaseCustomerRequest;
    @XmlElement(name = "ReleaseAllItemsToExecution")
    protected Boolean releaseAllItemsToExecution;
    @XmlElement(name = "FinishFulfilmentProcessingOfAllItems")
    protected Boolean finishFulfilmentProcessingOfAllItems;
    @XmlElement(name = "BusinessTransactionDocumentReference")
    protected List<SalesOrderMaintainRequestBusinessTransactionDocumentReference> businessTransactionDocumentReference;
    @XmlElement(name = "SalesAndServiceBusinessArea")
    protected SalesOrderMaintainRequestSalesAndServiceBusinessArea salesAndServiceBusinessArea;
    @XmlElement(name = "BillToParty")
    protected SalesOrderMaintainRequestPartyParty billToParty;
    @XmlElement(name = "AccountParty")
    protected SalesOrderMaintainRequestPartyParty accountParty;
    @XmlElement(name = "PayerParty")
    protected SalesOrderMaintainRequestPartyParty payerParty;
    @XmlElement(name = "ProductRecipientParty")
    protected SalesOrderMaintainRequestPartyParty productRecipientParty;
    @XmlElement(name = "SalesPartnerParty")
    protected SalesOrderMaintainRequestPartyParty salesPartnerParty;
    @XmlElement(name = "FreightForwarderParty")
    protected SalesOrderMaintainRequestPartyParty freightForwarderParty;
    @XmlElement(name = "EmployeeResponsibleParty")
    protected SalesOrderMaintainRequestPartyIDParty employeeResponsibleParty;
    @XmlElement(name = "SellerParty")
    protected SalesOrderMaintainRequestPartyIDParty sellerParty;
    @XmlElement(name = "SalesUnitParty")
    protected SalesOrderMaintainRequestPartyIDParty salesUnitParty;
    @XmlElement(name = "ServiceExecutionTeamParty")
    protected SalesOrderMaintainRequestPartyIDParty serviceExecutionTeamParty;
    @XmlElement(name = "ServicePerformerParty")
    protected SalesOrderMaintainRequestPartyIDParty servicePerformerParty;
    @XmlElement(name = "SalesEmployeeParty")
    protected SalesOrderMaintainRequestPartyIDParty salesEmployeeParty;
    @XmlElement(name = "BillFromParty")
    protected SalesOrderMaintainRequestPartyIDParty billFromParty;
    @XmlElement(name = "RequestedFulfillmentPeriodPeriodTerms")
    protected SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms requestedFulfillmentPeriodPeriodTerms;
    @XmlElement(name = "DeliveryTerms")
    protected SalesOrderMaintainRequestDeliveryTerms deliveryTerms;
    @XmlElement(name = "PricingTerms")
    protected SalesOrderMaintainRequestPricingTerms pricingTerms;
    @XmlElement(name = "SalesTerms")
    protected SalesOrderMaintainRequestSalesTerms salesTerms;
    @XmlElement(name = "InvoiceTerms")
    protected SalesOrderMaintainRequestInvoiceTerms invoiceTerms;
    @XmlElement(name = "Item")
    protected List<SalesOrderMaintainRequestItem> item;
    @XmlElement(name = "CashDiscountTerms")
    protected SalesOrderMaintainRequestCashDiscountTerms cashDiscountTerms;
    @XmlElement(name = "PaymentControl")
    protected SalesOrderMaintainRequestPaymentControl paymentControl;
    @XmlElement(name = "PriceAndTaxCalculation")
    protected SalesOrderMaintainRequestPriceAndTaxCalculation priceAndTaxCalculation;
    @XmlElement(name = "TextCollection")
    protected SalesOrderMaintainRequestTextCollection textCollection;
    @XmlElement(name = "AttachmentFolder")
    protected MaintenanceAttachmentFolder attachmentFolder;
    @XmlElement(name = "ChangeStateID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String changeStateID;
    @XmlElement(name = "CroatiaBusinessSpaceID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String croatiaBusinessSpaceID;
    @XmlElement(name = "CroatiaBillingMachineID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String croatiaBillingMachineID;
    @XmlElement(name = "CroatiaPaymentMethodCode")
    protected CroatiaPaymentMethodCode croatiaPaymentMethodCode;
    @XmlElement(name = "CroatiaPartyTaxID")
    protected PartyTaxID croatiaPartyTaxID;
    @XmlElement(name = "CroatiaParagonBillMark")
    protected String croatiaParagonBillMark;
    @XmlElement(name = "CroatiaSpecialNote")
    protected String croatiaSpecialNote;
    @XmlElement(name = "ServicioFLAM", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String servicioFLAM;
    @XmlElement(name = "Consignatarios", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarios;
    @XmlElement(name = "UbicacinDestino", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestino;
    @XmlElement(name = "UbicacionOrigen", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacionOrigen;
    @XmlElement(name = "UbicacinCruce", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCruce;
    @XmlElement(name = "FechaCarga1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCarga1;
    @XmlElement(name = "FechaCruce1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCruce1;
    @XmlElement(name = "FechaDestino1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaDestino1;
    @XmlElement(name = "FechadeAgenteAduanal1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechadeAgenteAduanal1;
    @XmlElement(name = "GuaHouse", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHouse;
    @XmlElement(name = "GuaMaster", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMaster;
    @XmlElement(name = "Nmerodeequipodecarga", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipodecarga;
    @XmlElement(name = "PzsPBPC", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pzsPBPC;
    @XmlElement(name = "Shipper", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String shipper;
    @XmlElement(name = "Tipodeequipo", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeequipo;
    @XmlElement(name = "RequierePOD", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String requierePOD;
    @XmlElement(name = "Ndeservicio1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ndeservicio1;
    @XmlElement(name = "ConsignatarioDestino", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioDestino;
    @XmlElement(name = "ConsignatarioCruce", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioCruce;
    @XmlAttribute(name = "itemListCompleteTransmissionIndicator")
    protected Boolean itemListCompleteTransmissionIndicator;
    @XmlAttribute(name = "businessTransactionDocumentReferenceListCompleteTransmissionIndicator")
    protected Boolean businessTransactionDocumentReferenceListCompleteTransmissionIndicator;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Gets the value of the objectNodeSenderTechnicalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodeSenderTechnicalID() {
        return objectNodeSenderTechnicalID;
    }

    /**
     * Sets the value of the objectNodeSenderTechnicalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodeSenderTechnicalID(String value) {
        this.objectNodeSenderTechnicalID = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setID(BusinessTransactionDocumentID value) {
        this.id = value;
    }

    /**
     * Gets the value of the buyerID property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBuyerID() {
        return buyerID;
    }

    /**
     * Sets the value of the buyerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBuyerID(BusinessTransactionDocumentID value) {
        this.buyerID = value;
    }

    /**
     * Gets the value of the postingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPostingDate() {
        return postingDate;
    }

    /**
     * Sets the value of the postingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPostingDate(XMLGregorianCalendar value) {
        this.postingDate = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link EXTENDEDName }
     *     
     */
    public EXTENDEDName getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link EXTENDEDName }
     *     
     */
    public void setName(EXTENDEDName value) {
        this.name = value;
    }

    /**
     * Gets the value of the uuid property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Sets the value of the uuid property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Gets the value of the dataOriginTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataOriginTypeCode() {
        return dataOriginTypeCode;
    }

    /**
     * Sets the value of the dataOriginTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataOriginTypeCode(String value) {
        this.dataOriginTypeCode = value;
    }

    /**
     * Gets the value of the fulfillmentBlockingReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentBlockingReasonCode() {
        return fulfillmentBlockingReasonCode;
    }

    /**
     * Sets the value of the fulfillmentBlockingReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentBlockingReasonCode(String value) {
        this.fulfillmentBlockingReasonCode = value;
    }

    /**
     * Gets the value of the releaseCustomerRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReleaseCustomerRequest() {
        return releaseCustomerRequest;
    }

    /**
     * Sets the value of the releaseCustomerRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReleaseCustomerRequest(Boolean value) {
        this.releaseCustomerRequest = value;
    }

    /**
     * Gets the value of the releaseAllItemsToExecution property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReleaseAllItemsToExecution() {
        return releaseAllItemsToExecution;
    }

    /**
     * Sets the value of the releaseAllItemsToExecution property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReleaseAllItemsToExecution(Boolean value) {
        this.releaseAllItemsToExecution = value;
    }

    /**
     * Gets the value of the finishFulfilmentProcessingOfAllItems property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFinishFulfilmentProcessingOfAllItems() {
        return finishFulfilmentProcessingOfAllItems;
    }

    /**
     * Sets the value of the finishFulfilmentProcessingOfAllItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFinishFulfilmentProcessingOfAllItems(Boolean value) {
        this.finishFulfilmentProcessingOfAllItems = value;
    }

    /**
     * Gets the value of the businessTransactionDocumentReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the businessTransactionDocumentReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusinessTransactionDocumentReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderMaintainRequestBusinessTransactionDocumentReference }
     * 
     * 
     */
    public List<SalesOrderMaintainRequestBusinessTransactionDocumentReference> getBusinessTransactionDocumentReference() {
        if (businessTransactionDocumentReference == null) {
            businessTransactionDocumentReference = new ArrayList<SalesOrderMaintainRequestBusinessTransactionDocumentReference>();
        }
        return this.businessTransactionDocumentReference;
    }

    /**
     * Gets the value of the salesAndServiceBusinessArea property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestSalesAndServiceBusinessArea }
     *     
     */
    public SalesOrderMaintainRequestSalesAndServiceBusinessArea getSalesAndServiceBusinessArea() {
        return salesAndServiceBusinessArea;
    }

    /**
     * Sets the value of the salesAndServiceBusinessArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestSalesAndServiceBusinessArea }
     *     
     */
    public void setSalesAndServiceBusinessArea(SalesOrderMaintainRequestSalesAndServiceBusinessArea value) {
        this.salesAndServiceBusinessArea = value;
    }

    /**
     * Gets the value of the billToParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public SalesOrderMaintainRequestPartyParty getBillToParty() {
        return billToParty;
    }

    /**
     * Sets the value of the billToParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public void setBillToParty(SalesOrderMaintainRequestPartyParty value) {
        this.billToParty = value;
    }

    /**
     * Gets the value of the accountParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public SalesOrderMaintainRequestPartyParty getAccountParty() {
        return accountParty;
    }

    /**
     * Sets the value of the accountParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public void setAccountParty(SalesOrderMaintainRequestPartyParty value) {
        this.accountParty = value;
    }

    /**
     * Gets the value of the payerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public SalesOrderMaintainRequestPartyParty getPayerParty() {
        return payerParty;
    }

    /**
     * Sets the value of the payerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public void setPayerParty(SalesOrderMaintainRequestPartyParty value) {
        this.payerParty = value;
    }

    /**
     * Gets the value of the productRecipientParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public SalesOrderMaintainRequestPartyParty getProductRecipientParty() {
        return productRecipientParty;
    }

    /**
     * Sets the value of the productRecipientParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public void setProductRecipientParty(SalesOrderMaintainRequestPartyParty value) {
        this.productRecipientParty = value;
    }

    /**
     * Gets the value of the salesPartnerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public SalesOrderMaintainRequestPartyParty getSalesPartnerParty() {
        return salesPartnerParty;
    }

    /**
     * Sets the value of the salesPartnerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public void setSalesPartnerParty(SalesOrderMaintainRequestPartyParty value) {
        this.salesPartnerParty = value;
    }

    /**
     * Gets the value of the freightForwarderParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public SalesOrderMaintainRequestPartyParty getFreightForwarderParty() {
        return freightForwarderParty;
    }

    /**
     * Sets the value of the freightForwarderParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyParty }
     *     
     */
    public void setFreightForwarderParty(SalesOrderMaintainRequestPartyParty value) {
        this.freightForwarderParty = value;
    }

    /**
     * Gets the value of the employeeResponsibleParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getEmployeeResponsibleParty() {
        return employeeResponsibleParty;
    }

    /**
     * Sets the value of the employeeResponsibleParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setEmployeeResponsibleParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.employeeResponsibleParty = value;
    }

    /**
     * Gets the value of the sellerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getSellerParty() {
        return sellerParty;
    }

    /**
     * Sets the value of the sellerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setSellerParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.sellerParty = value;
    }

    /**
     * Gets the value of the salesUnitParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getSalesUnitParty() {
        return salesUnitParty;
    }

    /**
     * Sets the value of the salesUnitParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setSalesUnitParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.salesUnitParty = value;
    }

    /**
     * Gets the value of the serviceExecutionTeamParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getServiceExecutionTeamParty() {
        return serviceExecutionTeamParty;
    }

    /**
     * Sets the value of the serviceExecutionTeamParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setServiceExecutionTeamParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.serviceExecutionTeamParty = value;
    }

    /**
     * Gets the value of the servicePerformerParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getServicePerformerParty() {
        return servicePerformerParty;
    }

    /**
     * Sets the value of the servicePerformerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setServicePerformerParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.servicePerformerParty = value;
    }

    /**
     * Gets the value of the salesEmployeeParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getSalesEmployeeParty() {
        return salesEmployeeParty;
    }

    /**
     * Sets the value of the salesEmployeeParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setSalesEmployeeParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.salesEmployeeParty = value;
    }

    /**
     * Gets the value of the billFromParty property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public SalesOrderMaintainRequestPartyIDParty getBillFromParty() {
        return billFromParty;
    }

    /**
     * Sets the value of the billFromParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPartyIDParty }
     *     
     */
    public void setBillFromParty(SalesOrderMaintainRequestPartyIDParty value) {
        this.billFromParty = value;
    }

    /**
     * Gets the value of the requestedFulfillmentPeriodPeriodTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms }
     *     
     */
    public SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms getRequestedFulfillmentPeriodPeriodTerms() {
        return requestedFulfillmentPeriodPeriodTerms;
    }

    /**
     * Sets the value of the requestedFulfillmentPeriodPeriodTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms }
     *     
     */
    public void setRequestedFulfillmentPeriodPeriodTerms(SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms value) {
        this.requestedFulfillmentPeriodPeriodTerms = value;
    }

    /**
     * Gets the value of the deliveryTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestDeliveryTerms }
     *     
     */
    public SalesOrderMaintainRequestDeliveryTerms getDeliveryTerms() {
        return deliveryTerms;
    }

    /**
     * Sets the value of the deliveryTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestDeliveryTerms }
     *     
     */
    public void setDeliveryTerms(SalesOrderMaintainRequestDeliveryTerms value) {
        this.deliveryTerms = value;
    }

    /**
     * Gets the value of the pricingTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPricingTerms }
     *     
     */
    public SalesOrderMaintainRequestPricingTerms getPricingTerms() {
        return pricingTerms;
    }

    /**
     * Sets the value of the pricingTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPricingTerms }
     *     
     */
    public void setPricingTerms(SalesOrderMaintainRequestPricingTerms value) {
        this.pricingTerms = value;
    }

    /**
     * Gets the value of the salesTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestSalesTerms }
     *     
     */
    public SalesOrderMaintainRequestSalesTerms getSalesTerms() {
        return salesTerms;
    }

    /**
     * Sets the value of the salesTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestSalesTerms }
     *     
     */
    public void setSalesTerms(SalesOrderMaintainRequestSalesTerms value) {
        this.salesTerms = value;
    }

    /**
     * Gets the value of the invoiceTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestInvoiceTerms }
     *     
     */
    public SalesOrderMaintainRequestInvoiceTerms getInvoiceTerms() {
        return invoiceTerms;
    }

    /**
     * Sets the value of the invoiceTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestInvoiceTerms }
     *     
     */
    public void setInvoiceTerms(SalesOrderMaintainRequestInvoiceTerms value) {
        this.invoiceTerms = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderMaintainRequestItem }
     * 
     * 
     */
    public List<SalesOrderMaintainRequestItem> getItem() {
        if (item == null) {
            item = new ArrayList<SalesOrderMaintainRequestItem>();
        }
        return this.item;
    }

    /**
     * Gets the value of the cashDiscountTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestCashDiscountTerms }
     *     
     */
    public SalesOrderMaintainRequestCashDiscountTerms getCashDiscountTerms() {
        return cashDiscountTerms;
    }

    /**
     * Sets the value of the cashDiscountTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestCashDiscountTerms }
     *     
     */
    public void setCashDiscountTerms(SalesOrderMaintainRequestCashDiscountTerms value) {
        this.cashDiscountTerms = value;
    }

    /**
     * Gets the value of the paymentControl property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPaymentControl }
     *     
     */
    public SalesOrderMaintainRequestPaymentControl getPaymentControl() {
        return paymentControl;
    }

    /**
     * Sets the value of the paymentControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPaymentControl }
     *     
     */
    public void setPaymentControl(SalesOrderMaintainRequestPaymentControl value) {
        this.paymentControl = value;
    }

    /**
     * Gets the value of the priceAndTaxCalculation property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestPriceAndTaxCalculation }
     *     
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculation getPriceAndTaxCalculation() {
        return priceAndTaxCalculation;
    }

    /**
     * Sets the value of the priceAndTaxCalculation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestPriceAndTaxCalculation }
     *     
     */
    public void setPriceAndTaxCalculation(SalesOrderMaintainRequestPriceAndTaxCalculation value) {
        this.priceAndTaxCalculation = value;
    }

    /**
     * Gets the value of the textCollection property.
     * 
     * @return
     *     possible object is
     *     {@link SalesOrderMaintainRequestTextCollection }
     *     
     */
    public SalesOrderMaintainRequestTextCollection getTextCollection() {
        return textCollection;
    }

    /**
     * Sets the value of the textCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesOrderMaintainRequestTextCollection }
     *     
     */
    public void setTextCollection(SalesOrderMaintainRequestTextCollection value) {
        this.textCollection = value;
    }

    /**
     * Gets the value of the attachmentFolder property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getAttachmentFolder() {
        return attachmentFolder;
    }

    /**
     * Sets the value of the attachmentFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.attachmentFolder = value;
    }

    /**
     * Gets the value of the changeStateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeStateID() {
        return changeStateID;
    }

    /**
     * Sets the value of the changeStateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeStateID(String value) {
        this.changeStateID = value;
    }

    /**
     * Gets the value of the croatiaBusinessSpaceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaBusinessSpaceID() {
        return croatiaBusinessSpaceID;
    }

    /**
     * Sets the value of the croatiaBusinessSpaceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaBusinessSpaceID(String value) {
        this.croatiaBusinessSpaceID = value;
    }

    /**
     * Gets the value of the croatiaBillingMachineID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaBillingMachineID() {
        return croatiaBillingMachineID;
    }

    /**
     * Sets the value of the croatiaBillingMachineID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaBillingMachineID(String value) {
        this.croatiaBillingMachineID = value;
    }

    /**
     * Gets the value of the croatiaPaymentMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link CroatiaPaymentMethodCode }
     *     
     */
    public CroatiaPaymentMethodCode getCroatiaPaymentMethodCode() {
        return croatiaPaymentMethodCode;
    }

    /**
     * Sets the value of the croatiaPaymentMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link CroatiaPaymentMethodCode }
     *     
     */
    public void setCroatiaPaymentMethodCode(CroatiaPaymentMethodCode value) {
        this.croatiaPaymentMethodCode = value;
    }

    /**
     * Gets the value of the croatiaPartyTaxID property.
     * 
     * @return
     *     possible object is
     *     {@link PartyTaxID }
     *     
     */
    public PartyTaxID getCroatiaPartyTaxID() {
        return croatiaPartyTaxID;
    }

    /**
     * Sets the value of the croatiaPartyTaxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyTaxID }
     *     
     */
    public void setCroatiaPartyTaxID(PartyTaxID value) {
        this.croatiaPartyTaxID = value;
    }

    /**
     * Gets the value of the croatiaParagonBillMark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaParagonBillMark() {
        return croatiaParagonBillMark;
    }

    /**
     * Sets the value of the croatiaParagonBillMark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaParagonBillMark(String value) {
        this.croatiaParagonBillMark = value;
    }

    /**
     * Gets the value of the croatiaSpecialNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCroatiaSpecialNote() {
        return croatiaSpecialNote;
    }

    /**
     * Sets the value of the croatiaSpecialNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCroatiaSpecialNote(String value) {
        this.croatiaSpecialNote = value;
    }

    /**
     * Gets the value of the servicioFLAM property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServicioFLAM() {
        return servicioFLAM;
    }

    /**
     * Sets the value of the servicioFLAM property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServicioFLAM(String value) {
        this.servicioFLAM = value;
    }

    /**
     * Gets the value of the consignatarios property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarios() {
        return consignatarios;
    }

    /**
     * Sets the value of the consignatarios property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarios(String value) {
        this.consignatarios = value;
    }

    /**
     * Gets the value of the ubicacinDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestino() {
        return ubicacinDestino;
    }

    /**
     * Sets the value of the ubicacinDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestino(String value) {
        this.ubicacinDestino = value;
    }

    /**
     * Gets the value of the ubicacionOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacionOrigen() {
        return ubicacionOrigen;
    }

    /**
     * Sets the value of the ubicacionOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacionOrigen(String value) {
        this.ubicacionOrigen = value;
    }

    /**
     * Gets the value of the ubicacinCruce property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCruce() {
        return ubicacinCruce;
    }

    /**
     * Sets the value of the ubicacinCruce property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCruce(String value) {
        this.ubicacinCruce = value;
    }

    /**
     * Gets the value of the fechaCarga1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCarga1() {
        return fechaCarga1;
    }

    /**
     * Sets the value of the fechaCarga1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCarga1(XMLGregorianCalendar value) {
        this.fechaCarga1 = value;
    }

    /**
     * Gets the value of the fechaCruce1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCruce1() {
        return fechaCruce1;
    }

    /**
     * Sets the value of the fechaCruce1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCruce1(XMLGregorianCalendar value) {
        this.fechaCruce1 = value;
    }

    /**
     * Gets the value of the fechaDestino1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestino1() {
        return fechaDestino1;
    }

    /**
     * Sets the value of the fechaDestino1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestino1(XMLGregorianCalendar value) {
        this.fechaDestino1 = value;
    }

    /**
     * Gets the value of the fechadeAgenteAduanal1 property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechadeAgenteAduanal1() {
        return fechadeAgenteAduanal1;
    }

    /**
     * Sets the value of the fechadeAgenteAduanal1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechadeAgenteAduanal1(XMLGregorianCalendar value) {
        this.fechadeAgenteAduanal1 = value;
    }

    /**
     * Gets the value of the guaHouse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHouse() {
        return guaHouse;
    }

    /**
     * Sets the value of the guaHouse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHouse(String value) {
        this.guaHouse = value;
    }

    /**
     * Gets the value of the guaMaster property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMaster() {
        return guaMaster;
    }

    /**
     * Sets the value of the guaMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMaster(String value) {
        this.guaMaster = value;
    }

    /**
     * Gets the value of the nmerodeequipodecarga property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipodecarga() {
        return nmerodeequipodecarga;
    }

    /**
     * Sets the value of the nmerodeequipodecarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipodecarga(String value) {
        this.nmerodeequipodecarga = value;
    }

    /**
     * Gets the value of the pzsPBPC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPzsPBPC() {
        return pzsPBPC;
    }

    /**
     * Sets the value of the pzsPBPC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPzsPBPC(String value) {
        this.pzsPBPC = value;
    }

    /**
     * Gets the value of the shipper property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipper() {
        return shipper;
    }

    /**
     * Sets the value of the shipper property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipper(String value) {
        this.shipper = value;
    }

    /**
     * Gets the value of the tipodeequipo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeequipo() {
        return tipodeequipo;
    }

    /**
     * Sets the value of the tipodeequipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeequipo(String value) {
        this.tipodeequipo = value;
    }

    /**
     * Gets the value of the requierePOD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequierePOD() {
        return requierePOD;
    }

    /**
     * Sets the value of the requierePOD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequierePOD(String value) {
        this.requierePOD = value;
    }

    /**
     * Gets the value of the ndeservicio1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNdeservicio1() {
        return ndeservicio1;
    }

    /**
     * Sets the value of the ndeservicio1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNdeservicio1(String value) {
        this.ndeservicio1 = value;
    }

    /**
     * Gets the value of the consignatarioDestino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioDestino() {
        return consignatarioDestino;
    }

    /**
     * Sets the value of the consignatarioDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioDestino(String value) {
        this.consignatarioDestino = value;
    }

    /**
     * Gets the value of the consignatarioCruce property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioCruce() {
        return consignatarioCruce;
    }

    /**
     * Sets the value of the consignatarioCruce property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioCruce(String value) {
        this.consignatarioCruce = value;
    }

    /**
     * Gets the value of the itemListCompleteTransmissionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemListCompleteTransmissionIndicator() {
        return itemListCompleteTransmissionIndicator;
    }

    /**
     * Sets the value of the itemListCompleteTransmissionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemListCompleteTransmissionIndicator(Boolean value) {
        this.itemListCompleteTransmissionIndicator = value;
    }

    /**
     * Gets the value of the businessTransactionDocumentReferenceListCompleteTransmissionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBusinessTransactionDocumentReferenceListCompleteTransmissionIndicator() {
        return businessTransactionDocumentReferenceListCompleteTransmissionIndicator;
    }

    /**
     * Sets the value of the businessTransactionDocumentReferenceListCompleteTransmissionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBusinessTransactionDocumentReferenceListCompleteTransmissionIndicator(Boolean value) {
        this.businessTransactionDocumentReferenceListCompleteTransmissionIndicator = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
