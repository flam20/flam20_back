
package com.logistica.sap.pedidoCliente;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SalesOrderMaintainConfirmationBundleMessage_sync complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderMaintainConfirmationBundleMessage_sync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesOrder" type="{http://sap.com/xi/A1S/Global}SalesOrderMaintainConfirmationBundle" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Log" type="{http://sap.com/xi/AP/Common/GDT}Log"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderMaintainConfirmationBundleMessage_sync", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "salesOrder",
    "log"
})
public class SalesOrderMaintainConfirmationBundleMessageSync {

    @XmlElement(name = "SalesOrder")
    protected List<SalesOrderMaintainConfirmationBundle> salesOrder;
    @XmlElement(name = "Log", required = true)
    protected Log log;

    /**
     * Gets the value of the salesOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the salesOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSalesOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesOrderMaintainConfirmationBundle }
     * 
     * 
     */
    public List<SalesOrderMaintainConfirmationBundle> getSalesOrder() {
        if (salesOrder == null) {
            salesOrder = new ArrayList<SalesOrderMaintainConfirmationBundle>();
        }
        return this.salesOrder;
    }

    /**
     * Gets the value of the log property.
     * 
     * @return
     *     possible object is
     *     {@link Log }
     *     
     */
    public Log getLog() {
        return log;
    }

    /**
     * Sets the value of the log property.
     * 
     * @param value
     *     allowed object is
     *     {@link Log }
     *     
     */
    public void setLog(Log value) {
        this.log = value;
    }

}
