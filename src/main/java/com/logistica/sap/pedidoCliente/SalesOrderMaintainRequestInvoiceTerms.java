
package com.logistica.sap.pedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SalesOrderMaintainRequestInvoiceTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderMaintainRequestInvoiceTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProposedInvoiceDate" type="{http://sap.com/xi/AP/Common/GDT}Date" minOccurs="0"/>
 *         &lt;element name="InvoicingBlockingReasonCode" type="{http://sap.com/xi/AP/Common/GDT}InvoicingBlockingReasonCode" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderMaintainRequestInvoiceTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "proposedInvoiceDate",
    "invoicingBlockingReasonCode"
})
public class SalesOrderMaintainRequestInvoiceTerms {

    @XmlElement(name = "ProposedInvoiceDate")
    protected XMLGregorianCalendar proposedInvoiceDate;
    @XmlElement(name = "InvoicingBlockingReasonCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String invoicingBlockingReasonCode;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Gets the value of the proposedInvoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getProposedInvoiceDate() {
        return proposedInvoiceDate;
    }

    /**
     * Sets the value of the proposedInvoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setProposedInvoiceDate(XMLGregorianCalendar value) {
        this.proposedInvoiceDate = value;
    }

    /**
     * Gets the value of the invoicingBlockingReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingBlockingReasonCode() {
        return invoicingBlockingReasonCode;
    }

    /**
     * Sets the value of the invoicingBlockingReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingBlockingReasonCode(String value) {
        this.invoicingBlockingReasonCode = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
