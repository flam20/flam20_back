
package com.logistica.sap.pedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StartDateTime" type="{http://sap.com/xi/AP/Common/GDT}LOCALNORMALISED_DateTime" minOccurs="0"/>
 *         &lt;element name="EndDateTime" type="{http://sap.com/xi/AP/Common/GDT}LOCALNORMALISED_DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "startDateTime",
    "endDateTime"
})
public class SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms {

    @XmlElement(name = "StartDateTime")
    protected LOCALNORMALISEDDateTime2 startDateTime;
    @XmlElement(name = "EndDateTime")
    protected LOCALNORMALISEDDateTime2 endDateTime;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Gets the value of the startDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link LOCALNORMALISEDDateTime2 }
     *     
     */
    public LOCALNORMALISEDDateTime2 getStartDateTime() {
        return startDateTime;
    }

    /**
     * Sets the value of the startDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link LOCALNORMALISEDDateTime2 }
     *     
     */
    public void setStartDateTime(LOCALNORMALISEDDateTime2 value) {
        this.startDateTime = value;
    }

    /**
     * Gets the value of the endDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link LOCALNORMALISEDDateTime2 }
     *     
     */
    public LOCALNORMALISEDDateTime2 getEndDateTime() {
        return endDateTime;
    }

    /**
     * Sets the value of the endDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link LOCALNORMALISEDDateTime2 }
     *     
     */
    public void setEndDateTime(LOCALNORMALISEDDateTime2 value) {
        this.endDateTime = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
