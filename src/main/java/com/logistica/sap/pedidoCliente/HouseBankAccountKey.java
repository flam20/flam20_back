
package com.logistica.sap.pedidoCliente;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HouseBankAccountKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HouseBankAccountKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CompanyUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="InternalID" type="{http://sap.com/xi/AP/Common/GDT}BankAccountInternalID"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HouseBankAccountKey", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "companyUUID",
    "internalID"
})
public class HouseBankAccountKey {

    @XmlElement(name = "CompanyUUID")
    protected UUID companyUUID;
    @XmlElement(name = "InternalID", required = true)
    protected BankAccountInternalID internalID;

    /**
     * Gets the value of the companyUUID property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getCompanyUUID() {
        return companyUUID;
    }

    /**
     * Sets the value of the companyUUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setCompanyUUID(UUID value) {
        this.companyUUID = value;
    }

    /**
     * Gets the value of the internalID property.
     * 
     * @return
     *     possible object is
     *     {@link BankAccountInternalID }
     *     
     */
    public BankAccountInternalID getInternalID() {
        return internalID;
    }

    /**
     * Sets the value of the internalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankAccountInternalID }
     *     
     */
    public void setInternalID(BankAccountInternalID value) {
        this.internalID = value;
    }

}
