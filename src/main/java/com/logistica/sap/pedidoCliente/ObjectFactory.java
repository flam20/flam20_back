
package com.logistica.sap.pedidoCliente;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.logistica.integracionsap.ws.pedidoCliente package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SalesOrderRequestBundleCheckMaintainResponseSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "SalesOrderRequestBundleCheckMaintainResponse_sync");
    private final static QName _SalesOrderBundleMaintainRequestSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "SalesOrderBundleMaintainRequest_sync");
    private final static QName _SalesOrderRequestBundleCheckMaintainQuerySync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "SalesOrderRequestBundleCheckMaintainQuery_sync");
    private final static QName _SalesOrderBundleMaintainConfirmationSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "SalesOrderBundleMaintainConfirmation_sync");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.logistica.integracionsap.ws.pedidoCliente
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentCardKey }
     * 
     */
    public PaymentCardKey createPaymentCardKey() {
        return new PaymentCardKey();
    }

    /**
     * Create an instance of {@link CroatiaPaymentMethodCode }
     * 
     */
    public CroatiaPaymentMethodCode createCroatiaPaymentMethodCode() {
        return new CroatiaPaymentMethodCode();
    }

    /**
     * Create an instance of {@link LOCALNORMALISEDDateTime }
     * 
     */
    public LOCALNORMALISEDDateTime createLOCALNORMALISEDDateTime() {
        return new LOCALNORMALISEDDateTime();
    }

    /**
     * Create an instance of {@link BinaryObject }
     * 
     */
    public BinaryObject createBinaryObject() {
        return new BinaryObject();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocument }
     * 
     */
    public MaintenanceAttachmentFolderDocument createMaintenanceAttachmentFolderDocument() {
        return new MaintenanceAttachmentFolderDocument();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentFileContent }
     * 
     */
    public MaintenanceAttachmentFolderDocumentFileContent createMaintenanceAttachmentFolderDocumentFileContent() {
        return new MaintenanceAttachmentFolderDocumentFileContent();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentPropertyPropertyValue }
     * 
     */
    public MaintenanceAttachmentFolderDocumentPropertyPropertyValue createMaintenanceAttachmentFolderDocumentPropertyPropertyValue() {
        return new MaintenanceAttachmentFolderDocumentPropertyPropertyValue();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentProperty }
     * 
     */
    public MaintenanceAttachmentFolderDocumentProperty createMaintenanceAttachmentFolderDocumentProperty() {
        return new MaintenanceAttachmentFolderDocumentProperty();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolder }
     * 
     */
    public MaintenanceAttachmentFolder createMaintenanceAttachmentFolder() {
        return new MaintenanceAttachmentFolder();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainConfirmationBundle }
     * 
     */
    public SalesOrderMaintainConfirmationBundle createSalesOrderMaintainConfirmationBundle() {
        return new SalesOrderMaintainConfirmationBundle();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequest }
     * 
     */
    public SalesOrderMaintainRequest createSalesOrderMaintainRequest() {
        return new SalesOrderMaintainRequest();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculation }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculation createSalesOrderMaintainRequestPriceAndTaxCalculation() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculation();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms createSalesOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationTaxationTerms }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationTaxationTerms createSalesOrderMaintainRequestPriceAndTaxCalculationTaxationTerms() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationTaxationTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPaymentControl }
     * 
     */
    public SalesOrderMaintainRequestPaymentControl createSalesOrderMaintainRequestPaymentControl() {
        return new SalesOrderMaintainRequestPaymentControl();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationPriceComponent }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationPriceComponent createSalesOrderMaintainRequestPriceAndTaxCalculationPriceComponent() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationPriceComponent();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestTextCollection }
     * 
     */
    public SalesOrderMaintainRequestTextCollection createSalesOrderMaintainRequestTextCollection() {
        return new SalesOrderMaintainRequestTextCollection();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemProduct }
     * 
     */
    public SalesOrderMaintainRequestItemProduct createSalesOrderMaintainRequestItemProduct() {
        return new SalesOrderMaintainRequestItemProduct();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressPostalAddress }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressPostalAddress createSalesOrderMaintainRequestPartyAddressPostalAddress() {
        return new SalesOrderMaintainRequestPartyAddressPostalAddress();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainConfirmationBundleMessageSync }
     * 
     */
    public SalesOrderMaintainConfirmationBundleMessageSync createSalesOrderMaintainConfirmationBundleMessageSync() {
        return new SalesOrderMaintainConfirmationBundleMessageSync();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationProductTaxDetails }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationProductTaxDetails createSalesOrderMaintainRequestPriceAndTaxCalculationProductTaxDetails() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationProductTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms }
     * 
     */
    public SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms createSalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms() {
        return new SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressEmail }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressEmail createSalesOrderMaintainRequestPartyAddressEmail() {
        return new SalesOrderMaintainRequestPartyAddressEmail();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestInvoiceTerms }
     * 
     */
    public SalesOrderMaintainRequestInvoiceTerms createSalesOrderMaintainRequestInvoiceTerms() {
        return new SalesOrderMaintainRequestInvoiceTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice createSalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemSalesTerms }
     * 
     */
    public SalesOrderMaintainRequestItemSalesTerms createSalesOrderMaintainRequestItemSalesTerms() {
        return new SalesOrderMaintainRequestItemSalesTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestDeliveryTerms }
     * 
     */
    public SalesOrderMaintainRequestDeliveryTerms createSalesOrderMaintainRequestDeliveryTerms() {
        return new SalesOrderMaintainRequestDeliveryTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyIDParty }
     * 
     */
    public SalesOrderMaintainRequestPartyIDParty createSalesOrderMaintainRequestPartyIDParty() {
        return new SalesOrderMaintainRequestPartyIDParty();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestBusinessTransactionDocumentReference }
     * 
     */
    public SalesOrderMaintainRequestBusinessTransactionDocumentReference createSalesOrderMaintainRequestBusinessTransactionDocumentReference() {
        return new SalesOrderMaintainRequestBusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationMainDiscount }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationMainDiscount createSalesOrderMaintainRequestPriceAndTaxCalculationMainDiscount() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationMainDiscount();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemLocation }
     * 
     */
    public SalesOrderMaintainRequestItemLocation createSalesOrderMaintainRequestItemLocation() {
        return new SalesOrderMaintainRequestItemLocation();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestCashDiscountTerms }
     * 
     */
    public SalesOrderMaintainRequestCashDiscountTerms createSalesOrderMaintainRequestCashDiscountTerms() {
        return new SalesOrderMaintainRequestCashDiscountTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemDeliveryTerms }
     * 
     */
    public SalesOrderMaintainRequestItemDeliveryTerms createSalesOrderMaintainRequestItemDeliveryTerms() {
        return new SalesOrderMaintainRequestItemDeliveryTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressFascmile }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressFascmile createSalesOrderMaintainRequestPartyAddressFascmile() {
        return new SalesOrderMaintainRequestPartyAddressFascmile();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItem }
     * 
     */
    public SalesOrderMaintainRequestItem createSalesOrderMaintainRequestItem() {
        return new SalesOrderMaintainRequestItem();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressDisplayName }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressDisplayName createSalesOrderMaintainRequestPartyAddressDisplayName() {
        return new SalesOrderMaintainRequestPartyAddressDisplayName();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestSalesTerms }
     * 
     */
    public SalesOrderMaintainRequestSalesTerms createSalesOrderMaintainRequestSalesTerms() {
        return new SalesOrderMaintainRequestSalesTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressWeb }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressWeb createSalesOrderMaintainRequestPartyAddressWeb() {
        return new SalesOrderMaintainRequestPartyAddressWeb();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent createSalesOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationWithholdingTaxDetails }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationWithholdingTaxDetails createSalesOrderMaintainRequestPriceAndTaxCalculationWithholdingTaxDetails() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationWithholdingTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemServiceTerms }
     * 
     */
    public SalesOrderMaintainRequestItemServiceTerms createSalesOrderMaintainRequestItemServiceTerms() {
        return new SalesOrderMaintainRequestItemServiceTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddress }
     * 
     */
    public SalesOrderMaintainRequestPartyAddress createSalesOrderMaintainRequestPartyAddress() {
        return new SalesOrderMaintainRequestPartyAddress();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemAccountingCodingBlockDistribution }
     * 
     */
    public SalesOrderMaintainRequestItemAccountingCodingBlockDistribution createSalesOrderMaintainRequestItemAccountingCodingBlockDistribution() {
        return new SalesOrderMaintainRequestItemAccountingCodingBlockDistribution();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails createSalesOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPricingTerms }
     * 
     */
    public SalesOrderMaintainRequestPricingTerms createSalesOrderMaintainRequestPricingTerms() {
        return new SalesOrderMaintainRequestPricingTerms();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyParty }
     * 
     */
    public SalesOrderMaintainRequestPartyParty createSalesOrderMaintainRequestPartyParty() {
        return new SalesOrderMaintainRequestPartyParty();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPaymentControlExternalPayment }
     * 
     */
    public SalesOrderMaintainRequestPaymentControlExternalPayment createSalesOrderMaintainRequestPaymentControlExternalPayment() {
        return new SalesOrderMaintainRequestPaymentControlExternalPayment();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestTextCollectionText }
     * 
     */
    public SalesOrderMaintainRequestTextCollectionText createSalesOrderMaintainRequestTextCollectionText() {
        return new SalesOrderMaintainRequestTextCollectionText();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressName }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressName createSalesOrderMaintainRequestPartyAddressName() {
        return new SalesOrderMaintainRequestPartyAddressName();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails createSalesOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItem }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItem createSalesOrderMaintainRequestPriceAndTaxCalculationItem() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItem();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestSalesAndServiceBusinessArea }
     * 
     */
    public SalesOrderMaintainRequestSalesAndServiceBusinessArea createSalesOrderMaintainRequestSalesAndServiceBusinessArea() {
        return new SalesOrderMaintainRequestSalesAndServiceBusinessArea();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemScheduleLine }
     * 
     */
    public SalesOrderMaintainRequestItemScheduleLine createSalesOrderMaintainRequestItemScheduleLine() {
        return new SalesOrderMaintainRequestItemScheduleLine();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPaymentControlCreditCardPayment }
     * 
     */
    public SalesOrderMaintainRequestPaymentControlCreditCardPayment createSalesOrderMaintainRequestPaymentControlCreditCardPayment() {
        return new SalesOrderMaintainRequestPaymentControlCreditCardPayment();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyAddressTelephone }
     * 
     */
    public SalesOrderMaintainRequestPartyAddressTelephone createSalesOrderMaintainRequestPartyAddressTelephone() {
        return new SalesOrderMaintainRequestPartyAddressTelephone();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestItemAccountingCodingBlockDistributionAccountingCodingBlockAssignment }
     * 
     */
    public SalesOrderMaintainRequestItemAccountingCodingBlockDistributionAccountingCodingBlockAssignment createSalesOrderMaintainRequestItemAccountingCodingBlockDistributionAccountingCodingBlockAssignment() {
        return new SalesOrderMaintainRequestItemAccountingCodingBlockDistributionAccountingCodingBlockAssignment();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPaymentControlCreditCardPaymentCreditCardPaymentAuthorisation }
     * 
     */
    public SalesOrderMaintainRequestPaymentControlCreditCardPaymentCreditCardPaymentAuthorisation createSalesOrderMaintainRequestPaymentControlCreditCardPaymentCreditCardPaymentAuthorisation() {
        return new SalesOrderMaintainRequestPaymentControlCreditCardPaymentCreditCardPaymentAuthorisation();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainDiscount }
     * 
     */
    public SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainDiscount createSalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainDiscount() {
        return new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainDiscount();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestPartyContactParty }
     * 
     */
    public SalesOrderMaintainRequestPartyContactParty createSalesOrderMaintainRequestPartyContactParty() {
        return new SalesOrderMaintainRequestPartyContactParty();
    }

    /**
     * Create an instance of {@link SalesOrderMaintainRequestBundleMessageSync }
     * 
     */
    public SalesOrderMaintainRequestBundleMessageSync createSalesOrderMaintainRequestBundleMessageSync() {
        return new SalesOrderMaintainRequestBundleMessageSync();
    }

    /**
     * Create an instance of {@link WithholdingTaxIncomeTypeCode }
     * 
     */
    public WithholdingTaxIncomeTypeCode createWithholdingTaxIncomeTypeCode() {
        return new WithholdingTaxIncomeTypeCode();
    }

    /**
     * Create an instance of {@link TaxTypeCode }
     * 
     */
    public TaxTypeCode createTaxTypeCode() {
        return new TaxTypeCode();
    }

    /**
     * Create an instance of {@link WithholdingTax }
     * 
     */
    public WithholdingTax createWithholdingTax() {
        return new WithholdingTax();
    }

    /**
     * Create an instance of {@link ProductTax }
     * 
     */
    public ProductTax createProductTax() {
        return new ProductTax();
    }

    /**
     * Create an instance of {@link DatePeriod }
     * 
     */
    public DatePeriod createDatePeriod() {
        return new DatePeriod();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentID }
     * 
     */
    public BusinessTransactionDocumentID createBusinessTransactionDocumentID() {
        return new BusinessTransactionDocumentID();
    }

    /**
     * Create an instance of {@link PhoneNumber }
     * 
     */
    public PhoneNumber createPhoneNumber() {
        return new PhoneNumber();
    }

    /**
     * Create an instance of {@link EXTENDEDName }
     * 
     */
    public EXTENDEDName createEXTENDEDName() {
        return new EXTENDEDName();
    }

    /**
     * Create an instance of {@link PartyTaxID }
     * 
     */
    public PartyTaxID createPartyTaxID() {
        return new PartyTaxID();
    }

    /**
     * Create an instance of {@link ProductInternalID }
     * 
     */
    public ProductInternalID createProductInternalID() {
        return new ProductInternalID();
    }

    /**
     * Create an instance of {@link AddressRepresentationCode }
     * 
     */
    public AddressRepresentationCode createAddressRepresentationCode() {
        return new AddressRepresentationCode();
    }

    /**
     * Create an instance of {@link PaymentCardHolderAuthenticationID }
     * 
     */
    public PaymentCardHolderAuthenticationID createPaymentCardHolderAuthenticationID() {
        return new PaymentCardHolderAuthenticationID();
    }

    /**
     * Create an instance of {@link DeviceID }
     * 
     */
    public DeviceID createDeviceID() {
        return new DeviceID();
    }

    /**
     * Create an instance of {@link PaymentCardVerificationValueAvailabilityCode }
     * 
     */
    public PaymentCardVerificationValueAvailabilityCode createPaymentCardVerificationValueAvailabilityCode() {
        return new PaymentCardVerificationValueAvailabilityCode();
    }

    /**
     * Create an instance of {@link SHORTDescription }
     * 
     */
    public SHORTDescription createSHORTDescription() {
        return new SHORTDescription();
    }

    /**
     * Create an instance of {@link ProjectElementID }
     * 
     */
    public ProjectElementID createProjectElementID() {
        return new ProjectElementID();
    }

    /**
     * Create an instance of {@link ProductTaxEventTypeCode }
     * 
     */
    public ProductTaxEventTypeCode createProductTaxEventTypeCode() {
        return new ProductTaxEventTypeCode();
    }

    /**
     * Create an instance of {@link TaxRateTypeCode }
     * 
     */
    public TaxRateTypeCode createTaxRateTypeCode() {
        return new TaxRateTypeCode();
    }

    /**
     * Create an instance of {@link ResourceID }
     * 
     */
    public ResourceID createResourceID() {
        return new ResourceID();
    }

    /**
     * Create an instance of {@link TextCollectionTextTypeCode }
     * 
     */
    public TextCollectionTextTypeCode createTextCollectionTextTypeCode() {
        return new TextCollectionTextTypeCode();
    }

    /**
     * Create an instance of {@link LogItem }
     * 
     */
    public LogItem createLogItem() {
        return new LogItem();
    }

    /**
     * Create an instance of {@link UUID }
     * 
     */
    public UUID createUUID() {
        return new UUID();
    }

    /**
     * Create an instance of {@link CancellationReasonCode }
     * 
     */
    public CancellationReasonCode createCancellationReasonCode() {
        return new CancellationReasonCode();
    }

    /**
     * Create an instance of {@link ServiceWorkingConditionsCode }
     * 
     */
    public ServiceWorkingConditionsCode createServiceWorkingConditionsCode() {
        return new ServiceWorkingConditionsCode();
    }

    /**
     * Create an instance of {@link ClearingHouseAccountID }
     * 
     */
    public ClearingHouseAccountID createClearingHouseAccountID() {
        return new ClearingHouseAccountID();
    }

    /**
     * Create an instance of {@link TaxExemption }
     * 
     */
    public TaxExemption createTaxExemption() {
        return new TaxExemption();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link PaymentBlockingReasonCode }
     * 
     */
    public PaymentBlockingReasonCode createPaymentBlockingReasonCode() {
        return new PaymentBlockingReasonCode();
    }

    /**
     * Create an instance of {@link TaxJurisdictionCode }
     * 
     */
    public TaxJurisdictionCode createTaxJurisdictionCode() {
        return new TaxJurisdictionCode();
    }

    /**
     * Create an instance of {@link ExchangeRate }
     * 
     */
    public ExchangeRate createExchangeRate() {
        return new ExchangeRate();
    }

    /**
     * Create an instance of {@link PriceSpecificationElementTypeCode }
     * 
     */
    public PriceSpecificationElementTypeCode createPriceSpecificationElementTypeCode() {
        return new PriceSpecificationElementTypeCode();
    }

    /**
     * Create an instance of {@link Incoterms }
     * 
     */
    public Incoterms createIncoterms() {
        return new Incoterms();
    }

    /**
     * Create an instance of {@link MEDIUMName }
     * 
     */
    public MEDIUMName createMEDIUMName() {
        return new MEDIUMName();
    }

    /**
     * Create an instance of {@link NOCONVERSIONProductID }
     * 
     */
    public NOCONVERSIONProductID createNOCONVERSIONProductID() {
        return new NOCONVERSIONProductID();
    }

    /**
     * Create an instance of {@link BusinessDocumentBasicMessageHeader }
     * 
     */
    public BusinessDocumentBasicMessageHeader createBusinessDocumentBasicMessageHeader() {
        return new BusinessDocumentBasicMessageHeader();
    }

    /**
     * Create an instance of {@link PaymentCardHolderAuthenticationResultCode }
     * 
     */
    public PaymentCardHolderAuthenticationResultCode createPaymentCardHolderAuthenticationResultCode() {
        return new PaymentCardHolderAuthenticationResultCode();
    }

    /**
     * Create an instance of {@link ProductStandardID }
     * 
     */
    public ProductStandardID createProductStandardID() {
        return new ProductStandardID();
    }

    /**
     * Create an instance of {@link Rate }
     * 
     */
    public Rate createRate() {
        return new Rate();
    }

    /**
     * Create an instance of {@link PaymentCardID }
     * 
     */
    public PaymentCardID createPaymentCardID() {
        return new PaymentCardID();
    }

    /**
     * Create an instance of {@link Description }
     * 
     */
    public Description createDescription() {
        return new Description();
    }

    /**
     * Create an instance of {@link MEDIUMNote }
     * 
     */
    public MEDIUMNote createMEDIUMNote() {
        return new MEDIUMNote();
    }

    /**
     * Create an instance of {@link PaymentBlock }
     * 
     */
    public PaymentBlock createPaymentBlock() {
        return new PaymentBlock();
    }

    /**
     * Create an instance of {@link TaxExemptionReasonCode }
     * 
     */
    public TaxExemptionReasonCode createTaxExemptionReasonCode() {
        return new TaxExemptionReasonCode();
    }

    /**
     * Create an instance of {@link RegionCode }
     * 
     */
    public RegionCode createRegionCode() {
        return new RegionCode();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentTypeCode }
     * 
     */
    public BusinessTransactionDocumentTypeCode createBusinessTransactionDocumentTypeCode() {
        return new BusinessTransactionDocumentTypeCode();
    }

    /**
     * Create an instance of {@link OrganisationName }
     * 
     */
    public OrganisationName createOrganisationName() {
        return new OrganisationName();
    }

    /**
     * Create an instance of {@link WithholdingTaxationCharacteristicsCode }
     * 
     */
    public WithholdingTaxationCharacteristicsCode createWithholdingTaxationCharacteristicsCode() {
        return new WithholdingTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link DocumentTypeCode }
     * 
     */
    public DocumentTypeCode createDocumentTypeCode() {
        return new DocumentTypeCode();
    }

    /**
     * Create an instance of {@link DistributionChannelCode }
     * 
     */
    public DistributionChannelCode createDistributionChannelCode() {
        return new DistributionChannelCode();
    }

    /**
     * Create an instance of {@link LocationInternalID }
     * 
     */
    public LocationInternalID createLocationInternalID() {
        return new LocationInternalID();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentReference }
     * 
     */
    public BusinessTransactionDocumentReference createBusinessTransactionDocumentReference() {
        return new BusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link WithholdingTaxEventTypeCode }
     * 
     */
    public WithholdingTaxEventTypeCode createWithholdingTaxEventTypeCode() {
        return new WithholdingTaxEventTypeCode();
    }

    /**
     * Create an instance of {@link PartyID }
     * 
     */
    public PartyID createPartyID() {
        return new PartyID();
    }

    /**
     * Create an instance of {@link PaymentCardPaymentAuthorisationPartyIDV1 }
     * 
     */
    public PaymentCardPaymentAuthorisationPartyIDV1 createPaymentCardPaymentAuthorisationPartyIDV1() {
        return new PaymentCardPaymentAuthorisationPartyIDV1();
    }

    /**
     * Create an instance of {@link LogItemCategoryCode }
     * 
     */
    public LogItemCategoryCode createLogItemCategoryCode() {
        return new LogItemCategoryCode();
    }

    /**
     * Create an instance of {@link QuantityTypeCode }
     * 
     */
    public QuantityTypeCode createQuantityTypeCode() {
        return new QuantityTypeCode();
    }

    /**
     * Create an instance of {@link FulfilmentPartyCategoryCode }
     * 
     */
    public FulfilmentPartyCategoryCode createFulfilmentPartyCategoryCode() {
        return new FulfilmentPartyCategoryCode();
    }

    /**
     * Create an instance of {@link LocationID }
     * 
     */
    public LocationID createLocationID() {
        return new LocationID();
    }

    /**
     * Create an instance of {@link TaxIdentificationNumberTypeCode }
     * 
     */
    public TaxIdentificationNumberTypeCode createTaxIdentificationNumberTypeCode() {
        return new TaxIdentificationNumberTypeCode();
    }

    /**
     * Create an instance of {@link LogItemNotePlaceholderSubstitutionList }
     * 
     */
    public LogItemNotePlaceholderSubstitutionList createLogItemNotePlaceholderSubstitutionList() {
        return new LogItemNotePlaceholderSubstitutionList();
    }

    /**
     * Create an instance of {@link BankAccountInternalID }
     * 
     */
    public BankAccountInternalID createBankAccountInternalID() {
        return new BankAccountInternalID();
    }

    /**
     * Create an instance of {@link DataOriginTypeCode }
     * 
     */
    public DataOriginTypeCode createDataOriginTypeCode() {
        return new DataOriginTypeCode();
    }

    /**
     * Create an instance of {@link CashDiscountTermsCode }
     * 
     */
    public CashDiscountTermsCode createCashDiscountTermsCode() {
        return new CashDiscountTermsCode();
    }

    /**
     * Create an instance of {@link ProductTaxationCharacteristicsCode }
     * 
     */
    public ProductTaxationCharacteristicsCode createProductTaxationCharacteristicsCode() {
        return new ProductTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link NamespaceURI }
     * 
     */
    public NamespaceURI createNamespaceURI() {
        return new NamespaceURI();
    }

    /**
     * Create an instance of {@link RequirementSpecificationID }
     * 
     */
    public RequirementSpecificationID createRequirementSpecificationID() {
        return new RequirementSpecificationID();
    }

    /**
     * Create an instance of {@link TaxExemptionCertificateID }
     * 
     */
    public TaxExemptionCertificateID createTaxExemptionCertificateID() {
        return new TaxExemptionCertificateID();
    }

    /**
     * Create an instance of {@link ProductTaxStandardClassificationSystemCode }
     * 
     */
    public ProductTaxStandardClassificationSystemCode createProductTaxStandardClassificationSystemCode() {
        return new ProductTaxStandardClassificationSystemCode();
    }

    /**
     * Create an instance of {@link BusinessDocumentMessageID }
     * 
     */
    public BusinessDocumentMessageID createBusinessDocumentMessageID() {
        return new BusinessDocumentMessageID();
    }

    /**
     * Create an instance of {@link LONGName }
     * 
     */
    public LONGName createLONGName() {
        return new LONGName();
    }

    /**
     * Create an instance of {@link PaymentCardTypeCode }
     * 
     */
    public PaymentCardTypeCode createPaymentCardTypeCode() {
        return new PaymentCardTypeCode();
    }

    /**
     * Create an instance of {@link ProductTaxStandardClassificationCode }
     * 
     */
    public ProductTaxStandardClassificationCode createProductTaxStandardClassificationCode() {
        return new ProductTaxStandardClassificationCode();
    }

    /**
     * Create an instance of {@link Log }
     * 
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link LOCALNORMALISEDDateTime2 }
     * 
     */
    public LOCALNORMALISEDDateTime2 createLOCALNORMALISEDDateTime2() {
        return new LOCALNORMALISEDDateTime2();
    }

    /**
     * Create an instance of {@link Quantity }
     * 
     */
    public Quantity createQuantity() {
        return new Quantity();
    }

    /**
     * Create an instance of {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     * 
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod createUPPEROPENLOCALNORMALISEDDateTimePeriod() {
        return new UPPEROPENLOCALNORMALISEDDateTimePeriod();
    }

    /**
     * Create an instance of {@link TaxJurisdictionSubdivisionCode }
     * 
     */
    public TaxJurisdictionSubdivisionCode createTaxJurisdictionSubdivisionCode() {
        return new TaxJurisdictionSubdivisionCode();
    }

    /**
     * Create an instance of {@link FormOfAddressCode }
     * 
     */
    public FormOfAddressCode createFormOfAddressCode() {
        return new FormOfAddressCode();
    }

    /**
     * Create an instance of {@link EmailURI }
     * 
     */
    public EmailURI createEmailURI() {
        return new EmailURI();
    }

    /**
     * Create an instance of {@link TaxDeductibilityCode }
     * 
     */
    public TaxDeductibilityCode createTaxDeductibilityCode() {
        return new TaxDeductibilityCode();
    }

    /**
     * Create an instance of {@link TaxJurisdictionSubdivisionTypeCode }
     * 
     */
    public TaxJurisdictionSubdivisionTypeCode createTaxJurisdictionSubdivisionTypeCode() {
        return new TaxJurisdictionSubdivisionTypeCode();
    }

    /**
     * Create an instance of {@link StandardFaultMessage }
     * 
     */
    public StandardFaultMessage createStandardFaultMessage() {
        return new StandardFaultMessage();
    }

    /**
     * Create an instance of {@link ExchangeFaultData }
     * 
     */
    public ExchangeFaultData createExchangeFaultData() {
        return new ExchangeFaultData();
    }

    /**
     * Create an instance of {@link StandardFaultMessageExtension }
     * 
     */
    public StandardFaultMessageExtension createStandardFaultMessageExtension() {
        return new StandardFaultMessageExtension();
    }

    /**
     * Create an instance of {@link ProjectTaskKey }
     * 
     */
    public ProjectTaskKey createProjectTaskKey() {
        return new ProjectTaskKey();
    }

    /**
     * Create an instance of {@link RequirementSpecificationKey }
     * 
     */
    public RequirementSpecificationKey createRequirementSpecificationKey() {
        return new RequirementSpecificationKey();
    }

    /**
     * Create an instance of {@link HouseBankAccountKey }
     * 
     */
    public HouseBankAccountKey createHouseBankAccountKey() {
        return new HouseBankAccountKey();
    }

    /**
     * Create an instance of {@link ExchangeLogData }
     * 
     */
    public ExchangeLogData createExchangeLogData() {
        return new ExchangeLogData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderMaintainConfirmationBundleMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "SalesOrderRequestBundleCheckMaintainResponse_sync")
    public JAXBElement<SalesOrderMaintainConfirmationBundleMessageSync> createSalesOrderRequestBundleCheckMaintainResponseSync(SalesOrderMaintainConfirmationBundleMessageSync value) {
        return new JAXBElement<SalesOrderMaintainConfirmationBundleMessageSync>(_SalesOrderRequestBundleCheckMaintainResponseSync_QNAME, SalesOrderMaintainConfirmationBundleMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderMaintainRequestBundleMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "SalesOrderBundleMaintainRequest_sync")
    public JAXBElement<SalesOrderMaintainRequestBundleMessageSync> createSalesOrderBundleMaintainRequestSync(SalesOrderMaintainRequestBundleMessageSync value) {
        return new JAXBElement<SalesOrderMaintainRequestBundleMessageSync>(_SalesOrderBundleMaintainRequestSync_QNAME, SalesOrderMaintainRequestBundleMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderMaintainRequestBundleMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "SalesOrderRequestBundleCheckMaintainQuery_sync")
    public JAXBElement<SalesOrderMaintainRequestBundleMessageSync> createSalesOrderRequestBundleCheckMaintainQuerySync(SalesOrderMaintainRequestBundleMessageSync value) {
        return new JAXBElement<SalesOrderMaintainRequestBundleMessageSync>(_SalesOrderRequestBundleCheckMaintainQuerySync_QNAME, SalesOrderMaintainRequestBundleMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SalesOrderMaintainConfirmationBundleMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "SalesOrderBundleMaintainConfirmation_sync")
    public JAXBElement<SalesOrderMaintainConfirmationBundleMessageSync> createSalesOrderBundleMaintainConfirmationSync(SalesOrderMaintainConfirmationBundleMessageSync value) {
        return new JAXBElement<SalesOrderMaintainConfirmationBundleMessageSync>(_SalesOrderBundleMaintainConfirmationSync_QNAME, SalesOrderMaintainConfirmationBundleMessageSync.class, null, value);
    }

}
