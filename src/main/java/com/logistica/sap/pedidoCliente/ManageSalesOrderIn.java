
package com.logistica.sap.pedidoCliente;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ManageSalesOrderIn", targetNamespace = "http://sap.com/xi/A1S/Global")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ManageSalesOrderIn {


    /**
     * 
     * @param salesOrderRequestBundleCheckMaintainQuerySync
     * @return
     *     returns com.logistica.integracionsap.ws.pedidoCliente.SalesOrderMaintainConfirmationBundleMessageSync
     * @throws StandardFaultMessage_Exception
     */
    @WebMethod(operationName = "CheckMaintainBundle", action = "http://sap.com/xi/A1S/Global/ManageSalesOrderIn/CheckMaintainBundleRequest")
    @WebResult(name = "SalesOrderRequestBundleCheckMaintainResponse_sync", targetNamespace = "http://sap.com/xi/SAPGlobal20/Global", partName = "SalesOrderRequestBundleCheckMaintainResponse_sync")
    public SalesOrderMaintainConfirmationBundleMessageSync checkMaintainBundle(
        @WebParam(name = "SalesOrderRequestBundleCheckMaintainQuery_sync", targetNamespace = "http://sap.com/xi/SAPGlobal20/Global", partName = "SalesOrderRequestBundleCheckMaintainQuery_sync")
        SalesOrderMaintainRequestBundleMessageSync salesOrderRequestBundleCheckMaintainQuerySync)
        throws StandardFaultMessage_Exception
    ;

    /**
     * 
     * @param salesOrderBundleMaintainRequestSync
     * @return
     *     returns com.logistica.integracionsap.ws.pedidoCliente.SalesOrderMaintainConfirmationBundleMessageSync
     * @throws StandardFaultMessage_Exception
     */
    @WebMethod(operationName = "MaintainBundle", action = "http://sap.com/xi/A1S/Global/ManageSalesOrderIn/MaintainBundleRequest")
    @WebResult(name = "SalesOrderBundleMaintainConfirmation_sync", targetNamespace = "http://sap.com/xi/SAPGlobal20/Global", partName = "SalesOrderBundleMaintainConfirmation_sync")
    public SalesOrderMaintainConfirmationBundleMessageSync maintainBundle(
        @WebParam(name = "SalesOrderBundleMaintainRequest_sync", targetNamespace = "http://sap.com/xi/SAPGlobal20/Global", partName = "SalesOrderBundleMaintainRequest_sync")
        SalesOrderMaintainRequestBundleMessageSync salesOrderBundleMaintainRequestSync)
        throws StandardFaultMessage_Exception
    ;

}
