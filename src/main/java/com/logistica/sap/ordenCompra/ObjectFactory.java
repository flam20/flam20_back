
package com.logistica.sap.ordenCompra;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.logistica.integracionsap.ws.ordenCompra package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PurchaseRequestMaintainBundleRequest_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseRequestMaintainBundleRequest");
    private final static QName _PurchaseRequestMaintainCheckConfirmation_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseRequestMaintainCheckConfirmation");
    private final static QName _PurchaseRequestMaintainBundleConfirmation_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseRequestMaintainBundleConfirmation");
    private final static QName _PurchaseRequestMaintainCheckRequest_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseRequestMaintainCheckRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.logistica.integracionsap.ws.ordenCompra
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LOCALNORMALISEDDateTime }
     * 
     */
    public LOCALNORMALISEDDateTime createLOCALNORMALISEDDateTime() {
        return new LOCALNORMALISEDDateTime();
    }

    /**
     * Create an instance of {@link BinaryObject }
     * 
     */
    public BinaryObject createBinaryObject() {
        return new BinaryObject();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey }
     * 
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey createMaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey() {
        return new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey();
    }

    /**
     * Create an instance of {@link AccountingObjectCheckItemCostObjectReference }
     * 
     */
    public AccountingObjectCheckItemCostObjectReference createAccountingObjectCheckItemCostObjectReference() {
        return new AccountingObjectCheckItemCostObjectReference();
    }

    /**
     * Create an instance of {@link CodingBlockCustomField2Code }
     * 
     */
    public CodingBlockCustomField2Code createCodingBlockCustomField2Code() {
        return new CodingBlockCustomField2Code();
    }

    /**
     * Create an instance of {@link CustomObjectID }
     * 
     */
    public CustomObjectID createCustomObjectID() {
        return new CustomObjectID();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistribution }
     * 
     */
    public MaintenanceAccountingCodingBlockDistribution createMaintenanceAccountingCodingBlockDistribution() {
        return new MaintenanceAccountingCodingBlockDistribution();
    }

    /**
     * Create an instance of {@link CodingBlockCustomField3Code }
     * 
     */
    public CodingBlockCustomField3Code createCodingBlockCustomField3Code() {
        return new CodingBlockCustomField3Code();
    }

    /**
     * Create an instance of {@link CodingBlockCustomField1Code }
     * 
     */
    public CodingBlockCustomField1Code createCodingBlockCustomField1Code() {
        return new CodingBlockCustomField1Code();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey }
     * 
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey createMaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey() {
        return new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment }
     * 
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment createMaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment() {
        return new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment();
    }

    /**
     * Create an instance of {@link PurchaseRequestResponse }
     * 
     */
    public PurchaseRequestResponse createPurchaseRequestResponse() {
        return new PurchaseRequestResponse();
    }

    /**
     * Create an instance of {@link PurchaseRequestItemManually }
     * 
     */
    public PurchaseRequestItemManually createPurchaseRequestItemManually() {
        return new PurchaseRequestItemManually();
    }

    /**
     * Create an instance of {@link ManuallyPurchaseRequest }
     * 
     */
    public ManuallyPurchaseRequest createManuallyPurchaseRequest() {
        return new ManuallyPurchaseRequest();
    }

    /**
     * Create an instance of {@link PurchaseRequestMessageManuallyCreate }
     * 
     */
    public PurchaseRequestMessageManuallyCreate createPurchaseRequestMessageManuallyCreate() {
        return new PurchaseRequestMessageManuallyCreate();
    }

    /**
     * Create an instance of {@link ManuallyPurchaseRequestResponse }
     * 
     */
    public ManuallyPurchaseRequestResponse createManuallyPurchaseRequestResponse() {
        return new ManuallyPurchaseRequestResponse();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocument }
     * 
     */
    public MaintenanceAttachmentFolderDocument createMaintenanceAttachmentFolderDocument() {
        return new MaintenanceAttachmentFolderDocument();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentFileContent }
     * 
     */
    public MaintenanceAttachmentFolderDocumentFileContent createMaintenanceAttachmentFolderDocumentFileContent() {
        return new MaintenanceAttachmentFolderDocumentFileContent();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentPropertyPropertyValue }
     * 
     */
    public MaintenanceAttachmentFolderDocumentPropertyPropertyValue createMaintenanceAttachmentFolderDocumentPropertyPropertyValue() {
        return new MaintenanceAttachmentFolderDocumentPropertyPropertyValue();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentProperty }
     * 
     */
    public MaintenanceAttachmentFolderDocumentProperty createMaintenanceAttachmentFolderDocumentProperty() {
        return new MaintenanceAttachmentFolderDocumentProperty();
    }

    /**
     * Create an instance of {@link MaintenanceTextCollectionTextTextContent }
     * 
     */
    public MaintenanceTextCollectionTextTextContent createMaintenanceTextCollectionTextTextContent() {
        return new MaintenanceTextCollectionTextTextContent();
    }

    /**
     * Create an instance of {@link MaintenanceTextCollectionText }
     * 
     */
    public MaintenanceTextCollectionText createMaintenanceTextCollectionText() {
        return new MaintenanceTextCollectionText();
    }

    /**
     * Create an instance of {@link MaintenanceTextCollection }
     * 
     */
    public MaintenanceTextCollection createMaintenanceTextCollection() {
        return new MaintenanceTextCollection();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolder }
     * 
     */
    public MaintenanceAttachmentFolder createMaintenanceAttachmentFolder() {
        return new MaintenanceAttachmentFolder();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference createPurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference() {
        return new PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link PurchaseRequestMaintainRequestPriceCalculationItem }
     * 
     */
    public PurchaseRequestMaintainRequestPriceCalculationItem createPurchaseRequestMaintainRequestPriceCalculationItem() {
        return new PurchaseRequestMaintainRequestPriceCalculationItem();
    }

    /**
     * Create an instance of {@link PurchaseRequestMaintainRequestBundleItemParty }
     * 
     */
    public PurchaseRequestMaintainRequestBundleItemParty createPurchaseRequestMaintainRequestBundleItemParty() {
        return new PurchaseRequestMaintainRequestBundleItemParty();
    }

    /**
     * Create an instance of {@link PurchaseRequestMaintainRequestPriceAndTaxCalculationItemItemPriceComponent }
     * 
     */
    public PurchaseRequestMaintainRequestPriceAndTaxCalculationItemItemPriceComponent createPurchaseRequestMaintainRequestPriceAndTaxCalculationItemItemPriceComponent() {
        return new PurchaseRequestMaintainRequestPriceAndTaxCalculationItemItemPriceComponent();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentTypeCode }
     * 
     */
    public BusinessTransactionDocumentTypeCode createBusinessTransactionDocumentTypeCode() {
        return new BusinessTransactionDocumentTypeCode();
    }

    /**
     * Create an instance of {@link WithholdingTaxationCharacteristicsCode }
     * 
     */
    public WithholdingTaxationCharacteristicsCode createWithholdingTaxationCharacteristicsCode() {
        return new WithholdingTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link DocumentTypeCode }
     * 
     */
    public DocumentTypeCode createDocumentTypeCode() {
        return new DocumentTypeCode();
    }

    /**
     * Create an instance of {@link ProjectID }
     * 
     */
    public ProjectID createProjectID() {
        return new ProjectID();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentID }
     * 
     */
    public BusinessTransactionDocumentID createBusinessTransactionDocumentID() {
        return new BusinessTransactionDocumentID();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentReference }
     * 
     */
    public BusinessTransactionDocumentReference createBusinessTransactionDocumentReference() {
        return new BusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link PartyID }
     * 
     */
    public PartyID createPartyID() {
        return new PartyID();
    }

    /**
     * Create an instance of {@link LogItemCategoryCode }
     * 
     */
    public LogItemCategoryCode createLogItemCategoryCode() {
        return new LogItemCategoryCode();
    }

    /**
     * Create an instance of {@link EXTENDEDName }
     * 
     */
    public EXTENDEDName createEXTENDEDName() {
        return new EXTENDEDName();
    }

    /**
     * Create an instance of {@link Price }
     * 
     */
    public Price createPrice() {
        return new Price();
    }

    /**
     * Create an instance of {@link AccountingCodingBlockTypeCode }
     * 
     */
    public AccountingCodingBlockTypeCode createAccountingCodingBlockTypeCode() {
        return new AccountingCodingBlockTypeCode();
    }

    /**
     * Create an instance of {@link QuantityTypeCode }
     * 
     */
    public QuantityTypeCode createQuantityTypeCode() {
        return new QuantityTypeCode();
    }

    /**
     * Create an instance of {@link SHORTDescription }
     * 
     */
    public SHORTDescription createSHORTDescription() {
        return new SHORTDescription();
    }

    /**
     * Create an instance of {@link ProjectElementID }
     * 
     */
    public ProjectElementID createProjectElementID() {
        return new ProjectElementID();
    }

    /**
     * Create an instance of {@link EmployeeID }
     * 
     */
    public EmployeeID createEmployeeID() {
        return new EmployeeID();
    }

    /**
     * Create an instance of {@link LocationID }
     * 
     */
    public LocationID createLocationID() {
        return new LocationID();
    }

    /**
     * Create an instance of {@link GeneralLedgerAccountAliasCodeContextElements }
     * 
     */
    public GeneralLedgerAccountAliasCodeContextElements createGeneralLedgerAccountAliasCodeContextElements() {
        return new GeneralLedgerAccountAliasCodeContextElements();
    }

    /**
     * Create an instance of {@link IdentityID }
     * 
     */
    public IdentityID createIdentityID() {
        return new IdentityID();
    }

    /**
     * Create an instance of {@link LogItemNotePlaceholderSubstitutionList }
     * 
     */
    public LogItemNotePlaceholderSubstitutionList createLogItemNotePlaceholderSubstitutionList() {
        return new LogItemNotePlaceholderSubstitutionList();
    }

    /**
     * Create an instance of {@link TextCollectionTextTypeCode }
     * 
     */
    public TextCollectionTextTypeCode createTextCollectionTextTypeCode() {
        return new TextCollectionTextTypeCode();
    }

    /**
     * Create an instance of {@link LogItem }
     * 
     */
    public LogItem createLogItem() {
        return new LogItem();
    }

    /**
     * Create an instance of {@link GeneralLedgerAccountAliasCode }
     * 
     */
    public GeneralLedgerAccountAliasCode createGeneralLedgerAccountAliasCode() {
        return new GeneralLedgerAccountAliasCode();
    }

    /**
     * Create an instance of {@link UUID }
     * 
     */
    public UUID createUUID() {
        return new UUID();
    }

    /**
     * Create an instance of {@link ProductTaxationCharacteristicsCode }
     * 
     */
    public ProductTaxationCharacteristicsCode createProductTaxationCharacteristicsCode() {
        return new ProductTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link NamespaceURI }
     * 
     */
    public NamespaceURI createNamespaceURI() {
        return new NamespaceURI();
    }

    /**
     * Create an instance of {@link RequirementSpecificationID }
     * 
     */
    public RequirementSpecificationID createRequirementSpecificationID() {
        return new RequirementSpecificationID();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link BusinessDocumentMessageID }
     * 
     */
    public BusinessDocumentMessageID createBusinessDocumentMessageID() {
        return new BusinessDocumentMessageID();
    }

    /**
     * Create an instance of {@link ProjectReference }
     * 
     */
    public ProjectReference createProjectReference() {
        return new ProjectReference();
    }

    /**
     * Create an instance of {@link ObjectTypeCode }
     * 
     */
    public ObjectTypeCode createObjectTypeCode() {
        return new ObjectTypeCode();
    }

    /**
     * Create an instance of {@link TaxJurisdictionCode }
     * 
     */
    public TaxJurisdictionCode createTaxJurisdictionCode() {
        return new TaxJurisdictionCode();
    }

    /**
     * Create an instance of {@link PriceSpecificationElementTypeCode }
     * 
     */
    public PriceSpecificationElementTypeCode createPriceSpecificationElementTypeCode() {
        return new PriceSpecificationElementTypeCode();
    }

    /**
     * Create an instance of {@link ProductCategoryHierarchyID }
     * 
     */
    public ProductCategoryHierarchyID createProductCategoryHierarchyID() {
        return new ProductCategoryHierarchyID();
    }

    /**
     * Create an instance of {@link MEDIUMName }
     * 
     */
    public MEDIUMName createMEDIUMName() {
        return new MEDIUMName();
    }

    /**
     * Create an instance of {@link ProductID }
     * 
     */
    public ProductID createProductID() {
        return new ProductID();
    }

    /**
     * Create an instance of {@link BusinessDocumentBasicMessageHeader }
     * 
     */
    public BusinessDocumentBasicMessageHeader createBusinessDocumentBasicMessageHeader() {
        return new BusinessDocumentBasicMessageHeader();
    }

    /**
     * Create an instance of {@link Log }
     * 
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link CostObjectTypeCode }
     * 
     */
    public CostObjectTypeCode createCostObjectTypeCode() {
        return new CostObjectTypeCode();
    }

    /**
     * Create an instance of {@link Rate }
     * 
     */
    public Rate createRate() {
        return new Rate();
    }

    /**
     * Create an instance of {@link Description }
     * 
     */
    public Description createDescription() {
        return new Description();
    }

    /**
     * Create an instance of {@link Quantity }
     * 
     */
    public Quantity createQuantity() {
        return new Quantity();
    }

    /**
     * Create an instance of {@link PriceComponentCalculationBasis }
     * 
     */
    public PriceComponentCalculationBasis createPriceComponentCalculationBasis() {
        return new PriceComponentCalculationBasis();
    }

    /**
     * Create an instance of {@link MEDIUMDescription }
     * 
     */
    public MEDIUMDescription createMEDIUMDescription() {
        return new MEDIUMDescription();
    }

    /**
     * Create an instance of {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     * 
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod createUPPEROPENLOCALNORMALISEDDateTimePeriod() {
        return new UPPEROPENLOCALNORMALISEDDateTimePeriod();
    }

    /**
     * Create an instance of {@link Text }
     * 
     */
    public Text createText() {
        return new Text();
    }

    /**
     * Create an instance of {@link StandardFaultMessage }
     * 
     */
    public StandardFaultMessage createStandardFaultMessage() {
        return new StandardFaultMessage();
    }

    /**
     * Create an instance of {@link ExchangeFaultData }
     * 
     */
    public ExchangeFaultData createExchangeFaultData() {
        return new ExchangeFaultData();
    }

    /**
     * Create an instance of {@link StandardFaultMessageExtension }
     * 
     */
    public StandardFaultMessageExtension createStandardFaultMessageExtension() {
        return new StandardFaultMessageExtension();
    }

    /**
     * Create an instance of {@link PurchaseRequestMaintainRequestProductKeyItem }
     * 
     */
    public PurchaseRequestMaintainRequestProductKeyItem createPurchaseRequestMaintainRequestProductKeyItem() {
        return new PurchaseRequestMaintainRequestProductKeyItem();
    }

    /**
     * Create an instance of {@link PurchaseRequestMaintainRequestProductCategoryIDKeyItem }
     * 
     */
    public PurchaseRequestMaintainRequestProductCategoryIDKeyItem createPurchaseRequestMaintainRequestProductCategoryIDKeyItem() {
        return new PurchaseRequestMaintainRequestProductCategoryIDKeyItem();
    }

    /**
     * Create an instance of {@link PurchaseRequestMaintainRequestProductRequirementSpecificationItem }
     * 
     */
    public PurchaseRequestMaintainRequestProductRequirementSpecificationItem createPurchaseRequestMaintainRequestProductRequirementSpecificationItem() {
        return new PurchaseRequestMaintainRequestProductRequirementSpecificationItem();
    }

    /**
     * Create an instance of {@link ExchangeLogData }
     * 
     */
    public ExchangeLogData createExchangeLogData() {
        return new ExchangeLogData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseRequestMessageManuallyCreate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseRequestMaintainBundleRequest")
    public JAXBElement<PurchaseRequestMessageManuallyCreate> createPurchaseRequestMaintainBundleRequest(PurchaseRequestMessageManuallyCreate value) {
        return new JAXBElement<PurchaseRequestMessageManuallyCreate>(_PurchaseRequestMaintainBundleRequest_QNAME, PurchaseRequestMessageManuallyCreate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseRequestMaintainCheckConfirmation")
    public JAXBElement<PurchaseRequestResponse> createPurchaseRequestMaintainCheckConfirmation(PurchaseRequestResponse value) {
        return new JAXBElement<PurchaseRequestResponse>(_PurchaseRequestMaintainCheckConfirmation_QNAME, PurchaseRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseRequestMaintainBundleConfirmation")
    public JAXBElement<PurchaseRequestResponse> createPurchaseRequestMaintainBundleConfirmation(PurchaseRequestResponse value) {
        return new JAXBElement<PurchaseRequestResponse>(_PurchaseRequestMaintainBundleConfirmation_QNAME, PurchaseRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseRequestMessageManuallyCreate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseRequestMaintainCheckRequest")
    public JAXBElement<PurchaseRequestMessageManuallyCreate> createPurchaseRequestMaintainCheckRequest(PurchaseRequestMessageManuallyCreate value) {
        return new JAXBElement<PurchaseRequestMessageManuallyCreate>(_PurchaseRequestMaintainCheckRequest_QNAME, PurchaseRequestMessageManuallyCreate.class, null, value);
    }

}
