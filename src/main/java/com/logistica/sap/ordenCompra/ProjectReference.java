
package com.logistica.sap.ordenCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for ProjectReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProjectReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProjectID" type="{http://sap.com/xi/AP/Common/GDT}ProjectID" minOccurs="0"/>
 *         &lt;element name="ProjectUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ProjectName" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="ProjectElementID" type="{http://sap.com/xi/AP/Common/GDT}ProjectElementID" minOccurs="0"/>
 *         &lt;element name="ProjectElementUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ProjectElementName" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="ProjectElementTypeCode" type="{http://sap.com/xi/AP/Common/GDT}ProjectElementTypeCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProjectReference", propOrder = {
    "projectID",
    "projectUUID",
    "projectName",
    "projectElementID",
    "projectElementUUID",
    "projectElementName",
    "projectElementTypeCode"
})
public class ProjectReference {

    @XmlElement(name = "ProjectID")
    protected ProjectID projectID;
    @XmlElement(name = "ProjectUUID")
    protected UUID projectUUID;
    @XmlElement(name = "ProjectName")
    protected MEDIUMName projectName;
    @XmlElement(name = "ProjectElementID")
    protected ProjectElementID projectElementID;
    @XmlElement(name = "ProjectElementUUID")
    protected UUID projectElementUUID;
    @XmlElement(name = "ProjectElementName")
    protected MEDIUMName projectElementName;
    @XmlElement(name = "ProjectElementTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String projectElementTypeCode;

    /**
     * Gets the value of the projectID property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectID }
     *     
     */
    public ProjectID getProjectID() {
        return projectID;
    }

    /**
     * Sets the value of the projectID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectID }
     *     
     */
    public void setProjectID(ProjectID value) {
        this.projectID = value;
    }

    /**
     * Gets the value of the projectUUID property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getProjectUUID() {
        return projectUUID;
    }

    /**
     * Sets the value of the projectUUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setProjectUUID(UUID value) {
        this.projectUUID = value;
    }

    /**
     * Gets the value of the projectName property.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMName }
     *     
     */
    public MEDIUMName getProjectName() {
        return projectName;
    }

    /**
     * Sets the value of the projectName property.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMName }
     *     
     */
    public void setProjectName(MEDIUMName value) {
        this.projectName = value;
    }

    /**
     * Gets the value of the projectElementID property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectElementID }
     *     
     */
    public ProjectElementID getProjectElementID() {
        return projectElementID;
    }

    /**
     * Sets the value of the projectElementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectElementID }
     *     
     */
    public void setProjectElementID(ProjectElementID value) {
        this.projectElementID = value;
    }

    /**
     * Gets the value of the projectElementUUID property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getProjectElementUUID() {
        return projectElementUUID;
    }

    /**
     * Sets the value of the projectElementUUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setProjectElementUUID(UUID value) {
        this.projectElementUUID = value;
    }

    /**
     * Gets the value of the projectElementName property.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMName }
     *     
     */
    public MEDIUMName getProjectElementName() {
        return projectElementName;
    }

    /**
     * Sets the value of the projectElementName property.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMName }
     *     
     */
    public void setProjectElementName(MEDIUMName value) {
        this.projectElementName = value;
    }

    /**
     * Gets the value of the projectElementTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectElementTypeCode() {
        return projectElementTypeCode;
    }

    /**
     * Sets the value of the projectElementTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectElementTypeCode(String value) {
        this.projectElementTypeCode = value;
    }

}
