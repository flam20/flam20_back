
package com.logistica.sap.ordenCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for PurchaseRequestMaintainRequestProductCategoryIDKeyItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseRequestMaintainRequestProductCategoryIDKeyItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductCategoryHierarchyID" type="{http://sap.com/xi/AP/Common/GDT}ProductCategoryHierarchyID" minOccurs="0"/>
 *         &lt;element name="ProductCategoryInternalID" type="{http://sap.com/xi/AP/Common/GDT}ProductCategoryInternalID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseRequestMaintainRequestProductCategoryIDKeyItem", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "productCategoryHierarchyID",
    "productCategoryInternalID"
})
public class PurchaseRequestMaintainRequestProductCategoryIDKeyItem {

    @XmlElement(name = "ProductCategoryHierarchyID")
    protected ProductCategoryHierarchyID productCategoryHierarchyID;
    @XmlElement(name = "ProductCategoryInternalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String productCategoryInternalID;

    /**
     * Gets the value of the productCategoryHierarchyID property.
     * 
     * @return
     *     possible object is
     *     {@link ProductCategoryHierarchyID }
     *     
     */
    public ProductCategoryHierarchyID getProductCategoryHierarchyID() {
        return productCategoryHierarchyID;
    }

    /**
     * Sets the value of the productCategoryHierarchyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCategoryHierarchyID }
     *     
     */
    public void setProductCategoryHierarchyID(ProductCategoryHierarchyID value) {
        this.productCategoryHierarchyID = value;
    }

    /**
     * Gets the value of the productCategoryInternalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategoryInternalID() {
        return productCategoryInternalID;
    }

    /**
     * Sets the value of the productCategoryInternalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategoryInternalID(String value) {
        this.productCategoryInternalID = value;
    }

}
