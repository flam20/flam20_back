
package com.logistica.sap.ordenCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchaseRequestMessageManuallyCreate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseRequestMessageManuallyCreate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BasicMessageHeader" type="{http://sap.com/xi/AP/Common/GDT}BusinessDocumentBasicMessageHeader"/>
 *         &lt;element name="PurchaseRequestMaintainBundle" type="{http://sap.com/xi/AP/Purchasing/Global}ManuallyPurchaseRequest" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseRequestMessageManuallyCreate", namespace = "http://sap.com/xi/AP/Purchasing/Global", propOrder = {
    "basicMessageHeader",
    "purchaseRequestMaintainBundle"
})
public class PurchaseRequestMessageManuallyCreate {

    @XmlElement(name = "BasicMessageHeader", required = true)
    protected BusinessDocumentBasicMessageHeader basicMessageHeader;
    @XmlElement(name = "PurchaseRequestMaintainBundle", required = true)
    protected List<ManuallyPurchaseRequest> purchaseRequestMaintainBundle;

    /**
     * Gets the value of the basicMessageHeader property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessDocumentBasicMessageHeader }
     *     
     */
    public BusinessDocumentBasicMessageHeader getBasicMessageHeader() {
        return basicMessageHeader;
    }

    /**
     * Sets the value of the basicMessageHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessDocumentBasicMessageHeader }
     *     
     */
    public void setBasicMessageHeader(BusinessDocumentBasicMessageHeader value) {
        this.basicMessageHeader = value;
    }

    /**
     * Gets the value of the purchaseRequestMaintainBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the purchaseRequestMaintainBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPurchaseRequestMaintainBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ManuallyPurchaseRequest }
     * 
     * 
     */
    public List<ManuallyPurchaseRequest> getPurchaseRequestMaintainBundle() {
        if (purchaseRequestMaintainBundle == null) {
            purchaseRequestMaintainBundle = new ArrayList<ManuallyPurchaseRequest>();
        }
        return this.purchaseRequestMaintainBundle;
    }

}
