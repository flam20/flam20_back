
package com.logistica.sap.ordenCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for PurchaseRequestMaintainRequestProductKeyItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseRequestMaintainRequestProductKeyItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductTypeCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTypeCode" minOccurs="0"/>
 *         &lt;element name="ProductIdentifierTypeCode" type="{http://sap.com/xi/AP/Common/GDT}ProductIdentifierTypeCode" minOccurs="0"/>
 *         &lt;element name="ProductID" type="{http://sap.com/xi/AP/Common/GDT}ProductID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseRequestMaintainRequestProductKeyItem", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "productTypeCode",
    "productIdentifierTypeCode",
    "productID"
})
public class PurchaseRequestMaintainRequestProductKeyItem {

    @XmlElement(name = "ProductTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String productTypeCode;
    @XmlElement(name = "ProductIdentifierTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String productIdentifierTypeCode;
    @XmlElement(name = "ProductID")
    protected ProductID productID;

    /**
     * Gets the value of the productTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductTypeCode() {
        return productTypeCode;
    }

    /**
     * Sets the value of the productTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductTypeCode(String value) {
        this.productTypeCode = value;
    }

    /**
     * Gets the value of the productIdentifierTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductIdentifierTypeCode() {
        return productIdentifierTypeCode;
    }

    /**
     * Sets the value of the productIdentifierTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductIdentifierTypeCode(String value) {
        this.productIdentifierTypeCode = value;
    }

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link ProductID }
     *     
     */
    public ProductID getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductID }
     *     
     */
    public void setProductID(ProductID value) {
        this.productID = value;
    }

}
