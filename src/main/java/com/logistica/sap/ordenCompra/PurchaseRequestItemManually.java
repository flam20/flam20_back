
package com.logistica.sap.ordenCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PurchaseRequestItemManually complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseRequestItemManually">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodeSenderTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ItemID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;element name="ProductKeyItem" type="{http://sap.com/xi/AP/Common/Global}PurchaseRequestMaintainRequestProductKeyItem" minOccurs="0"/>
 *         &lt;element name="ProductCategoryKeyItem" type="{http://sap.com/xi/AP/Common/Global}PurchaseRequestMaintainRequestProductCategoryIDKeyItem" minOccurs="0"/>
 *         &lt;element name="ProductDescription" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemTypeCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="DeliveryPeriod" type="{http://sap.com/xi/AP/Common/GDT}UPPEROPEN_LOCALNORMALISED_DateTimePeriod" minOccurs="0"/>
 *         &lt;element name="ShipToLocationID" type="{http://sap.com/xi/AP/Common/GDT}LocationID" minOccurs="0"/>
 *         &lt;element name="LimitValueAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="ListUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="SupplierPartNumberID" type="{http://sap.com/xi/AP/Common/GDT}ProductPartyID" minOccurs="0"/>
 *         &lt;element name="SiteID" type="{http://sap.com/xi/AP/Common/GDT}LocationID" minOccurs="0"/>
 *         &lt;element name="CompanyIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="RequesterIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="RecipientIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="BuyerResponsibleIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="PurchasingUnitIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="SupplierIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ServiceAgentIDParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ItemPurchasingContractReferenceID" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference" minOccurs="0"/>
 *         &lt;element name="cancelOpenQuantityActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="reopenQuantityActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ItemAccountAssignmentDistribution" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}MaintenanceAccountingCodingBlockDistribution" minOccurs="0"/>
 *         &lt;element name="AttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="TextCollection" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceTextCollection" minOccurs="0"/>
 *         &lt;element name="directMaterialIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="thirdPartyIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ProductRequirementSpecificationItem" type="{http://sap.com/xi/AP/Common/Global}PurchaseRequestMaintainRequestProductRequirementSpecificationItem" minOccurs="0"/>
 *         &lt;element name="PreferredSupplierParty" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ItemPriceCalculationItem" type="{http://sap.com/xi/A1S/Global}PurchaseRequestMaintainRequestPriceCalculationItem" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1EE9BDEAF9DC61AF1E0F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8810CB317DB15"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8A05645EDDB95"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8A20AE1021B95"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8A37BDA091B95"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8A501A88D9B95"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8A6765FC5DB9A"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8A88695585B9A"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8AB9085065BA4"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8AD14DE815BA4"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8AE83E21C5BEC"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8AFE45C0E5BED"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8B16F90DA9BEE"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8B2D8A9E9DBF5"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8BB8FFC8C3BFE"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1ED9BEE8BD8929665BFE"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EDA8A811C873B514344"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EEA89A956C9457AD2F5"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A15A1EEA89A9A27DB91714FB"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseRequestItemManually", namespace = "http://sap.com/xi/AP/Purchasing/Global", propOrder = {
    "objectNodeSenderTechnicalID",
    "itemID",
    "productKeyItem",
    "productCategoryKeyItem",
    "productDescription",
    "typeCode",
    "quantity",
    "deliveryPeriod",
    "shipToLocationID",
    "limitValueAmount",
    "listUnitPrice",
    "supplierPartNumberID",
    "siteID",
    "companyIDParty",
    "requesterIDParty",
    "recipientIDParty",
    "buyerResponsibleIDParty",
    "purchasingUnitIDParty",
    "supplierIDParty",
    "serviceAgentIDParty",
    "itemPurchasingContractReferenceID",
    "cancelOpenQuantityActionIndicator",
    "reopenQuantityActionIndicator",
    "itemAccountAssignmentDistribution",
    "attachmentFolder",
    "textCollection",
    "directMaterialIndicator",
    "thirdPartyIndicator",
    "productRequirementSpecificationItem",
    "preferredSupplierParty",
    "itemPriceCalculationItem",
    "ndeservicio2",
    "consignatariosS",
    "fechaCargaS",
    "fechaCruceS",
    "fechaDestinoS",
    "fechadeAgenteAduanalS",
    "guaHouseS",
    "guaMasterS",
    "nmerodeequipodecargaS",
    "pzsPBPCS",
    "requierePODS",
    "shipperS",
    "tipodeequipoS",
    "ubicacinCruceS",
    "ubicacinDestinoS",
    "ubicacinOrigenS",
    "unidaddeventaS",
    "consignatarioCruceS",
    "consignatarioDestino1"
})
public class PurchaseRequestItemManually {

    @XmlElement(name = "ObjectNodeSenderTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String objectNodeSenderTechnicalID;
    @XmlElement(name = "ItemID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String itemID;
    @XmlElement(name = "ProductKeyItem")
    protected PurchaseRequestMaintainRequestProductKeyItem productKeyItem;
    @XmlElement(name = "ProductCategoryKeyItem")
    protected PurchaseRequestMaintainRequestProductCategoryIDKeyItem productCategoryKeyItem;
    @XmlElement(name = "ProductDescription")
    protected SHORTDescription productDescription;
    @XmlElement(name = "TypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String typeCode;
    @XmlElement(name = "Quantity")
    protected Quantity quantity;
    @XmlElement(name = "DeliveryPeriod")
    protected UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod;
    @XmlElement(name = "ShipToLocationID")
    protected LocationID shipToLocationID;
    @XmlElement(name = "LimitValueAmount")
    protected Amount limitValueAmount;
    @XmlElement(name = "ListUnitPrice")
    protected Price listUnitPrice;
    @XmlElement(name = "SupplierPartNumberID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String supplierPartNumberID;
    @XmlElement(name = "SiteID")
    protected LocationID siteID;
    @XmlElement(name = "CompanyIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty companyIDParty;
    @XmlElement(name = "RequesterIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty requesterIDParty;
    @XmlElement(name = "RecipientIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty recipientIDParty;
    @XmlElement(name = "BuyerResponsibleIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty buyerResponsibleIDParty;
    @XmlElement(name = "PurchasingUnitIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty purchasingUnitIDParty;
    @XmlElement(name = "SupplierIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty supplierIDParty;
    @XmlElement(name = "ServiceAgentIDParty")
    protected PurchaseRequestMaintainRequestBundleItemParty serviceAgentIDParty;
    @XmlElement(name = "ItemPurchasingContractReferenceID")
    protected PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference itemPurchasingContractReferenceID;
    protected Boolean cancelOpenQuantityActionIndicator;
    protected Boolean reopenQuantityActionIndicator;
    @XmlElement(name = "ItemAccountAssignmentDistribution")
    protected MaintenanceAccountingCodingBlockDistribution itemAccountAssignmentDistribution;
    @XmlElement(name = "AttachmentFolder")
    protected MaintenanceAttachmentFolder attachmentFolder;
    @XmlElement(name = "TextCollection")
    protected MaintenanceTextCollection textCollection;
    protected Boolean directMaterialIndicator;
    protected Boolean thirdPartyIndicator;
    @XmlElement(name = "ProductRequirementSpecificationItem")
    protected PurchaseRequestMaintainRequestProductRequirementSpecificationItem productRequirementSpecificationItem;
    @XmlElement(name = "PreferredSupplierParty")
    protected PurchaseRequestMaintainRequestBundleItemParty preferredSupplierParty;
    @XmlElement(name = "ItemPriceCalculationItem")
    protected PurchaseRequestMaintainRequestPriceCalculationItem itemPriceCalculationItem;
    @XmlElement(name = "Ndeservicio2", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ndeservicio2;
    @XmlElement(name = "ConsignatariosS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatariosS;
    @XmlElement(name = "FechaCargaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCargaS;
    @XmlElement(name = "FechaCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCruceS;
    @XmlElement(name = "FechaDestinoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaDestinoS;
    @XmlElement(name = "FechadeAgenteAduanalS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechadeAgenteAduanalS;
    @XmlElement(name = "GuaHouseS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHouseS;
    @XmlElement(name = "GuaMasterS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMasterS;
    @XmlElement(name = "NmerodeequipodecargaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipodecargaS;
    @XmlElement(name = "PzsPBPCS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pzsPBPCS;
    @XmlElement(name = "RequierePODS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String requierePODS;
    @XmlElement(name = "ShipperS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String shipperS;
    @XmlElement(name = "TipodeequipoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeequipoS;
    @XmlElement(name = "UbicacinCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCruceS;
    @XmlElement(name = "UbicacinDestinoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestinoS;
    @XmlElement(name = "UbicacinOrigenS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinOrigenS;
    @XmlElement(name = "UnidaddeventaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String unidaddeventaS;
    @XmlElement(name = "ConsignatarioCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioCruceS;
    @XmlElement(name = "ConsignatarioDestino1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarioDestino1;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Gets the value of the objectNodeSenderTechnicalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodeSenderTechnicalID() {
        return objectNodeSenderTechnicalID;
    }

    /**
     * Sets the value of the objectNodeSenderTechnicalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodeSenderTechnicalID(String value) {
        this.objectNodeSenderTechnicalID = value;
    }

    /**
     * Gets the value of the itemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Sets the value of the itemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Gets the value of the productKeyItem property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestProductKeyItem }
     *     
     */
    public PurchaseRequestMaintainRequestProductKeyItem getProductKeyItem() {
        return productKeyItem;
    }

    /**
     * Sets the value of the productKeyItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestProductKeyItem }
     *     
     */
    public void setProductKeyItem(PurchaseRequestMaintainRequestProductKeyItem value) {
        this.productKeyItem = value;
    }

    /**
     * Gets the value of the productCategoryKeyItem property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestProductCategoryIDKeyItem }
     *     
     */
    public PurchaseRequestMaintainRequestProductCategoryIDKeyItem getProductCategoryKeyItem() {
        return productCategoryKeyItem;
    }

    /**
     * Sets the value of the productCategoryKeyItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestProductCategoryIDKeyItem }
     *     
     */
    public void setProductCategoryKeyItem(PurchaseRequestMaintainRequestProductCategoryIDKeyItem value) {
        this.productCategoryKeyItem = value;
    }

    /**
     * Gets the value of the productDescription property.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getProductDescription() {
        return productDescription;
    }

    /**
     * Sets the value of the productDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setProductDescription(SHORTDescription value) {
        this.productDescription = value;
    }

    /**
     * Gets the value of the typeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCode() {
        return typeCode;
    }

    /**
     * Sets the value of the typeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCode(String value) {
        this.typeCode = value;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * Sets the value of the quantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setQuantity(Quantity value) {
        this.quantity = value;
    }

    /**
     * Gets the value of the deliveryPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod getDeliveryPeriod() {
        return deliveryPeriod;
    }

    /**
     * Sets the value of the deliveryPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public void setDeliveryPeriod(UPPEROPENLOCALNORMALISEDDateTimePeriod value) {
        this.deliveryPeriod = value;
    }

    /**
     * Gets the value of the shipToLocationID property.
     * 
     * @return
     *     possible object is
     *     {@link LocationID }
     *     
     */
    public LocationID getShipToLocationID() {
        return shipToLocationID;
    }

    /**
     * Sets the value of the shipToLocationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationID }
     *     
     */
    public void setShipToLocationID(LocationID value) {
        this.shipToLocationID = value;
    }

    /**
     * Gets the value of the limitValueAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getLimitValueAmount() {
        return limitValueAmount;
    }

    /**
     * Sets the value of the limitValueAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setLimitValueAmount(Amount value) {
        this.limitValueAmount = value;
    }

    /**
     * Gets the value of the listUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getListUnitPrice() {
        return listUnitPrice;
    }

    /**
     * Sets the value of the listUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setListUnitPrice(Price value) {
        this.listUnitPrice = value;
    }

    /**
     * Gets the value of the supplierPartNumberID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierPartNumberID() {
        return supplierPartNumberID;
    }

    /**
     * Sets the value of the supplierPartNumberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierPartNumberID(String value) {
        this.supplierPartNumberID = value;
    }

    /**
     * Gets the value of the siteID property.
     * 
     * @return
     *     possible object is
     *     {@link LocationID }
     *     
     */
    public LocationID getSiteID() {
        return siteID;
    }

    /**
     * Sets the value of the siteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationID }
     *     
     */
    public void setSiteID(LocationID value) {
        this.siteID = value;
    }

    /**
     * Gets the value of the companyIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getCompanyIDParty() {
        return companyIDParty;
    }

    /**
     * Sets the value of the companyIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setCompanyIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.companyIDParty = value;
    }

    /**
     * Gets the value of the requesterIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getRequesterIDParty() {
        return requesterIDParty;
    }

    /**
     * Sets the value of the requesterIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setRequesterIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.requesterIDParty = value;
    }

    /**
     * Gets the value of the recipientIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getRecipientIDParty() {
        return recipientIDParty;
    }

    /**
     * Sets the value of the recipientIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setRecipientIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.recipientIDParty = value;
    }

    /**
     * Gets the value of the buyerResponsibleIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getBuyerResponsibleIDParty() {
        return buyerResponsibleIDParty;
    }

    /**
     * Sets the value of the buyerResponsibleIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setBuyerResponsibleIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.buyerResponsibleIDParty = value;
    }

    /**
     * Gets the value of the purchasingUnitIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getPurchasingUnitIDParty() {
        return purchasingUnitIDParty;
    }

    /**
     * Sets the value of the purchasingUnitIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setPurchasingUnitIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.purchasingUnitIDParty = value;
    }

    /**
     * Gets the value of the supplierIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getSupplierIDParty() {
        return supplierIDParty;
    }

    /**
     * Sets the value of the supplierIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setSupplierIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.supplierIDParty = value;
    }

    /**
     * Gets the value of the serviceAgentIDParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getServiceAgentIDParty() {
        return serviceAgentIDParty;
    }

    /**
     * Sets the value of the serviceAgentIDParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setServiceAgentIDParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.serviceAgentIDParty = value;
    }

    /**
     * Gets the value of the itemPurchasingContractReferenceID property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference getItemPurchasingContractReferenceID() {
        return itemPurchasingContractReferenceID;
    }

    /**
     * Sets the value of the itemPurchasingContractReferenceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     *     
     */
    public void setItemPurchasingContractReferenceID(PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference value) {
        this.itemPurchasingContractReferenceID = value;
    }

    /**
     * Gets the value of the cancelOpenQuantityActionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelOpenQuantityActionIndicator() {
        return cancelOpenQuantityActionIndicator;
    }

    /**
     * Sets the value of the cancelOpenQuantityActionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelOpenQuantityActionIndicator(Boolean value) {
        this.cancelOpenQuantityActionIndicator = value;
    }

    /**
     * Gets the value of the reopenQuantityActionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReopenQuantityActionIndicator() {
        return reopenQuantityActionIndicator;
    }

    /**
     * Sets the value of the reopenQuantityActionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReopenQuantityActionIndicator(Boolean value) {
        this.reopenQuantityActionIndicator = value;
    }

    /**
     * Gets the value of the itemAccountAssignmentDistribution property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public MaintenanceAccountingCodingBlockDistribution getItemAccountAssignmentDistribution() {
        return itemAccountAssignmentDistribution;
    }

    /**
     * Sets the value of the itemAccountAssignmentDistribution property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public void setItemAccountAssignmentDistribution(MaintenanceAccountingCodingBlockDistribution value) {
        this.itemAccountAssignmentDistribution = value;
    }

    /**
     * Gets the value of the attachmentFolder property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getAttachmentFolder() {
        return attachmentFolder;
    }

    /**
     * Sets the value of the attachmentFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.attachmentFolder = value;
    }

    /**
     * Gets the value of the textCollection property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public MaintenanceTextCollection getTextCollection() {
        return textCollection;
    }

    /**
     * Sets the value of the textCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public void setTextCollection(MaintenanceTextCollection value) {
        this.textCollection = value;
    }

    /**
     * Gets the value of the directMaterialIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectMaterialIndicator() {
        return directMaterialIndicator;
    }

    /**
     * Sets the value of the directMaterialIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectMaterialIndicator(Boolean value) {
        this.directMaterialIndicator = value;
    }

    /**
     * Gets the value of the thirdPartyIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isThirdPartyIndicator() {
        return thirdPartyIndicator;
    }

    /**
     * Sets the value of the thirdPartyIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThirdPartyIndicator(Boolean value) {
        this.thirdPartyIndicator = value;
    }

    /**
     * Gets the value of the productRequirementSpecificationItem property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestProductRequirementSpecificationItem }
     *     
     */
    public PurchaseRequestMaintainRequestProductRequirementSpecificationItem getProductRequirementSpecificationItem() {
        return productRequirementSpecificationItem;
    }

    /**
     * Sets the value of the productRequirementSpecificationItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestProductRequirementSpecificationItem }
     *     
     */
    public void setProductRequirementSpecificationItem(PurchaseRequestMaintainRequestProductRequirementSpecificationItem value) {
        this.productRequirementSpecificationItem = value;
    }

    /**
     * Gets the value of the preferredSupplierParty property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseRequestMaintainRequestBundleItemParty getPreferredSupplierParty() {
        return preferredSupplierParty;
    }

    /**
     * Sets the value of the preferredSupplierParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestBundleItemParty }
     *     
     */
    public void setPreferredSupplierParty(PurchaseRequestMaintainRequestBundleItemParty value) {
        this.preferredSupplierParty = value;
    }

    /**
     * Gets the value of the itemPriceCalculationItem property.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseRequestMaintainRequestPriceCalculationItem }
     *     
     */
    public PurchaseRequestMaintainRequestPriceCalculationItem getItemPriceCalculationItem() {
        return itemPriceCalculationItem;
    }

    /**
     * Sets the value of the itemPriceCalculationItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseRequestMaintainRequestPriceCalculationItem }
     *     
     */
    public void setItemPriceCalculationItem(PurchaseRequestMaintainRequestPriceCalculationItem value) {
        this.itemPriceCalculationItem = value;
    }

    /**
     * Gets the value of the ndeservicio2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNdeservicio2() {
        return ndeservicio2;
    }

    /**
     * Sets the value of the ndeservicio2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNdeservicio2(String value) {
        this.ndeservicio2 = value;
    }

    /**
     * Gets the value of the consignatariosS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatariosS() {
        return consignatariosS;
    }

    /**
     * Sets the value of the consignatariosS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatariosS(String value) {
        this.consignatariosS = value;
    }

    /**
     * Gets the value of the fechaCargaS property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCargaS() {
        return fechaCargaS;
    }

    /**
     * Sets the value of the fechaCargaS property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCargaS(XMLGregorianCalendar value) {
        this.fechaCargaS = value;
    }

    /**
     * Gets the value of the fechaCruceS property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCruceS() {
        return fechaCruceS;
    }

    /**
     * Sets the value of the fechaCruceS property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCruceS(XMLGregorianCalendar value) {
        this.fechaCruceS = value;
    }

    /**
     * Gets the value of the fechaDestinoS property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestinoS() {
        return fechaDestinoS;
    }

    /**
     * Sets the value of the fechaDestinoS property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestinoS(XMLGregorianCalendar value) {
        this.fechaDestinoS = value;
    }

    /**
     * Gets the value of the fechadeAgenteAduanalS property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechadeAgenteAduanalS() {
        return fechadeAgenteAduanalS;
    }

    /**
     * Sets the value of the fechadeAgenteAduanalS property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechadeAgenteAduanalS(XMLGregorianCalendar value) {
        this.fechadeAgenteAduanalS = value;
    }

    /**
     * Gets the value of the guaHouseS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHouseS() {
        return guaHouseS;
    }

    /**
     * Sets the value of the guaHouseS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHouseS(String value) {
        this.guaHouseS = value;
    }

    /**
     * Gets the value of the guaMasterS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMasterS() {
        return guaMasterS;
    }

    /**
     * Sets the value of the guaMasterS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMasterS(String value) {
        this.guaMasterS = value;
    }

    /**
     * Gets the value of the nmerodeequipodecargaS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipodecargaS() {
        return nmerodeequipodecargaS;
    }

    /**
     * Sets the value of the nmerodeequipodecargaS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipodecargaS(String value) {
        this.nmerodeequipodecargaS = value;
    }

    /**
     * Gets the value of the pzsPBPCS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPzsPBPCS() {
        return pzsPBPCS;
    }

    /**
     * Sets the value of the pzsPBPCS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPzsPBPCS(String value) {
        this.pzsPBPCS = value;
    }

    /**
     * Gets the value of the requierePODS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequierePODS() {
        return requierePODS;
    }

    /**
     * Sets the value of the requierePODS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequierePODS(String value) {
        this.requierePODS = value;
    }

    /**
     * Gets the value of the shipperS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperS() {
        return shipperS;
    }

    /**
     * Sets the value of the shipperS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperS(String value) {
        this.shipperS = value;
    }

    /**
     * Gets the value of the tipodeequipoS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeequipoS() {
        return tipodeequipoS;
    }

    /**
     * Sets the value of the tipodeequipoS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeequipoS(String value) {
        this.tipodeequipoS = value;
    }

    /**
     * Gets the value of the ubicacinCruceS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCruceS() {
        return ubicacinCruceS;
    }

    /**
     * Sets the value of the ubicacinCruceS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCruceS(String value) {
        this.ubicacinCruceS = value;
    }

    /**
     * Gets the value of the ubicacinDestinoS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestinoS() {
        return ubicacinDestinoS;
    }

    /**
     * Sets the value of the ubicacinDestinoS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestinoS(String value) {
        this.ubicacinDestinoS = value;
    }

    /**
     * Gets the value of the ubicacinOrigenS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinOrigenS() {
        return ubicacinOrigenS;
    }

    /**
     * Sets the value of the ubicacinOrigenS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinOrigenS(String value) {
        this.ubicacinOrigenS = value;
    }

    /**
     * Gets the value of the unidaddeventaS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidaddeventaS() {
        return unidaddeventaS;
    }

    /**
     * Sets the value of the unidaddeventaS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidaddeventaS(String value) {
        this.unidaddeventaS = value;
    }

    /**
     * Gets the value of the consignatarioCruceS property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioCruceS() {
        return consignatarioCruceS;
    }

    /**
     * Sets the value of the consignatarioCruceS property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioCruceS(String value) {
        this.consignatarioCruceS = value;
    }

    /**
     * Gets the value of the consignatarioDestino1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarioDestino1() {
        return consignatarioDestino1;
    }

    /**
     * Sets the value of the consignatarioDestino1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarioDestino1(String value) {
        this.consignatarioDestino1 = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
