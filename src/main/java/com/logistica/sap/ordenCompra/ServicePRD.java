package com.logistica.sap.ordenCompra;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "service", targetNamespace = "http://sap.com/xi/A1S/Global", wsdlLocation = "file:/E:/WS_DEV/ManagePurchaseRequestIn_1PRD.wsdl")
public class ServicePRD
    extends javax.xml.ws.Service
{

    private final static URL SERVICE_WSDL_LOCATION;
    private final static WebServiceException SERVICE_EXCEPTION;
    private final static QName SERVICE_QNAME = new QName("http://sap.com/xi/A1S/Global", "service");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/E:/WS_DEV/ManagePurchaseRequestIn_1PRD.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SERVICE_WSDL_LOCATION = url;
        SERVICE_EXCEPTION = e;
    }

    public ServicePRD() {
        super(__getWsdlLocation(), SERVICE_QNAME);
    }

    public ServicePRD(WebServiceFeature... features) {
        super(__getWsdlLocation(), SERVICE_QNAME, features);
    }

    public ServicePRD(URL wsdlLocation) {
        super(wsdlLocation, SERVICE_QNAME);
    }

    public ServicePRD(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICE_QNAME, features);
    }

    public ServicePRD(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ServicePRD(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ManagePurchaseRequestIn
     */
    @WebEndpoint(name = "binding")
    public ManagePurchaseRequestIn getBinding() {
        return super.getPort(new QName("http://sap.com/xi/A1S/Global", "binding"), ManagePurchaseRequestIn.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ManagePurchaseRequestIn
     */
    @WebEndpoint(name = "binding")
    public ManagePurchaseRequestIn getBinding(WebServiceFeature... features) {
        return super.getPort(new QName("http://sap.com/xi/A1S/Global", "binding"), ManagePurchaseRequestIn.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SERVICE_EXCEPTION!= null) {
            throw SERVICE_EXCEPTION;
        }
        return SERVICE_WSDL_LOCATION;
    }

}

