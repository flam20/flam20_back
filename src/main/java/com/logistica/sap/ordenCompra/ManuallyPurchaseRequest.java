
package com.logistica.sap.ordenCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ManuallyPurchaseRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ManuallyPurchaseRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodeSenderTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ChangeStateID" type="{http://sap.com/xi/AP/Common/GDT}ChangeStateID" minOccurs="0"/>
 *         &lt;element name="PurchaseRequestID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="PurchaseRequestUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="Item" type="{http://sap.com/xi/AP/Purchasing/Global}PurchaseRequestItemManually" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3A84D6233D23B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3AE9DBD70D23E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3B073F86F526B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3B2A3E191926E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3B4ACCB951271"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3BA42B1A01279"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3BC5F1873D27C"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3BEAD4C1B927F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3C205D1449281"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3C40581591281"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3C630239912B1"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3C825A77A92B2"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3CA6A197E12B3"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E78A14C1ED9BAF3CC6035A392B4"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *       &lt;attribute name="itemListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManuallyPurchaseRequest", namespace = "http://sap.com/xi/AP/Purchasing/Global", propOrder = {
    "objectNodeSenderTechnicalID",
    "changeStateID",
    "purchaseRequestID",
    "purchaseRequestUUID",
    "item",
    "attachmentFolder",
    "consignatarios1",
    "fechaCarga",
    "fechaCruce",
    "fechaDestino",
    "fechadeAgenteAduanal",
    "guaHouse1",
    "guaMaster1",
    "nmerodeequipodecarga1",
    "pzsPBPC1",
    "shipper1",
    "tipodeequipo1",
    "ubicacinCruce1",
    "ubicacinDestino1",
    "ubicacinOrigen"
})
public class ManuallyPurchaseRequest {

    @XmlElement(name = "ObjectNodeSenderTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String objectNodeSenderTechnicalID;
    @XmlElement(name = "ChangeStateID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String changeStateID;
    @XmlElement(name = "PurchaseRequestID")
    protected BusinessTransactionDocumentID purchaseRequestID;
    @XmlElement(name = "PurchaseRequestUUID")
    protected UUID purchaseRequestUUID;
    @XmlElement(name = "Item")
    protected List<PurchaseRequestItemManually> item;
    @XmlElement(name = "AttachmentFolder")
    protected MaintenanceAttachmentFolder attachmentFolder;
    @XmlElement(name = "Consignatarios1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatarios1;
    @XmlElement(name = "FechaCarga", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCarga;
    @XmlElement(name = "FechaCruce", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaCruce;
    @XmlElement(name = "FechaDestino", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechaDestino;
    @XmlElement(name = "FechadeAgenteAduanal", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected XMLGregorianCalendar fechadeAgenteAduanal;
    @XmlElement(name = "GuaHouse1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHouse1;
    @XmlElement(name = "GuaMaster1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMaster1;
    @XmlElement(name = "Nmerodeequipodecarga1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipodecarga1;
    @XmlElement(name = "PzsPBPC1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pzsPBPC1;
    @XmlElement(name = "Shipper1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String shipper1;
    @XmlElement(name = "Tipodeequipo1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeequipo1;
    @XmlElement(name = "UbicacinCruce1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCruce1;
    @XmlElement(name = "UbicacinDestino1", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestino1;
    @XmlElement(name = "UbicacinOrigen", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinOrigen;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;
    @XmlAttribute(name = "itemListCompleteTransmissionIndicator")
    protected Boolean itemListCompleteTransmissionIndicator;

    /**
     * Gets the value of the objectNodeSenderTechnicalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodeSenderTechnicalID() {
        return objectNodeSenderTechnicalID;
    }

    /**
     * Sets the value of the objectNodeSenderTechnicalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodeSenderTechnicalID(String value) {
        this.objectNodeSenderTechnicalID = value;
    }

    /**
     * Gets the value of the changeStateID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeStateID() {
        return changeStateID;
    }

    /**
     * Sets the value of the changeStateID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeStateID(String value) {
        this.changeStateID = value;
    }

    /**
     * Gets the value of the purchaseRequestID property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getPurchaseRequestID() {
        return purchaseRequestID;
    }

    /**
     * Sets the value of the purchaseRequestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setPurchaseRequestID(BusinessTransactionDocumentID value) {
        this.purchaseRequestID = value;
    }

    /**
     * Gets the value of the purchaseRequestUUID property.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getPurchaseRequestUUID() {
        return purchaseRequestUUID;
    }

    /**
     * Sets the value of the purchaseRequestUUID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setPurchaseRequestUUID(UUID value) {
        this.purchaseRequestUUID = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseRequestItemManually }
     * 
     * 
     */
    public List<PurchaseRequestItemManually> getItem() {
        if (item == null) {
            item = new ArrayList<PurchaseRequestItemManually>();
        }
        return this.item;
    }

    /**
     * Gets the value of the attachmentFolder property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getAttachmentFolder() {
        return attachmentFolder;
    }

    /**
     * Sets the value of the attachmentFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.attachmentFolder = value;
    }

    /**
     * Gets the value of the consignatarios1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatarios1() {
        return consignatarios1;
    }

    /**
     * Sets the value of the consignatarios1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatarios1(String value) {
        this.consignatarios1 = value;
    }

    /**
     * Gets the value of the fechaCarga property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCarga() {
        return fechaCarga;
    }

    /**
     * Sets the value of the fechaCarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCarga(XMLGregorianCalendar value) {
        this.fechaCarga = value;
    }

    /**
     * Gets the value of the fechaCruce property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCruce() {
        return fechaCruce;
    }

    /**
     * Sets the value of the fechaCruce property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCruce(XMLGregorianCalendar value) {
        this.fechaCruce = value;
    }

    /**
     * Gets the value of the fechaDestino property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestino() {
        return fechaDestino;
    }

    /**
     * Sets the value of the fechaDestino property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestino(XMLGregorianCalendar value) {
        this.fechaDestino = value;
    }

    /**
     * Gets the value of the fechadeAgenteAduanal property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechadeAgenteAduanal() {
        return fechadeAgenteAduanal;
    }

    /**
     * Sets the value of the fechadeAgenteAduanal property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechadeAgenteAduanal(XMLGregorianCalendar value) {
        this.fechadeAgenteAduanal = value;
    }

    /**
     * Gets the value of the guaHouse1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHouse1() {
        return guaHouse1;
    }

    /**
     * Sets the value of the guaHouse1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHouse1(String value) {
        this.guaHouse1 = value;
    }

    /**
     * Gets the value of the guaMaster1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMaster1() {
        return guaMaster1;
    }

    /**
     * Sets the value of the guaMaster1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMaster1(String value) {
        this.guaMaster1 = value;
    }

    /**
     * Gets the value of the nmerodeequipodecarga1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipodecarga1() {
        return nmerodeequipodecarga1;
    }

    /**
     * Sets the value of the nmerodeequipodecarga1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipodecarga1(String value) {
        this.nmerodeequipodecarga1 = value;
    }

    /**
     * Gets the value of the pzsPBPC1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPzsPBPC1() {
        return pzsPBPC1;
    }

    /**
     * Sets the value of the pzsPBPC1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPzsPBPC1(String value) {
        this.pzsPBPC1 = value;
    }

    /**
     * Gets the value of the shipper1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipper1() {
        return shipper1;
    }

    /**
     * Sets the value of the shipper1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipper1(String value) {
        this.shipper1 = value;
    }

    /**
     * Gets the value of the tipodeequipo1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeequipo1() {
        return tipodeequipo1;
    }

    /**
     * Sets the value of the tipodeequipo1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeequipo1(String value) {
        this.tipodeequipo1 = value;
    }

    /**
     * Gets the value of the ubicacinCruce1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCruce1() {
        return ubicacinCruce1;
    }

    /**
     * Sets the value of the ubicacinCruce1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCruce1(String value) {
        this.ubicacinCruce1 = value;
    }

    /**
     * Gets the value of the ubicacinDestino1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestino1() {
        return ubicacinDestino1;
    }

    /**
     * Sets the value of the ubicacinDestino1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestino1(String value) {
        this.ubicacinDestino1 = value;
    }

    /**
     * Gets the value of the ubicacinOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinOrigen() {
        return ubicacinOrigen;
    }

    /**
     * Sets the value of the ubicacinOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinOrigen(String value) {
        this.ubicacinOrigen = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the itemListCompleteTransmissionIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemListCompleteTransmissionIndicator() {
        return itemListCompleteTransmissionIndicator;
    }

    /**
     * Sets the value of the itemListCompleteTransmissionIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemListCompleteTransmissionIndicator(Boolean value) {
        this.itemListCompleteTransmissionIndicator = value;
    }

}
