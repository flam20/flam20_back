
package com.logistica.sap.ordenCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PurchaseRequestResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseRequestResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseRequestResponse" type="{http://sap.com/xi/AP/Purchasing/Global}ManuallyPurchaseRequestResponse" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Log" type="{http://sap.com/xi/AP/Common/GDT}Log"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseRequestResponse", namespace = "http://sap.com/xi/AP/Purchasing/Global", propOrder = {
    "purchaseRequestResponse",
    "log"
})
public class PurchaseRequestResponse {

    @XmlElement(name = "PurchaseRequestResponse")
    protected List<ManuallyPurchaseRequestResponse> purchaseRequestResponse;
    @XmlElement(name = "Log", required = true)
    protected Log log;

    /**
     * Gets the value of the purchaseRequestResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the purchaseRequestResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPurchaseRequestResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ManuallyPurchaseRequestResponse }
     * 
     * 
     */
    public List<ManuallyPurchaseRequestResponse> getPurchaseRequestResponse() {
        if (purchaseRequestResponse == null) {
            purchaseRequestResponse = new ArrayList<ManuallyPurchaseRequestResponse>();
        }
        return this.purchaseRequestResponse;
    }

    /**
     * Gets the value of the log property.
     * 
     * @return
     *     possible object is
     *     {@link Log }
     *     
     */
    public Log getLog() {
        return log;
    }

    /**
     * Sets the value of the log property.
     * 
     * @param value
     *     allowed object is
     *     {@link Log }
     *     
     */
    public void setLog(Log value) {
        this.log = value;
    }

}
