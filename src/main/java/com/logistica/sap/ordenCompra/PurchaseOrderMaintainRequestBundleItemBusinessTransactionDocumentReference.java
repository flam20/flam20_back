
package com.logistica.sap.ordenCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodePartyTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReference" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentReference" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ActionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodePartyTechnicalID",
    "businessTransactionDocumentReference"
})
public class PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference {

    @XmlElement(name = "ObjectNodePartyTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String objectNodePartyTechnicalID;
    @XmlElement(name = "BusinessTransactionDocumentReference")
    protected BusinessTransactionDocumentReference businessTransactionDocumentReference;
    @XmlAttribute(name = "ActionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Gets the value of the objectNodePartyTechnicalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodePartyTechnicalID() {
        return objectNodePartyTechnicalID;
    }

    /**
     * Sets the value of the objectNodePartyTechnicalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodePartyTechnicalID(String value) {
        this.objectNodePartyTechnicalID = value;
    }

    /**
     * Gets the value of the businessTransactionDocumentReference property.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public BusinessTransactionDocumentReference getBusinessTransactionDocumentReference() {
        return businessTransactionDocumentReference;
    }

    /**
     * Sets the value of the businessTransactionDocumentReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public void setBusinessTransactionDocumentReference(BusinessTransactionDocumentReference value) {
        this.businessTransactionDocumentReference = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
