package com.logistica.sap.handler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

public class PedidoClienteHandlerResolver implements HandlerResolver {

	public List getHandlerChain(PortInfo portInfo) {
		ArrayList<PedidoClienteHandler> handlerChain = new ArrayList<PedidoClienteHandler>();
		handlerChain.add(new PedidoClienteHandler());
		return handlerChain;
	}

}