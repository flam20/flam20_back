package com.logistica.sap.handler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

public class OrdenCompraHandlerResolver implements HandlerResolver {

	public List getHandlerChain(PortInfo portInfo) {
		ArrayList<OrdenCompraHandler> handlerChain = new ArrayList<OrdenCompraHandler>();
		handlerChain.add(new OrdenCompraHandler());
		return handlerChain;
	}

}
