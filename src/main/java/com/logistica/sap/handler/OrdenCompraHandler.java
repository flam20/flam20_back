package com.logistica.sap.handler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;


public class OrdenCompraHandler implements SOAPHandler<SOAPMessageContext> {
	static Logger logger = Logger.getLogger(OrdenCompraHandler.class.getName());

	public boolean handleMessage(SOAPMessageContext smc) {

		Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		SOAPMessage message = smc.getMessage();
		try {
			if (outboundProperty.booleanValue()) {
				logger.info("ORDEN COMPRA REQUEST-->" + convierte(message));
			} else {
				logger.info("ORDEN COMPRA RESPONSE-->" + convierte(message));
			}
		} catch (Exception e) {
			logger.error("Error en ejecución de ORDEN DE COMPRA:" + e.getMessage(), e);
		}
		return true;

	}

	public Set getHeaders() {
		return null;
	}

	public boolean handleFault(SOAPMessageContext context) {
		return true;
	}

	public void close(MessageContext context) {
	}

	private String convierte(SOAPMessage message) throws SOAPException, IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		message.writeTo(out);
		return out.toString();
	}

}
