package com.logistica.sap.handler;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

public class PedidoCompraHandlerResolver implements HandlerResolver {

	public List getHandlerChain(PortInfo portInfo) {
		ArrayList<PedidoCompraHandler> handlerChain = new ArrayList<PedidoCompraHandler>();
		handlerChain.add(new PedidoCompraHandler());
		return handlerChain;
	}

}
