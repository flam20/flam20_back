
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para AccountingObjectCheckItemCostObjectReference complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AccountingObjectCheckItemCostObjectReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CostObjectTypeCode" type="{http://sap.com/xi/AP/Common/GDT}CostObjectTypeCode" minOccurs="0"/>
 *         &lt;element name="CostObjectID" type="{http://sap.com/xi/AP/Common/GDT}FinancialAccountingViewOfCostObjectID" minOccurs="0"/>
 *         &lt;element name="CostObjectUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="CostObjectDescription" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Description" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountingObjectCheckItemCostObjectReference", namespace = "http://sap.com/xi/AP/IS/CodingBlock/Global", propOrder = {
    "costObjectTypeCode",
    "costObjectID",
    "costObjectUUID",
    "costObjectDescription"
})
public class AccountingObjectCheckItemCostObjectReference {

    @XmlElement(name = "CostObjectTypeCode")
    protected CostObjectTypeCode costObjectTypeCode;
    @XmlElement(name = "CostObjectID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String costObjectID;
    @XmlElement(name = "CostObjectUUID")
    protected UUID costObjectUUID;
    @XmlElement(name = "CostObjectDescription")
    protected MEDIUMDescription costObjectDescription;

    /**
     * Obtiene el valor de la propiedad costObjectTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link CostObjectTypeCode }
     *     
     */
    public CostObjectTypeCode getCostObjectTypeCode() {
        return costObjectTypeCode;
    }

    /**
     * Define el valor de la propiedad costObjectTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CostObjectTypeCode }
     *     
     */
    public void setCostObjectTypeCode(CostObjectTypeCode value) {
        this.costObjectTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad costObjectID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostObjectID() {
        return costObjectID;
    }

    /**
     * Define el valor de la propiedad costObjectID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostObjectID(String value) {
        this.costObjectID = value;
    }

    /**
     * Obtiene el valor de la propiedad costObjectUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getCostObjectUUID() {
        return costObjectUUID;
    }

    /**
     * Define el valor de la propiedad costObjectUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setCostObjectUUID(UUID value) {
        this.costObjectUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad costObjectDescription.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMDescription }
     *     
     */
    public MEDIUMDescription getCostObjectDescription() {
        return costObjectDescription;
    }

    /**
     * Define el valor de la propiedad costObjectDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMDescription }
     *     
     */
    public void setCostObjectDescription(MEDIUMDescription value) {
        this.costObjectDescription = value;
    }

}
