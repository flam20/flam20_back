
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para RequirementSpecificationKey complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequirementSpecificationKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequirementSpecificationID" type="{http://sap.com/xi/AP/Common/GDT}RequirementSpecificationID"/>
 *         &lt;element name="RequirementSpecificationVersionID" type="{http://sap.com/xi/AP/Common/GDT}VersionID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequirementSpecificationKey", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "requirementSpecificationID",
    "requirementSpecificationVersionID"
})
public class RequirementSpecificationKey {

    @XmlElement(name = "RequirementSpecificationID", required = true)
    protected RequirementSpecificationID requirementSpecificationID;
    @XmlElement(name = "RequirementSpecificationVersionID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String requirementSpecificationVersionID;

    /**
     * Obtiene el valor de la propiedad requirementSpecificationID.
     * 
     * @return
     *     possible object is
     *     {@link RequirementSpecificationID }
     *     
     */
    public RequirementSpecificationID getRequirementSpecificationID() {
        return requirementSpecificationID;
    }

    /**
     * Define el valor de la propiedad requirementSpecificationID.
     * 
     * @param value
     *     allowed object is
     *     {@link RequirementSpecificationID }
     *     
     */
    public void setRequirementSpecificationID(RequirementSpecificationID value) {
        this.requirementSpecificationID = value;
    }

    /**
     * Obtiene el valor de la propiedad requirementSpecificationVersionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequirementSpecificationVersionID() {
        return requirementSpecificationVersionID;
    }

    /**
     * Define el valor de la propiedad requirementSpecificationVersionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequirementSpecificationVersionID(String value) {
        this.requirementSpecificationVersionID = value;
    }

}
