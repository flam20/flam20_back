
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestLocationAddressDisplayName complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestLocationAddressDisplayName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormattedName" type="{http://sap.com/xi/AP/Common/GDT}LONG_Name" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestLocationAddressDisplayName", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "formattedName"
})
public class PurchaseOrderMaintainRequestLocationAddressDisplayName {

    @XmlElement(name = "FormattedName")
    protected LONGName formattedName;

    /**
     * Obtiene el valor de la propiedad formattedName.
     * 
     * @return
     *     possible object is
     *     {@link LONGName }
     *     
     */
    public LONGName getFormattedName() {
        return formattedName;
    }

    /**
     * Define el valor de la propiedad formattedName.
     * 
     * @param value
     *     allowed object is
     *     {@link LONGName }
     *     
     */
    public void setFormattedName(LONGName value) {
        this.formattedName = value;
    }

}
