
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para AccessCashDiscountTerms complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AccessCashDiscountTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="Code" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountTermsCode" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="CashDiscountLevelCode" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountLevelCode" minOccurs="0"/>
 *         &lt;element name="CashDiscountLevelName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="PaymentBaselineDate" type="{http://sap.com/xi/BASIS/Global}Date" minOccurs="0"/>
 *         &lt;element name="MaximumCashDiscount" type="{http://sap.com/xi/AP/Common/GDT}CashDiscount" minOccurs="0"/>
 *         &lt;element name="NormalCashDiscount" type="{http://sap.com/xi/AP/Common/GDT}CashDiscount" minOccurs="0"/>
 *         &lt;element name="FullPaymentDueDaysValue" type="{http://sap.com/xi/AP/Common/GDT}FullPaymentCashDiscountTermsDueDaysValue" minOccurs="0"/>
 *         &lt;element name="FullPaymentDayOfMonthValue" type="{http://sap.com/xi/AP/Common/GDT}FullPaymentCashDiscountTermsDayOfMonthValue" minOccurs="0"/>
 *         &lt;element name="FullPaymentMonthOffsetValue" type="{http://sap.com/xi/AP/Common/GDT}FullPaymentCashDiscountTermsMonthOffsetValue" minOccurs="0"/>
 *         &lt;element name="FullPaymentEndDate" type="{http://sap.com/xi/BASIS/Global}Date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessCashDiscountTerms", namespace = "http://sap.com/xi/AP/FO/CashDiscountTerms/Global", propOrder = {
    "uuid",
    "code",
    "name",
    "cashDiscountLevelCode",
    "cashDiscountLevelName",
    "paymentBaselineDate",
    "maximumCashDiscount",
    "normalCashDiscount",
    "fullPaymentDueDaysValue",
    "fullPaymentDayOfMonthValue",
    "fullPaymentMonthOffsetValue",
    "fullPaymentEndDate"
})
public class AccessCashDiscountTerms {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "Code")
    protected CashDiscountTermsCode code;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "CashDiscountLevelCode")
    protected CashDiscountLevelCode cashDiscountLevelCode;
    @XmlElement(name = "CashDiscountLevelName")
    protected String cashDiscountLevelName;
    @XmlElement(name = "PaymentBaselineDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar paymentBaselineDate;
    @XmlElement(name = "MaximumCashDiscount")
    protected CashDiscount maximumCashDiscount;
    @XmlElement(name = "NormalCashDiscount")
    protected CashDiscount normalCashDiscount;
    @XmlElement(name = "FullPaymentDueDaysValue")
    protected Integer fullPaymentDueDaysValue;
    @XmlElement(name = "FullPaymentDayOfMonthValue")
    protected Integer fullPaymentDayOfMonthValue;
    @XmlElement(name = "FullPaymentMonthOffsetValue")
    protected Integer fullPaymentMonthOffsetValue;
    @XmlElement(name = "FullPaymentEndDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fullPaymentEndDate;

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link CashDiscountTermsCode }
     *     
     */
    public CashDiscountTermsCode getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link CashDiscountTermsCode }
     *     
     */
    public void setCode(CashDiscountTermsCode value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad cashDiscountLevelCode.
     * 
     * @return
     *     possible object is
     *     {@link CashDiscountLevelCode }
     *     
     */
    public CashDiscountLevelCode getCashDiscountLevelCode() {
        return cashDiscountLevelCode;
    }

    /**
     * Define el valor de la propiedad cashDiscountLevelCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CashDiscountLevelCode }
     *     
     */
    public void setCashDiscountLevelCode(CashDiscountLevelCode value) {
        this.cashDiscountLevelCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cashDiscountLevelName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashDiscountLevelName() {
        return cashDiscountLevelName;
    }

    /**
     * Define el valor de la propiedad cashDiscountLevelName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashDiscountLevelName(String value) {
        this.cashDiscountLevelName = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentBaselineDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaymentBaselineDate() {
        return paymentBaselineDate;
    }

    /**
     * Define el valor de la propiedad paymentBaselineDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaymentBaselineDate(XMLGregorianCalendar value) {
        this.paymentBaselineDate = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumCashDiscount.
     * 
     * @return
     *     possible object is
     *     {@link CashDiscount }
     *     
     */
    public CashDiscount getMaximumCashDiscount() {
        return maximumCashDiscount;
    }

    /**
     * Define el valor de la propiedad maximumCashDiscount.
     * 
     * @param value
     *     allowed object is
     *     {@link CashDiscount }
     *     
     */
    public void setMaximumCashDiscount(CashDiscount value) {
        this.maximumCashDiscount = value;
    }

    /**
     * Obtiene el valor de la propiedad normalCashDiscount.
     * 
     * @return
     *     possible object is
     *     {@link CashDiscount }
     *     
     */
    public CashDiscount getNormalCashDiscount() {
        return normalCashDiscount;
    }

    /**
     * Define el valor de la propiedad normalCashDiscount.
     * 
     * @param value
     *     allowed object is
     *     {@link CashDiscount }
     *     
     */
    public void setNormalCashDiscount(CashDiscount value) {
        this.normalCashDiscount = value;
    }

    /**
     * Obtiene el valor de la propiedad fullPaymentDueDaysValue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFullPaymentDueDaysValue() {
        return fullPaymentDueDaysValue;
    }

    /**
     * Define el valor de la propiedad fullPaymentDueDaysValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFullPaymentDueDaysValue(Integer value) {
        this.fullPaymentDueDaysValue = value;
    }

    /**
     * Obtiene el valor de la propiedad fullPaymentDayOfMonthValue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFullPaymentDayOfMonthValue() {
        return fullPaymentDayOfMonthValue;
    }

    /**
     * Define el valor de la propiedad fullPaymentDayOfMonthValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFullPaymentDayOfMonthValue(Integer value) {
        this.fullPaymentDayOfMonthValue = value;
    }

    /**
     * Obtiene el valor de la propiedad fullPaymentMonthOffsetValue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFullPaymentMonthOffsetValue() {
        return fullPaymentMonthOffsetValue;
    }

    /**
     * Define el valor de la propiedad fullPaymentMonthOffsetValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFullPaymentMonthOffsetValue(Integer value) {
        this.fullPaymentMonthOffsetValue = value;
    }

    /**
     * Obtiene el valor de la propiedad fullPaymentEndDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFullPaymentEndDate() {
        return fullPaymentEndDate;
    }

    /**
     * Define el valor de la propiedad fullPaymentEndDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFullPaymentEndDate(XMLGregorianCalendar value) {
        this.fullPaymentEndDate = value;
    }

}
