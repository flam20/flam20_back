
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PartyKey complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PartyKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartyTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessObjectTypeCode" minOccurs="0"/>
 *         &lt;element name="PartyID" type="{http://sap.com/xi/AP/Common/GDT}PartyID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyKey", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "partyTypeCode",
    "partyID"
})
public class PartyKey {

    @XmlElement(name = "PartyTypeCode")
    protected BusinessObjectTypeCode partyTypeCode;
    @XmlElement(name = "PartyID")
    protected PartyID partyID;

    /**
     * Obtiene el valor de la propiedad partyTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link BusinessObjectTypeCode }
     *     
     */
    public BusinessObjectTypeCode getPartyTypeCode() {
        return partyTypeCode;
    }

    /**
     * Define el valor de la propiedad partyTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessObjectTypeCode }
     *     
     */
    public void setPartyTypeCode(BusinessObjectTypeCode value) {
        this.partyTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad partyID.
     * 
     * @return
     *     possible object is
     *     {@link PartyID }
     *     
     */
    public PartyID getPartyID() {
        return partyID;
    }

    /**
     * Define el valor de la propiedad partyID.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyID }
     *     
     */
    public void setPartyID(PartyID value) {
        this.partyID = value;
    }

}
