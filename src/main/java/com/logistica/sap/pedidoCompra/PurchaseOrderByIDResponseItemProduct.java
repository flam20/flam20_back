
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItemProduct complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItemProduct">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodePartyTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ProductStandardID" type="{http://sap.com/xi/AP/Common/GDT}ProductStandardID" minOccurs="0"/>
 *         &lt;element name="ProductCategoryStandardID" type="{http://sap.com/xi/AP/Common/GDT}ProductCategoryStandardID" minOccurs="0"/>
 *         &lt;element name="CashDiscountDeductibleIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator"/>
 *         &lt;element name="ProductCategoryIDKey" type="{http://sap.com/xi/AP/Common/Global}ProductCategoryHierarchyProductCategoryIDKey" minOccurs="0"/>
 *         &lt;element name="ProductKey" type="{http://sap.com/xi/AP/Common/Global}ProductKey" minOccurs="0"/>
 *         &lt;element name="ProductRequirementSpecificationKey" type="{http://sap.com/xi/AP/Common/Global}RequirementSpecificationKey" minOccurs="0"/>
 *         &lt;element name="ProductSellerID" type="{http://sap.com/xi/AP/Common/GDT}ProductPartyID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItemProduct", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodePartyTechnicalID",
    "productStandardID",
    "productCategoryStandardID",
    "cashDiscountDeductibleIndicator",
    "productCategoryIDKey",
    "productKey",
    "productRequirementSpecificationKey",
    "productSellerID"
})
public class PurchaseOrderByIDResponseItemProduct {

    @XmlElement(name = "ObjectNodePartyTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String objectNodePartyTechnicalID;
    @XmlElement(name = "ProductStandardID")
    protected ProductStandardID productStandardID;
    @XmlElement(name = "ProductCategoryStandardID")
    protected ProductCategoryStandardID productCategoryStandardID;
    @XmlElement(name = "CashDiscountDeductibleIndicator")
    protected boolean cashDiscountDeductibleIndicator;
    @XmlElement(name = "ProductCategoryIDKey")
    protected ProductCategoryHierarchyProductCategoryIDKey productCategoryIDKey;
    @XmlElement(name = "ProductKey")
    protected ProductKey productKey;
    @XmlElement(name = "ProductRequirementSpecificationKey")
    protected RequirementSpecificationKey productRequirementSpecificationKey;
    @XmlElement(name = "ProductSellerID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String productSellerID;

    /**
     * Obtiene el valor de la propiedad objectNodePartyTechnicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodePartyTechnicalID() {
        return objectNodePartyTechnicalID;
    }

    /**
     * Define el valor de la propiedad objectNodePartyTechnicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodePartyTechnicalID(String value) {
        this.objectNodePartyTechnicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad productStandardID.
     * 
     * @return
     *     possible object is
     *     {@link ProductStandardID }
     *     
     */
    public ProductStandardID getProductStandardID() {
        return productStandardID;
    }

    /**
     * Define el valor de la propiedad productStandardID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductStandardID }
     *     
     */
    public void setProductStandardID(ProductStandardID value) {
        this.productStandardID = value;
    }

    /**
     * Obtiene el valor de la propiedad productCategoryStandardID.
     * 
     * @return
     *     possible object is
     *     {@link ProductCategoryStandardID }
     *     
     */
    public ProductCategoryStandardID getProductCategoryStandardID() {
        return productCategoryStandardID;
    }

    /**
     * Define el valor de la propiedad productCategoryStandardID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCategoryStandardID }
     *     
     */
    public void setProductCategoryStandardID(ProductCategoryStandardID value) {
        this.productCategoryStandardID = value;
    }

    /**
     * Obtiene el valor de la propiedad cashDiscountDeductibleIndicator.
     * 
     */
    public boolean isCashDiscountDeductibleIndicator() {
        return cashDiscountDeductibleIndicator;
    }

    /**
     * Define el valor de la propiedad cashDiscountDeductibleIndicator.
     * 
     */
    public void setCashDiscountDeductibleIndicator(boolean value) {
        this.cashDiscountDeductibleIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad productCategoryIDKey.
     * 
     * @return
     *     possible object is
     *     {@link ProductCategoryHierarchyProductCategoryIDKey }
     *     
     */
    public ProductCategoryHierarchyProductCategoryIDKey getProductCategoryIDKey() {
        return productCategoryIDKey;
    }

    /**
     * Define el valor de la propiedad productCategoryIDKey.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCategoryHierarchyProductCategoryIDKey }
     *     
     */
    public void setProductCategoryIDKey(ProductCategoryHierarchyProductCategoryIDKey value) {
        this.productCategoryIDKey = value;
    }

    /**
     * Obtiene el valor de la propiedad productKey.
     * 
     * @return
     *     possible object is
     *     {@link ProductKey }
     *     
     */
    public ProductKey getProductKey() {
        return productKey;
    }

    /**
     * Define el valor de la propiedad productKey.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductKey }
     *     
     */
    public void setProductKey(ProductKey value) {
        this.productKey = value;
    }

    /**
     * Obtiene el valor de la propiedad productRequirementSpecificationKey.
     * 
     * @return
     *     possible object is
     *     {@link RequirementSpecificationKey }
     *     
     */
    public RequirementSpecificationKey getProductRequirementSpecificationKey() {
        return productRequirementSpecificationKey;
    }

    /**
     * Define el valor de la propiedad productRequirementSpecificationKey.
     * 
     * @param value
     *     allowed object is
     *     {@link RequirementSpecificationKey }
     *     
     */
    public void setProductRequirementSpecificationKey(RequirementSpecificationKey value) {
        this.productRequirementSpecificationKey = value;
    }

    /**
     * Obtiene el valor de la propiedad productSellerID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductSellerID() {
        return productSellerID;
    }

    /**
     * Define el valor de la propiedad productSellerID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductSellerID(String value) {
        this.productSellerID = value;
    }

}
