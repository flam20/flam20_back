
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItemFollowUpDelivery complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItemFollowUpDelivery">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequirementCode" type="{http://sap.com/xi/AP/Common/GDT}FollowUpBusinessTransactionDocumentRequirementCode"/>
 *         &lt;element name="EmployeeTimeConfirmationRequiredIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItemFollowUpDelivery", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "requirementCode",
    "employeeTimeConfirmationRequiredIndicator"
})
public class PurchaseOrderByIDResponseItemFollowUpDelivery {

    @XmlElement(name = "RequirementCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String requirementCode;
    @XmlElement(name = "EmployeeTimeConfirmationRequiredIndicator")
    protected boolean employeeTimeConfirmationRequiredIndicator;

    /**
     * Obtiene el valor de la propiedad requirementCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequirementCode() {
        return requirementCode;
    }

    /**
     * Define el valor de la propiedad requirementCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequirementCode(String value) {
        this.requirementCode = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeTimeConfirmationRequiredIndicator.
     * 
     */
    public boolean isEmployeeTimeConfirmationRequiredIndicator() {
        return employeeTimeConfirmationRequiredIndicator;
    }

    /**
     * Define el valor de la propiedad employeeTimeConfirmationRequiredIndicator.
     * 
     */
    public void setEmployeeTimeConfirmationRequiredIndicator(boolean value) {
        this.employeeTimeConfirmationRequiredIndicator = value;
    }

}
