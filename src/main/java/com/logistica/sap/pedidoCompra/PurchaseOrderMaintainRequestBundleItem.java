
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestBundleItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestBundleItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodeSenderTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ItemID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;element name="CancelItemPurchaseOrderActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderItemFinishInvoice" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="PurchaseOrdeItemFinishDelivery" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentItemTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemTypeCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="QuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="NetAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="NetUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="GrossAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="GrossUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="ListUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="LimitAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="DeliveryPeriod" type="{http://sap.com/xi/AP/Common/GDT}UPPEROPEN_LOCALNORMALISED_DateTimePeriod" minOccurs="0"/>
 *         &lt;element name="DirectMaterialIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ThirdPartyDealIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FollowUpPurchaseOrderConfirmation" type="{http://sap.com/xi/AP/XU/SRM/Global}ProcurementDocumentItemFollowUpPurchaseOrderConfirmation" minOccurs="0"/>
 *         &lt;element name="FollowUpDelivery" type="{http://sap.com/xi/AP/XU/SRM/Global}ProcurementDocumentItemFollowUpDelivery" minOccurs="0"/>
 *         &lt;element name="FollowUpInvoice" type="{http://sap.com/xi/AP/XU/SRM/Global}ProcurementDocumentItemFollowUpInvoice" minOccurs="0"/>
 *         &lt;element name="ItemDeliveryTerms" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="ItemProduct" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemProduct" minOccurs="0"/>
 *         &lt;element name="ItemIndividualMaterial" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemIndividualMaterial" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipToLocation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemLocation" minOccurs="0"/>
 *         &lt;element name="ReceivingItemSite" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemLocation" minOccurs="0"/>
 *         &lt;element name="EndBuyerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ProductRecipientParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ServicePerformerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="RequestorParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ItemAccountingCodingBlockDistribution" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}MaintenanceAccountingCodingBlockDistribution" minOccurs="0"/>
 *         &lt;element name="ItemAttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="ItemTextCollection" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceTextCollection" minOccurs="0"/>
 *         &lt;element name="ItemPurchasingContractReference" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference" minOccurs="0"/>
 *         &lt;element name="ItemPriceCalculation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestPriceCalculationItem" minOccurs="0"/>
 *         &lt;element name="ItemTaxCalculation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestTaxCalculationItem" minOccurs="0"/>
 *         &lt;element name="ItemPlannedLandedCost" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemLandedCosts" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemScheduleLine" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemScheduleLine" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9834E0DB67A4127"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9844A949C2A862B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB984640C63F9069D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB49755672E75675E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497630CFD8A27BF"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB49763EE74A807C0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB49769BE929147C2"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB49771B9E9AF47C5"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497737094D527C5"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB4977FDEF9F16819"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497885D5CD2281D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB4979DC3D530C875"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497A94FEBCF08C7"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497ADEC784488C9"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497C26CA16A6927"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497DD99A5D1C97E"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163EC2A6F51EDBB497F209A837E9E9"/>
 *         &lt;group ref="{http://sap.com/xi/AP/Globalization}PurchaseOrderMaintainRequestBundleItemGloExtension"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ItemImatListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" />
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestBundleItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodeSenderTechnicalID",
    "itemID",
    "cancelItemPurchaseOrderActionIndicator",
    "purchaseOrderItemFinishInvoice",
    "purchaseOrdeItemFinishDelivery",
    "businessTransactionDocumentItemTypeCode",
    "quantity",
    "quantityTypeCode",
    "description",
    "netAmount",
    "netUnitPrice",
    "grossAmount",
    "grossUnitPrice",
    "listUnitPrice",
    "limitAmount",
    "deliveryPeriod",
    "directMaterialIndicator",
    "thirdPartyDealIndicator",
    "followUpPurchaseOrderConfirmation",
    "followUpDelivery",
    "followUpInvoice",
    "itemDeliveryTerms",
    "itemProduct",
    "itemIndividualMaterial",
    "shipToLocation",
    "receivingItemSite",
    "endBuyerParty",
    "productRecipientParty",
    "servicePerformerParty",
    "requestorParty",
    "itemAccountingCodingBlockDistribution",
    "itemAttachmentFolder",
    "itemTextCollection",
    "itemPurchasingContractReference",
    "itemPriceCalculation",
    "itemTaxCalculation",
    "itemPlannedLandedCost",
    "itemScheduleLine",
    "unidaddeventaS",
    "ndeservicio2",
    "fechaCruceS",
    "ubicacinOrigenS",
    "ubicacinDestinoS",
    "ubicacinCruceS",
    "guaHouseS",
    "nmerodeequipodecargaS",
    "tipodeequipoS",
    "fechaDestinoS",
    "pzsPBPCS",
    "shipperS",
    "fechadeAgenteAduanalS",
    "fechaCargaS",
    "guaMasterS",
    "consignatariosS",
    "requierePODS",
    "hsnCodeIndiaCode"
})
public class PurchaseOrderMaintainRequestBundleItem {

    @XmlElement(name = "ObjectNodeSenderTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String objectNodeSenderTechnicalID;
    @XmlElement(name = "ItemID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String itemID;
    @XmlElement(name = "CancelItemPurchaseOrderActionIndicator")
    protected Boolean cancelItemPurchaseOrderActionIndicator;
    @XmlElement(name = "PurchaseOrderItemFinishInvoice")
    protected Boolean purchaseOrderItemFinishInvoice;
    @XmlElement(name = "PurchaseOrdeItemFinishDelivery")
    protected Boolean purchaseOrdeItemFinishDelivery;
    @XmlElement(name = "BusinessTransactionDocumentItemTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessTransactionDocumentItemTypeCode;
    @XmlElement(name = "Quantity")
    protected Quantity quantity;
    @XmlElement(name = "QuantityTypeCode")
    protected QuantityTypeCode quantityTypeCode;
    @XmlElement(name = "Description")
    protected SHORTDescription description;
    @XmlElement(name = "NetAmount")
    protected Amount netAmount;
    @XmlElement(name = "NetUnitPrice")
    protected Price netUnitPrice;
    @XmlElement(name = "GrossAmount")
    protected Amount grossAmount;
    @XmlElement(name = "GrossUnitPrice")
    protected Price grossUnitPrice;
    @XmlElement(name = "ListUnitPrice")
    protected Price listUnitPrice;
    @XmlElement(name = "LimitAmount")
    protected Amount limitAmount;
    @XmlElement(name = "DeliveryPeriod")
    protected UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod;
    @XmlElement(name = "DirectMaterialIndicator")
    protected Boolean directMaterialIndicator;
    @XmlElement(name = "ThirdPartyDealIndicator")
    protected Boolean thirdPartyDealIndicator;
    @XmlElement(name = "FollowUpPurchaseOrderConfirmation")
    protected ProcurementDocumentItemFollowUpPurchaseOrderConfirmation followUpPurchaseOrderConfirmation;
    @XmlElement(name = "FollowUpDelivery")
    protected ProcurementDocumentItemFollowUpDelivery followUpDelivery;
    @XmlElement(name = "FollowUpInvoice")
    protected ProcurementDocumentItemFollowUpInvoice followUpInvoice;
    @XmlElement(name = "ItemDeliveryTerms")
    protected PurchaseOrderMaintainRequestBundleItemDeliveryTerms itemDeliveryTerms;
    @XmlElement(name = "ItemProduct")
    protected PurchaseOrderMaintainRequestBundleItemProduct itemProduct;
    @XmlElement(name = "ItemIndividualMaterial")
    protected List<PurchaseOrderMaintainRequestBundleItemIndividualMaterial> itemIndividualMaterial;
    @XmlElement(name = "ShipToLocation")
    protected PurchaseOrderMaintainRequestBundleItemLocation shipToLocation;
    @XmlElement(name = "ReceivingItemSite")
    protected PurchaseOrderMaintainRequestBundleItemLocation receivingItemSite;
    @XmlElement(name = "EndBuyerParty")
    protected PurchaseOrderMaintainRequestBundleItemParty endBuyerParty;
    @XmlElement(name = "ProductRecipientParty")
    protected PurchaseOrderMaintainRequestBundleItemParty productRecipientParty;
    @XmlElement(name = "ServicePerformerParty")
    protected PurchaseOrderMaintainRequestBundleItemParty servicePerformerParty;
    @XmlElement(name = "RequestorParty")
    protected PurchaseOrderMaintainRequestBundleItemParty requestorParty;
    @XmlElement(name = "ItemAccountingCodingBlockDistribution")
    protected MaintenanceAccountingCodingBlockDistribution itemAccountingCodingBlockDistribution;
    @XmlElement(name = "ItemAttachmentFolder")
    protected MaintenanceAttachmentFolder itemAttachmentFolder;
    @XmlElement(name = "ItemTextCollection")
    protected MaintenanceTextCollection itemTextCollection;
    @XmlElement(name = "ItemPurchasingContractReference")
    protected PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference itemPurchasingContractReference;
    @XmlElement(name = "ItemPriceCalculation")
    protected PurchaseOrderMaintainRequestPriceCalculationItem itemPriceCalculation;
    @XmlElement(name = "ItemTaxCalculation")
    protected PurchaseOrderMaintainRequestTaxCalculationItem itemTaxCalculation;
    @XmlElement(name = "ItemPlannedLandedCost")
    protected List<PurchaseOrderMaintainRequestBundleItemLandedCosts> itemPlannedLandedCost;
    @XmlElement(name = "ItemScheduleLine")
    protected List<PurchaseOrderMaintainRequestBundleItemScheduleLine> itemScheduleLine;
    @XmlElement(name = "UnidaddeventaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String unidaddeventaS;
    @XmlElement(name = "Ndeservicio2", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ndeservicio2;
    @XmlElement(name = "FechaCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaCruceS;
    @XmlElement(name = "UbicacinOrigenS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinOrigenS;
    @XmlElement(name = "UbicacinDestinoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestinoS;
    @XmlElement(name = "UbicacinCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCruceS;
    @XmlElement(name = "GuaHouseS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHouseS;
    @XmlElement(name = "NmerodeequipodecargaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipodecargaS;
    @XmlElement(name = "TipodeequipoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeequipoS;
    @XmlElement(name = "FechaDestinoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaDestinoS;
    @XmlElement(name = "PzsPBPCS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pzsPBPCS;
    @XmlElement(name = "ShipperS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String shipperS;
    @XmlElement(name = "FechadeAgenteAduanalS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechadeAgenteAduanalS;
    @XmlElement(name = "FechaCargaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaCargaS;
    @XmlElement(name = "GuaMasterS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMasterS;
    @XmlElement(name = "ConsignatariosS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatariosS;
    @XmlElement(name = "RequierePODS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String requierePODS;
    @XmlElement(name = "HSNCodeIndiaCode", namespace = "http://sap.com/xi/AP/Globalization")
    protected ProductTaxStandardClassificationCode hsnCodeIndiaCode;
    @XmlAttribute(name = "ItemImatListCompleteTransmissionIndicator")
    protected Boolean itemImatListCompleteTransmissionIndicator;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Obtiene el valor de la propiedad objectNodeSenderTechnicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodeSenderTechnicalID() {
        return objectNodeSenderTechnicalID;
    }

    /**
     * Define el valor de la propiedad objectNodeSenderTechnicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodeSenderTechnicalID(String value) {
        this.objectNodeSenderTechnicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad itemID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Define el valor de la propiedad itemID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelItemPurchaseOrderActionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelItemPurchaseOrderActionIndicator() {
        return cancelItemPurchaseOrderActionIndicator;
    }

    /**
     * Define el valor de la propiedad cancelItemPurchaseOrderActionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelItemPurchaseOrderActionIndicator(Boolean value) {
        this.cancelItemPurchaseOrderActionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderItemFinishInvoice.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPurchaseOrderItemFinishInvoice() {
        return purchaseOrderItemFinishInvoice;
    }

    /**
     * Define el valor de la propiedad purchaseOrderItemFinishInvoice.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPurchaseOrderItemFinishInvoice(Boolean value) {
        this.purchaseOrderItemFinishInvoice = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrdeItemFinishDelivery.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPurchaseOrdeItemFinishDelivery() {
        return purchaseOrdeItemFinishDelivery;
    }

    /**
     * Define el valor de la propiedad purchaseOrdeItemFinishDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPurchaseOrdeItemFinishDelivery(Boolean value) {
        this.purchaseOrdeItemFinishDelivery = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentItemTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessTransactionDocumentItemTypeCode() {
        return businessTransactionDocumentItemTypeCode;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentItemTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessTransactionDocumentItemTypeCode(String value) {
        this.businessTransactionDocumentItemTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setQuantity(Quantity value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad quantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getQuantityTypeCode() {
        return quantityTypeCode;
    }

    /**
     * Define el valor de la propiedad quantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setQuantityTypeCode(QuantityTypeCode value) {
        this.quantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setDescription(SHORTDescription value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad netAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getNetAmount() {
        return netAmount;
    }

    /**
     * Define el valor de la propiedad netAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setNetAmount(Amount value) {
        this.netAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad netUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getNetUnitPrice() {
        return netUnitPrice;
    }

    /**
     * Define el valor de la propiedad netUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setNetUnitPrice(Price value) {
        this.netUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad grossAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getGrossAmount() {
        return grossAmount;
    }

    /**
     * Define el valor de la propiedad grossAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setGrossAmount(Amount value) {
        this.grossAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad grossUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getGrossUnitPrice() {
        return grossUnitPrice;
    }

    /**
     * Define el valor de la propiedad grossUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setGrossUnitPrice(Price value) {
        this.grossUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad listUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getListUnitPrice() {
        return listUnitPrice;
    }

    /**
     * Define el valor de la propiedad listUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setListUnitPrice(Price value) {
        this.listUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad limitAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getLimitAmount() {
        return limitAmount;
    }

    /**
     * Define el valor de la propiedad limitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setLimitAmount(Amount value) {
        this.limitAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryPeriod.
     * 
     * @return
     *     possible object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod getDeliveryPeriod() {
        return deliveryPeriod;
    }

    /**
     * Define el valor de la propiedad deliveryPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public void setDeliveryPeriod(UPPEROPENLOCALNORMALISEDDateTimePeriod value) {
        this.deliveryPeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad directMaterialIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectMaterialIndicator() {
        return directMaterialIndicator;
    }

    /**
     * Define el valor de la propiedad directMaterialIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectMaterialIndicator(Boolean value) {
        this.directMaterialIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad thirdPartyDealIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isThirdPartyDealIndicator() {
        return thirdPartyDealIndicator;
    }

    /**
     * Define el valor de la propiedad thirdPartyDealIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThirdPartyDealIndicator(Boolean value) {
        this.thirdPartyDealIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpPurchaseOrderConfirmation.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentItemFollowUpPurchaseOrderConfirmation }
     *     
     */
    public ProcurementDocumentItemFollowUpPurchaseOrderConfirmation getFollowUpPurchaseOrderConfirmation() {
        return followUpPurchaseOrderConfirmation;
    }

    /**
     * Define el valor de la propiedad followUpPurchaseOrderConfirmation.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentItemFollowUpPurchaseOrderConfirmation }
     *     
     */
    public void setFollowUpPurchaseOrderConfirmation(ProcurementDocumentItemFollowUpPurchaseOrderConfirmation value) {
        this.followUpPurchaseOrderConfirmation = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpDelivery.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentItemFollowUpDelivery }
     *     
     */
    public ProcurementDocumentItemFollowUpDelivery getFollowUpDelivery() {
        return followUpDelivery;
    }

    /**
     * Define el valor de la propiedad followUpDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentItemFollowUpDelivery }
     *     
     */
    public void setFollowUpDelivery(ProcurementDocumentItemFollowUpDelivery value) {
        this.followUpDelivery = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpInvoice.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentItemFollowUpInvoice }
     *     
     */
    public ProcurementDocumentItemFollowUpInvoice getFollowUpInvoice() {
        return followUpInvoice;
    }

    /**
     * Define el valor de la propiedad followUpInvoice.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentItemFollowUpInvoice }
     *     
     */
    public void setFollowUpInvoice(ProcurementDocumentItemFollowUpInvoice value) {
        this.followUpInvoice = value;
    }

    /**
     * Obtiene el valor de la propiedad itemDeliveryTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemDeliveryTerms }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemDeliveryTerms getItemDeliveryTerms() {
        return itemDeliveryTerms;
    }

    /**
     * Define el valor de la propiedad itemDeliveryTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemDeliveryTerms }
     *     
     */
    public void setItemDeliveryTerms(PurchaseOrderMaintainRequestBundleItemDeliveryTerms value) {
        this.itemDeliveryTerms = value;
    }

    /**
     * Obtiene el valor de la propiedad itemProduct.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemProduct }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemProduct getItemProduct() {
        return itemProduct;
    }

    /**
     * Define el valor de la propiedad itemProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemProduct }
     *     
     */
    public void setItemProduct(PurchaseOrderMaintainRequestBundleItemProduct value) {
        this.itemProduct = value;
    }

    /**
     * Gets the value of the itemIndividualMaterial property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIndividualMaterial property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIndividualMaterial().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItemIndividualMaterial }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItemIndividualMaterial> getItemIndividualMaterial() {
        if (itemIndividualMaterial == null) {
            itemIndividualMaterial = new ArrayList<PurchaseOrderMaintainRequestBundleItemIndividualMaterial>();
        }
        return this.itemIndividualMaterial;
    }

    /**
     * Obtiene el valor de la propiedad shipToLocation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemLocation getShipToLocation() {
        return shipToLocation;
    }

    /**
     * Define el valor de la propiedad shipToLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public void setShipToLocation(PurchaseOrderMaintainRequestBundleItemLocation value) {
        this.shipToLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad receivingItemSite.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemLocation getReceivingItemSite() {
        return receivingItemSite;
    }

    /**
     * Define el valor de la propiedad receivingItemSite.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public void setReceivingItemSite(PurchaseOrderMaintainRequestBundleItemLocation value) {
        this.receivingItemSite = value;
    }

    /**
     * Obtiene el valor de la propiedad endBuyerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getEndBuyerParty() {
        return endBuyerParty;
    }

    /**
     * Define el valor de la propiedad endBuyerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setEndBuyerParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.endBuyerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad productRecipientParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getProductRecipientParty() {
        return productRecipientParty;
    }

    /**
     * Define el valor de la propiedad productRecipientParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setProductRecipientParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.productRecipientParty = value;
    }

    /**
     * Obtiene el valor de la propiedad servicePerformerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getServicePerformerParty() {
        return servicePerformerParty;
    }

    /**
     * Define el valor de la propiedad servicePerformerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setServicePerformerParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.servicePerformerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad requestorParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getRequestorParty() {
        return requestorParty;
    }

    /**
     * Define el valor de la propiedad requestorParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setRequestorParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.requestorParty = value;
    }

    /**
     * Obtiene el valor de la propiedad itemAccountingCodingBlockDistribution.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public MaintenanceAccountingCodingBlockDistribution getItemAccountingCodingBlockDistribution() {
        return itemAccountingCodingBlockDistribution;
    }

    /**
     * Define el valor de la propiedad itemAccountingCodingBlockDistribution.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public void setItemAccountingCodingBlockDistribution(MaintenanceAccountingCodingBlockDistribution value) {
        this.itemAccountingCodingBlockDistribution = value;
    }

    /**
     * Obtiene el valor de la propiedad itemAttachmentFolder.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getItemAttachmentFolder() {
        return itemAttachmentFolder;
    }

    /**
     * Define el valor de la propiedad itemAttachmentFolder.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setItemAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.itemAttachmentFolder = value;
    }

    /**
     * Obtiene el valor de la propiedad itemTextCollection.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public MaintenanceTextCollection getItemTextCollection() {
        return itemTextCollection;
    }

    /**
     * Define el valor de la propiedad itemTextCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public void setItemTextCollection(MaintenanceTextCollection value) {
        this.itemTextCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad itemPurchasingContractReference.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference getItemPurchasingContractReference() {
        return itemPurchasingContractReference;
    }

    /**
     * Define el valor de la propiedad itemPurchasingContractReference.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     *     
     */
    public void setItemPurchasingContractReference(PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference value) {
        this.itemPurchasingContractReference = value;
    }

    /**
     * Obtiene el valor de la propiedad itemPriceCalculation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestPriceCalculationItem }
     *     
     */
    public PurchaseOrderMaintainRequestPriceCalculationItem getItemPriceCalculation() {
        return itemPriceCalculation;
    }

    /**
     * Define el valor de la propiedad itemPriceCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestPriceCalculationItem }
     *     
     */
    public void setItemPriceCalculation(PurchaseOrderMaintainRequestPriceCalculationItem value) {
        this.itemPriceCalculation = value;
    }

    /**
     * Obtiene el valor de la propiedad itemTaxCalculation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestTaxCalculationItem }
     *     
     */
    public PurchaseOrderMaintainRequestTaxCalculationItem getItemTaxCalculation() {
        return itemTaxCalculation;
    }

    /**
     * Define el valor de la propiedad itemTaxCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestTaxCalculationItem }
     *     
     */
    public void setItemTaxCalculation(PurchaseOrderMaintainRequestTaxCalculationItem value) {
        this.itemTaxCalculation = value;
    }

    /**
     * Gets the value of the itemPlannedLandedCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemPlannedLandedCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemPlannedLandedCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItemLandedCosts }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItemLandedCosts> getItemPlannedLandedCost() {
        if (itemPlannedLandedCost == null) {
            itemPlannedLandedCost = new ArrayList<PurchaseOrderMaintainRequestBundleItemLandedCosts>();
        }
        return this.itemPlannedLandedCost;
    }

    /**
     * Gets the value of the itemScheduleLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemScheduleLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemScheduleLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItemScheduleLine }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItemScheduleLine> getItemScheduleLine() {
        if (itemScheduleLine == null) {
            itemScheduleLine = new ArrayList<PurchaseOrderMaintainRequestBundleItemScheduleLine>();
        }
        return this.itemScheduleLine;
    }

    /**
     * Obtiene el valor de la propiedad unidaddeventaS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidaddeventaS() {
        return unidaddeventaS;
    }

    /**
     * Define el valor de la propiedad unidaddeventaS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidaddeventaS(String value) {
        this.unidaddeventaS = value;
    }

    /**
     * Obtiene el valor de la propiedad ndeservicio2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNdeservicio2() {
        return ndeservicio2;
    }

    /**
     * Define el valor de la propiedad ndeservicio2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNdeservicio2(String value) {
        this.ndeservicio2 = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCruceS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCruceS() {
        return fechaCruceS;
    }

    /**
     * Define el valor de la propiedad fechaCruceS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCruceS(XMLGregorianCalendar value) {
        this.fechaCruceS = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacinOrigenS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinOrigenS() {
        return ubicacinOrigenS;
    }

    /**
     * Define el valor de la propiedad ubicacinOrigenS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinOrigenS(String value) {
        this.ubicacinOrigenS = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacinDestinoS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestinoS() {
        return ubicacinDestinoS;
    }

    /**
     * Define el valor de la propiedad ubicacinDestinoS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestinoS(String value) {
        this.ubicacinDestinoS = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacinCruceS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCruceS() {
        return ubicacinCruceS;
    }

    /**
     * Define el valor de la propiedad ubicacinCruceS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCruceS(String value) {
        this.ubicacinCruceS = value;
    }

    /**
     * Obtiene el valor de la propiedad guaHouseS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHouseS() {
        return guaHouseS;
    }

    /**
     * Define el valor de la propiedad guaHouseS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHouseS(String value) {
        this.guaHouseS = value;
    }

    /**
     * Obtiene el valor de la propiedad nmerodeequipodecargaS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipodecargaS() {
        return nmerodeequipodecargaS;
    }

    /**
     * Define el valor de la propiedad nmerodeequipodecargaS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipodecargaS(String value) {
        this.nmerodeequipodecargaS = value;
    }

    /**
     * Obtiene el valor de la propiedad tipodeequipoS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeequipoS() {
        return tipodeequipoS;
    }

    /**
     * Define el valor de la propiedad tipodeequipoS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeequipoS(String value) {
        this.tipodeequipoS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDestinoS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestinoS() {
        return fechaDestinoS;
    }

    /**
     * Define el valor de la propiedad fechaDestinoS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestinoS(XMLGregorianCalendar value) {
        this.fechaDestinoS = value;
    }

    /**
     * Obtiene el valor de la propiedad pzsPBPCS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPzsPBPCS() {
        return pzsPBPCS;
    }

    /**
     * Define el valor de la propiedad pzsPBPCS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPzsPBPCS(String value) {
        this.pzsPBPCS = value;
    }

    /**
     * Obtiene el valor de la propiedad shipperS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperS() {
        return shipperS;
    }

    /**
     * Define el valor de la propiedad shipperS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperS(String value) {
        this.shipperS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechadeAgenteAduanalS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechadeAgenteAduanalS() {
        return fechadeAgenteAduanalS;
    }

    /**
     * Define el valor de la propiedad fechadeAgenteAduanalS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechadeAgenteAduanalS(XMLGregorianCalendar value) {
        this.fechadeAgenteAduanalS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCargaS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCargaS() {
        return fechaCargaS;
    }

    /**
     * Define el valor de la propiedad fechaCargaS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCargaS(XMLGregorianCalendar value) {
        this.fechaCargaS = value;
    }

    /**
     * Obtiene el valor de la propiedad guaMasterS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMasterS() {
        return guaMasterS;
    }

    /**
     * Define el valor de la propiedad guaMasterS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMasterS(String value) {
        this.guaMasterS = value;
    }

    /**
     * Obtiene el valor de la propiedad consignatariosS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatariosS() {
        return consignatariosS;
    }

    /**
     * Define el valor de la propiedad consignatariosS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatariosS(String value) {
        this.consignatariosS = value;
    }

    /**
     * Obtiene el valor de la propiedad requierePODS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequierePODS() {
        return requierePODS;
    }

    /**
     * Define el valor de la propiedad requierePODS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequierePODS(String value) {
        this.requierePODS = value;
    }

    /**
     * Obtiene el valor de la propiedad hsnCodeIndiaCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public ProductTaxStandardClassificationCode getHSNCodeIndiaCode() {
        return hsnCodeIndiaCode;
    }

    /**
     * Define el valor de la propiedad hsnCodeIndiaCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public void setHSNCodeIndiaCode(ProductTaxStandardClassificationCode value) {
        this.hsnCodeIndiaCode = value;
    }

    /**
     * Obtiene el valor de la propiedad itemImatListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemImatListCompleteTransmissionIndicator() {
        return itemImatListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemImatListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemImatListCompleteTransmissionIndicator(Boolean value) {
        this.itemImatListCompleteTransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
