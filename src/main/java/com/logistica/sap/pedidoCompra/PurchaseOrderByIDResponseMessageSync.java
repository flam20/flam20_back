
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseMessage_sync complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseMessage_sync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseOrder" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponse" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Log" type="{http://sap.com/xi/AP/Common/GDT}Log"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseMessage_sync", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "purchaseOrder",
    "log"
})
public class PurchaseOrderByIDResponseMessageSync {

    @XmlElement(name = "PurchaseOrder")
    protected List<PurchaseOrderByIDResponse> purchaseOrder;
    @XmlElement(name = "Log", required = true)
    protected Log log;

    /**
     * Gets the value of the purchaseOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the purchaseOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPurchaseOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderByIDResponse }
     * 
     * 
     */
    public List<PurchaseOrderByIDResponse> getPurchaseOrder() {
        if (purchaseOrder == null) {
            purchaseOrder = new ArrayList<PurchaseOrderByIDResponse>();
        }
        return this.purchaseOrder;
    }

    /**
     * Obtiene el valor de la propiedad log.
     * 
     * @return
     *     possible object is
     *     {@link Log }
     *     
     */
    public Log getLog() {
        return log;
    }

    /**
     * Define el valor de la propiedad log.
     * 
     * @param value
     *     allowed object is
     *     {@link Log }
     *     
     */
    public void setLog(Log value) {
        this.log = value;
    }

}
