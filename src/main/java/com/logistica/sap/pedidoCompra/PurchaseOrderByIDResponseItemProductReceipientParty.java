
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItemProductReceipientParty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItemProductReceipientParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodePartyTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="PARTYKEY" type="{http://sap.com/xi/AP/Common/Global}PartyKey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItemProductReceipientParty", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodePartyTechnicalID",
    "partykey"
})
public class PurchaseOrderByIDResponseItemProductReceipientParty {

    @XmlElement(name = "ObjectNodePartyTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String objectNodePartyTechnicalID;
    @XmlElement(name = "PARTYKEY")
    protected PartyKey partykey;

    /**
     * Obtiene el valor de la propiedad objectNodePartyTechnicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodePartyTechnicalID() {
        return objectNodePartyTechnicalID;
    }

    /**
     * Define el valor de la propiedad objectNodePartyTechnicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodePartyTechnicalID(String value) {
        this.objectNodePartyTechnicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad partykey.
     * 
     * @return
     *     possible object is
     *     {@link PartyKey }
     *     
     */
    public PartyKey getPARTYKEY() {
        return partykey;
    }

    /**
     * Define el valor de la propiedad partykey.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyKey }
     *     
     */
    public void setPARTYKEY(PartyKey value) {
        this.partykey = value;
    }

}
