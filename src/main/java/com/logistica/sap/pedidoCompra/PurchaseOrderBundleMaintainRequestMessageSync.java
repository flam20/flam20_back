
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderBundleMaintainRequestMessage_sync complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderBundleMaintainRequestMessage_sync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BasicMessageHeader" type="{http://sap.com/xi/AP/Common/GDT}BusinessDocumentBasicMessageHeader"/>
 *         &lt;element name="PurchaseOrderMaintainBundle" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundle" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderBundleMaintainRequestMessage_sync", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "basicMessageHeader",
    "purchaseOrderMaintainBundle"
})
public class PurchaseOrderBundleMaintainRequestMessageSync {

    @XmlElement(name = "BasicMessageHeader", required = true)
    protected BusinessDocumentBasicMessageHeader basicMessageHeader;
    @XmlElement(name = "PurchaseOrderMaintainBundle", required = true)
    protected List<PurchaseOrderMaintainRequestBundle> purchaseOrderMaintainBundle;

    /**
     * Obtiene el valor de la propiedad basicMessageHeader.
     * 
     * @return
     *     possible object is
     *     {@link BusinessDocumentBasicMessageHeader }
     *     
     */
    public BusinessDocumentBasicMessageHeader getBasicMessageHeader() {
        return basicMessageHeader;
    }

    /**
     * Define el valor de la propiedad basicMessageHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessDocumentBasicMessageHeader }
     *     
     */
    public void setBasicMessageHeader(BusinessDocumentBasicMessageHeader value) {
        this.basicMessageHeader = value;
    }

    /**
     * Gets the value of the purchaseOrderMaintainBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the purchaseOrderMaintainBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPurchaseOrderMaintainBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundle }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundle> getPurchaseOrderMaintainBundle() {
        if (purchaseOrderMaintainBundle == null) {
            purchaseOrderMaintainBundle = new ArrayList<PurchaseOrderMaintainRequestBundle>();
        }
        return this.purchaseOrderMaintainBundle;
    }

}
