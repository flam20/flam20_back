
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderReadTaxCalculationItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderReadTaxCalculationItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CountryCode" type="{http://sap.com/xi/AP/Common/GDT}CountryCode" minOccurs="0"/>
 *         &lt;element name="TaxationCharacteristicsCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTaxationCharacteristicsCode" minOccurs="0"/>
 *         &lt;element name="TaxJurisdictionCode" type="{http://sap.com/xi/AP/Common/GDT}TaxJurisdictionCode" minOccurs="0"/>
 *         &lt;element name="WithholdingTaxationCharacteristicsCode" type="{http://sap.com/xi/AP/Common/GDT}WithholdingTaxationCharacteristicsCode" minOccurs="0"/>
 *         &lt;element name="ItemProductTaxDetails" type="{http://sap.com/xi/A1S/Global}PurchaseOrderReadItemProductTaxDetails" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemTaxationTerms" type="{http://sap.com/xi/A1S/Global}PurchaseOrderReadItemTaxationTerms" minOccurs="0"/>
 *         &lt;element name="ItemWithholdingTaxDetails" type="{http://sap.com/xi/A1S/Global}PurchaseOrderReadItemWithholdingTaxDetails" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderReadTaxCalculationItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "countryCode",
    "taxationCharacteristicsCode",
    "taxJurisdictionCode",
    "withholdingTaxationCharacteristicsCode",
    "itemProductTaxDetails",
    "itemTaxationTerms",
    "itemWithholdingTaxDetails"
})
public class PurchaseOrderReadTaxCalculationItem {

    @XmlElement(name = "CountryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String countryCode;
    @XmlElement(name = "TaxationCharacteristicsCode")
    protected ProductTaxationCharacteristicsCode taxationCharacteristicsCode;
    @XmlElement(name = "TaxJurisdictionCode")
    protected TaxJurisdictionCode taxJurisdictionCode;
    @XmlElement(name = "WithholdingTaxationCharacteristicsCode")
    protected WithholdingTaxationCharacteristicsCode withholdingTaxationCharacteristicsCode;
    @XmlElement(name = "ItemProductTaxDetails")
    protected List<PurchaseOrderReadItemProductTaxDetails> itemProductTaxDetails;
    @XmlElement(name = "ItemTaxationTerms")
    protected PurchaseOrderReadItemTaxationTerms itemTaxationTerms;
    @XmlElement(name = "ItemWithholdingTaxDetails")
    protected List<PurchaseOrderReadItemWithholdingTaxDetails> itemWithholdingTaxDetails;

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxationCharacteristicsCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxationCharacteristicsCode }
     *     
     */
    public ProductTaxationCharacteristicsCode getTaxationCharacteristicsCode() {
        return taxationCharacteristicsCode;
    }

    /**
     * Define el valor de la propiedad taxationCharacteristicsCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxationCharacteristicsCode }
     *     
     */
    public void setTaxationCharacteristicsCode(ProductTaxationCharacteristicsCode value) {
        this.taxationCharacteristicsCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxJurisdictionCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public TaxJurisdictionCode getTaxJurisdictionCode() {
        return taxJurisdictionCode;
    }

    /**
     * Define el valor de la propiedad taxJurisdictionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public void setTaxJurisdictionCode(TaxJurisdictionCode value) {
        this.taxJurisdictionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad withholdingTaxationCharacteristicsCode.
     * 
     * @return
     *     possible object is
     *     {@link WithholdingTaxationCharacteristicsCode }
     *     
     */
    public WithholdingTaxationCharacteristicsCode getWithholdingTaxationCharacteristicsCode() {
        return withholdingTaxationCharacteristicsCode;
    }

    /**
     * Define el valor de la propiedad withholdingTaxationCharacteristicsCode.
     * 
     * @param value
     *     allowed object is
     *     {@link WithholdingTaxationCharacteristicsCode }
     *     
     */
    public void setWithholdingTaxationCharacteristicsCode(WithholdingTaxationCharacteristicsCode value) {
        this.withholdingTaxationCharacteristicsCode = value;
    }

    /**
     * Gets the value of the itemProductTaxDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemProductTaxDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemProductTaxDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderReadItemProductTaxDetails }
     * 
     * 
     */
    public List<PurchaseOrderReadItemProductTaxDetails> getItemProductTaxDetails() {
        if (itemProductTaxDetails == null) {
            itemProductTaxDetails = new ArrayList<PurchaseOrderReadItemProductTaxDetails>();
        }
        return this.itemProductTaxDetails;
    }

    /**
     * Obtiene el valor de la propiedad itemTaxationTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderReadItemTaxationTerms }
     *     
     */
    public PurchaseOrderReadItemTaxationTerms getItemTaxationTerms() {
        return itemTaxationTerms;
    }

    /**
     * Define el valor de la propiedad itemTaxationTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderReadItemTaxationTerms }
     *     
     */
    public void setItemTaxationTerms(PurchaseOrderReadItemTaxationTerms value) {
        this.itemTaxationTerms = value;
    }

    /**
     * Gets the value of the itemWithholdingTaxDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemWithholdingTaxDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemWithholdingTaxDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderReadItemWithholdingTaxDetails }
     * 
     * 
     */
    public List<PurchaseOrderReadItemWithholdingTaxDetails> getItemWithholdingTaxDetails() {
        if (itemWithholdingTaxDetails == null) {
            itemWithholdingTaxDetails = new ArrayList<PurchaseOrderReadItemWithholdingTaxDetails>();
        }
        return this.itemWithholdingTaxDetails;
    }

}
