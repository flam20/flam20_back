
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseStatus complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApprovalStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ApprovalStatusCode" minOccurs="0"/>
 *         &lt;element name="DeliveryProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoiceProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoicingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}InvoicingStatusCode" minOccurs="0"/>
 *         &lt;element name="OrderDeliveryStatusCode" type="{http://sap.com/xi/AP/Common/GDT}DeliveryStatusCode" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderConfirmationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}PurchaseOrderConfirmationStatusCode" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderDeliveryStatusCode" type="{http://sap.com/xi/AP/Common/GDT}PurchaseOrderDeliveryStatusCode" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderLifeCycleStatusCode" type="{http://sap.com/xi/AP/Common/GDT}PurchaseOrderLifeCycleStatusCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseStatus", namespace = "http://sap.com/xi/AP/CRM/Global", propOrder = {
    "approvalStatusCode",
    "deliveryProcessingStatusCode",
    "invoiceProcessingStatusCode",
    "invoicingStatusCode",
    "orderDeliveryStatusCode",
    "purchaseOrderConfirmationStatusCode",
    "purchaseOrderDeliveryStatusCode",
    "purchaseOrderLifeCycleStatusCode"
})
public class PurchaseOrderByIDResponseStatus {

    @XmlElement(name = "ApprovalStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String approvalStatusCode;
    @XmlElement(name = "DeliveryProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryProcessingStatusCode;
    @XmlElement(name = "InvoiceProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String invoiceProcessingStatusCode;
    @XmlElement(name = "InvoicingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String invoicingStatusCode;
    @XmlElement(name = "OrderDeliveryStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String orderDeliveryStatusCode;
    @XmlElement(name = "PurchaseOrderConfirmationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderConfirmationStatusCode;
    @XmlElement(name = "PurchaseOrderDeliveryStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderDeliveryStatusCode;
    @XmlElement(name = "PurchaseOrderLifeCycleStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderLifeCycleStatusCode;

    /**
     * Obtiene el valor de la propiedad approvalStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalStatusCode() {
        return approvalStatusCode;
    }

    /**
     * Define el valor de la propiedad approvalStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalStatusCode(String value) {
        this.approvalStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryProcessingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryProcessingStatusCode() {
        return deliveryProcessingStatusCode;
    }

    /**
     * Define el valor de la propiedad deliveryProcessingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryProcessingStatusCode(String value) {
        this.deliveryProcessingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceProcessingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceProcessingStatusCode() {
        return invoiceProcessingStatusCode;
    }

    /**
     * Define el valor de la propiedad invoiceProcessingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceProcessingStatusCode(String value) {
        this.invoiceProcessingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invoicingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingStatusCode() {
        return invoicingStatusCode;
    }

    /**
     * Define el valor de la propiedad invoicingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingStatusCode(String value) {
        this.invoicingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad orderDeliveryStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderDeliveryStatusCode() {
        return orderDeliveryStatusCode;
    }

    /**
     * Define el valor de la propiedad orderDeliveryStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderDeliveryStatusCode(String value) {
        this.orderDeliveryStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderConfirmationStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderConfirmationStatusCode() {
        return purchaseOrderConfirmationStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderConfirmationStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderConfirmationStatusCode(String value) {
        this.purchaseOrderConfirmationStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderDeliveryStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderDeliveryStatusCode() {
        return purchaseOrderDeliveryStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderDeliveryStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderDeliveryStatusCode(String value) {
        this.purchaseOrderDeliveryStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderLifeCycleStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderLifeCycleStatusCode() {
        return purchaseOrderLifeCycleStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderLifeCycleStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderLifeCycleStatusCode(String value) {
        this.purchaseOrderLifeCycleStatusCode = value;
    }

}
