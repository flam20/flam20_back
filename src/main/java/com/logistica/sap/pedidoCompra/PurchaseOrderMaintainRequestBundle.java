
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestBundle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestBundle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodeSenderTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ChangeStateID" type="{http://sap.com/xi/AP/Common/GDT}ChangeStateID" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentTypeCode"/>
 *         &lt;element name="Name" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://sap.com/xi/AP/Common/GDT}Date" minOccurs="0"/>
 *         &lt;element name="OrderPurchaseOrderActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="CancelPurchaseOrderActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FinishDeliveryPOActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FinishInvoicePOActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="PreventDocumentOutputIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://sap.com/xi/AP/Common/GDT}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="BuyerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleParty" minOccurs="0"/>
 *         &lt;element name="SellerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleParty" minOccurs="0"/>
 *         &lt;element name="EmployeeResponsibleParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleParty" minOccurs="0"/>
 *         &lt;element name="BillToParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleParty" minOccurs="0"/>
 *         &lt;element name="ResponsiblePurchasingUnitParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleParty" minOccurs="0"/>
 *         &lt;element name="AttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="TextCollection" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceTextCollection" minOccurs="0"/>
 *         &lt;element name="DeliveryTerms" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="CashDiscountTerms" type="{http://sap.com/xi/AP/FO/CashDiscountTerms/Global}PurchaseOrderMaintenanceCashDiscountTerms" minOccurs="0"/>
 *         &lt;element name="Item" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *       &lt;attribute name="ItemListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestBundle", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodeSenderTechnicalID",
    "changeStateID",
    "businessTransactionDocumentTypeCode",
    "name",
    "purchaseOrderID",
    "date",
    "orderPurchaseOrderActionIndicator",
    "cancelPurchaseOrderActionIndicator",
    "finishDeliveryPOActionIndicator",
    "finishInvoicePOActionIndicator",
    "preventDocumentOutputIndicator",
    "currencyCode",
    "buyerParty",
    "sellerParty",
    "employeeResponsibleParty",
    "billToParty",
    "responsiblePurchasingUnitParty",
    "attachmentFolder",
    "textCollection",
    "deliveryTerms",
    "cashDiscountTerms",
    "item"
})
public class PurchaseOrderMaintainRequestBundle {

    @XmlElement(name = "ObjectNodeSenderTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String objectNodeSenderTechnicalID;
    @XmlElement(name = "ChangeStateID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeStateID;
    @XmlElement(name = "BusinessTransactionDocumentTypeCode", required = true)
    protected BusinessTransactionDocumentTypeCode businessTransactionDocumentTypeCode;
    @XmlElement(name = "Name")
    protected MEDIUMName name;
    @XmlElement(name = "PurchaseOrderID")
    protected BusinessTransactionDocumentID purchaseOrderID;
    @XmlElement(name = "Date")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;
    @XmlElement(name = "OrderPurchaseOrderActionIndicator")
    protected Boolean orderPurchaseOrderActionIndicator;
    @XmlElement(name = "CancelPurchaseOrderActionIndicator")
    protected Boolean cancelPurchaseOrderActionIndicator;
    @XmlElement(name = "FinishDeliveryPOActionIndicator")
    protected Boolean finishDeliveryPOActionIndicator;
    @XmlElement(name = "FinishInvoicePOActionIndicator")
    protected Boolean finishInvoicePOActionIndicator;
    @XmlElement(name = "PreventDocumentOutputIndicator")
    protected Boolean preventDocumentOutputIndicator;
    @XmlElement(name = "CurrencyCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String currencyCode;
    @XmlElement(name = "BuyerParty")
    protected PurchaseOrderMaintainRequestBundleParty buyerParty;
    @XmlElement(name = "SellerParty")
    protected PurchaseOrderMaintainRequestBundleParty sellerParty;
    @XmlElement(name = "EmployeeResponsibleParty")
    protected PurchaseOrderMaintainRequestBundleParty employeeResponsibleParty;
    @XmlElement(name = "BillToParty")
    protected PurchaseOrderMaintainRequestBundleParty billToParty;
    @XmlElement(name = "ResponsiblePurchasingUnitParty")
    protected PurchaseOrderMaintainRequestBundleParty responsiblePurchasingUnitParty;
    @XmlElement(name = "AttachmentFolder")
    protected MaintenanceAttachmentFolder attachmentFolder;
    @XmlElement(name = "TextCollection")
    protected MaintenanceTextCollection textCollection;
    @XmlElement(name = "DeliveryTerms")
    protected PurchaseOrderMaintainRequestBundleDeliveryTerms deliveryTerms;
    @XmlElement(name = "CashDiscountTerms")
    protected PurchaseOrderMaintenanceCashDiscountTerms cashDiscountTerms;
    @XmlElement(name = "Item")
    protected List<PurchaseOrderMaintainRequestBundleItem> item;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;
    @XmlAttribute(name = "ItemListCompleteTransmissionIndicator")
    protected Boolean itemListCompleteTransmissionIndicator;

    /**
     * Obtiene el valor de la propiedad objectNodeSenderTechnicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodeSenderTechnicalID() {
        return objectNodeSenderTechnicalID;
    }

    /**
     * Define el valor de la propiedad objectNodeSenderTechnicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodeSenderTechnicalID(String value) {
        this.objectNodeSenderTechnicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad changeStateID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeStateID() {
        return changeStateID;
    }

    /**
     * Define el valor de la propiedad changeStateID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeStateID(String value) {
        this.changeStateID = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentTypeCode }
     *     
     */
    public BusinessTransactionDocumentTypeCode getBusinessTransactionDocumentTypeCode() {
        return businessTransactionDocumentTypeCode;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentTypeCode }
     *     
     */
    public void setBusinessTransactionDocumentTypeCode(BusinessTransactionDocumentTypeCode value) {
        this.businessTransactionDocumentTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMName }
     *     
     */
    public MEDIUMName getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMName }
     *     
     */
    public void setName(MEDIUMName value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getPurchaseOrderID() {
        return purchaseOrderID;
    }

    /**
     * Define el valor de la propiedad purchaseOrderID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setPurchaseOrderID(BusinessTransactionDocumentID value) {
        this.purchaseOrderID = value;
    }

    /**
     * Obtiene el valor de la propiedad date.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Define el valor de la propiedad date.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

    /**
     * Obtiene el valor de la propiedad orderPurchaseOrderActionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOrderPurchaseOrderActionIndicator() {
        return orderPurchaseOrderActionIndicator;
    }

    /**
     * Define el valor de la propiedad orderPurchaseOrderActionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOrderPurchaseOrderActionIndicator(Boolean value) {
        this.orderPurchaseOrderActionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelPurchaseOrderActionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelPurchaseOrderActionIndicator() {
        return cancelPurchaseOrderActionIndicator;
    }

    /**
     * Define el valor de la propiedad cancelPurchaseOrderActionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelPurchaseOrderActionIndicator(Boolean value) {
        this.cancelPurchaseOrderActionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad finishDeliveryPOActionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFinishDeliveryPOActionIndicator() {
        return finishDeliveryPOActionIndicator;
    }

    /**
     * Define el valor de la propiedad finishDeliveryPOActionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFinishDeliveryPOActionIndicator(Boolean value) {
        this.finishDeliveryPOActionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad finishInvoicePOActionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFinishInvoicePOActionIndicator() {
        return finishInvoicePOActionIndicator;
    }

    /**
     * Define el valor de la propiedad finishInvoicePOActionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFinishInvoicePOActionIndicator(Boolean value) {
        this.finishInvoicePOActionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad preventDocumentOutputIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPreventDocumentOutputIndicator() {
        return preventDocumentOutputIndicator;
    }

    /**
     * Define el valor de la propiedad preventDocumentOutputIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPreventDocumentOutputIndicator(Boolean value) {
        this.preventDocumentOutputIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad buyerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleParty getBuyerParty() {
        return buyerParty;
    }

    /**
     * Define el valor de la propiedad buyerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public void setBuyerParty(PurchaseOrderMaintainRequestBundleParty value) {
        this.buyerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad sellerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleParty getSellerParty() {
        return sellerParty;
    }

    /**
     * Define el valor de la propiedad sellerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public void setSellerParty(PurchaseOrderMaintainRequestBundleParty value) {
        this.sellerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeResponsibleParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleParty getEmployeeResponsibleParty() {
        return employeeResponsibleParty;
    }

    /**
     * Define el valor de la propiedad employeeResponsibleParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public void setEmployeeResponsibleParty(PurchaseOrderMaintainRequestBundleParty value) {
        this.employeeResponsibleParty = value;
    }

    /**
     * Obtiene el valor de la propiedad billToParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleParty getBillToParty() {
        return billToParty;
    }

    /**
     * Define el valor de la propiedad billToParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public void setBillToParty(PurchaseOrderMaintainRequestBundleParty value) {
        this.billToParty = value;
    }

    /**
     * Obtiene el valor de la propiedad responsiblePurchasingUnitParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleParty getResponsiblePurchasingUnitParty() {
        return responsiblePurchasingUnitParty;
    }

    /**
     * Define el valor de la propiedad responsiblePurchasingUnitParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleParty }
     *     
     */
    public void setResponsiblePurchasingUnitParty(PurchaseOrderMaintainRequestBundleParty value) {
        this.responsiblePurchasingUnitParty = value;
    }

    /**
     * Obtiene el valor de la propiedad attachmentFolder.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getAttachmentFolder() {
        return attachmentFolder;
    }

    /**
     * Define el valor de la propiedad attachmentFolder.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.attachmentFolder = value;
    }

    /**
     * Obtiene el valor de la propiedad textCollection.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public MaintenanceTextCollection getTextCollection() {
        return textCollection;
    }

    /**
     * Define el valor de la propiedad textCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public void setTextCollection(MaintenanceTextCollection value) {
        this.textCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleDeliveryTerms }
     *     
     */
    public PurchaseOrderMaintainRequestBundleDeliveryTerms getDeliveryTerms() {
        return deliveryTerms;
    }

    /**
     * Define el valor de la propiedad deliveryTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleDeliveryTerms }
     *     
     */
    public void setDeliveryTerms(PurchaseOrderMaintainRequestBundleDeliveryTerms value) {
        this.deliveryTerms = value;
    }

    /**
     * Obtiene el valor de la propiedad cashDiscountTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintenanceCashDiscountTerms }
     *     
     */
    public PurchaseOrderMaintenanceCashDiscountTerms getCashDiscountTerms() {
        return cashDiscountTerms;
    }

    /**
     * Define el valor de la propiedad cashDiscountTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintenanceCashDiscountTerms }
     *     
     */
    public void setCashDiscountTerms(PurchaseOrderMaintenanceCashDiscountTerms value) {
        this.cashDiscountTerms = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItem }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItem> getItem() {
        if (item == null) {
            item = new ArrayList<PurchaseOrderMaintainRequestBundleItem>();
        }
        return this.item;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad itemListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemListCompleteTransmissionIndicator() {
        return itemListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemListCompleteTransmissionIndicator(Boolean value) {
        this.itemListCompleteTransmissionIndicator = value;
    }

}
