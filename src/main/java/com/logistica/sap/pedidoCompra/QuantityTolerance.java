
package com.logistica.sap.pedidoCompra;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QuantityTolerance complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuantityTolerance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OverPercent" type="{http://sap.com/xi/AP/Common/GDT}Percent" minOccurs="0"/>
 *         &lt;element name="OverPercentUnlimitedIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="UnderPercent" type="{http://sap.com/xi/AP/Common/GDT}Percent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuantityTolerance", propOrder = {
    "overPercent",
    "overPercentUnlimitedIndicator",
    "underPercent"
})
public class QuantityTolerance {

    @XmlElement(name = "OverPercent")
    protected BigDecimal overPercent;
    @XmlElement(name = "OverPercentUnlimitedIndicator")
    protected Boolean overPercentUnlimitedIndicator;
    @XmlElement(name = "UnderPercent")
    protected BigDecimal underPercent;

    /**
     * Obtiene el valor de la propiedad overPercent.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOverPercent() {
        return overPercent;
    }

    /**
     * Define el valor de la propiedad overPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOverPercent(BigDecimal value) {
        this.overPercent = value;
    }

    /**
     * Obtiene el valor de la propiedad overPercentUnlimitedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverPercentUnlimitedIndicator() {
        return overPercentUnlimitedIndicator;
    }

    /**
     * Define el valor de la propiedad overPercentUnlimitedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverPercentUnlimitedIndicator(Boolean value) {
        this.overPercentUnlimitedIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad underPercent.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnderPercent() {
        return underPercent;
    }

    /**
     * Define el valor de la propiedad underPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnderPercent(BigDecimal value) {
        this.underPercent = value;
    }

}
