
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderReadPriceCalculationItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderReadPriceCalculationItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemPriceComponent" type="{http://sap.com/xi/A1S/Global}PurchaseOrderReadItemPriceComponent" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="itemPriceComponentListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="itemProductTaxDetailsListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="itemWithholdingTaxDetailsListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://sap.com/xi/AP/Common/GDT}CountryCode" minOccurs="0"/>
 *         &lt;element name="TaxationCharacteristicsCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTaxationCharacteristicsCode" minOccurs="0"/>
 *         &lt;element name="TaxJurisdictionCode" type="{http://sap.com/xi/AP/Common/GDT}TaxJurisdictionCode" minOccurs="0"/>
 *         &lt;element name="WithholdingTaxationCharacteristicsCode" type="{http://sap.com/xi/AP/Common/GDT}WithholdingTaxationCharacteristicsCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderReadPriceCalculationItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "itemPriceComponent",
    "itemPriceComponentListCompleteTransmissionIndicator",
    "itemProductTaxDetailsListCompleteTransmissionIndicator",
    "itemWithholdingTaxDetailsListCompleteTransmissionIndicator",
    "countryCode",
    "taxationCharacteristicsCode",
    "taxJurisdictionCode",
    "withholdingTaxationCharacteristicsCode"
})
public class PurchaseOrderReadPriceCalculationItem {

    @XmlElement(name = "ItemPriceComponent")
    protected List<PurchaseOrderReadItemPriceComponent> itemPriceComponent;
    protected Boolean itemPriceComponentListCompleteTransmissionIndicator;
    protected Boolean itemProductTaxDetailsListCompleteTransmissionIndicator;
    protected Boolean itemWithholdingTaxDetailsListCompleteTransmissionIndicator;
    @XmlElement(name = "CountryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String countryCode;
    @XmlElement(name = "TaxationCharacteristicsCode")
    protected ProductTaxationCharacteristicsCode taxationCharacteristicsCode;
    @XmlElement(name = "TaxJurisdictionCode")
    protected TaxJurisdictionCode taxJurisdictionCode;
    @XmlElement(name = "WithholdingTaxationCharacteristicsCode")
    protected WithholdingTaxationCharacteristicsCode withholdingTaxationCharacteristicsCode;

    /**
     * Gets the value of the itemPriceComponent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemPriceComponent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemPriceComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderReadItemPriceComponent }
     * 
     * 
     */
    public List<PurchaseOrderReadItemPriceComponent> getItemPriceComponent() {
        if (itemPriceComponent == null) {
            itemPriceComponent = new ArrayList<PurchaseOrderReadItemPriceComponent>();
        }
        return this.itemPriceComponent;
    }

    /**
     * Obtiene el valor de la propiedad itemPriceComponentListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemPriceComponentListCompleteTransmissionIndicator() {
        return itemPriceComponentListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemPriceComponentListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemPriceComponentListCompleteTransmissionIndicator(Boolean value) {
        this.itemPriceComponentListCompleteTransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad itemProductTaxDetailsListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemProductTaxDetailsListCompleteTransmissionIndicator() {
        return itemProductTaxDetailsListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemProductTaxDetailsListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemProductTaxDetailsListCompleteTransmissionIndicator(Boolean value) {
        this.itemProductTaxDetailsListCompleteTransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad itemWithholdingTaxDetailsListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemWithholdingTaxDetailsListCompleteTransmissionIndicator() {
        return itemWithholdingTaxDetailsListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemWithholdingTaxDetailsListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemWithholdingTaxDetailsListCompleteTransmissionIndicator(Boolean value) {
        this.itemWithholdingTaxDetailsListCompleteTransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxationCharacteristicsCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxationCharacteristicsCode }
     *     
     */
    public ProductTaxationCharacteristicsCode getTaxationCharacteristicsCode() {
        return taxationCharacteristicsCode;
    }

    /**
     * Define el valor de la propiedad taxationCharacteristicsCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxationCharacteristicsCode }
     *     
     */
    public void setTaxationCharacteristicsCode(ProductTaxationCharacteristicsCode value) {
        this.taxationCharacteristicsCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxJurisdictionCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public TaxJurisdictionCode getTaxJurisdictionCode() {
        return taxJurisdictionCode;
    }

    /**
     * Define el valor de la propiedad taxJurisdictionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public void setTaxJurisdictionCode(TaxJurisdictionCode value) {
        this.taxJurisdictionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad withholdingTaxationCharacteristicsCode.
     * 
     * @return
     *     possible object is
     *     {@link WithholdingTaxationCharacteristicsCode }
     *     
     */
    public WithholdingTaxationCharacteristicsCode getWithholdingTaxationCharacteristicsCode() {
        return withholdingTaxationCharacteristicsCode;
    }

    /**
     * Define el valor de la propiedad withholdingTaxationCharacteristicsCode.
     * 
     * @param value
     *     allowed object is
     *     {@link WithholdingTaxationCharacteristicsCode }
     *     
     */
    public void setWithholdingTaxationCharacteristicsCode(WithholdingTaxationCharacteristicsCode value) {
        this.withholdingTaxationCharacteristicsCode = value;
    }

}
