
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestBundleItemDeliveryTerms complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestBundleItemDeliveryTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodePartyTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="MaximumLeadTimeDuration" type="{http://sap.com/xi/AP/Common/GDT}DAY_Duration" minOccurs="0"/>
 *         &lt;element name="DeliveryPriorityCode" type="{http://sap.com/xi/AP/Common/GDT}PriorityCode" minOccurs="0"/>
 *         &lt;element name="QuantityTolerance" type="{http://sap.com/xi/AP/Common/GDT}QuantityTolerance" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestBundleItemDeliveryTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodePartyTechnicalID",
    "maximumLeadTimeDuration",
    "deliveryPriorityCode",
    "quantityTolerance"
})
public class PurchaseOrderMaintainRequestBundleItemDeliveryTerms {

    @XmlElement(name = "ObjectNodePartyTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String objectNodePartyTechnicalID;
    @XmlElement(name = "MaximumLeadTimeDuration")
    protected Duration maximumLeadTimeDuration;
    @XmlElement(name = "DeliveryPriorityCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryPriorityCode;
    @XmlElement(name = "QuantityTolerance")
    protected QuantityTolerance quantityTolerance;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Obtiene el valor de la propiedad objectNodePartyTechnicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodePartyTechnicalID() {
        return objectNodePartyTechnicalID;
    }

    /**
     * Define el valor de la propiedad objectNodePartyTechnicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodePartyTechnicalID(String value) {
        this.objectNodePartyTechnicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad maximumLeadTimeDuration.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMaximumLeadTimeDuration() {
        return maximumLeadTimeDuration;
    }

    /**
     * Define el valor de la propiedad maximumLeadTimeDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMaximumLeadTimeDuration(Duration value) {
        this.maximumLeadTimeDuration = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryPriorityCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryPriorityCode() {
        return deliveryPriorityCode;
    }

    /**
     * Define el valor de la propiedad deliveryPriorityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryPriorityCode(String value) {
        this.deliveryPriorityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad quantityTolerance.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTolerance }
     *     
     */
    public QuantityTolerance getQuantityTolerance() {
        return quantityTolerance;
    }

    /**
     * Define el valor de la propiedad quantityTolerance.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTolerance }
     *     
     */
    public void setQuantityTolerance(QuantityTolerance value) {
        this.quantityTolerance = value;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
