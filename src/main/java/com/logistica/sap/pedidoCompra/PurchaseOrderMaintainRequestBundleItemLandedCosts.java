
package com.logistica.sap.pedidoCompra;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestBundleItemLandedCosts complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestBundleItemLandedCosts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="PriceComponent" type="{http://sap.com/xi/AP/FO/PlannedLandedCost}PlannedLandedCostProfilePriceComponentCode" minOccurs="0"/>
 *         &lt;element name="CalculationMethod" type="{http://sap.com/xi/AP/FO/PlannedLandedCost}PlannedLandedCostProfileCalculationMethodCode" minOccurs="0"/>
 *         &lt;element name="ChargesBasedOn" type="{http://sap.com/xi/AP/FO/PlannedLandedCost}PlannedLandedCostProfileChargesBasedOn" minOccurs="0"/>
 *         &lt;element name="ServiceProviderParty" type="{http://sap.com/xi/AP/Common/Global}PartyKey" minOccurs="0"/>
 *         &lt;element name="SOSTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentTypeCode" minOccurs="0"/>
 *         &lt;element name="SOSID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="SOSItemID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="ServiceProductID" type="{http://sap.com/xi/AP/Common/GDT}ProductID" minOccurs="0"/>
 *         &lt;element name="ServiceProductQuantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="BaseProductQuantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="Percentage" type="{http://sap.com/xi/AP/Common/GDT}Percent" minOccurs="0"/>
 *         &lt;element name="NetPrice" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="NetValue" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestBundleItemLandedCosts", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "uuid",
    "priceComponent",
    "calculationMethod",
    "chargesBasedOn",
    "serviceProviderParty",
    "sosTypeCode",
    "sosid",
    "sosItemID",
    "serviceProductID",
    "serviceProductQuantity",
    "baseProductQuantity",
    "percentage",
    "netPrice",
    "netValue"
})
public class PurchaseOrderMaintainRequestBundleItemLandedCosts {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "PriceComponent")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String priceComponent;
    @XmlElement(name = "CalculationMethod")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String calculationMethod;
    @XmlElement(name = "ChargesBasedOn")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String chargesBasedOn;
    @XmlElement(name = "ServiceProviderParty")
    protected PartyKey serviceProviderParty;
    @XmlElement(name = "SOSTypeCode")
    protected BusinessTransactionDocumentTypeCode sosTypeCode;
    @XmlElement(name = "SOSID")
    protected BusinessTransactionDocumentID sosid;
    @XmlElement(name = "SOSItemID")
    protected BusinessTransactionDocumentID sosItemID;
    @XmlElement(name = "ServiceProductID")
    protected ProductID serviceProductID;
    @XmlElement(name = "ServiceProductQuantity")
    protected Quantity serviceProductQuantity;
    @XmlElement(name = "BaseProductQuantity")
    protected Quantity baseProductQuantity;
    @XmlElement(name = "Percentage")
    protected BigDecimal percentage;
    @XmlElement(name = "NetPrice")
    protected Amount netPrice;
    @XmlElement(name = "NetValue")
    protected Amount netValue;

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad priceComponent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceComponent() {
        return priceComponent;
    }

    /**
     * Define el valor de la propiedad priceComponent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceComponent(String value) {
        this.priceComponent = value;
    }

    /**
     * Obtiene el valor de la propiedad calculationMethod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculationMethod() {
        return calculationMethod;
    }

    /**
     * Define el valor de la propiedad calculationMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculationMethod(String value) {
        this.calculationMethod = value;
    }

    /**
     * Obtiene el valor de la propiedad chargesBasedOn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargesBasedOn() {
        return chargesBasedOn;
    }

    /**
     * Define el valor de la propiedad chargesBasedOn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargesBasedOn(String value) {
        this.chargesBasedOn = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceProviderParty.
     * 
     * @return
     *     possible object is
     *     {@link PartyKey }
     *     
     */
    public PartyKey getServiceProviderParty() {
        return serviceProviderParty;
    }

    /**
     * Define el valor de la propiedad serviceProviderParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyKey }
     *     
     */
    public void setServiceProviderParty(PartyKey value) {
        this.serviceProviderParty = value;
    }

    /**
     * Obtiene el valor de la propiedad sosTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentTypeCode }
     *     
     */
    public BusinessTransactionDocumentTypeCode getSOSTypeCode() {
        return sosTypeCode;
    }

    /**
     * Define el valor de la propiedad sosTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentTypeCode }
     *     
     */
    public void setSOSTypeCode(BusinessTransactionDocumentTypeCode value) {
        this.sosTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad sosid.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getSOSID() {
        return sosid;
    }

    /**
     * Define el valor de la propiedad sosid.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setSOSID(BusinessTransactionDocumentID value) {
        this.sosid = value;
    }

    /**
     * Obtiene el valor de la propiedad sosItemID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getSOSItemID() {
        return sosItemID;
    }

    /**
     * Define el valor de la propiedad sosItemID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setSOSItemID(BusinessTransactionDocumentID value) {
        this.sosItemID = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceProductID.
     * 
     * @return
     *     possible object is
     *     {@link ProductID }
     *     
     */
    public ProductID getServiceProductID() {
        return serviceProductID;
    }

    /**
     * Define el valor de la propiedad serviceProductID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductID }
     *     
     */
    public void setServiceProductID(ProductID value) {
        this.serviceProductID = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceProductQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getServiceProductQuantity() {
        return serviceProductQuantity;
    }

    /**
     * Define el valor de la propiedad serviceProductQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setServiceProductQuantity(Quantity value) {
        this.serviceProductQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad baseProductQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getBaseProductQuantity() {
        return baseProductQuantity;
    }

    /**
     * Define el valor de la propiedad baseProductQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setBaseProductQuantity(Quantity value) {
        this.baseProductQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad percentage.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercentage() {
        return percentage;
    }

    /**
     * Define el valor de la propiedad percentage.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercentage(BigDecimal value) {
        this.percentage = value;
    }

    /**
     * Obtiene el valor de la propiedad netPrice.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getNetPrice() {
        return netPrice;
    }

    /**
     * Define el valor de la propiedad netPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setNetPrice(Amount value) {
        this.netPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad netValue.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getNetValue() {
        return netValue;
    }

    /**
     * Define el valor de la propiedad netValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setNetValue(Amount value) {
        this.netValue = value;
    }

}
