
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;&lt;ccts:RepresentationTerm xmlns:ccts="urn:un:unece:uncefact:documentation:standard:CoreComponentsTechnicalSpecification:3.0" xmlns="http://sap.com/xi/AP/Common/GDT" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:n1="http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU" xmlns:n10="http://sap.com/xi/AP/FO/PlannedLandedCost" xmlns:n11="http://sap.com/xi/AP/FO/FundManagement/Global" xmlns:n12="http://sap.com/xi/A1S" xmlns:n13="http://sap.com/xi/AP/FO/GrantManagement/Global" xmlns:n14="http://sap.com/xi/AP/PDI/ABSL" xmlns:n15="http://sap.com/xi/AP/XU/SRM/Global" xmlns:n16="http://sap.com/xi/AP/CRM/Global" xmlns:n17="http://sap.com/xi/Common/DataTypes" xmlns:n2="http://sap.com/xi/AP/Globalization" xmlns:n3="http://sap.com/xi/SAPGlobal20/Global" xmlns:n4="http://sap.com/xi/AP/Common/Global" xmlns:n5="http://sap.com/xi/AP/FO/CashDiscountTerms/Global" xmlns:n6="http://sap.com/xi/AP/Common/GDT" xmlns:n7="http://sap.com/xi/AP/IS/CodingBlock/Global" xmlns:n8="http://sap.com/xi/BASIS/Global" xmlns:n9="http://sap.com/xi/DocumentServices/Global" xmlns:rfc="urn:sap-com:sap:rfc:functions" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://sap.com/xi/A1S/Global" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:xi29="http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU" xmlns:xi32="http://sap.com/xi/A1S/Global" xmlns:xi33="http://sap.com/xi/AP/Globalization" xmlns:xi34="http://sap.com/xi/SAPGlobal20/Global" xmlns:xi35="http://sap.com/xi/AP/Common/Global" xmlns:xi36="http://sap.com/xi/AP/FO/CashDiscountTerms/Global" xmlns:xi37="http://sap.com/xi/AP/Common/GDT" xmlns:xi38="http://sap.com/xi/AP/IS/CodingBlock/Global" xmlns:xi39="http://sap.com/xi/BASIS/Global" xmlns:xi40="http://sap.com/xi/DocumentServices/Global" xmlns:xi43="http://sap.com/xi/AP/FO/PlannedLandedCost" xmlns:xi45="http://sap.com/xi/AP/FO/FundManagement/Global" xmlns:xi46="http://sap.com/xi/A1S" xmlns:xi47="http://sap.com/xi/AP/FO/GrantManagement/Global" xmlns:xi48="http://sap.com/xi/AP/PDI/ABSL" xmlns:xi50="http://sap.com/xi/AP/XU/SRM/Global" xmlns:xi54="http://sap.com/xi/AP/CRM/Global" xmlns:xi55="http://sap.com/xi/Common/DataTypes" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;Text&lt;/ccts:RepresentationTerm&gt;
 * </pre>
 * 
 * 
 * <p>Clase Java para Text complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Text">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="languageCode" type="{http://sap.com/xi/BASIS/Global}LanguageCode" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Text", propOrder = {
    "value"
})
public class Text {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "languageCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String languageCode;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad languageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * Define el valor de la propiedad languageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageCode(String value) {
        this.languageCode = value;
    }

}
