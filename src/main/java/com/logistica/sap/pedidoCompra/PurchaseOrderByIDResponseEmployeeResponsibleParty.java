
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseEmployeeResponsibleParty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseEmployeeResponsibleParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NodeID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="PartyUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="PartyTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessObjectTypeCode" minOccurs="0"/>
 *         &lt;element name="AddressReference" type="{http://sap.com/xi/AP/Common/GDT}PartyAddressReference" minOccurs="0"/>
 *         &lt;element name="PartyKey" type="{http://sap.com/xi/AP/Common/Global}PartyKey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseEmployeeResponsibleParty", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "nodeID",
    "partyUUID",
    "partyTypeCode",
    "addressReference",
    "partyKey"
})
public class PurchaseOrderByIDResponseEmployeeResponsibleParty {

    @XmlElement(name = "NodeID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String nodeID;
    @XmlElement(name = "PartyUUID")
    protected UUID partyUUID;
    @XmlElement(name = "PartyTypeCode")
    protected BusinessObjectTypeCode partyTypeCode;
    @XmlElement(name = "AddressReference")
    protected PartyAddressReference addressReference;
    @XmlElement(name = "PartyKey")
    protected PartyKey partyKey;

    /**
     * Obtiene el valor de la propiedad nodeID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeID() {
        return nodeID;
    }

    /**
     * Define el valor de la propiedad nodeID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeID(String value) {
        this.nodeID = value;
    }

    /**
     * Obtiene el valor de la propiedad partyUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getPartyUUID() {
        return partyUUID;
    }

    /**
     * Define el valor de la propiedad partyUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setPartyUUID(UUID value) {
        this.partyUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad partyTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link BusinessObjectTypeCode }
     *     
     */
    public BusinessObjectTypeCode getPartyTypeCode() {
        return partyTypeCode;
    }

    /**
     * Define el valor de la propiedad partyTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessObjectTypeCode }
     *     
     */
    public void setPartyTypeCode(BusinessObjectTypeCode value) {
        this.partyTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad addressReference.
     * 
     * @return
     *     possible object is
     *     {@link PartyAddressReference }
     *     
     */
    public PartyAddressReference getAddressReference() {
        return addressReference;
    }

    /**
     * Define el valor de la propiedad addressReference.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyAddressReference }
     *     
     */
    public void setAddressReference(PartyAddressReference value) {
        this.addressReference = value;
    }

    /**
     * Obtiene el valor de la propiedad partyKey.
     * 
     * @return
     *     possible object is
     *     {@link PartyKey }
     *     
     */
    public PartyKey getPartyKey() {
        return partyKey;
    }

    /**
     * Define el valor de la propiedad partyKey.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyKey }
     *     
     */
    public void setPartyKey(PartyKey value) {
        this.partyKey = value;
    }

}
