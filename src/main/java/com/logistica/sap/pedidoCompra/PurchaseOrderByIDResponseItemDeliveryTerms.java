
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItemDeliveryTerms complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItemDeliveryTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuantityTolerance" type="{http://sap.com/xi/AP/Common/GDT}QuantityTolerance" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItemDeliveryTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "quantityTolerance"
})
public class PurchaseOrderByIDResponseItemDeliveryTerms {

    @XmlElement(name = "QuantityTolerance")
    protected QuantityTolerance quantityTolerance;

    /**
     * Obtiene el valor de la propiedad quantityTolerance.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTolerance }
     *     
     */
    public QuantityTolerance getQuantityTolerance() {
        return quantityTolerance;
    }

    /**
     * Define el valor de la propiedad quantityTolerance.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTolerance }
     *     
     */
    public void setQuantityTolerance(QuantityTolerance value) {
        this.quantityTolerance = value;
    }

}
