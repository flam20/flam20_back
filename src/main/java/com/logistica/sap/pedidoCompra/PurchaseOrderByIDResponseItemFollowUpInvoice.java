
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItemFollowUpInvoice complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItemFollowUpInvoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessTransactionDocumentSettlementRelevanceIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator"/>
 *         &lt;element name="RequirementCode" type="{http://sap.com/xi/AP/Common/GDT}FollowUpBusinessTransactionDocumentRequirementCode"/>
 *         &lt;element name="EvaluatedReceiptSettlementIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator"/>
 *         &lt;element name="DeliveryBasedInvoiceVerificationIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItemFollowUpInvoice", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "businessTransactionDocumentSettlementRelevanceIndicator",
    "requirementCode",
    "evaluatedReceiptSettlementIndicator",
    "deliveryBasedInvoiceVerificationIndicator"
})
public class PurchaseOrderByIDResponseItemFollowUpInvoice {

    @XmlElement(name = "BusinessTransactionDocumentSettlementRelevanceIndicator")
    protected boolean businessTransactionDocumentSettlementRelevanceIndicator;
    @XmlElement(name = "RequirementCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String requirementCode;
    @XmlElement(name = "EvaluatedReceiptSettlementIndicator")
    protected boolean evaluatedReceiptSettlementIndicator;
    @XmlElement(name = "DeliveryBasedInvoiceVerificationIndicator")
    protected boolean deliveryBasedInvoiceVerificationIndicator;

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentSettlementRelevanceIndicator.
     * 
     */
    public boolean isBusinessTransactionDocumentSettlementRelevanceIndicator() {
        return businessTransactionDocumentSettlementRelevanceIndicator;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentSettlementRelevanceIndicator.
     * 
     */
    public void setBusinessTransactionDocumentSettlementRelevanceIndicator(boolean value) {
        this.businessTransactionDocumentSettlementRelevanceIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad requirementCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequirementCode() {
        return requirementCode;
    }

    /**
     * Define el valor de la propiedad requirementCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequirementCode(String value) {
        this.requirementCode = value;
    }

    /**
     * Obtiene el valor de la propiedad evaluatedReceiptSettlementIndicator.
     * 
     */
    public boolean isEvaluatedReceiptSettlementIndicator() {
        return evaluatedReceiptSettlementIndicator;
    }

    /**
     * Define el valor de la propiedad evaluatedReceiptSettlementIndicator.
     * 
     */
    public void setEvaluatedReceiptSettlementIndicator(boolean value) {
        this.evaluatedReceiptSettlementIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryBasedInvoiceVerificationIndicator.
     * 
     */
    public boolean isDeliveryBasedInvoiceVerificationIndicator() {
        return deliveryBasedInvoiceVerificationIndicator;
    }

    /**
     * Define el valor de la propiedad deliveryBasedInvoiceVerificationIndicator.
     * 
     */
    public void setDeliveryBasedInvoiceVerificationIndicator(boolean value) {
        this.deliveryBasedInvoiceVerificationIndicator = value;
    }

}
