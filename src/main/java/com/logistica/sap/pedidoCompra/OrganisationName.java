
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OrganisationName complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OrganisationName">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormOfAddressCode" type="{http://sap.com/xi/AP/Common/GDT}FormOfAddressCode" minOccurs="0"/>
 *         &lt;element name="FirstLineName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="SecondLineName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="ThirdLineName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="FourthLineName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_MEDIUM_Name" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganisationName", propOrder = {
    "formOfAddressCode",
    "firstLineName",
    "secondLineName",
    "thirdLineName",
    "fourthLineName"
})
public class OrganisationName {

    @XmlElement(name = "FormOfAddressCode")
    protected FormOfAddressCode formOfAddressCode;
    @XmlElement(name = "FirstLineName")
    protected String firstLineName;
    @XmlElement(name = "SecondLineName")
    protected String secondLineName;
    @XmlElement(name = "ThirdLineName")
    protected String thirdLineName;
    @XmlElement(name = "FourthLineName")
    protected String fourthLineName;

    /**
     * Obtiene el valor de la propiedad formOfAddressCode.
     * 
     * @return
     *     possible object is
     *     {@link FormOfAddressCode }
     *     
     */
    public FormOfAddressCode getFormOfAddressCode() {
        return formOfAddressCode;
    }

    /**
     * Define el valor de la propiedad formOfAddressCode.
     * 
     * @param value
     *     allowed object is
     *     {@link FormOfAddressCode }
     *     
     */
    public void setFormOfAddressCode(FormOfAddressCode value) {
        this.formOfAddressCode = value;
    }

    /**
     * Obtiene el valor de la propiedad firstLineName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstLineName() {
        return firstLineName;
    }

    /**
     * Define el valor de la propiedad firstLineName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstLineName(String value) {
        this.firstLineName = value;
    }

    /**
     * Obtiene el valor de la propiedad secondLineName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondLineName() {
        return secondLineName;
    }

    /**
     * Define el valor de la propiedad secondLineName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondLineName(String value) {
        this.secondLineName = value;
    }

    /**
     * Obtiene el valor de la propiedad thirdLineName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThirdLineName() {
        return thirdLineName;
    }

    /**
     * Define el valor de la propiedad thirdLineName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThirdLineName(String value) {
        this.thirdLineName = value;
    }

    /**
     * Obtiene el valor de la propiedad fourthLineName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFourthLineName() {
        return fourthLineName;
    }

    /**
     * Define el valor de la propiedad fourthLineName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFourthLineName(String value) {
        this.fourthLineName = value;
    }

}
