
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestLocationAddress complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestLocationAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CorrespondenceLanguageCode" type="{http://sap.com/xi/BASIS/Global}LanguageCode" minOccurs="0"/>
 *         &lt;element name="Email" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressEmail" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Facsimile" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressFascmile" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Telephone" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressTelephone" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Web" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressWeb" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DisplayName" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressDisplayName" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressName" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PostalAddress" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestLocationAddressPostalAddress" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestLocationAddress", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "correspondenceLanguageCode",
    "email",
    "facsimile",
    "telephone",
    "web",
    "displayName",
    "name",
    "postalAddress"
})
public class PurchaseOrderMaintainRequestLocationAddress {

    @XmlElement(name = "CorrespondenceLanguageCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String correspondenceLanguageCode;
    @XmlElement(name = "Email")
    protected List<PurchaseOrderMaintainRequestLocationAddressEmail> email;
    @XmlElement(name = "Facsimile")
    protected List<PurchaseOrderMaintainRequestLocationAddressFascmile> facsimile;
    @XmlElement(name = "Telephone")
    protected List<PurchaseOrderMaintainRequestLocationAddressTelephone> telephone;
    @XmlElement(name = "Web")
    protected List<PurchaseOrderMaintainRequestLocationAddressWeb> web;
    @XmlElement(name = "DisplayName")
    protected List<PurchaseOrderMaintainRequestLocationAddressDisplayName> displayName;
    @XmlElement(name = "Name")
    protected List<PurchaseOrderMaintainRequestLocationAddressName> name;
    @XmlElement(name = "PostalAddress")
    protected List<PurchaseOrderMaintainRequestLocationAddressPostalAddress> postalAddress;

    /**
     * Obtiene el valor de la propiedad correspondenceLanguageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorrespondenceLanguageCode() {
        return correspondenceLanguageCode;
    }

    /**
     * Define el valor de la propiedad correspondenceLanguageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorrespondenceLanguageCode(String value) {
        this.correspondenceLanguageCode = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the email property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressEmail }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressEmail> getEmail() {
        if (email == null) {
            email = new ArrayList<PurchaseOrderMaintainRequestLocationAddressEmail>();
        }
        return this.email;
    }

    /**
     * Gets the value of the facsimile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the facsimile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFacsimile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressFascmile }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressFascmile> getFacsimile() {
        if (facsimile == null) {
            facsimile = new ArrayList<PurchaseOrderMaintainRequestLocationAddressFascmile>();
        }
        return this.facsimile;
    }

    /**
     * Gets the value of the telephone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the telephone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTelephone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressTelephone }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressTelephone> getTelephone() {
        if (telephone == null) {
            telephone = new ArrayList<PurchaseOrderMaintainRequestLocationAddressTelephone>();
        }
        return this.telephone;
    }

    /**
     * Gets the value of the web property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the web property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeb().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressWeb }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressWeb> getWeb() {
        if (web == null) {
            web = new ArrayList<PurchaseOrderMaintainRequestLocationAddressWeb>();
        }
        return this.web;
    }

    /**
     * Gets the value of the displayName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the displayName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDisplayName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressDisplayName }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressDisplayName> getDisplayName() {
        if (displayName == null) {
            displayName = new ArrayList<PurchaseOrderMaintainRequestLocationAddressDisplayName>();
        }
        return this.displayName;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressName }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressName> getName() {
        if (name == null) {
            name = new ArrayList<PurchaseOrderMaintainRequestLocationAddressName>();
        }
        return this.name;
    }

    /**
     * Gets the value of the postalAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the postalAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPostalAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestLocationAddressPostalAddress }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestLocationAddressPostalAddress> getPostalAddress() {
        if (postalAddress == null) {
            postalAddress = new ArrayList<PurchaseOrderMaintainRequestLocationAddressPostalAddress>();
        }
        return this.postalAddress;
    }

}
