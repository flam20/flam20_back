
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para PurchaseOrderByIDResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://sap.com/xi/BASIS/Global}Date" minOccurs="0"/>
 *         &lt;element name="OrderedDateTime" type="{http://sap.com/xi/BASIS/Global}LOCALNORMALISED_DateTime" minOccurs="0"/>
 *         &lt;element name="DataOriginTypeCode" type="{http://sap.com/xi/AP/Common/GDT}ProcurementDocumentDataOriginTypeCode" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentTypeCode" minOccurs="0"/>
 *         &lt;element name="ProcessingTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentProcessingTypeCode" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://sap.com/xi/AP/Common/GDT}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="TemplateIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="TotalNetAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="CashDiscountTerms" type="{http://sap.com/xi/AP/FO/CashDiscountTerms/Global}AccessCashDiscountTerms" minOccurs="0"/>
 *         &lt;element name="ChangeStateID" type="{http://sap.com/xi/AP/Common/GDT}ChangeStateID"/>
 *         &lt;element name="DeliveryTerms" type="{http://sap.com/xi/AP/CRM/Global}PurchaseOrderByIDResponseDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://sap.com/xi/AP/CRM/Global}PurchaseOrderByIDResponseStatus" minOccurs="0"/>
 *         &lt;element name="BillToParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseBillToParty" minOccurs="0"/>
 *         &lt;element name="BuyerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseBuyerParty" minOccurs="0"/>
 *         &lt;element name="EmployeeResponsibleParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseEmployeeResponsibleParty" minOccurs="0"/>
 *         &lt;element name="ResponsiblePurchasingUnitParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseResponsiblePurchasingUnitParty" minOccurs="0"/>
 *         &lt;element name="Item" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItem" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TextCollection" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceTextCollection" minOccurs="0"/>
 *         &lt;element name="AttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="SellerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseSellerParty" minOccurs="0"/>
 *         &lt;element name="SystemAdministrativeData" type="{http://sap.com/xi/AP/Common/GDT}SystemAdministrativeData" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReferencePurchasingContractID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReferenceInternalRequestID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReferenceOutboundDeliveryID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentReferencePurchaseRequestID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponse", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "id",
    "uuid",
    "date",
    "orderedDateTime",
    "dataOriginTypeCode",
    "typeCode",
    "processingTypeCode",
    "name",
    "currencyCode",
    "templateIndicator",
    "totalNetAmount",
    "cashDiscountTerms",
    "changeStateID",
    "deliveryTerms",
    "status",
    "billToParty",
    "buyerParty",
    "employeeResponsibleParty",
    "responsiblePurchasingUnitParty",
    "item",
    "textCollection",
    "attachmentFolder",
    "sellerParty",
    "systemAdministrativeData",
    "businessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID",
    "businessTransactionDocumentReferencePurchasingContractID",
    "businessTransactionDocumentReferenceInternalRequestID",
    "businessTransactionDocumentReferenceOutboundDeliveryID",
    "businessTransactionDocumentReferencePurchaseRequestID"
})
public class PurchaseOrderByIDResponse {

    @XmlElement(name = "ID")
    protected BusinessTransactionDocumentID id;
    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "Date")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;
    @XmlElement(name = "OrderedDateTime")
    protected LOCALNORMALISEDDateTime orderedDateTime;
    @XmlElement(name = "DataOriginTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dataOriginTypeCode;
    @XmlElement(name = "TypeCode")
    protected BusinessTransactionDocumentTypeCode typeCode;
    @XmlElement(name = "ProcessingTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String processingTypeCode;
    @XmlElement(name = "Name")
    protected MEDIUMName name;
    @XmlElement(name = "CurrencyCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String currencyCode;
    @XmlElement(name = "TemplateIndicator")
    protected Boolean templateIndicator;
    @XmlElement(name = "TotalNetAmount")
    protected Amount totalNetAmount;
    @XmlElement(name = "CashDiscountTerms")
    protected AccessCashDiscountTerms cashDiscountTerms;
    @XmlElement(name = "ChangeStateID", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String changeStateID;
    @XmlElement(name = "DeliveryTerms")
    protected PurchaseOrderByIDResponseDeliveryTerms deliveryTerms;
    @XmlElement(name = "Status")
    protected PurchaseOrderByIDResponseStatus status;
    @XmlElement(name = "BillToParty")
    protected PurchaseOrderByIDResponseBillToParty billToParty;
    @XmlElement(name = "BuyerParty")
    protected PurchaseOrderByIDResponseBuyerParty buyerParty;
    @XmlElement(name = "EmployeeResponsibleParty")
    protected PurchaseOrderByIDResponseEmployeeResponsibleParty employeeResponsibleParty;
    @XmlElement(name = "ResponsiblePurchasingUnitParty")
    protected PurchaseOrderByIDResponseResponsiblePurchasingUnitParty responsiblePurchasingUnitParty;
    @XmlElement(name = "Item")
    protected List<PurchaseOrderByIDResponseItem> item;
    @XmlElement(name = "TextCollection")
    protected MaintenanceTextCollection textCollection;
    @XmlElement(name = "AttachmentFolder")
    protected MaintenanceAttachmentFolder attachmentFolder;
    @XmlElement(name = "SellerParty")
    protected PurchaseOrderByIDResponseSellerParty sellerParty;
    @XmlElement(name = "SystemAdministrativeData")
    protected SystemAdministrativeData systemAdministrativeData;
    @XmlElement(name = "BusinessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID")
    protected BusinessTransactionDocumentID businessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID;
    @XmlElement(name = "BusinessTransactionDocumentReferencePurchasingContractID")
    protected BusinessTransactionDocumentID businessTransactionDocumentReferencePurchasingContractID;
    @XmlElement(name = "BusinessTransactionDocumentReferenceInternalRequestID")
    protected BusinessTransactionDocumentID businessTransactionDocumentReferenceInternalRequestID;
    @XmlElement(name = "BusinessTransactionDocumentReferenceOutboundDeliveryID")
    protected BusinessTransactionDocumentID businessTransactionDocumentReferenceOutboundDeliveryID;
    @XmlElement(name = "BusinessTransactionDocumentReferencePurchaseRequestID")
    protected BusinessTransactionDocumentID businessTransactionDocumentReferencePurchaseRequestID;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getID() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setID(BusinessTransactionDocumentID value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad date.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Define el valor de la propiedad date.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

    /**
     * Obtiene el valor de la propiedad orderedDateTime.
     * 
     * @return
     *     possible object is
     *     {@link LOCALNORMALISEDDateTime }
     *     
     */
    public LOCALNORMALISEDDateTime getOrderedDateTime() {
        return orderedDateTime;
    }

    /**
     * Define el valor de la propiedad orderedDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link LOCALNORMALISEDDateTime }
     *     
     */
    public void setOrderedDateTime(LOCALNORMALISEDDateTime value) {
        this.orderedDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad dataOriginTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataOriginTypeCode() {
        return dataOriginTypeCode;
    }

    /**
     * Define el valor de la propiedad dataOriginTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataOriginTypeCode(String value) {
        this.dataOriginTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad typeCode.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentTypeCode }
     *     
     */
    public BusinessTransactionDocumentTypeCode getTypeCode() {
        return typeCode;
    }

    /**
     * Define el valor de la propiedad typeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentTypeCode }
     *     
     */
    public void setTypeCode(BusinessTransactionDocumentTypeCode value) {
        this.typeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad processingTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessingTypeCode() {
        return processingTypeCode;
    }

    /**
     * Define el valor de la propiedad processingTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessingTypeCode(String value) {
        this.processingTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMName }
     *     
     */
    public MEDIUMName getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMName }
     *     
     */
    public void setName(MEDIUMName value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad templateIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTemplateIndicator() {
        return templateIndicator;
    }

    /**
     * Define el valor de la propiedad templateIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTemplateIndicator(Boolean value) {
        this.templateIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad totalNetAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getTotalNetAmount() {
        return totalNetAmount;
    }

    /**
     * Define el valor de la propiedad totalNetAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setTotalNetAmount(Amount value) {
        this.totalNetAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad cashDiscountTerms.
     * 
     * @return
     *     possible object is
     *     {@link AccessCashDiscountTerms }
     *     
     */
    public AccessCashDiscountTerms getCashDiscountTerms() {
        return cashDiscountTerms;
    }

    /**
     * Define el valor de la propiedad cashDiscountTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link AccessCashDiscountTerms }
     *     
     */
    public void setCashDiscountTerms(AccessCashDiscountTerms value) {
        this.cashDiscountTerms = value;
    }

    /**
     * Obtiene el valor de la propiedad changeStateID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeStateID() {
        return changeStateID;
    }

    /**
     * Define el valor de la propiedad changeStateID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeStateID(String value) {
        this.changeStateID = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseDeliveryTerms }
     *     
     */
    public PurchaseOrderByIDResponseDeliveryTerms getDeliveryTerms() {
        return deliveryTerms;
    }

    /**
     * Define el valor de la propiedad deliveryTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseDeliveryTerms }
     *     
     */
    public void setDeliveryTerms(PurchaseOrderByIDResponseDeliveryTerms value) {
        this.deliveryTerms = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseStatus }
     *     
     */
    public PurchaseOrderByIDResponseStatus getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseStatus }
     *     
     */
    public void setStatus(PurchaseOrderByIDResponseStatus value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad billToParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseBillToParty }
     *     
     */
    public PurchaseOrderByIDResponseBillToParty getBillToParty() {
        return billToParty;
    }

    /**
     * Define el valor de la propiedad billToParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseBillToParty }
     *     
     */
    public void setBillToParty(PurchaseOrderByIDResponseBillToParty value) {
        this.billToParty = value;
    }

    /**
     * Obtiene el valor de la propiedad buyerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseBuyerParty }
     *     
     */
    public PurchaseOrderByIDResponseBuyerParty getBuyerParty() {
        return buyerParty;
    }

    /**
     * Define el valor de la propiedad buyerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseBuyerParty }
     *     
     */
    public void setBuyerParty(PurchaseOrderByIDResponseBuyerParty value) {
        this.buyerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeResponsibleParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseEmployeeResponsibleParty }
     *     
     */
    public PurchaseOrderByIDResponseEmployeeResponsibleParty getEmployeeResponsibleParty() {
        return employeeResponsibleParty;
    }

    /**
     * Define el valor de la propiedad employeeResponsibleParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseEmployeeResponsibleParty }
     *     
     */
    public void setEmployeeResponsibleParty(PurchaseOrderByIDResponseEmployeeResponsibleParty value) {
        this.employeeResponsibleParty = value;
    }

    /**
     * Obtiene el valor de la propiedad responsiblePurchasingUnitParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseResponsiblePurchasingUnitParty }
     *     
     */
    public PurchaseOrderByIDResponseResponsiblePurchasingUnitParty getResponsiblePurchasingUnitParty() {
        return responsiblePurchasingUnitParty;
    }

    /**
     * Define el valor de la propiedad responsiblePurchasingUnitParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseResponsiblePurchasingUnitParty }
     *     
     */
    public void setResponsiblePurchasingUnitParty(PurchaseOrderByIDResponseResponsiblePurchasingUnitParty value) {
        this.responsiblePurchasingUnitParty = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderByIDResponseItem }
     * 
     * 
     */
    public List<PurchaseOrderByIDResponseItem> getItem() {
        if (item == null) {
            item = new ArrayList<PurchaseOrderByIDResponseItem>();
        }
        return this.item;
    }

    /**
     * Obtiene el valor de la propiedad textCollection.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public MaintenanceTextCollection getTextCollection() {
        return textCollection;
    }

    /**
     * Define el valor de la propiedad textCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public void setTextCollection(MaintenanceTextCollection value) {
        this.textCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad attachmentFolder.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getAttachmentFolder() {
        return attachmentFolder;
    }

    /**
     * Define el valor de la propiedad attachmentFolder.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.attachmentFolder = value;
    }

    /**
     * Obtiene el valor de la propiedad sellerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseSellerParty }
     *     
     */
    public PurchaseOrderByIDResponseSellerParty getSellerParty() {
        return sellerParty;
    }

    /**
     * Define el valor de la propiedad sellerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseSellerParty }
     *     
     */
    public void setSellerParty(PurchaseOrderByIDResponseSellerParty value) {
        this.sellerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad systemAdministrativeData.
     * 
     * @return
     *     possible object is
     *     {@link SystemAdministrativeData }
     *     
     */
    public SystemAdministrativeData getSystemAdministrativeData() {
        return systemAdministrativeData;
    }

    /**
     * Define el valor de la propiedad systemAdministrativeData.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemAdministrativeData }
     *     
     */
    public void setSystemAdministrativeData(SystemAdministrativeData value) {
        this.systemAdministrativeData = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBusinessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID() {
        return businessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBusinessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID(BusinessTransactionDocumentID value) {
        this.businessTransactionDocumentReferenceGoodsAndServiceAcknowledgementID = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentReferencePurchasingContractID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBusinessTransactionDocumentReferencePurchasingContractID() {
        return businessTransactionDocumentReferencePurchasingContractID;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentReferencePurchasingContractID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBusinessTransactionDocumentReferencePurchasingContractID(BusinessTransactionDocumentID value) {
        this.businessTransactionDocumentReferencePurchasingContractID = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentReferenceInternalRequestID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBusinessTransactionDocumentReferenceInternalRequestID() {
        return businessTransactionDocumentReferenceInternalRequestID;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentReferenceInternalRequestID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBusinessTransactionDocumentReferenceInternalRequestID(BusinessTransactionDocumentID value) {
        this.businessTransactionDocumentReferenceInternalRequestID = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentReferenceOutboundDeliveryID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBusinessTransactionDocumentReferenceOutboundDeliveryID() {
        return businessTransactionDocumentReferenceOutboundDeliveryID;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentReferenceOutboundDeliveryID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBusinessTransactionDocumentReferenceOutboundDeliveryID(BusinessTransactionDocumentID value) {
        this.businessTransactionDocumentReferenceOutboundDeliveryID = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentReferencePurchaseRequestID.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public BusinessTransactionDocumentID getBusinessTransactionDocumentReferencePurchaseRequestID() {
        return businessTransactionDocumentReferencePurchaseRequestID;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentReferencePurchaseRequestID.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentID }
     *     
     */
    public void setBusinessTransactionDocumentReferencePurchaseRequestID(BusinessTransactionDocumentID value) {
        this.businessTransactionDocumentReferencePurchaseRequestID = value;
    }

}
