
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="WithholdingTax" type="{http://sap.com/xi/AP/Common/GDT}WithholdingTax" minOccurs="0"/>
 *         &lt;element name="TransactionCurrencyWithholdingTax" type="{http://sap.com/xi/AP/Common/GDT}WithholdingTax" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "uuid",
    "withholdingTax",
    "transactionCurrencyWithholdingTax"
})
public class PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "WithholdingTax")
    protected WithholdingTax withholdingTax;
    @XmlElement(name = "TransactionCurrencyWithholdingTax")
    protected WithholdingTax transactionCurrencyWithholdingTax;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad withholdingTax.
     * 
     * @return
     *     possible object is
     *     {@link WithholdingTax }
     *     
     */
    public WithholdingTax getWithholdingTax() {
        return withholdingTax;
    }

    /**
     * Define el valor de la propiedad withholdingTax.
     * 
     * @param value
     *     allowed object is
     *     {@link WithholdingTax }
     *     
     */
    public void setWithholdingTax(WithholdingTax value) {
        this.withholdingTax = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionCurrencyWithholdingTax.
     * 
     * @return
     *     possible object is
     *     {@link WithholdingTax }
     *     
     */
    public WithholdingTax getTransactionCurrencyWithholdingTax() {
        return transactionCurrencyWithholdingTax;
    }

    /**
     * Define el valor de la propiedad transactionCurrencyWithholdingTax.
     * 
     * @param value
     *     allowed object is
     *     {@link WithholdingTax }
     *     
     */
    public void setTransactionCurrencyWithholdingTax(WithholdingTax value) {
        this.transactionCurrencyWithholdingTax = value;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
