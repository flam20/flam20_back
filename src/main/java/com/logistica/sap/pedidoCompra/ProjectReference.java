
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para ProjectReference complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProjectReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProjectID" type="{http://sap.com/xi/AP/Common/GDT}ProjectID" minOccurs="0"/>
 *         &lt;element name="ProjectUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ProjectName" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="ProjectElementID" type="{http://sap.com/xi/AP/Common/GDT}ProjectElementID" minOccurs="0"/>
 *         &lt;element name="ProjectElementUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ProjectElementName" type="{http://sap.com/xi/AP/Common/GDT}MEDIUM_Name" minOccurs="0"/>
 *         &lt;element name="ProjectElementTypeCode" type="{http://sap.com/xi/AP/Common/GDT}ProjectElementTypeCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProjectReference", propOrder = {
    "projectID",
    "projectUUID",
    "projectName",
    "projectElementID",
    "projectElementUUID",
    "projectElementName",
    "projectElementTypeCode"
})
public class ProjectReference {

    @XmlElement(name = "ProjectID")
    protected ProjectID projectID;
    @XmlElement(name = "ProjectUUID")
    protected UUID projectUUID;
    @XmlElement(name = "ProjectName")
    protected MEDIUMName projectName;
    @XmlElement(name = "ProjectElementID")
    protected ProjectElementID projectElementID;
    @XmlElement(name = "ProjectElementUUID")
    protected UUID projectElementUUID;
    @XmlElement(name = "ProjectElementName")
    protected MEDIUMName projectElementName;
    @XmlElement(name = "ProjectElementTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String projectElementTypeCode;

    /**
     * Obtiene el valor de la propiedad projectID.
     * 
     * @return
     *     possible object is
     *     {@link ProjectID }
     *     
     */
    public ProjectID getProjectID() {
        return projectID;
    }

    /**
     * Define el valor de la propiedad projectID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectID }
     *     
     */
    public void setProjectID(ProjectID value) {
        this.projectID = value;
    }

    /**
     * Obtiene el valor de la propiedad projectUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getProjectUUID() {
        return projectUUID;
    }

    /**
     * Define el valor de la propiedad projectUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setProjectUUID(UUID value) {
        this.projectUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad projectName.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMName }
     *     
     */
    public MEDIUMName getProjectName() {
        return projectName;
    }

    /**
     * Define el valor de la propiedad projectName.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMName }
     *     
     */
    public void setProjectName(MEDIUMName value) {
        this.projectName = value;
    }

    /**
     * Obtiene el valor de la propiedad projectElementID.
     * 
     * @return
     *     possible object is
     *     {@link ProjectElementID }
     *     
     */
    public ProjectElementID getProjectElementID() {
        return projectElementID;
    }

    /**
     * Define el valor de la propiedad projectElementID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectElementID }
     *     
     */
    public void setProjectElementID(ProjectElementID value) {
        this.projectElementID = value;
    }

    /**
     * Obtiene el valor de la propiedad projectElementUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getProjectElementUUID() {
        return projectElementUUID;
    }

    /**
     * Define el valor de la propiedad projectElementUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setProjectElementUUID(UUID value) {
        this.projectElementUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad projectElementName.
     * 
     * @return
     *     possible object is
     *     {@link MEDIUMName }
     *     
     */
    public MEDIUMName getProjectElementName() {
        return projectElementName;
    }

    /**
     * Define el valor de la propiedad projectElementName.
     * 
     * @param value
     *     allowed object is
     *     {@link MEDIUMName }
     *     
     */
    public void setProjectElementName(MEDIUMName value) {
        this.projectElementName = value;
    }

    /**
     * Obtiene el valor de la propiedad projectElementTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectElementTypeCode() {
        return projectElementTypeCode;
    }

    /**
     * Define el valor de la propiedad projectElementTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectElementTypeCode(String value) {
        this.projectElementTypeCode = value;
    }

}
