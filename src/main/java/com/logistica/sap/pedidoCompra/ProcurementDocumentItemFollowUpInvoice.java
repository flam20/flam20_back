
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para ProcurementDocumentItemFollowUpInvoice complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProcurementDocumentItemFollowUpInvoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BusinessTransactionDocumentSettlementRelevanceIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="RequirementCode" type="{http://sap.com/xi/AP/Common/GDT}FollowUpBusinessTransactionDocumentRequirementCode" minOccurs="0"/>
 *         &lt;element name="EvaluatedReceiptSettlementIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="DeliveryBasedInvoiceVerificationIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcurementDocumentItemFollowUpInvoice", namespace = "http://sap.com/xi/AP/XU/SRM/Global", propOrder = {
    "businessTransactionDocumentSettlementRelevanceIndicator",
    "requirementCode",
    "evaluatedReceiptSettlementIndicator",
    "deliveryBasedInvoiceVerificationIndicator"
})
public class ProcurementDocumentItemFollowUpInvoice {

    @XmlElement(name = "BusinessTransactionDocumentSettlementRelevanceIndicator")
    protected Boolean businessTransactionDocumentSettlementRelevanceIndicator;
    @XmlElement(name = "RequirementCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String requirementCode;
    @XmlElement(name = "EvaluatedReceiptSettlementIndicator")
    protected Boolean evaluatedReceiptSettlementIndicator;
    @XmlElement(name = "DeliveryBasedInvoiceVerificationIndicator")
    protected Boolean deliveryBasedInvoiceVerificationIndicator;

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentSettlementRelevanceIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBusinessTransactionDocumentSettlementRelevanceIndicator() {
        return businessTransactionDocumentSettlementRelevanceIndicator;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentSettlementRelevanceIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBusinessTransactionDocumentSettlementRelevanceIndicator(Boolean value) {
        this.businessTransactionDocumentSettlementRelevanceIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad requirementCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequirementCode() {
        return requirementCode;
    }

    /**
     * Define el valor de la propiedad requirementCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequirementCode(String value) {
        this.requirementCode = value;
    }

    /**
     * Obtiene el valor de la propiedad evaluatedReceiptSettlementIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEvaluatedReceiptSettlementIndicator() {
        return evaluatedReceiptSettlementIndicator;
    }

    /**
     * Define el valor de la propiedad evaluatedReceiptSettlementIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEvaluatedReceiptSettlementIndicator(Boolean value) {
        this.evaluatedReceiptSettlementIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryBasedInvoiceVerificationIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeliveryBasedInvoiceVerificationIndicator() {
        return deliveryBasedInvoiceVerificationIndicator;
    }

    /**
     * Define el valor de la propiedad deliveryBasedInvoiceVerificationIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeliveryBasedInvoiceVerificationIndicator(Boolean value) {
        this.deliveryBasedInvoiceVerificationIndicator = value;
    }

}
