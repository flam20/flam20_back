
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItemStatus complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItemStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseOrderItemLifeCycleStatusCode" type="{http://sap.com/xi/AP/Common/GDT}PurchaseOrderLifeCycleStatusCode" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderItemApprovalStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ApprovalStatusCode" minOccurs="0"/>
 *         &lt;element name="CancellationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}CancellationStatusCode" minOccurs="0"/>
 *         &lt;element name="DeliveryProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoiceProcessingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}ProcessingStatusCode" minOccurs="0"/>
 *         &lt;element name="InvoicingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}InvoicingStatusCode" minOccurs="0"/>
 *         &lt;element name="OrderingStatusCode" type="{http://sap.com/xi/AP/Common/GDT}OrderingStatusCode" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderConfirmationStatusCode" type="{http://sap.com/xi/AP/Common/GDT}PurchaseOrderConfirmationStatusCode" minOccurs="0"/>
 *         &lt;element name="PurchaseOrderDeliveryStatusCode" type="{http://sap.com/xi/AP/Common/GDT}PurchaseOrderDeliveryStatusCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItemStatus", namespace = "http://sap.com/xi/AP/CRM/Global", propOrder = {
    "purchaseOrderItemLifeCycleStatusCode",
    "purchaseOrderItemApprovalStatusCode",
    "cancellationStatusCode",
    "deliveryProcessingStatusCode",
    "invoiceProcessingStatusCode",
    "invoicingStatusCode",
    "orderingStatusCode",
    "purchaseOrderConfirmationStatusCode",
    "purchaseOrderDeliveryStatusCode"
})
public class PurchaseOrderByIDResponseItemStatus {

    @XmlElement(name = "PurchaseOrderItemLifeCycleStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderItemLifeCycleStatusCode;
    @XmlElement(name = "PurchaseOrderItemApprovalStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderItemApprovalStatusCode;
    @XmlElement(name = "CancellationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String cancellationStatusCode;
    @XmlElement(name = "DeliveryProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryProcessingStatusCode;
    @XmlElement(name = "InvoiceProcessingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String invoiceProcessingStatusCode;
    @XmlElement(name = "InvoicingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String invoicingStatusCode;
    @XmlElement(name = "OrderingStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String orderingStatusCode;
    @XmlElement(name = "PurchaseOrderConfirmationStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderConfirmationStatusCode;
    @XmlElement(name = "PurchaseOrderDeliveryStatusCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String purchaseOrderDeliveryStatusCode;

    /**
     * Obtiene el valor de la propiedad purchaseOrderItemLifeCycleStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderItemLifeCycleStatusCode() {
        return purchaseOrderItemLifeCycleStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderItemLifeCycleStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderItemLifeCycleStatusCode(String value) {
        this.purchaseOrderItemLifeCycleStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderItemApprovalStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderItemApprovalStatusCode() {
        return purchaseOrderItemApprovalStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderItemApprovalStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderItemApprovalStatusCode(String value) {
        this.purchaseOrderItemApprovalStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cancellationStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCancellationStatusCode() {
        return cancellationStatusCode;
    }

    /**
     * Define el valor de la propiedad cancellationStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCancellationStatusCode(String value) {
        this.cancellationStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryProcessingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryProcessingStatusCode() {
        return deliveryProcessingStatusCode;
    }

    /**
     * Define el valor de la propiedad deliveryProcessingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryProcessingStatusCode(String value) {
        this.deliveryProcessingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceProcessingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceProcessingStatusCode() {
        return invoiceProcessingStatusCode;
    }

    /**
     * Define el valor de la propiedad invoiceProcessingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceProcessingStatusCode(String value) {
        this.invoiceProcessingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad invoicingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingStatusCode() {
        return invoicingStatusCode;
    }

    /**
     * Define el valor de la propiedad invoicingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingStatusCode(String value) {
        this.invoicingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad orderingStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderingStatusCode() {
        return orderingStatusCode;
    }

    /**
     * Define el valor de la propiedad orderingStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderingStatusCode(String value) {
        this.orderingStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderConfirmationStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderConfirmationStatusCode() {
        return purchaseOrderConfirmationStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderConfirmationStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderConfirmationStatusCode(String value) {
        this.purchaseOrderConfirmationStatusCode = value;
    }

    /**
     * Obtiene el valor de la propiedad purchaseOrderDeliveryStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseOrderDeliveryStatusCode() {
        return purchaseOrderDeliveryStatusCode;
    }

    /**
     * Define el valor de la propiedad purchaseOrderDeliveryStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseOrderDeliveryStatusCode(String value) {
        this.purchaseOrderDeliveryStatusCode = value;
    }

}
