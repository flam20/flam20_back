
package com.logistica.sap.pedidoCompra;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para ExchangeRate complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ExchangeRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UnitCurrency" type="{http://sap.com/xi/AP/Common/GDT}CurrencyCode"/>
 *         &lt;element name="QuotedCurrency" type="{http://sap.com/xi/AP/Common/GDT}CurrencyCode"/>
 *         &lt;element name="Rate" type="{http://sap.com/xi/AP/Common/GDT}ExchangeRateRate"/>
 *         &lt;element name="QuotationDateTime" type="{http://sap.com/xi/BASIS/Global}GLOBAL_DateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeRate", propOrder = {
    "unitCurrency",
    "quotedCurrency",
    "rate",
    "quotationDateTime"
})
public class ExchangeRate {

    @XmlElement(name = "UnitCurrency", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String unitCurrency;
    @XmlElement(name = "QuotedCurrency", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String quotedCurrency;
    @XmlElement(name = "Rate", required = true)
    protected BigDecimal rate;
    @XmlElement(name = "QuotationDateTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar quotationDateTime;

    /**
     * Obtiene el valor de la propiedad unitCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCurrency() {
        return unitCurrency;
    }

    /**
     * Define el valor de la propiedad unitCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCurrency(String value) {
        this.unitCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad quotedCurrency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuotedCurrency() {
        return quotedCurrency;
    }

    /**
     * Define el valor de la propiedad quotedCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuotedCurrency(String value) {
        this.quotedCurrency = value;
    }

    /**
     * Obtiene el valor de la propiedad rate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Define el valor de la propiedad rate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Obtiene el valor de la propiedad quotationDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQuotationDateTime() {
        return quotationDateTime;
    }

    /**
     * Define el valor de la propiedad quotationDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQuotationDateTime(XMLGregorianCalendar value) {
        this.quotationDateTime = value;
    }

}
