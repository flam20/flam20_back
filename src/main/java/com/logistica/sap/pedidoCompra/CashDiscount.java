
package com.logistica.sap.pedidoCompra;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para CashDiscount complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CashDiscount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DaysValue" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountDaysValue" minOccurs="0"/>
 *         &lt;element name="DayOfMonthValue" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountDayOfMonthValue" minOccurs="0"/>
 *         &lt;element name="MonthOffsetValue" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountMonthOffsetValue" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://sap.com/xi/AP/Common/GDT}Date" minOccurs="0"/>
 *         &lt;element name="Percent" type="{http://sap.com/xi/AP/Common/GDT}CashDiscountPercent"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CashDiscount", propOrder = {
    "daysValue",
    "dayOfMonthValue",
    "monthOffsetValue",
    "endDate",
    "percent"
})
public class CashDiscount {

    @XmlElement(name = "DaysValue")
    protected Integer daysValue;
    @XmlElement(name = "DayOfMonthValue")
    protected Integer dayOfMonthValue;
    @XmlElement(name = "MonthOffsetValue")
    protected Integer monthOffsetValue;
    @XmlElement(name = "EndDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    @XmlElement(name = "Percent", required = true)
    protected BigDecimal percent;

    /**
     * Obtiene el valor de la propiedad daysValue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDaysValue() {
        return daysValue;
    }

    /**
     * Define el valor de la propiedad daysValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDaysValue(Integer value) {
        this.daysValue = value;
    }

    /**
     * Obtiene el valor de la propiedad dayOfMonthValue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDayOfMonthValue() {
        return dayOfMonthValue;
    }

    /**
     * Define el valor de la propiedad dayOfMonthValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDayOfMonthValue(Integer value) {
        this.dayOfMonthValue = value;
    }

    /**
     * Obtiene el valor de la propiedad monthOffsetValue.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMonthOffsetValue() {
        return monthOffsetValue;
    }

    /**
     * Define el valor de la propiedad monthOffsetValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMonthOffsetValue(Integer value) {
        this.monthOffsetValue = value;
    }

    /**
     * Obtiene el valor de la propiedad endDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Define el valor de la propiedad endDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Obtiene el valor de la propiedad percent.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercent() {
        return percent;
    }

    /**
     * Define el valor de la propiedad percent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercent(BigDecimal value) {
        this.percent = value;
    }

}
