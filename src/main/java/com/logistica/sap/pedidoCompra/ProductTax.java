
package com.logistica.sap.pedidoCompra;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para ProductTax complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProductTax">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CountryCode" type="{http://sap.com/xi/AP/Common/GDT}CountryCode" minOccurs="0"/>
 *         &lt;element name="RegionCode" type="{http://sap.com/xi/AP/Common/GDT}RegionCode" minOccurs="0"/>
 *         &lt;element name="JurisdictionCode" type="{http://sap.com/xi/AP/Common/GDT}TaxJurisdictionCode" minOccurs="0"/>
 *         &lt;element name="JurisdictionSubdivisionCode" type="{http://sap.com/xi/AP/Common/GDT}TaxJurisdictionSubdivisionCode" minOccurs="0"/>
 *         &lt;element name="JurisdictionSubdivisionTypeCode" type="{http://sap.com/xi/AP/Common/GDT}TaxJurisdictionSubdivisionTypeCode" minOccurs="0"/>
 *         &lt;element name="EventTypeCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTaxEventTypeCode" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}TaxTypeCode" minOccurs="0"/>
 *         &lt;element name="RateTypeCode" type="{http://sap.com/xi/AP/Common/GDT}TaxRateTypeCode" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://sap.com/xi/AP/Common/GDT}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="BaseAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="Percent" type="{http://sap.com/xi/AP/Common/GDT}Percent" minOccurs="0"/>
 *         &lt;element name="BaseQuantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="BaseQuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="Rate" type="{http://sap.com/xi/AP/Common/GDT}Rate" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="InternalAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="NonDeductiblePercent" type="{http://sap.com/xi/AP/Common/GDT}Percent" minOccurs="0"/>
 *         &lt;element name="NonDeductibleAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="DeductibleAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="DeductibilityCode" type="{http://sap.com/xi/AP/Common/GDT}TaxDeductibilityCode" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentItemGroupID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemGroupID" minOccurs="0"/>
 *         &lt;element name="EuropeanCommunityVATTriangulationIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="DueCategoryCode" type="{http://sap.com/xi/AP/Common/GDT}DueCategoryCode" minOccurs="0"/>
 *         &lt;element name="StatisticRelevanceIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="DeferredIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="Exemption" type="{http://sap.com/xi/AP/Common/GDT}TaxExemption" minOccurs="0"/>
 *         &lt;element name="FollowUpExemption" type="{http://sap.com/xi/AP/Common/GDT}TaxExemption" minOccurs="0"/>
 *         &lt;element name="LegallyRequiredPhrase" type="{http://sap.com/xi/AP/Common/GDT}LegallyRequiredPhraseText" minOccurs="0"/>
 *         &lt;element name="ExchangeRate" type="{http://sap.com/xi/AP/Common/GDT}ExchangeRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductTax", propOrder = {
    "countryCode",
    "regionCode",
    "jurisdictionCode",
    "jurisdictionSubdivisionCode",
    "jurisdictionSubdivisionTypeCode",
    "eventTypeCode",
    "typeCode",
    "rateTypeCode",
    "currencyCode",
    "baseAmount",
    "percent",
    "baseQuantity",
    "baseQuantityTypeCode",
    "rate",
    "amount",
    "internalAmount",
    "nonDeductiblePercent",
    "nonDeductibleAmount",
    "deductibleAmount",
    "deductibilityCode",
    "businessTransactionDocumentItemGroupID",
    "europeanCommunityVATTriangulationIndicator",
    "dueCategoryCode",
    "statisticRelevanceIndicator",
    "deferredIndicator",
    "exemption",
    "followUpExemption",
    "legallyRequiredPhrase",
    "exchangeRate"
})
public class ProductTax {

    @XmlElement(name = "CountryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String countryCode;
    @XmlElement(name = "RegionCode")
    protected RegionCode regionCode;
    @XmlElement(name = "JurisdictionCode")
    protected TaxJurisdictionCode jurisdictionCode;
    @XmlElement(name = "JurisdictionSubdivisionCode")
    protected TaxJurisdictionSubdivisionCode jurisdictionSubdivisionCode;
    @XmlElement(name = "JurisdictionSubdivisionTypeCode")
    protected TaxJurisdictionSubdivisionTypeCode jurisdictionSubdivisionTypeCode;
    @XmlElement(name = "EventTypeCode")
    protected ProductTaxEventTypeCode eventTypeCode;
    @XmlElement(name = "TypeCode")
    protected TaxTypeCode typeCode;
    @XmlElement(name = "RateTypeCode")
    protected TaxRateTypeCode rateTypeCode;
    @XmlElement(name = "CurrencyCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String currencyCode;
    @XmlElement(name = "BaseAmount")
    protected Amount baseAmount;
    @XmlElement(name = "Percent")
    protected BigDecimal percent;
    @XmlElement(name = "BaseQuantity")
    protected Quantity baseQuantity;
    @XmlElement(name = "BaseQuantityTypeCode")
    protected QuantityTypeCode baseQuantityTypeCode;
    @XmlElement(name = "Rate")
    protected Rate rate;
    @XmlElement(name = "Amount")
    protected Amount amount;
    @XmlElement(name = "InternalAmount")
    protected Amount internalAmount;
    @XmlElement(name = "NonDeductiblePercent")
    protected BigDecimal nonDeductiblePercent;
    @XmlElement(name = "NonDeductibleAmount")
    protected Amount nonDeductibleAmount;
    @XmlElement(name = "DeductibleAmount")
    protected Amount deductibleAmount;
    @XmlElement(name = "DeductibilityCode")
    protected TaxDeductibilityCode deductibilityCode;
    @XmlElement(name = "BusinessTransactionDocumentItemGroupID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessTransactionDocumentItemGroupID;
    @XmlElement(name = "EuropeanCommunityVATTriangulationIndicator")
    protected Boolean europeanCommunityVATTriangulationIndicator;
    @XmlElement(name = "DueCategoryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String dueCategoryCode;
    @XmlElement(name = "StatisticRelevanceIndicator")
    protected Boolean statisticRelevanceIndicator;
    @XmlElement(name = "DeferredIndicator")
    protected Boolean deferredIndicator;
    @XmlElement(name = "Exemption")
    protected TaxExemption exemption;
    @XmlElement(name = "FollowUpExemption")
    protected TaxExemption followUpExemption;
    @XmlElement(name = "LegallyRequiredPhrase")
    protected String legallyRequiredPhrase;
    @XmlElement(name = "ExchangeRate")
    protected ExchangeRate exchangeRate;

    /**
     * Obtiene el valor de la propiedad countryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Define el valor de la propiedad countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad regionCode.
     * 
     * @return
     *     possible object is
     *     {@link RegionCode }
     *     
     */
    public RegionCode getRegionCode() {
        return regionCode;
    }

    /**
     * Define el valor de la propiedad regionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionCode }
     *     
     */
    public void setRegionCode(RegionCode value) {
        this.regionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad jurisdictionCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public TaxJurisdictionCode getJurisdictionCode() {
        return jurisdictionCode;
    }

    /**
     * Define el valor de la propiedad jurisdictionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxJurisdictionCode }
     *     
     */
    public void setJurisdictionCode(TaxJurisdictionCode value) {
        this.jurisdictionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad jurisdictionSubdivisionCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxJurisdictionSubdivisionCode }
     *     
     */
    public TaxJurisdictionSubdivisionCode getJurisdictionSubdivisionCode() {
        return jurisdictionSubdivisionCode;
    }

    /**
     * Define el valor de la propiedad jurisdictionSubdivisionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxJurisdictionSubdivisionCode }
     *     
     */
    public void setJurisdictionSubdivisionCode(TaxJurisdictionSubdivisionCode value) {
        this.jurisdictionSubdivisionCode = value;
    }

    /**
     * Obtiene el valor de la propiedad jurisdictionSubdivisionTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxJurisdictionSubdivisionTypeCode }
     *     
     */
    public TaxJurisdictionSubdivisionTypeCode getJurisdictionSubdivisionTypeCode() {
        return jurisdictionSubdivisionTypeCode;
    }

    /**
     * Define el valor de la propiedad jurisdictionSubdivisionTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxJurisdictionSubdivisionTypeCode }
     *     
     */
    public void setJurisdictionSubdivisionTypeCode(TaxJurisdictionSubdivisionTypeCode value) {
        this.jurisdictionSubdivisionTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad eventTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxEventTypeCode }
     *     
     */
    public ProductTaxEventTypeCode getEventTypeCode() {
        return eventTypeCode;
    }

    /**
     * Define el valor de la propiedad eventTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxEventTypeCode }
     *     
     */
    public void setEventTypeCode(ProductTaxEventTypeCode value) {
        this.eventTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad typeCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxTypeCode }
     *     
     */
    public TaxTypeCode getTypeCode() {
        return typeCode;
    }

    /**
     * Define el valor de la propiedad typeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxTypeCode }
     *     
     */
    public void setTypeCode(TaxTypeCode value) {
        this.typeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rateTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxRateTypeCode }
     *     
     */
    public TaxRateTypeCode getRateTypeCode() {
        return rateTypeCode;
    }

    /**
     * Define el valor de la propiedad rateTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxRateTypeCode }
     *     
     */
    public void setRateTypeCode(TaxRateTypeCode value) {
        this.rateTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad baseAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getBaseAmount() {
        return baseAmount;
    }

    /**
     * Define el valor de la propiedad baseAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setBaseAmount(Amount value) {
        this.baseAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad percent.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercent() {
        return percent;
    }

    /**
     * Define el valor de la propiedad percent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercent(BigDecimal value) {
        this.percent = value;
    }

    /**
     * Obtiene el valor de la propiedad baseQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getBaseQuantity() {
        return baseQuantity;
    }

    /**
     * Define el valor de la propiedad baseQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setBaseQuantity(Quantity value) {
        this.baseQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad baseQuantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getBaseQuantityTypeCode() {
        return baseQuantityTypeCode;
    }

    /**
     * Define el valor de la propiedad baseQuantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setBaseQuantityTypeCode(QuantityTypeCode value) {
        this.baseQuantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rate.
     * 
     * @return
     *     possible object is
     *     {@link Rate }
     *     
     */
    public Rate getRate() {
        return rate;
    }

    /**
     * Define el valor de la propiedad rate.
     * 
     * @param value
     *     allowed object is
     *     {@link Rate }
     *     
     */
    public void setRate(Rate value) {
        this.rate = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setAmount(Amount value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad internalAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getInternalAmount() {
        return internalAmount;
    }

    /**
     * Define el valor de la propiedad internalAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setInternalAmount(Amount value) {
        this.internalAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad nonDeductiblePercent.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getNonDeductiblePercent() {
        return nonDeductiblePercent;
    }

    /**
     * Define el valor de la propiedad nonDeductiblePercent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setNonDeductiblePercent(BigDecimal value) {
        this.nonDeductiblePercent = value;
    }

    /**
     * Obtiene el valor de la propiedad nonDeductibleAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getNonDeductibleAmount() {
        return nonDeductibleAmount;
    }

    /**
     * Define el valor de la propiedad nonDeductibleAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setNonDeductibleAmount(Amount value) {
        this.nonDeductibleAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad deductibleAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getDeductibleAmount() {
        return deductibleAmount;
    }

    /**
     * Define el valor de la propiedad deductibleAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setDeductibleAmount(Amount value) {
        this.deductibleAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad deductibilityCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxDeductibilityCode }
     *     
     */
    public TaxDeductibilityCode getDeductibilityCode() {
        return deductibilityCode;
    }

    /**
     * Define el valor de la propiedad deductibilityCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxDeductibilityCode }
     *     
     */
    public void setDeductibilityCode(TaxDeductibilityCode value) {
        this.deductibilityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentItemGroupID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessTransactionDocumentItemGroupID() {
        return businessTransactionDocumentItemGroupID;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentItemGroupID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessTransactionDocumentItemGroupID(String value) {
        this.businessTransactionDocumentItemGroupID = value;
    }

    /**
     * Obtiene el valor de la propiedad europeanCommunityVATTriangulationIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEuropeanCommunityVATTriangulationIndicator() {
        return europeanCommunityVATTriangulationIndicator;
    }

    /**
     * Define el valor de la propiedad europeanCommunityVATTriangulationIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEuropeanCommunityVATTriangulationIndicator(Boolean value) {
        this.europeanCommunityVATTriangulationIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad dueCategoryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDueCategoryCode() {
        return dueCategoryCode;
    }

    /**
     * Define el valor de la propiedad dueCategoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDueCategoryCode(String value) {
        this.dueCategoryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad statisticRelevanceIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStatisticRelevanceIndicator() {
        return statisticRelevanceIndicator;
    }

    /**
     * Define el valor de la propiedad statisticRelevanceIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStatisticRelevanceIndicator(Boolean value) {
        this.statisticRelevanceIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad deferredIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDeferredIndicator() {
        return deferredIndicator;
    }

    /**
     * Define el valor de la propiedad deferredIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDeferredIndicator(Boolean value) {
        this.deferredIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad exemption.
     * 
     * @return
     *     possible object is
     *     {@link TaxExemption }
     *     
     */
    public TaxExemption getExemption() {
        return exemption;
    }

    /**
     * Define el valor de la propiedad exemption.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxExemption }
     *     
     */
    public void setExemption(TaxExemption value) {
        this.exemption = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpExemption.
     * 
     * @return
     *     possible object is
     *     {@link TaxExemption }
     *     
     */
    public TaxExemption getFollowUpExemption() {
        return followUpExemption;
    }

    /**
     * Define el valor de la propiedad followUpExemption.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxExemption }
     *     
     */
    public void setFollowUpExemption(TaxExemption value) {
        this.followUpExemption = value;
    }

    /**
     * Obtiene el valor de la propiedad legallyRequiredPhrase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegallyRequiredPhrase() {
        return legallyRequiredPhrase;
    }

    /**
     * Define el valor de la propiedad legallyRequiredPhrase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegallyRequiredPhrase(String value) {
        this.legallyRequiredPhrase = value;
    }

    /**
     * Obtiene el valor de la propiedad exchangeRate.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRate }
     *     
     */
    public ExchangeRate getExchangeRate() {
        return exchangeRate;
    }

    /**
     * Define el valor de la propiedad exchangeRate.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRate }
     *     
     */
    public void setExchangeRate(ExchangeRate value) {
        this.exchangeRate = value;
    }

}
