
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderReadItemProductTaxDetails complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderReadItemProductTaxDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="TransactionCurrencyProductTax" type="{http://sap.com/xi/AP/Common/GDT}ProductTax" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderReadItemProductTaxDetails", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "uuid",
    "transactionCurrencyProductTax"
})
public class PurchaseOrderReadItemProductTaxDetails {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "TransactionCurrencyProductTax")
    protected ProductTax transactionCurrencyProductTax;

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionCurrencyProductTax.
     * 
     * @return
     *     possible object is
     *     {@link ProductTax }
     *     
     */
    public ProductTax getTransactionCurrencyProductTax() {
        return transactionCurrencyProductTax;
    }

    /**
     * Define el valor de la propiedad transactionCurrencyProductTax.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTax }
     *     
     */
    public void setTransactionCurrencyProductTax(ProductTax value) {
        this.transactionCurrencyProductTax = value;
    }

}
