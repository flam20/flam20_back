
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para PurchaseOrderByIDResponseItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDResponseItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;element name="ItemUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="TypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemTypeCode" minOccurs="0"/>
 *         &lt;element name="OrderedDateTime" type="{http://sap.com/xi/BASIS/Global}LOCALNORMALISED_DateTime" minOccurs="0"/>
 *         &lt;element name="DeliveryPeriod" type="{http://sap.com/xi/AP/Common/GDT}UPPEROPEN_LOCALNORMALISED_DateTimePeriod" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="QuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="TotalDeliveredQuantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="TotalDeliveredQuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="TotalInvoiceQuantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="TotalInvoiceQuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="TotalInvoicedQuantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="TotalInvoicedQuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="ItemStatus" type="{http://sap.com/xi/AP/CRM/Global}PurchaseOrderByIDResponseItemStatus" minOccurs="0"/>
 *         &lt;element name="GrossAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="NetAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="CostUpperLimitAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="CostUpperLimitExpectedAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="GrossUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="NetUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="ListUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="FollowUpPurchaseOrderConfirmation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation" minOccurs="0"/>
 *         &lt;element name="FollowUpDelivery" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemFollowUpDelivery" minOccurs="0"/>
 *         &lt;element name="FollowUpInvoice" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemFollowUpInvoice" minOccurs="0"/>
 *         &lt;element name="DirectMaterialIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ThirdPartyDealIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ItemProduct" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemProduct" minOccurs="0"/>
 *         &lt;element name="ItemDeliveryTerms" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="EndBuyerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemEndBuyerParty" minOccurs="0"/>
 *         &lt;element name="ProductRecipientParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemProductReceipientParty" minOccurs="0"/>
 *         &lt;element name="ServicePerformerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemServicePerfomerParty" minOccurs="0"/>
 *         &lt;element name="ItemAccountingCodingBlockDistribution" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}MaintenanceAccountingCodingBlockDistribution" minOccurs="0"/>
 *         &lt;element name="ItemAttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="ItemTextCollection" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceTextCollection" minOccurs="0"/>
 *         &lt;element name="ItemImatListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ItemPriceCalculation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderReadPriceCalculationItem" minOccurs="0"/>
 *         &lt;element name="ItemTaxCalculation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderReadTaxCalculationItem" minOccurs="0"/>
 *         &lt;element name="ShipToLocationID" type="{http://sap.com/xi/AP/Common/GDT}LocationID" minOccurs="0"/>
 *         &lt;element name="ShipToLocationAddressHostUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ItemScheduleLine" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemScheduleLine" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB983553DC24C0130"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9841A0C7DD2251F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB98429635A3BA58B"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9842D0EC5ED058F"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB98430FE052B85A7"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB984372D9B5785B0"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9843CEE6412C5BB"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB98442772372061D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB98446E594CF262A"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9844C0E023D062D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB984510A83B10631"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9845753B378068C"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9845A7D669BC693"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9845D8B40048699"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB98464D03347A69D"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9846853E083C6A3"/>
 *         &lt;group ref="{http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU}Ext00163E9C25DC1EEBB9846B9FB7CDE6A9"/>
 *         &lt;group ref="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDResponseItemGloExtension"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDResponseItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "itemID",
    "itemUUID",
    "typeCode",
    "orderedDateTime",
    "deliveryPeriod",
    "description",
    "quantity",
    "quantityTypeCode",
    "totalDeliveredQuantity",
    "totalDeliveredQuantityTypeCode",
    "totalInvoiceQuantity",
    "totalInvoiceQuantityTypeCode",
    "totalInvoicedQuantity",
    "totalInvoicedQuantityTypeCode",
    "itemStatus",
    "grossAmount",
    "netAmount",
    "costUpperLimitAmount",
    "costUpperLimitExpectedAmount",
    "grossUnitPrice",
    "netUnitPrice",
    "listUnitPrice",
    "followUpPurchaseOrderConfirmation",
    "followUpDelivery",
    "followUpInvoice",
    "directMaterialIndicator",
    "thirdPartyDealIndicator",
    "itemProduct",
    "itemDeliveryTerms",
    "endBuyerParty",
    "productRecipientParty",
    "servicePerformerParty",
    "itemAccountingCodingBlockDistribution",
    "itemAttachmentFolder",
    "itemTextCollection",
    "itemImatListCompleteTransmissionIndicator",
    "itemPriceCalculation",
    "itemTaxCalculation",
    "shipToLocationID",
    "shipToLocationAddressHostUUID",
    "itemScheduleLine",
    "ubicacinOrigenS",
    "unidaddeventaS",
    "ubicacinDestinoS",
    "ubicacinCruceS",
    "tipodeequipoS",
    "shipperS",
    "requierePODS",
    "pzsPBPCS",
    "nmerodeequipodecargaS",
    "ndeservicio2",
    "guaMasterS",
    "guaHouseS",
    "fechadeAgenteAduanalS",
    "fechaDestinoS",
    "fechaCruceS",
    "fechaCargaS",
    "consignatariosS",
    "hsnCodeIndiaCode"
})
public class PurchaseOrderByIDResponseItem {

    @XmlElement(name = "ItemID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String itemID;
    @XmlElement(name = "ItemUUID")
    protected UUID itemUUID;
    @XmlElement(name = "TypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String typeCode;
    @XmlElement(name = "OrderedDateTime")
    protected LOCALNORMALISEDDateTime orderedDateTime;
    @XmlElement(name = "DeliveryPeriod")
    protected UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod;
    @XmlElement(name = "Description")
    protected SHORTDescription description;
    @XmlElement(name = "Quantity")
    protected Quantity quantity;
    @XmlElement(name = "QuantityTypeCode")
    protected QuantityTypeCode quantityTypeCode;
    @XmlElement(name = "TotalDeliveredQuantity")
    protected Quantity totalDeliveredQuantity;
    @XmlElement(name = "TotalDeliveredQuantityTypeCode")
    protected QuantityTypeCode totalDeliveredQuantityTypeCode;
    @XmlElement(name = "TotalInvoiceQuantity")
    protected Quantity totalInvoiceQuantity;
    @XmlElement(name = "TotalInvoiceQuantityTypeCode")
    protected QuantityTypeCode totalInvoiceQuantityTypeCode;
    @XmlElement(name = "TotalInvoicedQuantity")
    protected Quantity totalInvoicedQuantity;
    @XmlElement(name = "TotalInvoicedQuantityTypeCode")
    protected QuantityTypeCode totalInvoicedQuantityTypeCode;
    @XmlElement(name = "ItemStatus")
    protected PurchaseOrderByIDResponseItemStatus itemStatus;
    @XmlElement(name = "GrossAmount")
    protected Amount grossAmount;
    @XmlElement(name = "NetAmount")
    protected Amount netAmount;
    @XmlElement(name = "CostUpperLimitAmount")
    protected Amount costUpperLimitAmount;
    @XmlElement(name = "CostUpperLimitExpectedAmount")
    protected Amount costUpperLimitExpectedAmount;
    @XmlElement(name = "GrossUnitPrice")
    protected Price grossUnitPrice;
    @XmlElement(name = "NetUnitPrice")
    protected Price netUnitPrice;
    @XmlElement(name = "ListUnitPrice")
    protected Price listUnitPrice;
    @XmlElement(name = "FollowUpPurchaseOrderConfirmation")
    protected PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation followUpPurchaseOrderConfirmation;
    @XmlElement(name = "FollowUpDelivery")
    protected PurchaseOrderByIDResponseItemFollowUpDelivery followUpDelivery;
    @XmlElement(name = "FollowUpInvoice")
    protected PurchaseOrderByIDResponseItemFollowUpInvoice followUpInvoice;
    @XmlElement(name = "DirectMaterialIndicator")
    protected Boolean directMaterialIndicator;
    @XmlElement(name = "ThirdPartyDealIndicator")
    protected Boolean thirdPartyDealIndicator;
    @XmlElement(name = "ItemProduct")
    protected PurchaseOrderByIDResponseItemProduct itemProduct;
    @XmlElement(name = "ItemDeliveryTerms")
    protected PurchaseOrderByIDResponseItemDeliveryTerms itemDeliveryTerms;
    @XmlElement(name = "EndBuyerParty")
    protected PurchaseOrderByIDResponseItemEndBuyerParty endBuyerParty;
    @XmlElement(name = "ProductRecipientParty")
    protected PurchaseOrderByIDResponseItemProductReceipientParty productRecipientParty;
    @XmlElement(name = "ServicePerformerParty")
    protected PurchaseOrderByIDResponseItemServicePerfomerParty servicePerformerParty;
    @XmlElement(name = "ItemAccountingCodingBlockDistribution")
    protected MaintenanceAccountingCodingBlockDistribution itemAccountingCodingBlockDistribution;
    @XmlElement(name = "ItemAttachmentFolder")
    protected MaintenanceAttachmentFolder itemAttachmentFolder;
    @XmlElement(name = "ItemTextCollection")
    protected MaintenanceTextCollection itemTextCollection;
    @XmlElement(name = "ItemImatListCompleteTransmissionIndicator")
    protected Boolean itemImatListCompleteTransmissionIndicator;
    @XmlElement(name = "ItemPriceCalculation")
    protected PurchaseOrderReadPriceCalculationItem itemPriceCalculation;
    @XmlElement(name = "ItemTaxCalculation")
    protected PurchaseOrderReadTaxCalculationItem itemTaxCalculation;
    @XmlElement(name = "ShipToLocationID")
    protected LocationID shipToLocationID;
    @XmlElement(name = "ShipToLocationAddressHostUUID")
    protected UUID shipToLocationAddressHostUUID;
    @XmlElement(name = "ItemScheduleLine")
    protected List<PurchaseOrderMaintainRequestBundleItemScheduleLine> itemScheduleLine;
    @XmlElement(name = "UbicacinOrigenS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinOrigenS;
    @XmlElement(name = "UnidaddeventaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String unidaddeventaS;
    @XmlElement(name = "UbicacinDestinoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinDestinoS;
    @XmlElement(name = "UbicacinCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ubicacinCruceS;
    @XmlElement(name = "TipodeequipoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String tipodeequipoS;
    @XmlElement(name = "ShipperS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String shipperS;
    @XmlElement(name = "RequierePODS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String requierePODS;
    @XmlElement(name = "PzsPBPCS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String pzsPBPCS;
    @XmlElement(name = "NmerodeequipodecargaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String nmerodeequipodecargaS;
    @XmlElement(name = "Ndeservicio2", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String ndeservicio2;
    @XmlElement(name = "GuaMasterS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaMasterS;
    @XmlElement(name = "GuaHouseS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String guaHouseS;
    @XmlElement(name = "FechadeAgenteAduanalS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechadeAgenteAduanalS;
    @XmlElement(name = "FechaDestinoS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaDestinoS;
    @XmlElement(name = "FechaCruceS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaCruceS;
    @XmlElement(name = "FechaCargaS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaCargaS;
    @XmlElement(name = "ConsignatariosS", namespace = "http://sap.com/xi/AP/CustomerExtension/BYD/A3QYU")
    protected String consignatariosS;
    @XmlElement(name = "HSNCodeIndiaCode", namespace = "http://sap.com/xi/A1S/Global")
    protected ProductTaxStandardClassificationCode hsnCodeIndiaCode;

    /**
     * Obtiene el valor de la propiedad itemID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Define el valor de la propiedad itemID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Obtiene el valor de la propiedad itemUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getItemUUID() {
        return itemUUID;
    }

    /**
     * Define el valor de la propiedad itemUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setItemUUID(UUID value) {
        this.itemUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad typeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCode() {
        return typeCode;
    }

    /**
     * Define el valor de la propiedad typeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCode(String value) {
        this.typeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad orderedDateTime.
     * 
     * @return
     *     possible object is
     *     {@link LOCALNORMALISEDDateTime }
     *     
     */
    public LOCALNORMALISEDDateTime getOrderedDateTime() {
        return orderedDateTime;
    }

    /**
     * Define el valor de la propiedad orderedDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link LOCALNORMALISEDDateTime }
     *     
     */
    public void setOrderedDateTime(LOCALNORMALISEDDateTime value) {
        this.orderedDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryPeriod.
     * 
     * @return
     *     possible object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod getDeliveryPeriod() {
        return deliveryPeriod;
    }

    /**
     * Define el valor de la propiedad deliveryPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public void setDeliveryPeriod(UPPEROPENLOCALNORMALISEDDateTimePeriod value) {
        this.deliveryPeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setDescription(SHORTDescription value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setQuantity(Quantity value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad quantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getQuantityTypeCode() {
        return quantityTypeCode;
    }

    /**
     * Define el valor de la propiedad quantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setQuantityTypeCode(QuantityTypeCode value) {
        this.quantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad totalDeliveredQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getTotalDeliveredQuantity() {
        return totalDeliveredQuantity;
    }

    /**
     * Define el valor de la propiedad totalDeliveredQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setTotalDeliveredQuantity(Quantity value) {
        this.totalDeliveredQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad totalDeliveredQuantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getTotalDeliveredQuantityTypeCode() {
        return totalDeliveredQuantityTypeCode;
    }

    /**
     * Define el valor de la propiedad totalDeliveredQuantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setTotalDeliveredQuantityTypeCode(QuantityTypeCode value) {
        this.totalDeliveredQuantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad totalInvoiceQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getTotalInvoiceQuantity() {
        return totalInvoiceQuantity;
    }

    /**
     * Define el valor de la propiedad totalInvoiceQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setTotalInvoiceQuantity(Quantity value) {
        this.totalInvoiceQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad totalInvoiceQuantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getTotalInvoiceQuantityTypeCode() {
        return totalInvoiceQuantityTypeCode;
    }

    /**
     * Define el valor de la propiedad totalInvoiceQuantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setTotalInvoiceQuantityTypeCode(QuantityTypeCode value) {
        this.totalInvoiceQuantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad totalInvoicedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getTotalInvoicedQuantity() {
        return totalInvoicedQuantity;
    }

    /**
     * Define el valor de la propiedad totalInvoicedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setTotalInvoicedQuantity(Quantity value) {
        this.totalInvoicedQuantity = value;
    }

    /**
     * Obtiene el valor de la propiedad totalInvoicedQuantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getTotalInvoicedQuantityTypeCode() {
        return totalInvoicedQuantityTypeCode;
    }

    /**
     * Define el valor de la propiedad totalInvoicedQuantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setTotalInvoicedQuantityTypeCode(QuantityTypeCode value) {
        this.totalInvoicedQuantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad itemStatus.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemStatus }
     *     
     */
    public PurchaseOrderByIDResponseItemStatus getItemStatus() {
        return itemStatus;
    }

    /**
     * Define el valor de la propiedad itemStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemStatus }
     *     
     */
    public void setItemStatus(PurchaseOrderByIDResponseItemStatus value) {
        this.itemStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad grossAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getGrossAmount() {
        return grossAmount;
    }

    /**
     * Define el valor de la propiedad grossAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setGrossAmount(Amount value) {
        this.grossAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad netAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getNetAmount() {
        return netAmount;
    }

    /**
     * Define el valor de la propiedad netAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setNetAmount(Amount value) {
        this.netAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad costUpperLimitAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getCostUpperLimitAmount() {
        return costUpperLimitAmount;
    }

    /**
     * Define el valor de la propiedad costUpperLimitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setCostUpperLimitAmount(Amount value) {
        this.costUpperLimitAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad costUpperLimitExpectedAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getCostUpperLimitExpectedAmount() {
        return costUpperLimitExpectedAmount;
    }

    /**
     * Define el valor de la propiedad costUpperLimitExpectedAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setCostUpperLimitExpectedAmount(Amount value) {
        this.costUpperLimitExpectedAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad grossUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getGrossUnitPrice() {
        return grossUnitPrice;
    }

    /**
     * Define el valor de la propiedad grossUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setGrossUnitPrice(Price value) {
        this.grossUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad netUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getNetUnitPrice() {
        return netUnitPrice;
    }

    /**
     * Define el valor de la propiedad netUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setNetUnitPrice(Price value) {
        this.netUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad listUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getListUnitPrice() {
        return listUnitPrice;
    }

    /**
     * Define el valor de la propiedad listUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setListUnitPrice(Price value) {
        this.listUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpPurchaseOrderConfirmation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation }
     *     
     */
    public PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation getFollowUpPurchaseOrderConfirmation() {
        return followUpPurchaseOrderConfirmation;
    }

    /**
     * Define el valor de la propiedad followUpPurchaseOrderConfirmation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation }
     *     
     */
    public void setFollowUpPurchaseOrderConfirmation(PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation value) {
        this.followUpPurchaseOrderConfirmation = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpDelivery.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemFollowUpDelivery }
     *     
     */
    public PurchaseOrderByIDResponseItemFollowUpDelivery getFollowUpDelivery() {
        return followUpDelivery;
    }

    /**
     * Define el valor de la propiedad followUpDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemFollowUpDelivery }
     *     
     */
    public void setFollowUpDelivery(PurchaseOrderByIDResponseItemFollowUpDelivery value) {
        this.followUpDelivery = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpInvoice.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemFollowUpInvoice }
     *     
     */
    public PurchaseOrderByIDResponseItemFollowUpInvoice getFollowUpInvoice() {
        return followUpInvoice;
    }

    /**
     * Define el valor de la propiedad followUpInvoice.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemFollowUpInvoice }
     *     
     */
    public void setFollowUpInvoice(PurchaseOrderByIDResponseItemFollowUpInvoice value) {
        this.followUpInvoice = value;
    }

    /**
     * Obtiene el valor de la propiedad directMaterialIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectMaterialIndicator() {
        return directMaterialIndicator;
    }

    /**
     * Define el valor de la propiedad directMaterialIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectMaterialIndicator(Boolean value) {
        this.directMaterialIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad thirdPartyDealIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isThirdPartyDealIndicator() {
        return thirdPartyDealIndicator;
    }

    /**
     * Define el valor de la propiedad thirdPartyDealIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThirdPartyDealIndicator(Boolean value) {
        this.thirdPartyDealIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad itemProduct.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemProduct }
     *     
     */
    public PurchaseOrderByIDResponseItemProduct getItemProduct() {
        return itemProduct;
    }

    /**
     * Define el valor de la propiedad itemProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemProduct }
     *     
     */
    public void setItemProduct(PurchaseOrderByIDResponseItemProduct value) {
        this.itemProduct = value;
    }

    /**
     * Obtiene el valor de la propiedad itemDeliveryTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemDeliveryTerms }
     *     
     */
    public PurchaseOrderByIDResponseItemDeliveryTerms getItemDeliveryTerms() {
        return itemDeliveryTerms;
    }

    /**
     * Define el valor de la propiedad itemDeliveryTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemDeliveryTerms }
     *     
     */
    public void setItemDeliveryTerms(PurchaseOrderByIDResponseItemDeliveryTerms value) {
        this.itemDeliveryTerms = value;
    }

    /**
     * Obtiene el valor de la propiedad endBuyerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemEndBuyerParty }
     *     
     */
    public PurchaseOrderByIDResponseItemEndBuyerParty getEndBuyerParty() {
        return endBuyerParty;
    }

    /**
     * Define el valor de la propiedad endBuyerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemEndBuyerParty }
     *     
     */
    public void setEndBuyerParty(PurchaseOrderByIDResponseItemEndBuyerParty value) {
        this.endBuyerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad productRecipientParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemProductReceipientParty }
     *     
     */
    public PurchaseOrderByIDResponseItemProductReceipientParty getProductRecipientParty() {
        return productRecipientParty;
    }

    /**
     * Define el valor de la propiedad productRecipientParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemProductReceipientParty }
     *     
     */
    public void setProductRecipientParty(PurchaseOrderByIDResponseItemProductReceipientParty value) {
        this.productRecipientParty = value;
    }

    /**
     * Obtiene el valor de la propiedad servicePerformerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDResponseItemServicePerfomerParty }
     *     
     */
    public PurchaseOrderByIDResponseItemServicePerfomerParty getServicePerformerParty() {
        return servicePerformerParty;
    }

    /**
     * Define el valor de la propiedad servicePerformerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDResponseItemServicePerfomerParty }
     *     
     */
    public void setServicePerformerParty(PurchaseOrderByIDResponseItemServicePerfomerParty value) {
        this.servicePerformerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad itemAccountingCodingBlockDistribution.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public MaintenanceAccountingCodingBlockDistribution getItemAccountingCodingBlockDistribution() {
        return itemAccountingCodingBlockDistribution;
    }

    /**
     * Define el valor de la propiedad itemAccountingCodingBlockDistribution.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public void setItemAccountingCodingBlockDistribution(MaintenanceAccountingCodingBlockDistribution value) {
        this.itemAccountingCodingBlockDistribution = value;
    }

    /**
     * Obtiene el valor de la propiedad itemAttachmentFolder.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getItemAttachmentFolder() {
        return itemAttachmentFolder;
    }

    /**
     * Define el valor de la propiedad itemAttachmentFolder.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setItemAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.itemAttachmentFolder = value;
    }

    /**
     * Obtiene el valor de la propiedad itemTextCollection.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public MaintenanceTextCollection getItemTextCollection() {
        return itemTextCollection;
    }

    /**
     * Define el valor de la propiedad itemTextCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public void setItemTextCollection(MaintenanceTextCollection value) {
        this.itemTextCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad itemImatListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemImatListCompleteTransmissionIndicator() {
        return itemImatListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemImatListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemImatListCompleteTransmissionIndicator(Boolean value) {
        this.itemImatListCompleteTransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad itemPriceCalculation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderReadPriceCalculationItem }
     *     
     */
    public PurchaseOrderReadPriceCalculationItem getItemPriceCalculation() {
        return itemPriceCalculation;
    }

    /**
     * Define el valor de la propiedad itemPriceCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderReadPriceCalculationItem }
     *     
     */
    public void setItemPriceCalculation(PurchaseOrderReadPriceCalculationItem value) {
        this.itemPriceCalculation = value;
    }

    /**
     * Obtiene el valor de la propiedad itemTaxCalculation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderReadTaxCalculationItem }
     *     
     */
    public PurchaseOrderReadTaxCalculationItem getItemTaxCalculation() {
        return itemTaxCalculation;
    }

    /**
     * Define el valor de la propiedad itemTaxCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderReadTaxCalculationItem }
     *     
     */
    public void setItemTaxCalculation(PurchaseOrderReadTaxCalculationItem value) {
        this.itemTaxCalculation = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToLocationID.
     * 
     * @return
     *     possible object is
     *     {@link LocationID }
     *     
     */
    public LocationID getShipToLocationID() {
        return shipToLocationID;
    }

    /**
     * Define el valor de la propiedad shipToLocationID.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationID }
     *     
     */
    public void setShipToLocationID(LocationID value) {
        this.shipToLocationID = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToLocationAddressHostUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getShipToLocationAddressHostUUID() {
        return shipToLocationAddressHostUUID;
    }

    /**
     * Define el valor de la propiedad shipToLocationAddressHostUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setShipToLocationAddressHostUUID(UUID value) {
        this.shipToLocationAddressHostUUID = value;
    }

    /**
     * Gets the value of the itemScheduleLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemScheduleLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemScheduleLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItemScheduleLine }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItemScheduleLine> getItemScheduleLine() {
        if (itemScheduleLine == null) {
            itemScheduleLine = new ArrayList<PurchaseOrderMaintainRequestBundleItemScheduleLine>();
        }
        return this.itemScheduleLine;
    }

    /**
     * Obtiene el valor de la propiedad ubicacinOrigenS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinOrigenS() {
        return ubicacinOrigenS;
    }

    /**
     * Define el valor de la propiedad ubicacinOrigenS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinOrigenS(String value) {
        this.ubicacinOrigenS = value;
    }

    /**
     * Obtiene el valor de la propiedad unidaddeventaS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnidaddeventaS() {
        return unidaddeventaS;
    }

    /**
     * Define el valor de la propiedad unidaddeventaS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnidaddeventaS(String value) {
        this.unidaddeventaS = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacinDestinoS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinDestinoS() {
        return ubicacinDestinoS;
    }

    /**
     * Define el valor de la propiedad ubicacinDestinoS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinDestinoS(String value) {
        this.ubicacinDestinoS = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacinCruceS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUbicacinCruceS() {
        return ubicacinCruceS;
    }

    /**
     * Define el valor de la propiedad ubicacinCruceS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUbicacinCruceS(String value) {
        this.ubicacinCruceS = value;
    }

    /**
     * Obtiene el valor de la propiedad tipodeequipoS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipodeequipoS() {
        return tipodeequipoS;
    }

    /**
     * Define el valor de la propiedad tipodeequipoS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipodeequipoS(String value) {
        this.tipodeequipoS = value;
    }

    /**
     * Obtiene el valor de la propiedad shipperS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipperS() {
        return shipperS;
    }

    /**
     * Define el valor de la propiedad shipperS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipperS(String value) {
        this.shipperS = value;
    }

    /**
     * Obtiene el valor de la propiedad requierePODS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequierePODS() {
        return requierePODS;
    }

    /**
     * Define el valor de la propiedad requierePODS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequierePODS(String value) {
        this.requierePODS = value;
    }

    /**
     * Obtiene el valor de la propiedad pzsPBPCS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPzsPBPCS() {
        return pzsPBPCS;
    }

    /**
     * Define el valor de la propiedad pzsPBPCS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPzsPBPCS(String value) {
        this.pzsPBPCS = value;
    }

    /**
     * Obtiene el valor de la propiedad nmerodeequipodecargaS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmerodeequipodecargaS() {
        return nmerodeequipodecargaS;
    }

    /**
     * Define el valor de la propiedad nmerodeequipodecargaS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmerodeequipodecargaS(String value) {
        this.nmerodeequipodecargaS = value;
    }

    /**
     * Obtiene el valor de la propiedad ndeservicio2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNdeservicio2() {
        return ndeservicio2;
    }

    /**
     * Define el valor de la propiedad ndeservicio2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNdeservicio2(String value) {
        this.ndeservicio2 = value;
    }

    /**
     * Obtiene el valor de la propiedad guaMasterS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaMasterS() {
        return guaMasterS;
    }

    /**
     * Define el valor de la propiedad guaMasterS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaMasterS(String value) {
        this.guaMasterS = value;
    }

    /**
     * Obtiene el valor de la propiedad guaHouseS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuaHouseS() {
        return guaHouseS;
    }

    /**
     * Define el valor de la propiedad guaHouseS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuaHouseS(String value) {
        this.guaHouseS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechadeAgenteAduanalS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechadeAgenteAduanalS() {
        return fechadeAgenteAduanalS;
    }

    /**
     * Define el valor de la propiedad fechadeAgenteAduanalS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechadeAgenteAduanalS(XMLGregorianCalendar value) {
        this.fechadeAgenteAduanalS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaDestinoS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaDestinoS() {
        return fechaDestinoS;
    }

    /**
     * Define el valor de la propiedad fechaDestinoS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaDestinoS(XMLGregorianCalendar value) {
        this.fechaDestinoS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCruceS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCruceS() {
        return fechaCruceS;
    }

    /**
     * Define el valor de la propiedad fechaCruceS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCruceS(XMLGregorianCalendar value) {
        this.fechaCruceS = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCargaS.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCargaS() {
        return fechaCargaS;
    }

    /**
     * Define el valor de la propiedad fechaCargaS.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCargaS(XMLGregorianCalendar value) {
        this.fechaCargaS = value;
    }

    /**
     * Obtiene el valor de la propiedad consignatariosS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignatariosS() {
        return consignatariosS;
    }

    /**
     * Define el valor de la propiedad consignatariosS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignatariosS(String value) {
        this.consignatariosS = value;
    }

    /**
     * Obtiene el valor de la propiedad hsnCodeIndiaCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public ProductTaxStandardClassificationCode getHSNCodeIndiaCode() {
        return hsnCodeIndiaCode;
    }

    /**
     * Define el valor de la propiedad hsnCodeIndiaCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public void setHSNCodeIndiaCode(ProductTaxStandardClassificationCode value) {
        this.hsnCodeIndiaCode = value;
    }

}
