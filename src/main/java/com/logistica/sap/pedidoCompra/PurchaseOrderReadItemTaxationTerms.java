
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para PurchaseOrderReadItemTaxationTerms complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderReadItemTaxationTerms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="SellerCountryCode" type="{http://sap.com/xi/AP/Common/GDT}CountryCode" minOccurs="0"/>
 *         &lt;element name="SellerTaxID" type="{http://sap.com/xi/AP/Common/GDT}PartyTaxID" minOccurs="0"/>
 *         &lt;element name="SellerTaxIdentificationNumberTypeCode" type="{http://sap.com/xi/AP/Common/GDT}TaxIdentificationNumberTypeCode" minOccurs="0"/>
 *         &lt;element name="BuyerCountryCode" type="{http://sap.com/xi/AP/Common/GDT}CountryCode" minOccurs="0"/>
 *         &lt;element name="BuyerTaxID" type="{http://sap.com/xi/AP/Common/GDT}PartyTaxID" minOccurs="0"/>
 *         &lt;element name="BuyerTaxIdentificationNumberTypeCode" type="{http://sap.com/xi/AP/Common/GDT}TaxIdentificationNumberTypeCode" minOccurs="0"/>
 *         &lt;element name="EuropeanCommunityVATTriangulationIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="TaxDate" type="{http://sap.com/xi/AP/Common/GDT}Date" minOccurs="0"/>
 *         &lt;element name="TaxDueDate" type="{http://sap.com/xi/AP/Common/GDT}Date" minOccurs="0"/>
 *         &lt;element name="TaxExemptionCertificateID" type="{http://sap.com/xi/AP/Common/GDT}TaxExemptionCertificateID" minOccurs="0"/>
 *         &lt;element name="TaxExemptionReasonCode" type="{http://sap.com/xi/AP/Common/GDT}TaxExemptionReasonCode" minOccurs="0"/>
 *         &lt;element name="TaxExemptionReasonCodeRelevanceIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FollowUpTaxExemptionCertificateID" type="{http://sap.com/xi/AP/Common/GDT}TaxExemptionCertificateID" minOccurs="0"/>
 *         &lt;element name="ProductTaxStandardClassificationCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTaxStandardClassificationCode" minOccurs="0"/>
 *         &lt;element name="ProductTaxStandardClassificationSystemCode" type="{http://sap.com/xi/AP/Common/GDT}ProductTaxStandardClassificationSystemCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderReadItemTaxationTerms", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "uuid",
    "sellerCountryCode",
    "sellerTaxID",
    "sellerTaxIdentificationNumberTypeCode",
    "buyerCountryCode",
    "buyerTaxID",
    "buyerTaxIdentificationNumberTypeCode",
    "europeanCommunityVATTriangulationIndicator",
    "taxDate",
    "taxDueDate",
    "taxExemptionCertificateID",
    "taxExemptionReasonCode",
    "taxExemptionReasonCodeRelevanceIndicator",
    "followUpTaxExemptionCertificateID",
    "productTaxStandardClassificationCode",
    "productTaxStandardClassificationSystemCode"
})
public class PurchaseOrderReadItemTaxationTerms {

    @XmlElement(name = "UUID")
    protected UUID uuid;
    @XmlElement(name = "SellerCountryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sellerCountryCode;
    @XmlElement(name = "SellerTaxID")
    protected PartyTaxID sellerTaxID;
    @XmlElement(name = "SellerTaxIdentificationNumberTypeCode")
    protected TaxIdentificationNumberTypeCode sellerTaxIdentificationNumberTypeCode;
    @XmlElement(name = "BuyerCountryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String buyerCountryCode;
    @XmlElement(name = "BuyerTaxID")
    protected PartyTaxID buyerTaxID;
    @XmlElement(name = "BuyerTaxIdentificationNumberTypeCode")
    protected TaxIdentificationNumberTypeCode buyerTaxIdentificationNumberTypeCode;
    @XmlElement(name = "EuropeanCommunityVATTriangulationIndicator")
    protected Boolean europeanCommunityVATTriangulationIndicator;
    @XmlElement(name = "TaxDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar taxDate;
    @XmlElement(name = "TaxDueDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar taxDueDate;
    @XmlElement(name = "TaxExemptionCertificateID")
    protected TaxExemptionCertificateID taxExemptionCertificateID;
    @XmlElement(name = "TaxExemptionReasonCode")
    protected TaxExemptionReasonCode taxExemptionReasonCode;
    @XmlElement(name = "TaxExemptionReasonCodeRelevanceIndicator")
    protected Boolean taxExemptionReasonCodeRelevanceIndicator;
    @XmlElement(name = "FollowUpTaxExemptionCertificateID")
    protected TaxExemptionCertificateID followUpTaxExemptionCertificateID;
    @XmlElement(name = "ProductTaxStandardClassificationCode")
    protected ProductTaxStandardClassificationCode productTaxStandardClassificationCode;
    @XmlElement(name = "ProductTaxStandardClassificationSystemCode")
    protected ProductTaxStandardClassificationSystemCode productTaxStandardClassificationSystemCode;

    /**
     * Obtiene el valor de la propiedad uuid.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Define el valor de la propiedad uuid.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setUUID(UUID value) {
        this.uuid = value;
    }

    /**
     * Obtiene el valor de la propiedad sellerCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerCountryCode() {
        return sellerCountryCode;
    }

    /**
     * Define el valor de la propiedad sellerCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerCountryCode(String value) {
        this.sellerCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad sellerTaxID.
     * 
     * @return
     *     possible object is
     *     {@link PartyTaxID }
     *     
     */
    public PartyTaxID getSellerTaxID() {
        return sellerTaxID;
    }

    /**
     * Define el valor de la propiedad sellerTaxID.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyTaxID }
     *     
     */
    public void setSellerTaxID(PartyTaxID value) {
        this.sellerTaxID = value;
    }

    /**
     * Obtiene el valor de la propiedad sellerTaxIdentificationNumberTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxIdentificationNumberTypeCode }
     *     
     */
    public TaxIdentificationNumberTypeCode getSellerTaxIdentificationNumberTypeCode() {
        return sellerTaxIdentificationNumberTypeCode;
    }

    /**
     * Define el valor de la propiedad sellerTaxIdentificationNumberTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxIdentificationNumberTypeCode }
     *     
     */
    public void setSellerTaxIdentificationNumberTypeCode(TaxIdentificationNumberTypeCode value) {
        this.sellerTaxIdentificationNumberTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad buyerCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerCountryCode() {
        return buyerCountryCode;
    }

    /**
     * Define el valor de la propiedad buyerCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerCountryCode(String value) {
        this.buyerCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad buyerTaxID.
     * 
     * @return
     *     possible object is
     *     {@link PartyTaxID }
     *     
     */
    public PartyTaxID getBuyerTaxID() {
        return buyerTaxID;
    }

    /**
     * Define el valor de la propiedad buyerTaxID.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyTaxID }
     *     
     */
    public void setBuyerTaxID(PartyTaxID value) {
        this.buyerTaxID = value;
    }

    /**
     * Obtiene el valor de la propiedad buyerTaxIdentificationNumberTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxIdentificationNumberTypeCode }
     *     
     */
    public TaxIdentificationNumberTypeCode getBuyerTaxIdentificationNumberTypeCode() {
        return buyerTaxIdentificationNumberTypeCode;
    }

    /**
     * Define el valor de la propiedad buyerTaxIdentificationNumberTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxIdentificationNumberTypeCode }
     *     
     */
    public void setBuyerTaxIdentificationNumberTypeCode(TaxIdentificationNumberTypeCode value) {
        this.buyerTaxIdentificationNumberTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad europeanCommunityVATTriangulationIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEuropeanCommunityVATTriangulationIndicator() {
        return europeanCommunityVATTriangulationIndicator;
    }

    /**
     * Define el valor de la propiedad europeanCommunityVATTriangulationIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEuropeanCommunityVATTriangulationIndicator(Boolean value) {
        this.europeanCommunityVATTriangulationIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad taxDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxDate() {
        return taxDate;
    }

    /**
     * Define el valor de la propiedad taxDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxDate(XMLGregorianCalendar value) {
        this.taxDate = value;
    }

    /**
     * Obtiene el valor de la propiedad taxDueDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxDueDate() {
        return taxDueDate;
    }

    /**
     * Define el valor de la propiedad taxDueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxDueDate(XMLGregorianCalendar value) {
        this.taxDueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptionCertificateID.
     * 
     * @return
     *     possible object is
     *     {@link TaxExemptionCertificateID }
     *     
     */
    public TaxExemptionCertificateID getTaxExemptionCertificateID() {
        return taxExemptionCertificateID;
    }

    /**
     * Define el valor de la propiedad taxExemptionCertificateID.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxExemptionCertificateID }
     *     
     */
    public void setTaxExemptionCertificateID(TaxExemptionCertificateID value) {
        this.taxExemptionCertificateID = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptionReasonCode.
     * 
     * @return
     *     possible object is
     *     {@link TaxExemptionReasonCode }
     *     
     */
    public TaxExemptionReasonCode getTaxExemptionReasonCode() {
        return taxExemptionReasonCode;
    }

    /**
     * Define el valor de la propiedad taxExemptionReasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxExemptionReasonCode }
     *     
     */
    public void setTaxExemptionReasonCode(TaxExemptionReasonCode value) {
        this.taxExemptionReasonCode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxExemptionReasonCodeRelevanceIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxExemptionReasonCodeRelevanceIndicator() {
        return taxExemptionReasonCodeRelevanceIndicator;
    }

    /**
     * Define el valor de la propiedad taxExemptionReasonCodeRelevanceIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxExemptionReasonCodeRelevanceIndicator(Boolean value) {
        this.taxExemptionReasonCodeRelevanceIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpTaxExemptionCertificateID.
     * 
     * @return
     *     possible object is
     *     {@link TaxExemptionCertificateID }
     *     
     */
    public TaxExemptionCertificateID getFollowUpTaxExemptionCertificateID() {
        return followUpTaxExemptionCertificateID;
    }

    /**
     * Define el valor de la propiedad followUpTaxExemptionCertificateID.
     * 
     * @param value
     *     allowed object is
     *     {@link TaxExemptionCertificateID }
     *     
     */
    public void setFollowUpTaxExemptionCertificateID(TaxExemptionCertificateID value) {
        this.followUpTaxExemptionCertificateID = value;
    }

    /**
     * Obtiene el valor de la propiedad productTaxStandardClassificationCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public ProductTaxStandardClassificationCode getProductTaxStandardClassificationCode() {
        return productTaxStandardClassificationCode;
    }

    /**
     * Define el valor de la propiedad productTaxStandardClassificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public void setProductTaxStandardClassificationCode(ProductTaxStandardClassificationCode value) {
        this.productTaxStandardClassificationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad productTaxStandardClassificationSystemCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxStandardClassificationSystemCode }
     *     
     */
    public ProductTaxStandardClassificationSystemCode getProductTaxStandardClassificationSystemCode() {
        return productTaxStandardClassificationSystemCode;
    }

    /**
     * Define el valor de la propiedad productTaxStandardClassificationSystemCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxStandardClassificationSystemCode }
     *     
     */
    public void setProductTaxStandardClassificationSystemCode(ProductTaxStandardClassificationSystemCode value) {
        this.productTaxStandardClassificationSystemCode = value;
    }

}
