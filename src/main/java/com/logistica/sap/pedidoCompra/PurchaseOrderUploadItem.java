
package com.logistica.sap.pedidoCompra;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para PurchaseOrderUploadItem complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderUploadItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ObjectNodeSenderTechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodePartyTechnicalID" minOccurs="0"/>
 *         &lt;element name="ItemID" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemID" minOccurs="0"/>
 *         &lt;element name="CancelItemPurchaseOrderActionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="BusinessTransactionDocumentItemTypeCode" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentItemTypeCode" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="QuantityTypeCode" type="{http://sap.com/xi/AP/Common/GDT}QuantityTypeCode" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="NetAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="NetUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="GrossAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="GrossUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="ListUnitPrice" type="{http://sap.com/xi/AP/Common/GDT}Price" minOccurs="0"/>
 *         &lt;element name="LimitAmount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="DeliveryPeriod" type="{http://sap.com/xi/AP/Common/GDT}UPPEROPEN_LOCALNORMALISED_DateTimePeriod" minOccurs="0"/>
 *         &lt;element name="DirectMaterialIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="ThirdPartyDealIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="FollowUpPurchaseOrderConfirmation" type="{http://sap.com/xi/AP/XU/SRM/Global}ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload" minOccurs="0"/>
 *         &lt;element name="FollowUpDelivery" type="{http://sap.com/xi/AP/XU/SRM/Global}ProcurementDocumentItemFollowUpDeliveryForExcelUpload" minOccurs="0"/>
 *         &lt;element name="FollowUpInvoice" type="{http://sap.com/xi/AP/XU/SRM/Global}ProcurementDocumentItemFollowUpInvoiceForExcelUpload" minOccurs="0"/>
 *         &lt;element name="ItemDeliveryTerms" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemDeliveryTerms" minOccurs="0"/>
 *         &lt;element name="ItemProduct" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemProduct" minOccurs="0"/>
 *         &lt;element name="ItemIndividualMaterial" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemIndividualMaterial" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipToLocation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemLocation" minOccurs="0"/>
 *         &lt;element name="ReceivingItemSite" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemLocation" minOccurs="0"/>
 *         &lt;element name="EndBuyerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ProductRecipientParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ServicePerformerParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="RequestorParty" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemParty" minOccurs="0"/>
 *         &lt;element name="ItemAccountingCodingBlockDistribution" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}MaintenanceAccountingCodingBlockDistribution" minOccurs="0"/>
 *         &lt;element name="ItemAttachmentFolder" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceAttachmentFolder" minOccurs="0"/>
 *         &lt;element name="ItemTextCollection" type="{http://sap.com/xi/DocumentServices/Global}MaintenanceTextCollection" minOccurs="0"/>
 *         &lt;element name="ItemPurchasingContractReference" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference" minOccurs="0"/>
 *         &lt;element name="ItemPriceCalculation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestPriceCalculationItem" minOccurs="0"/>
 *         &lt;element name="ItemTaxCalculation" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestTaxCalculationItem" minOccurs="0"/>
 *         &lt;element name="DeliveryStartDate" type="{http://sap.com/xi/A1S}General_StringForExcelUpload" minOccurs="0"/>
 *         &lt;element name="DeliveryStartTimeZone" type="{http://sap.com/xi/BASIS/Global}TimeZoneCode" minOccurs="0"/>
 *         &lt;element name="DeliveryEndDate" type="{http://sap.com/xi/A1S}General_StringForExcelUpload" minOccurs="0"/>
 *         &lt;element name="DeliveryEndTimeZone" type="{http://sap.com/xi/BASIS/Global}TimeZoneCode" minOccurs="0"/>
 *         &lt;element name="ItemPlannedLandedCost" type="{http://sap.com/xi/A1S/Global}PurchaseOrderMaintainRequestBundleItemLandedCosts" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;group ref="{http://sap.com/xi/A1S/Global}PurchaseOrderUploadItemGloExtension"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ItemImatListCompleteTransmissionIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" />
 *       &lt;attribute name="actionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderUploadItem", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "objectNodeSenderTechnicalID",
    "itemID",
    "cancelItemPurchaseOrderActionIndicator",
    "businessTransactionDocumentItemTypeCode",
    "quantity",
    "quantityTypeCode",
    "description",
    "netAmount",
    "netUnitPrice",
    "grossAmount",
    "grossUnitPrice",
    "listUnitPrice",
    "limitAmount",
    "deliveryPeriod",
    "directMaterialIndicator",
    "thirdPartyDealIndicator",
    "followUpPurchaseOrderConfirmation",
    "followUpDelivery",
    "followUpInvoice",
    "itemDeliveryTerms",
    "itemProduct",
    "itemIndividualMaterial",
    "shipToLocation",
    "receivingItemSite",
    "endBuyerParty",
    "productRecipientParty",
    "servicePerformerParty",
    "requestorParty",
    "itemAccountingCodingBlockDistribution",
    "itemAttachmentFolder",
    "itemTextCollection",
    "itemPurchasingContractReference",
    "itemPriceCalculation",
    "itemTaxCalculation",
    "deliveryStartDate",
    "deliveryStartTimeZone",
    "deliveryEndDate",
    "deliveryEndTimeZone",
    "itemPlannedLandedCost",
    "hsnCodeIndiaCode"
})
public class PurchaseOrderUploadItem {

    @XmlElement(name = "ObjectNodeSenderTechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String objectNodeSenderTechnicalID;
    @XmlElement(name = "ItemID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String itemID;
    @XmlElement(name = "CancelItemPurchaseOrderActionIndicator")
    protected Boolean cancelItemPurchaseOrderActionIndicator;
    @XmlElement(name = "BusinessTransactionDocumentItemTypeCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessTransactionDocumentItemTypeCode;
    @XmlElement(name = "Quantity")
    protected Quantity quantity;
    @XmlElement(name = "QuantityTypeCode")
    protected QuantityTypeCode quantityTypeCode;
    @XmlElement(name = "Description")
    protected SHORTDescription description;
    @XmlElement(name = "NetAmount")
    protected Amount netAmount;
    @XmlElement(name = "NetUnitPrice")
    protected Price netUnitPrice;
    @XmlElement(name = "GrossAmount")
    protected Amount grossAmount;
    @XmlElement(name = "GrossUnitPrice")
    protected Price grossUnitPrice;
    @XmlElement(name = "ListUnitPrice")
    protected Price listUnitPrice;
    @XmlElement(name = "LimitAmount")
    protected Amount limitAmount;
    @XmlElement(name = "DeliveryPeriod")
    protected UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod;
    @XmlElement(name = "DirectMaterialIndicator")
    protected Boolean directMaterialIndicator;
    @XmlElement(name = "ThirdPartyDealIndicator")
    protected Boolean thirdPartyDealIndicator;
    @XmlElement(name = "FollowUpPurchaseOrderConfirmation")
    protected ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload followUpPurchaseOrderConfirmation;
    @XmlElement(name = "FollowUpDelivery")
    protected ProcurementDocumentItemFollowUpDeliveryForExcelUpload followUpDelivery;
    @XmlElement(name = "FollowUpInvoice")
    protected ProcurementDocumentItemFollowUpInvoiceForExcelUpload followUpInvoice;
    @XmlElement(name = "ItemDeliveryTerms")
    protected PurchaseOrderMaintainRequestBundleItemDeliveryTerms itemDeliveryTerms;
    @XmlElement(name = "ItemProduct")
    protected PurchaseOrderMaintainRequestBundleItemProduct itemProduct;
    @XmlElement(name = "ItemIndividualMaterial")
    protected List<PurchaseOrderMaintainRequestBundleItemIndividualMaterial> itemIndividualMaterial;
    @XmlElement(name = "ShipToLocation")
    protected PurchaseOrderMaintainRequestBundleItemLocation shipToLocation;
    @XmlElement(name = "ReceivingItemSite")
    protected PurchaseOrderMaintainRequestBundleItemLocation receivingItemSite;
    @XmlElement(name = "EndBuyerParty")
    protected PurchaseOrderMaintainRequestBundleItemParty endBuyerParty;
    @XmlElement(name = "ProductRecipientParty")
    protected PurchaseOrderMaintainRequestBundleItemParty productRecipientParty;
    @XmlElement(name = "ServicePerformerParty")
    protected PurchaseOrderMaintainRequestBundleItemParty servicePerformerParty;
    @XmlElement(name = "RequestorParty")
    protected PurchaseOrderMaintainRequestBundleItemParty requestorParty;
    @XmlElement(name = "ItemAccountingCodingBlockDistribution")
    protected MaintenanceAccountingCodingBlockDistribution itemAccountingCodingBlockDistribution;
    @XmlElement(name = "ItemAttachmentFolder")
    protected MaintenanceAttachmentFolder itemAttachmentFolder;
    @XmlElement(name = "ItemTextCollection")
    protected MaintenanceTextCollection itemTextCollection;
    @XmlElement(name = "ItemPurchasingContractReference")
    protected PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference itemPurchasingContractReference;
    @XmlElement(name = "ItemPriceCalculation")
    protected PurchaseOrderMaintainRequestPriceCalculationItem itemPriceCalculation;
    @XmlElement(name = "ItemTaxCalculation")
    protected PurchaseOrderMaintainRequestTaxCalculationItem itemTaxCalculation;
    @XmlElement(name = "DeliveryStartDate")
    protected String deliveryStartDate;
    @XmlElement(name = "DeliveryStartTimeZone")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryStartTimeZone;
    @XmlElement(name = "DeliveryEndDate")
    protected String deliveryEndDate;
    @XmlElement(name = "DeliveryEndTimeZone")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String deliveryEndTimeZone;
    @XmlElement(name = "ItemPlannedLandedCost")
    protected List<PurchaseOrderMaintainRequestBundleItemLandedCosts> itemPlannedLandedCost;
    @XmlElement(name = "HSNCodeIndiaCode", namespace = "http://sap.com/xi/A1S/Global")
    protected ProductTaxStandardClassificationCode hsnCodeIndiaCode;
    @XmlAttribute(name = "ItemImatListCompleteTransmissionIndicator")
    protected Boolean itemImatListCompleteTransmissionIndicator;
    @XmlAttribute(name = "actionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Obtiene el valor de la propiedad objectNodeSenderTechnicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectNodeSenderTechnicalID() {
        return objectNodeSenderTechnicalID;
    }

    /**
     * Define el valor de la propiedad objectNodeSenderTechnicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectNodeSenderTechnicalID(String value) {
        this.objectNodeSenderTechnicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad itemID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemID() {
        return itemID;
    }

    /**
     * Define el valor de la propiedad itemID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemID(String value) {
        this.itemID = value;
    }

    /**
     * Obtiene el valor de la propiedad cancelItemPurchaseOrderActionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancelItemPurchaseOrderActionIndicator() {
        return cancelItemPurchaseOrderActionIndicator;
    }

    /**
     * Define el valor de la propiedad cancelItemPurchaseOrderActionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancelItemPurchaseOrderActionIndicator(Boolean value) {
        this.cancelItemPurchaseOrderActionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad businessTransactionDocumentItemTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessTransactionDocumentItemTypeCode() {
        return businessTransactionDocumentItemTypeCode;
    }

    /**
     * Define el valor de la propiedad businessTransactionDocumentItemTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessTransactionDocumentItemTypeCode(String value) {
        this.businessTransactionDocumentItemTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setQuantity(Quantity value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad quantityTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link QuantityTypeCode }
     *     
     */
    public QuantityTypeCode getQuantityTypeCode() {
        return quantityTypeCode;
    }

    /**
     * Define el valor de la propiedad quantityTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityTypeCode }
     *     
     */
    public void setQuantityTypeCode(QuantityTypeCode value) {
        this.quantityTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setDescription(SHORTDescription value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad netAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getNetAmount() {
        return netAmount;
    }

    /**
     * Define el valor de la propiedad netAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setNetAmount(Amount value) {
        this.netAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad netUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getNetUnitPrice() {
        return netUnitPrice;
    }

    /**
     * Define el valor de la propiedad netUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setNetUnitPrice(Price value) {
        this.netUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad grossAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getGrossAmount() {
        return grossAmount;
    }

    /**
     * Define el valor de la propiedad grossAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setGrossAmount(Amount value) {
        this.grossAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad grossUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getGrossUnitPrice() {
        return grossUnitPrice;
    }

    /**
     * Define el valor de la propiedad grossUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setGrossUnitPrice(Price value) {
        this.grossUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad listUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getListUnitPrice() {
        return listUnitPrice;
    }

    /**
     * Define el valor de la propiedad listUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setListUnitPrice(Price value) {
        this.listUnitPrice = value;
    }

    /**
     * Obtiene el valor de la propiedad limitAmount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getLimitAmount() {
        return limitAmount;
    }

    /**
     * Define el valor de la propiedad limitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setLimitAmount(Amount value) {
        this.limitAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryPeriod.
     * 
     * @return
     *     possible object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod getDeliveryPeriod() {
        return deliveryPeriod;
    }

    /**
     * Define el valor de la propiedad deliveryPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     *     
     */
    public void setDeliveryPeriod(UPPEROPENLOCALNORMALISEDDateTimePeriod value) {
        this.deliveryPeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad directMaterialIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDirectMaterialIndicator() {
        return directMaterialIndicator;
    }

    /**
     * Define el valor de la propiedad directMaterialIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDirectMaterialIndicator(Boolean value) {
        this.directMaterialIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad thirdPartyDealIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isThirdPartyDealIndicator() {
        return thirdPartyDealIndicator;
    }

    /**
     * Define el valor de la propiedad thirdPartyDealIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setThirdPartyDealIndicator(Boolean value) {
        this.thirdPartyDealIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpPurchaseOrderConfirmation.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload }
     *     
     */
    public ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload getFollowUpPurchaseOrderConfirmation() {
        return followUpPurchaseOrderConfirmation;
    }

    /**
     * Define el valor de la propiedad followUpPurchaseOrderConfirmation.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload }
     *     
     */
    public void setFollowUpPurchaseOrderConfirmation(ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload value) {
        this.followUpPurchaseOrderConfirmation = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpDelivery.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentItemFollowUpDeliveryForExcelUpload }
     *     
     */
    public ProcurementDocumentItemFollowUpDeliveryForExcelUpload getFollowUpDelivery() {
        return followUpDelivery;
    }

    /**
     * Define el valor de la propiedad followUpDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentItemFollowUpDeliveryForExcelUpload }
     *     
     */
    public void setFollowUpDelivery(ProcurementDocumentItemFollowUpDeliveryForExcelUpload value) {
        this.followUpDelivery = value;
    }

    /**
     * Obtiene el valor de la propiedad followUpInvoice.
     * 
     * @return
     *     possible object is
     *     {@link ProcurementDocumentItemFollowUpInvoiceForExcelUpload }
     *     
     */
    public ProcurementDocumentItemFollowUpInvoiceForExcelUpload getFollowUpInvoice() {
        return followUpInvoice;
    }

    /**
     * Define el valor de la propiedad followUpInvoice.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcurementDocumentItemFollowUpInvoiceForExcelUpload }
     *     
     */
    public void setFollowUpInvoice(ProcurementDocumentItemFollowUpInvoiceForExcelUpload value) {
        this.followUpInvoice = value;
    }

    /**
     * Obtiene el valor de la propiedad itemDeliveryTerms.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemDeliveryTerms }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemDeliveryTerms getItemDeliveryTerms() {
        return itemDeliveryTerms;
    }

    /**
     * Define el valor de la propiedad itemDeliveryTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemDeliveryTerms }
     *     
     */
    public void setItemDeliveryTerms(PurchaseOrderMaintainRequestBundleItemDeliveryTerms value) {
        this.itemDeliveryTerms = value;
    }

    /**
     * Obtiene el valor de la propiedad itemProduct.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemProduct }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemProduct getItemProduct() {
        return itemProduct;
    }

    /**
     * Define el valor de la propiedad itemProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemProduct }
     *     
     */
    public void setItemProduct(PurchaseOrderMaintainRequestBundleItemProduct value) {
        this.itemProduct = value;
    }

    /**
     * Gets the value of the itemIndividualMaterial property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIndividualMaterial property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIndividualMaterial().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItemIndividualMaterial }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItemIndividualMaterial> getItemIndividualMaterial() {
        if (itemIndividualMaterial == null) {
            itemIndividualMaterial = new ArrayList<PurchaseOrderMaintainRequestBundleItemIndividualMaterial>();
        }
        return this.itemIndividualMaterial;
    }

    /**
     * Obtiene el valor de la propiedad shipToLocation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemLocation getShipToLocation() {
        return shipToLocation;
    }

    /**
     * Define el valor de la propiedad shipToLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public void setShipToLocation(PurchaseOrderMaintainRequestBundleItemLocation value) {
        this.shipToLocation = value;
    }

    /**
     * Obtiene el valor de la propiedad receivingItemSite.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemLocation getReceivingItemSite() {
        return receivingItemSite;
    }

    /**
     * Define el valor de la propiedad receivingItemSite.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemLocation }
     *     
     */
    public void setReceivingItemSite(PurchaseOrderMaintainRequestBundleItemLocation value) {
        this.receivingItemSite = value;
    }

    /**
     * Obtiene el valor de la propiedad endBuyerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getEndBuyerParty() {
        return endBuyerParty;
    }

    /**
     * Define el valor de la propiedad endBuyerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setEndBuyerParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.endBuyerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad productRecipientParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getProductRecipientParty() {
        return productRecipientParty;
    }

    /**
     * Define el valor de la propiedad productRecipientParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setProductRecipientParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.productRecipientParty = value;
    }

    /**
     * Obtiene el valor de la propiedad servicePerformerParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getServicePerformerParty() {
        return servicePerformerParty;
    }

    /**
     * Define el valor de la propiedad servicePerformerParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setServicePerformerParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.servicePerformerParty = value;
    }

    /**
     * Obtiene el valor de la propiedad requestorParty.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemParty getRequestorParty() {
        return requestorParty;
    }

    /**
     * Define el valor de la propiedad requestorParty.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemParty }
     *     
     */
    public void setRequestorParty(PurchaseOrderMaintainRequestBundleItemParty value) {
        this.requestorParty = value;
    }

    /**
     * Obtiene el valor de la propiedad itemAccountingCodingBlockDistribution.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public MaintenanceAccountingCodingBlockDistribution getItemAccountingCodingBlockDistribution() {
        return itemAccountingCodingBlockDistribution;
    }

    /**
     * Define el valor de la propiedad itemAccountingCodingBlockDistribution.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAccountingCodingBlockDistribution }
     *     
     */
    public void setItemAccountingCodingBlockDistribution(MaintenanceAccountingCodingBlockDistribution value) {
        this.itemAccountingCodingBlockDistribution = value;
    }

    /**
     * Obtiene el valor de la propiedad itemAttachmentFolder.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public MaintenanceAttachmentFolder getItemAttachmentFolder() {
        return itemAttachmentFolder;
    }

    /**
     * Define el valor de la propiedad itemAttachmentFolder.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAttachmentFolder }
     *     
     */
    public void setItemAttachmentFolder(MaintenanceAttachmentFolder value) {
        this.itemAttachmentFolder = value;
    }

    /**
     * Obtiene el valor de la propiedad itemTextCollection.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public MaintenanceTextCollection getItemTextCollection() {
        return itemTextCollection;
    }

    /**
     * Define el valor de la propiedad itemTextCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceTextCollection }
     *     
     */
    public void setItemTextCollection(MaintenanceTextCollection value) {
        this.itemTextCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad itemPurchasingContractReference.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     *     
     */
    public PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference getItemPurchasingContractReference() {
        return itemPurchasingContractReference;
    }

    /**
     * Define el valor de la propiedad itemPurchasingContractReference.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     *     
     */
    public void setItemPurchasingContractReference(PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference value) {
        this.itemPurchasingContractReference = value;
    }

    /**
     * Obtiene el valor de la propiedad itemPriceCalculation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestPriceCalculationItem }
     *     
     */
    public PurchaseOrderMaintainRequestPriceCalculationItem getItemPriceCalculation() {
        return itemPriceCalculation;
    }

    /**
     * Define el valor de la propiedad itemPriceCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestPriceCalculationItem }
     *     
     */
    public void setItemPriceCalculation(PurchaseOrderMaintainRequestPriceCalculationItem value) {
        this.itemPriceCalculation = value;
    }

    /**
     * Obtiene el valor de la propiedad itemTaxCalculation.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderMaintainRequestTaxCalculationItem }
     *     
     */
    public PurchaseOrderMaintainRequestTaxCalculationItem getItemTaxCalculation() {
        return itemTaxCalculation;
    }

    /**
     * Define el valor de la propiedad itemTaxCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderMaintainRequestTaxCalculationItem }
     *     
     */
    public void setItemTaxCalculation(PurchaseOrderMaintainRequestTaxCalculationItem value) {
        this.itemTaxCalculation = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryStartDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryStartDate() {
        return deliveryStartDate;
    }

    /**
     * Define el valor de la propiedad deliveryStartDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryStartDate(String value) {
        this.deliveryStartDate = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryStartTimeZone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryStartTimeZone() {
        return deliveryStartTimeZone;
    }

    /**
     * Define el valor de la propiedad deliveryStartTimeZone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryStartTimeZone(String value) {
        this.deliveryStartTimeZone = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryEndDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryEndDate() {
        return deliveryEndDate;
    }

    /**
     * Define el valor de la propiedad deliveryEndDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryEndDate(String value) {
        this.deliveryEndDate = value;
    }

    /**
     * Obtiene el valor de la propiedad deliveryEndTimeZone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryEndTimeZone() {
        return deliveryEndTimeZone;
    }

    /**
     * Define el valor de la propiedad deliveryEndTimeZone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryEndTimeZone(String value) {
        this.deliveryEndTimeZone = value;
    }

    /**
     * Gets the value of the itemPlannedLandedCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemPlannedLandedCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemPlannedLandedCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PurchaseOrderMaintainRequestBundleItemLandedCosts }
     * 
     * 
     */
    public List<PurchaseOrderMaintainRequestBundleItemLandedCosts> getItemPlannedLandedCost() {
        if (itemPlannedLandedCost == null) {
            itemPlannedLandedCost = new ArrayList<PurchaseOrderMaintainRequestBundleItemLandedCosts>();
        }
        return this.itemPlannedLandedCost;
    }

    /**
     * Obtiene el valor de la propiedad hsnCodeIndiaCode.
     * 
     * @return
     *     possible object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public ProductTaxStandardClassificationCode getHSNCodeIndiaCode() {
        return hsnCodeIndiaCode;
    }

    /**
     * Define el valor de la propiedad hsnCodeIndiaCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductTaxStandardClassificationCode }
     *     
     */
    public void setHSNCodeIndiaCode(ProductTaxStandardClassificationCode value) {
        this.hsnCodeIndiaCode = value;
    }

    /**
     * Obtiene el valor de la propiedad itemImatListCompleteTransmissionIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isItemImatListCompleteTransmissionIndicator() {
        return itemImatListCompleteTransmissionIndicator;
    }

    /**
     * Define el valor de la propiedad itemImatListCompleteTransmissionIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setItemImatListCompleteTransmissionIndicator(Boolean value) {
        this.itemImatListCompleteTransmissionIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
