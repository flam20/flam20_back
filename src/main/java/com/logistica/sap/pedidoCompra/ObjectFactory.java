
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.logistica.integracionsap.ws.pedidoCompra package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PurchaseOrderUploadRequestSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderUploadRequest_sync");
    private final static QName _PurchaseOrderBundleMaintainConfirmationSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderBundleMaintainConfirmation_sync");
    private final static QName _PurchaseOrderByIDQuerySync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderByIDQuery_sync");
    private final static QName _PurchaseOrderBundleMaintainRequestSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderBundleMaintainRequest_sync");
    private final static QName _PurchaseOrderByIDResponseSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderByIDResponse_sync");
    private final static QName _PurchaseOrderBundleCheckMaintainRequestSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderBundleCheckMaintainRequest_sync");
    private final static QName _PurchaseOrderUploadConfirmationSync_QNAME = new QName("http://sap.com/xi/SAPGlobal20/Global", "PurchaseOrderUploadConfirmation_sync");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.logistica.integracionsap.ws.pedidoCompra
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemStatus }
     * 
     */
    public PurchaseOrderByIDResponseItemStatus createPurchaseOrderByIDResponseItemStatus() {
        return new PurchaseOrderByIDResponseItemStatus();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseDeliveryTerms }
     * 
     */
    public PurchaseOrderByIDResponseDeliveryTerms createPurchaseOrderByIDResponseDeliveryTerms() {
        return new PurchaseOrderByIDResponseDeliveryTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseStatus }
     * 
     */
    public PurchaseOrderByIDResponseStatus createPurchaseOrderByIDResponseStatus() {
        return new PurchaseOrderByIDResponseStatus();
    }

    /**
     * Create an instance of {@link ProcurementDocumentItemFollowUpInvoice }
     * 
     */
    public ProcurementDocumentItemFollowUpInvoice createProcurementDocumentItemFollowUpInvoice() {
        return new ProcurementDocumentItemFollowUpInvoice();
    }

    /**
     * Create an instance of {@link ProcurementDocumentItemFollowUpPurchaseOrderConfirmation }
     * 
     */
    public ProcurementDocumentItemFollowUpPurchaseOrderConfirmation createProcurementDocumentItemFollowUpPurchaseOrderConfirmation() {
        return new ProcurementDocumentItemFollowUpPurchaseOrderConfirmation();
    }

    /**
     * Create an instance of {@link ProcurementDocumentItemFollowUpDeliveryForExcelUpload }
     * 
     */
    public ProcurementDocumentItemFollowUpDeliveryForExcelUpload createProcurementDocumentItemFollowUpDeliveryForExcelUpload() {
        return new ProcurementDocumentItemFollowUpDeliveryForExcelUpload();
    }

    /**
     * Create an instance of {@link ProcurementDocumentItemFollowUpDelivery }
     * 
     */
    public ProcurementDocumentItemFollowUpDelivery createProcurementDocumentItemFollowUpDelivery() {
        return new ProcurementDocumentItemFollowUpDelivery();
    }

    /**
     * Create an instance of {@link ProcurementDocumentItemFollowUpInvoiceForExcelUpload }
     * 
     */
    public ProcurementDocumentItemFollowUpInvoiceForExcelUpload createProcurementDocumentItemFollowUpInvoiceForExcelUpload() {
        return new ProcurementDocumentItemFollowUpInvoiceForExcelUpload();
    }

    /**
     * Create an instance of {@link ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload }
     * 
     */
    public ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload createProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload() {
        return new ProcurementDocumentItemFollowUpPurchaseOrderConfirmationForExcelUpload();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocument }
     * 
     */
    public MaintenanceAttachmentFolderDocument createMaintenanceAttachmentFolderDocument() {
        return new MaintenanceAttachmentFolderDocument();
    }

    /**
     * Create an instance of {@link MaintenanceTextCollection }
     * 
     */
    public MaintenanceTextCollection createMaintenanceTextCollection() {
        return new MaintenanceTextCollection();
    }

    /**
     * Create an instance of {@link MaintenanceTextCollectionText }
     * 
     */
    public MaintenanceTextCollectionText createMaintenanceTextCollectionText() {
        return new MaintenanceTextCollectionText();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentFileContent }
     * 
     */
    public MaintenanceAttachmentFolderDocumentFileContent createMaintenanceAttachmentFolderDocumentFileContent() {
        return new MaintenanceAttachmentFolderDocumentFileContent();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentProperty }
     * 
     */
    public MaintenanceAttachmentFolderDocumentProperty createMaintenanceAttachmentFolderDocumentProperty() {
        return new MaintenanceAttachmentFolderDocumentProperty();
    }

    /**
     * Create an instance of {@link MaintenanceTextCollectionTextTextContent }
     * 
     */
    public MaintenanceTextCollectionTextTextContent createMaintenanceTextCollectionTextTextContent() {
        return new MaintenanceTextCollectionTextTextContent();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolder }
     * 
     */
    public MaintenanceAttachmentFolder createMaintenanceAttachmentFolder() {
        return new MaintenanceAttachmentFolder();
    }

    /**
     * Create an instance of {@link MaintenanceAttachmentFolderDocumentPropertyPropertyValue }
     * 
     */
    public MaintenanceAttachmentFolderDocumentPropertyPropertyValue createMaintenanceAttachmentFolderDocumentPropertyPropertyValue() {
        return new MaintenanceAttachmentFolderDocumentPropertyPropertyValue();
    }

    /**
     * Create an instance of {@link AccessCashDiscountTerms }
     * 
     */
    public AccessCashDiscountTerms createAccessCashDiscountTerms() {
        return new AccessCashDiscountTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintenanceCashDiscountTerms }
     * 
     */
    public PurchaseOrderMaintenanceCashDiscountTerms createPurchaseOrderMaintenanceCashDiscountTerms() {
        return new PurchaseOrderMaintenanceCashDiscountTerms();
    }

    /**
     * Create an instance of {@link BusinessDocumentBasicMessageHeader }
     * 
     */
    public BusinessDocumentBasicMessageHeader createBusinessDocumentBasicMessageHeader() {
        return new BusinessDocumentBasicMessageHeader();
    }

    /**
     * Create an instance of {@link WithholdingTax }
     * 
     */
    public WithholdingTax createWithholdingTax() {
        return new WithholdingTax();
    }

    /**
     * Create an instance of {@link IdentityID }
     * 
     */
    public IdentityID createIdentityID() {
        return new IdentityID();
    }

    /**
     * Create an instance of {@link OrganisationName }
     * 
     */
    public OrganisationName createOrganisationName() {
        return new OrganisationName();
    }

    /**
     * Create an instance of {@link MEDIUMName }
     * 
     */
    public MEDIUMName createMEDIUMName() {
        return new MEDIUMName();
    }

    /**
     * Create an instance of {@link SHORTDescription }
     * 
     */
    public SHORTDescription createSHORTDescription() {
        return new SHORTDescription();
    }

    /**
     * Create an instance of {@link PartyTaxID }
     * 
     */
    public PartyTaxID createPartyTaxID() {
        return new PartyTaxID();
    }

    /**
     * Create an instance of {@link WithholdingTaxEventTypeCode }
     * 
     */
    public WithholdingTaxEventTypeCode createWithholdingTaxEventTypeCode() {
        return new WithholdingTaxEventTypeCode();
    }

    /**
     * Create an instance of {@link ProductTaxStandardClassificationCode }
     * 
     */
    public ProductTaxStandardClassificationCode createProductTaxStandardClassificationCode() {
        return new ProductTaxStandardClassificationCode();
    }

    /**
     * Create an instance of {@link GeneralLedgerAccountAliasCodeContextElements }
     * 
     */
    public GeneralLedgerAccountAliasCodeContextElements createGeneralLedgerAccountAliasCodeContextElements() {
        return new GeneralLedgerAccountAliasCodeContextElements();
    }

    /**
     * Create an instance of {@link RequirementSpecificationID }
     * 
     */
    public RequirementSpecificationID createRequirementSpecificationID() {
        return new RequirementSpecificationID();
    }

    /**
     * Create an instance of {@link WithholdingTaxationCharacteristicsCode }
     * 
     */
    public WithholdingTaxationCharacteristicsCode createWithholdingTaxationCharacteristicsCode() {
        return new WithholdingTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link ProductTaxEventTypeCode }
     * 
     */
    public ProductTaxEventTypeCode createProductTaxEventTypeCode() {
        return new ProductTaxEventTypeCode();
    }

    /**
     * Create an instance of {@link SystemAdministrativeData }
     * 
     */
    public SystemAdministrativeData createSystemAdministrativeData() {
        return new SystemAdministrativeData();
    }

    /**
     * Create an instance of {@link PhoneNumber }
     * 
     */
    public PhoneNumber createPhoneNumber() {
        return new PhoneNumber();
    }

    /**
     * Create an instance of {@link TaxJurisdictionSubdivisionTypeCode }
     * 
     */
    public TaxJurisdictionSubdivisionTypeCode createTaxJurisdictionSubdivisionTypeCode() {
        return new TaxJurisdictionSubdivisionTypeCode();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentReference }
     * 
     */
    public BusinessTransactionDocumentReference createBusinessTransactionDocumentReference() {
        return new BusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link TaxTypeCode }
     * 
     */
    public TaxTypeCode createTaxTypeCode() {
        return new TaxTypeCode();
    }

    /**
     * Create an instance of {@link ObjectTypeCode }
     * 
     */
    public ObjectTypeCode createObjectTypeCode() {
        return new ObjectTypeCode();
    }

    /**
     * Create an instance of {@link ProductStandardID }
     * 
     */
    public ProductStandardID createProductStandardID() {
        return new ProductStandardID();
    }

    /**
     * Create an instance of {@link CashDiscountLevelCode }
     * 
     */
    public CashDiscountLevelCode createCashDiscountLevelCode() {
        return new CashDiscountLevelCode();
    }

    /**
     * Create an instance of {@link PartyAddressReference }
     * 
     */
    public PartyAddressReference createPartyAddressReference() {
        return new PartyAddressReference();
    }

    /**
     * Create an instance of {@link CashDiscount }
     * 
     */
    public CashDiscount createCashDiscount() {
        return new CashDiscount();
    }

    /**
     * Create an instance of {@link EXTENDEDName }
     * 
     */
    public EXTENDEDName createEXTENDEDName() {
        return new EXTENDEDName();
    }

    /**
     * Create an instance of {@link ProductCategoryHierarchyID }
     * 
     */
    public ProductCategoryHierarchyID createProductCategoryHierarchyID() {
        return new ProductCategoryHierarchyID();
    }

    /**
     * Create an instance of {@link ProductTaxationCharacteristicsCode }
     * 
     */
    public ProductTaxationCharacteristicsCode createProductTaxationCharacteristicsCode() {
        return new ProductTaxationCharacteristicsCode();
    }

    /**
     * Create an instance of {@link EmployeeID }
     * 
     */
    public EmployeeID createEmployeeID() {
        return new EmployeeID();
    }

    /**
     * Create an instance of {@link ExchangeRate }
     * 
     */
    public ExchangeRate createExchangeRate() {
        return new ExchangeRate();
    }

    /**
     * Create an instance of {@link TaxIdentificationNumberTypeCode }
     * 
     */
    public TaxIdentificationNumberTypeCode createTaxIdentificationNumberTypeCode() {
        return new TaxIdentificationNumberTypeCode();
    }

    /**
     * Create an instance of {@link QuantityTolerance }
     * 
     */
    public QuantityTolerance createQuantityTolerance() {
        return new QuantityTolerance();
    }

    /**
     * Create an instance of {@link DocumentTypeCode }
     * 
     */
    public DocumentTypeCode createDocumentTypeCode() {
        return new DocumentTypeCode();
    }

    /**
     * Create an instance of {@link ProductID }
     * 
     */
    public ProductID createProductID() {
        return new ProductID();
    }

    /**
     * Create an instance of {@link TaxExemption }
     * 
     */
    public TaxExemption createTaxExemption() {
        return new TaxExemption();
    }

    /**
     * Create an instance of {@link TaxExemptionReasonCode }
     * 
     */
    public TaxExemptionReasonCode createTaxExemptionReasonCode() {
        return new TaxExemptionReasonCode();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentTypeCode }
     * 
     */
    public BusinessTransactionDocumentTypeCode createBusinessTransactionDocumentTypeCode() {
        return new BusinessTransactionDocumentTypeCode();
    }

    /**
     * Create an instance of {@link WithholdingTaxIncomeTypeCode }
     * 
     */
    public WithholdingTaxIncomeTypeCode createWithholdingTaxIncomeTypeCode() {
        return new WithholdingTaxIncomeTypeCode();
    }

    /**
     * Create an instance of {@link Incoterms }
     * 
     */
    public Incoterms createIncoterms() {
        return new Incoterms();
    }

    /**
     * Create an instance of {@link TaxExemptionCertificateID }
     * 
     */
    public TaxExemptionCertificateID createTaxExemptionCertificateID() {
        return new TaxExemptionCertificateID();
    }

    /**
     * Create an instance of {@link RegionCode }
     * 
     */
    public RegionCode createRegionCode() {
        return new RegionCode();
    }

    /**
     * Create an instance of {@link BusinessDocumentMessageID }
     * 
     */
    public BusinessDocumentMessageID createBusinessDocumentMessageID() {
        return new BusinessDocumentMessageID();
    }

    /**
     * Create an instance of {@link LocationID }
     * 
     */
    public LocationID createLocationID() {
        return new LocationID();
    }

    /**
     * Create an instance of {@link ProductTax }
     * 
     */
    public ProductTax createProductTax() {
        return new ProductTax();
    }

    /**
     * Create an instance of {@link GeneralLedgerAccountAliasCode }
     * 
     */
    public GeneralLedgerAccountAliasCode createGeneralLedgerAccountAliasCode() {
        return new GeneralLedgerAccountAliasCode();
    }

    /**
     * Create an instance of {@link QuantityTypeCode }
     * 
     */
    public QuantityTypeCode createQuantityTypeCode() {
        return new QuantityTypeCode();
    }

    /**
     * Create an instance of {@link Log }
     * 
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link Rate }
     * 
     */
    public Rate createRate() {
        return new Rate();
    }

    /**
     * Create an instance of {@link NamespaceURI }
     * 
     */
    public NamespaceURI createNamespaceURI() {
        return new NamespaceURI();
    }

    /**
     * Create an instance of {@link TaxJurisdictionCode }
     * 
     */
    public TaxJurisdictionCode createTaxJurisdictionCode() {
        return new TaxJurisdictionCode();
    }

    /**
     * Create an instance of {@link AccountingCodingBlockTypeCode }
     * 
     */
    public AccountingCodingBlockTypeCode createAccountingCodingBlockTypeCode() {
        return new AccountingCodingBlockTypeCode();
    }

    /**
     * Create an instance of {@link Description }
     * 
     */
    public Description createDescription() {
        return new Description();
    }

    /**
     * Create an instance of {@link TaxDeductibilityCode }
     * 
     */
    public TaxDeductibilityCode createTaxDeductibilityCode() {
        return new TaxDeductibilityCode();
    }

    /**
     * Create an instance of {@link LogItemNotePlaceholderSubstitutionList }
     * 
     */
    public LogItemNotePlaceholderSubstitutionList createLogItemNotePlaceholderSubstitutionList() {
        return new LogItemNotePlaceholderSubstitutionList();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link EmailURI }
     * 
     */
    public EmailURI createEmailURI() {
        return new EmailURI();
    }

    /**
     * Create an instance of {@link Text }
     * 
     */
    public Text createText() {
        return new Text();
    }

    /**
     * Create an instance of {@link PriceSpecificationElementTypeCode }
     * 
     */
    public PriceSpecificationElementTypeCode createPriceSpecificationElementTypeCode() {
        return new PriceSpecificationElementTypeCode();
    }

    /**
     * Create an instance of {@link FormOfAddressCode }
     * 
     */
    public FormOfAddressCode createFormOfAddressCode() {
        return new FormOfAddressCode();
    }

    /**
     * Create an instance of {@link UPPEROPENLOCALNORMALISEDDateTimePeriod }
     * 
     */
    public UPPEROPENLOCALNORMALISEDDateTimePeriod createUPPEROPENLOCALNORMALISEDDateTimePeriod() {
        return new UPPEROPENLOCALNORMALISEDDateTimePeriod();
    }

    /**
     * Create an instance of {@link MEDIUMDescription }
     * 
     */
    public MEDIUMDescription createMEDIUMDescription() {
        return new MEDIUMDescription();
    }

    /**
     * Create an instance of {@link CashDiscountTermsCode }
     * 
     */
    public CashDiscountTermsCode createCashDiscountTermsCode() {
        return new CashDiscountTermsCode();
    }

    /**
     * Create an instance of {@link ProjectReference }
     * 
     */
    public ProjectReference createProjectReference() {
        return new ProjectReference();
    }

    /**
     * Create an instance of {@link ProductTaxStandardClassificationSystemCode }
     * 
     */
    public ProductTaxStandardClassificationSystemCode createProductTaxStandardClassificationSystemCode() {
        return new ProductTaxStandardClassificationSystemCode();
    }

    /**
     * Create an instance of {@link ProjectElementID }
     * 
     */
    public ProjectElementID createProjectElementID() {
        return new ProjectElementID();
    }

    /**
     * Create an instance of {@link PartyID }
     * 
     */
    public PartyID createPartyID() {
        return new PartyID();
    }

    /**
     * Create an instance of {@link ProductCategoryStandardID }
     * 
     */
    public ProductCategoryStandardID createProductCategoryStandardID() {
        return new ProductCategoryStandardID();
    }

    /**
     * Create an instance of {@link UUID }
     * 
     */
    public UUID createUUID() {
        return new UUID();
    }

    /**
     * Create an instance of {@link LogItemCategoryCode }
     * 
     */
    public LogItemCategoryCode createLogItemCategoryCode() {
        return new LogItemCategoryCode();
    }

    /**
     * Create an instance of {@link TextCollectionTextTypeCode }
     * 
     */
    public TextCollectionTextTypeCode createTextCollectionTextTypeCode() {
        return new TextCollectionTextTypeCode();
    }

    /**
     * Create an instance of {@link Price }
     * 
     */
    public Price createPrice() {
        return new Price();
    }

    /**
     * Create an instance of {@link IncotermsPOExcelUpload }
     * 
     */
    public IncotermsPOExcelUpload createIncotermsPOExcelUpload() {
        return new IncotermsPOExcelUpload();
    }

    /**
     * Create an instance of {@link BusinessObjectTypeCode }
     * 
     */
    public BusinessObjectTypeCode createBusinessObjectTypeCode() {
        return new BusinessObjectTypeCode();
    }

    /**
     * Create an instance of {@link DatePeriod }
     * 
     */
    public DatePeriod createDatePeriod() {
        return new DatePeriod();
    }

    /**
     * Create an instance of {@link TaxRateTypeCode }
     * 
     */
    public TaxRateTypeCode createTaxRateTypeCode() {
        return new TaxRateTypeCode();
    }

    /**
     * Create an instance of {@link LogItem }
     * 
     */
    public LogItem createLogItem() {
        return new LogItem();
    }

    /**
     * Create an instance of {@link ProjectID }
     * 
     */
    public ProjectID createProjectID() {
        return new ProjectID();
    }

    /**
     * Create an instance of {@link TaxJurisdictionSubdivisionCode }
     * 
     */
    public TaxJurisdictionSubdivisionCode createTaxJurisdictionSubdivisionCode() {
        return new TaxJurisdictionSubdivisionCode();
    }

    /**
     * Create an instance of {@link CostObjectTypeCode }
     * 
     */
    public CostObjectTypeCode createCostObjectTypeCode() {
        return new CostObjectTypeCode();
    }

    /**
     * Create an instance of {@link BusinessTransactionDocumentID }
     * 
     */
    public BusinessTransactionDocumentID createBusinessTransactionDocumentID() {
        return new BusinessTransactionDocumentID();
    }

    /**
     * Create an instance of {@link Quantity }
     * 
     */
    public Quantity createQuantity() {
        return new Quantity();
    }

    /**
     * Create an instance of {@link AddressRepresentationCode }
     * 
     */
    public AddressRepresentationCode createAddressRepresentationCode() {
        return new AddressRepresentationCode();
    }

    /**
     * Create an instance of {@link LONGName }
     * 
     */
    public LONGName createLONGName() {
        return new LONGName();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey }
     * 
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey createMaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey() {
        return new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey();
    }

    /**
     * Create an instance of {@link CodingBlockCustomField3Code }
     * 
     */
    public CodingBlockCustomField3Code createCodingBlockCustomField3Code() {
        return new CodingBlockCustomField3Code();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment }
     * 
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment createMaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment() {
        return new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment();
    }

    /**
     * Create an instance of {@link CodingBlockCustomField2Code }
     * 
     */
    public CodingBlockCustomField2Code createCodingBlockCustomField2Code() {
        return new CodingBlockCustomField2Code();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistribution }
     * 
     */
    public MaintenanceAccountingCodingBlockDistribution createMaintenanceAccountingCodingBlockDistribution() {
        return new MaintenanceAccountingCodingBlockDistribution();
    }

    /**
     * Create an instance of {@link CodingBlockCustomField1Code }
     * 
     */
    public CodingBlockCustomField1Code createCodingBlockCustomField1Code() {
        return new CodingBlockCustomField1Code();
    }

    /**
     * Create an instance of {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey }
     * 
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey createMaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey() {
        return new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey();
    }

    /**
     * Create an instance of {@link AccountingObjectCheckItemCostObjectReference }
     * 
     */
    public AccountingObjectCheckItemCostObjectReference createAccountingObjectCheckItemCostObjectReference() {
        return new AccountingObjectCheckItemCostObjectReference();
    }

    /**
     * Create an instance of {@link CustomObjectID }
     * 
     */
    public CustomObjectID createCustomObjectID() {
        return new CustomObjectID();
    }

    /**
     * Create an instance of {@link BinaryObject }
     * 
     */
    public BinaryObject createBinaryObject() {
        return new BinaryObject();
    }

    /**
     * Create an instance of {@link LOCALNORMALISEDDateTime }
     * 
     */
    public LOCALNORMALISEDDateTime createLOCALNORMALISEDDateTime() {
        return new LOCALNORMALISEDDateTime();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleDeliveryTerms }
     * 
     */
    public PurchaseOrderMaintainRequestBundleDeliveryTerms createPurchaseOrderMaintainRequestBundleDeliveryTerms() {
        return new PurchaseOrderMaintainRequestBundleDeliveryTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemFollowUpDelivery }
     * 
     */
    public PurchaseOrderByIDResponseItemFollowUpDelivery createPurchaseOrderByIDResponseItemFollowUpDelivery() {
        return new PurchaseOrderByIDResponseItemFollowUpDelivery();
    }

    /**
     * Create an instance of {@link PurchaseOrderReadItemWithholdingTaxDetails }
     * 
     */
    public PurchaseOrderReadItemWithholdingTaxDetails createPurchaseOrderReadItemWithholdingTaxDetails() {
        return new PurchaseOrderReadItemWithholdingTaxDetails();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails }
     * 
     */
    public PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails createPurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails() {
        return new PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemWithholdingTaxDetails();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItem }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItem createPurchaseOrderMaintainRequestBundleItem() {
        return new PurchaseOrderMaintainRequestBundleItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseSellerParty }
     * 
     */
    public PurchaseOrderByIDResponseSellerParty createPurchaseOrderByIDResponseSellerParty() {
        return new PurchaseOrderByIDResponseSellerParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemIndividualMaterial }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemIndividualMaterial createPurchaseOrderMaintainRequestBundleItemIndividualMaterial() {
        return new PurchaseOrderMaintainRequestBundleItemIndividualMaterial();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundle }
     * 
     */
    public PurchaseOrderMaintainRequestBundle createPurchaseOrderMaintainRequestBundle() {
        return new PurchaseOrderMaintainRequestBundle();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressDisplayName }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressDisplayName createPurchaseOrderMaintainRequestLocationAddressDisplayName() {
        return new PurchaseOrderMaintainRequestLocationAddressDisplayName();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressPostalAddress }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressPostalAddress createPurchaseOrderMaintainRequestLocationAddressPostalAddress() {
        return new PurchaseOrderMaintainRequestLocationAddressPostalAddress();
    }

    /**
     * Create an instance of {@link PurchaseOrderReadItemTaxationTerms }
     * 
     */
    public PurchaseOrderReadItemTaxationTerms createPurchaseOrderReadItemTaxationTerms() {
        return new PurchaseOrderReadItemTaxationTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestTaxCalculationItem }
     * 
     */
    public PurchaseOrderMaintainRequestTaxCalculationItem createPurchaseOrderMaintainRequestTaxCalculationItem() {
        return new PurchaseOrderMaintainRequestTaxCalculationItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderUploadRequestMessageSync }
     * 
     */
    public PurchaseOrderUploadRequestMessageSync createPurchaseOrderUploadRequestMessageSync() {
        return new PurchaseOrderUploadRequestMessageSync();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails }
     * 
     */
    public PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails createPurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails() {
        return new PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemProductTaxDetails();
    }

    /**
     * Create an instance of {@link PurchaseOrderUploadRequest }
     * 
     */
    public PurchaseOrderUploadRequest createPurchaseOrderUploadRequest() {
        return new PurchaseOrderUploadRequest();
    }

    /**
     * Create an instance of {@link PurchaseOrderReadPriceCalculationItem }
     * 
     */
    public PurchaseOrderReadPriceCalculationItem createPurchaseOrderReadPriceCalculationItem() {
        return new PurchaseOrderReadPriceCalculationItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleParty }
     * 
     */
    public PurchaseOrderMaintainRequestBundleParty createPurchaseOrderMaintainRequestBundleParty() {
        return new PurchaseOrderMaintainRequestBundleParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemProductReceipientParty }
     * 
     */
    public PurchaseOrderByIDResponseItemProductReceipientParty createPurchaseOrderByIDResponseItemProductReceipientParty() {
        return new PurchaseOrderByIDResponseItemProductReceipientParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestUploadDeliveryTerms }
     * 
     */
    public PurchaseOrderMaintainRequestUploadDeliveryTerms createPurchaseOrderMaintainRequestUploadDeliveryTerms() {
        return new PurchaseOrderMaintainRequestUploadDeliveryTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemProduct }
     * 
     */
    public PurchaseOrderByIDResponseItemProduct createPurchaseOrderByIDResponseItemProduct() {
        return new PurchaseOrderByIDResponseItemProduct();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddress }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddress createPurchaseOrderMaintainRequestLocationAddress() {
        return new PurchaseOrderMaintainRequestLocationAddress();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemParty }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemParty createPurchaseOrderMaintainRequestBundleItemParty() {
        return new PurchaseOrderMaintainRequestBundleItemParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseMessageSync }
     * 
     */
    public PurchaseOrderByIDResponseMessageSync createPurchaseOrderByIDResponseMessageSync() {
        return new PurchaseOrderByIDResponseMessageSync();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemScheduleLine }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemScheduleLine createPurchaseOrderMaintainRequestBundleItemScheduleLine() {
        return new PurchaseOrderMaintainRequestBundleItemScheduleLine();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms }
     * 
     */
    public PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms createPurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms() {
        return new PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemTaxationTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressEmail }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressEmail createPurchaseOrderMaintainRequestLocationAddressEmail() {
        return new PurchaseOrderMaintainRequestLocationAddressEmail();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItem }
     * 
     */
    public PurchaseOrderByIDResponseItem createPurchaseOrderByIDResponseItem() {
        return new PurchaseOrderByIDResponseItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressTelephone }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressTelephone createPurchaseOrderMaintainRequestLocationAddressTelephone() {
        return new PurchaseOrderMaintainRequestLocationAddressTelephone();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemDeliveryTerms }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemDeliveryTerms createPurchaseOrderMaintainRequestBundleItemDeliveryTerms() {
        return new PurchaseOrderMaintainRequestBundleItemDeliveryTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemLandedCosts }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemLandedCosts createPurchaseOrderMaintainRequestBundleItemLandedCosts() {
        return new PurchaseOrderMaintainRequestBundleItemLandedCosts();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemFollowUpInvoice }
     * 
     */
    public PurchaseOrderByIDResponseItemFollowUpInvoice createPurchaseOrderByIDResponseItemFollowUpInvoice() {
        return new PurchaseOrderByIDResponseItemFollowUpInvoice();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDQueryMessageSync }
     * 
     */
    public PurchaseOrderByIDQueryMessageSync createPurchaseOrderByIDQueryMessageSync() {
        return new PurchaseOrderByIDQueryMessageSync();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseBuyerParty }
     * 
     */
    public PurchaseOrderByIDResponseBuyerParty createPurchaseOrderByIDResponseBuyerParty() {
        return new PurchaseOrderByIDResponseBuyerParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainConfirmationBundle }
     * 
     */
    public PurchaseOrderMaintainConfirmationBundle createPurchaseOrderMaintainConfirmationBundle() {
        return new PurchaseOrderMaintainConfirmationBundle();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponse }
     * 
     */
    public PurchaseOrderByIDResponse createPurchaseOrderByIDResponse() {
        return new PurchaseOrderByIDResponse();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemLocation }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemLocation createPurchaseOrderMaintainRequestBundleItemLocation() {
        return new PurchaseOrderMaintainRequestBundleItemLocation();
    }

    /**
     * Create an instance of {@link PurchaseOrderUploadItem }
     * 
     */
    public PurchaseOrderUploadItem createPurchaseOrderUploadItem() {
        return new PurchaseOrderUploadItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderReadItemPriceComponent }
     * 
     */
    public PurchaseOrderReadItemPriceComponent createPurchaseOrderReadItemPriceComponent() {
        return new PurchaseOrderReadItemPriceComponent();
    }

    /**
     * Create an instance of {@link PurchaseOrderReadTaxCalculationItem }
     * 
     */
    public PurchaseOrderReadTaxCalculationItem createPurchaseOrderReadTaxCalculationItem() {
        return new PurchaseOrderReadTaxCalculationItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressName }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressName createPurchaseOrderMaintainRequestLocationAddressName() {
        return new PurchaseOrderMaintainRequestLocationAddressName();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestPriceCalculationItem }
     * 
     */
    public PurchaseOrderMaintainRequestPriceCalculationItem createPurchaseOrderMaintainRequestPriceCalculationItem() {
        return new PurchaseOrderMaintainRequestPriceCalculationItem();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemServicePerfomerParty }
     * 
     */
    public PurchaseOrderByIDResponseItemServicePerfomerParty createPurchaseOrderByIDResponseItemServicePerfomerParty() {
        return new PurchaseOrderByIDResponseItemServicePerfomerParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressFascmile }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressFascmile createPurchaseOrderMaintainRequestLocationAddressFascmile() {
        return new PurchaseOrderMaintainRequestLocationAddressFascmile();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemEndBuyerParty }
     * 
     */
    public PurchaseOrderByIDResponseItemEndBuyerParty createPurchaseOrderByIDResponseItemEndBuyerParty() {
        return new PurchaseOrderByIDResponseItemEndBuyerParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainConfirmationBundleMessageSync }
     * 
     */
    public PurchaseOrderMaintainConfirmationBundleMessageSync createPurchaseOrderMaintainConfirmationBundleMessageSync() {
        return new PurchaseOrderMaintainConfirmationBundleMessageSync();
    }

    /**
     * Create an instance of {@link PurchaseOrderUploadConfirmationMessageSync }
     * 
     */
    public PurchaseOrderUploadConfirmationMessageSync createPurchaseOrderUploadConfirmationMessageSync() {
        return new PurchaseOrderUploadConfirmationMessageSync();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDQuery }
     * 
     */
    public PurchaseOrderByIDQuery createPurchaseOrderByIDQuery() {
        return new PurchaseOrderByIDQuery();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemProduct }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemProduct createPurchaseOrderMaintainRequestBundleItemProduct() {
        return new PurchaseOrderMaintainRequestBundleItemProduct();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseBillToParty }
     * 
     */
    public PurchaseOrderByIDResponseBillToParty createPurchaseOrderByIDResponseBillToParty() {
        return new PurchaseOrderByIDResponseBillToParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseResponsiblePurchasingUnitParty }
     * 
     */
    public PurchaseOrderByIDResponseResponsiblePurchasingUnitParty createPurchaseOrderByIDResponseResponsiblePurchasingUnitParty() {
        return new PurchaseOrderByIDResponseResponsiblePurchasingUnitParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestLocationAddressWeb }
     * 
     */
    public PurchaseOrderMaintainRequestLocationAddressWeb createPurchaseOrderMaintainRequestLocationAddressWeb() {
        return new PurchaseOrderMaintainRequestLocationAddressWeb();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseEmployeeResponsibleParty }
     * 
     */
    public PurchaseOrderByIDResponseEmployeeResponsibleParty createPurchaseOrderByIDResponseEmployeeResponsibleParty() {
        return new PurchaseOrderByIDResponseEmployeeResponsibleParty();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference }
     * 
     */
    public PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference createPurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference() {
        return new PurchaseOrderMaintainRequestBundleItemBusinessTransactionDocumentReference();
    }

    /**
     * Create an instance of {@link PurchaseOrderUploadConfirmation }
     * 
     */
    public PurchaseOrderUploadConfirmation createPurchaseOrderUploadConfirmation() {
        return new PurchaseOrderUploadConfirmation();
    }

    /**
     * Create an instance of {@link PurchaseOrderReadItemProductTaxDetails }
     * 
     */
    public PurchaseOrderReadItemProductTaxDetails createPurchaseOrderReadItemProductTaxDetails() {
        return new PurchaseOrderReadItemProductTaxDetails();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation }
     * 
     */
    public PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation createPurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation() {
        return new PurchaseOrderByIDResponseItemFollowUpPurchaseOrderConfirmation();
    }

    /**
     * Create an instance of {@link PurchaseOrderBundleMaintainRequestMessageSync }
     * 
     */
    public PurchaseOrderBundleMaintainRequestMessageSync createPurchaseOrderBundleMaintainRequestMessageSync() {
        return new PurchaseOrderBundleMaintainRequestMessageSync();
    }

    /**
     * Create an instance of {@link PurchaseOrderByIDResponseItemDeliveryTerms }
     * 
     */
    public PurchaseOrderByIDResponseItemDeliveryTerms createPurchaseOrderByIDResponseItemDeliveryTerms() {
        return new PurchaseOrderByIDResponseItemDeliveryTerms();
    }

    /**
     * Create an instance of {@link PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent }
     * 
     */
    public PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent createPurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent() {
        return new PurchaseOrderMaintainRequestPriceAndTaxCalculationItemItemPriceComponent();
    }

    /**
     * Create an instance of {@link StandardFaultMessage }
     * 
     */
    public StandardFaultMessage createStandardFaultMessage() {
        return new StandardFaultMessage();
    }

    /**
     * Create an instance of {@link ExchangeFaultData }
     * 
     */
    public ExchangeFaultData createExchangeFaultData() {
        return new ExchangeFaultData();
    }

    /**
     * Create an instance of {@link StandardFaultMessageExtension }
     * 
     */
    public StandardFaultMessageExtension createStandardFaultMessageExtension() {
        return new StandardFaultMessageExtension();
    }

    /**
     * Create an instance of {@link ProductCategoryHierarchyProductCategoryIDKey }
     * 
     */
    public ProductCategoryHierarchyProductCategoryIDKey createProductCategoryHierarchyProductCategoryIDKey() {
        return new ProductCategoryHierarchyProductCategoryIDKey();
    }

    /**
     * Create an instance of {@link RequirementSpecificationKey }
     * 
     */
    public RequirementSpecificationKey createRequirementSpecificationKey() {
        return new RequirementSpecificationKey();
    }

    /**
     * Create an instance of {@link PartyKey }
     * 
     */
    public PartyKey createPartyKey() {
        return new PartyKey();
    }

    /**
     * Create an instance of {@link ExchangeLogData }
     * 
     */
    public ExchangeLogData createExchangeLogData() {
        return new ExchangeLogData();
    }

    /**
     * Create an instance of {@link ProductKey }
     * 
     */
    public ProductKey createProductKey() {
        return new ProductKey();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderUploadRequestMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderUploadRequest_sync")
    public JAXBElement<PurchaseOrderUploadRequestMessageSync> createPurchaseOrderUploadRequestSync(PurchaseOrderUploadRequestMessageSync value) {
        return new JAXBElement<PurchaseOrderUploadRequestMessageSync>(_PurchaseOrderUploadRequestSync_QNAME, PurchaseOrderUploadRequestMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderMaintainConfirmationBundleMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderBundleMaintainConfirmation_sync")
    public JAXBElement<PurchaseOrderMaintainConfirmationBundleMessageSync> createPurchaseOrderBundleMaintainConfirmationSync(PurchaseOrderMaintainConfirmationBundleMessageSync value) {
        return new JAXBElement<PurchaseOrderMaintainConfirmationBundleMessageSync>(_PurchaseOrderBundleMaintainConfirmationSync_QNAME, PurchaseOrderMaintainConfirmationBundleMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderByIDQueryMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderByIDQuery_sync")
    public JAXBElement<PurchaseOrderByIDQueryMessageSync> createPurchaseOrderByIDQuerySync(PurchaseOrderByIDQueryMessageSync value) {
        return new JAXBElement<PurchaseOrderByIDQueryMessageSync>(_PurchaseOrderByIDQuerySync_QNAME, PurchaseOrderByIDQueryMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderBundleMaintainRequestMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderBundleMaintainRequest_sync")
    public JAXBElement<PurchaseOrderBundleMaintainRequestMessageSync> createPurchaseOrderBundleMaintainRequestSync(PurchaseOrderBundleMaintainRequestMessageSync value) {
        return new JAXBElement<PurchaseOrderBundleMaintainRequestMessageSync>(_PurchaseOrderBundleMaintainRequestSync_QNAME, PurchaseOrderBundleMaintainRequestMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderByIDResponseMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderByIDResponse_sync")
    public JAXBElement<PurchaseOrderByIDResponseMessageSync> createPurchaseOrderByIDResponseSync(PurchaseOrderByIDResponseMessageSync value) {
        return new JAXBElement<PurchaseOrderByIDResponseMessageSync>(_PurchaseOrderByIDResponseSync_QNAME, PurchaseOrderByIDResponseMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderBundleMaintainRequestMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderBundleCheckMaintainRequest_sync")
    public JAXBElement<PurchaseOrderBundleMaintainRequestMessageSync> createPurchaseOrderBundleCheckMaintainRequestSync(PurchaseOrderBundleMaintainRequestMessageSync value) {
        return new JAXBElement<PurchaseOrderBundleMaintainRequestMessageSync>(_PurchaseOrderBundleCheckMaintainRequestSync_QNAME, PurchaseOrderBundleMaintainRequestMessageSync.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderUploadConfirmationMessageSync }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sap.com/xi/SAPGlobal20/Global", name = "PurchaseOrderUploadConfirmation_sync")
    public JAXBElement<PurchaseOrderUploadConfirmationMessageSync> createPurchaseOrderUploadConfirmationSync(PurchaseOrderUploadConfirmationMessageSync value) {
        return new JAXBElement<PurchaseOrderUploadConfirmationMessageSync>(_PurchaseOrderUploadConfirmationSync_QNAME, PurchaseOrderUploadConfirmationMessageSync.class, null, value);
    }

}
