
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderByIDQueryMessage_sync complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderByIDQueryMessage_sync">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PurchaseOrder" type="{http://sap.com/xi/A1S/Global}PurchaseOrderByIDQuery"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderByIDQueryMessage_sync", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "purchaseOrder"
})
public class PurchaseOrderByIDQueryMessageSync {

    @XmlElement(name = "PurchaseOrder", required = true)
    protected PurchaseOrderByIDQuery purchaseOrder;

    /**
     * Obtiene el valor de la propiedad purchaseOrder.
     * 
     * @return
     *     possible object is
     *     {@link PurchaseOrderByIDQuery }
     *     
     */
    public PurchaseOrderByIDQuery getPurchaseOrder() {
        return purchaseOrder;
    }

    /**
     * Define el valor de la propiedad purchaseOrder.
     * 
     * @param value
     *     allowed object is
     *     {@link PurchaseOrderByIDQuery }
     *     
     */
    public void setPurchaseOrder(PurchaseOrderByIDQuery value) {
        this.purchaseOrder = value;
    }

}
