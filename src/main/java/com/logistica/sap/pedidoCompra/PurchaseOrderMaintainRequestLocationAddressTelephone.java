
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PurchaseOrderMaintainRequestLocationAddressTelephone complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PurchaseOrderMaintainRequestLocationAddressTelephone">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://sap.com/xi/AP/Common/GDT}PhoneNumber" minOccurs="0"/>
 *         &lt;element name="FormattedNumberDescription" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_SHORT_Description" minOccurs="0"/>
 *         &lt;element name="DefaultConventionalPhoneNumberIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="DefaultMobilePhoneNumberIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="UsageDeniedIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="MobilePhoneNumberIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *         &lt;element name="SMSEnabledIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PurchaseOrderMaintainRequestLocationAddressTelephone", namespace = "http://sap.com/xi/A1S/Global", propOrder = {
    "number",
    "formattedNumberDescription",
    "defaultConventionalPhoneNumberIndicator",
    "defaultMobilePhoneNumberIndicator",
    "usageDeniedIndicator",
    "mobilePhoneNumberIndicator",
    "smsEnabledIndicator"
})
public class PurchaseOrderMaintainRequestLocationAddressTelephone {

    @XmlElement(name = "Number")
    protected PhoneNumber number;
    @XmlElement(name = "FormattedNumberDescription")
    protected String formattedNumberDescription;
    @XmlElement(name = "DefaultConventionalPhoneNumberIndicator")
    protected Boolean defaultConventionalPhoneNumberIndicator;
    @XmlElement(name = "DefaultMobilePhoneNumberIndicator")
    protected Boolean defaultMobilePhoneNumberIndicator;
    @XmlElement(name = "UsageDeniedIndicator")
    protected Boolean usageDeniedIndicator;
    @XmlElement(name = "MobilePhoneNumberIndicator")
    protected Boolean mobilePhoneNumberIndicator;
    @XmlElement(name = "SMSEnabledIndicator")
    protected Boolean smsEnabledIndicator;

    /**
     * Obtiene el valor de la propiedad number.
     * 
     * @return
     *     possible object is
     *     {@link PhoneNumber }
     *     
     */
    public PhoneNumber getNumber() {
        return number;
    }

    /**
     * Define el valor de la propiedad number.
     * 
     * @param value
     *     allowed object is
     *     {@link PhoneNumber }
     *     
     */
    public void setNumber(PhoneNumber value) {
        this.number = value;
    }

    /**
     * Obtiene el valor de la propiedad formattedNumberDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormattedNumberDescription() {
        return formattedNumberDescription;
    }

    /**
     * Define el valor de la propiedad formattedNumberDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormattedNumberDescription(String value) {
        this.formattedNumberDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad defaultConventionalPhoneNumberIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultConventionalPhoneNumberIndicator() {
        return defaultConventionalPhoneNumberIndicator;
    }

    /**
     * Define el valor de la propiedad defaultConventionalPhoneNumberIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultConventionalPhoneNumberIndicator(Boolean value) {
        this.defaultConventionalPhoneNumberIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad defaultMobilePhoneNumberIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDefaultMobilePhoneNumberIndicator() {
        return defaultMobilePhoneNumberIndicator;
    }

    /**
     * Define el valor de la propiedad defaultMobilePhoneNumberIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDefaultMobilePhoneNumberIndicator(Boolean value) {
        this.defaultMobilePhoneNumberIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad usageDeniedIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsageDeniedIndicator() {
        return usageDeniedIndicator;
    }

    /**
     * Define el valor de la propiedad usageDeniedIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsageDeniedIndicator(Boolean value) {
        this.usageDeniedIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad mobilePhoneNumberIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMobilePhoneNumberIndicator() {
        return mobilePhoneNumberIndicator;
    }

    /**
     * Define el valor de la propiedad mobilePhoneNumberIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMobilePhoneNumberIndicator(Boolean value) {
        this.mobilePhoneNumberIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad smsEnabledIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSMSEnabledIndicator() {
        return smsEnabledIndicator;
    }

    /**
     * Define el valor de la propiedad smsEnabledIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSMSEnabledIndicator(Boolean value) {
        this.smsEnabledIndicator = value;
    }

}
