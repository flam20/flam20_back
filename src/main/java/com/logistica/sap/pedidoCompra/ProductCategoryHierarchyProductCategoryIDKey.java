
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para ProductCategoryHierarchyProductCategoryIDKey complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProductCategoryHierarchyProductCategoryIDKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductCategoryHierarchyID" type="{http://sap.com/xi/AP/Common/GDT}ProductCategoryHierarchyID"/>
 *         &lt;element name="ProductCategoryInternalID" type="{http://sap.com/xi/AP/Common/GDT}ProductCategoryInternalID"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductCategoryHierarchyProductCategoryIDKey", namespace = "http://sap.com/xi/AP/Common/Global", propOrder = {
    "productCategoryHierarchyID",
    "productCategoryInternalID"
})
public class ProductCategoryHierarchyProductCategoryIDKey {

    @XmlElement(name = "ProductCategoryHierarchyID", required = true)
    protected ProductCategoryHierarchyID productCategoryHierarchyID;
    @XmlElement(name = "ProductCategoryInternalID", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String productCategoryInternalID;

    /**
     * Obtiene el valor de la propiedad productCategoryHierarchyID.
     * 
     * @return
     *     possible object is
     *     {@link ProductCategoryHierarchyID }
     *     
     */
    public ProductCategoryHierarchyID getProductCategoryHierarchyID() {
        return productCategoryHierarchyID;
    }

    /**
     * Define el valor de la propiedad productCategoryHierarchyID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCategoryHierarchyID }
     *     
     */
    public void setProductCategoryHierarchyID(ProductCategoryHierarchyID value) {
        this.productCategoryHierarchyID = value;
    }

    /**
     * Obtiene el valor de la propiedad productCategoryInternalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategoryInternalID() {
        return productCategoryInternalID;
    }

    /**
     * Define el valor de la propiedad productCategoryInternalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategoryInternalID(String value) {
        this.productCategoryInternalID = value;
    }

}
