
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para Incoterms_PO_excel_upload complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Incoterms_PO_excel_upload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClassificationCode" type="{http://sap.com/xi/AP/Common/GDT}IncotermsClassificationCode" minOccurs="0"/>
 *         &lt;element name="TransferLocationName" type="{http://sap.com/xi/AP/Common/GDT}IncotermsTransferLocationName" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Incoterms_PO_excel_upload", propOrder = {
    "classificationCode",
    "transferLocationName"
})
public class IncotermsPOExcelUpload {

    @XmlElement(name = "ClassificationCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String classificationCode;
    @XmlElement(name = "TransferLocationName")
    protected String transferLocationName;

    /**
     * Obtiene el valor de la propiedad classificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Define el valor de la propiedad classificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Obtiene el valor de la propiedad transferLocationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransferLocationName() {
        return transferLocationName;
    }

    /**
     * Define el valor de la propiedad transferLocationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransferLocationName(String value) {
        this.transferLocationName = value;
    }

}
