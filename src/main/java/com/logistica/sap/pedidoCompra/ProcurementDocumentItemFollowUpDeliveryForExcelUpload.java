
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para ProcurementDocumentItemFollowUpDeliveryForExcelUpload complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProcurementDocumentItemFollowUpDeliveryForExcelUpload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequirementCode" type="{http://sap.com/xi/AP/Common/GDT}FollowUpBusinessTransactionDocumentRequirementCode" minOccurs="0"/>
 *         &lt;element name="EmployeeTimeConfirmationRequiredIndicator" type="{http://sap.com/xi/AP/Common/GDT}Indicator" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcurementDocumentItemFollowUpDeliveryForExcelUpload", namespace = "http://sap.com/xi/AP/XU/SRM/Global", propOrder = {
    "requirementCode",
    "employeeTimeConfirmationRequiredIndicator"
})
public class ProcurementDocumentItemFollowUpDeliveryForExcelUpload {

    @XmlElement(name = "RequirementCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String requirementCode;
    @XmlElement(name = "EmployeeTimeConfirmationRequiredIndicator")
    protected Boolean employeeTimeConfirmationRequiredIndicator;

    /**
     * Obtiene el valor de la propiedad requirementCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequirementCode() {
        return requirementCode;
    }

    /**
     * Define el valor de la propiedad requirementCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequirementCode(String value) {
        this.requirementCode = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeTimeConfirmationRequiredIndicator.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEmployeeTimeConfirmationRequiredIndicator() {
        return employeeTimeConfirmationRequiredIndicator;
    }

    /**
     * Define el valor de la propiedad employeeTimeConfirmationRequiredIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEmployeeTimeConfirmationRequiredIndicator(Boolean value) {
        this.employeeTimeConfirmationRequiredIndicator = value;
    }

}
