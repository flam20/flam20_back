
package com.logistica.sap.pedidoCompra;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerContractItemDescription" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="CustomerContractName" type="{http://sap.com/xi/AP/Common/GDT}EXTENDED_Name" minOccurs="0"/>
 *         &lt;element name="CustomerContractReference" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentReference" minOccurs="0"/>
 *         &lt;element name="TechnicalID" type="{http://sap.com/xi/AP/Common/GDT}ObjectNodeTechnicalID" minOccurs="0"/>
 *         &lt;element name="Percent" type="{http://sap.com/xi/AP/Common/GDT}Percent" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://sap.com/xi/AP/Common/GDT}Amount" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://sap.com/xi/AP/Common/GDT}Quantity" minOccurs="0"/>
 *         &lt;element name="AccountingCodingBlockTypeCode" type="{http://sap.com/xi/AP/Common/GDT}AccountingCodingBlockTypeCode" minOccurs="0"/>
 *         &lt;element name="AccountDeterminationExpenseGroupCode" type="{http://sap.com/xi/AP/Common/GDT}AccountDeterminationExpenseGroupCode" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerAccountAliasCode" type="{http://sap.com/xi/AP/Common/GDT}GeneralLedgerAccountAliasCode" minOccurs="0"/>
 *         &lt;element name="ProfitCentreID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *         &lt;element name="ProfitCentreUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="CostCentreID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *         &lt;element name="CostCentreUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="IndividualMaterialKey" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey" minOccurs="0"/>
 *         &lt;element name="IndividualMaterialUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="ProjectTaskKey" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey" minOccurs="0"/>
 *         &lt;element name="ProjectReference" type="{http://sap.com/xi/AP/Common/GDT}ProjectReference" minOccurs="0"/>
 *         &lt;element name="ProjectReferenceProjectElementTypeName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="SalesOrderReference" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentReference" minOccurs="0"/>
 *         &lt;element name="SalesOrderReferenceTypeName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="SalesOrderReferenceItemTypeName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="SalesOrderName" type="{http://sap.com/xi/AP/Common/GDT}EXTENDED_Name" minOccurs="0"/>
 *         &lt;element name="SalesOrderItemDescription" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="ServiceOrderReference" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentReference" minOccurs="0"/>
 *         &lt;element name="ServiceOrderReferenceTypeName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="ServiceOrderReferenceItemTypeName" type="{http://sap.com/xi/AP/Common/GDT}LANGUAGEINDEPENDENT_LONG_Name" minOccurs="0"/>
 *         &lt;element name="ServiceOrderName" type="{http://sap.com/xi/AP/Common/GDT}EXTENDED_Name" minOccurs="0"/>
 *         &lt;element name="ServiceOrderItemDescription" type="{http://sap.com/xi/AP/Common/GDT}SHORT_Description" minOccurs="0"/>
 *         &lt;element name="EmployeeID" type="{http://sap.com/xi/AP/Common/GDT}EmployeeID" minOccurs="0"/>
 *         &lt;element name="EmployeeUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="CompanyID" type="{http://sap.com/xi/AP/Common/GDT}OrganisationalCentreID" minOccurs="0"/>
 *         &lt;element name="CompanyUUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="FinancialFunctionUUID" type="{http://sap.com/xi/Common/DataTypes}UUID" minOccurs="0"/>
 *         &lt;element name="FinancialFundUUID" type="{http://sap.com/xi/Common/DataTypes}UUID" minOccurs="0"/>
 *         &lt;element name="FinancialFunctionID" type="{http://sap.com/xi/AP/FO/FundManagement/Global}FunctionID" minOccurs="0"/>
 *         &lt;element name="FinancialFundID" type="{http://sap.com/xi/AP/FO/FundManagement/Global}FundID" minOccurs="0"/>
 *         &lt;element name="CustomCode1" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}CodingBlockCustomField1Code" minOccurs="0"/>
 *         &lt;element name="CustomCode2" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}CodingBlockCustomField2Code" minOccurs="0"/>
 *         &lt;element name="CustomCode3" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}CodingBlockCustomField3Code" minOccurs="0"/>
 *         &lt;element name="CustomObject1UUID" type="{http://sap.com/xi/AP/Common/GDT}UUID" minOccurs="0"/>
 *         &lt;element name="CustomObject1ID" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}CustomObjectID" minOccurs="0"/>
 *         &lt;element name="CustomText1" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}CodingBlockCustomText" minOccurs="0"/>
 *         &lt;element name="CostObjectReference" type="{http://sap.com/xi/AP/IS/CodingBlock/Global}AccountingObjectCheckItemCostObjectReference" minOccurs="0"/>
 *         &lt;element name="GrantID" type="{http://sap.com/xi/AP/FO/GrantManagement/Global}GrantID" minOccurs="0"/>
 *         &lt;element name="GrantUUID" type="{http://sap.com/xi/Common/DataTypes}UUID" minOccurs="0"/>
 *         &lt;element name="LeaseContractReference" type="{http://sap.com/xi/AP/Common/GDT}BusinessTransactionDocumentReference" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="ActionCode" type="{http://sap.com/xi/AP/Common/GDT}ActionCode" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment", namespace = "http://sap.com/xi/AP/IS/CodingBlock/Global", propOrder = {
    "customerContractItemDescription",
    "customerContractName",
    "customerContractReference",
    "technicalID",
    "percent",
    "amount",
    "quantity",
    "accountingCodingBlockTypeCode",
    "accountDeterminationExpenseGroupCode",
    "generalLedgerAccountAliasCode",
    "profitCentreID",
    "profitCentreUUID",
    "costCentreID",
    "costCentreUUID",
    "individualMaterialKey",
    "individualMaterialUUID",
    "projectTaskKey",
    "projectReference",
    "projectReferenceProjectElementTypeName",
    "salesOrderReference",
    "salesOrderReferenceTypeName",
    "salesOrderReferenceItemTypeName",
    "salesOrderName",
    "salesOrderItemDescription",
    "serviceOrderReference",
    "serviceOrderReferenceTypeName",
    "serviceOrderReferenceItemTypeName",
    "serviceOrderName",
    "serviceOrderItemDescription",
    "employeeID",
    "employeeUUID",
    "companyID",
    "companyUUID",
    "financialFunctionUUID",
    "financialFundUUID",
    "financialFunctionID",
    "financialFundID",
    "customCode1",
    "customCode2",
    "customCode3",
    "customObject1UUID",
    "customObject1ID",
    "customText1",
    "costObjectReference",
    "grantID",
    "grantUUID",
    "leaseContractReference"
})
public class MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment {

    @XmlElement(name = "CustomerContractItemDescription")
    protected SHORTDescription customerContractItemDescription;
    @XmlElement(name = "CustomerContractName")
    protected EXTENDEDName customerContractName;
    @XmlElement(name = "CustomerContractReference")
    protected BusinessTransactionDocumentReference customerContractReference;
    @XmlElement(name = "TechnicalID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String technicalID;
    @XmlElement(name = "Percent")
    protected BigDecimal percent;
    @XmlElement(name = "Amount")
    protected Amount amount;
    @XmlElement(name = "Quantity")
    protected Quantity quantity;
    @XmlElement(name = "AccountingCodingBlockTypeCode")
    protected AccountingCodingBlockTypeCode accountingCodingBlockTypeCode;
    @XmlElement(name = "AccountDeterminationExpenseGroupCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String accountDeterminationExpenseGroupCode;
    @XmlElement(name = "GeneralLedgerAccountAliasCode")
    protected GeneralLedgerAccountAliasCode generalLedgerAccountAliasCode;
    @XmlElement(name = "ProfitCentreID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String profitCentreID;
    @XmlElement(name = "ProfitCentreUUID")
    protected UUID profitCentreUUID;
    @XmlElement(name = "CostCentreID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String costCentreID;
    @XmlElement(name = "CostCentreUUID")
    protected UUID costCentreUUID;
    @XmlElement(name = "IndividualMaterialKey")
    protected MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey individualMaterialKey;
    @XmlElement(name = "IndividualMaterialUUID")
    protected UUID individualMaterialUUID;
    @XmlElement(name = "ProjectTaskKey")
    protected MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey projectTaskKey;
    @XmlElement(name = "ProjectReference")
    protected ProjectReference projectReference;
    @XmlElement(name = "ProjectReferenceProjectElementTypeName")
    protected String projectReferenceProjectElementTypeName;
    @XmlElement(name = "SalesOrderReference")
    protected BusinessTransactionDocumentReference salesOrderReference;
    @XmlElement(name = "SalesOrderReferenceTypeName")
    protected String salesOrderReferenceTypeName;
    @XmlElement(name = "SalesOrderReferenceItemTypeName")
    protected String salesOrderReferenceItemTypeName;
    @XmlElement(name = "SalesOrderName")
    protected EXTENDEDName salesOrderName;
    @XmlElement(name = "SalesOrderItemDescription")
    protected SHORTDescription salesOrderItemDescription;
    @XmlElement(name = "ServiceOrderReference")
    protected BusinessTransactionDocumentReference serviceOrderReference;
    @XmlElement(name = "ServiceOrderReferenceTypeName")
    protected String serviceOrderReferenceTypeName;
    @XmlElement(name = "ServiceOrderReferenceItemTypeName")
    protected String serviceOrderReferenceItemTypeName;
    @XmlElement(name = "ServiceOrderName")
    protected EXTENDEDName serviceOrderName;
    @XmlElement(name = "ServiceOrderItemDescription")
    protected SHORTDescription serviceOrderItemDescription;
    @XmlElement(name = "EmployeeID")
    protected EmployeeID employeeID;
    @XmlElement(name = "EmployeeUUID")
    protected UUID employeeUUID;
    @XmlElement(name = "CompanyID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String companyID;
    @XmlElement(name = "CompanyUUID")
    protected UUID companyUUID;
    @XmlElement(name = "FinancialFunctionUUID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String financialFunctionUUID;
    @XmlElement(name = "FinancialFundUUID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String financialFundUUID;
    @XmlElement(name = "FinancialFunctionID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String financialFunctionID;
    @XmlElement(name = "FinancialFundID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String financialFundID;
    @XmlElement(name = "CustomCode1")
    protected CodingBlockCustomField1Code customCode1;
    @XmlElement(name = "CustomCode2")
    protected CodingBlockCustomField2Code customCode2;
    @XmlElement(name = "CustomCode3")
    protected CodingBlockCustomField3Code customCode3;
    @XmlElement(name = "CustomObject1UUID")
    protected UUID customObject1UUID;
    @XmlElement(name = "CustomObject1ID")
    protected CustomObjectID customObject1ID;
    @XmlElement(name = "CustomText1")
    protected String customText1;
    @XmlElement(name = "CostObjectReference")
    protected AccountingObjectCheckItemCostObjectReference costObjectReference;
    @XmlElement(name = "GrantID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String grantID;
    @XmlElement(name = "GrantUUID")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String grantUUID;
    @XmlElement(name = "LeaseContractReference")
    protected BusinessTransactionDocumentReference leaseContractReference;
    @XmlAttribute(name = "ActionCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String actionCode;

    /**
     * Obtiene el valor de la propiedad customerContractItemDescription.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getCustomerContractItemDescription() {
        return customerContractItemDescription;
    }

    /**
     * Define el valor de la propiedad customerContractItemDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setCustomerContractItemDescription(SHORTDescription value) {
        this.customerContractItemDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad customerContractName.
     * 
     * @return
     *     possible object is
     *     {@link EXTENDEDName }
     *     
     */
    public EXTENDEDName getCustomerContractName() {
        return customerContractName;
    }

    /**
     * Define el valor de la propiedad customerContractName.
     * 
     * @param value
     *     allowed object is
     *     {@link EXTENDEDName }
     *     
     */
    public void setCustomerContractName(EXTENDEDName value) {
        this.customerContractName = value;
    }

    /**
     * Obtiene el valor de la propiedad customerContractReference.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public BusinessTransactionDocumentReference getCustomerContractReference() {
        return customerContractReference;
    }

    /**
     * Define el valor de la propiedad customerContractReference.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public void setCustomerContractReference(BusinessTransactionDocumentReference value) {
        this.customerContractReference = value;
    }

    /**
     * Obtiene el valor de la propiedad technicalID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechnicalID() {
        return technicalID;
    }

    /**
     * Define el valor de la propiedad technicalID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechnicalID(String value) {
        this.technicalID = value;
    }

    /**
     * Obtiene el valor de la propiedad percent.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPercent() {
        return percent;
    }

    /**
     * Define el valor de la propiedad percent.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPercent(BigDecimal value) {
        this.percent = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link Amount }
     *     
     */
    public Amount getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link Amount }
     *     
     */
    public void setAmount(Amount value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link Quantity }
     *     
     */
    public Quantity getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Quantity }
     *     
     */
    public void setQuantity(Quantity value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad accountingCodingBlockTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link AccountingCodingBlockTypeCode }
     *     
     */
    public AccountingCodingBlockTypeCode getAccountingCodingBlockTypeCode() {
        return accountingCodingBlockTypeCode;
    }

    /**
     * Define el valor de la propiedad accountingCodingBlockTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountingCodingBlockTypeCode }
     *     
     */
    public void setAccountingCodingBlockTypeCode(AccountingCodingBlockTypeCode value) {
        this.accountingCodingBlockTypeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad accountDeterminationExpenseGroupCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountDeterminationExpenseGroupCode() {
        return accountDeterminationExpenseGroupCode;
    }

    /**
     * Define el valor de la propiedad accountDeterminationExpenseGroupCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountDeterminationExpenseGroupCode(String value) {
        this.accountDeterminationExpenseGroupCode = value;
    }

    /**
     * Obtiene el valor de la propiedad generalLedgerAccountAliasCode.
     * 
     * @return
     *     possible object is
     *     {@link GeneralLedgerAccountAliasCode }
     *     
     */
    public GeneralLedgerAccountAliasCode getGeneralLedgerAccountAliasCode() {
        return generalLedgerAccountAliasCode;
    }

    /**
     * Define el valor de la propiedad generalLedgerAccountAliasCode.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralLedgerAccountAliasCode }
     *     
     */
    public void setGeneralLedgerAccountAliasCode(GeneralLedgerAccountAliasCode value) {
        this.generalLedgerAccountAliasCode = value;
    }

    /**
     * Obtiene el valor de la propiedad profitCentreID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitCentreID() {
        return profitCentreID;
    }

    /**
     * Define el valor de la propiedad profitCentreID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitCentreID(String value) {
        this.profitCentreID = value;
    }

    /**
     * Obtiene el valor de la propiedad profitCentreUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getProfitCentreUUID() {
        return profitCentreUUID;
    }

    /**
     * Define el valor de la propiedad profitCentreUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setProfitCentreUUID(UUID value) {
        this.profitCentreUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad costCentreID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostCentreID() {
        return costCentreID;
    }

    /**
     * Define el valor de la propiedad costCentreID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostCentreID(String value) {
        this.costCentreID = value;
    }

    /**
     * Obtiene el valor de la propiedad costCentreUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getCostCentreUUID() {
        return costCentreUUID;
    }

    /**
     * Define el valor de la propiedad costCentreUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setCostCentreUUID(UUID value) {
        this.costCentreUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad individualMaterialKey.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey }
     *     
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey getIndividualMaterialKey() {
        return individualMaterialKey;
    }

    /**
     * Define el valor de la propiedad individualMaterialKey.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey }
     *     
     */
    public void setIndividualMaterialKey(MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentIndividualMaterialKey value) {
        this.individualMaterialKey = value;
    }

    /**
     * Obtiene el valor de la propiedad individualMaterialUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getIndividualMaterialUUID() {
        return individualMaterialUUID;
    }

    /**
     * Define el valor de la propiedad individualMaterialUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setIndividualMaterialUUID(UUID value) {
        this.individualMaterialUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad projectTaskKey.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey }
     *     
     */
    public MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey getProjectTaskKey() {
        return projectTaskKey;
    }

    /**
     * Define el valor de la propiedad projectTaskKey.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey }
     *     
     */
    public void setProjectTaskKey(MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey value) {
        this.projectTaskKey = value;
    }

    /**
     * Obtiene el valor de la propiedad projectReference.
     * 
     * @return
     *     possible object is
     *     {@link ProjectReference }
     *     
     */
    public ProjectReference getProjectReference() {
        return projectReference;
    }

    /**
     * Define el valor de la propiedad projectReference.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectReference }
     *     
     */
    public void setProjectReference(ProjectReference value) {
        this.projectReference = value;
    }

    /**
     * Obtiene el valor de la propiedad projectReferenceProjectElementTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectReferenceProjectElementTypeName() {
        return projectReferenceProjectElementTypeName;
    }

    /**
     * Define el valor de la propiedad projectReferenceProjectElementTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectReferenceProjectElementTypeName(String value) {
        this.projectReferenceProjectElementTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad salesOrderReference.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public BusinessTransactionDocumentReference getSalesOrderReference() {
        return salesOrderReference;
    }

    /**
     * Define el valor de la propiedad salesOrderReference.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public void setSalesOrderReference(BusinessTransactionDocumentReference value) {
        this.salesOrderReference = value;
    }

    /**
     * Obtiene el valor de la propiedad salesOrderReferenceTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOrderReferenceTypeName() {
        return salesOrderReferenceTypeName;
    }

    /**
     * Define el valor de la propiedad salesOrderReferenceTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOrderReferenceTypeName(String value) {
        this.salesOrderReferenceTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad salesOrderReferenceItemTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesOrderReferenceItemTypeName() {
        return salesOrderReferenceItemTypeName;
    }

    /**
     * Define el valor de la propiedad salesOrderReferenceItemTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesOrderReferenceItemTypeName(String value) {
        this.salesOrderReferenceItemTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad salesOrderName.
     * 
     * @return
     *     possible object is
     *     {@link EXTENDEDName }
     *     
     */
    public EXTENDEDName getSalesOrderName() {
        return salesOrderName;
    }

    /**
     * Define el valor de la propiedad salesOrderName.
     * 
     * @param value
     *     allowed object is
     *     {@link EXTENDEDName }
     *     
     */
    public void setSalesOrderName(EXTENDEDName value) {
        this.salesOrderName = value;
    }

    /**
     * Obtiene el valor de la propiedad salesOrderItemDescription.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getSalesOrderItemDescription() {
        return salesOrderItemDescription;
    }

    /**
     * Define el valor de la propiedad salesOrderItemDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setSalesOrderItemDescription(SHORTDescription value) {
        this.salesOrderItemDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceOrderReference.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public BusinessTransactionDocumentReference getServiceOrderReference() {
        return serviceOrderReference;
    }

    /**
     * Define el valor de la propiedad serviceOrderReference.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public void setServiceOrderReference(BusinessTransactionDocumentReference value) {
        this.serviceOrderReference = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceOrderReferenceTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOrderReferenceTypeName() {
        return serviceOrderReferenceTypeName;
    }

    /**
     * Define el valor de la propiedad serviceOrderReferenceTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOrderReferenceTypeName(String value) {
        this.serviceOrderReferenceTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceOrderReferenceItemTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOrderReferenceItemTypeName() {
        return serviceOrderReferenceItemTypeName;
    }

    /**
     * Define el valor de la propiedad serviceOrderReferenceItemTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOrderReferenceItemTypeName(String value) {
        this.serviceOrderReferenceItemTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceOrderName.
     * 
     * @return
     *     possible object is
     *     {@link EXTENDEDName }
     *     
     */
    public EXTENDEDName getServiceOrderName() {
        return serviceOrderName;
    }

    /**
     * Define el valor de la propiedad serviceOrderName.
     * 
     * @param value
     *     allowed object is
     *     {@link EXTENDEDName }
     *     
     */
    public void setServiceOrderName(EXTENDEDName value) {
        this.serviceOrderName = value;
    }

    /**
     * Obtiene el valor de la propiedad serviceOrderItemDescription.
     * 
     * @return
     *     possible object is
     *     {@link SHORTDescription }
     *     
     */
    public SHORTDescription getServiceOrderItemDescription() {
        return serviceOrderItemDescription;
    }

    /**
     * Define el valor de la propiedad serviceOrderItemDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link SHORTDescription }
     *     
     */
    public void setServiceOrderItemDescription(SHORTDescription value) {
        this.serviceOrderItemDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeID.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeID }
     *     
     */
    public EmployeeID getEmployeeID() {
        return employeeID;
    }

    /**
     * Define el valor de la propiedad employeeID.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeID }
     *     
     */
    public void setEmployeeID(EmployeeID value) {
        this.employeeID = value;
    }

    /**
     * Obtiene el valor de la propiedad employeeUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getEmployeeUUID() {
        return employeeUUID;
    }

    /**
     * Define el valor de la propiedad employeeUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setEmployeeUUID(UUID value) {
        this.employeeUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad companyID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyID() {
        return companyID;
    }

    /**
     * Define el valor de la propiedad companyID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyID(String value) {
        this.companyID = value;
    }

    /**
     * Obtiene el valor de la propiedad companyUUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getCompanyUUID() {
        return companyUUID;
    }

    /**
     * Define el valor de la propiedad companyUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setCompanyUUID(UUID value) {
        this.companyUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad financialFunctionUUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialFunctionUUID() {
        return financialFunctionUUID;
    }

    /**
     * Define el valor de la propiedad financialFunctionUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialFunctionUUID(String value) {
        this.financialFunctionUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad financialFundUUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialFundUUID() {
        return financialFundUUID;
    }

    /**
     * Define el valor de la propiedad financialFundUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialFundUUID(String value) {
        this.financialFundUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad financialFunctionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialFunctionID() {
        return financialFunctionID;
    }

    /**
     * Define el valor de la propiedad financialFunctionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialFunctionID(String value) {
        this.financialFunctionID = value;
    }

    /**
     * Obtiene el valor de la propiedad financialFundID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialFundID() {
        return financialFundID;
    }

    /**
     * Define el valor de la propiedad financialFundID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialFundID(String value) {
        this.financialFundID = value;
    }

    /**
     * Obtiene el valor de la propiedad customCode1.
     * 
     * @return
     *     possible object is
     *     {@link CodingBlockCustomField1Code }
     *     
     */
    public CodingBlockCustomField1Code getCustomCode1() {
        return customCode1;
    }

    /**
     * Define el valor de la propiedad customCode1.
     * 
     * @param value
     *     allowed object is
     *     {@link CodingBlockCustomField1Code }
     *     
     */
    public void setCustomCode1(CodingBlockCustomField1Code value) {
        this.customCode1 = value;
    }

    /**
     * Obtiene el valor de la propiedad customCode2.
     * 
     * @return
     *     possible object is
     *     {@link CodingBlockCustomField2Code }
     *     
     */
    public CodingBlockCustomField2Code getCustomCode2() {
        return customCode2;
    }

    /**
     * Define el valor de la propiedad customCode2.
     * 
     * @param value
     *     allowed object is
     *     {@link CodingBlockCustomField2Code }
     *     
     */
    public void setCustomCode2(CodingBlockCustomField2Code value) {
        this.customCode2 = value;
    }

    /**
     * Obtiene el valor de la propiedad customCode3.
     * 
     * @return
     *     possible object is
     *     {@link CodingBlockCustomField3Code }
     *     
     */
    public CodingBlockCustomField3Code getCustomCode3() {
        return customCode3;
    }

    /**
     * Define el valor de la propiedad customCode3.
     * 
     * @param value
     *     allowed object is
     *     {@link CodingBlockCustomField3Code }
     *     
     */
    public void setCustomCode3(CodingBlockCustomField3Code value) {
        this.customCode3 = value;
    }

    /**
     * Obtiene el valor de la propiedad customObject1UUID.
     * 
     * @return
     *     possible object is
     *     {@link UUID }
     *     
     */
    public UUID getCustomObject1UUID() {
        return customObject1UUID;
    }

    /**
     * Define el valor de la propiedad customObject1UUID.
     * 
     * @param value
     *     allowed object is
     *     {@link UUID }
     *     
     */
    public void setCustomObject1UUID(UUID value) {
        this.customObject1UUID = value;
    }

    /**
     * Obtiene el valor de la propiedad customObject1ID.
     * 
     * @return
     *     possible object is
     *     {@link CustomObjectID }
     *     
     */
    public CustomObjectID getCustomObject1ID() {
        return customObject1ID;
    }

    /**
     * Define el valor de la propiedad customObject1ID.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomObjectID }
     *     
     */
    public void setCustomObject1ID(CustomObjectID value) {
        this.customObject1ID = value;
    }

    /**
     * Obtiene el valor de la propiedad customText1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomText1() {
        return customText1;
    }

    /**
     * Define el valor de la propiedad customText1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomText1(String value) {
        this.customText1 = value;
    }

    /**
     * Obtiene el valor de la propiedad costObjectReference.
     * 
     * @return
     *     possible object is
     *     {@link AccountingObjectCheckItemCostObjectReference }
     *     
     */
    public AccountingObjectCheckItemCostObjectReference getCostObjectReference() {
        return costObjectReference;
    }

    /**
     * Define el valor de la propiedad costObjectReference.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountingObjectCheckItemCostObjectReference }
     *     
     */
    public void setCostObjectReference(AccountingObjectCheckItemCostObjectReference value) {
        this.costObjectReference = value;
    }

    /**
     * Obtiene el valor de la propiedad grantID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrantID() {
        return grantID;
    }

    /**
     * Define el valor de la propiedad grantID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrantID(String value) {
        this.grantID = value;
    }

    /**
     * Obtiene el valor de la propiedad grantUUID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrantUUID() {
        return grantUUID;
    }

    /**
     * Define el valor de la propiedad grantUUID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrantUUID(String value) {
        this.grantUUID = value;
    }

    /**
     * Obtiene el valor de la propiedad leaseContractReference.
     * 
     * @return
     *     possible object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public BusinessTransactionDocumentReference getLeaseContractReference() {
        return leaseContractReference;
    }

    /**
     * Define el valor de la propiedad leaseContractReference.
     * 
     * @param value
     *     allowed object is
     *     {@link BusinessTransactionDocumentReference }
     *     
     */
    public void setLeaseContractReference(BusinessTransactionDocumentReference value) {
        this.leaseContractReference = value;
    }

    /**
     * Obtiene el valor de la propiedad actionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Define el valor de la propiedad actionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

}
