
package com.logistica.sap.pedidoCompra;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaskID" type="{http://sap.com/xi/AP/Common/GDT}ProjectElementID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey", namespace = "http://sap.com/xi/AP/IS/CodingBlock/Global", propOrder = {
    "taskID"
})
public class MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignmentProjectTaskKey {

    @XmlElement(name = "TaskID")
    protected ProjectElementID taskID;

    /**
     * Obtiene el valor de la propiedad taskID.
     * 
     * @return
     *     possible object is
     *     {@link ProjectElementID }
     *     
     */
    public ProjectElementID getTaskID() {
        return taskID;
    }

    /**
     * Define el valor de la propiedad taskID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectElementID }
     *     
     */
    public void setTaskID(ProjectElementID value) {
        this.taskID = value;
    }

}
