package com.logistica.dto;

import java.io.Serializable;

public class AnioMesDTO implements Serializable {

	private static final long serialVersionUID = 3688525915806724500L;

	private Integer anio;
	private MesDTO mes;

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public MesDTO getMes() {
		return mes;
	}

	public void setMes(MesDTO mes) {
		this.mes = mes;
	}

}
