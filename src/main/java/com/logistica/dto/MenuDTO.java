package com.logistica.dto;

import java.io.Serializable;
import java.util.List;

public class MenuDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idMenu;
	private Long idMenuPadre;
	private String name;
	private String link;
	private String iconclass;
	private Integer order;
	private Boolean heading;
	private List<MenuDTO> subitems;

	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public Long getIdMenuPadre() {
		return idMenuPadre;
	}

	public void setIdMenuPadre(Long idMenuPadre) {
		this.idMenuPadre = idMenuPadre;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIconclass() {
		return iconclass;
	}

	public void setIconclass(String iconclass) {
		this.iconclass = iconclass;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Boolean getHeading() {
		return heading;
	}

	public void setHeading(Boolean heading) {
		this.heading = heading;
	}

	public List<MenuDTO> getSubitems() {
		return subitems;
	}

	public void setSubitems(List<MenuDTO> subitems) {
		this.subitems = subitems;
	}

}
