package com.logistica.dto;

import java.io.Serializable;

public class MesDTO implements Serializable {

	private static final long serialVersionUID = 7857481076442331088L;

	private int idMes;
	private String nombreCorto;
	private String nombreLargo;

	public int getIdMes() {
		return idMes;
	}

	public void setIdMes(int idMes) {
		this.idMes = idMes;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getNombreLargo() {
		return nombreLargo;
	}

	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}

}
