package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tipos_cambio")
public class TipoCambio extends CommonEntity {

	private static final long serialVersionUID = -5457522228000850859L;

	@Id
	@Column(name = "id_tipo_cambio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTipoCambio;

	@Embedded
	private AnioMes anioMes;

	@OneToOne
	@JoinColumn(name = "id_moneda")
	private Moneda moneda;

	@Column(nullable = true)
	private Double valor;

	public Long getIdTipoCambio() {
		return idTipoCambio;
	}

	public void setIdTipoCambio(Long idTipoCambio) {
		this.idTipoCambio = idTipoCambio;
	}

	public AnioMes getAnioMes() {
		return anioMes;
	}

	public void setAnioMes(AnioMes anioMes) {
		this.anioMes = anioMes;
	}

	public Moneda getMoneda() {
		return moneda;
	}

	public void setMoneda(Moneda moneda) {
		this.moneda = moneda;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}
