package com.logistica.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "facturas_pago")
public class FacturaPago extends CommonEntity {

	private static final long serialVersionUID = 2282665827570507464L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pago_factura", nullable = false, length = 45)
	Long idPagoFactura;

	@Column(nullable = true, length = 50)
	private String idDocumento;

	@Column(nullable = true, length = 500)
	private String folio;

	@Column(nullable = true, length = 5)
	private String monedaPago;

	@Column(nullable = true, length = 5)
	private String serie;

	@Column(nullable = true, length = 5)
	private String formaPago;

	@Column(nullable = true, length = 100)
	private String formaPagoDescr;

	@Column(nullable = true)
	private LocalDateTime fechaPago;

	@Column(nullable = true, precision = 12, scale = 2)
	private BigDecimal impPagado;

	@Column(nullable = true)
	private String rfcEmisor;

	@Column(nullable = true)
	private String rfcReceptor;

	public Long getIdPagoFactura() {
		return idPagoFactura;
	}

	public void setIdPagoFactura(Long idPagoFactura) {
		this.idPagoFactura = idPagoFactura;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getMonedaPago() {
		return monedaPago;
	}

	public void setMonedaPago(String monedaPago) {
		this.monedaPago = monedaPago;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getFormaPagoDescr() {
		return formaPagoDescr;
	}

	public void setFormaPagoDescr(String formaPagoDescr) {
		this.formaPagoDescr = formaPagoDescr;
	}

	public LocalDateTime getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(LocalDateTime fechaPago) {
		this.fechaPago = fechaPago;
	}

	public BigDecimal getImpPagado() {
		return impPagado;
	}

	public void setImpPagado(BigDecimal impPagado) {
		this.impPagado = impPagado;
	}

	public String getRfcEmisor() {
		return rfcEmisor;
	}

	public void setRfcEmisor(String rfcEmisor) {
		this.rfcEmisor = rfcEmisor;
	}

	public String getRfcReceptor() {
		return rfcReceptor;
	}

	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}

}
