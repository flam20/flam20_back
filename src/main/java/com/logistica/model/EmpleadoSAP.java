package com.logistica.model;

public class EmpleadoSAP {
	private String unidadVentas;
	private String idEmpleado;
	private String idUser;

	public EmpleadoSAP() {
		super();

	}

	public String getUnidadVentas() {
		return unidadVentas;
	}

	public void setUnidadVentas(String unidadVentas) {
		this.unidadVentas = unidadVentas;
	}

	public String getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(String idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
}
