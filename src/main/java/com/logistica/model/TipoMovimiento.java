package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipos_movimiento")
public class TipoMovimiento extends CommonEntity {

	private static final long serialVersionUID = 3857728834129726969L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tipo_movimiento", nullable = false, unique = true)
	Long idTipoMovimiento;

	@Column(nullable = true, length = 100)
	String descripcion;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public TipoMovimiento() {
	}

	public TipoMovimiento(String descripcion) {
		this.descripcion = descripcion;
		this.borrado = false;
	}

	public Long getIdTipoMovimiento() {
		return idTipoMovimiento;
	}

	public void setIdTipoMovimiento(Long idTipoMovimiento) {
		this.idTipoMovimiento = idTipoMovimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
