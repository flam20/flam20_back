package com.logistica.model;

public enum EnumMes {

	ENERO(1, "Enero"), FEBRERO(2, "Febrero"), MARZO(3, "Marzo"), ABRIL(4, "Abril"), MAYO(5, "Mayo"), JUNIO(6, "Junio"),
	JULIO(7, "Julio"), AGOSTO(8, "Agosto"), SEPTIEMBRE(9, "Septiembre"), OCTUBRE(10, "Octubre"),
	NOVIEMBRE(11, "Noviembre"), DICIEMBRE(12, "Diciembre");

	private int idMes;
	private String descMes;

	private EnumMes(int idMes, String descMes) {
		this.idMes = idMes;
		this.descMes = descMes;
	}

	public int getIdMes() {
		return idMes;
	}

	public void setIdMes(int idMes) {
		this.idMes = idMes;
	}

	public String getDescMes() {
		return descMes;
	}

	public void setDescMes(String descMes) {
		this.descMes = descMes;
	}

	public static EnumMes getValue(int value) {
		for (EnumMes e : EnumMes.values()) {
			if (e.idMes == value) {
				return e;
			}
		}
		return null;
	}
}
