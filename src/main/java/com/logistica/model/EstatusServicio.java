package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estatus_servicio")
public class EstatusServicio extends CommonEntity {

	private static final long serialVersionUID = 5411208359446377237L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estatus_servicio", nullable = false, unique = true)
	Long idEstatusServicio;

	@Column(nullable = true, length = 100)
	String descripcion;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public EstatusServicio() {
	}

	public EstatusServicio(String descripcion) {
		this.descripcion = descripcion;
		this.borrado = false;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	public Long getIdEstatusServicio() {
		return idEstatusServicio;
	}

	public void setIdEstatusServicio(Long idEstatusServicio) {
		this.idEstatusServicio = idEstatusServicio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

}
