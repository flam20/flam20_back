package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cargos")
public class Cargo extends CommonEntity {

	private static final long serialVersionUID = 6162989152557333345L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cargo", nullable = false, unique = true)
	Long idCargo;

	@Column(nullable = true, length = 15)
	String descCorta;

	@Column(nullable = true, length = 100)
	String descLarga;

	@Column(nullable = true)
	Boolean borrado;

	@Column(nullable = false, length = 4)
	String posCtoDls;

	@Column(nullable = false, length = 4)
	String posVtaDls;

	@Column(nullable = false, length = 4)
	String posCtoMxn;

	@Column(nullable = false, length = 4)
	String posVtaMxn;

	@Embedded
	private ControlAcceso controlAcceso;

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public String getDescCorta() {
		return descCorta;
	}

	public void setDescCorta(String descCorta) {
		this.descCorta = descCorta;
	}

	public String getDescLarga() {
		return descLarga;
	}

	public void setDescLarga(String descLarga) {
		this.descLarga = descLarga;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public String getPosCtoDls() {
		return posCtoDls;
	}

	public void setPosCtoDls(String posCtoDls) {
		this.posCtoDls = posCtoDls;
	}

	public String getPosVtaDls() {
		return posVtaDls;
	}

	public void setPosVtaDls(String posVtaDls) {
		this.posVtaDls = posVtaDls;
	}

	public String getPosCtoMxn() {
		return posCtoMxn;
	}

	public void setPosCtoMxn(String posCtoMxn) {
		this.posCtoMxn = posCtoMxn;
	}

	public String getPosVtaMxn() {
		return posVtaMxn;
	}

	public void setPosVtaMxn(String posVtaMxn) {
		this.posVtaMxn = posVtaMxn;
	}
	
	public String getDescripcionCombinada() {
		return this.descLarga + " (" + this.descCorta + ")";
	}
}
