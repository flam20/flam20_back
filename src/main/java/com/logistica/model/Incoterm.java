package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "incoterms")
public class Incoterm extends CommonEntity {

	private static final long serialVersionUID = -7689835745099849570L;

	@Id
	@Column(name = "id_incoterm")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idIncoterm;

	@Column(nullable = true, length = 100)
	String descripcion;

	@Column(nullable = false, length = 3)
	String iniciales;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdIncoterm() {
		return idIncoterm;
	}

	public void setIdIncoterm(Long idIncoterm) {
		this.idIncoterm = idIncoterm;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIniciales() {
		return iniciales;
	}

	public void setIniciales(String iniciales) {
		this.iniciales = iniciales;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
