package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "meses")
public class Mes extends CommonEntity {

	private static final long serialVersionUID = 1872908199320485946L;

	@Id
	@Column(name = "id_mes")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMes;

	@Column(nullable = false)
	private String nombreCorto;

	@Column(nullable = false)
	private String nombreLargo;

	public Long getIdMes() {
		return idMes;
	}

	public void setIdMes(Long idMes) {
		this.idMes = idMes;
	}

	public String getNombreCorto() {
		return nombreCorto;
	}

	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	public String getNombreLargo() {
		return nombreLargo;
	}

	public void setNombreLargo(String nombreLargo) {
		this.nombreLargo = nombreLargo;
	}

}
