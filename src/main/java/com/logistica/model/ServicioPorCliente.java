package com.logistica.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "servicios_cliente")
public class ServicioPorCliente extends CommonEntity implements Comparable<ServicioPorCliente> {

	private static final long serialVersionUID = -943401009744938780L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_servicio_cliente", nullable = false)
	Long idServicioCliente;

	@OneToOne
	@JoinColumn(name = "id_tipo_ruta")
	TipoRuta tipoRuta;

	@Column(nullable = false)
	Integer orden;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal costo;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal venta;

	@OneToOne
	@JoinColumn(name = "id_moneda_costo")
	Moneda monedaCosto;

	@OneToOne
	@JoinColumn(name = "id_moneda_venta")
	Moneda monedaVenta;

	@Column(nullable = true, length = 45)
	String numFactura;

	@Column(nullable = true)
	//@JsonFormat(pattern="yyyy-MM-dd", timezone="UTC")
	LocalDate fechaFactura;

	@Column(nullable = true)
	//@JsonFormat(timezone="GMT+8")
	//@JsonDeserialize(using=com.logistica.util.LocalDateDeserializer.class)
	LocalDate fechaCarga;

	@Column(nullable = true)
	//@JsonFormat(pattern="yyyy-MM-dd", timezone="UTC")
	LocalDate fechaAgenteAduanal;

	@Column(nullable = true)
	//@JsonFormat(pattern="yyyy-MM-dd", timezone="UTC")
	LocalDate fechaCruce;

	@Column(nullable = true)
	//@JsonFormat(pattern="yyyy-MM-dd", timezone="UTC")
	LocalDate fechaDestino;

	@Column(nullable = true, length = 500)
	String referenciaCliente;

	@Column(nullable = true, precision = 12, scale = 2)
	Long podId;

	@Column(nullable = true)
	boolean entregado;

	@OneToOne(optional = true)
	@JoinColumn(name = "id_tipo_movimiento")
	TipoMovimiento tipoMovimiento;

	@Column(nullable = true, length = 45)
	String numEquipoTipoMovimiento;

	@OneToOne(optional = true)
	@JoinColumn(name = "id_tipo_equipo")
	TipoEquipo tipoEquipo;

	@Column(nullable = true, length = 45)
	String numEquipoTipoEquipo;

	@Column(nullable = true, length = 500)
	String comentarios;

	@OneToOne
	@JoinColumn(name = "id_archivo_pod")
	ArchivoBox archivoPod;

	@Column(nullable = true, length = 100)
	String consignatario;

	@OneToOne
	@JoinColumn(name = "id_proveedor")
	Proveedor proveedor;

	@OneToOne
	@JoinColumn(name = "id_ubicacion")
	Ubicacion ubicacion;

	@Column(nullable = true)
	String pedidoCliente;

	@Column(nullable = true, precision = 12, scale = 2)
	Long crmId;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdServicioCliente() {
		return idServicioCliente;
	}

	public void setIdServicioCliente(Long idServicioCliente) {
		this.idServicioCliente = idServicioCliente;
	}

	public TipoRuta getTipoRuta() {
		return tipoRuta;
	}

	public void setTipoRuta(TipoRuta tipoRuta) {
		this.tipoRuta = tipoRuta;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public BigDecimal getCosto() {
		return costo;
	}

	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}

	public BigDecimal getVenta() {
		return venta;
	}

	public void setVenta(BigDecimal venta) {
		this.venta = venta;
	}

	public Moneda getMonedaCosto() {
		return monedaCosto;
	}

	public void setMonedaCosto(Moneda monedaCosto) {
		this.monedaCosto = monedaCosto;
	}

	public Moneda getMonedaVenta() {
		return monedaVenta;
	}

	public void setMonedaVenta(Moneda monedaVenta) {
		this.monedaVenta = monedaVenta;
	}

	public String getNumFactura() {
		return numFactura;
	}

	public void setNumFactura(String numFactura) {
		this.numFactura = numFactura;
	}

	public LocalDate getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(LocalDate fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public LocalDate getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(LocalDate fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public LocalDate getFechaAgenteAduanal() {
		return fechaAgenteAduanal;
	}

	public void setFechaAgenteAduanal(LocalDate fechaAgenteAduanal) {
		this.fechaAgenteAduanal = fechaAgenteAduanal;
	}

	public LocalDate getFechaCruce() {
		return fechaCruce;
	}

	public void setFechaCruce(LocalDate fechaCruce) {
		this.fechaCruce = fechaCruce;
	}

	public LocalDate getFechaDestino() {
		return fechaDestino;
	}

	public void setFechaDestino(LocalDate fechaDestino) {
		this.fechaDestino = fechaDestino;
	}

	public String getReferenciaCliente() {
		return referenciaCliente;
	}

	public void setReferenciaCliente(String referenciaCliente) {
		this.referenciaCliente = referenciaCliente;
	}

	public Long getPodId() {
		return podId;
	}

	public void setPodId(Long podId) {
		this.podId = podId;
	}

	public boolean isEntregado() {
		return entregado;
	}

	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}

	public TipoMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

	public String getNumEquipoTipoMovimiento() {
		return numEquipoTipoMovimiento;
	}

	public void setNumEquipoTipoMovimiento(String numEquipoTipoMovimiento) {
		this.numEquipoTipoMovimiento = numEquipoTipoMovimiento;
	}

	public TipoEquipo getTipoEquipo() {
		return tipoEquipo;
	}

	public void setTipoEquipo(TipoEquipo tipoEquipo) {
		this.tipoEquipo = tipoEquipo;
	}

	public String getNumEquipoTipoEquipo() {
		return numEquipoTipoEquipo;
	}

	public void setNumEquipoTipoEquipo(String numEquipoTipoEquipo) {
		this.numEquipoTipoEquipo = numEquipoTipoEquipo;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public String getConsignatario() {
		return consignatario;
	}

	public void setConsignatario(String consignatario) {
		this.consignatario = consignatario;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getPedidoCliente() {
		return pedidoCliente;
	}

	public void setPedidoCliente(String pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}

	public Long getCrmId() {
		return crmId;
	}

	public void setCrmId(Long crmId) {
		this.crmId = crmId;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	@Override
	public int compareTo(ServicioPorCliente o) {
		return this.getTipoRuta().getIdTipoRuta().compareTo(o.getTipoRuta().getIdTipoRuta());
	}

	public ArchivoBox getArchivoPod() {
		return archivoPod;
	}

	public void setArchivoPod(ArchivoBox archivoPod) {
		this.archivoPod = archivoPod;
	}

}
