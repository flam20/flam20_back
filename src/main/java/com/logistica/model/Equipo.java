package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "equipos")
public class Equipo extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_equipo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEquipo;

	@Column(nullable = true)
	private String descripcion;

	@Column(nullable = true)
	private String comentarios;

	@Column(nullable = true)
	private Integer cantidadSeco;

	@Column(nullable = true)
	private Integer cantidadRefrigerado;

	@Column(nullable = true)
	private Integer cantidadPeligroso;

	public Equipo() {
		super();
	}

	public Equipo(String descripcion, Integer cantidadSeco, Integer cantidadRefrigerado, Integer cantidadPeligroso,
			String comentarios) {
		super();
		this.descripcion = descripcion;
		this.cantidadSeco = cantidadSeco;
		this.cantidadRefrigerado = cantidadRefrigerado;
		this.cantidadPeligroso = cantidadPeligroso;
		this.comentarios = comentarios;
	}

	public Long getIdEquipo() {
		return idEquipo;
	}

	public void setIdEquipo(Long idEquipo) {
		this.idEquipo = idEquipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Integer getCantidadSeco() {
		return cantidadSeco;
	}

	public void setCantidadSeco(Integer cantidadSeco) {
		this.cantidadSeco = cantidadSeco;
	}

	public Integer getCantidadRefrigerado() {
		return cantidadRefrigerado;
	}

	public void setCantidadRefrigerado(Integer cantidadRefrigerado) {
		this.cantidadRefrigerado = cantidadRefrigerado;
	}

	public Integer getCantidadPeligroso() {
		return cantidadPeligroso;
	}

	public void setCantidadPeligroso(Integer cantidadPeligroso) {
		this.cantidadPeligroso = cantidadPeligroso;
	}

}
