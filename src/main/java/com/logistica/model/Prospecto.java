package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "prospectos")
public class Prospecto extends CommonEntity {

	private static final long serialVersionUID = 3991907493828450890L;

	@Id
	@Column(name = "id_prospecto")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProspecto;

	@Column(nullable = true, length = 100)
	private String razonSocial;

	@Column(nullable = true, length = 100)
	private String razonComercial;

	@Column(nullable = true, length = 15)
	private Long idEvaluacion;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdProspecto() {
		return idProspecto;
	}

	public void setIdProspecto(Long idProspecto) {
		this.idProspecto = idProspecto;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRazonComercial() {
		return razonComercial;
	}

	public void setRazonComercial(String razonComercial) {
		this.razonComercial = razonComercial;
	}

	public Long getIdEvaluacion() {
		return idEvaluacion;
	}

	public void setIdEvaluacion(Long idEvaluacion) {
		this.idEvaluacion = idEvaluacion;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
