package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "menus")
public class Menu extends CommonEntity implements Comparable<Menu>{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_menu")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMenu;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_menu_padre")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Menu menuPadre;

	@Column(unique = true)
	private String nombre;
	
	@Column(unique = true)
	private String url;
	
	@Column(unique = true)
	private String icono;
	
	@Column(unique = true)
	private Integer orden;
	
	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Menu getMenuPadre() {
		return menuPadre;
	}

	public void setMenuPadre(Menu menuPadre) {
		this.menuPadre = menuPadre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	@Override
	public int compareTo(Menu o) {
		int ordenComp = this.getOrden().compareTo(o.getOrden());
		
		if(ordenComp != 0) {
			return ordenComp;
		}
		
		return this.getNombre().compareTo(o.getNombre());
	}

}