package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "aerolineas")
public class Aerolinea extends CommonEntity {

	private static final long serialVersionUID = -6456578098584488836L;

	@Id
	@Column(name = "id_aerolinea")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idAerolinea;

	@Column(nullable = true, length = 15)
	String prefijo;

	@Column(nullable = true, length = 50)
	String nombre;

	@Column(nullable = true, length = 15)
	String territorio;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdAerolinea() {
		return idAerolinea;
	}

	public void setIdAerolinea(Long idAerolinea) {
		this.idAerolinea = idAerolinea;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTerritorio() {
		return territorio;
	}

	public void setTerritorio(String territorio) {
		this.territorio = territorio;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
