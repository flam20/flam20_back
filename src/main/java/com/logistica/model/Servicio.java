package com.logistica.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.cartaporte.CartaPorte;

@Entity
@Table(name = "servicios")
public class Servicio extends CommonEntity {

	private static final long serialVersionUID = 5184504868420221502L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_servicio", nullable = false)
	Long idServicio;

	@Column(nullable = true)
	String idServicioStr;

	@Column(nullable = true)
	Integer anio;

	@Column(nullable = true)
	Integer consecutivo;

	@Column(nullable = true)
	Boolean borrador;

	@Column(nullable = true)
	LocalDate fechaServicio;
	
	@Column(nullable = true)
	LocalDate fechaCierre;

	@OneToOne
	@JoinColumn(name = "id_mpc")
	Mpc mpc;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<ServicioPorCliente> serviciosPorCliente;

	@OneToOne
	@JoinColumn(name = "id_cliente")
	Cliente cliente;

	@OneToOne
	@JoinColumn(name = "id_pagador")
	Cliente pagador;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<CargoPorServicio> cargosPorServicio;

	@Column(name = "id_usuario", nullable = false, length = 50)
	String trafico;

	@Column(name = "nombre_usuario", nullable = true)
	String nombreTrafico;

	@OneToOne
	@JoinColumn(name = "id_estatus_servicio")
	EstatusServicio estatus;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal costoServicioUsd;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal ventaServicioUsd;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal costoCargoUsd;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal ventaCargoUsd;

	@Column(nullable = true, length = 500)
	String notas;

	@Column(nullable = true, length = 500)
	String motivoCancelacion;

	@Column(nullable = true, length = 5)
	Boolean notasCompletas;

	@Column(nullable = true, length = 50)
	String noFacturaCliente;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	List<Factura> facturas;

	@Column(nullable = true, length = 500)
	String estatusBox;

	@Column(nullable = true, length = 500)
	String estatusSat;

	@Column(nullable = true)
	String ordenCompra;

	@Column(nullable = true)
	Boolean deCrm;

	@Column(nullable = true)
	String ofertaCrm;

	@OneToOne
	@JoinColumn(name = "id_carta_porte")
	CartaPorte cartaPorte;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<BitacoraSap> bitacoras;

	@Column(nullable = true)
	String idChat;

	@Embedded
	private ControlAcceso controlAcceso;

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getIdServicioStr() {
		return idServicioStr;
	}

	public void setIdServicioStr(String idServicioStr) {
		this.idServicioStr = idServicioStr;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}

	public Boolean getBorrador() {
		return borrador;
	}

	public void setBorrador(Boolean borrador) {
		this.borrador = borrador;
	}

	public LocalDate getFechaServicio() {
		return fechaServicio;
	}

	public void setFechaServicio(LocalDate fechaServicio) {
		this.fechaServicio = fechaServicio;
	}

	public Mpc getMpc() {
		return mpc;
	}

	public void setMpc(Mpc mpc) {
		this.mpc = mpc;
	}

	public List<ServicioPorCliente> getServiciosPorCliente() {
		return serviciosPorCliente;
	}

	public void setServiciosPorCliente(List<ServicioPorCliente> serviciosPorCliente) {
		this.serviciosPorCliente = serviciosPorCliente;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<CargoPorServicio> getCargosPorServicio() {
		return cargosPorServicio;
	}

	public void setCargosPorServicio(List<CargoPorServicio> cargosPorServicio) {
		this.cargosPorServicio = cargosPorServicio;
	}

	public String getTrafico() {
		return trafico;
	}

	public void setTrafico(String trafico) {
		this.trafico = trafico;
	}

	public EstatusServicio getEstatus() {
		return estatus;
	}

	public void setEstatus(EstatusServicio estatus) {
		this.estatus = estatus;
	}

	public BigDecimal getCostoServicioUsd() {
		return costoServicioUsd;
	}

	public void setCostoServicioUsd(BigDecimal costoServicioUsd) {
		this.costoServicioUsd = costoServicioUsd;
	}

	public BigDecimal getVentaServicioUsd() {
		return ventaServicioUsd;
	}

	public void setVentaServicioUsd(BigDecimal ventaServicioUsd) {
		this.ventaServicioUsd = ventaServicioUsd;
	}

	public BigDecimal getCostoCargoUsd() {
		return costoCargoUsd;
	}

	public void setCostoCargoUsd(BigDecimal costoCargoUsd) {
		this.costoCargoUsd = costoCargoUsd;
	}

	public BigDecimal getVentaCargoUsd() {
		return ventaCargoUsd;
	}

	public void setVentaCargoUsd(BigDecimal ventaCargoUsd) {
		this.ventaCargoUsd = ventaCargoUsd;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public Boolean getNotasCompletas() {
		return notasCompletas;
	}

	public void setNotasCompletas(Boolean notasCompletas) {
		this.notasCompletas = notasCompletas;
	}

	public String getNoFacturaCliente() {
		return noFacturaCliente;
	}

	public void setNoFacturaCliente(String noFacturaCliente) {
		this.noFacturaCliente = noFacturaCliente;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}

	public String getEstatusBox() {
		return estatusBox;
	}

	public void setEstatusBox(String estatusBox) {
		this.estatusBox = estatusBox;
	}

	public String getEstatusSat() {
		return estatusSat;
	}

	public void setEstatusSat(String estatusSat) {
		this.estatusSat = estatusSat;
	}

	public String getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public Boolean getDeCrm() {
		return deCrm;
	}

	public void setDeCrm(Boolean deCrm) {
		this.deCrm = deCrm;
	}

	public String getOfertaCrm() {
		return ofertaCrm;
	}

	public void setOfertaCrm(String ofertaCrm) {
		this.ofertaCrm = ofertaCrm;
	}

	public CartaPorte getCartaPorte() {
		return cartaPorte;
	}

	public void setCartaPorte(CartaPorte cartaPorte) {
		this.cartaPorte = cartaPorte;
	}

	public List<BitacoraSap> getBitacoras() {
		return bitacoras;
	}

	public void setBitacoras(List<BitacoraSap> bitacoras) {
		this.bitacoras = bitacoras;
	}

	public String getIdChat() {
		return idChat;
	}

	public void setIdChat(String idChat) {
		this.idChat = idChat;
	}

	public String getNombreTrafico() {
		return nombreTrafico;
	}

	public void setNombreTrafico(String nombreTrafico) {
		this.nombreTrafico = nombreTrafico;
	}

	public Cliente getPagador() {
		return pagador;
	}

	public void setPagador(Cliente pagador) {
		this.pagador = pagador;
	}

	public LocalDate getFechaCierre() {
		return fechaCierre;
	}

	public void setFechaCierre(LocalDate fechaCierre) {
		this.fechaCierre = fechaCierre;
	}
	
	

}
