package com.logistica.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cargos_origen")
public class CargoOrigen extends CommonEntity {

	private static final long serialVersionUID = 971402374494646473L;

	@Id
	@Column(name = "id_cargo_origen")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idCargoOrigen;

	@OneToOne
	@JoinColumn(name = "id_cargo_aereo")
	CargoAereo cargoAereo;

	@Column(nullable = true)
	BigDecimal compraTarifa;

	@Column(nullable = true)
	BigDecimal compraMonto;

	@Column(nullable = true)
	BigDecimal ventaTarifa;

	@Column(nullable = true)
	BigDecimal ventaMonto;

	@Column(nullable = true)
	BigDecimal profit;

	@OneToOne
	@JoinColumn(name = "id_moneda_costo")
	Moneda monedaCosto;

	@OneToOne
	@JoinColumn(name = "id_moneda_venta")
	Moneda monedaVenta;

	@OneToOne
	@JoinColumn(name = "id_proveedor")
	Proveedor proveedor;

	@Column(nullable = true)
	Boolean borrado;

	@Column(nullable = true)
	Long itemID;

	@Column(nullable = true)
	String pedidoCliente;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdCargoOrigen() {
		return idCargoOrigen;
	}

	public void setIdCargoOrigen(Long idCargoOrigen) {
		this.idCargoOrigen = idCargoOrigen;
	}

	public CargoAereo getCargoAereo() {
		return cargoAereo;
	}

	public void setCargoAereo(CargoAereo cargoAereo) {
		this.cargoAereo = cargoAereo;
	}

	public BigDecimal getCompraTarifa() {
		return compraTarifa;
	}

	public void setCompraTarifa(BigDecimal compraTarifa) {
		this.compraTarifa = compraTarifa;
	}

	public BigDecimal getCompraMonto() {
		return compraMonto;
	}

	public void setCompraMonto(BigDecimal compraMonto) {
		this.compraMonto = compraMonto;
	}

	public BigDecimal getVentaTarifa() {
		return ventaTarifa;
	}

	public void setVentaTarifa(BigDecimal ventaTarifa) {
		this.ventaTarifa = ventaTarifa;
	}

	public BigDecimal getVentaMonto() {
		return ventaMonto;
	}

	public void setVentaMonto(BigDecimal ventaMonto) {
		this.ventaMonto = ventaMonto;
	}

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public Moneda getMonedaCosto() {
		return monedaCosto;
	}

	public void setMonedaCosto(Moneda monedaCosto) {
		this.monedaCosto = monedaCosto;
	}

	public Moneda getMonedaVenta() {
		return monedaVenta;
	}

	public void setMonedaVenta(Moneda monedaVenta) {
		this.monedaVenta = monedaVenta;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public Long getItemID() {
		return itemID;
	}

	public void setItemID(Long itemID) {
		this.itemID = itemID;
	}

	public String getPedidoCliente() {
		return pedidoCliente;
	}

	public void setPedidoCliente(String pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
