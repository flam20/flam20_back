package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ejecutivos")
public class Ejecutivo extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_ejecutivo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEjecutivo;

	@Column(length = 45)
	private String nombre;

	@Column(length = 45)
	private String apellidoPaterno;

	@Column(length = 45)
	private String apellidoMaterno;

	@Column(length = 255)
	private String correoElectronico;

	@Column(nullable = true, length = 20)
	String unidadVentasSAP;

	@Column(nullable = true)
	Integer idUsuarioSAP;

	@Column(nullable = true)
	String idTeams;
	
	public Long getIdEjecutivo() {
		return idEjecutivo;
	}

	public void setIdEjecutivo(Long idEjecutivo) {
		this.idEjecutivo = idEjecutivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getUnidadVentasSAP() {
		return unidadVentasSAP;
	}

	public void setUnidadVentasSAP(String unidadVentasSAP) {
		this.unidadVentasSAP = unidadVentasSAP;
	}

	public Integer getIdUsuarioSAP() {
		return idUsuarioSAP;
	}

	public void setIdUsuarioSAP(Integer idUsuarioSAP) {
		this.idUsuarioSAP = idUsuarioSAP;
	}
	
	public String getNombreCompleto() {
		return this.nombre+" "+this.getApellidoPaterno();
	}

	public String getIdTeams() {
		return idTeams;
	}

	public void setIdTeams(String idTeams) {
		this.idTeams = idTeams;
	}

}
