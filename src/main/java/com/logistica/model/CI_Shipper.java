package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "ci_shippers")
public class CI_Shipper extends CommonEntity {

	private static final long serialVersionUID = 7977089757834443313L;

	@Id
	@Column(name = "id_shipper")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idShipper;

	@Column(nullable = true, length = 100)
	String nombre;

	@Column(nullable = true, length = 15)
	String rfc;
	
	@Column(nullable = true, length = 100)
	String contacto;
	
	@Column(nullable = true, length = 100)
	String telefono;

	@Column(nullable = true)
	Boolean origen;
	
	/* DIRECCION */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pais")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Pais pais;
	
	@Column(nullable = true, length = 100)
	String estado;
	
	@Column(nullable = true, length = 100)
	String localidad;
	
	@Column(nullable = true, length = 100)
	String municipio;
	
	@Column(nullable = true, length = 100)
	String colonia;
	
	@Column(nullable = true, length = 100)
	String calle;
	
	@Column(nullable = true, length = 20)
	String numExt;
	
	@Column(nullable = true, length = 20)
	String numInt;
	
	@Column(nullable = true, length = 5)
	String codigoPostal;

	public Long getIdShipper() {
		return idShipper;
	}

	public void setIdShipper(Long idShipper) {
		this.idShipper = idShipper;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Boolean getOrigen() {
		return origen;
	}

	public void setOrigen(Boolean origen) {
		this.origen = origen;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumExt() {
		return numExt;
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}

	public String getNumInt() {
		return numInt;
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
		

}

