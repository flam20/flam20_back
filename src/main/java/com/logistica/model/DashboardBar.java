package com.logistica.model;

import java.util.List;

public class DashboardBar {
	String estatusServicio;
	List<DashboardBarMonth> meses;
	
	public String getEstatusServicio() {
		return estatusServicio;
	}
	public void setEstatusServicio(String estatusServicio) {
		this.estatusServicio = estatusServicio;
	}
	public List<DashboardBarMonth> getMeses() {
		return meses;
	}
	public void setMeses(List<DashboardBarMonth> meses) {
		this.meses = meses;
	}
	public DashboardBar(String estatusServicio) {
		super();
		this.estatusServicio = estatusServicio;
	}
	
	
	
}
