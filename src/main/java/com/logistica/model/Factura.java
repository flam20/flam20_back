package com.logistica.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "facturas")
public class Factura extends CommonEntity {

	private static final long serialVersionUID = 1774246920343002945L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura", nullable = false, length = 45)
	Long idFactura;

	@Column(nullable = false, length = 45)
	private String id;

	@Column(nullable = true)
	private String nombreArchivo;

	@Transient
	private String facturaLabel;

	@Column(nullable = true, length = 2000)
	private String xmlUrlDownload;

	@Column(nullable = true, length = 2000)
	private String xmlUrl;

	@Column(nullable = true, length = 2000)
	private String pdfUrlDownload;

	@Column(nullable = true, length = 2000)
	private String pdfUrl;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal subtotal;

	@Column(nullable = true, precision = 12, scale = 2)
	BigDecimal total;

	@Column(nullable = false, length = 45)
	Long idServicio;

	// DATOS EMISOR
	@Column(nullable = true)
	private String emisorRfc;

	@Column(nullable = true)
	private String emisorNombre;

	@Column(nullable = true)
	private String emisorCP;

	// DATOS RECEPTOR
	@Column(nullable = true)
	private String receptorRfc;

	@Transient
	private String receptorNombre;

	@Transient
	private String usoCfdi;

	@Transient
	private String servicioFlam;

	public String getUsoCfdi() {
		return usoCfdi;
	}

	public void setUsoCfdi(String usoCfdi) {
		this.usoCfdi = usoCfdi;
	}

	// DATOS FACTURA
	@Column(nullable = true)
	private LocalDateTime fecha;

	@Column(nullable = true)
	private String folio;

	@Column(nullable = true)
	private String moneda;

	@Column(nullable = true)
	private String timbreFiscalUUID;

	@Column(nullable = true)
	private String estatusFactura;

	@Column(nullable = true, length = 500)
	private String satCodigo;

	@Column(nullable = true, length = 500)
	private String satEstado;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<Concepto> conceptos;

	// Columna para pago cliente o proveedor
	@Column(nullable = true, length = 10)
	String tipoPago;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<FacturaPago> pagos;

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getFacturaLabel() {
		return facturaLabel;
	}

	public void setFacturaLabel(String facturaLabel) {
		this.facturaLabel = facturaLabel;
	}

	public String getXmlUrlDownload() {
		return xmlUrlDownload;
	}

	public void setXmlUrlDownload(String xmlUrlDownload) {
		this.xmlUrlDownload = xmlUrlDownload;
	}

	public String getXmlUrl() {
		return xmlUrl;
	}

	public void setXmlUrl(String xmlUrl) {
		this.xmlUrl = xmlUrl;
	}

	public String getPdfUrlDownload() {
		return pdfUrlDownload;
	}

	public void setPdfUrlDownload(String pdfUrlDownload) {
		this.pdfUrlDownload = pdfUrlDownload;
	}

	public String getPdfUrl() {
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getEmisorRfc() {
		return emisorRfc;
	}

	public void setEmisorRfc(String emisorRfc) {
		this.emisorRfc = emisorRfc;
	}

	public String getEmisorNombre() {
		return emisorNombre;
	}

	public void setEmisorNombre(String emisorNombre) {
		this.emisorNombre = emisorNombre;
	}

	public String getEmisorCP() {
		return emisorCP;
	}

	public void setEmisorCP(String emisorCP) {
		this.emisorCP = emisorCP;
	}

	public String getReceptorRfc() {
		return receptorRfc;
	}

	public void setReceptorRfc(String receptorRfc) {
		this.receptorRfc = receptorRfc;
	}

	public String getReceptorNombre() {
		return receptorNombre;
	}

	public void setReceptorNombre(String receptorNombre) {
		this.receptorNombre = receptorNombre;
	}

	public String getServicioFlam() {
		return servicioFlam;
	}

	public void setServicioFlam(String servicioFlam) {
		this.servicioFlam = servicioFlam;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getTimbreFiscalUUID() {
		return timbreFiscalUUID;
	}

	public void setTimbreFiscalUUID(String timbreFiscalUUID) {
		this.timbreFiscalUUID = timbreFiscalUUID;
	}

	public String getEstatusFactura() {
		return estatusFactura;
	}

	public void setEstatusFactura(String estatusFactura) {
		this.estatusFactura = estatusFactura;
	}

	public String getSatCodigo() {
		return satCodigo;
	}

	public void setSatCodigo(String satCodigo) {
		this.satCodigo = satCodigo;
	}

	public String getSatEstado() {
		return satEstado;
	}

	public void setSatEstado(String satEstado) {
		this.satEstado = satEstado;
	}

	public List<Concepto> getConceptos() {
		return conceptos;
	}

	public void setConceptos(List<Concepto> conceptos) {
		this.conceptos = conceptos;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public List<FacturaPago> getPagos() {
		return pagos;
	}

	public void setPagos(List<FacturaPago> pagos) {
		this.pagos = pagos;
	}

}
