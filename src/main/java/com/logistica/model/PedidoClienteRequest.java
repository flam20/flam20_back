package com.logistica.model;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PedidoClienteRequest {
	private static final long serialVersionUID = 833041600253014866L;

	private String actionCode;
	private String pedidoCliente;

	private String currency;
	private String buyerId;
	private String requestName;
	private String billTo;
	private String accountId;
	private String employeeId;
	private String salesUnit;
	private String pricingTermsCurrency;
	private String deliveryTermsPriority;
	private String notasGenerales;
	private String referenciaFactura;
	private String ubicacionDestino;
	private String ubicacionOrigen;
	private String ubicacionCruce;
	private String guiaHouse;
	private String guíaMaster;
	private String numeroEquipoCarga;
	private String pzasPBPC;
	private String shipper;
	private String tipoEquipo;
	private String requierePOD;
	private Date fechaCarga;
	private Date fechaCruce;
	private Date fechaDestino;
	private Date fechaAgente;

	private String noServicio;
	private String consignatarioOrigen;
	private String consignatarioCruce;
	private String consignatarioDestino;
}
