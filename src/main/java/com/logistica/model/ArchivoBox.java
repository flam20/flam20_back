package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "archivos_box")
public class ArchivoBox extends CommonEntity {

	private static final long serialVersionUID = 7245243914799311587L;

	@Id
	@Column(name = "id_archivo_box")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idArchivoBox;

	@Column(nullable = true)
	Long idServicio;

	@Column(nullable = false)
	private String fileName;

	@Column(nullable = true)
	private String url;

	@Column(nullable = true)
	private String downloadUrl;

	public ArchivoBox() {
		super();
	}

	public ArchivoBox(Long idServicio, String fileName, String url, String downloadUrl) {
		super();
		this.idServicio = idServicio;
		this.fileName = fileName;
		this.url = url;
		this.downloadUrl = downloadUrl;
	}

	public Long getIdArchivoBox() {
		return idArchivoBox;
	}

	public void setIdArchivoBox(Long idArchivoBox) {
		this.idArchivoBox = idArchivoBox;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
