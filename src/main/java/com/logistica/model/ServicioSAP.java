package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ServicioSAP")
public class ServicioSAP extends CommonEntity {
	private static final long serialVersionUID = -6456578098584488836L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, length = 45)
	Long idServicioSAP;

	@Column(nullable = true)
	private String idServicio;

	@Column(nullable = true)
	private String tipoServicio;

	@Column(nullable = true)
	private String descripcionServicio;

	@Column(nullable = true)
	private String idCategoriaProducto;

	@Column(nullable = true)
	private String descripcionCategoriaProducto;

	@Column(nullable = true)
	private String cargoFlam;

	public ServicioSAP() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServicioSAP(Long idServicioSAP, String idServicio, String tipoServicio, String descripcionServicio,
			String idCategoriaProducto, String descripcionCategoriaProducto, String cargoFlam) {
		super();
		this.idServicioSAP = idServicioSAP;
		this.idServicio = idServicio;
		this.tipoServicio = tipoServicio;
		this.descripcionServicio = descripcionServicio;
		this.idCategoriaProducto = idCategoriaProducto;
		this.descripcionCategoriaProducto = descripcionCategoriaProducto;
		this.cargoFlam = cargoFlam;
	}

	public Long getIdServicioSAP() {
		return idServicioSAP;
	}

	public void setIdServicioSAP(Long idServicioSAP) {
		this.idServicioSAP = idServicioSAP;
	}

	public String getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(String idServicio) {
		this.idServicio = idServicio;
	}

	public String getTipoServicio() {
		return tipoServicio;
	}

	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public String getDescripcionServicio() {
		return descripcionServicio;
	}

	public void setDescripcionServicio(String descripcionServicio) {
		this.descripcionServicio = descripcionServicio;
	}

	public String getIdCategoriaProducto() {
		return idCategoriaProducto;
	}

	public void setIdCategoriaProducto(String idCategoriaProducto) {
		this.idCategoriaProducto = idCategoriaProducto;
	}

	public String getDescripcionCategoriaProducto() {
		return descripcionCategoriaProducto;
	}

	public void setDescripcionCategoriaProducto(String descripcionCategoriaProducto) {
		this.descripcionCategoriaProducto = descripcionCategoriaProducto;
	}

	public String getCargoFlam() {
		return cargoFlam;
	}

	public void setCargoFlam(String cargoFlam) {
		this.cargoFlam = cargoFlam;
	}

}
