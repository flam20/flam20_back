package com.logistica.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cargos_servicio")
public class CargoPorServicio extends CommonEntity {

	private static final long serialVersionUID = -5114825149176424904L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cargo_servicio", nullable = false)
	Long idCargoServicio;

	@OneToOne
	@JoinColumn(name = "id_proveedor")
	Proveedor proveedor;

	@OneToOne
	@JoinColumn(name = "id_cargo")
	Cargo cargo;

	@OneToOne
	@JoinColumn(name = "id_moneda_costo")
	Moneda monedaCosto;

	@Column(nullable = true)
	BigDecimal costo;

	@OneToOne
	@JoinColumn(name = "id_moneda_venta")
	Moneda monedaVenta;

	@Column(nullable = true)
	BigDecimal venta;

	@Column(nullable = true)
	String observaciones;

	@Embedded
	Direccion direccionStop;

	@Column(nullable = true)
	String consignatarioStop;

	@Column(nullable = true)
	Long itemId;

	@Column(nullable = true)
	String pedidoCliente;

	@Column(nullable = true, precision = 12, scale = 2)
	Long crmId;

	@Column(nullable = true)
	Integer posicion;

	@Embedded
	private ControlAcceso controlAcceso;

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	public Long getIdCargoServicio() {
		return idCargoServicio;
	}

	public void setIdCargoServicio(Long idCargoServicio) {
		this.idCargoServicio = idCargoServicio;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Moneda getMonedaCosto() {
		return monedaCosto;
	}

	public void setMonedaCosto(Moneda monedaCosto) {
		this.monedaCosto = monedaCosto;
	}

	public BigDecimal getCosto() {
		return costo;
	}

	public void setCosto(BigDecimal costo) {
		this.costo = costo;
	}

	public Moneda getMonedaVenta() {
		return monedaVenta;
	}

	public void setMonedaVenta(Moneda monedaVenta) {
		this.monedaVenta = monedaVenta;
	}

	public BigDecimal getVenta() {
		return venta;
	}

	public void setVenta(BigDecimal venta) {
		this.venta = venta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Direccion getDireccionStop() {
		return direccionStop;
	}

	public void setDireccionStop(Direccion direccionStop) {
		this.direccionStop = direccionStop;
	}

	public String getConsignatarioStop() {
		return consignatarioStop;
	}

	public void setConsignatarioStop(String consignatarioStop) {
		this.consignatarioStop = consignatarioStop;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public String getPedidoCliente() {
		return pedidoCliente;
	}

	public void setPedidoCliente(String pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}

	public Long getCrmId() {
		return crmId;
	}

	public void setCrmId(Long crmId) {
		this.crmId = crmId;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

}
