package com.logistica.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "proveedores")
public class Proveedor extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_proveedor")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idProveedor;

	@Column(nullable = true, length = 100)
	private String razonSocial;

	@Column(nullable = true, length = 100)
	private String razonComercial;

	@Column(nullable = true, length = 60)
	private String terminoBusqueda;

	@Column(nullable = true)
	private Boolean bloqueado;

	@Column(nullable = true, length = 500)
	private String motivoBloqueo;

	@Column(nullable = true, length = 15)
	private String rfc;

	@Column(nullable = true)
	private Boolean borrado;

	@Column(nullable = true)
	private Boolean aereo;

	@Embedded
	private Direccion direccion;

	@Column(nullable = true, precision = 12, scale = 4)
	private BigDecimal lineaCredito;

	@Column(nullable = true, precision = 12, scale = 4)
	private BigDecimal plazo;

	@Column(nullable = true, length = 15)
	private Long idEvaluacion;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRazonComercial() {
		return razonComercial;
	}

	public void setRazonComercial(String razonComercial) {
		this.razonComercial = razonComercial;
	}

	public String getTerminoBusqueda() {
		return terminoBusqueda;
	}

	public void setTerminoBusqueda(String terminoBusqueda) {
		this.terminoBusqueda = terminoBusqueda;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Boolean getAereo() {
		return aereo;
	}

	public void setAereo(Boolean aereo) {
		this.aereo = aereo;
	}

	public Long getIdEvaluacion() {
		return idEvaluacion;
	}

	public void setIdEvaluacion(Long idEvaluacion) {
		this.idEvaluacion = idEvaluacion;
	}

	public String getMotivoBloqueo() {
		return motivoBloqueo;
	}

	public void setMotivoBloqueo(String motivoBloqueo) {
		this.motivoBloqueo = motivoBloqueo;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public BigDecimal getLineaCredito() {
		return lineaCredito;
	}

	public void setLineaCredito(BigDecimal lineaCredito) {
		this.lineaCredito = lineaCredito;
	}

	public BigDecimal getPlazo() {
		return plazo;
	}

	public void setPlazo(BigDecimal plazo) {
		this.plazo = plazo;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
