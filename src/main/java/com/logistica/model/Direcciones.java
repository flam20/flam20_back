package com.logistica.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "direcciones")
public class Direcciones extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_direccion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDireccion;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "calle", column = @Column(name = "calleOrigen")),
			@AttributeOverride(name = "numInterior", column = @Column(name = "numInteriorOrigen")),
			@AttributeOverride(name = "numExterior", column = @Column(name = "numExteriorOrigen")),
			@AttributeOverride(name = "colonia", column = @Column(name = "coloniaOrigen")),
			@AttributeOverride(name = "cp", column = @Column(name = "cpOrigen")),
			@AttributeOverride(name = "ciudad", column = @Column(name = "ciudadOrigen")),
			@AttributeOverride(name = "correoElectronico", column = @Column(name = "correoElectronicoOrigen")),
			@AttributeOverride(name = "telefono", column = @Column(name = "telefonoOrigen")), })
	@AssociationOverrides({ @AssociationOverride(name = "pais", joinColumns = @JoinColumn(name = "idPaisOrigen")), })
	private Direccion origen;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "calle", column = @Column(name = "calleDestino")),
			@AttributeOverride(name = "numInterior", column = @Column(name = "numInteriorDestino")),
			@AttributeOverride(name = "numExterior", column = @Column(name = "numExteriorDestino")),
			@AttributeOverride(name = "colonia", column = @Column(name = "coloniaDestino")),
			@AttributeOverride(name = "cp", column = @Column(name = "cpDestino")),
			@AttributeOverride(name = "ciudad", column = @Column(name = "ciudadDestino")),
			@AttributeOverride(name = "correoElectronico", column = @Column(name = "correoElectronicoDestino")),
			@AttributeOverride(name = "telefono", column = @Column(name = "telefonoDestino")), })
	@AssociationOverrides({ @AssociationOverride(name = "pais", joinColumns = @JoinColumn(name = "idPaisDestino")), })
	private Direccion destino;

	@Embedded
	private ControlAcceso controlAcceso;

	public Direcciones() {
	}

	public Direcciones(Direccion destino, Direccion origen) {
		this.destino = destino;
		this.origen = origen;
	}

	public Long getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(Long idDireccion) {
		this.idDireccion = idDireccion;
	}

	public Direccion getOrigen() {
		return origen;
	}

	public void setOrigen(Direccion origen) {
		this.origen = origen;
	}

	public Direccion getDestino() {
		return destino;
	}

	public void setDestino(Direccion destino) {
		this.destino = destino;
	}

	public String getNombreDireccionCompleta() {
		return origen.getCiudad() + " > " + destino.getCiudad();
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
