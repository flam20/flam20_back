package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Embeddable
public class AnioMes extends CommonEntity {

	private static final long serialVersionUID = 4388431002823296557L;

	@Column(nullable = true)
	private Integer anio;

	@OneToOne
	@JoinColumn(name = "id_mes")
	private Mes mes;

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

}
