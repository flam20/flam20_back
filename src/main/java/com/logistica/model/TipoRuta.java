package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipos_ruta")
public class TipoRuta extends CommonEntity {

	private static final long serialVersionUID = 7978288485458520477L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tipo_ruta", nullable = false, unique = true)
	Long idTipoRuta;

	@Column(nullable = true, length = 100)
	String descripcion;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public TipoRuta() {
	}

	public TipoRuta(String descripcion) {
		this.descripcion = descripcion;
		this.borrado = false;
	}

	public Long getIdTipoRuta() {
		return idTipoRuta;
	}

	public void setIdTipoRuta(Long idTipoRuta) {
		this.idTipoRuta = idTipoRuta;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
