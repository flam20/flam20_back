package com.logistica.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.cartaporte.CartaPorte;

@Entity
@Table(name = "servicios_inter")
public class ServicioInter extends CommonEntity {

	private static final long serialVersionUID = -3562918206019154284L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_servicio", nullable = false)
	Long idServicio;

	@Column(nullable = true)
	String idServicioStr;

	@Column(nullable = true)
	Integer anio;

	@Column(nullable = true)
	Integer consecutivo;

	@Column(nullable = true)
	Boolean borrador;

	@Column(nullable = true)
	LocalDate fechaServicio;

	@OneToOne
	@JoinColumn(name = "id_mpc")
	Mpc mpc;

	@Column(nullable = false, length = 50)
	String tipo;

	@Column(nullable = false, length = 50)
	String subtipo;

	@OneToOne(optional = true)
	@JoinColumn(name = "id_shipper")
	Shipper shipper;

	@OneToOne(optional = true)
	@JoinColumn(name = "id_consignatario")
	Consignatario consignatario;

	@OneToOne
	@JoinColumn(name = "id_cliente")
	Cliente cliente;

	@OneToOne
	@JoinColumn(name = "id_pagador")
	Cliente pagador;
	
	@Column(name = "id_usuario", nullable = false, length = 50)
	String trafico;

	@Column(name = "nombre_usuario", nullable = true)
	String nombreTrafico;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<CargoOrigen> cargosOrigen;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<CargoDestino> cargosDestino;

	@OneToOne
	@JoinColumn(name = "id_estatus_servicio")
	EstatusServicio estatus;

	@Column(nullable = true)
	LocalDate fechaSalida;

	@Transient
	String sFechaSalida;

	@Column(nullable = true)
	LocalDate fechaArribo1;

	@Transient
	String sFechaArribo1;

	@Column(nullable = true)
	LocalDate fechaArribo2;

	@Transient
	String sFechaArribo2;

	@Column(nullable = true)
	LocalDate fechaArribo3;

	@Transient
	String sFechaArribo3;

	@Column(nullable = true)
	LocalDate fechaDocumentacion;

	@Transient
	String sFechaDocumentacion;

	@OneToOne(optional = true)
	@JoinColumn(name = "id_agente")
	Agente ghPrefijoAgente;

	@Column(nullable = true, length = 100)
	String ghNumGuia;

	@OneToOne
	@JoinColumn(name = "id_aerolinea")
	Aerolinea gmPrefijoAerolinea;

	@Column(nullable = true, length = 100)
	String gmNumGuia;

	@OneToOne
	@JoinColumn(name = "id_aeropuerto_origen")
	Aeropuerto aeropuertoOrigen;

	@OneToOne
	@JoinColumn(name = "id_aeropuerto_destino")
	Aeropuerto aeropuertoDestino;

	@OneToOne
	@JoinColumn(name = "id_incoterm_origen")
	Incoterm incotermOrigen;

	@OneToOne
	@JoinColumn(name = "id_incoterm_destino")
	Incoterm incotermDestino;

	@Column(nullable = true, unique = false)
	Long numPiezas;

	@Column(nullable = true)
	BigDecimal pesoBruto;

	@Column(nullable = true)
	BigDecimal pesoCargable;

	@Column(nullable = true, length = 500)
	String notas;

	@Column(nullable = true, length = 500)
	String motivoCancelacion;

	@Column(nullable = true, length = 5)
	Boolean notasCompletas;

	@Column(nullable = true, length = 50)
	String noFacturaCliente;

	@Column(nullable = true, length = 50)
	String refFacturacion;

	@Column(nullable = true)
	BigDecimal totalprofitCargosOrigen;

	@Column(nullable = true)
	BigDecimal totalprofitCargosDestino;

	@Column(nullable = true)
	BigDecimal totalprofit;

	@Column(nullable = true)
	LocalDate fechaFactura;

	@Column(nullable = true)
	String ordenCompra;

	@OneToOne
	@JoinColumn(name = "id_carta_porte")
	CartaPorte cartaPorte;

	@Column(nullable = true)
	String idChat;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<BitacoraSap> bitacoras;
	
	@Column(nullable = true)
	String lcl;
	
	@Column(nullable = true)
	String contenedor;
	
	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getIdServicioStr() {
		return idServicioStr;
	}

	public void setIdServicioStr(String idServicioStr) {
		this.idServicioStr = idServicioStr;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getConsecutivo() {
		return consecutivo;
	}

	public void setConsecutivo(Integer consecutivo) {
		this.consecutivo = consecutivo;
	}

	public Boolean getBorrador() {
		return borrador;
	}

	public void setBorrador(Boolean borrador) {
		this.borrador = borrador;
	}

	public LocalDate getFechaServicio() {
		return fechaServicio;
	}

	public void setFechaServicio(LocalDate fechaServicio) {
		this.fechaServicio = fechaServicio;
	}

	public Mpc getMpc() {
		return mpc;
	}

	public void setMpc(Mpc mpc) {
		this.mpc = mpc;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSubtipo() {
		return subtipo;
	}

	public void setSubtipo(String subtipo) {
		this.subtipo = subtipo;
	}

	public Shipper getShipper() {
		return shipper;
	}

	public void setShipper(Shipper shipper) {
		this.shipper = shipper;
	}

	public Consignatario getConsignatario() {
		return consignatario;
	}

	public void setConsignatario(Consignatario consignatario) {
		this.consignatario = consignatario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getTrafico() {
		return trafico;
	}

	public void setTrafico(String trafico) {
		this.trafico = trafico;
	}

	public List<CargoOrigen> getCargosOrigen() {
		return cargosOrigen;
	}

	public void setCargosOrigen(List<CargoOrigen> cargosOrigen) {
		this.cargosOrigen = cargosOrigen;
	}

	public List<CargoDestino> getCargosDestino() {
		return cargosDestino;
	}

	public void setCargosDestino(List<CargoDestino> cargosDestino) {
		this.cargosDestino = cargosDestino;
	}

	public EstatusServicio getEstatus() {
		return estatus;
	}

	public void setEstatus(EstatusServicio estatus) {
		this.estatus = estatus;
	}

	public LocalDate getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(LocalDate fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public LocalDate getFechaArribo1() {
		return fechaArribo1;
	}

	public void setFechaArribo1(LocalDate fechaArribo1) {
		this.fechaArribo1 = fechaArribo1;
	}

	public LocalDate getFechaArribo2() {
		return fechaArribo2;
	}

	public void setFechaArribo2(LocalDate fechaArribo2) {
		this.fechaArribo2 = fechaArribo2;
	}

	public LocalDate getFechaArribo3() {
		return fechaArribo3;
	}

	public void setFechaArribo3(LocalDate fechaArribo3) {
		this.fechaArribo3 = fechaArribo3;
	}

	public LocalDate getFechaDocumentacion() {
		return fechaDocumentacion;
	}

	public void setFechaDocumentacion(LocalDate fechaDocumentacion) {
		this.fechaDocumentacion = fechaDocumentacion;
	}

	public Agente getGhPrefijoAgente() {
		return ghPrefijoAgente;
	}

	public void setGhPrefijoAgente(Agente ghPrefijoAgente) {
		this.ghPrefijoAgente = ghPrefijoAgente;
	}

	public String getGhNumGuia() {
		return ghNumGuia;
	}

	public void setGhNumGuia(String ghNumGuia) {
		this.ghNumGuia = ghNumGuia;
	}

	public Aerolinea getGmPrefijoAerolinea() {
		return gmPrefijoAerolinea;
	}

	public void setGmPrefijoAerolinea(Aerolinea gmPrefijoAerolinea) {
		this.gmPrefijoAerolinea = gmPrefijoAerolinea;
	}

	public String getGmNumGuia() {
		return gmNumGuia;
	}

	public void setGmNumGuia(String gmNumGuia) {
		this.gmNumGuia = gmNumGuia;
	}

	public Aeropuerto getAeropuertoOrigen() {
		return aeropuertoOrigen;
	}

	public void setAeropuertoOrigen(Aeropuerto aeropuertoOrigen) {
		this.aeropuertoOrigen = aeropuertoOrigen;
	}

	public Aeropuerto getAeropuertoDestino() {
		return aeropuertoDestino;
	}

	public void setAeropuertoDestino(Aeropuerto aeropuertoDestino) {
		this.aeropuertoDestino = aeropuertoDestino;
	}

	public Incoterm getIncotermOrigen() {
		return incotermOrigen;
	}

	public void setIncotermOrigen(Incoterm incotermOrigen) {
		this.incotermOrigen = incotermOrigen;
	}

	public Incoterm getIncotermDestino() {
		return incotermDestino;
	}

	public void setIncotermDestino(Incoterm incotermDestino) {
		this.incotermDestino = incotermDestino;
	}

	public Long getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(Long numPiezas) {
		this.numPiezas = numPiezas;
	}

	public BigDecimal getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(BigDecimal pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public BigDecimal getPesoCargable() {
		return pesoCargable;
	}

	public void setPesoCargable(BigDecimal pesoCargable) {
		this.pesoCargable = pesoCargable;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public String getMotivoCancelacion() {
		return motivoCancelacion;
	}

	public void setMotivoCancelacion(String motivoCancelacion) {
		this.motivoCancelacion = motivoCancelacion;
	}

	public Boolean getNotasCompletas() {
		return notasCompletas;
	}

	public void setNotasCompletas(Boolean notasCompletas) {
		this.notasCompletas = notasCompletas;
	}

	public String getNoFacturaCliente() {
		return noFacturaCliente;
	}

	public void setNoFacturaCliente(String noFacturaCliente) {
		this.noFacturaCliente = noFacturaCliente;
	}

	public String getRefFacturacion() {
		return refFacturacion;
	}

	public void setRefFacturacion(String refFacturacion) {
		this.refFacturacion = refFacturacion;
	}

	public BigDecimal getTotalprofitCargosOrigen() {
		return totalprofitCargosOrigen;
	}

	public void setTotalprofitCargosOrigen(BigDecimal totalprofitCargosOrigen) {
		this.totalprofitCargosOrigen = totalprofitCargosOrigen;
	}

	public BigDecimal getTotalprofitCargosDestino() {
		return totalprofitCargosDestino;
	}

	public void setTotalprofitCargosDestino(BigDecimal totalprofitCargosDestino) {
		this.totalprofitCargosDestino = totalprofitCargosDestino;
	}

	public BigDecimal getTotalprofit() {
		return totalprofit;
	}

	public void setTotalprofit(BigDecimal totalprofit) {
		this.totalprofit = totalprofit;
	}

	public LocalDate getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(LocalDate fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public String getOrdenCompra() {
		return ordenCompra;
	}

	public void setOrdenCompra(String ordenCompra) {
		this.ordenCompra = ordenCompra;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	public String getsFechaSalida() {
		return sFechaSalida;
	}

	public void setsFechaSalida(String sFechaSalida) {
		this.sFechaSalida = sFechaSalida;
	}

	public String getsFechaArribo1() {
		return sFechaArribo1;
	}

	public void setsFechaArribo1(String sFechaArribo1) {
		this.sFechaArribo1 = sFechaArribo1;
	}

	public String getsFechaArribo2() {
		return sFechaArribo2;
	}

	public void setsFechaArribo2(String sFechaArribo2) {
		this.sFechaArribo2 = sFechaArribo2;
	}

	public String getsFechaArribo3() {
		return sFechaArribo3;
	}

	public void setsFechaArribo3(String sFechaArribo3) {
		this.sFechaArribo3 = sFechaArribo3;
	}

	public String getsFechaDocumentacion() {
		return sFechaDocumentacion;
	}

	public void setsFechaDocumentacion(String sFechaDocumentacion) {
		this.sFechaDocumentacion = sFechaDocumentacion;
	}

	public CartaPorte getCartaPorte() {
		return cartaPorte;
	}

	public void setCartaPorte(CartaPorte cartaPorte) {
		this.cartaPorte = cartaPorte;
	}

	public String getIdChat() {
		return idChat;
	}

	public void setIdChat(String idChat) {
		this.idChat = idChat;
	}

	public String getNombreTrafico() {
		return nombreTrafico;
	}

	public void setNombreTrafico(String nombreTrafico) {
		this.nombreTrafico = nombreTrafico;
	}

	public List<BitacoraSap> getBitacoras() {
		return bitacoras;
	}

	public void setBitacoras(List<BitacoraSap> bitacoras) {
		this.bitacoras = bitacoras;
	}

	public String getLcl() {
		return lcl;
	}

	public void setLcl(String lcl) {
		this.lcl = lcl;
	}

	public String getContenedor() {
		return contenedor;
	}

	public void setContenedor(String contenedor) {
		this.contenedor = contenedor;
	}

	public Cliente getPagador() {
		return pagador;
	}

	public void setPagador(Cliente pagador) {
		this.pagador = pagador;
	}
	
}
