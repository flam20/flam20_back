package com.logistica.model;

public class DashboardBarMonth {
	String mes;
	int servicios;
	
	
	
	public DashboardBarMonth(String mes, int servicios) {
		super();
		this.mes = mes;
		this.servicios = servicios;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public int getServicios() {
		return servicios;
	}
	public void setServicios(int servicios) {
		this.servicios = servicios;
	}
	

}
