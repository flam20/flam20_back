package com.logistica.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "reporteAM")
public class ReporteAM implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	@Embedded
	private ControlAcceso controlAcceso;

	@Id
	@Column(nullable = false, unique = true)
	String idReportColumn;

	@Column(nullable = true, length = 100)
	String columnaTitulo;

	@Column(nullable = true)
	int columnaPosicion;

	public String getIdReportColumn() {
		return idReportColumn;
	}

	public void setIdReportColumn(String idReportColumn) {
		this.idReportColumn = idReportColumn;
	}

	public String getColumnaTitulo() {
		return columnaTitulo;
	}

	public void setColumnaTitulo(String columnaTitulo) {
		this.columnaTitulo = columnaTitulo;
	}

	public int getColumnaPosicion() {
		return columnaPosicion;
	}

	public void setColumnaPosicion(int columnaPosicion) {
		this.columnaPosicion = columnaPosicion;
	}

	@Transient
	boolean editingStatus = false;

	public ReporteAM() {
	}

	public ReporteAM(String id, String columnaTitulo) {
		this.idReportColumn = id;
		this.columnaTitulo = columnaTitulo;

	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idReportColumn == null) ? 0 : idReportColumn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReporteAM other = (ReporteAM) obj;
		if (idReportColumn == null) {
			if (other.idReportColumn != null)
				return false;
		} else if (!idReportColumn.equals(other.idReportColumn))
			return false;
		return true;
	}

	public static ReporteAM clone(ReporteAM rep) {
		try {
			return (ReporteAM) rep.clone();
		} catch (CloneNotSupportedException e) {
			// not possible
		}
		return null;
	}

	public boolean getEditingStatus() {
		return editingStatus;
	}

	public void setEditingStatus(boolean editingStatus) {
		this.editingStatus = editingStatus;
	}
}
