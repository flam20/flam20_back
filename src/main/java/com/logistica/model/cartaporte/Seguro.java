package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "seguros_cp")
public class Seguro extends CommonEntity {

	private static final long serialVersionUID = 335696920409347171L;

	@Id
	@Column(name = "id_seguro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSeguro;

	@Column(name = "asegura_resp_civil")
	private String aseguraRespCivil;

	@Column(name = "poliza_resp_civil")
	private String polizaRespCivil;

	@Column(name = "asegura_med_ambiente")
	private String aseguraMedAmbiente;

	@Column(name = "poliza_med_ambiente")
	private String polizaMedAmbiente;

	@Column(name = "asegura_carga")
	private String aseguraCarga;

	@Column(name = "poliza_carga")
	private String polizaCarga;

	@Column(name = "prima_seguro")
	private String primaSeguro;

	public Long getIdSeguro() {
		return idSeguro;
	}

	public void setIdSeguro(Long idSeguro) {
		this.idSeguro = idSeguro;
	}

	public String getAseguraRespCivil() {
		return aseguraRespCivil;
	}

	public void setAseguraRespCivil(String aseguraRespCivil) {
		this.aseguraRespCivil = aseguraRespCivil;
	}

	public String getPolizaRespCivil() {
		return polizaRespCivil;
	}

	public void setPolizaRespCivil(String polizaRespCivil) {
		this.polizaRespCivil = polizaRespCivil;
	}

	public String getAseguraMedAmbiente() {
		return aseguraMedAmbiente;
	}

	public void setAseguraMedAmbiente(String aseguraMedAmbiente) {
		this.aseguraMedAmbiente = aseguraMedAmbiente;
	}

	public String getPolizaMedAmbiente() {
		return polizaMedAmbiente;
	}

	public void setPolizaMedAmbiente(String polizaMedAmbiente) {
		this.polizaMedAmbiente = polizaMedAmbiente;
	}

	public String getAseguraCarga() {
		return aseguraCarga;
	}

	public void setAseguraCarga(String aseguraCarga) {
		this.aseguraCarga = aseguraCarga;
	}

	public String getPolizaCarga() {
		return polizaCarga;
	}

	public void setPolizaCarga(String polizaCarga) {
		this.polizaCarga = polizaCarga;
	}

	public String getPrimaSeguro() {
		return primaSeguro;
	}

	public void setPrimaSeguro(String primaSeguro) {
		this.primaSeguro = primaSeguro;
	}

}
