package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "pedimentos_cp")
public class Pedimento extends CommonEntity {

	private static final long serialVersionUID = 6476602072352089503L;

	@Id
	@Column(name = "id_pedimento")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPedimento;

	@Column(name = "pedimento")
	private String pedimento;

	public Long getIdPedimento() {
		return idPedimento;
	}

	public void setIdPedimento(Long idPedimento) {
		this.idPedimento = idPedimento;
	}

	public String getPedimento() {
		return pedimento;
	}

	public void setPedimento(String pedimento) {
		this.pedimento = pedimento;
	}

}
