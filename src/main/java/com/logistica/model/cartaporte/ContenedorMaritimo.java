package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "contenedores_maritimo_cp")
public class ContenedorMaritimo extends CommonEntity {

	private static final long serialVersionUID = 6152635860820193351L;

	@Id
	@Column(name = "id_contenedor_maritimo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idContenedorMaritimo;

	@Column(name = "matricula_contenedor")
	private String matriculaContenedor;

	@Column(name = "tipo_contenedor")
	private String tipoContenedor;

	@Column(name = "num_precinto")
	private String numPrecinto;

	public Long getIdContenedorMaritimo() {
		return idContenedorMaritimo;
	}

	public void setIdContenedorMaritimo(Long idContenedorMaritimo) {
		this.idContenedorMaritimo = idContenedorMaritimo;
	}

	public String getMatriculaContenedor() {
		return matriculaContenedor;
	}

	public void setMatriculaContenedor(String matriculaContenedor) {
		this.matriculaContenedor = matriculaContenedor;
	}

	public String getTipoContenedor() {
		return tipoContenedor;
	}

	public void setTipoContenedor(String tipoContenedor) {
		this.tipoContenedor = tipoContenedor;
	}

	public String getNumPrecinto() {
		return numPrecinto;
	}

	public void setNumPrecinto(String numPrecinto) {
		this.numPrecinto = numPrecinto;
	}

}
