package com.logistica.model.cartaporte;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "cartas_porte")
public class CartaPorte extends CommonEntity {

	private static final long serialVersionUID = 1218505804756790982L;

	@Id
	@Column(name = "id_carta_porte")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCartaPorte;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<UbicacionCp> ubicaciones;

	@OneToOne
	@JoinColumn(name = "id_resumen_mercancia")
	Mercancias mercancias;

	@OneToOne
	@JoinColumn(name = "id_figura_transporte")
	FiguraTransporte figuraTransporte;

	@Column(name = "version")
	private String version;

	@Column(name = "transp_internac")
	private String transpInternac;

	@Column(name = "entrada_salida_merc")
	private String entradaSalidaMerc;

	@Column(name = "pais_origen_destino")
	private String paisOrigenDestino;

	@Column(name = "via_entrada_salida")
	private String viaEntradaSalida;

	@Column(name = "total_dist_rec")
	private BigDecimal totalDistRec;

	public Long getIdCartaPorte() {
		return idCartaPorte;
	}

	public void setIdCartaPorte(Long idCartaPorte) {
		this.idCartaPorte = idCartaPorte;
	}

	public List<UbicacionCp> getUbicaciones() {
		return ubicaciones;
	}

	public void setUbicaciones(List<UbicacionCp> ubicaciones) {
		this.ubicaciones = ubicaciones;
	}

	public Mercancias getMercancias() {
		return mercancias;
	}

	public void setMercancias(Mercancias mercancias) {
		this.mercancias = mercancias;
	}

	public FiguraTransporte getFiguraTransporte() {
		return figuraTransporte;
	}

	public void setFiguraTransporte(FiguraTransporte figuraTransporte) {
		this.figuraTransporte = figuraTransporte;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getTranspInternac() {
		return transpInternac;
	}

	public void setTranspInternac(String transpInternac) {
		this.transpInternac = transpInternac;
	}

	public String getEntradaSalidaMerc() {
		return entradaSalidaMerc;
	}

	public void setEntradaSalidaMerc(String entradaSalidaMerc) {
		this.entradaSalidaMerc = entradaSalidaMerc;
	}

	public String getViaEntradaSalida() {
		return viaEntradaSalida;
	}

	public void setViaEntradaSalida(String viaEntradaSalida) {
		this.viaEntradaSalida = viaEntradaSalida;
	}

	public BigDecimal getTotalDistRec() {
		return totalDistRec;
	}

	public void setTotalDistRec(BigDecimal totalDistRec) {
		this.totalDistRec = totalDistRec;
	}

	public String getPaisOrigenDestino() {
		return paisOrigenDestino;
	}

	public void setPaisOrigenDestino(String paisOrigenDestino) {
		this.paisOrigenDestino = paisOrigenDestino;
	}

}
