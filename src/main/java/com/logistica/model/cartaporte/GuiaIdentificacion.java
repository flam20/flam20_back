package com.logistica.model.cartaporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "guias_identificacion_cp")
public class GuiaIdentificacion extends CommonEntity {

	private static final long serialVersionUID = -337300530079292961L;

	@Id
	@Column(name = "id_guia_identificacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idGuiaIdentificacion;
	
	@Column(name = "numero_guia_identificacion")
	private String numeroGuiaIdentificacion;

	@Column(name = "descrip_guia_identificacion")
	private String descripGuiaIdentificacion;

	@Column(name = "peso_guia_identificacion")
	private BigDecimal pesoGuiaIdentificacion;

	public String getNumeroGuiaIdentificacion() {
		return numeroGuiaIdentificacion;
	}

	public void setNumeroGuiaIdentificacion(String numeroGuiaIdentificacion) {
		this.numeroGuiaIdentificacion = numeroGuiaIdentificacion;
	}

	public String getDescripGuiaIdentificacion() {
		return descripGuiaIdentificacion;
	}

	public void setDescripGuiaIdentificacion(String descripGuiaIdentificacion) {
		this.descripGuiaIdentificacion = descripGuiaIdentificacion;
	}

	public BigDecimal getPesoGuiaIdentificacion() {
		return pesoGuiaIdentificacion;
	}

	public void setPesoGuiaIdentificacion(BigDecimal pesoGuiaIdentificacion) {
		this.pesoGuiaIdentificacion = pesoGuiaIdentificacion;
	}

	public Long getIdGuiaIdentificacion() {
		return idGuiaIdentificacion;
	}

	public void setIdGuiaIdentificacion(Long idGuiaIdentificacion) {
		this.idGuiaIdentificacion = idGuiaIdentificacion;
	}

}
