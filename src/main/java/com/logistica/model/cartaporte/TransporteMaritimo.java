package com.logistica.model.cartaporte;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "transportes_maritimo_cp")
public class TransporteMaritimo extends CommonEntity {

	private static final long serialVersionUID = 8609279029342364772L;

	@Id
	@Column(name = "id_transporte_maritimo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTransporteMaritimo;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<ContenedorMaritimo> contenedores;

	@Column(name = "perm_sct")
	private String permSCT;

	@Column(name = "num_permiso_sct")
	private String numPermisoSCT;

	@Column(name = "nombre_aseg")
	private String nombreAseg;

	@Column(name = "num_poliza_seguro")
	private String numPolizaSeguro;

	@Column(name = "tipo_embarcacion")
	private String tipoEmbarcacion;

	@Column(name = "matricula")
	private String matricula;

	@Column(name = "numero_omi")
	private String numeroOMI;

	@Column(name = "anio_embarcacion")
	private Integer anioEmbarcacion;

	@Column(name = "nombre_embarc")
	private String nombreEmbarc;

	@Column(name = "nacionalidad_embarc")
	private String nacionalidadEmbarc;

	@Column(name = "unidades_de_arq_bruto")
	private BigDecimal unidadesDeArqBruto;

	@Column(name = "tipo_carga")
	private String tipoCarga;

	@Column(name = "num_cert_itc")
	private String numCertITC;

	@Column(name = "eslora")
	private BigDecimal eslora;

	@Column(name = "manga")
	private BigDecimal manga;

	@Column(name = "calado")
	private BigDecimal calado;

	@Column(name = "linea_naviera")
	private String lineaNaviera;

	@Column(name = "nombre_agente_naviero")
	private String nombreAgenteNaviero;

	@Column(name = "num_autorizacion_naviero")
	private String numAutorizacionNaviero;

	@Column(name = "num_viaje")
	private String numViaje;

	@Column(name = "num_conoc_embarc")
	private String numConocEmbarc;

	public Long getIdTransporteMaritimo() {
		return idTransporteMaritimo;
	}

	public void setIdTransporteMaritimo(Long idTransporteMaritimo) {
		this.idTransporteMaritimo = idTransporteMaritimo;
	}

	public List<ContenedorMaritimo> getContenedores() {
		return contenedores;
	}

	public void setContenedores(List<ContenedorMaritimo> contenedores) {
		this.contenedores = contenedores;
	}

	public String getPermSCT() {
		return permSCT;
	}

	public void setPermSCT(String permSCT) {
		this.permSCT = permSCT;
	}

	public String getNumPermisoSCT() {
		return numPermisoSCT;
	}

	public void setNumPermisoSCT(String numPermisoSCT) {
		this.numPermisoSCT = numPermisoSCT;
	}

	public String getNombreAseg() {
		return nombreAseg;
	}

	public void setNombreAseg(String nombreAseg) {
		this.nombreAseg = nombreAseg;
	}

	public String getNumPolizaSeguro() {
		return numPolizaSeguro;
	}

	public void setNumPolizaSeguro(String numPolizaSeguro) {
		this.numPolizaSeguro = numPolizaSeguro;
	}

	public String getTipoEmbarcacion() {
		return tipoEmbarcacion;
	}

	public void setTipoEmbarcacion(String tipoEmbarcacion) {
		this.tipoEmbarcacion = tipoEmbarcacion;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNumeroOMI() {
		return numeroOMI;
	}

	public void setNumeroOMI(String numeroOMI) {
		this.numeroOMI = numeroOMI;
	}

	public Integer getAnioEmbarcacion() {
		return anioEmbarcacion;
	}

	public void setAnioEmbarcacion(Integer anioEmbarcacion) {
		this.anioEmbarcacion = anioEmbarcacion;
	}

	public String getNombreEmbarc() {
		return nombreEmbarc;
	}

	public void setNombreEmbarc(String nombreEmbarc) {
		this.nombreEmbarc = nombreEmbarc;
	}

	public String getNacionalidadEmbarc() {
		return nacionalidadEmbarc;
	}

	public void setNacionalidadEmbarc(String nacionalidadEmbarc) {
		this.nacionalidadEmbarc = nacionalidadEmbarc;
	}

	public BigDecimal getUnidadesDeArqBruto() {
		return unidadesDeArqBruto;
	}

	public void setUnidadesDeArqBruto(BigDecimal unidadesDeArqBruto) {
		this.unidadesDeArqBruto = unidadesDeArqBruto;
	}

	public String getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public String getNumCertITC() {
		return numCertITC;
	}

	public void setNumCertITC(String numCertITC) {
		this.numCertITC = numCertITC;
	}

	public BigDecimal getEslora() {
		return eslora;
	}

	public void setEslora(BigDecimal eslora) {
		this.eslora = eslora;
	}

	public BigDecimal getManga() {
		return manga;
	}

	public void setManga(BigDecimal manga) {
		this.manga = manga;
	}

	public BigDecimal getCalado() {
		return calado;
	}

	public void setCalado(BigDecimal calado) {
		this.calado = calado;
	}

	public String getLineaNaviera() {
		return lineaNaviera;
	}

	public void setLineaNaviera(String lineaNaviera) {
		this.lineaNaviera = lineaNaviera;
	}

	public String getNombreAgenteNaviero() {
		return nombreAgenteNaviero;
	}

	public void setNombreAgenteNaviero(String nombreAgenteNaviero) {
		this.nombreAgenteNaviero = nombreAgenteNaviero;
	}

	public String getNumAutorizacionNaviero() {
		return numAutorizacionNaviero;
	}

	public void setNumAutorizacionNaviero(String numAutorizacionNaviero) {
		this.numAutorizacionNaviero = numAutorizacionNaviero;
	}

	public String getNumViaje() {
		return numViaje;
	}

	public void setNumViaje(String numViaje) {
		this.numViaje = numViaje;
	}

	public String getNumConocEmbarc() {
		return numConocEmbarc;
	}

	public void setNumConocEmbarc(String numConocEmbarc) {
		this.numConocEmbarc = numConocEmbarc;
	}

}
