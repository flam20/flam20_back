package com.logistica.model.cartaporte;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "tipos_figura_cp")
public class TipoFigura extends CommonEntity {

	private static final long serialVersionUID = -3343819308886821354L;

	@Id
	@Column(name = "id_tipo_figuera")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTipoFigura;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<ParteTransporte> partesTransporte;

	@Column(name = "tipo_figura")
	private String tipoFigura;

	@Column(name = "rfc_figura")
	private String rfcFigura;

	@Column(name = "num_licencia")
	private String numLicencia;

	@Column(name = "nombre_figura")
	private String nombreFigura;

	@Column(name = "num_reg_id_trib_figura")
	private String numRegIdTribFigura;

	@Column(name = "residencia_fiscal_figura")
	private String residenciaFiscalFigura;

	public String getNumLicencia() {
		return numLicencia;
	}

	public void setNumLicencia(String numLicencia) {
		this.numLicencia = numLicencia;
	}

	public Long getIdTipoFigura() {
		return idTipoFigura;
	}

	public void setIdTipoFigura(Long idTipoFigura) {
		this.idTipoFigura = idTipoFigura;
	}

	public List<ParteTransporte> getPartesTransporte() {
		return partesTransporte;
	}

	public void setPartesTransporte(List<ParteTransporte> partesTransporte) {
		this.partesTransporte = partesTransporte;
	}

	public String getTipoFigura() {
		return tipoFigura;
	}

	public void setTipoFigura(String tipoFigura) {
		this.tipoFigura = tipoFigura;
	}

	public String getRfcFigura() {
		return rfcFigura;
	}

	public void setRfcFigura(String rfcFigura) {
		this.rfcFigura = rfcFigura;
	}

	public String getNombreFigura() {
		return nombreFigura;
	}

	public void setNombreFigura(String nombreFigura) {
		this.nombreFigura = nombreFigura;
	}

	public String getNumRegIdTribFigura() {
		return numRegIdTribFigura;
	}

	public void setNumRegIdTribFigura(String numRegIdTribFigura) {
		this.numRegIdTribFigura = numRegIdTribFigura;
	}

	public String getResidenciaFiscalFigura() {
		return residenciaFiscalFigura;
	}

	public void setResidenciaFiscalFigura(String residenciaFiscalFigura) {
		this.residenciaFiscalFigura = residenciaFiscalFigura;
	}

}
