package com.logistica.model.cartaporte;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "resumenes_mercancia_cp")
public class Mercancias extends CommonEntity {

	private static final long serialVersionUID = 7300586396430583774L;

	@Id
	@Column(name = "id_resumen_mercancia")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idResumenMercancia;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<Mercancia> mercancia;

	@OneToOne
	@JoinColumn(name = "id_autotransporte")
	Autotransporte autotransporte;

	@OneToOne
	@JoinColumn(name = "id_transporte_maritimo")
	TransporteMaritimo transporteMaritimo;

	@OneToOne
	@JoinColumn(name = "id_transporte_aereo")
	TransporteAereo transporteAereo;

	@OneToOne
	@JoinColumn(name = "id_transporte_ferroviario")
	TransporteFerroviario transporteFerroviario;

	@Column(name = "peso_bruto_total")
	private BigDecimal pesoBrutoTotal;

	@Column(name = "unidad_peso")
	private String unidadPeso;

	@Column(name = "peso_neto_total")
	private BigDecimal pesoNetoTotal;

	@Column(name = "num_total_mercancias")
	private int numTotalMercancias;

	@Column(name = "cargo_por_tasacion")
	private BigDecimal cargoPorTasacion;

	public Long getIdResumenMercancia() {
		return idResumenMercancia;
	}

	public void setIdResumenMercancia(Long idResumenMercancia) {
		this.idResumenMercancia = idResumenMercancia;
	}

	public List<Mercancia> getMercancia() {
		return mercancia;
	}

	public void setMercancia(List<Mercancia> mercancia) {
		this.mercancia = mercancia;
	}

	public Autotransporte getAutotransporte() {
		return autotransporte;
	}

	public void setAutotransporte(Autotransporte autotransporte) {
		this.autotransporte = autotransporte;
	}

	public TransporteMaritimo getTransporteMaritimo() {
		return transporteMaritimo;
	}

	public void setTransporteMaritimo(TransporteMaritimo transporteMaritimo) {
		this.transporteMaritimo = transporteMaritimo;
	}

	public TransporteAereo getTransporteAereo() {
		return transporteAereo;
	}

	public void setTransporteAereo(TransporteAereo transporteAereo) {
		this.transporteAereo = transporteAereo;
	}

	public TransporteFerroviario getTransporteFerroviario() {
		return transporteFerroviario;
	}

	public void setTransporteFerroviario(TransporteFerroviario transporteFerroviario) {
		this.transporteFerroviario = transporteFerroviario;
	}

	public BigDecimal getPesoBrutoTotal() {
		return pesoBrutoTotal;
	}

	public void setPesoBrutoTotal(BigDecimal pesoBrutoTotal) {
		this.pesoBrutoTotal = pesoBrutoTotal;
	}

	public String getUnidadPeso() {
		return unidadPeso;
	}

	public void setUnidadPeso(String unidadPeso) {
		this.unidadPeso = unidadPeso;
	}

	public BigDecimal getPesoNetoTotal() {
		return pesoNetoTotal;
	}

	public void setPesoNetoTotal(BigDecimal pesoNetoTotal) {
		this.pesoNetoTotal = pesoNetoTotal;
	}

	public int getNumTotalMercancias() {
		return numTotalMercancias;
	}

	public void setNumTotalMercancias(int numTotalMercancias) {
		this.numTotalMercancias = numTotalMercancias;
	}

	public BigDecimal getCargoPorTasacion() {
		return cargoPorTasacion;
	}

	public void setCargoPorTasacion(BigDecimal cargoPorTasacion) {
		this.cargoPorTasacion = cargoPorTasacion;
	}

}
