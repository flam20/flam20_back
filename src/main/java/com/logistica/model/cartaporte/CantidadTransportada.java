package com.logistica.model.cartaporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "cantidades_transporta_cp")
public class CantidadTransportada extends CommonEntity {

	private static final long serialVersionUID = -4012826379802767314L;

	@Id
	@Column(name = "id_cantidad_transporta")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCantidadTransporta;

	@Column(name = "cantidad")
	private BigDecimal cantidad;

	@Column(name = "id_origen")
	private String idOrigen;

	@Column(name = "id_destino")
	private String idDestino;

	@Column(name = "cves_transporte")
	private String cvesTransporte;

	public Long getIdCantidadTransporta() {
		return idCantidadTransporta;
	}

	public void setIdCantidadTransporta(Long idCantidadTransporta) {
		this.idCantidadTransporta = idCantidadTransporta;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getIdOrigen() {
		return idOrigen;
	}

	public void setIdOrigen(String idOrigen) {
		this.idOrigen = idOrigen;
	}

	public String getIdDestino() {
		return idDestino;
	}

	public void setIdDestino(String idDestino) {
		this.idDestino = idDestino;
	}

	public String getCvesTransporte() {
		return cvesTransporte;
	}

	public void setCvesTransporte(String cvesTransporte) {
		this.cvesTransporte = cvesTransporte;
	}

}
