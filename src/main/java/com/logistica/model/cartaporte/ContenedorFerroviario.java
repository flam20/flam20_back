package com.logistica.model.cartaporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "contenedores_ferroviario_cp")
public class ContenedorFerroviario extends CommonEntity {

	private static final long serialVersionUID = 3634596049090361964L;

	@Id
	@Column(name = "id_contenedor_ferroviario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idContenedorFerroviario;

	@Column(name = "tipo_contenedor")
	private String tipoContenedor;

	@Column(name = "peso_contenedor_vacio")
	private BigDecimal pesoContenedorVacio;

	@Column(name = "peso_neto_mercancia")
	private BigDecimal pesoNetoMercancia;

	public Long getIdContenedorFerroviario() {
		return idContenedorFerroviario;
	}

	public void setIdContenedorFerroviario(Long idContenedorFerroviario) {
		this.idContenedorFerroviario = idContenedorFerroviario;
	}

	public String getTipoContenedor() {
		return tipoContenedor;
	}

	public void setTipoContenedor(String tipoContenedor) {
		this.tipoContenedor = tipoContenedor;
	}

	public BigDecimal getPesoContenedorVacio() {
		return pesoContenedorVacio;
	}

	public void setPesoContenedorVacio(BigDecimal pesoContenedorVacio) {
		this.pesoContenedorVacio = pesoContenedorVacio;
	}

	public BigDecimal getPesoNetoMercancia() {
		return pesoNetoMercancia;
	}

	public void setPesoNetoMercancia(BigDecimal pesoNetoMercancia) {
		this.pesoNetoMercancia = pesoNetoMercancia;
	}

}
