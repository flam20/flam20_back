package com.logistica.model.cartaporte;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "carros_cp")
public class Carro extends CommonEntity {

	private static final long serialVersionUID = -2006145896829516668L;

	@Id
	@Column(name = "id_carro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCarro;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<ContenedorFerroviario> contenedores;

	@Column(name = "tipo_carro")
	private String tipoCarro;

	@Column(name = "matricula_carro")
	private String matriculaCarro;

	@Column(name = "guia_carro")
	private String guiaCarro;

	@Column(name = "toneladas_netas_carro")
	private BigDecimal toneladasNetasCarro;

	public Long getIdCarro() {
		return idCarro;
	}

	public void setIdCarro(Long idCarro) {
		this.idCarro = idCarro;
	}

	public List<ContenedorFerroviario> getContenedores() {
		return contenedores;
	}

	public void setContenedores(List<ContenedorFerroviario> contenedores) {
		this.contenedores = contenedores;
	}

	public String getTipoCarro() {
		return tipoCarro;
	}

	public void setTipoCarro(String tipoCarro) {
		this.tipoCarro = tipoCarro;
	}

	public String getMatriculaCarro() {
		return matriculaCarro;
	}

	public void setMatriculaCarro(String matriculaCarro) {
		this.matriculaCarro = matriculaCarro;
	}

	public String getGuiaCarro() {
		return guiaCarro;
	}

	public void setGuiaCarro(String guiaCarro) {
		this.guiaCarro = guiaCarro;
	}

	public BigDecimal getToneladasNetasCarro() {
		return toneladasNetasCarro;
	}

	public void setToneladasNetasCarro(BigDecimal toneladasNetasCarro) {
		this.toneladasNetasCarro = toneladasNetasCarro;
	}

}
