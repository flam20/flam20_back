package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "partes_transportes_cp")
public class ParteTransporte extends CommonEntity {

	private static final long serialVersionUID = 8751966123425809557L;

	@Id
	@Column(name = "id_parte_transporte")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idParteTransporte;

	@OneToOne
	@JoinColumn(name = "id_domicilio")
	private Domicilio domicilio;

	@Column(name = "parte_transporte")
	private String parteTransporte;

	public Long getIdParteTransporte() {
		return idParteTransporte;
	}

	public void setIdParteTransporte(Long idParteTransporte) {
		this.idParteTransporte = idParteTransporte;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public String getParteTransporte() {
		return parteTransporte;
	}

	public void setParteTransporte(String parteTransporte) {
		this.parteTransporte = parteTransporte;
	}

}
