package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "identificaciones_vehiculares_cp")
public class IdentificacionVehicular extends CommonEntity {

	private static final long serialVersionUID = -3690925478020464044L;

	@Id
	@Column(name = "id_identificacion_vehicular")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idIdentificacionVehicular;

	@Column(name = "configVehicular")
	private String configVehicular;

	@Column(name = "placa_vm")
	private String placaVM;

	@Column(name = "anio_modelo_vm")
	private int anioModeloVM;

	public Long getIdIdentificacionVehicular() {
		return idIdentificacionVehicular;
	}

	public void setIdIdentificacionVehicular(Long idIdentificacionVehicular) {
		this.idIdentificacionVehicular = idIdentificacionVehicular;
	}

	public String getConfigVehicular() {
		return configVehicular;
	}

	public void setConfigVehicular(String configVehicular) {
		this.configVehicular = configVehicular;
	}

	public String getPlacaVM() {
		return placaVM;
	}

	public void setPlacaVM(String placaVM) {
		this.placaVM = placaVM;
	}

	public int getAnioModeloVM() {
		return anioModeloVM;
	}

	public void setAnioModeloVM(int anioModeloVM) {
		this.anioModeloVM = anioModeloVM;
	}

}
