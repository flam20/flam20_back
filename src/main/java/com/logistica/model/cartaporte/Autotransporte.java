package com.logistica.model.cartaporte;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "autotransportes_cp")
public class Autotransporte extends CommonEntity {

	private static final long serialVersionUID = 6498608382208561793L;

	@Id
	@Column(name = "id_autotransporte")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAutotransporte;

	@OneToOne
	@JoinColumn(name = "id_identificacion_vehicular")
	IdentificacionVehicular identificacionVehicular;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<Seguro> seguros;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<Remolque> remolques;

	@Column(name = "perm_sct")
	private String permSCT;

	@Column(name = "num_permiso_sct")
	private String numPermisoSCT;

	public Long getIdAutotransporte() {
		return idAutotransporte;
	}

	public void setIdAutotransporte(Long idAutotransporte) {
		this.idAutotransporte = idAutotransporte;
	}

	public IdentificacionVehicular getIdentificacionVehicular() {
		return identificacionVehicular;
	}

	public void setIdentificacionVehicular(IdentificacionVehicular identificacionVehicular) {
		this.identificacionVehicular = identificacionVehicular;
	}

	public List<Remolque> getRemolques() {
		return remolques;
	}

	public void setRemolques(List<Remolque> remolques) {
		this.remolques = remolques;
	}

	public String getPermSCT() {
		return permSCT;
	}

	public void setPermSCT(String permSCT) {
		this.permSCT = permSCT;
	}

	public String getNumPermisoSCT() {
		return numPermisoSCT;
	}

	public void setNumPermisoSCT(String numPermisoSCT) {
		this.numPermisoSCT = numPermisoSCT;
	}

	public List<Seguro> getSeguros() {
		return seguros;
	}

	public void setSeguros(List<Seguro> seguros) {
		this.seguros = seguros;
	}

}
