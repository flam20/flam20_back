package com.logistica.model.cartaporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "ubicaciones_cp")
public class UbicacionCp extends CommonEntity {

	private static final long serialVersionUID = -541875035457787484L;

	@Id
	@Column(name = "id_ubicacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUbicacion;

	@OneToOne
	@JoinColumn(name = "id_domicilio")
	private Domicilio domicilio;

	@Column(name = "tipo_ubicacion")
	private String tipoUbicacion;

	@Column(name = "rfc_remitente_destinatario")
	private String rfcRemitenteDestinatario;

	@Column(name = "nombre_remitente_destinatario")
	private String nombreRemitenteDestinatario;

	@Column(name = "num_reg_id_trib")
	private String numRegIdTrib;

	@Column(name = "residencia_fiscal")
	private String residenciaFiscal;

	@Column(name = "num_estacion")
	private String numEstacion;

	@Column(name = "nombre_estacion")
	private String nombreEstacion;

	@Column(name = "navegacion_trafico")
	private String navegacionTrafico;

	@Column(name = "fecha_hora_salida_llegada")
	private String fechaHoraSalidaLlegada;

	@Column(name = "tipo_estacion")
	private String tipoEstacion;

	@Column(name = "distancia_recorrida")
	private BigDecimal distanciaRecorrida;

	public Long getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(Long idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public Domicilio getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(Domicilio domicilio) {
		this.domicilio = domicilio;
	}

	public String getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(String tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public String getRfcRemitenteDestinatario() {
		return rfcRemitenteDestinatario;
	}

	public void setRfcRemitenteDestinatario(String rfcRemitenteDestinatario) {
		this.rfcRemitenteDestinatario = rfcRemitenteDestinatario;
	}

	public String getNombreRemitenteDestinatario() {
		return nombreRemitenteDestinatario;
	}

	public void setNombreRemitenteDestinatario(String nombreRemitenteDestinatario) {
		this.nombreRemitenteDestinatario = nombreRemitenteDestinatario;
	}

	public String getNumRegIdTrib() {
		return numRegIdTrib;
	}

	public void setNumRegIdTrib(String numRegIdTrib) {
		this.numRegIdTrib = numRegIdTrib;
	}

	public String getResidenciaFiscal() {
		return residenciaFiscal;
	}

	public void setResidenciaFiscal(String residenciaFiscal) {
		this.residenciaFiscal = residenciaFiscal;
	}

	public String getNumEstacion() {
		return numEstacion;
	}

	public void setNumEstacion(String numEstacion) {
		this.numEstacion = numEstacion;
	}

	public String getNombreEstacion() {
		return nombreEstacion;
	}

	public void setNombreEstacion(String nombreEstacion) {
		this.nombreEstacion = nombreEstacion;
	}

	public String getNavegacionTrafico() {
		return navegacionTrafico;
	}

	public void setNavegacionTrafico(String navegacionTrafico) {
		this.navegacionTrafico = navegacionTrafico;
	}

	public String getFechaHoraSalidaLlegada() {
		return fechaHoraSalidaLlegada;
	}

	public void setFechaHoraSalidaLlegada(String fechaHoraSalidaLlegada) {
		this.fechaHoraSalidaLlegada = fechaHoraSalidaLlegada;
	}

	public String getTipoEstacion() {
		return tipoEstacion;
	}

	public void setTipoEstacion(String tipoEstacion) {
		this.tipoEstacion = tipoEstacion;
	}

	public BigDecimal getDistanciaRecorrida() {
		return distanciaRecorrida;
	}

	public void setDistanciaRecorrida(BigDecimal distanciaRecorrida) {
		this.distanciaRecorrida = distanciaRecorrida;
	}

}
