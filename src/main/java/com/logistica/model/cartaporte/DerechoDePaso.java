package com.logistica.model.cartaporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "derechos_paso_cp")
public class DerechoDePaso extends CommonEntity {

	private static final long serialVersionUID = 546527316970007781L;

	@Id
	@Column(name = "id_derecho_paso")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDerechoPaso;

	@Column(name = "tipo_derecho_de_paso")
	private String tipoDerechoDePaso;

	@Column(name = "kilometraje_pagado")
	private BigDecimal kilometrajePagado;

	public Long getIdDerechoPaso() {
		return idDerechoPaso;
	}

	public void setIdDerechoPaso(Long idDerechoPaso) {
		this.idDerechoPaso = idDerechoPaso;
	}

	public String getTipoDerechoDePaso() {
		return tipoDerechoDePaso;
	}

	public void setTipoDerechoDePaso(String tipoDerechoDePaso) {
		this.tipoDerechoDePaso = tipoDerechoDePaso;
	}

	public BigDecimal getKilometrajePagado() {
		return kilometrajePagado;
	}

	public void setKilometrajePagado(BigDecimal kilometrajePagado) {
		this.kilometrajePagado = kilometrajePagado;
	}

}
