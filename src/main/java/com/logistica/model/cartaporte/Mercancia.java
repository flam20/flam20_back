package com.logistica.model.cartaporte;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "mercancias_cp")
public class Mercancia extends CommonEntity {

	private static final long serialVersionUID = 3712350142216984532L;

	@Id
	@Column(name = "id_mercancia")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMercancia;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<Pedimento> pedimentos;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<GuiaIdentificacion> guiasIdentificacion;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<CantidadTransportada> cantidadesTransportadas;

	@OneToOne
	@JoinColumn(name = "id_detalle_mercancia")
	DetalleMercancia detalleMercancia;

	@Column(name = "bienes_transp")
	private String bienesTransp;

	@Column(name = "claves_stcc")
	private String claveSTCC;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "cantidad")
	private BigDecimal cantidad;

	@Column(name = "clave_unidad")
	private String claveUnidad;

	@Column(name = "unidad")
	private String unidad;

	@Column(name = "dimensiones")
	private String dimensiones;

	@Column(name = "material_peligroso")
	private String materialPeligroso;

	@Column(name = "cve_material_peligroso")
	private String cveMaterialPeligroso;

	@Column(name = "embalaje")
	private String embalaje;

	@Column(name = "descrip_embalaje")
	private String descripEmbalaje;

	@Column(name = "peso_en_kg")
	private BigDecimal pesoEnKg;

	@Column(name = "valor_mercancia")
	private BigDecimal valorMercancia;

	@Column(name = "moneda")
	private String moneda;

	@Column(name = "fraccion_arancelaria")
	private String fraccionArancelaria;

	@Column(name = "uuid_comercio_ext")
	private String uuidComercioExt;

	public Long getIdMercancia() {
		return idMercancia;
	}

	public void setIdMercancia(Long idMercancia) {
		this.idMercancia = idMercancia;
	}

	public List<CantidadTransportada> getCantidadesTransportadas() {
		return cantidadesTransportadas;
	}

	public void setCantidadesTransportadas(List<CantidadTransportada> cantidadesTransportadas) {
		this.cantidadesTransportadas = cantidadesTransportadas;
	}

	public DetalleMercancia getDetalleMercancia() {
		return detalleMercancia;
	}

	public void setDetalleMercancia(DetalleMercancia detalleMercancia) {
		this.detalleMercancia = detalleMercancia;
	}

	public String getBienesTransp() {
		return bienesTransp;
	}

	public void setBienesTransp(String bienesTransp) {
		this.bienesTransp = bienesTransp;
	}

	public String getClaveSTCC() {
		return claveSTCC;
	}

	public void setClaveSTCC(String claveSTCC) {
		this.claveSTCC = claveSTCC;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public String getClaveUnidad() {
		return claveUnidad;
	}

	public void setClaveUnidad(String claveUnidad) {
		this.claveUnidad = claveUnidad;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public String getDimensiones() {
		return dimensiones;
	}

	public void setDimensiones(String dimensiones) {
		this.dimensiones = dimensiones;
	}

	public String getMaterialPeligroso() {
		return materialPeligroso;
	}

	public void setMaterialPeligroso(String materialPeligroso) {
		this.materialPeligroso = materialPeligroso;
	}

	public String getCveMaterialPeligroso() {
		return cveMaterialPeligroso;
	}

	public void setCveMaterialPeligroso(String cveMaterialPeligroso) {
		this.cveMaterialPeligroso = cveMaterialPeligroso;
	}

	public String getEmbalaje() {
		return embalaje;
	}

	public void setEmbalaje(String embalaje) {
		this.embalaje = embalaje;
	}

	public String getDescripEmbalaje() {
		return descripEmbalaje;
	}

	public void setDescripEmbalaje(String descripEmbalaje) {
		this.descripEmbalaje = descripEmbalaje;
	}

	public BigDecimal getPesoEnKg() {
		return pesoEnKg;
	}

	public void setPesoEnKg(BigDecimal pesoEnKg) {
		this.pesoEnKg = pesoEnKg;
	}

	public BigDecimal getValorMercancia() {
		return valorMercancia;
	}

	public void setValorMercancia(BigDecimal valorMercancia) {
		this.valorMercancia = valorMercancia;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFraccionArancelaria() {
		return fraccionArancelaria;
	}

	public void setFraccionArancelaria(String fraccionArancelaria) {
		this.fraccionArancelaria = fraccionArancelaria;
	}

	public String getUuidComercioExt() {
		return uuidComercioExt;
	}

	public void setUuidComercioExt(String uuidComercioExt) {
		this.uuidComercioExt = uuidComercioExt;
	}

	public List<Pedimento> getPedimentos() {
		return pedimentos;
	}

	public void setPedimentos(List<Pedimento> pedimentos) {
		this.pedimentos = pedimentos;
	}

	public List<GuiaIdentificacion> getGuiasIdentificacion() {
		return guiasIdentificacion;
	}

	public void setGuiasIdentificacion(List<GuiaIdentificacion> guiasIdentificacion) {
		this.guiasIdentificacion = guiasIdentificacion;
	}

}
