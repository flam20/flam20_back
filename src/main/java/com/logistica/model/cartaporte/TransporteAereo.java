package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "transportes_aereo_cp")
public class TransporteAereo extends CommonEntity {

	private static final long serialVersionUID = 7350701934320906575L;

	@Id
	@Column(name = "id_transporte_aereo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTransporteAereo;

	@Column(name = "perm_sct")
	private String permSCT;

	@Column(name = "num_permiso_sct")
	private String numPermisoSCT;

	@Column(name = "matricula_aeronave")
	private String matriculaAeronave;

	@Column(name = "nombre_aseg")
	private String nombreAseg;

	@Column(name = "num_poliza_seguro")
	private String numPolizaSeguro;

	@Column(name = "numero_guia")
	private String numeroGuia;

	@Column(name = "lugar_contrato")
	private String lugarContrato;

	@Column(name = "rfc_transportista")
	private String rfcTransportista;

	@Column(name = "codigo_transportista")
	private String codigoTransportista;

	@Column(name = "rfc_embarcador")
	private String rfcEmbarcador;
	
	@Column(name = "num_reg_id_trib_embarc")
	private String numRegIdTribEmbarc;

	@Column(name = "residencia_fiscal_embarc")
	private String residenciaFiscalEmbarc;

	@Column(name = "nombre_embarcador")
	private String nombreEmbarcador;

	public Long getIdTransporteAereo() {
		return idTransporteAereo;
	}

	public void setIdTransporteAereo(Long idTransporteAereo) {
		this.idTransporteAereo = idTransporteAereo;
	}

	public String getPermSCT() {
		return permSCT;
	}

	public void setPermSCT(String permSCT) {
		this.permSCT = permSCT;
	}

	public String getNumPermisoSCT() {
		return numPermisoSCT;
	}

	public void setNumPermisoSCT(String numPermisoSCT) {
		this.numPermisoSCT = numPermisoSCT;
	}

	public String getMatriculaAeronave() {
		return matriculaAeronave;
	}

	public void setMatriculaAeronave(String matriculaAeronave) {
		this.matriculaAeronave = matriculaAeronave;
	}

	public String getNombreAseg() {
		return nombreAseg;
	}

	public void setNombreAseg(String nombreAseg) {
		this.nombreAseg = nombreAseg;
	}

	public String getNumPolizaSeguro() {
		return numPolizaSeguro;
	}

	public void setNumPolizaSeguro(String numPolizaSeguro) {
		this.numPolizaSeguro = numPolizaSeguro;
	}

	public String getNumeroGuia() {
		return numeroGuia;
	}

	public void setNumeroGuia(String numeroGuia) {
		this.numeroGuia = numeroGuia;
	}

	public String getLugarContrato() {
		return lugarContrato;
	}

	public void setLugarContrato(String lugarContrato) {
		this.lugarContrato = lugarContrato;
	}

	public String getRfcTransportista() {
		return rfcTransportista;
	}

	public void setRfcTransportista(String rfcTransportista) {
		this.rfcTransportista = rfcTransportista;
	}

	public String getCodigoTransportista() {
		return codigoTransportista;
	}

	public void setCodigoTransportista(String codigoTransportista) {
		this.codigoTransportista = codigoTransportista;
	}

	public String getRfcEmbarcador() {
		return rfcEmbarcador;
	}

	public void setRfcEmbarcador(String rfcEmbarcador) {
		this.rfcEmbarcador = rfcEmbarcador;
	}

	public String getNumRegIdTribEmbarc() {
		return numRegIdTribEmbarc;
	}

	public void setNumRegIdTribEmbarc(String numRegIdTribEmbarc) {
		this.numRegIdTribEmbarc = numRegIdTribEmbarc;
	}

	public String getResidenciaFiscalEmbarc() {
		return residenciaFiscalEmbarc;
	}

	public void setResidenciaFiscalEmbarc(String residenciaFiscalEmbarc) {
		this.residenciaFiscalEmbarc = residenciaFiscalEmbarc;
	}

	public String getNombreEmbarcador() {
		return nombreEmbarcador;
	}

	public void setNombreEmbarcador(String nombreEmbarcador) {
		this.nombreEmbarcador = nombreEmbarcador;
	}

}
