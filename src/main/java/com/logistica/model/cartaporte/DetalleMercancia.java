package com.logistica.model.cartaporte;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "detalles_mercancia_cp")
public class DetalleMercancia extends CommonEntity {

	private static final long serialVersionUID = -8384410215684314653L;

	@Id
	@Column(name = "id_detalle_mercancia")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDetalleMercancia;

	@Column(name = "unidad_peso_merc")
	private String unidadPesoMerc;

	@Column(name = "peso_bruto")
	private BigDecimal pesoBruto;

	@Column(name = "peso_neto")
	private BigDecimal pesoNeto;

	@Column(name = "peso_tara")
	private BigDecimal pesoTara;

	@Column(name = "num_piezas")
	private Integer numPiezas;

	public Long getIdDetalleMercancia() {
		return idDetalleMercancia;
	}

	public void setIdDetalleMercancia(Long idDetalleMercancia) {
		this.idDetalleMercancia = idDetalleMercancia;
	}

	public String getUnidadPesoMerc() {
		return unidadPesoMerc;
	}

	public void setUnidadPesoMerc(String unidadPesoMerc) {
		this.unidadPesoMerc = unidadPesoMerc;
	}

	public BigDecimal getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(BigDecimal pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public BigDecimal getPesoNeto() {
		return pesoNeto;
	}

	public void setPesoNeto(BigDecimal pesoNeto) {
		this.pesoNeto = pesoNeto;
	}

	public BigDecimal getPesoTara() {
		return pesoTara;
	}

	public void setPesoTara(BigDecimal pesoTara) {
		this.pesoTara = pesoTara;
	}

	public Integer getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(Integer numPiezas) {
		this.numPiezas = numPiezas;
	}

}
