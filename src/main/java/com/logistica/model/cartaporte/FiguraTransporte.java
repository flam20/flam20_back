package com.logistica.model.cartaporte;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "figuras_transporte_cp")
public class FiguraTransporte extends CommonEntity {

	private static final long serialVersionUID = 557969724854627303L;

	@Id
	@Column(name = "id_figura_transporte")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idFiguraTransporte;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<TipoFigura> tiposFigura;

	public Long getIdFiguraTransporte() {
		return idFiguraTransporte;
	}

	public void setIdFiguraTransporte(Long idFiguraTransporte) {
		this.idFiguraTransporte = idFiguraTransporte;
	}

	public List<TipoFigura> getTiposFigura() {
		return tiposFigura;
	}

	public void setTiposFigura(List<TipoFigura> tiposFigura) {
		this.tiposFigura = tiposFigura;
	}

}
