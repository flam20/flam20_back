package com.logistica.model.cartaporte;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "transportes_ferroviario_cp")
public class TransporteFerroviario extends CommonEntity {

	private static final long serialVersionUID = -3265666135911841497L;

	@Id
	@Column(name = "id_transporte_ferroviario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idTransporteFerroviario;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<DerechoDePaso> derechosDePaso;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	List<Carro> carros;

	@Column(name = "tipo_de_servicio")
	private String tipoDeServicio;

	@Column(name = "tipo_de_trafico")
	private String tipoDeTrafico;
	
	@Column(name = "nombre_aseg")
	private String nombreAseg;

	@Column(name = "num_poliza_seguro")
	private String numPolizaSeguro;

	public Long getIdTransporteFerroviario() {
		return idTransporteFerroviario;
	}

	public void setIdTransporteFerroviario(Long idTransporteFerroviario) {
		this.idTransporteFerroviario = idTransporteFerroviario;
	}

	public List<DerechoDePaso> getDerechosDePaso() {
		return derechosDePaso;
	}

	public void setDerechosDePaso(List<DerechoDePaso> derechosDePaso) {
		this.derechosDePaso = derechosDePaso;
	}

	public List<Carro> getCarros() {
		return carros;
	}

	public void setCarros(List<Carro> carros) {
		this.carros = carros;
	}

	public String getTipoDeServicio() {
		return tipoDeServicio;
	}

	public void setTipoDeServicio(String tipoDeServicio) {
		this.tipoDeServicio = tipoDeServicio;
	}

	public String getNombreAseg() {
		return nombreAseg;
	}

	public void setNombreAseg(String nombreAseg) {
		this.nombreAseg = nombreAseg;
	}

	public String getNumPolizaSeguro() {
		return numPolizaSeguro;
	}

	public void setNumPolizaSeguro(String numPolizaSeguro) {
		this.numPolizaSeguro = numPolizaSeguro;
	}

	public String getTipoDeTrafico() {
		return tipoDeTrafico;
	}

	public void setTipoDeTrafico(String tipoDeTrafico) {
		this.tipoDeTrafico = tipoDeTrafico;
	}

}
