package com.logistica.model.cartaporte;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.logistica.model.CommonEntity;

@Entity
@Table(name = "remolques_cp")
public class Remolque extends CommonEntity {

	private static final long serialVersionUID = 7622370991053016811L;

	@Id
	@Column(name = "id_remolque")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRemolque;

	@Column(name = "sub_tipo_rem")
	private String subTipoRem;

	@Column(name = "placa")
	private String placa;

	public Long getIdRemolque() {
		return idRemolque;
	}

	public void setIdRemolque(Long idRemolque) {
		this.idRemolque = idRemolque;
	}

	public String getSubTipoRem() {
		return subTipoRem;
	}

	public void setSubTipoRem(String subTipoRem) {
		this.subTipoRem = subTipoRem;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

}
