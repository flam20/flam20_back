package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "monedas")
public class Moneda extends CommonEntity {

	private static final long serialVersionUID = 2625218163452479296L;

	@Id
	@Column(name = "id_moneda")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMoneda;

	@Column(nullable = true)
	private String descCorta;

	@Column(nullable = true)
	private String descLarga;

	@Column(nullable = true)
	private String simbolo;

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(Long idMoneda) {
		this.idMoneda = idMoneda;
	}

	public String getDescCorta() {
		return descCorta;
	}

	public void setDescCorta(String descCorta) {
		this.descCorta = descCorta;
	}

	public String getDescLarga() {
		return descLarga;
	}

	public void setDescLarga(String descLarga) {
		this.descLarga = descLarga;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

}
