package com.logistica.model;

public class Dashboard {

	int transito;
	int cancelado;
	int cerrado;
	
	public int getTransito() {
		return transito;
	}
	public void setTransito(int transito) {
		this.transito = transito;
	}
	public int getCancelado() {
		return cancelado;
	}
	public void setCancelado(int cancelado) {
		this.cancelado = cancelado;
	}
	public int getCerrado() {
		return cerrado;
	}
	public void setCerrado(int cerrado) {
		this.cerrado = cerrado;
	}
	
	
	
}
