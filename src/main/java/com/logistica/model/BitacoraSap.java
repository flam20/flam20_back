package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "bitacora_sap")
public class BitacoraSap extends CommonEntity {

	private static final long serialVersionUID = 2101529538346573198L;

	@Id
	@Column(name = "id_bitacora_sap")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	Long idBitacoraSap;

	@Column(nullable = false)
	Long idServicio;

	@Column(nullable = false)
	String tipoBitacora;

	@Column(nullable = true)
	String idSAP;

	@Lob
	@Column(nullable = true)
	String notas;

	@Embedded
	private ControlAcceso controlAcceso;

	public BitacoraSap() {
		super();
	}

	public BitacoraSap(Long idServicio, String tipoBitacora, String notas, String idSAP) {
		super();
		this.idServicio = idServicio;
		this.tipoBitacora = tipoBitacora;
		this.notas = notas;
		this.idSAP = idSAP;
	}

	public Long getIdBitacoraSap() {
		return idBitacoraSap;
	}

	public void setIdBitacoraSap(Long idBitacoraSap) {
		this.idBitacoraSap = idBitacoraSap;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getTipoBitacora() {
		return tipoBitacora;
	}

	public void setTipoBitacora(String tipoBitacora) {
		this.tipoBitacora = tipoBitacora;
	}

	public String getIdSAP() {
		return idSAP;
	}

	public void setIdSAP(String idSAP) {
		this.idSAP = idSAP;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
