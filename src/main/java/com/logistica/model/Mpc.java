package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mpcs")
public class Mpc extends CommonEntity {

	private static final long serialVersionUID = -3498899204583039550L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_mpc", nullable = false, unique = true)
	Long idMpc;

	@Column(nullable = true, length = 100)
	String descripcion;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdMpc() {
		return idMpc;
	}

	public void setIdMpc(Long idMpc) {
		this.idMpc = idMpc;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
