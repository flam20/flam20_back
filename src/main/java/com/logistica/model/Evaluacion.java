package com.logistica.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "evaluaciones")
public class Evaluacion extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_evaluacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEvaluacion;

	@Column(nullable = true)
	private Boolean esProveedor;

	@Column(nullable = true)
	private String parentId;

	@Column(nullable = true)
	private String comentarios;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	private List<Equipo> equipos;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	private List<Contacto> contactos;

	@Column(nullable = true)
	private Long diasCredito;

	@Column(nullable = true)
	private String usuario;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> giro;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> sociosComerciales;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> coberturaNacional;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> patiosOperacion;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> codigosHazmat;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> servicios;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> unidadesAutAero;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> puertosArrastre;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> fronterasCruce;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> recursosSeguridad;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> certificacionesEmpresa;

	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<String> certificadoOperadores;

	public Long getIdEvaluacion() {
		return idEvaluacion;
	}

	public void setIdEvaluacion(Long idEvaluacion) {
		this.idEvaluacion = idEvaluacion;
	}

	public boolean isEsProveedor() {
		return esProveedor;
	}

	public void setEsProveedor(boolean esProveedor) {
		this.esProveedor = esProveedor;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}

	public Long getDiasCredito() {
		return diasCredito;
	}

	public void setDiasCredito(Long diasCredito) {
		this.diasCredito = diasCredito;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List<String> getGiro() {
		return giro;
	}

	public void setGiro(List<String> giro) {
		this.giro = giro;
	}

	public List<String> getSociosComerciales() {
		return sociosComerciales;
	}

	public void setSociosComerciales(List<String> sociosComerciales) {
		this.sociosComerciales = sociosComerciales;
	}

	public List<String> getCoberturaNacional() {
		return coberturaNacional;
	}

	public void setCoberturaNacional(List<String> coberturaNacional) {
		this.coberturaNacional = coberturaNacional;
	}

	public List<String> getPatiosOperacion() {
		return patiosOperacion;
	}

	public void setPatiosOperacion(List<String> patiosOperacion) {
		this.patiosOperacion = patiosOperacion;
	}

	public List<String> getCodigosHazmat() {
		return codigosHazmat;
	}

	public void setCodigosHazmat(List<String> codigosHazmat) {
		this.codigosHazmat = codigosHazmat;
	}

	public List<String> getServicios() {
		return servicios;
	}

	public void setServicios(List<String> servicios) {
		this.servicios = servicios;
	}

	public List<String> getUnidadesAutAero() {
		return unidadesAutAero;
	}

	public void setUnidadesAutAero(List<String> unidadesAutAero) {
		this.unidadesAutAero = unidadesAutAero;
	}

	public List<String> getPuertosArrastre() {
		return puertosArrastre;
	}

	public void setPuertosArrastre(List<String> puertosArrastre) {
		this.puertosArrastre = puertosArrastre;
	}

	public List<String> getFronterasCruce() {
		return fronterasCruce;
	}

	public void setFronterasCruce(List<String> fronterasCruce) {
		this.fronterasCruce = fronterasCruce;
	}

	public List<String> getRecursosSeguridad() {
		return recursosSeguridad;
	}

	public void setRecursosSeguridad(List<String> recursosSeguridad) {
		this.recursosSeguridad = recursosSeguridad;
	}

	public List<String> getCertificacionesEmpresa() {
		return certificacionesEmpresa;
	}

	public void setCertificacionesEmpresa(List<String> certificacionesEmpresa) {
		this.certificacionesEmpresa = certificacionesEmpresa;
	}

	public List<String> getCertificadoOperadores() {
		return certificadoOperadores;
	}

	public void setCertificadoOperadores(List<String> certificadoOperadores) {
		this.certificadoOperadores = certificadoOperadores;
	}

}
