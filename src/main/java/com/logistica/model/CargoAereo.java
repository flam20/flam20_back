package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cargos_aereos")
public class CargoAereo extends CommonEntity {

	private static final long serialVersionUID = 7720357506058520704L;

	@Id
	@Column(name = "id_cargo_aereo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idCargoAereo;

	@Column(nullable = true, length = 100)
	String concepto;

	@Column(nullable = true, length = 100)
	String traduccion;

	@Column(nullable = false, length = 10)
	String punto;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdCargoAereo() {
		return idCargoAereo;
	}

	public void setIdCargoAereo(Long idCargoAereo) {
		this.idCargoAereo = idCargoAereo;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getTraduccion() {
		return traduccion;
	}

	public void setTraduccion(String traduccion) {
		this.traduccion = traduccion;
	}

	public String getPunto() {
		return punto;
	}

	public void setPunto(String punto) {
		this.punto = punto;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
