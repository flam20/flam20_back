package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ubicaciones")
public class Ubicacion extends CommonEntity {

	private static final long serialVersionUID = -191203556587580351L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ubicacion", nullable = false, length = 45)
	Long idUbicacion;

	@Column(nullable = true, length = 30)
	String pais;

	@Column(nullable = true, length = 30)
	String abreviaturaPais;

	@Column(nullable = true, length = 30)
	String estado;

	@Column(nullable = true, length = 30)
	String abreviaturaEstado;

	@Column(nullable = true, length = 50)
	String ciudad;

	String label;

	public Long getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(Long idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getAbreviaturaPais() {
		return abreviaturaPais;
	}

	public void setAbreviaturaPais(String abreviaturaPais) {
		this.abreviaturaPais = abreviaturaPais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getAbreviaturaEstado() {
		return abreviaturaEstado;
	}

	public void setAbreviaturaEstado(String abreviaturaEstado) {
		this.abreviaturaEstado = abreviaturaEstado;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getLabel() {
		if (this.ciudad != null && this.ciudad.length() > 0 && this.abreviaturaEstado != null
				&& this.abreviaturaEstado.length() > 0) {
			return this.ciudad + ", " + this.estado + " (" + this.pais + ")";
		} else {
			return "";
		}
	}

	public String getLabelCruce() {
		if (this.ciudad != null && this.ciudad.length() > 0 && this.estado != null && this.estado.length() > 0) {
			return this.ciudad + ", " + this.estado;
		} else {
			return "";
		}
	}

}
