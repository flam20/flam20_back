package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contactos")
public class Contacto extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_contacto")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idContacto;

	@Column(nullable = true)
	private String nombreContacto;

	@Column(nullable = true)
	private String correoElectronico;

	@Column(nullable = true)
	private String telefonoOficina;

	@Column(nullable = true)
	private String telefonoCelular;

	public Contacto() {
		super();
	}

	public Contacto(String nombreContacto, String correoElectronico, String telefonoOficina, String telefonoCelular) {
		super();
		this.nombreContacto = nombreContacto;
		this.correoElectronico = correoElectronico;
		this.telefonoOficina = telefonoOficina;
		this.telefonoCelular = telefonoCelular;
	}

	public Long getIdContacto() {
		return idContacto;
	}

	public void setIdContacto(Long idContacto) {
		this.idContacto = idContacto;
	}

	public String getNombreContacto() {
		return nombreContacto;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getTelefonoOficina() {
		return telefonoOficina;
	}

	public void setTelefonoOficina(String telefonoOficina) {
		this.telefonoOficina = telefonoOficina;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

}
