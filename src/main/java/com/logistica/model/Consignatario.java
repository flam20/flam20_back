package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "consignatarios")
public class Consignatario extends CommonEntity {

	private static final long serialVersionUID = 6253935732568794165L;

	@Id
	@Column(name = "id_consignatario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idConsignatario;

	@Column(nullable = true, length = 100)
	String nombre;

	@Column(nullable = true, length = 100)
	String rfc;

	@Embedded
	private Direccion direccion1;

	@Column(nullable = true, length = 100)
	String contacto;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdConsignatario() {
		return idConsignatario;
	}

	public void setIdConsignatario(Long idConsignatario) {
		this.idConsignatario = idConsignatario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Direccion getDireccion1() {
		return direccion1;
	}

	public void setDireccion1(Direccion direccion1) {
		this.direccion1 = direccion1;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
