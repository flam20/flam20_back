package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "paises")
public class Pais extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_pais")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPais;

	@Column(nullable = false, length = 2)
	private String descCortaPais;

	@Column(nullable = false, length = 100)
	private String descLargaPais;

	public Long getIdPais() {
		return idPais;
	}

	public void setIdPais(Long idPais) {
		this.idPais = idPais;
	}

	public String getDescCortaPais() {
		return descCortaPais;
	}

	public void setDescCortaPais(String descCortaPais) {
		this.descCortaPais = descCortaPais;
	}

	public String getDescLargaPais() {
		return descLargaPais;
	}

	public void setDescLargaPais(String descLargaPais) {
		this.descLargaPais = descLargaPais;
	}

}
