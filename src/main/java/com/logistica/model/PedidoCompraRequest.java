package com.logistica.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PedidoCompraRequest implements Serializable {
	private static final long serialVersionUID = 3668486729876830044L;

	private String idServicio;
	private String poActionCode;

	// PARAMETROS ITEM
	private Long itemID;
	private String itemActionCode;
	private String itemProductId;
	private String itemCurrency;
	private BigDecimal itemPriceValue;
	private String itemServicioFLAM;
	private Date itemDeliveryStartTime;
	private Date itemFechaCarga;
	private Date itemFechaCruce;
	private Date itemFechaDestino;
	private Date itemFechaAgenteAduanal;
	private String itemSupplierId;
//	private String itemConsignatarioOrigen;
//	private String itemConsignatarioCruce;
	private String itemConsignatarioDestino;
	private String itemGuiaHouse;
	private String itemGuiaMaster;
	private String itemNumeroEquipoCarga;
	private String itemPzsPBPCS;
	private String itemRequierePOD;
	private String itemShipper;
	private String itemTipoEquipo;
	private String itemUbicacionCruce;
	private String itemUbicacionDestino;
	private String itemUbicacionOrigen;

	private String itemTextValue;
	private String itemUnidadVentas;

	private boolean cancelItem;

}
