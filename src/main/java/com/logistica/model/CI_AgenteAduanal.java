package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ci_agentesaduanales")
public class CI_AgenteAduanal extends CommonEntity {

	private static final long serialVersionUID = 7977089757834443313L;

	@Id
	@Column(name = "id_agenteAduanal")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idAgenteAduanal;

	@Column(nullable = false, length = 100)
	String nombre;

	@Column(nullable = true, length = 200)
	String direccion;
	
	@Column(nullable = false, length = 15)
	String tipo;

	public Long getIdAgenteAduanal() {
		return idAgenteAduanal;
	}

	public void setIdAgenteAduanal(Long idAgenteAduanal) {
		this.idAgenteAduanal = idAgenteAduanal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
