package com.logistica.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "clientes")
public class Cliente extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_cliente")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCliente;

	@Column(nullable = true, length = 100)
	private String nombre1;

	@Column(nullable = true, length = 100)
	private String nombre2;

	@Column(nullable = true, length = 60)
	private String terminoBusqueda;

	@Column(nullable = true)
	private Boolean bloqueado;

	@Column(nullable = true, length = 500)
	private String motivoBloqueo;

	@Column(nullable = true, length = 15)
	private String rfc;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@Fetch(FetchMode.SUBSELECT)
	private List<Ejecutivo> ejecutivos;

	@Column(nullable = true)
	private Boolean borrado;

	@Column(nullable = true)
	private Boolean aereo;

	@Column(nullable = true)
	private Boolean clientePotencial;

	@Column(nullable = true)
	private String numeroClienteSAP;

	@Embedded
	private ControlAcceso controlAcceso;

	public Cliente() {
	}

	public Cliente(Long idCliente, String nombre1, String nombre2, String terminoBusqueda, String rfc,
			String correoFacturacion, Boolean bloqueado, String motivoBloqueo, Direccion direccion,
			BigDecimal lineaCredito, BigDecimal plazo, FormaPago formaPago) {
		this.idCliente = idCliente;
		this.nombre1 = nombre1;
		this.nombre2 = nombre2;
		this.terminoBusqueda = terminoBusqueda;
		this.bloqueado = bloqueado;
		this.motivoBloqueo = motivoBloqueo;
		this.rfc = rfc;
		this.borrado = false;
		this.aereo = false;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre1(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getNombre2() {
		return nombre2;
	}

	public void setNombre2(String nombre2) {
		this.nombre2 = nombre2;
	}

	public String getTerminoBusqueda() {
		return terminoBusqueda;
	}

	public void setTerminoBusqueda(String terminoBusqueda) {
		this.terminoBusqueda = terminoBusqueda;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Boolean getAereo() {
		return aereo;
	}

	public void setAereo(Boolean aereo) {
		this.aereo = aereo;
	}

	public Boolean getClientePotencial() {
		return clientePotencial;
	}

	public void setClientePotencial(Boolean clientePotencial) {
		this.clientePotencial = clientePotencial;
	}

	public String getNumeroClienteSAP() {
		return numeroClienteSAP;
	}

	public void setNumeroClienteSAP(String numeroClienteSAP) {
		this.numeroClienteSAP = numeroClienteSAP;
	}

	public String getMotivoBloqueo() {
		return motivoBloqueo;
	}

	public void setMotivoBloqueo(String motivoBloqueo) {
		this.motivoBloqueo = motivoBloqueo;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

	public List<Ejecutivo> getEjecutivos() {
		return ejecutivos;
	}

	public void setEjecutivos(List<Ejecutivo> ejecutivos) {
		this.ejecutivos = ejecutivos;
	}

}
