package com.logistica.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PedidoClienteItemRequest implements Serializable {
	private static final long serialVersionUID = 1358617342991796916L;
	private String itemID;
	private String itemProductId;
	private BigDecimal itemPrice;
	private String itemCurrency;
	private String itemVendorId;
	private String itemObservaciones;
}
