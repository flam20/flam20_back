package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "preferencias_export")
public class PreferenciaExport extends CommonEntity {

	private static final long serialVersionUID = -1063306698779131437L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_preferencia", nullable = false)
	Long idPreferencia;

	@OneToOne
	@JoinColumn(name = "id_usuario")
	Usuario trafico;

	@Column(nullable = true)
	String seleccion;

	@Column(nullable = true)
	private String tipo;

	public Long getIdPreferencia() {
		return idPreferencia;
	}

	public void setIdPreferencia(Long idPreferencia) {
		this.idPreferencia = idPreferencia;
	}

	public Usuario getTrafico() {
		return trafico;
	}

	public void setTrafico(Usuario trafico) {
		this.trafico = trafico;
	}

	public String getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
