package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "formas_pago")
public class FormaPago extends CommonEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_forma_pago")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idFormaPago;

	@Column(nullable = true, length = 100)
	private String descripcion;

	@Column(nullable = true)
	private Boolean borrado;

	@Transient
	boolean editingStatus = false;

	public FormaPago() {
	}

	public FormaPago(String descripcion) {
		this.descripcion = descripcion;
		this.borrado = false;
	}

	public Long getIdFormaPago() {
		return idFormaPago;
	}

	public void setIdFormaPago(Long idFormaPago) {
		this.idFormaPago = idFormaPago;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public boolean getEditingStatus() {
		return editingStatus;
	}

	public void setEditingStatus(boolean editingStatus) {
		this.editingStatus = editingStatus;
	}

}
