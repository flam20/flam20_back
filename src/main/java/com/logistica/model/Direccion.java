package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Embeddable
public class Direccion extends CommonEntity {
	private static final long serialVersionUID = 1L;

	@Column(nullable = true, length = 45)
	private String calle;

	@Column(nullable = true, length = 20)
	private String numInterior;

	@Column(nullable = true, length = 20)
	private String numExterior;

	@Column(nullable = true, length = 100)
	private String colonia;

	@Column(nullable = true, length = 15)
	private String cp;

	@Column(nullable = true, length = 100)
	private String ciudad;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_pais")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Pais pais;

	@Column(nullable = true, length = 60)
	private String correoElectronico;

	@Column(nullable = true, length = 60)
	private String telefono;

	public Direccion() {
	}

	public Direccion(String calle, String numInterior, String numExterior, String colonia, String cp, String ciudad,
			Pais pais, String correoElectronico, String telefono) {
		this.calle = calle;
		this.numInterior = numInterior;
		this.numExterior = numExterior;
		this.colonia = colonia;
		this.cp = cp;
		this.ciudad = ciudad;
		this.pais = pais;
		this.correoElectronico = correoElectronico;
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumInterior() {
		return numInterior;
	}

	public void setNumInterior(String numInterior) {
		this.numInterior = numInterior;
	}

	public String getNumExterior() {
		return numExterior;
	}

	public void setNumExterior(String numExterior) {
		this.numExterior = numExterior;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccionCompleta(boolean mx) {
		String direccionCompleta = "";

		if (mx) {
			direccionCompleta = calle != null ? direccionCompleta.concat(calle).concat(", ") : direccionCompleta;
			direccionCompleta = numExterior != null ? direccionCompleta.concat("No." + numExterior).concat(", ")
					: direccionCompleta;
			direccionCompleta = numInterior != null ? direccionCompleta.concat("Int. " + numInterior).concat(", ")
					: direccionCompleta;
			direccionCompleta = colonia != null ? direccionCompleta.concat(colonia).concat(", ") : direccionCompleta;
			direccionCompleta = ciudad != null ? direccionCompleta.concat(ciudad).concat(", ") : direccionCompleta;
			direccionCompleta = pais != null ? direccionCompleta.concat(pais.getDescLargaPais()).concat(", ")
					: direccionCompleta;
		} else {
			direccionCompleta = calle != null ? direccionCompleta.concat(calle).concat(", ") : direccionCompleta;
			direccionCompleta = numInterior != null ? direccionCompleta.concat("#" + numInterior).concat(", ")
					: direccionCompleta;
			direccionCompleta = colonia != null ? direccionCompleta.concat(colonia).concat(", ") : direccionCompleta;
			direccionCompleta = ciudad != null ? direccionCompleta.concat(ciudad).concat(", ") : direccionCompleta;
			direccionCompleta = pais != null ? direccionCompleta.concat(pais.getDescCortaPais()).concat(", ")
					: direccionCompleta;
		}

		return direccionCompleta;
	}

}