package com.logistica.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mensajes")
public class Mensaje extends CommonEntity implements Comparable<Mensaje> {

	private static final long serialVersionUID = 1389341249814373961L;

	@Id
	@Column(name = "id_mensaje")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMensaje;

	@OneToOne
	@JoinColumn(name = "id_usuario_origen")
	private Usuario usuarioOrigen;

	@OneToOne
	@JoinColumn(name = "id_usuario_destino")
	private Usuario usuarioDestino;

	@Column
	private String titulo;

	@Column
	private String texto;

	@Column
	private LocalDateTime fecha;

	@Column
	private Boolean leido;

	@Column
	private Boolean eliminado;

	@Column
	private Long idServicio;

	@Column
	private String tipo;

	public Long getIdMensaje() {
		return idMensaje;
	}

	public void setIdMensaje(Long idMensaje) {
		this.idMensaje = idMensaje;
	}

	public Usuario getUsuarioOrigen() {
		return usuarioOrigen;
	}

	public void setUsuarioOrigen(Usuario usuarioOrigen) {
		this.usuarioOrigen = usuarioOrigen;
	}

	public Usuario getUsuarioDestino() {
		return usuarioDestino;
	}

	public void setUsuarioDestino(Usuario usuarioDestino) {
		this.usuarioDestino = usuarioDestino;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Boolean getLeido() {
		return leido;
	}

	public void setLeido(Boolean leido) {
		this.leido = leido;
	}

	public Boolean getEliminado() {
		return eliminado;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Long idServicio) {
		this.idServicio = idServicio;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int compareTo(Mensaje m) {
		if (fecha.isEqual(m.getFecha())) {
			return 0;
		} else if (fecha.isAfter(m.getFecha())) {
			return 1;
		} else {
			return -1;
		}
	}

}
