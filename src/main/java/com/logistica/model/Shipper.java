package com.logistica.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shippers")
public class Shipper extends CommonEntity {

	private static final long serialVersionUID = 7977089757834443313L;

	@Id
	@Column(name = "id_shipper")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long idShipper;

	@Column(nullable = true, length = 100)
	String nombre;

	@Column(nullable = true, length = 100)
	String contacto;

	@Column(nullable = true, length = 60)
	String terminoBusqueda;

	@Column(nullable = true)
	Boolean bloqueado;

	@Column(nullable = true, length = 500)
	String motivoBloqueo;

	@Column(nullable = true, length = 15)
	String rfc;

	@Column(nullable = true)
	Boolean borrado;

	@Embedded
	private Direccion direccion;

	@Embedded
	private ControlAcceso controlAcceso;

	public Long getIdShipper() {
		return idShipper;
	}

	public void setIdShipper(Long idShipper) {
		this.idShipper = idShipper;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getTerminoBusqueda() {
		return terminoBusqueda;
	}

	public void setTerminoBusqueda(String terminoBusqueda) {
		this.terminoBusqueda = terminoBusqueda;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getMotivoBloqueo() {
		return motivoBloqueo;
	}

	public void setMotivoBloqueo(String motivoBloqueo) {
		this.motivoBloqueo = motivoBloqueo;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Boolean getBorrado() {
		return borrado;
	}

	public void setBorrado(Boolean borrado) {
		this.borrado = borrado;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public ControlAcceso getControlAcceso() {
		return controlAcceso;
	}

	public void setControlAcceso(ControlAcceso controlAcceso) {
		this.controlAcceso = controlAcceso;
	}

}
