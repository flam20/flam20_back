package com.logistica.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Prospecto;
import com.logistica.service.IProspectoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/prospecto")
public class ProspectoRestController {

	@Autowired
	private IProspectoService prospectoService;
	
	@GetMapping("/page/{page}")
	public Page<Prospecto> pageAllProspectos(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return prospectoService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Prospecto> getAllProspectos() {
		return prospectoService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getProspecto(@PathVariable Long id) {
		Prospecto prospecto = null;
		Map<String, Object> response = new HashMap<>();
		try {
			prospecto = prospectoService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (prospecto == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "prospecto", String.valueOf(id), response);
		}
		return new ResponseEntity<Prospecto>(prospecto, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createProspecto(@RequestBody Prospecto prospecto, BindingResult result) {
		Prospecto prospectoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			prospectoNuevo = prospectoService.save(prospecto);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "prospecto", prospectoNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateProspecto(@RequestBody Prospecto prospecto, BindingResult result) {
		Prospecto prospectoActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			prospectoActual = prospectoService.findById(prospecto.getIdProspecto());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (prospectoActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "prospecto", String.valueOf(prospecto.getIdProspecto()),
					response);
		}
		prospectoActual.setIdEvaluacion(prospecto.getIdEvaluacion());
		prospectoActual.setRazonComercial(prospecto.getRazonComercial());
		prospectoActual.setRazonSocial(prospecto.getRazonSocial());

		Prospecto prospectoActualizado = null;
		try {
			prospectoActualizado = prospectoService.save(prospectoActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "prospecto", prospectoActualizado, "actualizado", response);
	}

	@GetMapping("/{idProspecto}/evaluacion/{idEvaluacion}")
	@ResponseStatus
	public ResponseEntity<?> updateEvaluacion(@PathVariable Long idProspecto, @PathVariable Long idEvaluacion) {
		Prospecto prospecto = null;
		Map<String, Object> response = new HashMap<>();

		try {
			prospecto = prospectoService.findById(idProspecto);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (prospecto == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "prospecto", String.valueOf(idProspecto), response);
		}
		prospecto.setIdEvaluacion(idEvaluacion);

		Prospecto prospectoActualizado = null;
		try {
			prospectoActualizado = prospectoService.save(prospecto);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}

		return new ResponseEntity<Prospecto>(prospectoActualizado, HttpStatus.OK);
	}

	@GetMapping(value = "/search/evaluacion", produces = MediaType.ALL_VALUE)
	@ResponseBody
	public List<Prospecto> getEvaluacionProspectos(@RequestParam(value = "giros", required = false) String[] giros,
			@RequestParam(value = "sociosComerciales", required = false) String[] sociosComerciales,
			@RequestParam(value = "coberturas", required = false) String[] coberturas,
			@RequestParam(value = "patios", required = false) String[] patios,
			@RequestParam(value = "codigosHazmat", required = false) String[] codigosHazmat,
			@RequestParam(value = "servicios", required = false) String[] servicios,
			@RequestParam(value = "unidadesAutAero", required = false) String[] unidadesAutAero,
			@RequestParam(value = "puertos", required = false) String[] puertos,
			@RequestParam(value = "fronteras", required = false) String[] fronteras,
			@RequestParam(value = "recursos", required = false) String[] recursos,
			@RequestParam(value = "certificacionesEmpresa", required = false) String[] certificacionesEmpresa,
			@RequestParam(value = "certificacionesOperador", required = false) String[] certificacionesOperador,
			@RequestParam(value = "equipos", required = false) String[] equipos)
			throws IOException {
		return prospectoService.search(giros, sociosComerciales, coberturas, patios, codigosHazmat, servicios,
				unidadesAutAero, puertos, fronteras, recursos, certificacionesEmpresa, certificacionesOperador,
				equipos);
	}
	
}
