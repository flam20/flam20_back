package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.FormaPago;
import com.logistica.service.IFormaPagoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/formaPago")
public class FormaPagoRestController {

	@Autowired
	private IFormaPagoService formaPagoService;

	@GetMapping("/page/{page}")
	public Page<FormaPago> pageAllFormasPago(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return formaPagoService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<FormaPago> getAllFormasPago() {
		return formaPagoService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getFormaPago(@PathVariable Long id) {
		FormaPago formaPago = null;
		Map<String, Object> response = new HashMap<>();
		try {
			formaPago = formaPagoService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (formaPago == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "formaPago", String.valueOf(id), response);
		}
		return new ResponseEntity<FormaPago>(formaPago, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createFormaPago(@RequestBody FormaPago formaPago, BindingResult result) {
		FormaPago formaPagoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			formaPagoNuevo = formaPagoService.save(formaPago);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "formaPago", formaPagoNuevo, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateFormaPago(@RequestBody FormaPago formaPago, BindingResult result) {
		FormaPago formaPagoActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			formaPagoActual = formaPagoService.findById(formaPago.getIdFormaPago());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (formaPagoActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "formaPago", String.valueOf(formaPago.getIdFormaPago()),
					response);
		}
		formaPagoActual.setDescripcion(formaPago.getDescripcion());
		formaPagoActual.setBorrado(formaPago.getBorrado());

		FormaPago formaPagoActualizada = null;
		try {
			formaPagoActualizada = formaPagoService.save(formaPagoActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "formaPago", formaPagoActualizada, "actualizada", response);
	}

}
