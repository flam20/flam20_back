package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.TipoMovimiento;
import com.logistica.service.ITipoMovimientoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/tipoMovimiento")
public class TipoMovimientoRestController {

	@Autowired
	ITipoMovimientoService tipoMovimientoService;

	@GetMapping("/page/{page}")
	public Page<TipoMovimiento> pageAllTiposMovimiento(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return tipoMovimientoService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<TipoMovimiento> getAllTiposMovimiento() {
		return tipoMovimientoService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getTipoMovimiento(@PathVariable Long id) {
		TipoMovimiento tipoMovimiento = null;
		Map<String, Object> response = new HashMap<>();
		try {
			tipoMovimiento = tipoMovimientoService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoMovimiento == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoMovimiento", String.valueOf(id), response);
		}
		return new ResponseEntity<TipoMovimiento>(tipoMovimiento, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createTipoMovimiento(@RequestBody TipoMovimiento tipoMovimiento, BindingResult result) {
		TipoMovimiento tipoMovimientoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoMovimientoNuevo = tipoMovimientoService.save(tipoMovimiento);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoMovimiento", tipoMovimientoNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateTipoMovimiento(@RequestBody TipoMovimiento tipoMovimiento, BindingResult result) {
		TipoMovimiento tipoMovimientoActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoMovimientoActual = tipoMovimientoService.findById(tipoMovimiento.getIdTipoMovimiento());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoMovimientoActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoMovimiento",
					String.valueOf(tipoMovimiento.getIdTipoMovimiento()), response);
		}
		tipoMovimientoActual.setDescripcion(tipoMovimiento.getDescripcion());
		tipoMovimientoActual.setBorrado(tipoMovimiento.getBorrado());

		TipoMovimiento tipoMovimientoActualizado = null;
		try {
			tipoMovimientoActualizado = tipoMovimientoService.save(tipoMovimientoActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoMovimiento", tipoMovimientoActualizado, "actualizado",
				response);
	}

}
