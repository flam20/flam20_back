package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.CI_AgenteAduanal;
import com.logistica.service.ICI_AgenteAduanalService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/ci_agenteaduanal")
public class CI_AgenteAduanalRestController {

	@Autowired
	ICI_AgenteAduanalService service;

	@GetMapping("/page/{page}")
	public Page<CI_AgenteAduanal> pageAllAgentesAduanales(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<CI_AgenteAduanal> getAllAgentesAduanales() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getAgenteAduanal(@PathVariable Long id) {
		CI_AgenteAduanal agenteAduanal = null;
		Map<String, Object> response = new HashMap<>();
		try {
			agenteAduanal = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (agenteAduanal == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "agente aduanal", String.valueOf(id), response);
		}
		return new ResponseEntity<CI_AgenteAduanal>(agenteAduanal, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createAgenteAduanal(@RequestBody CI_AgenteAduanal agenteAduanal, BindingResult result) {
		CI_AgenteAduanal agenteNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			agenteNuevo = service.save(agenteAduanal);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "agente aduanal", agenteNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateAgenteAduanal(@RequestBody CI_AgenteAduanal agente, BindingResult result) {
		CI_AgenteAduanal agenteActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			agenteActual = service.findById(agente.getIdAgenteAduanal());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (agenteActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "agente aduanal",
					String.valueOf(agente.getIdAgenteAduanal()), response);
		}
		agenteActual.setNombre(agente.getNombre());
		agenteActual.setDireccion(agente.getDireccion());
		agenteActual.setTipo(agente.getTipo());

		CI_AgenteAduanal agenteActualizado = null;
		try {
			agenteActualizado = service.save(agenteActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "agente aduanal", agenteActualizado, "actualizado", response);
	}

}
