package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Cargo;
import com.logistica.service.ICargoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/cargo")
public class CargoRestController {

	@Autowired
	ICargoService service;

	@GetMapping("/page/{page}")
	public Page<Cargo> pageAllCargos(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Cargo> getAllCargos() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getCargo(@PathVariable Long id) {
		Cargo cargo = null;
		Map<String, Object> response = new HashMap<>();
		try {
			cargo = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (cargo == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cargo", String.valueOf(id), response);
		}
		return new ResponseEntity<Cargo>(cargo, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createCargo(@RequestBody Cargo cargo, BindingResult result) {
		Cargo cargoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			cargoNuevo = service.save(cargo);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cargo", cargoNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateCargo(@RequestBody Cargo cargo, BindingResult result) {
		Cargo cargoActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			cargoActual = service.findById(cargo.getIdCargo());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (cargoActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cargo", String.valueOf(cargo.getIdCargo()), response);
		}
		cargoActual.setDescCorta(cargo.getDescCorta());
		cargoActual.setDescLarga(cargo.getDescLarga());
		cargoActual.setBorrado(cargo.getBorrado());
		cargoActual.setPosCtoDls(cargo.getPosCtoDls());
		cargoActual.setPosCtoMxn(cargo.getPosCtoMxn());
		cargoActual.setPosVtaDls(cargo.getPosVtaDls());
		cargoActual.setPosVtaMxn(cargo.getPosVtaMxn());

		Cargo cargoActualizado = null;
		try {
			cargoActualizado = service.save(cargoActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cargo", cargoActualizado, "actualizado", response);
	}

}
