package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Evaluacion;
import com.logistica.service.IEvaluacionService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/evaluacion")
public class EvaluacionRestController {

	@Autowired
	private IEvaluacionService evaluacionService;

	@GetMapping("/page/{page}")
	public Page<Evaluacion> pageAllEvaluacioness(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return evaluacionService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Evaluacion> getAllEvaluaciones() {
		return evaluacionService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getEvaluacion(@PathVariable Long id) {
		Evaluacion evaluacion = null;
		Map<String, Object> response = new HashMap<>();
		try {
			evaluacion = evaluacionService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (evaluacion == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "evaluacion", String.valueOf(id), response);
		}
		return new ResponseEntity<Evaluacion>(evaluacion, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createEvaluacion(@RequestBody Evaluacion evaluacion, BindingResult result) {
		Evaluacion evaluacionNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			evaluacionNuevo = evaluacionService.save(evaluacion);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "evaluacion", evaluacionNuevo, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateEvaluacion(@RequestBody Evaluacion evaluacion, BindingResult result) {
		Evaluacion evaluacionActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			evaluacionActual = evaluacionService.findById(evaluacion.getIdEvaluacion());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (evaluacionActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "evaluacion",
					String.valueOf(evaluacion.getIdEvaluacion()), response);
		}
		evaluacionActual.setEsProveedor(evaluacion.isEsProveedor());
		evaluacionActual.setParentId(evaluacion.getParentId());
		evaluacionActual.setComentarios(evaluacion.getComentarios());
		evaluacionActual.setDiasCredito(evaluacion.getDiasCredito());
		evaluacionActual.setUsuario(evaluacion.getUsuario());

		if (evaluacion.getEquipos() != null) {
			evaluacionActual.setEquipos(evaluacion.getEquipos());
		}

		if (evaluacion.getContactos() != null) {
			evaluacionActual.setContactos(evaluacion.getContactos());
		}

		if (evaluacion.getGiro() != null) {
			evaluacionActual.setGiro(evaluacion.getGiro());
		}

		if (evaluacion.getSociosComerciales() != null) {
			evaluacionActual.setSociosComerciales(evaluacion.getSociosComerciales());
		}

		if (evaluacion.getCoberturaNacional() != null) {
			evaluacionActual.setCoberturaNacional(evaluacion.getCoberturaNacional());
		}

		if (evaluacion.getPatiosOperacion() != null) {
			evaluacionActual.setPatiosOperacion(evaluacion.getPatiosOperacion());
		}

		if (evaluacion.getCodigosHazmat() != null) {
			evaluacionActual.setCodigosHazmat(evaluacion.getCodigosHazmat());
		}

		if (evaluacion.getServicios() != null) {
			evaluacionActual.setServicios(evaluacion.getServicios());
		}

		if (evaluacion.getUnidadesAutAero() != null) {
			evaluacionActual.setUnidadesAutAero(evaluacion.getUnidadesAutAero());
		}

		if (evaluacion.getPuertosArrastre() != null) {
			evaluacionActual.setPuertosArrastre(evaluacion.getPuertosArrastre());
		}

		if (evaluacion.getFronterasCruce() != null) {
			evaluacionActual.setFronterasCruce(evaluacion.getFronterasCruce());
		}

		if (evaluacion.getRecursosSeguridad() != null) {
			evaluacionActual.setRecursosSeguridad(evaluacion.getRecursosSeguridad());
		}

		if (evaluacion.getCertificacionesEmpresa() != null) {
			evaluacionActual.setCertificacionesEmpresa(evaluacion.getCertificacionesEmpresa());
		}

		if (evaluacion.getCertificadoOperadores() != null) {
			evaluacionActual.setCertificadoOperadores(evaluacion.getCertificadoOperadores());
		}

		Evaluacion evaluacionActualizada = null;
		try {
			evaluacionActualizada = evaluacionService.save(evaluacionActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "evaluacion", evaluacionActualizada, "actualizada", response);
	}

}
