package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Incoterm;
import com.logistica.service.IIncotermService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/incoterm")
public class IncotermRestController {

	@Autowired
	IIncotermService service;

	@GetMapping("/page/{page}")
	public Page<Incoterm> pageAllIncoterms(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Incoterm> getAllIncoterms() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getIncoterm(@PathVariable Long id) {
		Incoterm incoterm = null;
		Map<String, Object> response = new HashMap<>();
		try {
			incoterm = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (incoterm == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "incoterm", String.valueOf(id), response);
		}
		return new ResponseEntity<Incoterm>(incoterm, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createIncoterm(@RequestBody Incoterm incoterm, BindingResult result) {
		Incoterm incotermNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			incotermNuevo = service.save(incoterm);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "incoterm", incotermNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateIncoterm(@RequestBody Incoterm incoterm, BindingResult result) {
		Incoterm incotermActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			incotermActual = service.findById(incoterm.getIdIncoterm());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (incotermActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "incoterm", String.valueOf(incoterm.getIdIncoterm()),
					response);
		}
		incotermActual.setDescripcion(incoterm.getDescripcion());
		incotermActual.setIniciales(incoterm.getIniciales());
		incotermActual.setBorrado(incoterm.getBorrado());

		Incoterm incotermActualizado = null;
		try {
			incotermActualizado = service.save(incotermActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "incoterm", incotermActualizado, "actualizado", response);
	}

}
