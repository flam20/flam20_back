package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.CargoAereo;
import com.logistica.service.ICargoAereoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/cargoAereo")
public class CargoAereoRestController {

	@Autowired
	ICargoAereoService service;

	@GetMapping("/page/{page}")
	public Page<CargoAereo> pageAll(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<CargoAereo> getAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		CargoAereo bean = null;
		Map<String, Object> response = new HashMap<>();
		try {
			bean = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (bean == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cargoAereo", String.valueOf(id), response);
		}
		return new ResponseEntity<CargoAereo>(bean, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody CargoAereo bean, BindingResult result) {
		CargoAereo nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = service.save(bean);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cargoAereo", nuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody CargoAereo bean, BindingResult result) {
		CargoAereo actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(bean.getIdCargoAereo());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cargoAereo", String.valueOf(bean.getIdCargoAereo()),
					response);
		}
		actual.setConcepto(bean.getConcepto());
		actual.setTraduccion(bean.getTraduccion());
		actual.setBorrado(bean.getBorrado());
		actual.setPunto(bean.getPunto());

		CargoAereo actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cargoAereo", actualizado, "actualizado", response);
	}

}
