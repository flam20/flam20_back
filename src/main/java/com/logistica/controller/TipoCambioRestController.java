package com.logistica.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.dto.AnioMesDTO;
import com.logistica.dto.MesDTO;
import com.logistica.model.TipoCambio;
import com.logistica.service.ITipoCambioService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/tipoCambio")
public class TipoCambioRestController {

	@Autowired
	private ITipoCambioService tipoCambioService;

	@GetMapping("/page/{page}")
	public Page<TipoCambio> pageAllTiposCambios(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return tipoCambioService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<TipoCambio> getAllTiposCambios() {
		return tipoCambioService.findAll();
	}

	@GetMapping("/anioMes/list")
	public List<AnioMesDTO> getAllAniosMeses() {
		List<TipoCambio> listaTiposCambios = getAllTiposCambios();
		if (listaTiposCambios != null && !listaTiposCambios.isEmpty()) {
			Map<String, AnioMesDTO> mapaAniosMeses = new HashMap<>();
			for (TipoCambio tipoCambio : listaTiposCambios) {
				String llave = tipoCambio.getAnioMes().getAnio() + "-" + tipoCambio.getAnioMes().getMes().getIdMes();
				if (!mapaAniosMeses.containsKey(llave)) {
					AnioMesDTO anioMesDTO = new AnioMesDTO();
					anioMesDTO.setAnio(tipoCambio.getAnioMes().getAnio());
					MesDTO mesDTO = new MesDTO();
					mesDTO.setIdMes(tipoCambio.getAnioMes().getMes().getIdMes().intValue());
					mesDTO.setNombreCorto(tipoCambio.getAnioMes().getMes().getNombreCorto());
					mesDTO.setNombreLargo(tipoCambio.getAnioMes().getMes().getNombreLargo());
					anioMesDTO.setMes(mesDTO);
					mapaAniosMeses.put(llave, anioMesDTO);
				}
			}
			return new ArrayList<AnioMesDTO>(mapaAniosMeses.values());
		}
		return null;
	}

	@GetMapping("/{anio}/{mes}")
	public List<TipoCambio> getTiposCambiosByAnioMes(@PathVariable Integer anio, @PathVariable Long mes) {
		return tipoCambioService.findByAnioMes(anio, mes);
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getTipoCambio(@PathVariable Long id) {
		TipoCambio tipoCambio = null;
		Map<String, Object> response = new HashMap<>();
		try {
			tipoCambio = tipoCambioService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoCambio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoCambio", String.valueOf(id), response);
		}
		return new ResponseEntity<TipoCambio>(tipoCambio, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createTipoCambio(@RequestBody TipoCambio tipoCambio, BindingResult result) {
		TipoCambio tipoCambioNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoCambioNuevo = tipoCambioService.save(tipoCambio);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoCambio", tipoCambioNuevo, "creado", response);
	}

	@PostMapping("/list")
	@ResponseStatus
	public ResponseEntity<?> createTiposCambio(@RequestBody List<TipoCambio> tiposCambio, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		List<TipoCambio> tiposCambioNuevos = new ArrayList<>();
		if (tiposCambio != null && !tiposCambio.isEmpty()) {
			for (TipoCambio tipoCambio : tiposCambio) {
				TipoCambio tipoCambioNuevo = null;
				if (result.hasErrors()) {
					return CommonsUtils.generaErrorValidaciones(result, response);
				}
				try {
					tipoCambioNuevo = tipoCambioService.save(tipoCambio);
					tiposCambioNuevos.add(tipoCambioNuevo);
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos",
							response, e);
				}
			}
		}
		return CommonsUtils.generaRespuestaCreacion("Los", "tiposCambio", tiposCambioNuevos, "creados", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateTipoCambio(@RequestBody TipoCambio tipoCambio, BindingResult result) {
		TipoCambio tipoCambioActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoCambioActual = tipoCambioService.findById(tipoCambio.getIdTipoCambio());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoCambioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoCambio",
					String.valueOf(tipoCambio.getIdTipoCambio()), response);
		}
		tipoCambioActual.getAnioMes().setAnio(tipoCambio.getAnioMes().getAnio());
		tipoCambioActual.getAnioMes().setMes(tipoCambio.getAnioMes().getMes());
		tipoCambioActual.setMoneda(tipoCambio.getMoneda());
		tipoCambioActual.setValor(tipoCambio.getValor());

		TipoCambio tipoCambioActualizado = null;
		try {
			tipoCambioActualizado = tipoCambioService.save(tipoCambioActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoCambio", tipoCambioActualizado, "actualizado", response);
	}

	@PutMapping("/list")
	@ResponseStatus
	public ResponseEntity<?> updateTiposCambio(@RequestBody List<TipoCambio> tiposCambio, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
		List<TipoCambio> tiposCambiosActualizados = new ArrayList<>();
		if (tiposCambio != null && !tiposCambio.isEmpty()) {
			for (TipoCambio tipoCambio : tiposCambio) {
				TipoCambio tipoCambioActual = null;
				if (result.hasErrors()) {
					return CommonsUtils.generaErrorValidaciones(result, response);
				}
				try {
					tipoCambioActual = tipoCambioService.findById(tipoCambio.getIdTipoCambio());
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos",
							response, e);
				}
				if (tipoCambioActual == null) {
					return CommonsUtils.generaErrorNoEncontrado("El", "tipoCambio",
							String.valueOf(tipoCambio.getIdTipoCambio()), response);
				}
				tipoCambioActual.getAnioMes().setAnio(tipoCambio.getAnioMes().getAnio());
				tipoCambioActual.getAnioMes().setMes(tipoCambio.getAnioMes().getMes());
				tipoCambioActual.setMoneda(tipoCambio.getMoneda());
				tipoCambioActual.setValor(tipoCambio.getValor());

				TipoCambio tipoCambioActualizado = null;
				try {
					tipoCambioActualizado = tipoCambioService.save(tipoCambioActual);
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos",
							response, e);
				}
				tiposCambiosActualizados.add(tipoCambioActualizado);
			}
		}
		return CommonsUtils.generaRespuestaCreacion("Los", "tiposCambio", tiposCambiosActualizados, "actualizados",
				response);
	}

}
