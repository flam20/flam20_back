package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Mpc;
import com.logistica.service.IMpcService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/mpc")
public class MpcRestController {

	@Autowired
	private IMpcService mpcService;

	@GetMapping("/page/{page}")
	public Page<Mpc> pageAllMpcs(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return mpcService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Mpc> getAllMpcs() {
		return mpcService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getMpc(@PathVariable Long id) {
		Mpc mpc = null;
		Map<String, Object> response = new HashMap<>();
		try {
			mpc = mpcService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (mpc == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "mpc", String.valueOf(id), response);
		}
		return new ResponseEntity<Mpc>(mpc, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createMpc(@RequestBody Mpc mpc, BindingResult result) {
		Mpc mpcNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			mpcNuevo = mpcService.save(mpc);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "mpc", mpcNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateMpc(@RequestBody Mpc mpc, BindingResult result) {
		Mpc mpcActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			mpcActual = mpcService.findById(mpc.getIdMpc());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (mpcActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "mpc", String.valueOf(mpc.getIdMpc()), response);
		}
		mpcActual.setDescripcion(mpc.getDescripcion());
		mpcActual.setBorrado(mpc.getBorrado());

		Mpc mpcActualizado = null;
		try {
			mpcActualizado = mpcService.save(mpcActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "mpc", mpcActualizado, "actualizado", response);
	}

}
