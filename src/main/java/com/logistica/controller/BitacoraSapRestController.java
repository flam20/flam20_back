package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.BitacoraSap;
import com.logistica.service.IBitacoraSapService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/bitacoraSap")
public class BitacoraSapRestController {
	private Logger logger = LoggerFactory.getLogger(BitacoraSapRestController.class);
	@Autowired
	IBitacoraSapService service;

	@GetMapping("/page/{page}")
	public Page<BitacoraSap> pageAll(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<BitacoraSap> getAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		BitacoraSap bean = null;
		Map<String, Object> response = new HashMap<>();
		try {
			bean = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (bean == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "bitacoraSap", String.valueOf(id), response);
		}
		return new ResponseEntity<BitacoraSap>(bean, HttpStatus.OK);
	}

	@GetMapping("/servicio/{idServicio}")
	public List<BitacoraSap> getByIdServicio(@PathVariable Long idServicio) {
		logger.info("BUSCA BITACORA DE SERVICIO:" + idServicio);
		return service.findByIdServicio(idServicio);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody BitacoraSap bean, BindingResult result) {
		BitacoraSap nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = service.save(bean);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "bitacoraSap", nuevo, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody BitacoraSap bean, BindingResult result) {
		BitacoraSap actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(bean.getIdBitacoraSap());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "bitacoraSap", String.valueOf(bean.getIdBitacoraSap()),
					response);
		}
		actual.setIdServicio(bean.getIdServicio());
		actual.setTipoBitacora(bean.getTipoBitacora());
		actual.setIdSAP(bean.getIdSAP());
		actual.setNotas(bean.getNotas());

		BitacoraSap actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "bitacoraSap", actualizado, "actualizado", response);
	}

}
