package com.logistica.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Direccion;
import com.logistica.model.Pais;
import com.logistica.model.Proveedor;
import com.logistica.service.IPaisService;
import com.logistica.service.IProveedorService;
import com.logistica.service.IReporteExcelService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/proveedor")
public class ProveedorRestController {
	static Logger logger = Logger.getLogger(ProveedorRestController.class.getName());
	
	@Autowired
	private IProveedorService proveedorService;

	@Autowired
	private IPaisService paisService;
	
	@Autowired
	IReporteExcelService reporteExcelService;

	@GetMapping("/page/{page}")
	public Page<Proveedor> pageAllProveedors(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return proveedorService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Proveedor> getAllProveedores() {
		return proveedorService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getProveedor(@PathVariable Long id) {
		Proveedor proveedor = null;
		Map<String, Object> response = new HashMap<>();
		try {
			proveedor = proveedorService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (proveedor == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "proveedor", String.valueOf(id), response);
		}
		return new ResponseEntity<Proveedor>(proveedor, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createProveedor(@RequestBody Proveedor proveedor, BindingResult result) {
		Proveedor proveedorNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			proveedorNuevo = proveedorService.save(proveedor);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "proveedor", proveedorNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateProveedor(@RequestBody Proveedor proveedor, BindingResult result) {
		Proveedor proveedorActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			proveedorActual = proveedorService.findById(proveedor.getIdProveedor());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (proveedorActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "proveedor", String.valueOf(proveedor.getIdProveedor()),
					response);
		}
		proveedorActual.setAereo(proveedor.getAereo());
		proveedorActual.setBloqueado(proveedor.getBloqueado());
		proveedorActual.setIdEvaluacion(proveedor.getIdEvaluacion());
		proveedorActual.setRazonComercial(proveedor.getRazonComercial());
		proveedorActual.setRazonSocial(proveedor.getRazonSocial());
		proveedorActual.setRfc(proveedor.getRfc());
		proveedorActual.setTerminoBusqueda(proveedor.getTerminoBusqueda());

		if (proveedorActual.getDireccion() == null) {
			proveedorActual.setDireccion(new Direccion());
		}

		if (proveedor.getDireccion() == null) {
			proveedor.setDireccion(new Direccion());
		}

		proveedorActual.getDireccion().setCalle(proveedor.getDireccion().getCalle());
		proveedorActual.getDireccion().setNumExterior(proveedor.getDireccion().getNumExterior());
		proveedorActual.getDireccion().setNumInterior(proveedor.getDireccion().getNumInterior());
		proveedorActual.getDireccion().setColonia(proveedor.getDireccion().getColonia());
		proveedorActual.getDireccion().setCp(proveedor.getDireccion().getCp());
		proveedorActual.getDireccion().setCiudad(proveedor.getDireccion().getCiudad());
		proveedorActual.getDireccion().setCorreoElectronico(proveedor.getDireccion().getCorreoElectronico());
		proveedorActual.getDireccion().setTelefono(proveedor.getDireccion().getTelefono());

		if (proveedor.getDireccion().getPais() != null
				&& proveedor.getDireccion().getPais().getIdPais().intValue() > 0) {
			Pais paisNuevo = paisService.findById(proveedor.getDireccion().getPais().getIdPais());
			if (paisNuevo != null) {
				proveedorActual.getDireccion().setPais(paisNuevo);
			}
		}

		Proveedor proveedorActualizado = null;
		try {
			proveedorActualizado = proveedorService.save(proveedorActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "proveedor", proveedorActualizado, "actualizado", response);
	}

	@GetMapping("/search")
	public List<Proveedor> getFirst10(@RequestParam(value = "razonComercial", required = false) String razonComercial) {
		return proveedorService.findFirst10ByRazonComercial(razonComercial);
	}

	@GetMapping("/{idProveedor}/evaluacion/{idEvaluacion}")
	@ResponseStatus
	public ResponseEntity<?> updateEvaluacion(@PathVariable Long idProveedor, @PathVariable Long idEvaluacion) {
		Proveedor proveedor = null;
		Map<String, Object> response = new HashMap<>();

		try {
			proveedor = proveedorService.findById(idProveedor);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (proveedor == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "proveedor", String.valueOf(idProveedor), response);
		}
		proveedor.setIdEvaluacion(idEvaluacion);

		Proveedor proveedorActualizado = null;
		try {
			proveedorActualizado = proveedorService.save(proveedor);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}

		return new ResponseEntity<Proveedor>(proveedorActualizado, HttpStatus.OK);
	}

	@GetMapping(value = "/search/evaluacion", produces = MediaType.ALL_VALUE)
	@ResponseBody
	public List<Proveedor> getEvaluacionProveedor(@RequestParam(value = "giros", required = false) String[] giros,
			@RequestParam(value = "sociosComerciales", required = false) String[] sociosComerciales,
			@RequestParam(value = "coberturas", required = false) String[] coberturas,
			@RequestParam(value = "patios", required = false) String[] patios,
			@RequestParam(value = "codigosHazmat", required = false) String[] codigosHazmat,
			@RequestParam(value = "servicios", required = false) String[] servicios,
			@RequestParam(value = "unidadesAutAero", required = false) String[] unidadesAutAero,
			@RequestParam(value = "puertos", required = false) String[] puertos,
			@RequestParam(value = "fronteras", required = false) String[] fronteras,
			@RequestParam(value = "recursos", required = false) String[] recursos,
			@RequestParam(value = "certificacionesEmpresa", required = false) String[] certificacionesEmpresa,
			@RequestParam(value = "certificacionesOperador", required = false) String[] certificacionesOperador,
			@RequestParam(value = "equipos", required = false) String[] equipos)
			throws IOException {
		return proveedorService.search(giros, sociosComerciales, coberturas, patios, codigosHazmat, servicios,
				unidadesAutAero, puertos, fronteras, recursos, certificacionesEmpresa, certificacionesOperador,
				equipos);
	}
	
	
	@GetMapping(value = "/searchProveedorReport", produces = MediaType.ALL_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> getReporteProveedores(@RequestParam(value = "giros", required = false) String[] giros,
			@RequestParam(value = "sociosComerciales", required = false) String[] sociosComerciales,
			@RequestParam(value = "coberturas", required = false) String[] coberturas,
			@RequestParam(value = "patios", required = false) String[] patios,
			@RequestParam(value = "codigosHazmat", required = false) String[] codigosHazmat,
			@RequestParam(value = "servicios", required = false) String[] servicios,
			@RequestParam(value = "unidadesAutAero", required = false) String[] unidadesAutAero,
			@RequestParam(value = "puertos", required = false) String[] puertos,
			@RequestParam(value = "fronteras", required = false) String[] fronteras,
			@RequestParam(value = "recursos", required = false) String[] recursos,
			@RequestParam(value = "certificacionesEmpresa", required = false) String[] certificacionesEmpresa,
			@RequestParam(value = "certificacionesOperador", required = false) String[] certificacionesOperador,
			@RequestParam(value = "equipos", required = false) String[] equipos)
			throws IOException {
		List<Proveedor> proveedores =  proveedorService.search(giros, sociosComerciales, coberturas, patios, codigosHazmat, servicios,
				unidadesAutAero, puertos, fronteras, recursos, certificacionesEmpresa, certificacionesOperador, equipos);
		
		logger.info("Se encontraron " + proveedores.size() + " proveedores");

		if (proveedores != null && !proveedores.isEmpty()) {

			File fileExcel = reporteExcelService.exportProveedoresToExcel(proveedores);
			Resource resource = new UrlResource(fileExcel.toURI());

			Path path = resource.getFile().toPath();

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);

		} else {
			logger.error("NO SE ENCONTRARON SERVICIOS");
			return null;
		}
		
		
	}

}
