package com.logistica.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Mensaje;
import com.logistica.model.Usuario;
import com.logistica.service.IMensajeService;
import com.logistica.service.IUsuarioService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/mensaje")
public class MensajeRestController {

	@Autowired
	IMensajeService mensajeService;

	@Autowired
	IUsuarioService usuarioService;

	@GetMapping("/{idUsuario}")
	public List<Mensaje> getMensajesByUsuario(@PathVariable Long idUsuario) {
		return mensajeService.findMensajesByUsuarioDestino(idUsuario);
	}

	@GetMapping("/{idUsuario}/{tipo}/{idServicio}")
	public List<Mensaje> getMensajesByUsuarioAndTipoAndServicio(@PathVariable Long idUsuario, @PathVariable String tipo,
			@PathVariable Long idServicio) {
		List<Mensaje> mensajes = mensajeService.findMensajesByUsuarioAndTipoAndServicio(idUsuario, tipo, idServicio);
		if (mensajes != null && !mensajes.isEmpty()) {
			Map<String, Mensaje> mapaMensajes = new HashMap<>();
			for (Mensaje mensaje : mensajes) {
				String llave = mensaje.getUsuarioOrigen().getIdUsuario() + mensaje.getTexto().trim();
				if (!mapaMensajes.containsKey(llave)) {
					mapaMensajes.put(llave, mensaje);
				}
			}
			mensajes = new ArrayList<Mensaje>(mapaMensajes.values());
			Collections.sort(mensajes);
		}
		return mensajes;
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createMensaje(@RequestBody Mensaje mensaje, BindingResult result) {
		Mensaje mensajeNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			mensaje.setFecha(LocalDateTime.now());
			mensaje.setLeido(Boolean.FALSE);
			mensaje.setEliminado(Boolean.FALSE);

			if (mensaje.getIdServicio() != null && mensaje.getUsuarioDestino() == null) {
				List<Usuario> usuariosCP = usuarioService.findCuentasPorPagarAndAdministracion();
				if (usuariosCP != null && !usuariosCP.isEmpty()) {
					for (Usuario usuario : usuariosCP) {
						Mensaje mensajeCP = new Mensaje();
						mensajeCP.setUsuarioOrigen(mensaje.getUsuarioOrigen());
						mensajeCP.setUsuarioDestino(usuario);
						mensajeCP.setTitulo(mensaje.getTitulo());
						mensajeCP.setTexto(mensaje.getTexto());
						mensajeCP.setFecha(mensaje.getFecha());
						mensajeCP.setLeido(mensaje.getLeido());
						mensajeCP.setEliminado(mensaje.getEliminado());
						mensajeCP.setIdServicio(mensaje.getIdServicio());
						mensajeCP.setTipo(mensaje.getTipo());
						mensajeNuevo = mensajeService.save(mensajeCP);
					}
				}
			} else {
				mensajeNuevo = mensajeService.save(mensaje);
			}
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "mensaje", mensajeNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateMensaje(@RequestBody Mensaje mensaje, BindingResult result) {
		Mensaje mensajeActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			mensajeActual = mensajeService.findById(mensaje.getIdMensaje());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (mensajeActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "mensaje", String.valueOf(mensaje.getIdMensaje()),
					response);
		}
		mensajeActual.setTitulo(mensaje.getTitulo());
		mensajeActual.setTexto(mensaje.getTexto());
		mensajeActual.setFecha(LocalDateTime.now());
		mensajeActual.setLeido(mensaje.getLeido());
		mensajeActual.setEliminado(mensaje.getEliminado());

		Mensaje mensajeActualizado = null;
		try {
			mensajeActualizado = mensajeService.save(mensajeActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "mensaje", mensajeActualizado, "actualizado", response);
	}

}
