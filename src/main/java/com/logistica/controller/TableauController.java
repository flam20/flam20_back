package com.logistica.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.service.ITableauService;
import com.logistica.util.CommonsUtils;
import com.nimbusds.jose.JOSEException;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/tableau")
public class TableauController {

	@Autowired
	ITableauService tableauService;
	
	@GetMapping("/auth/{user}")
	public ResponseEntity<?> authenticate(@PathVariable String user) {
		Map<String, Object> response = new HashMap<>();
		try {
			String token = tableauService.getToken(user);
			if(token != null) {
				response.put("token", token);
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
			}
		} catch (JOSEException e) {
			e.printStackTrace();
			return CommonsUtils.generaError("Error al obtener el token de Tableau", response, e);
		}
		return CommonsUtils.generaError("Error al obtener el token de Tableau", "TABLEAU_TOKEN", response);
	}
	
}
