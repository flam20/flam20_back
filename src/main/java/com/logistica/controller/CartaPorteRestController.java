package com.logistica.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;
import com.logistica.model.cartaporte.CantidadTransportada;
import com.logistica.model.cartaporte.Carro;
import com.logistica.model.cartaporte.CartaPorte;
import com.logistica.model.cartaporte.ContenedorMaritimo;
import com.logistica.model.cartaporte.DerechoDePaso;
import com.logistica.model.cartaporte.GuiaIdentificacion;
import com.logistica.model.cartaporte.Mercancia;
import com.logistica.model.cartaporte.ParteTransporte;
import com.logistica.model.cartaporte.Pedimento;
import com.logistica.model.cartaporte.Remolque;
import com.logistica.model.cartaporte.Seguro;
import com.logistica.model.cartaporte.TipoFigura;
import com.logistica.model.cartaporte.UbicacionCp;
import com.logistica.service.IAutotransporteService;
import com.logistica.service.ICantidadTransportadaService;
import com.logistica.service.ICarroService;
import com.logistica.service.ICartaPorteService;
import com.logistica.service.IContenedorFerroviarioService;
import com.logistica.service.IContenedorMaritimoService;
import com.logistica.service.IDerechoDePasoService;
import com.logistica.service.IDetalleMercanciaService;
import com.logistica.service.IDomicilioService;
import com.logistica.service.IFiguraTransporteService;
import com.logistica.service.IGuiaIdentificacionService;
import com.logistica.service.IIdentificacionVehicularService;
import com.logistica.service.IMercanciaService;
import com.logistica.service.IMercanciasService;
import com.logistica.service.IParteTransporteService;
import com.logistica.service.IPedimentoService;
import com.logistica.service.IRemolqueService;
import com.logistica.service.ISeguroService;
import com.logistica.service.IServicioInterService;
import com.logistica.service.IServicioService;
import com.logistica.service.ITipoFiguraService;
import com.logistica.service.ITransporteAereoService;
import com.logistica.service.ITransporteFerroviarioService;
import com.logistica.service.ITransporteMaritimoService;
import com.logistica.service.IUbicacionCpService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/cartaPorte")
public class CartaPorteRestController {

	@Autowired
	ICartaPorteService service;

	@Autowired
	IFiguraTransporteService figuraService;

	@Autowired
	IMercanciasService mercanciasService;

	@Autowired
	IAutotransporteService autotransporteService;

	@Autowired
	ITransporteMaritimoService maritimoService;

	@Autowired
	ITransporteAereoService aereoService;

	@Autowired
	ITransporteFerroviarioService ferroviarioService;

	@Autowired
	IIdentificacionVehicularService identifcacionService;

	@Autowired
	IServicioService servicioService;

	@Autowired
	IServicioInterService servicioInterService;

	@Autowired
	IGuiaIdentificacionService guiaIdentificacionService;

	@Autowired
	IDomicilioService domicilioService;

	@Autowired
	IUbicacionCpService ubicacionCpService;

	@Autowired
	IMercanciaService mercanciaService;

	@Autowired
	IPedimentoService pedimentoService;

	@Autowired
	ICantidadTransportadaService cantidadService;

	@Autowired
	IDetalleMercanciaService detalleService;

	@Autowired
	ISeguroService seguroService;

	@Autowired
	IRemolqueService remolqueService;

	@Autowired
	IContenedorMaritimoService contenedorService;

	@Autowired
	IDerechoDePasoService derechoService;

	@Autowired
	ICarroService carroService;

	@Autowired
	ITipoFiguraService tipoFiguraService;

	@Autowired
	IParteTransporteService propietarioService;

	@Autowired
	IParteTransporteService parteTransporteService;

	@Autowired
	IContenedorFerroviarioService notificadoService;

	@GetMapping("/page/{page}")
	public Page<CartaPorte> pageAllCartasPorte(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<CartaPorte> getAllCartasPorte() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getCartaPorte(@PathVariable Long id) {
		CartaPorte cartaPorte = null;
		Map<String, Object> response = new HashMap<>();
		try {
			cartaPorte = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (cartaPorte == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cartaPorte", String.valueOf(id), response);
		}
		return new ResponseEntity<CartaPorte>(cartaPorte, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createCartaPorte(@RequestBody CartaPorte cartaPorte, BindingResult result) {
		CartaPorte nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			if (cartaPorte.getUbicaciones() != null && !cartaPorte.getUbicaciones().isEmpty()) {
				List<UbicacionCp> listaUbicaciones = new ArrayList<>();
				for (UbicacionCp ubicacion : cartaPorte.getUbicaciones()) {
					if (ubicacion.getDomicilio() != null) {
						ubicacion.setDomicilio(domicilioService.save(ubicacion.getDomicilio()));
					}
					listaUbicaciones.add(ubicacionCpService.save(ubicacion));
				}
				cartaPorte.setUbicaciones(listaUbicaciones);
			}

			if (cartaPorte.getMercancias() != null && cartaPorte.getMercancias().getMercancia() != null
					&& !cartaPorte.getMercancias().getMercancia().isEmpty()) {
				List<Mercancia> listaMercancias = new ArrayList<>();
				for (Mercancia mercancia : cartaPorte.getMercancias().getMercancia()) {
					if (mercancia.getPedimentos() != null && !mercancia.getPedimentos().isEmpty()) {
						List<Pedimento> listaPedimentos = new ArrayList<>();
						for (Pedimento pedimento : mercancia.getPedimentos()) {
							listaPedimentos.add(pedimentoService.save(pedimento));
						}
						mercancia.setPedimentos(listaPedimentos);
					}
					if (mercancia.getGuiasIdentificacion() != null && !mercancia.getGuiasIdentificacion().isEmpty()) {
						List<GuiaIdentificacion> listaGuiasIdentificacion = new ArrayList<>();
						for (GuiaIdentificacion guiaIdentificacion : mercancia.getGuiasIdentificacion()) {
							listaGuiasIdentificacion.add(guiaIdentificacionService.save(guiaIdentificacion));
						}
						mercancia.setGuiasIdentificacion(listaGuiasIdentificacion);
					}
					if (mercancia.getCantidadesTransportadas() != null
							&& !mercancia.getCantidadesTransportadas().isEmpty()) {
						List<CantidadTransportada> listaCantidades = new ArrayList<>();
						for (CantidadTransportada cantidad : mercancia.getCantidadesTransportadas()) {
							listaCantidades.add(cantidadService.save(cantidad));
						}
						mercancia.setCantidadesTransportadas(listaCantidades);
					}
					detalleService.save(mercancia.getDetalleMercancia());
					listaMercancias.add(mercanciaService.save(mercancia));
				}
				cartaPorte.getMercancias().setMercancia(listaMercancias);
			}

			identifcacionService.save(cartaPorte.getMercancias().getAutotransporte().getIdentificacionVehicular());
			if (cartaPorte.getMercancias().getAutotransporte().getSeguros() != null
					&& !cartaPorte.getMercancias().getAutotransporte().getSeguros().isEmpty()) {
				List<Seguro> listaSeguros = new ArrayList<>();
				for (Seguro seguro : cartaPorte.getMercancias().getAutotransporte().getSeguros()) {
					listaSeguros.add(seguroService.save(seguro));
				}
				cartaPorte.getMercancias().getAutotransporte().setSeguros(listaSeguros);
			}
			if (cartaPorte.getMercancias().getAutotransporte().getRemolques() != null
					&& !cartaPorte.getMercancias().getAutotransporte().getRemolques().isEmpty()) {
				List<Remolque> listaRemolques = new ArrayList<>();
				for (Remolque remolque : cartaPorte.getMercancias().getAutotransporte().getRemolques()) {
					listaRemolques.add(remolqueService.save(remolque));
				}
				cartaPorte.getMercancias().getAutotransporte().setRemolques(listaRemolques);
			}
			autotransporteService.save(cartaPorte.getMercancias().getAutotransporte());

			if (cartaPorte.getMercancias().getTransporteMaritimo().getContenedores() != null
					&& !cartaPorte.getMercancias().getTransporteMaritimo().getContenedores().isEmpty()) {
				List<ContenedorMaritimo> listaContenedores = new ArrayList<>();
				for (ContenedorMaritimo contenedor : cartaPorte.getMercancias().getTransporteMaritimo()
						.getContenedores()) {
					listaContenedores.add(contenedorService.save(contenedor));
				}
				cartaPorte.getMercancias().getTransporteMaritimo().setContenedores(listaContenedores);
			}
			maritimoService.save(cartaPorte.getMercancias().getTransporteMaritimo());

			aereoService.save(cartaPorte.getMercancias().getTransporteAereo());

			if (cartaPorte.getMercancias().getTransporteFerroviario().getDerechosDePaso() != null
					&& !cartaPorte.getMercancias().getTransporteFerroviario().getDerechosDePaso().isEmpty()) {
				List<DerechoDePaso> listaDerechos = new ArrayList<>();
				for (DerechoDePaso derecho : cartaPorte.getMercancias().getTransporteFerroviario()
						.getDerechosDePaso()) {
					listaDerechos.add(derechoService.save(derecho));
				}
				cartaPorte.getMercancias().getTransporteFerroviario().setDerechosDePaso(listaDerechos);
			}
			if (cartaPorte.getMercancias().getTransporteFerroviario().getCarros() != null
					&& !cartaPorte.getMercancias().getTransporteFerroviario().getCarros().isEmpty()) {
				List<Carro> listaCarros = new ArrayList<>();
				for (Carro carro : cartaPorte.getMercancias().getTransporteFerroviario().getCarros()) {
					listaCarros.add(carroService.save(carro));
				}
				cartaPorte.getMercancias().getTransporteFerroviario().setCarros(listaCarros);
			}
			ferroviarioService.save(cartaPorte.getMercancias().getTransporteFerroviario());

			mercanciasService.save(cartaPorte.getMercancias());

			if (cartaPorte.getFiguraTransporte().getTiposFigura() != null
					&& !cartaPorte.getFiguraTransporte().getTiposFigura().isEmpty()) {
				List<TipoFigura> listaTiposFigura = new ArrayList<>();
				for (TipoFigura tipoFigura : cartaPorte.getFiguraTransporte().getTiposFigura()) {
					if (tipoFigura.getPartesTransporte() != null && !tipoFigura.getPartesTransporte().isEmpty()) {
						List<ParteTransporte> listaPartesTransporte = new ArrayList<>();
						for (ParteTransporte parteTransporte : tipoFigura.getPartesTransporte()) {
							domicilioService.save(parteTransporte.getDomicilio());
							listaPartesTransporte.add(parteTransporteService.save(parteTransporte));
						}
						tipoFigura.setPartesTransporte(listaPartesTransporte);
					}
					listaTiposFigura.add(tipoFiguraService.save(tipoFigura));
				}
				cartaPorte.getFiguraTransporte().setTiposFigura(listaTiposFigura);
			}

			figuraService.save(cartaPorte.getFiguraTransporte());

			nuevo = service.save(cartaPorte);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cartaPorte", nuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateCartaPorte(@RequestBody CartaPorte cartaPorte, BindingResult result) {
		CartaPorte actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(cartaPorte.getIdCartaPorte());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cartaPorte",
					String.valueOf(cartaPorte.getIdCartaPorte()), response);
		}
		if (cartaPorte.getUbicaciones() != null && !cartaPorte.getUbicaciones().isEmpty()) {
			List<UbicacionCp> listaUbicaciones = new ArrayList<>();
			for (UbicacionCp ubicacion : cartaPorte.getUbicaciones()) {
				if (ubicacion.getDomicilio() != null) {
					ubicacion.setDomicilio(domicilioService.save(ubicacion.getDomicilio()));
				}
				listaUbicaciones.add(ubicacionCpService.save(ubicacion));
			}
			actual.setUbicaciones(listaUbicaciones);
		}

		actual.getMercancias().setPesoBrutoTotal(cartaPorte.getMercancias().getPesoBrutoTotal());
		actual.getMercancias().setUnidadPeso(cartaPorte.getMercancias().getUnidadPeso());
		actual.getMercancias().setPesoNetoTotal(cartaPorte.getMercancias().getPesoNetoTotal());
		actual.getMercancias().setNumTotalMercancias(cartaPorte.getMercancias().getNumTotalMercancias());
		actual.getMercancias().setCargoPorTasacion(cartaPorte.getMercancias().getCargoPorTasacion());

		if (cartaPorte.getMercancias() != null && cartaPorte.getMercancias().getMercancia() != null
				&& !cartaPorte.getMercancias().getMercancia().isEmpty()) {
			List<Mercancia> listaMercancias = new ArrayList<>();
			for (Mercancia mercancia : cartaPorte.getMercancias().getMercancia()) {
				if (mercancia.getPedimentos() != null && !mercancia.getPedimentos().isEmpty()) {
					List<Pedimento> listaPedimentos = new ArrayList<>();
					for (Pedimento pedimento : mercancia.getPedimentos()) {
						listaPedimentos.add(pedimentoService.save(pedimento));
					}
					mercancia.setPedimentos(listaPedimentos);
				}
				if (mercancia.getGuiasIdentificacion() != null && !mercancia.getGuiasIdentificacion().isEmpty()) {
					List<GuiaIdentificacion> listaGuiasIdentificacion = new ArrayList<>();
					for (GuiaIdentificacion guiaIdentificacion : mercancia.getGuiasIdentificacion()) {
						listaGuiasIdentificacion.add(guiaIdentificacionService.save(guiaIdentificacion));
					}
					mercancia.setGuiasIdentificacion(listaGuiasIdentificacion);
				}
				if (mercancia.getCantidadesTransportadas() != null
						&& !mercancia.getCantidadesTransportadas().isEmpty()) {
					List<CantidadTransportada> listaCantidades = new ArrayList<>();
					for (CantidadTransportada cantidad : mercancia.getCantidadesTransportadas()) {
						listaCantidades.add(cantidadService.save(cantidad));
					}
					mercancia.setCantidadesTransportadas(listaCantidades);
				}
				detalleService.save(mercancia.getDetalleMercancia());
				listaMercancias.add(mercanciaService.save(mercancia));
			}
			actual.getMercancias().setMercancia(listaMercancias);
		}

		actual.getMercancias().getAutotransporte().getIdentificacionVehicular().setConfigVehicular(
				cartaPorte.getMercancias().getAutotransporte().getIdentificacionVehicular().getConfigVehicular());
		actual.getMercancias().getAutotransporte().getIdentificacionVehicular()
				.setPlacaVM(cartaPorte.getMercancias().getAutotransporte().getIdentificacionVehicular().getPlacaVM());
		actual.getMercancias().getAutotransporte().getIdentificacionVehicular().setAnioModeloVM(
				cartaPorte.getMercancias().getAutotransporte().getIdentificacionVehicular().getAnioModeloVM());

		actual.getMercancias().getAutotransporte()
				.setPermSCT(cartaPorte.getMercancias().getAutotransporte().getPermSCT());
		actual.getMercancias().getAutotransporte()
				.setNumPermisoSCT(cartaPorte.getMercancias().getAutotransporte().getNumPermisoSCT());

		if (cartaPorte.getMercancias().getAutotransporte().getSeguros() != null
				&& !cartaPorte.getMercancias().getAutotransporte().getSeguros().isEmpty()) {
			List<Seguro> listaSeguros = new ArrayList<>();
			for (Seguro seguro : cartaPorte.getMercancias().getAutotransporte().getSeguros()) {
				listaSeguros.add(seguroService.save(seguro));
			}
			actual.getMercancias().getAutotransporte().setSeguros(listaSeguros);
		}

		if (cartaPorte.getMercancias().getAutotransporte().getRemolques() != null
				&& !cartaPorte.getMercancias().getAutotransporte().getRemolques().isEmpty()) {
			List<Remolque> listaRemolques = new ArrayList<>();
			for (Remolque remolque : cartaPorte.getMercancias().getAutotransporte().getRemolques()) {
				listaRemolques.add(remolqueService.save(remolque));
			}
			actual.getMercancias().getAutotransporte().setRemolques(listaRemolques);
		}

		actual.getMercancias().getTransporteMaritimo()
				.setPermSCT(cartaPorte.getMercancias().getTransporteMaritimo().getPermSCT());
		actual.getMercancias().getTransporteMaritimo()
				.setNumPermisoSCT(cartaPorte.getMercancias().getTransporteMaritimo().getNumPermisoSCT());
		actual.getMercancias().getTransporteMaritimo()
				.setNombreAseg(cartaPorte.getMercancias().getTransporteMaritimo().getNombreAseg());
		actual.getMercancias().getTransporteMaritimo()
				.setNumPolizaSeguro(cartaPorte.getMercancias().getTransporteMaritimo().getNumPolizaSeguro());
		actual.getMercancias().getTransporteMaritimo()
				.setTipoEmbarcacion(cartaPorte.getMercancias().getTransporteMaritimo().getTipoEmbarcacion());
		actual.getMercancias().getTransporteMaritimo()
				.setMatricula(cartaPorte.getMercancias().getTransporteMaritimo().getMatricula());
		actual.getMercancias().getTransporteMaritimo()
				.setNumeroOMI(cartaPorte.getMercancias().getTransporteMaritimo().getNumeroOMI());
		actual.getMercancias().getTransporteMaritimo()
				.setAnioEmbarcacion(cartaPorte.getMercancias().getTransporteMaritimo().getAnioEmbarcacion());
		actual.getMercancias().getTransporteMaritimo()
				.setNombreEmbarc(cartaPorte.getMercancias().getTransporteMaritimo().getNombreEmbarc());
		actual.getMercancias().getTransporteMaritimo()
				.setNacionalidadEmbarc(cartaPorte.getMercancias().getTransporteMaritimo().getNacionalidadEmbarc());
		actual.getMercancias().getTransporteMaritimo()
				.setUnidadesDeArqBruto(cartaPorte.getMercancias().getTransporteMaritimo().getUnidadesDeArqBruto());
		actual.getMercancias().getTransporteMaritimo()
				.setTipoCarga(cartaPorte.getMercancias().getTransporteMaritimo().getTipoCarga());
		actual.getMercancias().getTransporteMaritimo()
				.setNumCertITC(cartaPorte.getMercancias().getTransporteMaritimo().getNumCertITC());
		actual.getMercancias().getTransporteMaritimo()
				.setEslora(cartaPorte.getMercancias().getTransporteMaritimo().getEslora());
		actual.getMercancias().getTransporteMaritimo()
				.setManga(cartaPorte.getMercancias().getTransporteMaritimo().getManga());
		actual.getMercancias().getTransporteMaritimo()
				.setCalado(cartaPorte.getMercancias().getTransporteMaritimo().getCalado());
		actual.getMercancias().getTransporteMaritimo()
				.setLineaNaviera(cartaPorte.getMercancias().getTransporteMaritimo().getLineaNaviera());
		actual.getMercancias().getTransporteMaritimo()
				.setNombreAgenteNaviero(cartaPorte.getMercancias().getTransporteMaritimo().getNombreAgenteNaviero());
		actual.getMercancias().getTransporteMaritimo().setNumAutorizacionNaviero(
				cartaPorte.getMercancias().getTransporteMaritimo().getNumAutorizacionNaviero());
		actual.getMercancias().getTransporteMaritimo()
				.setNumViaje(cartaPorte.getMercancias().getTransporteMaritimo().getNumViaje());
		actual.getMercancias().getTransporteMaritimo()
				.setNumConocEmbarc(cartaPorte.getMercancias().getTransporteMaritimo().getNumConocEmbarc());

		if (cartaPorte.getMercancias().getTransporteMaritimo().getContenedores() != null
				&& !cartaPorte.getMercancias().getTransporteMaritimo().getContenedores().isEmpty()) {
			List<ContenedorMaritimo> listaContenedores = new ArrayList<>();
			for (ContenedorMaritimo contenedor : cartaPorte.getMercancias().getTransporteMaritimo().getContenedores()) {
				listaContenedores.add(contenedorService.save(contenedor));
			}
			actual.getMercancias().getTransporteMaritimo().setContenedores(listaContenedores);
		}

		actual.getMercancias().getTransporteAereo()
				.setPermSCT(cartaPorte.getMercancias().getTransporteAereo().getPermSCT());
		actual.getMercancias().getTransporteAereo()
				.setNumPermisoSCT(cartaPorte.getMercancias().getTransporteAereo().getNumPermisoSCT());
		actual.getMercancias().getTransporteAereo()
				.setMatriculaAeronave(cartaPorte.getMercancias().getTransporteAereo().getMatriculaAeronave());
		actual.getMercancias().getTransporteAereo()
				.setNombreAseg(cartaPorte.getMercancias().getTransporteAereo().getNombreAseg());
		actual.getMercancias().getTransporteAereo()
				.setNumPolizaSeguro(cartaPorte.getMercancias().getTransporteAereo().getNumPolizaSeguro());
		actual.getMercancias().getTransporteAereo()
				.setNumeroGuia(cartaPorte.getMercancias().getTransporteAereo().getNumeroGuia());
		actual.getMercancias().getTransporteAereo()
				.setLugarContrato(cartaPorte.getMercancias().getTransporteAereo().getLugarContrato());
		actual.getMercancias().getTransporteAereo()
				.setRfcTransportista(cartaPorte.getMercancias().getTransporteAereo().getRfcTransportista());
		actual.getMercancias().getTransporteAereo()
				.setCodigoTransportista(cartaPorte.getMercancias().getTransporteAereo().getCodigoTransportista());
		actual.getMercancias().getTransporteAereo()
				.setRfcEmbarcador(cartaPorte.getMercancias().getTransporteAereo().getRfcEmbarcador());
		actual.getMercancias().getTransporteAereo()
				.setNumRegIdTribEmbarc(cartaPorte.getMercancias().getTransporteAereo().getNumRegIdTribEmbarc());
		actual.getMercancias().getTransporteAereo()
				.setResidenciaFiscalEmbarc(cartaPorte.getMercancias().getTransporteAereo().getResidenciaFiscalEmbarc());
		actual.getMercancias().getTransporteAereo()
				.setNombreEmbarcador(cartaPorte.getMercancias().getTransporteAereo().getNombreEmbarcador());

		actual.getMercancias().getTransporteFerroviario()
				.setTipoDeServicio(cartaPorte.getMercancias().getTransporteFerroviario().getTipoDeServicio());
		actual.getMercancias().getTransporteFerroviario()
				.setTipoDeTrafico(cartaPorte.getMercancias().getTransporteFerroviario().getTipoDeTrafico());
		actual.getMercancias().getTransporteFerroviario()
				.setNombreAseg(cartaPorte.getMercancias().getTransporteFerroviario().getNombreAseg());
		actual.getMercancias().getTransporteFerroviario()
				.setNumPolizaSeguro(cartaPorte.getMercancias().getTransporteFerroviario().getNumPolizaSeguro());

		if (cartaPorte.getMercancias().getTransporteFerroviario().getDerechosDePaso() != null
				&& !cartaPorte.getMercancias().getTransporteFerroviario().getDerechosDePaso().isEmpty()) {
			List<DerechoDePaso> listaDerechos = new ArrayList<>();
			for (DerechoDePaso derecho : cartaPorte.getMercancias().getTransporteFerroviario().getDerechosDePaso()) {
				listaDerechos.add(derechoService.save(derecho));
			}
			actual.getMercancias().getTransporteFerroviario().setDerechosDePaso(listaDerechos);
		}

		if (cartaPorte.getMercancias().getTransporteFerroviario().getCarros() != null
				&& !cartaPorte.getMercancias().getTransporteFerroviario().getCarros().isEmpty()) {
			List<Carro> listaCarros = new ArrayList<>();
			for (Carro carro : cartaPorte.getMercancias().getTransporteFerroviario().getCarros()) {
				listaCarros.add(carroService.save(carro));
			}
			actual.getMercancias().getTransporteFerroviario().setCarros(listaCarros);
		}

		if (cartaPorte.getFiguraTransporte().getTiposFigura() != null
				&& !cartaPorte.getFiguraTransporte().getTiposFigura().isEmpty()) {
			List<TipoFigura> listaTiposFigura = new ArrayList<>();
			for (TipoFigura tipoFigura : cartaPorte.getFiguraTransporte().getTiposFigura()) {
				if (tipoFigura.getPartesTransporte() != null && !tipoFigura.getPartesTransporte().isEmpty()) {
					List<ParteTransporte> listaPartesTransporte = new ArrayList<>();
					for (ParteTransporte parteTransporte : tipoFigura.getPartesTransporte()) {
						domicilioService.save(parteTransporte.getDomicilio());
						listaPartesTransporte.add(parteTransporteService.save(parteTransporte));
					}
					tipoFigura.setPartesTransporte(listaPartesTransporte);
				}
				listaTiposFigura.add(tipoFiguraService.save(tipoFigura));
			}
			actual.getFiguraTransporte().setTiposFigura(listaTiposFigura);
		}

		actual.setVersion(cartaPorte.getVersion());
		actual.setTranspInternac(cartaPorte.getTranspInternac());
		actual.setEntradaSalidaMerc(cartaPorte.getEntradaSalidaMerc());
		actual.setPaisOrigenDestino(cartaPorte.getPaisOrigenDestino());
		actual.setViaEntradaSalida(cartaPorte.getViaEntradaSalida());
		actual.setTotalDistRec(cartaPorte.getTotalDistRec());

		CartaPorte actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cartaPorte", actualizado, "actualizado", response);
	}

	@GetMapping("/{idServicio}/{idCartaPorte}/{tipoServicio}")
	@ResponseStatus
	public ResponseEntity<?> updateServicioCartaPorte(@PathVariable Long idServicio, @PathVariable Long idCartaPorte,
			@PathVariable Long tipoServicio) {
		Map<String, Object> response = new HashMap<>();
		CartaPorte cartaPorteActual = null;
		try {
			cartaPorteActual = service.findById(idCartaPorte);
			if (cartaPorteActual != null) {
				if (tipoServicio > 0 && tipoServicio == 1L) {
					try {
						Servicio servicioActual = servicioService.findById(idServicio);
						if (servicioActual != null) {
							try {
								servicioActual.setCartaPorte(cartaPorteActual);
								servicioService.save(servicioActual);
							} catch (DataAccessException e) {
								return CommonsUtils.generaErrorDataAccess(
										"Error al realizar la actualizacion a la base de datos", response, e);
							}
						}
					} catch (DataAccessException e) {
						return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos",
								response, e);
					}
				} else {
					try {
						ServicioInter servicioInterActual = servicioInterService.findById(idServicio);
						if (servicioInterActual != null) {
							try {
								servicioInterActual.setCartaPorte(cartaPorteActual);
								servicioInterService.save(servicioInterActual);
							} catch (DataAccessException e) {
								return CommonsUtils.generaErrorDataAccess(
										"Error al realizar la actualizacion a la base de datos", response, e);
							}
						}
					} catch (DataAccessException e) {
						return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos",
								response, e);
					}
				}
			}
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cartaPorte", cartaPorteActual, "actualizado", response);
	}

	@PostMapping("/uploadXml")
	public ResponseEntity<?> importXml(@RequestParam("archivoXml") MultipartFile archivoXml,
			@RequestParam("idCartaPorte") Long idCartaPorte) {
		Map<String, Object> response = new HashMap<>();
		try {
			// URI del espacio de nombres del esquema XML del W3C.
			// http://www.w3.org/2001/XMLSchema
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			// Representa un conjunto de restricciones que se pueden verificar / aplicar en
			// un documento XML
			// Un objeto de esquema generalmente se crea a partir de SchemaFactory
			Schema schema = factory.newSchema(new File("./src/main/xsd/CartaPorte20.xsd"));
			// verifica un documento XML contra el esquema XSD
			Validator validator = schema.newValidator();
			// Establece el ErrorHandler para recibir los errores encontrados durante la
			// invocación del método de validación.
			validator.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException exception) throws SAXException {
					mostrarError(exception);
				}

				@Override
				public void error(SAXParseException exception) throws SAXException {
					mostrarError(exception);
				}

				@Override
				public void fatalError(SAXParseException exception) throws SAXException {
					mostrarError(exception);
				}

				private void mostrarError(SAXParseException exception) {
					System.out.println(
							"Linea: " + String.format("%4s|", exception.getLineNumber()) + exception.getMessage());
				}

			});
			// Valida la entrada especificada. Debe ser un documento XML o un elemento XML y
			// no debe ser nulo
			validator.validate(new StreamSource(archivoXml.getInputStream()));

		} catch (SAXException | IOException ex) {
			System.err.println(ex.getMessage());
		}
		return CommonsUtils.generaErrorNoEncontrado("La", "cartaPorte", String.valueOf(idCartaPorte), response);
	}

}
