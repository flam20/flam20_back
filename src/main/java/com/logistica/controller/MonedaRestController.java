package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Moneda;
import com.logistica.service.IMonedaService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/moneda")
public class MonedaRestController {

	@Autowired
	IMonedaService monedaService;

	@GetMapping("/page/{page}")
	public Page<Moneda> pageAllMonedas(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return monedaService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Moneda> getAllMonedas() {
		return monedaService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getMoneda(@PathVariable Long id) {
		Moneda moneda = null;
		Map<String, Object> response = new HashMap<>();
		try {
			moneda = monedaService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (moneda == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "moneda", String.valueOf(id), response);
		}
		return new ResponseEntity<Moneda>(moneda, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createMoneda(@RequestBody Moneda moneda, BindingResult result) {
		Moneda monedaNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			monedaNuevo = monedaService.save(moneda);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "moneda", monedaNuevo, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateMoneda(@RequestBody Moneda moneda, BindingResult result) {
		Moneda monedaActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			monedaActual = monedaService.findById(moneda.getIdMoneda());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (monedaActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "moneda", String.valueOf(moneda.getIdMoneda()), response);
		}
		monedaActual.setDescCorta(moneda.getDescCorta());
		monedaActual.setDescLarga(moneda.getDescLarga());
		monedaActual.setSimbolo(moneda.getSimbolo());

		Moneda monedaActualizada = null;
		try {
			monedaActualizada = monedaService.save(monedaActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "moneda", monedaActualizada, "actualizada", response);
	}

}
