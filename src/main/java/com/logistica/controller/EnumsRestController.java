package com.logistica.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.xsd2java.CClavePedimento;
import com.logistica.xsd2java.CClaveTipoCarga;
import com.logistica.xsd2java.CCodigoTransporteAereo;
import com.logistica.xsd2java.CConfigAutotransporte;
import com.logistica.xsd2java.CConfigMaritima;
import com.logistica.xsd2java.CContenedor;
import com.logistica.xsd2java.CContenedorMaritimo;
import com.logistica.xsd2java.CDerechosDePaso;
import com.logistica.xsd2java.CEstaciones;
import com.logistica.xsd2java.CEstado;
import com.logistica.xsd2java.CINCOTERM;
import com.logistica.xsd2java.CMetodoPago;
import com.logistica.xsd2java.CMoneda;
import com.logistica.xsd2java.CNumAutorizacionNaviero;
import com.logistica.xsd2java.CPais;
import com.logistica.xsd2java.CParteTransporte;
import com.logistica.xsd2java.CSubTipoRem;
import com.logistica.xsd2java.CTipoCarro;
import com.logistica.xsd2java.CTipoDeComprobante;
import com.logistica.xsd2java.CTipoDeServicio;
import com.logistica.xsd2java.CTipoDeTrafico;
import com.logistica.xsd2java.CTipoFactor;
import com.logistica.xsd2java.CTipoPermiso;
import com.logistica.xsd2java.CUsoCFDI;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/enums")
public class EnumsRestController {

	@GetMapping("/clavesPedimento")
	public List<CClavePedimento> getClavesPedimento() {
		return Arrays.asList(CClavePedimento.values());
	}

	@GetMapping("/clavesTipoCarga")
	public List<CClaveTipoCarga> getClavesTipoCarga() {
		return Arrays.asList(CClaveTipoCarga.values());
	}

	@GetMapping("/codigosTransporteAereo")
	public List<CCodigoTransporteAereo> getCodigosTransporteAereo() {
		return Arrays.asList(CCodigoTransporteAereo.values());
	}

	@GetMapping("/configsAutotransporte")
	public List<CConfigAutotransporte> getConfigsAutotransporte() {
		return Arrays.asList(CConfigAutotransporte.values());
	}

	@GetMapping("/configsMaritima")
	public List<CConfigMaritima> getConfigsMaritima() {
		return Arrays.asList(CConfigMaritima.values());
	}

	@GetMapping("/contenedores")
	public List<CContenedor> getContenedores() {
		return Arrays.asList(CContenedor.values());
	}

	@GetMapping("/contenedoresMaritimo")
	public List<CContenedorMaritimo> getContenedoresMaritimo() {
		return Arrays.asList(CContenedorMaritimo.values());
	}

	@GetMapping("/derechosDePaso")
	public List<CDerechosDePaso> getDerechosDePaso() {
		return Arrays.asList(CDerechosDePaso.values());
	}

	@GetMapping("/estaciones")
	public List<CEstaciones> getEstaciones() {
		return Arrays.asList(CEstaciones.values());
	}

	@GetMapping("/estados")
	public List<CEstado> getEstados() {
		return Arrays.asList(CEstado.values());
	}

	@GetMapping("/incoterms")
	public List<CINCOTERM> getIncoterms() {
		return Arrays.asList(CINCOTERM.values());
	}

	@GetMapping("/metodosPago")
	public List<CMetodoPago> getMetodosPago() {
		return Arrays.asList(CMetodoPago.values());
	}

	@GetMapping("/monedas")
	public List<CMoneda> getMonedas() {
		return Arrays.asList(CMoneda.values());
	}

	@GetMapping("/numsAutorizacionNaviero")
	public List<CNumAutorizacionNaviero> getNumAutorizacionNaviero() {
		return Arrays.asList(CNumAutorizacionNaviero.values());
	}

	@GetMapping("/paises")
	public List<CPais> getPaises() {
		return Arrays.asList(CPais.values());
	}

	@GetMapping("/partesTransporte")
	public List<CParteTransporte> getPartesTransporte() {
		return Arrays.asList(CParteTransporte.values());
	}

	@GetMapping("/subsTipoRem")
	public List<CSubTipoRem> getSubsTipoRem() {
		return Arrays.asList(CSubTipoRem.values());
	}

	@GetMapping("/tiposCarro")
	public List<CTipoCarro> getTiposCarro() {
		return Arrays.asList(CTipoCarro.values());
	}

	@GetMapping("/tiposDeComprobante")
	public List<CTipoDeComprobante> getTiposDeComprobante() {
		return Arrays.asList(CTipoDeComprobante.values());
	}

	@GetMapping("/tiposDeServicio")
	public List<CTipoDeServicio> getTiposDeServicio() {
		return Arrays.asList(CTipoDeServicio.values());
	}

	@GetMapping("/tiposDeTrafico")
	public List<CTipoDeTrafico> getTiposDeTrafico() {
		return Arrays.asList(CTipoDeTrafico.values());
	}

	@GetMapping("/tiposFactor")
	public List<CTipoFactor> getTiposFactor() {
		return Arrays.asList(CTipoFactor.values());
	}

	@GetMapping("/tiposPermiso")
	public List<CTipoPermiso> getTiposPermiso() {
		return Arrays.asList(CTipoPermiso.values());
	}

	@GetMapping("/usosCfdi")
	public List<CUsoCFDI> getUsosCfdi() {
		return Arrays.asList(CUsoCFDI.values());
	}

}
