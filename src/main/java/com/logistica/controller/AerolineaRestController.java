package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Aerolinea;
import com.logistica.service.IAerolineaService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/aerolinea")
public class AerolineaRestController {

	@Autowired
	IAerolineaService service;

	@GetMapping("/page/{page}")
	public Page<Aerolinea> pageAll(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Aerolinea> getAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		Aerolinea bean = null;
		Map<String, Object> response = new HashMap<>();
		try {
			bean = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (bean == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "aerolinea", String.valueOf(id), response);
		}
		return new ResponseEntity<Aerolinea>(bean, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody Aerolinea bean, BindingResult result) {
		Aerolinea nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = service.save(bean);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "aerolinea", nuevo, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody Aerolinea bean, BindingResult result) {
		Aerolinea actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(bean.getIdAerolinea());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "aerolinea", String.valueOf(bean.getIdAerolinea()),
					response);
		}
		actual.setPrefijo(bean.getPrefijo());
		actual.setNombre(bean.getNombre());
		actual.setTerritorio(bean.getTerritorio());
		actual.setBorrado(bean.getBorrado());

		Aerolinea actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "aerolinea", actualizado, "actualizado", response);
	}

}
