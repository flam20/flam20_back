package com.logistica.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.logistica.model.Usuario;
import com.logistica.service.IUsuarioService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/usuario")
public class UsuarioRestController {

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@GetMapping("/page/{page}")
	public Page<Usuario> pageAllUsuarios(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return usuarioService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Usuario> getAllUsuarios() {
		return usuarioService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getUsuario(@PathVariable Long id) {
		Usuario usuario = null;
		Map<String, Object> response = new HashMap<>();
		try {
			usuario = usuarioService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (usuario == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "usuario", String.valueOf(id), response);
		}
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createUsuario(@RequestBody Usuario usuario, BindingResult result) {
		Usuario usuarioNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			usuario.setPassword(passwordEncoder.encode("temporal123"));
			usuarioNuevo = usuarioService.save(usuario);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "usuario", usuarioNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateUsuario(@RequestBody Usuario usuario, BindingResult result) {
		Usuario usuarioActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			usuarioActual = usuarioService.findById(usuario.getIdUsuario());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (usuarioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "usuario", String.valueOf(usuario.getIdUsuario()),
					response);
		}
		usuarioActual.setEnabled(usuario.getEnabled());
		usuarioActual.setNombre1(usuario.getNombre1());
		usuarioActual.setNombre2(usuario.getNombre2());
		usuarioActual.setNombre3(usuario.getNombre3());
		usuarioActual.setApellidoPaterno(usuario.getApellidoPaterno());
		usuarioActual.setApellidoMaterno(usuario.getApellidoMaterno());
		usuarioActual.setCorreoElectronico(usuario.getCorreoElectronico());
		usuarioActual.setPassword(passwordEncoder.encode(usuario.getPassword()));
		
		Usuario usuarioActualizado = null;
		try {
			usuarioActualizado = usuarioService.save(usuarioActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "usuario", usuarioActualizado, "actualizado", response);
	}

	@PostMapping("/upload")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo,
			@RequestParam("idUsuario") Long id) {
		Map<String, Object> response = new HashMap<>();
		Usuario usuario = usuarioService.findById(id);
		if (!archivo.isEmpty() && usuario != null) {
			try {
				usuario.setAvatar(archivo.getBytes());
			} catch (IOException e) {
				return CommonsUtils.generaError("Error al cargar la foto del empleado", response, e);
			}
			usuarioService.save(usuario);
			response.put("usuario", usuario);
			response.put("mensaje", "Se ha subido la imagen: " + archivo.getOriginalFilename());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		}
		return CommonsUtils.generaErrorNoEncontrado("El", "usuario", String.valueOf(id), response);
	}

}
