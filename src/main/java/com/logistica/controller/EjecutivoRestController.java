package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Ejecutivo;
import com.logistica.service.IEjecutivoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/ejecutivo")
public class EjecutivoRestController {

	@Autowired
	private IEjecutivoService service;

	@GetMapping("/page/{page}")
	public Page<Ejecutivo> pageAll(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Ejecutivo> getAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		Ejecutivo ejecutivo = null;
		Map<String, Object> response = new HashMap<>();
		try {
			ejecutivo = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (ejecutivo == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "ejecutivo", String.valueOf(id), response);
		}
		return new ResponseEntity<Ejecutivo>(ejecutivo, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody Ejecutivo bean, BindingResult result) {
		Ejecutivo nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = service.save(bean);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "ejecutivo", nuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody Ejecutivo bean, BindingResult result) {
		Ejecutivo actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(bean.getIdEjecutivo());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "ejecutivo", String.valueOf(bean.getIdEjecutivo()),
					response);
		}
		actual.setNombre(bean.getNombre());
		actual.setApellidoPaterno(bean.getApellidoPaterno());
		actual.setApellidoMaterno(bean.getApellidoMaterno());
		actual.setCorreoElectronico(bean.getCorreoElectronico());
		actual.setIdUsuarioSAP(bean.getIdUsuarioSAP());
		actual.setUnidadVentasSAP(bean.getUnidadVentasSAP());

		Ejecutivo actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "ejecutivo", actualizado, "actualizado", response);
	}

}
