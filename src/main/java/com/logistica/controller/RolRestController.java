package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Rol;
import com.logistica.service.IRolService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/rol")
public class RolRestController {

	@Autowired
	private IRolService rolService;

	@GetMapping("/page/{page}")
	public Page<Rol> pageAllRoles(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return rolService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Rol> getAllRoles() {
		return rolService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getRol(@PathVariable Long id) {
		Rol rol = null;
		Map<String, Object> response = new HashMap<>();
		try {
			rol = rolService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (rol == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "rol", String.valueOf(id), response);
		}
		return new ResponseEntity<Rol>(rol, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createRol(@RequestBody Rol rol, BindingResult result) {
		Rol nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = rolService.save(rol);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "rol", nuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateRol(@RequestBody Rol rol, BindingResult result) {
		Rol actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = rolService.findById(rol.getIdRol());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "rol", String.valueOf(rol.getIdRol()), response);
		}
		actual.setNombre(rol.getNombre());

		Rol actualizado = null;
		try {
			actualizado = rolService.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "rol", actualizado, "actualizado", response);
	}

}
