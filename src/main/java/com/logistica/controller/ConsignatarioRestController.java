package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Consignatario;
import com.logistica.service.IConsignatarioService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/consignatario")
public class ConsignatarioRestController {

	@Autowired
	IConsignatarioService service;

	@GetMapping("/page/{page}")
	public Page<Consignatario> pageAllConsignatarios(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Consignatario> getAllConsignatarios() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getConsignatario(@PathVariable Long id) {
		Consignatario consignatario = null;
		Map<String, Object> response = new HashMap<>();
		try {
			consignatario = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (consignatario == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "consignatario", String.valueOf(id), response);
		}
		return new ResponseEntity<Consignatario>(consignatario, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createConsignatario(@RequestBody Consignatario consignatario, BindingResult result) {
		Consignatario consignatarioNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			consignatarioNuevo = service.save(consignatario);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "consignatario", consignatarioNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateConsignatario(@RequestBody Consignatario consignatario, BindingResult result) {
		Consignatario consignatarioActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			consignatarioActual = service.findById(consignatario.getIdConsignatario());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (consignatarioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "consignatario",
					String.valueOf(consignatario.getIdConsignatario()), response);
		}
		consignatarioActual.setNombre(consignatario.getNombre());
		consignatarioActual.setRfc(consignatario.getRfc());
		consignatarioActual.setContacto(consignatario.getContacto());
		consignatarioActual.setBorrado(consignatario.getBorrado());

		Consignatario consignatarioActualizado = null;
		try {
			consignatarioActualizado = service.save(consignatarioActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "consignatario", consignatarioActualizado, "actualizado",
				response);
	}

}
