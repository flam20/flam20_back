package com.logistica.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Factura;
import com.logistica.service.IBoxService;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/factura")
public class FacturaRestController {

	private Logger logger = LoggerFactory.getLogger(FacturaRestController.class);

	@Autowired
	IBoxService boxService;

	@GetMapping("/proveedor/{idProveedor}")
	public List<Factura> getByProveedor(@PathVariable Long idProveedor) {
		logger.info("Buscando facturas, idProveedor: " + idProveedor);
		return boxService.getFacturasByProveedor(idProveedor);
	}

}
