package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Pais;
import com.logistica.service.IPaisService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/pais")
public class PaisRestController {

	@Autowired
	private IPaisService paisService;

	@GetMapping("/page/{page}")
	public Page<Pais> pageAllPaises(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return paisService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Pais> getAllPaises() {
		return paisService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getPais(@PathVariable Long id) {
		Pais pais = null;
		Map<String, Object> response = new HashMap<>();
		try {
			pais = paisService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (pais == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "pais", String.valueOf(id), response);
		}
		return new ResponseEntity<Pais>(pais, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createPais(@RequestBody Pais pais, BindingResult result) {
		Pais paisNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			paisNuevo = paisService.save(pais);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "pais", paisNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updatePais(@RequestBody Pais pais, BindingResult result) {
		Pais paisActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			paisActual = paisService.findById(pais.getIdPais());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (paisActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "pais", String.valueOf(pais.getIdPais()), response);
		}
		paisActual.setDescCortaPais(pais.getDescCortaPais());
		paisActual.setDescLargaPais(pais.getDescLargaPais());

		Pais paisActualizado = null;
		try {
			paisActualizado = paisService.save(paisActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "pais", paisActualizado, "actualizado", response);
	}

}
