package com.logistica.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.CargoDestino;
import com.logistica.model.CargoOrigen;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.Ejecutivo;
import com.logistica.model.EmpleadoSAP;
import com.logistica.model.EstatusServicio;
import com.logistica.model.ServicioInter;
import com.logistica.service.IClienteService;
import com.logistica.service.IEstatusServicioService;
import com.logistica.service.IOrdenCompraService;
import com.logistica.service.IPedidoClienteService;
import com.logistica.service.IReporteExcelService;
import com.logistica.service.IServicioInterService;
import com.logistica.service.IUsuarioService;
import com.logistica.util.ActionCode;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/servicioInter")
public class ServicioInterRestController {

	static Logger logger = Logger.getLogger(ServicioInterRestController.class.getName());
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	private DateTimeFormatter shortFormatter = DateTimeFormatter.ofPattern("yyyy/MM/d");

	@Autowired
	IServicioInterService servicioInterService;

	@Autowired
	IUsuarioService usuarioService;

	@Autowired
	IClienteService clienteService;

	@Autowired
	IOrdenCompraService ordenCompraService;

	@Autowired
	IPedidoClienteService pedidoClienteService;

	@Autowired
	IEstatusServicioService estatusServicioService;

	@Autowired
	IReporteExcelService reporteExcelService;

	@GetMapping("/page/{page}")
	public Page<ServicioInter> pageAll(@PathVariable Integer page) {
		logger.info("pageAll");
		Pageable pageable = PageRequest.of(page, 10);
		return servicioInterService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<ServicioInter> getAll() {
		logger.info("getAll");
		return servicioInterService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		ServicioInter servicio = null;
		Map<String, Object> response = new HashMap<>();
		try {
			servicio = servicioInterService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (servicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}
		return new ResponseEntity<ServicioInter>(servicio, HttpStatus.OK);
	}

	@PostMapping("/origen/update/{id}")
	@ResponseStatus
	public ResponseEntity<?> updateOrigen(@PathVariable Long id, @RequestBody CargoOrigen origen,
			BindingResult result) {
		logger.info("ACTUALIZA CARGO ORIGEN [" + id + "][" + origen.getItemID());
		logger.info("CARGO [" + origen.toString() + "]");
		Map<String, Object> response = new HashMap<>();
		ServicioInter servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioInterService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (servicioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}

		try {
			String pedidoCliente = null;
			String accion = "";

			CargoOrigen cargoSeleccionado = getCargoOrigen(servicioActual, origen.getItemID());
			cargoSeleccionado.setVentaMonto(origen.getVentaMonto());
			pedidoCliente = cargoSeleccionado.getPedidoCliente();

			EmpleadoSAP empleado = obtieneEmpleadoSAP(servicioActual);
			if (pedidoCliente != null && pedidoCliente.length() > 0) {
				String statusConsulta = pedidoClienteService.consultarStatusPedido(cargoSeleccionado.getItemID() + "",
						pedidoCliente);
				if (statusConsulta.equals("true")) {
					accion = "Actualizada";
					logger.info("SE VA A ACTUALIZAR EL PEDIDO DE CLIENTE");

					pedidoClienteService.actualizaCargoOrigen(servicioActual, ActionCode.SAVE, empleado,
							cargoSeleccionado);
					servicioActual = servicioInterService.save(servicioActual);
				} else {
					logger.error("NO SE PUEDE ACTUALIZAR EL ESTATUS: " + statusConsulta);
				}

			} else {
				accion = "Creada";
				logger.info("SE VA A CREAR EL PEDIDO DE CLIENTE");
				pedidoCliente = pedidoClienteService.actualizaCargoOrigen(servicioActual, ActionCode.CREATE, empleado,
						cargoSeleccionado);
				cargoSeleccionado.setPedidoCliente(pedidoCliente);
				servicioActual = servicioInterService.save(servicioActual);
			}

			return CommonsUtils.generaRespuestaCreacion("El", "cargo", origen, "actualizado", response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
	}

	@PostMapping("/origen/delete/{id}")
	@ResponseStatus
	public ResponseEntity<?> deleteOrigen(@PathVariable Long id, @RequestBody CargoOrigen origen,
			BindingResult result) {
		logger.info("Id Servicio: " + id);
		Map<String, Object> response = new HashMap<>();
		ServicioInter servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioInterService.findById(id);
			ordenCompraService.eliminaItem(servicioActual.getIdServicioStr(), servicioActual.getOrdenCompra(), new Long(origen.getItemID()), null);
			CargoOrigen cOrigen = getCargoOrigen(servicioActual, origen.getItemID());
			logger.info("-->"+servicioActual.getCargosOrigen().contains(cOrigen));
			
			boolean resultdelete = servicioActual.getCargosOrigen().remove(cOrigen);
			logger.info("resultdelete [" +resultdelete + "]");
			
			servicioActual = servicioInterService.save(servicioActual);
			
			return CommonsUtils.generaRespuestaCreacion("El", "cargo", origen, "actualizado", response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

//		if (servicioActual == null) {
//			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
//		}

//		try {
//			// Aqui se crea o actualiza la ruta
//
//			
//		} catch (DataAccessException e) {
//			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
//					e);
//		}
	}

	@PostMapping("/destino/update/{id}")
	@ResponseStatus
	public ResponseEntity<?> updateDestino(@PathVariable Long id, @RequestBody CargoDestino destino,
			BindingResult result) {
		logger.info("ACTUALIZA CARGO DESTINO [" + id + "][" + destino.getItemID());
		logger.info("CARGO [" + destino.toString() + "]");
		Map<String, Object> response = new HashMap<>();
		ServicioInter servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioInterService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (servicioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}

		try {
			String pedidoCliente = null;
			String accion = "";

			CargoDestino cargoSeleccionado = getCargoDestino(servicioActual, destino.getItemID());
			cargoSeleccionado.setVentaMonto(destino.getVentaMonto());
			pedidoCliente = cargoSeleccionado.getPedidoCliente();

			EmpleadoSAP empleado = obtieneEmpleadoSAP(servicioActual);
			if (pedidoCliente != null && pedidoCliente.length() > 0) {
				String statusConsulta = pedidoClienteService.consultarStatusPedido(cargoSeleccionado.getItemID() + "",
						pedidoCliente);
				if (statusConsulta.equals("true")) {
					accion = "Actualizada";
					logger.info("SE VA A ACTUALIZAR EL PEDIDO DE CLIENTE");

					pedidoClienteService.actualizaCargoDestino(servicioActual, ActionCode.SAVE, empleado,
							cargoSeleccionado);
					servicioActual = servicioInterService.save(servicioActual);
				} else {
					logger.error("NO SE PUEDE ACTUALIZAR EL ESTATUS: " + statusConsulta);
				}

			} else {
				accion = "Creada";
				logger.info("SE VA A CREAR EL PEDIDO DE CLIENTE");
				pedidoCliente = pedidoClienteService.actualizaCargoDestino(servicioActual, ActionCode.CREATE, empleado,
						cargoSeleccionado);
				cargoSeleccionado.setPedidoCliente(pedidoCliente);
				servicioActual = servicioInterService.save(servicioActual);
			}

			return CommonsUtils.generaRespuestaCreacion("El", "cargo", destino, "actualizado", response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
	}

	@PostMapping("/destino/delete/{id}")
	@ResponseStatus
	public ResponseEntity<?> deleteDestino(@PathVariable Long id, @RequestBody CargoDestino destino,
			BindingResult result) {
		logger.info("Id Servicio: " + id);
		Map<String, Object> response = new HashMap<>();
		ServicioInter servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioInterService.findById(id);
			ordenCompraService.eliminaItem(servicioActual.getIdServicioStr(), servicioActual.getOrdenCompra(), new Long(destino.getItemID()), null);
			CargoDestino cDestino = getCargoDestino(servicioActual, destino.getItemID());
			logger.info("-->"+servicioActual.getCargosDestino().contains(cDestino));
			
			boolean resultdelete = servicioActual.getCargosDestino().remove(cDestino);
			logger.info("resultdelete [" +resultdelete + "]");
			
			servicioActual = servicioInterService.save(servicioActual);
			return CommonsUtils.generaRespuestaCreacion("El", "cargo", destino, "actualizado", response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

//		if (servicioActual == null) {
//			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
//		}

//		try {
//			// Aqui se crea o actualiza la ruta
//
//			return CommonsUtils.generaRespuestaCreacion("El", "cargo", destino, "actualizado", response);
//		} catch (DataAccessException e) {
//			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
//					e);
//		}
	}

	@GetMapping("/search")
	public List<ServicioInter> search(@RequestParam(value = "numeroServicio", required = false) String numeroServicio,
			@RequestParam(value = "idTrafico", required = false) String idTrafico,
			@RequestParam(value = "guiaHouse", required = false) String guiaHouse,
			@RequestParam(value = "guiaMaster", required = false) String guiaMaster,
			@RequestParam(value = "idConsignatario", required = false) Long idConsignatario,
			@RequestParam(value = "idAgente", required = false) Long idAgente,
			@RequestParam(value = "fechaDesde", required = false) String fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) String fechaHasta,
			@RequestParam(value = "reporteImpo", required = false) Boolean reporteImpo,
			@RequestParam(value = "rol", required = false) String rol,
			@RequestParam(value = "borradores", required = false) Boolean borradores) {
		logger.info("Buscando servicios internacionales, numeroServicio: " + numeroServicio + ", guiaHouse: "
				+ guiaHouse + ", idTrafico: " + idTrafico + ", guiaMaster: " + guiaMaster + ", idConsignatario: "
				+ idConsignatario + ", idAgente: " + idAgente + ", fechaDesde: " + fechaDesde + ", fechaHasta: "
				+ fechaHasta + ", rol: " + rol + ", reporteImpo: " + reporteImpo);

		String tipo = null;
		if (rol != null && rol.length() > 0 && !rol.equalsIgnoreCase("null")) {
			idTrafico = null;
			logger.info("1");
			if (rol.endsWith("IMPO")) {
				tipo = "IMPO";
			} else if (rol.endsWith("EXPO")) {
				tipo = "EXPO";
			} else if (rol.equalsIgnoreCase("ROLE_SUPERADMIN")) {
				// ES ADMIN
				if (reporteImpo) {
					tipo = "IMPO";
				} else {
					tipo = "EXPO";
				}
			} else {
				tipo = null;
			}
		} else {
			tipo = null;
		}
		logger.info("TIPO " + tipo);
		List<ServicioInter> listaServicios = servicioInterService.searchServicios(numeroServicio, idTrafico, guiaHouse,
				guiaMaster, idConsignatario, idAgente, fechaDesde, fechaHasta, borradores, tipo);
		return listaServicios;
	}

	@GetMapping(value = "/searchReportInter", produces = MediaType.ALL_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> searchReporte(
			@RequestParam(value = "numeroServicio", required = false) String numeroServicio,
			@RequestParam(value = "idTrafico", required = false) String idTrafico,
			@RequestParam(value = "guiaHouse", required = false) String guiaHouse,
			@RequestParam(value = "guiaMaster", required = false) String guiaMaster,
			@RequestParam(value = "idConsignatario", required = false) Long idConsignatario,
			@RequestParam(value = "idAgente", required = false) Long idAgente,
			@RequestParam(value = "fechaDesde", required = false) String fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) String fechaHasta,
			@RequestParam(value = "reporteImpo", required = false) Boolean reporteImpo,
			@RequestParam(value = "rol", required = false) String rol,
			@RequestParam(value = "borradores", required = false) Boolean borradores) throws IOException {
		logger.info("Buscando servicios internacionales para reporte, numeroServicio: " + numeroServicio
				+ ", guiaHouse: " + guiaHouse + ", idTrafico: " + idTrafico + ", guiaMaster: " + guiaMaster
				+ ", idConsignatario: " + idConsignatario + ", idAgente: " + idAgente + ", fechaDesde: " + fechaDesde
				+ ", fechaHasta: " + fechaHasta + ", borradores: " + borradores + ", rol: " + rol + ", reporteImpo: "
				+ reporteImpo);

		String tipo = null;
		if (rol != null && rol.length() > 0 && !rol.equalsIgnoreCase("null")) {
			idTrafico = null;
			logger.info("1");
			if (rol.endsWith("IMPO")) {
				tipo = "IMPO";
			} else if (rol.endsWith("EXPO")) {
				tipo = "EXPO";
			} else if (rol.equalsIgnoreCase("ROLE_SUPERADMIN")) {
				// ES ADMIN
				if (reporteImpo) {
					tipo = "IMPO";
				} else {
					tipo = "EXPO";
				}

			} else {
				tipo = null;
			}
		} else {
			tipo = null;
		}
		logger.info("TIPO " + tipo);
		List<ServicioInter> listaServicios = servicioInterService.searchServicios(numeroServicio, idTrafico, guiaHouse,
				guiaMaster, idConsignatario, idAgente, fechaDesde, fechaHasta, borradores, tipo);

		logger.info("Se encontraron " + listaServicios.size() + " servicios");
		if (listaServicios != null && !listaServicios.isEmpty()) {
			File fileExcel = reporteExcelService.exportToExcelInter(listaServicios);
			Resource resource = new UrlResource(fileExcel.toURI());

			Path path = resource.getFile().toPath();

			return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);

//			return CommonsUtils.generaRespuestaCreacion("El", "reporte", serviciosNuevos, "creado", response);
		} else {
			logger.error("NO SE ENCONTRARON SERVICIOS");
//			return CommonsUtils.generaError("No se pudo crear el reporte", "Sin servicios", response);
			return null;
		}

	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody ServicioInter servicio, BindingResult result) {
		logger.info("============== CREACION DE SERVICIO INTER -> ESTATUS["
				+ servicio.getEstatus().getIdEstatusServicio() + "]");
		logger.info(servicio.toString());
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		EmpleadoSAP empleado = obtieneEmpleadoSAP(servicio);

		if (empleado != null) {

			if (empleado.getIdEmpleado() != null && empleado.getIdEmpleado().length() > 0
					&& empleado.getUnidadVentas() != null && empleado.getUnidadVentas().length() > 0) {

				ServicioInter servicioNuevo = null;

				try {
					if (servicio.getsFechaSalida() != null && !servicio.getsFechaSalida().isEmpty()) {
						if (servicio.getsFechaSalida().length() == 9) {
							servicio.setFechaSalida(LocalDate.parse(servicio.getsFechaSalida(), shortFormatter));
						} else {
							servicio.setFechaSalida(LocalDate.parse(servicio.getsFechaSalida(), formatter));
						}
					}
					if (servicio.getsFechaDocumentacion() != null && !servicio.getsFechaDocumentacion().isEmpty()) {
						if (servicio.getsFechaDocumentacion().length() == 9) {
							servicio.setFechaDocumentacion(
									LocalDate.parse(servicio.getsFechaDocumentacion(), shortFormatter));
						} else {
							servicio.setFechaDocumentacion(
									LocalDate.parse(servicio.getsFechaDocumentacion(), formatter));
						}
					}
					if (servicio.getsFechaArribo1() != null && !servicio.getsFechaArribo1().isEmpty()) {
						if (servicio.getsFechaArribo1().length() == 9) {
							servicio.setFechaArribo1(LocalDate.parse(servicio.getsFechaArribo1(), shortFormatter));
						} else {
							servicio.setFechaArribo1(LocalDate.parse(servicio.getsFechaArribo1(), formatter));
						}
					}
					if (servicio.getsFechaArribo2() != null && !servicio.getsFechaArribo2().isEmpty()) {
						if (servicio.getsFechaArribo2().length() == 9) {
							servicio.setFechaArribo2(LocalDate.parse(servicio.getsFechaArribo2(), shortFormatter));
						} else {
							servicio.setFechaArribo2(LocalDate.parse(servicio.getsFechaArribo2(), formatter));
						}
					}
					if (servicio.getsFechaArribo3() != null && !servicio.getsFechaArribo3().isEmpty()) {
						if (servicio.getsFechaArribo3().length() == 9) {
							servicio.setFechaArribo3(LocalDate.parse(servicio.getsFechaArribo3(), shortFormatter));
						} else {
							servicio.setFechaArribo3(LocalDate.parse(servicio.getsFechaArribo3(), formatter));
						}
					}

					Integer consecutivoActual = servicioInterService.getConsecutiveByAnio(servicio.getAnio());
					if (consecutivoActual == null) {
						consecutivoActual = 2699;// ultimo servicio flam 1.0
					}
					servicio.setConsecutivo(consecutivoActual + 1);
					String sAnio = "" + servicio.getAnio();
					servicio.setIdServicioStr(
							sAnio.substring(2) + "-" + formatearConsecutivo("" + servicio.getConsecutivo()) + "-"
									+ servicio.getTipo().charAt(0) + servicio.getSubtipo().charAt(0));

//					logger.info("FECHA DE FRONT " + servFront.getFechaServicio());
					LocalDate today = LocalDate.now(ZoneId.of("America/Chicago"));
//					logger.info("Current Date=" + today);
					servicio.setFechaServicio(today);

//					logger.info("ACTUALIZADO Date=" + servicioNuevo.getFechaServicio());

					servicioNuevo = servicioInterService.save(servicio);

					ordenCompraService.creaOrdenCompra_Inter(servicioNuevo, empleado);
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos",
							response, e);
				}
				return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioNuevo, "creado", response);
			} else {
				return CommonsUtils.generaError("No se pudo crear el servicio",
						"No se encontró unidad de ventas para ejecutivo", response);
			}
		} else {
			// No se encontraron ejecutivos asociados al cliente
			return CommonsUtils.generaError("No se pudo crear el servicio",
					"No se encontraron ejecutivos asociados al cliente", response);
		}
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody ServicioInter servicio, BindingResult result) {
		logger.info("============== ACTUALIZACION DE SERVICIO INTER -> ESTATUS["
				+ servicio.getEstatus().getIdEstatusServicio() + "]");
		logger.info(servicio.toString());
		ServicioInter servicioActual = null;
		Map<String, Object> response = new HashMap<>();

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		EmpleadoSAP empleado = obtieneEmpleadoSAP(servicio);

		if (empleado != null) {

			if (empleado.getIdEmpleado() != null && empleado.getIdEmpleado().length() > 0
					&& empleado.getUnidadVentas() != null && empleado.getUnidadVentas().length() > 0) {

				try {
					servicioActual = servicioInterService.findById(servicio.getIdServicio());
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos",
							response, e);
				}
				if (servicioActual == null) {
					return CommonsUtils.generaErrorNoEncontrado("El", "servicio",
							String.valueOf(servicio.getIdServicio()), response);
				}
				servicioActual.setIdServicioStr(servicio.getIdServicioStr());
				servicioActual.setAnio(servicio.getAnio());
				servicioActual.setConsecutivo(servicio.getConsecutivo());
				servicioActual.setBorrador(servicio.getBorrador());
				servicioActual.setFechaServicio(servicio.getFechaServicio());
				servicioActual.setMpc(servicio.getMpc());
				servicioActual.setTipo(servicio.getTipo());

				if (servicioActual.getSubtipo() == null || servicioActual.getSubtipo().isEmpty()) {
					if (servicioActual.getIdServicioStr() != null && servicioActual.getIdServicioStr().contains("AI")) {
						servicioActual.setSubtipo("Impo");
					} else {
						servicioActual.setSubtipo("Expo");
					}
				}

				servicioActual.setShipper(servicio.getShipper());
				servicioActual.setConsignatario(servicio.getConsignatario());
				servicioActual.setCliente(servicio.getCliente());
				servicioActual.setPagador(servicio.getPagador());
				servicioActual.setTrafico(servicio.getTrafico());
				servicioActual.setCargosOrigen(servicio.getCargosOrigen());
				servicioActual.setCargosDestino(servicio.getCargosDestino());
				servicioActual.setEstatus(servicio.getEstatus());

				if (servicio.getsFechaSalida() != null && !servicio.getsFechaSalida().isEmpty()) {
					if (servicio.getsFechaSalida().length() == 9) {
						servicioActual.setFechaSalida(LocalDate.parse(servicio.getsFechaSalida(), shortFormatter));
					} else {
						servicioActual.setFechaSalida(LocalDate.parse(servicio.getsFechaSalida(), formatter));
					}
				}

				if (servicio.getsFechaDocumentacion() != null && !servicio.getsFechaDocumentacion().isEmpty()) {
					if (servicio.getsFechaDocumentacion().length() == 9) {
						servicioActual.setFechaDocumentacion(
								LocalDate.parse(servicio.getsFechaDocumentacion(), shortFormatter));
					} else {
						servicioActual
								.setFechaDocumentacion(LocalDate.parse(servicio.getsFechaDocumentacion(), formatter));
					}
				}

				if (servicio.getsFechaArribo1() != null && !servicio.getsFechaArribo1().isEmpty()) {
					if (servicio.getsFechaArribo1().length() == 9) {
						servicioActual.setFechaArribo1(LocalDate.parse(servicio.getsFechaArribo1(), shortFormatter));
					} else {
						servicioActual.setFechaArribo1(LocalDate.parse(servicio.getsFechaArribo1(), formatter));
					}
				}

				if (servicio.getsFechaArribo2() != null && !servicio.getsFechaArribo2().isEmpty()) {
					if (servicio.getsFechaArribo2().length() == 9) {
						servicioActual.setFechaArribo2(LocalDate.parse(servicio.getsFechaArribo2(), shortFormatter));
					} else {
						servicioActual.setFechaArribo2(LocalDate.parse(servicio.getsFechaArribo2(), formatter));
					}
				}

				if (servicio.getsFechaArribo3() != null && !servicio.getsFechaArribo3().isEmpty()) {
					if (servicio.getsFechaArribo3().length() == 9) {
						servicioActual.setFechaArribo3(LocalDate.parse(servicio.getsFechaArribo3(), shortFormatter));
					} else {
						servicioActual.setFechaArribo3(LocalDate.parse(servicio.getsFechaArribo3(), formatter));
					}
				}

				servicioActual.setGhPrefijoAgente(servicio.getGhPrefijoAgente());
				servicioActual.setGhNumGuia(servicio.getGhNumGuia());
				servicioActual.setGmPrefijoAerolinea(servicio.getGmPrefijoAerolinea());
				servicioActual.setGmNumGuia(servicio.getGmNumGuia());
				servicioActual.setAeropuertoOrigen(servicio.getAeropuertoOrigen());
				servicioActual.setAeropuertoDestino(servicio.getAeropuertoDestino());
				servicioActual.setIncotermOrigen(servicio.getIncotermOrigen());
				servicioActual.setIncotermDestino(servicio.getIncotermDestino());
				servicioActual.setNumPiezas(servicio.getNumPiezas());
				servicioActual.setPesoBruto(servicio.getPesoBruto());
				servicioActual.setPesoCargable(servicio.getPesoCargable());
				servicioActual.setNotas(servicio.getNotas());
				servicioActual.setMotivoCancelacion(servicio.getMotivoCancelacion());
				servicioActual.setNotasCompletas(servicio.getNotasCompletas());
				servicioActual.setNoFacturaCliente(servicio.getNoFacturaCliente());
				servicioActual.setRefFacturacion(servicio.getRefFacturacion());
				servicioActual.setTotalprofitCargosOrigen(servicio.getTotalprofitCargosOrigen());
				servicioActual.setTotalprofitCargosDestino(servicio.getTotalprofitCargosDestino());
				servicioActual.setTotalprofit(servicio.getTotalprofit());
				servicioActual.setFechaFactura(servicio.getFechaFactura());
				servicioActual.setNombreTrafico(servicio.getNombreTrafico());
				servicioActual.setLcl(servicio.getLcl());
				servicioActual.setContenedor(servicio.getContenedor());
				servicioActual.setMotivoCancelacion(servicio.getMotivoCancelacion());
				servicioActual.setIdChat(servicio.getIdChat());

				if (servicio.getOrdenCompra() != null && servicio.getOrdenCompra().length() > 0) {
					logger.info("SI TRAE ORDEN DE COMPRA, SE USA " + servicio.getOrdenCompra());
					servicioActual.setOrdenCompra(servicio.getOrdenCompra());
				} else {
					logger.info("NO TRAE ORDEN DE COMPRA, SE USA LA DEL BACK " + servicioActual.getOrdenCompra());
				}

				ServicioInter servicioActualizado = null;
				try {

					logger.info("IDCHAT [" + servicioActual.getIdChat() + "]");
					logger.info("MOTIVO CANCELACION [" + servicioActual.getMotivoCancelacion() + "]");

					if (servicioActual.getMotivoCancelacion() != null) {
						// ES ACTUALIZACIÓN DE CHAT
						logger.info("ES ACTUALIZACION DE CHAT, NO SE ACTUALIZA OC");
						servicioInterService.updateChatID(servicioActual.getIdChat(), servicioActual.getIdServicio());

					} else {
						servicioActualizado = servicioInterService.save(servicioActual);
						ordenCompraService.actualizaOrdenCompra_Inter(servicioActualizado, empleado);
					}

					logger.info("ESTATUS [" + servicio.getEstatus().getIdEstatusServicio() + "]");
					if (servicio.getEstatus().getIdEstatusServicio().compareTo(new Long(3)) == 0) {
						logger.info("********************* PEDIDO A CLIENTE INICIO ********************");
						// verificar si contiene costos en usd y pesos
						String pedidoCliente = "";
						String resultadoSAP = "";

						if (containsCurrency(servicio, "MXN")) {
							pedidoCliente = pedidoClienteService.creaPedidoClienteInter(servicio, "MXN", empleado);

							if (pedidoCliente != null) {
								resultadoSAP += "\n - Se creó el pedido a Cliente [MXN] - [" + pedidoCliente + "]";
								if (servicio.getCargosOrigen() != null && servicio.getCargosOrigen().size() > 0) {
									for (CargoOrigen origen : servicio.getCargosOrigen()) {
										if (origen.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& origen.getMonedaVenta().getSimbolo().equals("MXN")) {
											origen.setPedidoCliente(pedidoCliente);
										}
									}
								}

								if (servicio.getCargosDestino() != null && servicio.getCargosDestino().size() > 0) {
									for (CargoDestino destino : servicio.getCargosDestino()) {
										if (destino.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& destino.getMonedaVenta().getSimbolo().equals("MXN")) {
											destino.setPedidoCliente(pedidoCliente);
										}
									}
								}

								servicioActualizado = servicioInterService.save(servicioActualizado);
							} else {
								logger.error("NO SE GENERÓ PEDIDO A CLIENTE MXN");
								resultadoSAP += "\n - No se generó pedido a Cliente [MXN]";
							}
						}

						if (containsCurrency(servicio, "EUR")) {
							pedidoCliente = pedidoClienteService.creaPedidoClienteInter(servicio, "EUR", empleado);

							if (pedidoCliente != null) {
								resultadoSAP += "\n - Se creó el pedido a Cliente [EUR] - [" + pedidoCliente + "]";

								if (servicio.getCargosOrigen() != null && servicio.getCargosOrigen().size() > 0) {
									for (CargoOrigen origen : servicio.getCargosOrigen()) {
										if (origen.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& origen.getMonedaVenta().getSimbolo().equals("EUR")) {
											origen.setPedidoCliente(pedidoCliente);
										}
									}
								}

								if (servicio.getCargosDestino() != null && servicio.getCargosDestino().size() > 0) {
									for (CargoDestino destino : servicio.getCargosDestino()) {
										if (destino.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& destino.getMonedaVenta().getSimbolo().equals("EUR")) {
											destino.setPedidoCliente(pedidoCliente);
										}
									}
								}

								servicioActualizado = servicioInterService.save(servicioActualizado);
							} else {
								logger.error("NO SE GENERÓ PEDIDO A CLIENTE EUR");
								resultadoSAP += "\n - No se generó pedido a Cliente [EUR]";
							}
						}

						if (containsCurrency(servicio, "GBP")) {
							pedidoCliente = pedidoClienteService.creaPedidoClienteInter(servicio, "GBP", empleado);

							if (pedidoCliente != null) {
								resultadoSAP += "\n - Se creó el pedido a Cliente [GBP] - [" + pedidoCliente + "]";
								if (servicio.getCargosOrigen() != null && servicio.getCargosOrigen().size() > 0) {
									for (CargoOrigen origen : servicio.getCargosOrigen()) {
										if (origen.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& origen.getMonedaVenta().getSimbolo().equals("GBP")) {
											origen.setPedidoCliente(pedidoCliente);
										}
									}
								}

								if (servicio.getCargosDestino() != null && servicio.getCargosDestino().size() > 0) {
									for (CargoDestino destino : servicio.getCargosDestino()) {
										if (destino.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& destino.getMonedaVenta().getSimbolo().equals("GBP")) {
											destino.setPedidoCliente(pedidoCliente);
										}
									}
								}
								servicioActualizado = servicioInterService.save(servicioActualizado);
							} else {
								logger.error("NO SE GENERÓ PEDIDO A CLIENTE GBP");
								resultadoSAP += "\n - No se generó pedido a Cliente [GBP]";
							}
						}

						if (containsCurrency(servicio, "JPY")) {
							pedidoCliente = pedidoClienteService.creaPedidoClienteInter(servicio, "JPY", empleado);

							if (pedidoCliente != null) {
								resultadoSAP += "\n - Se creó el pedido a Cliente [JPY] - [" + pedidoCliente + "]";
								if (servicio.getCargosOrigen() != null && servicio.getCargosOrigen().size() > 0) {
									for (CargoOrigen origen : servicio.getCargosOrigen()) {
										if (origen.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& origen.getMonedaVenta().getSimbolo().equals("JPY")) {
											origen.setPedidoCliente(pedidoCliente);
										}
									}
								}

								if (servicio.getCargosDestino() != null && servicio.getCargosDestino().size() > 0) {
									for (CargoDestino destino : servicio.getCargosDestino()) {
										if (destino.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& destino.getMonedaVenta().getSimbolo().equals("JPY")) {
											destino.setPedidoCliente(pedidoCliente);
										}
									}
								}
								servicioActualizado = servicioInterService.save(servicioActualizado);
							} else {
								logger.error("NO SE GENERÓ PEDIDO A CLIENTE JPY");
								resultadoSAP += "\n - No se generó pedido a Cliente [JPY]";
							}
						}

						if (containsCurrency(servicio, "USD")) {
							pedidoCliente = pedidoClienteService.creaPedidoClienteInter(servicio, "USD", empleado);

							if (pedidoCliente != null) {
								resultadoSAP += "\n - Se creó el pedido a Cliente [USD] - [" + pedidoCliente + "]";
								if (servicio.getCargosOrigen() != null && servicio.getCargosOrigen().size() > 0) {
									for (CargoOrigen origen : servicio.getCargosOrigen()) {
										if (origen.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& origen.getMonedaVenta().getSimbolo().equals("USD")) {
											origen.setPedidoCliente(pedidoCliente);
										}
									}
								}

								if (servicio.getCargosDestino() != null && servicio.getCargosDestino().size() > 0) {
									for (CargoDestino destino : servicio.getCargosDestino()) {
										if (destino.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
												&& destino.getMonedaVenta().getSimbolo().equals("USD")) {
											destino.setPedidoCliente(pedidoCliente);
										}
									}
								}

								servicioActualizado = servicioInterService.save(servicioActualizado);
							} else {
								logger.error("NO SE GENERÓ PEDIDO A CLIENTE USD");
								resultadoSAP += "\n - No se generó pedido a Cliente [USD]";
							}
						}
					} else {
						logger.info("NO SE GENERA PEDIDO DE CLIENTE PORQUE ESTATUS NO ES 3");
					}

				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos",
							response, e);
				}
				return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioActualizado, "actualizado",
						response);
			} else {
				return CommonsUtils.generaError("No se pudo crear el servicio",
						"No se encontró unidad de ventas para ejecutivo", response);
			}
		} else {
			// No se encontraron ejecutivos asociados al cliente
			return CommonsUtils.generaError("No se pudo crear el servicio",
					"No se encontraron ejecutivos asociados al cliente", response);
		}
	}

	@DeleteMapping("/cancel/{id}/{motivo}")
	@ResponseStatus
	public ResponseEntity<?> cancelarServicio(@PathVariable Long id, @PathVariable String motivo) {
		ServicioInter servicio = null;
		EstatusServicio estatusCancelado = null;
		Map<String, Object> response = new HashMap<>();

		try {
			servicio = servicioInterService.findById(id);
			estatusCancelado = estatusServicioService.findById(4L);
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (servicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}

		servicio.setEstatus(estatusCancelado);
		servicio.setMotivoCancelacion(motivo);
		servicio.setNotas("------- SERVICIO CANCELADO -------\nMOTIVO:" + motivo);

		for (CargoOrigen cso : servicio.getCargosOrigen()) {
			cso.setCompraMonto(BigDecimal.ZERO);
			cso.setVentaMonto(BigDecimal.ZERO);
			cso.setProfit(BigDecimal.ZERO);

		}

		for (CargoDestino csd : servicio.getCargosDestino()) {
			csd.setCompraMonto(BigDecimal.ZERO);
			csd.setVentaMonto(BigDecimal.ZERO);
			csd.setProfit(BigDecimal.ZERO);
		}

		servicio.setTotalprofit(BigDecimal.ZERO);
		servicio.setTotalprofitCargosOrigen(BigDecimal.ZERO);
		servicio.setTotalprofitCargosDestino(BigDecimal.ZERO);

		ServicioInter servicioActualizado = null;
		try {
			servicioActualizado = servicioInterService.save(servicio);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}

		return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioActualizado, "actualizado", response);
	}

	@GetMapping("/traficos")
	public List<ServicioInter> getTraficos() {
		return servicioInterService.findTraficos();
	}

	@GetMapping("/trafico/{id}/{idTrafico}/{nombre}")
	@ResponseStatus
	public ResponseEntity<?> updateTrafico(@PathVariable Long id, @PathVariable String idTrafico,
			@PathVariable String nombre) {
		ServicioInter servicio = null;
		Map<String, Object> response = new HashMap<>();
		try {
			servicio = servicioInterService.findById(id);
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (servicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}
		servicio.setTrafico(idTrafico);
		servicio.setNombreTrafico(nombre);

		ServicioInter servicioActualizado = null;
		try {
			servicioActualizado = servicioInterService.save(servicio);

		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioActualizado, "reasignado", response);
	}

	private String formatearConsecutivo(String consecutivo) {
		if (consecutivo.length() < 5) {
			return formatearConsecutivo("0" + consecutivo);
		}
		return consecutivo;
	}

	private EmpleadoSAP obtieneEmpleadoSAP(ServicioInter s) {
		// OBTENER ID DE EMPLEADO
		EmpleadoSAP empleadoSAP = null;

		try {
			List<Ejecutivo> ejec = clienteService.findEjecutivosByIdCliente(s.getCliente().getIdCliente());

			if (ejec != null && ejec.size() > 0) {
				String idUsuarioTmp = ejec.get(0).getIdEjecutivo() + "";

				if (idUsuarioTmp.equals("ACARRILLO") || idUsuarioTmp.equals("JCARRILLO")) {
					empleadoSAP = new EmpleadoSAP();

					if (idUsuarioTmp.equals("ACARRILLO")) {
						empleadoSAP.setIdEmpleado("1");
					} else {
						empleadoSAP.setIdEmpleado("44");
					}

					empleadoSAP.setUnidadVentas("MLGV1120");
					empleadoSAP.setIdUser(idUsuarioTmp);
					return empleadoSAP;
				} else {
					empleadoSAP = new EmpleadoSAP();
					empleadoSAP.setIdEmpleado(ejec.get(0).getIdUsuarioSAP() + "");
					empleadoSAP.setIdUser(ejec.get(0).getIdUsuarioSAP() + "");
					empleadoSAP.setUnidadVentas(ejec.get(0).getUnidadVentasSAP());
				}
			} else {
				logger.error("No se encontraron ejecutivos asociados al cliente");
			}

			return empleadoSAP;
		} catch (Exception e) {
			logger.error("Error al calcular la unidad de ventas:" + e.getMessage(), e);
			return null;
		}

	}

	private boolean containsCurrency(ServicioInter s, String currency) {
		boolean contains = false;

		if (s.getCargosOrigen() != null && s.getCargosOrigen().size() > 0) {
			for (CargoOrigen cargo : s.getCargosOrigen()) {
				if (cargo.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
						&& cargo.getMonedaVenta().getSimbolo().equals(currency)) {
					contains = true;
					break;
				}
			}
		}

		if (!contains) {
			if (s.getCargosDestino() != null && s.getCargosDestino().size() > 0) {
				for (CargoDestino cargo : s.getCargosDestino()) {
					if (cargo.getVentaMonto().compareTo(BigDecimal.ZERO) > 0
							&& cargo.getMonedaVenta().getSimbolo().equals(currency)) {
						contains = true;
						break;
					}
				}
			}
		}

		return contains;
	}

	private CargoOrigen getCargoOrigen(ServicioInter s, Long itemId) {
		CargoOrigen result = null;

		for (CargoOrigen cargo : s.getCargosOrigen()) {
			if (cargo.getItemID() == itemId) {
				result = cargo;
				break;
			}
		}

		return result;
	}

	private CargoDestino getCargoDestino(ServicioInter s, Long itemId) {
		CargoDestino result = null;

		for (CargoDestino cargo : s.getCargosDestino()) {
			if (cargo.getItemID() == itemId) {
				result = cargo;
				break;
			}
		}

		return result;
	}

}
