package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.PreferenciaExport;
import com.logistica.service.IPreferenciaExportService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/preferenciaExport")
public class PreferenciaExportRestController {

	@Autowired
	IPreferenciaExportService service;

	@GetMapping("/page/{page}")
	public Page<PreferenciaExport> pageAllPreferenciasExport(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<PreferenciaExport> getAllPreferenciasExport() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getPreferenciaExport(@PathVariable Long id) {
		PreferenciaExport preferenciaExport = null;
		Map<String, Object> response = new HashMap<>();
		try {
			preferenciaExport = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (preferenciaExport == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "preferenciaExport", String.valueOf(id), response);
		}
		return new ResponseEntity<PreferenciaExport>(preferenciaExport, HttpStatus.OK);
	}

	@GetMapping("/usuario/{idUsuario}/{tipo}")
	@ResponseStatus
	public ResponseEntity<?> getPreferenciaExportByUsuario(@PathVariable Long idUsuario, @PathVariable String tipo) {
		PreferenciaExport preferenciaExport = null;
		Map<String, Object> response = new HashMap<>();
		try {
			preferenciaExport = service.findByIdUsuarioAndTipo(idUsuario, tipo);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		return new ResponseEntity<PreferenciaExport>(preferenciaExport, HttpStatus.OK);
	}
	
	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createPreferenciaExport(@RequestBody PreferenciaExport preferenciaExport,
			BindingResult result) {
		PreferenciaExport nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = service.save(preferenciaExport);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "preferenciaExport", nuevo, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updatePreferenciaExport(@RequestBody PreferenciaExport preferenciaExport,
			BindingResult result) {
		PreferenciaExport actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(preferenciaExport.getIdPreferencia());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "preferenciaExport",
					String.valueOf(preferenciaExport.getIdPreferencia()), response);
		}
		actual.setTrafico(preferenciaExport.getTrafico());
		actual.setSeleccion(preferenciaExport.getSeleccion());

		PreferenciaExport actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "preferenciaExport", actualizado, "actualizada", response);
	}

}
