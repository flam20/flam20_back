package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.TipoEquipo;
import com.logistica.service.ITipoEquipoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/tipoEquipo")
public class TipoEquipoRestController {

	@Autowired
	ITipoEquipoService tipoEquipoService;

	@GetMapping("/page/{page}")
	public Page<TipoEquipo> pageAllTiposEquipo(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return tipoEquipoService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<TipoEquipo> getAllTiposEquipo() {
		return tipoEquipoService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getTipoEquipo(@PathVariable Long id) {
		TipoEquipo tipoEquipo = null;
		Map<String, Object> response = new HashMap<>();
		try {
			tipoEquipo = tipoEquipoService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoEquipo == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoEquipo", String.valueOf(id), response);
		}
		return new ResponseEntity<TipoEquipo>(tipoEquipo, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createTipoEquipo(@RequestBody TipoEquipo tipoEquipo, BindingResult result) {
		TipoEquipo tipoEquipoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoEquipoNuevo = tipoEquipoService.save(tipoEquipo);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoEquipo", tipoEquipoNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateTipoEquipo(@RequestBody TipoEquipo tipoEquipo, BindingResult result) {
		TipoEquipo tipoEquipoActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoEquipoActual = tipoEquipoService.findById(tipoEquipo.getIdTipoEquipo());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoEquipoActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoEquipo",
					String.valueOf(tipoEquipo.getIdTipoEquipo()), response);
		}
		tipoEquipoActual.setDescCorta(tipoEquipo.getDescCorta());
		tipoEquipoActual.setDescLarga(tipoEquipo.getDescLarga());
		tipoEquipoActual.setBorrado(tipoEquipo.getBorrado());

		TipoEquipo tipoEquipoActualizado = null;
		try {
			tipoEquipoActualizado = tipoEquipoService.save(tipoEquipoActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoEquipo", tipoEquipoActualizado, "actualizado", response);
	}

}
