package com.logistica.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.logistica.model.ArchivoBox;
import com.logistica.model.BitacoraSap;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.ControlAcceso;
import com.logistica.model.Dashboard;
import com.logistica.model.DashboardBar;
import com.logistica.model.Ejecutivo;
import com.logistica.model.EmpleadoSAP;
import com.logistica.model.EstatusServicio;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioPorCliente;
import com.logistica.service.IBitacoraSapService;
import com.logistica.service.IBoxService;
import com.logistica.service.ICargoService;
import com.logistica.service.IClienteService;
import com.logistica.service.IDashboardService;
import com.logistica.service.IEjecutivoService;
import com.logistica.service.IEstatusServicioService;
import com.logistica.service.IOrdenCompraService;
import com.logistica.service.IPedidoClienteService;
import com.logistica.service.IPedidoCompraService;
import com.logistica.service.IReporteExcelService;
import com.logistica.service.IServicioPorClienteService;
import com.logistica.service.IServicioService;
import com.logistica.util.ActionCode;
import com.logistica.util.CommonsUtils;
import com.logistica.util.ControlAccesoUtil;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/servicio")
public class ServicioRestController {

	static Logger logger = Logger.getLogger(ServicioRestController.class.getName());

	@Autowired
	IServicioService servicioService;

	@Autowired
	IServicioPorClienteService servicioPorClienteService;

	@Autowired
	IBoxService boxService;

	@Autowired
	IOrdenCompraService ordenCompraService;

//	@Autowired
//	IPedidoCompraService pedidoCompraService;

	@Autowired
	IPedidoClienteService pedidoClienteService;

	@Autowired
	IClienteService clienteService;

	@Autowired
	ICargoService cargoService;

	@Autowired
	IEjecutivoService ejecutivoService;

	@Autowired
	IReporteExcelService reporteExcelService;

	@Autowired
	IEstatusServicioService estatusServicioService;

	@Autowired
	IDashboardService dashboardService;

	@Autowired
	IBitacoraSapService bitacoraSAPService;

	@GetMapping("/page/{page}")
	public Page<Servicio> pageAllServicios(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return servicioService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Servicio> getAllServicios() {
		return servicioService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getServicio(@PathVariable Long id) {
		Servicio servicio = null;
		Map<String, Object> response = new HashMap<>();
		try {
			servicio = servicioService.findById(id);
			// List<ArchivoBox> listArchivos = boxService.consultaPODs(id);
			// logger.info("Archivos encontrados: " + listArchivos.size());
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (servicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}
		return new ResponseEntity<Servicio>(servicio, HttpStatus.OK);
	}

	@GetMapping("/search")
	public List<Servicio> getServicios(@RequestParam(value = "numeroServicio", required = false) String numeroServicio,
			@RequestParam(value = "idEstatus", required = false) Long idEstatus,
			@RequestParam(value = "idTrafico", required = false) String idTrafico,
			@RequestParam(value = "idCliente", required = false) Long idCliente,
			@RequestParam(value = "idProveedor", required = false) Long idProveedor,
			@RequestParam(value = "numeroFactura", required = false) String numeroFactura,
			@RequestParam(value = "fechaDesde", required = false) String fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) String fechaHasta,
			@RequestParam(value = "borradores", required = false) Boolean borradores,
			@RequestParam(value = "rolId", required = false) String rolId) {
		logger.info("============== BUSCA SERVICIO [ numeroServicio: " + numeroServicio + ", idEstatus: " + idEstatus
				+ ", idTrafico: " + idTrafico + ", idCliente: " + idCliente + ", idProveedor: " + idProveedor
				+ ", numeroFactura: " + numeroFactura + ", fechaDesde: " + fechaDesde + ", fechaHasta: " + fechaHasta
				+ ", borradores: " + borradores + ", rolId: " + rolId);

		if (rolId != null && rolId.length() > 0) {
			if (rolId.equalsIgnoreCase("ROLE_EJECUTIVO")) {
				return servicioService.searchServiciosVentas(numeroServicio, idEstatus, fechaDesde, fechaHasta,
						idCliente, idTrafico, idProveedor, null);
			} else if (rolId.equalsIgnoreCase("ROLE_TRAFICO_TERRESTRE")) {
				return servicioService.searchServicios(numeroServicio, idEstatus, idTrafico, idCliente, idProveedor,
						numeroFactura, fechaDesde, fechaHasta, borradores);
			} else if (rolId.equalsIgnoreCase("ROLE_SUPERADMIN")) {
				return servicioService.searchServicios(numeroServicio, idEstatus, null, idCliente, idProveedor,
						numeroFactura, fechaDesde, fechaHasta, borradores);
			} else {
				return servicioService.searchServiciosVentas(numeroServicio, idEstatus, fechaDesde, fechaHasta,
						idCliente, null, idProveedor, idTrafico);
			}
		} else {
			logger.info("ERROR - CONSULTA SIN ROL");
			return null;
		}

	}

	@GetMapping("/dashboardData")
	public Dashboard getDashboardData(@RequestParam(value = "idTrafico", required = false) String idTrafico,
			@RequestParam(value = "rolId", required = false) String rolId) {
		// logger.info("============== BUSCA DASHBOARD DATA [idTrafico: " + idTrafico +
		// "][rolId: " + rolId + "]");
		if (rolId != null && rolId.equalsIgnoreCase("ROLE_SUPERADMIN")) {
			return dashboardService.getCountToAdmin();
		} else if (rolId != null && rolId.equalsIgnoreCase("ROLE_EJECUTIVO")) {
			return dashboardService.getCountToVentas(idTrafico);
		} else {
			return dashboardService.getCountByUser(idTrafico);
		}
	}

	@GetMapping("/dashboardBarData")
	public List<DashboardBar> getDashboardBarData(@RequestParam(value = "idTrafico", required = false) String idTrafico,
			@RequestParam(value = "rolId", required = false) String rolId) {
		// logger.info("============== BUSCA DASHBOARD BAR DATA [idTrafico: " +
		// idTrafico + "][rolId: " + rolId + "]");
		if (rolId != null && rolId.equalsIgnoreCase("ROLE_SUPERADMIN")) {
			return dashboardService.getCountToAdminByMonth();
		} else if (rolId != null && rolId.equalsIgnoreCase("ROLE_EJECUTIVO")) {
			return dashboardService.getCountToVentasByMonth(idTrafico);
		} else {
			return dashboardService.getCountByUserByMonth(idTrafico);
		}

	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createServicio(@RequestBody Servicio servFront, BindingResult result) {
		logger.info("============== CREACION DE SERVICIO [" + servFront.getEstatus().getIdEstatusServicio() + "]");

		logger.info(servFront.toString());

//		for (ServicioPorCliente sxc : servFront.getServiciosPorCliente()) {
//			if (sxc.getTipoRuta().getIdTipoRuta() == 1L) {
//				logger.info("FECHA CARGA " + sxc.getFechaCarga() + " == " + sxc.getFechaCarga().toString());
//			}
//
//			if (sxc.getTipoRuta().getIdTipoRuta() == 2L) {
//				logger.info(
//						"Fecha AA " + sxc.getFechaAgenteAduanal() + " == " + sxc.getFechaAgenteAduanal().toString());
//				logger.info("Fecha CRUCE " + sxc.getFechaCruce() + " == " + sxc.getFechaCruce().toString());
//			}
//
//			if (sxc.getTipoRuta().getIdTipoRuta() == 3L) {
//				logger.info("FECHA DESTINO " + sxc.getFechaDestino() + " == " + sxc.getFechaDestino().toString());
//			}
//
//		}
		Servicio servicioNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		EmpleadoSAP empleado = obtieneEmpleadoSAP(servFront);
		if (empleado != null) {
			if (empleado.getIdEmpleado() != null && empleado.getIdEmpleado().length() > 0
					&& empleado.getUnidadVentas() != null && empleado.getUnidadVentas().length() > 0) {
				try {

					Integer consecutivoActual = servicioService.getConsecutiveByAnio(servFront.getAnio());
					if (consecutivoActual == null) {
						consecutivoActual = 1;// ultimo servicio flam 1.0
					}

					servFront.setConsecutivo(consecutivoActual + 1);
					String sAnio = "" + servFront.getAnio();
					servFront.setIdServicioStr(
							sAnio.substring(2) + "-" + formatearConsecutivo("" + servFront.getConsecutivo()));

					LocalDate today = LocalDate.now(ZoneId.of("America/Chicago"));
					servFront.setFechaServicio(today);
					servicioNuevo = servicioService.save(servFront);

					if (servicioNuevo.getServiciosPorCliente() != null
							&& servicioNuevo.getServiciosPorCliente().size() > 0) {

						for (ServicioPorCliente ruta : servicioNuevo.getServiciosPorCliente()) {
							if (ruta.getTipoRuta().getIdTipoRuta().intValue() == 3 && ruta.getArchivoPod() != null) {
								ruta.getArchivoPod().setIdServicio(servicioNuevo.getIdServicio());
							}
							/*
							 * if (ruta.getTipoRuta().getIdTipoRuta().intValue() == 1) {
							 * logger.info("FECHA CARGA: " + ruta.getFechaCarga()); } else if
							 * (ruta.getTipoRuta().getIdTipoRuta().intValue() == 2) {
							 * logger.info("FECHA CRUCE: " + ruta.getFechaCruce()); logger.info("FECHA AA: "
							 * + ruta.getFechaAgenteAduanal()); } else { logger.info("FECHA CRUCE: " +
							 * ruta.getFechaDestino()); }
							 */
						}
					}

					ordenCompraService.creaOrdenCompra(servicioNuevo, empleado);
					// pedidoCompraService.creaPedidoCompra(servicioNuevo, empleado);
					logger.info(
							"============== TERMINA CREACION DE SERVICIO [" + servicioNuevo.getIdServicioStr() + "]");
					return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioNuevo, "creado", response);
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos",
							response, e);
				}
			} else {
				return CommonsUtils.generaError("No se pudo crear el servicio",
						"No se encontró unidad de ventas para ejecutivo", response);
			}
		} else {
			// No se encontraron ejecutivos asociados al cliente
			return CommonsUtils.generaError("No se pudo crear el servicio",
					"No se encontraron ejecutivos asociados al cliente", response);
		}
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateServicio(@RequestBody Servicio servicio, BindingResult result) {
		logger.info("******************** ACTUALIZACION DE SERVICIO -> SERVICIO[" + servicio.getIdServicioStr()
				+ "] ESTATUS[" + servicio.getEstatus().getIdEstatusServicio() + "] ORDEN COMPRA ["
				+ servicio.getOrdenCompra() + "]");
		//logger.info(servicio.toString());

		Map<String, Object> response = new HashMap<>();
		EmpleadoSAP empleado = obtieneEmpleadoSAP(servicio);

		if (empleado != null) {

			if (empleado.getIdEmpleado() != null && empleado.getIdEmpleado().length() > 0
					&& empleado.getUnidadVentas() != null && empleado.getUnidadVentas().length() > 0) {
				// 1 - TRANSITO
				// 3 - CERRAR
				// 4 - CANCELAR
				// 5 - MODIFICADO ADMIN
				Servicio servicioActual = null;

				if (result.hasErrors()) {
					return CommonsUtils.generaErrorValidaciones(result, response);
				}
				try {
					servicioActual = servicioService.findById(servicio.getIdServicio());
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos",
							response, e);
				}
				if (servicioActual == null) {
					return CommonsUtils.generaErrorNoEncontrado("El", "servicio",
							String.valueOf(servicio.getIdServicio()), response);
				}

				logger.info("====> ESTATUS BD[" + servicioActual.getEstatus().getIdEstatusServicio()
						+ "] ESTATUS ENVIADO[" + servicio.getEstatus().getIdEstatusServicio() + "]");
				if (servicioActual.getEstatus().getIdEstatusServicio() == 3L) {
					logger.info("=> SERVICIO EN BD ES 3 Y LLEGO MODIFICACION, SE ACTUALIZA A 5");
					EstatusServicio modificadoEstatus = estatusServicioService.findById(new Long(5));
					servicioActual.setEstatus(modificadoEstatus);
					ControlAcceso controlAcceso = new ControlAcceso();
					LocalDateTime fechaModificacion = LocalDateTime.now(ZoneId.of("America/Chicago"));
					controlAcceso.setFechaModificacion(fechaModificacion);
					controlAcceso.setUsuarioModificacion("ADMIN");
					servicioActual.setControlAcceso(controlAcceso);

//					BitacoraSap t = new BitacoraSap(servicioActual.getIdServicio(), "MODIFICACION DE SERVICIO", "Modificación de Servicio Admin", "");
//					t.setControlAcceso(ControlAccesoUtil.generarControlAccesoCreacion());
//					bitacoraSAPService.save(t);

				} else {
					if (servicioActual.getEstatus().getIdEstatusServicio() == 1L
							&& servicio.getEstatus().getIdEstatusServicio() == 3) {
						logger.info("=> SE ESTÁ CERRANDO SERVICIO");
						servicioActual.setEstatus(servicio.getEstatus());
						LocalDate fechaCierre = LocalDate.now(ZoneId.of("America/Chicago"));
						servicioActual.setFechaCierre(fechaCierre);
					} else {
						logger.info("=> NO SE ESTA CERRANDO EL SERVICIO ");
						servicioActual.setEstatus(servicio.getEstatus());
					}
				}

				servicioActual.setIdServicioStr(servicio.getIdServicioStr());
				servicioActual.setFechaServicio(servicio.getFechaServicio());
				servicioActual.setMpc(servicio.getMpc());
				servicioActual.setServiciosPorCliente(servicio.getServiciosPorCliente());
				servicioActual.setCliente(servicio.getCliente());
				servicioActual.setPagador(servicio.getPagador());
				servicioActual.setCargosPorServicio(servicio.getCargosPorServicio());
				servicioActual.setTrafico(servicio.getTrafico());
//				servicioActual.setEstatus(servicio.getEstatus());
				servicioActual.setCostoServicioUsd(servicio.getCostoServicioUsd());
				servicioActual.setVentaServicioUsd(servicio.getVentaServicioUsd());
				servicioActual.setCostoCargoUsd(servicio.getCostoCargoUsd());
				servicioActual.setVentaCargoUsd(servicio.getVentaCargoUsd());
				servicioActual.setNotas(servicio.getNotas());
				servicioActual.setMotivoCancelacion(servicio.getMotivoCancelacion());
				servicioActual.setNotasCompletas(servicio.getNotasCompletas());
				servicioActual.setNoFacturaCliente(servicio.getNoFacturaCliente());
				servicioActual.setFacturas(servicio.getFacturas());
				servicioActual.setEstatusBox(servicio.getEstatusBox());
				servicioActual.setEstatusSat(servicio.getEstatusSat());

				if (servicio.getOrdenCompra() != null && servicio.getOrdenCompra().length() > 0) {
					servicioActual.setOrdenCompra(servicio.getOrdenCompra());
				}

				servicioActual.setDeCrm(servicio.getDeCrm());
				servicioActual.setOfertaCrm(servicio.getOfertaCrm());
				servicioActual.setAnio(servicio.getAnio());
				servicioActual.setConsecutivo(servicio.getConsecutivo());
				servicioActual.setBorrador(servicio.getBorrador());
				servicioActual.setBitacoras(servicio.getBitacoras());
				servicioActual.setIdChat(servicio.getIdChat());
				servicioActual.setNombreTrafico(servicio.getNombreTrafico());

				Servicio servicioActualizado = null;
				try {

					if (servicio.getDeCrm() != null) {
						if (servicio.getDeCrm()) {
							// ES ACTUALIZACIÓN DE CHAT
							servicioService.updateChatID(servicioActual.getIdChat(), servicioActual.getIdServicio());

						} else {
							servicioActualizado = servicioService.save(servicioActual);
							ordenCompraService.actualizaOrdenCompra(servicioActualizado, empleado);
							// pedidoCompraService.actualizaPedidoCompra(servicioActualizado, empleado);
						}
					} else {
						servicioActualizado = servicioService.save(servicioActual);
						ordenCompraService.actualizaOrdenCompra(servicioActualizado, empleado);
						// pedidoCompraService.actualizaPedidoCompra(servicioActualizado, empleado);
					}
					logger.info("ESTATUS [" + servicio.getEstatus().getIdEstatusServicio() + "]");
					if (servicio.getEstatus().getIdEstatusServicio().compareTo(new Long(3)) == 0
							|| servicio.getEstatus().getIdEstatusServicio().compareTo(new Long(5)) == 0) {
						// PEDIDO A CLIENTE CUANDO EL SERVICIO ESTÁ CERRADO
						if (validaItems(servicioActualizado)) {
							// verificar si contiene costos en usd y pesos
							if (containsCurrency(servicio, "MXN")) {
								pedidoClienteService.creaPedidoCliente(servicioActualizado, "MXN", empleado);
							}
							if (containsCurrency(servicio, "USD")) {
								pedidoClienteService.creaPedidoCliente(servicioActualizado, "USD", empleado);
							}
						} else {
							logger.info("DURANTE VALIDACION, NO HAY ITEMS QUE MANDAR PARA PEDIDO A CLIENTE");
						}
					} else {
						logger.info("NO SE GENERA PEDIDO DE CLIENTE PORQUE ESTATUS NO ES 3 O 5");
					}
					logger.info("******************** TERMINA ACTUALIZACION DE SERVICIO [" + servicio.getIdServicioStr()
							+ "]");
					return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioActualizado, "actualizado",
							response);
				} catch (DataAccessException e) {
					return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos",
							response, e);
				}
			} else {
				logger.error("No se pudo actualizar el servicio, No se encontró unidad de ventas para ejecutivo");
				return CommonsUtils.generaError("No se pudo crear el servicio",
						"No se encontró unidad de ventas para ejecutivo", response);
			}
		} else {
			// No se encontraron ejecutivos asociados al cliente
			logger.error("No se pudo actualizar el servicio, No se encontraron ejecutivos asociados al cliente");
			return CommonsUtils.generaError("No se pudo crear el servicio",
					"No se encontraron ejecutivos asociados al cliente", response);
		}
	}

	@DeleteMapping("/cancel/{id}/{motivo}")
	@ResponseStatus
	public ResponseEntity<?> cancelarServicio(@PathVariable Long id, @PathVariable String motivo) {
		Servicio servicio = null;
		EstatusServicio estatusCancelado = null;
		Map<String, Object> response = new HashMap<>();
		try {
			servicio = servicioService.findById(id);
			estatusCancelado = estatusServicioService.findById(4L);
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (servicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}
		servicio.setEstatus(estatusCancelado);
		servicio.setMotivoCancelacion(motivo);
		servicio.setNotas("------- SERVICIO CANCELADO -------\nMOTIVO:" + motivo);

		Servicio servicioActualizado = null;
		try {

			servicio.setCostoCargoUsd(BigDecimal.ZERO);
			servicio.setCostoServicioUsd(BigDecimal.ZERO);
			servicio.setVentaCargoUsd(BigDecimal.ZERO);
			servicio.setVentaServicioUsd(BigDecimal.ZERO);

			for (ServicioPorCliente ruta : servicio.getServiciosPorCliente()) {
				ruta.setCosto(BigDecimal.ZERO);
				ruta.setVenta(BigDecimal.ZERO);
			}

			for (CargoPorServicio cargo : servicio.getCargosPorServicio()) {
				cargo.setCosto(BigDecimal.ZERO);
				cargo.setVenta(BigDecimal.ZERO);
			}

			servicioActualizado = servicioService.save(servicio);

		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioActualizado, "cancelado", response);
	}

	@PostMapping("/ruta/update/{id}")
	@ResponseStatus
	public ResponseEntity<?> updateRuta(@PathVariable Long id, @RequestBody ServicioPorCliente ruta,
			BindingResult result) {
		logger.info("ACTUALIZA RUTA [" + id + "][" + ruta.getIdServicioCliente());
		logger.info("RUTA [" + ruta.toString() + "]");
		Map<String, Object> response = new HashMap<>();
		Servicio servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioService.findById(id);
			logger.info("" + servicioActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (servicioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}

		try {
			// Aqui se crea o actualiza la ruta
			String pedidoCliente = null;
			String accion = "";

			ServicioPorCliente rutaSeleccionada = getRuta(servicioActual, ruta.getTipoRuta().getIdTipoRuta());
			rutaSeleccionada.setVenta(ruta.getVenta());
			pedidoCliente = rutaSeleccionada.getPedidoCliente();

			EmpleadoSAP empleado = obtieneEmpleadoSAP(servicioActual);
			if (pedidoCliente != null && pedidoCliente.length() > 0) {
				String statusConsulta = pedidoClienteService
						.consultarStatusPedido(rutaSeleccionada.getTipoRuta().getIdTipoRuta() + "", pedidoCliente);
				if (statusConsulta.equals("true")) {
					accion = "Actualizada";
					logger.info("SE VA A ACTUALIZAR EL PEDIDO DE CLIENTE");

					pedidoClienteService.actualizaRuta(servicioActual, ActionCode.SAVE, empleado, rutaSeleccionada);
					servicioActual = servicioService.save(servicioActual);
				} else {
					logger.error("NO SE PUEDE ACTUALIZAR EL ESTATUS: " + statusConsulta);
				}

			} else {
				accion = "Creada";
				logger.info("SE VA A CREAR EL PEDIDO DE CLIENTE");
				pedidoCliente = pedidoClienteService.actualizaRuta(servicioActual, ActionCode.CREATE, empleado,
						rutaSeleccionada);
				rutaSeleccionada.setPedidoCliente(pedidoCliente);
				servicioActual = servicioService.save(servicioActual);
			}

			return CommonsUtils.generaRespuestaCreacion("La", "ruta", ruta, accion, response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
	}

	@PostMapping("/cargo/update/{id}")
	@ResponseStatus
	public ResponseEntity<?> updateCargo(@PathVariable Long id, @RequestBody CargoPorServicio cargo,
			BindingResult result) {
		logger.info("ACTUALIZA CARGO [" + id + "][" + cargo.getPosicion());
		logger.info("CARGO [" + cargo.toString() + "]");
		Map<String, Object> response = new HashMap<>();
		Servicio servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

		if (servicioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}

		try {
			// Aqui se crea o actualiza la ruta
			String pedidoCliente = null;
			String accion = "";

			CargoPorServicio cargoSeleccionado = getCargo(servicioActual, cargo.getPosicion());
			cargoSeleccionado.setVenta(cargo.getVenta());
			pedidoCliente = cargoSeleccionado.getPedidoCliente();

			EmpleadoSAP empleado = obtieneEmpleadoSAP(servicioActual);
			if (pedidoCliente != null && pedidoCliente.length() > 0) {
				String statusConsulta = pedidoClienteService.consultarStatusPedido(cargoSeleccionado.getPosicion() + "",
						pedidoCliente);
				if (statusConsulta.equals("true")) {
					accion = "Actualizada";
					logger.info("SE VA A ACTUALIZAR EL PEDIDO DE CLIENTE");

					pedidoClienteService.actualizaCargo(servicioActual, ActionCode.SAVE, empleado, cargoSeleccionado);
					servicioActual = servicioService.save(servicioActual);
				} else {
					logger.error("NO SE PUEDE ACTUALIZAR EL ESTATUS: " + statusConsulta);
				}

			} else {
				accion = "Creada";
				logger.info("SE VA A CREAR EL PEDIDO DE CLIENTE");
				pedidoCliente = pedidoClienteService.actualizaCargo(servicioActual, ActionCode.CREATE, empleado,
						cargoSeleccionado);
				cargoSeleccionado.setPedidoCliente(pedidoCliente);
				servicioActual = servicioService.save(servicioActual);
			}

			return CommonsUtils.generaRespuestaCreacion("El", "cargo", cargo, "actualizado", response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
	}

	@PostMapping("/cargo/delete/{id}")
	@ResponseStatus
	public ResponseEntity<?> deleteCargo(@PathVariable Long id, @RequestBody CargoPorServicio cargo,
			BindingResult result) {
		logger.info("BORRA CARGO [" + id + "][" + cargo.getPosicion());
		logger.info("CARGO [" + cargo.toString() + "]");
		Map<String, Object> response = new HashMap<>();
		Servicio servicioActual = null;

		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}

		try {
			servicioActual = servicioService.findById(id);
			ordenCompraService.eliminaItem(servicioActual.getIdServicioStr(), servicioActual.getOrdenCompra(),
					new Long(cargo.getPosicion()), null);
			CargoPorServicio cxs = getCargo(servicioActual, cargo.getPosicion());
			logger.info("-->" + servicioActual.getCargosPorServicio().contains(cxs));

			boolean resultdelete = servicioActual.getCargosPorServicio().remove(cxs);
			logger.info("resultdelete [" + resultdelete + "]");

			servicioActual = servicioService.save(servicioActual);
			return CommonsUtils.generaRespuestaCreacion("El", "cargo", cargo, "actualizado", response);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}

	}

	@PostMapping("/uploadPod")
	public ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo,
			@RequestParam(value = "idServicio", required = false) Long idServicio) {
		Map<String, Object> response = new HashMap<>();
		String extension = FilenameUtils.getExtension(archivo.getOriginalFilename());
		if (archivo != null && !archivo.isEmpty()) {
			ArchivoBox archivoBox = null;

			try {
				if (idServicio == null) {
					archivoBox = boxService.uploadArchivoPod(archivo.getInputStream(), extension);
				} else {
					Servicio servicioActual = null;
					try {
						servicioActual = servicioService.findById(idServicio);

						if (servicioActual == null) {
							return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(idServicio),
									response);
						}

						for (ServicioPorCliente spc : servicioActual.getServiciosPorCliente()) {
							if (spc.getArchivoPod() != null) {
								spc.setArchivoPod(null);
								servicioPorClienteService.save(spc);
							}
						}

					} catch (DataAccessException e) {
						return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos",
								response, e);
					}

					archivoBox = boxService.uploadArchivoPod(idServicio, archivo.getInputStream(), extension,
							servicioActual.getIdServicioStr());
				}

				if (archivoBox != null) {
					response.put("archivoBox", archivoBox);
					response.put("mensaje", "Se ha subido el archivo: " + archivoBox.getFileName());
					return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
				}
			} catch (IOException ioe) {
				return CommonsUtils.generaError("No se pudo cargar el archivo", response, ioe);
			}
		}
		return CommonsUtils.generaError("No se pudo cargar el archivo", "El archivo es nulo", response);
	}

	@GetMapping("/traficos")
	public List<Servicio> getTraficos() {
		return servicioService.findTraficos();
	}

	@GetMapping(value = "/searchReport", produces = MediaType.ALL_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> getServiciosReporte(
			@RequestParam(value = "numeroServicio", required = false) String numeroServicio,
			@RequestParam(value = "idEstatus", required = false) Long idEstatus,
			@RequestParam(value = "idTrafico", required = false) String idTrafico,
			@RequestParam(value = "idCliente", required = false) Long idCliente,
			@RequestParam(value = "idProveedor", required = false) Long idProveedor,
			@RequestParam(value = "numeroFactura", required = false) String numeroFactura,
			@RequestParam(value = "fechaDesde", required = false) String fechaDesde,
			@RequestParam(value = "fechaHasta", required = false) String fechaHasta,
			@RequestParam(value = "borradores", required = false) Boolean borradores,
			@RequestParam(value = "rolId", required = false) String rolId) throws IOException {
		List<Servicio> servicios = null;
		logger.info("============== BUSCA SERVICIO REPORTE [ numeroServicio: " + numeroServicio + ", idEstatus: "
				+ idEstatus + ", idTrafico: " + idTrafico + ", idCliente: " + idCliente + ", idProveedor: "
				+ idProveedor + ", numeroFactura: " + numeroFactura + ", fechaDesde: " + fechaDesde + ", fechaHasta: "
				+ fechaHasta + ", borradores: " + borradores + ", rolId: " + rolId);

		if (rolId != null && rolId.length() > 0) {
			if (rolId.equalsIgnoreCase("ROLE_EJECUTIVO")) {
				servicios = servicioService.searchServiciosVentas(numeroServicio, idEstatus, fechaDesde, fechaHasta,
						idCliente, idTrafico, idProveedor, null);
			} else if (rolId.equalsIgnoreCase("ROLE_SUPERADMIN")) {
				servicios = servicioService.searchServicios(numeroServicio, idEstatus, null, idCliente, idProveedor,
						numeroFactura, fechaDesde, fechaHasta, borradores);
			} else if (rolId.equalsIgnoreCase("ROLE_TRAFICO_TERRESTRE")) {
				servicios = servicioService.searchServicios(numeroServicio, idEstatus, idTrafico, idCliente,
						idProveedor, numeroFactura, fechaDesde, fechaHasta, borradores);
			} else {
				servicios = servicioService.searchServiciosVentas(numeroServicio, idEstatus, fechaDesde, fechaHasta,
						idCliente, null, idProveedor, idTrafico);
			}
		} else {
			logger.info("ERROR - CONSULTA SIN ROL");
			servicios = null;
		}

//		if (rolId != null && rolId.length() > 0) {
////			Ejecutivo ejec = ejecutivoService.findByIdTeams(idTrafico);
//			if (!rolId.equalsIgnoreCase("ROLE_TRAFICO_TERRESTRE")) {
//				// EXISTE EL IDTRAFICO COMO EJECUTIVO, SE BUSCAN LOS CLIENTES
////				logger.info("EJECUTIVO ENCONTRADO " + ejec.getIdEjecutivo() + " -> " + ejec.getNombreCompleto());
//				servicios = servicioService.searchServiciosVentas(numeroServicio, idEstatus, fechaDesde, fechaHasta,
//						idCliente, idTrafico, idProveedor, null);
//
//			} else {
//				servicios = servicioService.searchServicios(numeroServicio, idEstatus, idTrafico, idCliente,
//						idProveedor, numeroFactura, fechaDesde, fechaHasta, borradores);
//			}
//		} else {
//			servicios = servicioService.searchServicios(numeroServicio, idEstatus, idTrafico, idCliente, idProveedor,
//					numeroFactura, fechaDesde, fechaHasta, borradores);
//		}

		logger.info("Se encontraron " + servicios.size() + " servicios");

		if (servicios != null && !servicios.isEmpty()) {
			File fileExcel = reporteExcelService.exportToExcel(servicios);
			Resource resource = new UrlResource(fileExcel.toURI());
			Path path = resource.getFile().toPath();
			return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);
		} else {
			logger.error("NO SE ENCONTRARON SERVICIOS");
			return null;
		}
	}

	@GetMapping("/trafico/{id}/{idTrafico}/{nombre}")
	@ResponseStatus
	public ResponseEntity<?> updateTrafico(@PathVariable Long id, @PathVariable String idTrafico,
			@PathVariable String nombre) {
		Servicio servicio = null;
		Map<String, Object> response = new HashMap<>();
		try {
			servicio = servicioService.findById(id);
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (servicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "servicio", String.valueOf(id), response);
		}
		servicio.setTrafico(idTrafico);
		servicio.setNombreTrafico(nombre);

		Servicio servicioActualizado = null;
		try {
			servicioActualizado = servicioService.save(servicio);

		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "servicio", servicioActualizado, "reasignado", response);
	}

	private String formatearConsecutivo(String consecutivo) {
		if (consecutivo.length() < 6) {
			return formatearConsecutivo("0" + consecutivo);
		}
		return consecutivo;
	}

	private EmpleadoSAP obtieneEmpleadoSAP(Servicio s) {
		// OBTENER ID DE EMPLEADO
		EmpleadoSAP empleadoSAP = null;
		try {
			List<Ejecutivo> ejec = clienteService.findEjecutivosByIdCliente(s.getCliente().getIdCliente());

			if (ejec != null && ejec.size() > 0) {
				String idUsuarioTmp = ejec.get(0).getIdEjecutivo() + "";

				if (idUsuarioTmp.equals("ACARRILLO") || idUsuarioTmp.equals("JCARRILLO")) {
					empleadoSAP = new EmpleadoSAP();
					if (idUsuarioTmp.equals("ACARRILLO")) {
						empleadoSAP.setIdEmpleado("1");
					} else {
						empleadoSAP.setIdEmpleado("44");
					}

					empleadoSAP.setUnidadVentas("MLGV1120");
					empleadoSAP.setIdUser(idUsuarioTmp);
					return empleadoSAP;
				} else {
					empleadoSAP = new EmpleadoSAP();
					empleadoSAP.setIdEmpleado(ejec.get(0).getIdUsuarioSAP() + "");
					empleadoSAP.setIdUser(ejec.get(0).getIdUsuarioSAP() + "");
					empleadoSAP.setUnidadVentas(ejec.get(0).getUnidadVentasSAP());
				}
			} else {
				logger.error("No se encontraron ejecutivos asociados al cliente");
			}
			return empleadoSAP;
		} catch (Exception e) {
			logger.error("Error al calcular la unidad de ventas:" + e.getMessage(), e);
			return null;
		}

	}

	private boolean validaItems(Servicio servicio) {
		if (servicio.getServiciosPorCliente() != null) {
			for (ServicioPorCliente ruta : servicio.getServiciosPorCliente()) {
//				logger.info("RUTA PEDIDO CLIENTE ["+ruta.getPedidoCliente()+"]");
				if (ruta.getPedidoCliente() == null || ruta.getPedidoCliente().length() == 0) {
					return true;
				} else {
					// nada
				}
			}
		} else {
			logger.info("ERROR, SERVICIO SIN RUTAS?");
		}

		if (servicio.getCargosPorServicio() != null) {
			for (CargoPorServicio cargo : servicio.getCargosPorServicio()) {
//				logger.info("CARGO PEDIDO CLIENTE ["+cargo.getPedidoCliente()+"]");
				if (cargo.getPedidoCliente() == null || cargo.getPedidoCliente().length() == 0) {
					return true;
				} else {
					//
				}
			}
		} else {
//			logger.info("SERVICIO SIN CARGOS");
		}

		return false;
	}

	private boolean containsCurrency(Servicio s, String currency) {
		boolean contains = false;
		for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {
			if (sxc.getVenta().compareTo(BigDecimal.ZERO) > 0 && sxc.getMonedaVenta().getSimbolo().equals(currency)) {
				contains = true;
				break;
			}
		}

		if (!contains) {
			if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0) {
				for (CargoPorServicio c : s.getCargosPorServicio()) {
					if (c.getVenta().compareTo(BigDecimal.ZERO) > 0
							&& c.getMonedaVenta().getSimbolo().equals(currency)) {
						contains = true;
						break;
					}
				}
			}
		}
		return contains;
	}

	private ServicioPorCliente getRuta(Servicio s, Long tipo) {
		ServicioPorCliente result = null;
		for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {
			if (sxc.getTipoRuta().getIdTipoRuta() == tipo) {
				result = sxc;
				break;
			}
		}
		return result;
	}

	private CargoPorServicio getCargo(Servicio s, Integer posicion) {
		CargoPorServicio result = null;
		for (CargoPorServicio cargo : s.getCargosPorServicio()) {
			if (cargo.getPosicion() == posicion) {
				result = cargo;
				break;
			}
		}
		return result;
	}

}
