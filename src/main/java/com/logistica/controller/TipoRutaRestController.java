package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.TipoRuta;
import com.logistica.service.ITipoRutaService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/tipoRuta")
public class TipoRutaRestController {

	@Autowired
	ITipoRutaService tipoRutaService;

	@GetMapping("/page/{page}")
	public Page<TipoRuta> pageAllTiposRutas(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return tipoRutaService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<TipoRuta> getAllTiposRutas() {
		return tipoRutaService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getTipoRuta(@PathVariable Long id) {
		TipoRuta tipoRuta = null;
		Map<String, Object> response = new HashMap<>();
		try {
			tipoRuta = tipoRutaService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoRuta == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoRuta", String.valueOf(id), response);
		}
		return new ResponseEntity<TipoRuta>(tipoRuta, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createTipoRuta(@RequestBody TipoRuta tipoRuta, BindingResult result) {
		TipoRuta tipoRutaNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoRutaNuevo = tipoRutaService.save(tipoRuta);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoRuta", tipoRutaNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateMpc(@RequestBody TipoRuta tipoRuta, BindingResult result) {
		TipoRuta tipoRutaActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			tipoRutaActual = tipoRutaService.findById(tipoRuta.getIdTipoRuta());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (tipoRutaActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "tipoRuta", String.valueOf(tipoRuta.getIdTipoRuta()),
					response);
		}
		tipoRutaActual.setDescripcion(tipoRuta.getDescripcion());
		tipoRutaActual.setBorrado(tipoRuta.getBorrado());

		TipoRuta tipoRutaActualizado = null;
		try {
			tipoRutaActualizado = tipoRutaService.save(tipoRutaActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "tipoRuta", tipoRutaActualizado, "actualizado", response);
	}

}
