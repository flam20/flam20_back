package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Aeropuerto;
import com.logistica.service.IAeropuertoService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/aeropuerto")
public class AeropuertoRestController {

	@Autowired
	IAeropuertoService service;

	@GetMapping("/page/{page}")
	public Page<Aeropuerto> pageAll(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Aeropuerto> getAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		Aeropuerto aeropuerto = null;
		Map<String, Object> response = new HashMap<>();
		try {
			aeropuerto = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (aeropuerto == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "aeropuerto", String.valueOf(id), response);
		}
		return new ResponseEntity<Aeropuerto>(aeropuerto, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody Aeropuerto aeropuerto, BindingResult result) {
		Aeropuerto aeropuertoNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			aeropuertoNuevo = service.save(aeropuerto);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "aeropuerto", aeropuertoNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody Aeropuerto aeropuerto, BindingResult result) {
		Aeropuerto aeropuertoActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			aeropuertoActual = service.findById(aeropuerto.getIdAeropuerto());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (aeropuertoActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "aeropuerto",
					String.valueOf(aeropuerto.getIdAeropuerto()), response);
		}
		aeropuertoActual.setPais(aeropuerto.getPais());
		aeropuertoActual.setEstado(aeropuerto.getEstado());
		aeropuertoActual.setNombre(aeropuerto.getNombre());
		aeropuertoActual.setIniciales(aeropuerto.getIniciales());
		aeropuertoActual.setBorrado(aeropuerto.getBorrado());

		Aeropuerto aeropuertoActualizado = null;
		try {
			aeropuertoActualizado = service.save(aeropuertoActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "aeropuerto", aeropuertoActualizado, "actualizado", response);
	}

}
