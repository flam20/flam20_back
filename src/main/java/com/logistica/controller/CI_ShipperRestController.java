package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.CI_Shipper;
import com.logistica.service.ICI_ShipperService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/ci_shipper")
public class CI_ShipperRestController {
	
	@Autowired
	ICI_ShipperService service;
	
	@GetMapping("/page/{page}")
	public Page<CI_Shipper> pageAllShippers(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<CI_Shipper> getAllShippers() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getShipper(@PathVariable Long id) {
		CI_Shipper shipper = null;
		Map<String, Object> response = new HashMap<>();
		try {
			shipper = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (shipper == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "shipper", String.valueOf(id), response);
		}
		return new ResponseEntity<CI_Shipper>(shipper, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createShipper(@RequestBody CI_Shipper shipper, BindingResult result) {
		CI_Shipper shipperNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			shipperNuevo = service.save(shipper);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "shipper", shipperNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateShipper(@RequestBody CI_Shipper shipper, BindingResult result) {
		CI_Shipper shipperActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			shipperActual = service.findById(shipper.getIdShipper());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (shipperActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "shipper", String.valueOf(shipper.getIdShipper()),
					response);
		}
		shipperActual.setNombre(shipper.getNombre());
		shipperActual.setRfc(shipper.getRfc());
		shipperActual.setContacto(shipper.getContacto());
		shipperActual.setTelefono(shipper.getTelefono());
		shipperActual.setOrigen(shipper.getOrigen());
		shipperActual.setPais(shipper.getPais());
		shipperActual.setEstado(shipper.getEstado());
		shipperActual.setLocalidad(shipper.getLocalidad());
		shipperActual.setMunicipio(shipper.getMunicipio());
		shipperActual.setColonia(shipper.getColonia());
		shipperActual.setCalle(shipper.getCalle());
		shipperActual.setNumInt(shipper.getNumInt());
		shipperActual.setNumExt(shipper.getNumExt());
		shipperActual.setCodigoPostal(shipper.getCodigoPostal());
		

		CI_Shipper shipperActualizado = null;
		try {
			shipperActualizado = service.save(shipperActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "shipper", shipperActualizado, "actualizado", response);
	}

	
}
