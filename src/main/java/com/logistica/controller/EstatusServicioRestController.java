package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.EstatusServicio;
import com.logistica.service.IEstatusServicioService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/estatusServicio")
public class EstatusServicioRestController {

	@Autowired
	IEstatusServicioService estatusServicioService;

	@GetMapping("/page/{page}")
	public Page<EstatusServicio> pageAllEstatusServicio(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return estatusServicioService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<EstatusServicio> getAllEstatusServicio() {
		return estatusServicioService.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getEstatusServicio(@PathVariable Long id) {
		EstatusServicio estatusServicio = null;
		Map<String, Object> response = new HashMap<>();
		try {
			estatusServicio = estatusServicioService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (estatusServicio == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "estatusServicio", String.valueOf(id), response);
		}
		return new ResponseEntity<EstatusServicio>(estatusServicio, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createEstatusServicio(@RequestBody EstatusServicio estatusServicio, BindingResult result) {
		EstatusServicio estatusServicioNuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			estatusServicioNuevo = estatusServicioService.save(estatusServicio);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "estatusServicio", estatusServicioNuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateEstatusServicio(@RequestBody EstatusServicio estatusServicio, BindingResult result) {
		EstatusServicio estatusServicioActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			estatusServicioActual = estatusServicioService.findById(estatusServicio.getIdEstatusServicio());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (estatusServicioActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "estatusServicio",
					String.valueOf(estatusServicio.getIdEstatusServicio()), response);
		}
		estatusServicioActual.setDescripcion(estatusServicio.getDescripcion());
		estatusServicioActual.setBorrado(estatusServicio.getBorrado());

		EstatusServicio estatusServicioActualizado = null;
		try {
			estatusServicioActualizado = estatusServicioService.save(estatusServicioActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "estatusServicio", estatusServicioActualizado, "actualizado",
				response);
	}

}
