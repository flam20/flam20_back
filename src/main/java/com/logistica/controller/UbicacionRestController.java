package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Ubicacion;
import com.logistica.service.IUbicacionService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/ubicacion")
public class UbicacionRestController {

	static Logger logger = Logger.getLogger(UbicacionRestController.class.getName());

	@Autowired
	IUbicacionService ubicacionService;

	@GetMapping("/page/{page}")
	public Page<Ubicacion> pageAllUbicaciones(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return ubicacionService.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Ubicacion> getAllUbicaciones() {
		return ubicacionService.findAll();
	}

	@GetMapping("/search")
	public List<Ubicacion> getFirst10(@RequestParam(value = "ciudad", required = false) String ciudad,
			@RequestParam(value = "pais", required = false) String pais) {
//		logger.info("Buscando ubicaciones, ciudad: " + ciudad + ", pais: " + pais);
		if (ciudad != null && pais != null) {
			return ubicacionService.findFirst10ByCiudadAndPais(ciudad, pais);
		} else {
			return ubicacionService.findFirst10ByCiudad(ciudad);
		}
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> getTipoRuta(@PathVariable Long id) {
		Ubicacion ubicacion = null;
		Map<String, Object> response = new HashMap<>();
		try {
			ubicacion = ubicacionService.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (ubicacion == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "ubicacion", String.valueOf(id), response);
		}
		return new ResponseEntity<Ubicacion>(ubicacion, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> createTipoRuta(@RequestBody Ubicacion ubicacion, BindingResult result) {
		Ubicacion ubicacionNueva = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			ubicacionNueva = ubicacionService.save(ubicacion);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "ubicacion", ubicacionNueva, "creada", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> updateMpc(@RequestBody Ubicacion ubicacion, BindingResult result) {
		Ubicacion ubicacionActual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			ubicacionActual = ubicacionService.findById(ubicacion.getIdUbicacion());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (ubicacionActual == null) {
			return CommonsUtils.generaErrorNoEncontrado("La", "ubicacion", String.valueOf(ubicacion.getIdUbicacion()),
					response);
		}
		ubicacionActual.setPais(ubicacion.getPais());
		ubicacionActual.setAbreviaturaPais(ubicacion.getAbreviaturaPais());
		ubicacionActual.setEstado(ubicacion.getEstado());
		ubicacionActual.setAbreviaturaEstado(ubicacion.getAbreviaturaEstado());
		ubicacionActual.setCiudad(ubicacion.getCiudad());

		Ubicacion ubicacionActualizada = null;
		try {
			ubicacionActualizada = ubicacionService.save(ubicacionActual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("La", "ubicacion", ubicacionActualizada, "actualizada", response);
	}

}
