package com.logistica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.logistica.model.Cliente;
import com.logistica.model.Ejecutivo;
import com.logistica.service.IClienteService;
import com.logistica.util.CommonsUtils;

@CrossOrigin(origins = { "*" })
@RestController
@RequestMapping("/cliente")
public class ClienteRestController {

	@Autowired
	private IClienteService service;

	@GetMapping("/page/{page}")
	public Page<Cliente> pageAll(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return service.pageAll(pageable);
	}

	@GetMapping("/list")
	public List<Cliente> getAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	@ResponseStatus
	public ResponseEntity<?> get(@PathVariable Long id) {
		Cliente bean = null;
		Map<String, Object> response = new HashMap<>();
		try {
			bean = service.findById(id);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (bean == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cliente", String.valueOf(id), response);
		}
		return new ResponseEntity<Cliente>(bean, HttpStatus.OK);
	}

	@PostMapping
	@ResponseStatus
	public ResponseEntity<?> create(@RequestBody Cliente bean, BindingResult result) {
		Cliente nuevo = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			nuevo = service.save(bean);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar el insert a la base de datos", response, e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cliente", nuevo, "creado", response);
	}

	@PutMapping
	@ResponseStatus
	public ResponseEntity<?> update(@RequestBody Cliente bean, BindingResult result) {
		Cliente actual = null;
		Map<String, Object> response = new HashMap<>();
		if (result.hasErrors()) {
			return CommonsUtils.generaErrorValidaciones(result, response);
		}
		try {
			actual = service.findById(bean.getIdCliente());
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la consulta a la base de datos", response, e);
		}
		if (actual == null) {
			return CommonsUtils.generaErrorNoEncontrado("El", "cliente", String.valueOf(bean.getIdCliente()), response);
		}
		actual.setNombre1(bean.getNombre1());
		actual.setNombre2(bean.getNombre2());
		actual.setTerminoBusqueda(bean.getTerminoBusqueda());
		actual.setBloqueado(bean.getBloqueado());
		actual.setMotivoBloqueo(bean.getMotivoBloqueo());
		actual.setRfc(bean.getRfc());
		actual.setBorrado(bean.getBorrado());
		actual.setAereo(bean.getAereo());
		actual.setClientePotencial(bean.getClientePotencial());
		actual.setNumeroClienteSAP(bean.getNumeroClienteSAP());
		actual.setEjecutivos(bean.getEjecutivos());
		
		Cliente actualizado = null;
		try {
			actualizado = service.save(actual);
		} catch (DataAccessException e) {
			return CommonsUtils.generaErrorDataAccess("Error al realizar la actualizacion a la base de datos", response,
					e);
		}
		return CommonsUtils.generaRespuestaCreacion("El", "cliente", actualizado, "actualizado", response);
	}

	@GetMapping("/search")
	public List<Cliente> getFirst10(@RequestParam(value = "busqueda", required = false) String busqueda) {
		return service.findFirst10ByBusqueda(busqueda);
	}

	@GetMapping("/ejecutivos/{id}")
	public List<Ejecutivo> getEjecutivos(@PathVariable Long id) {
		return service.findEjecutivosByIdCliente(id);
	}

}
