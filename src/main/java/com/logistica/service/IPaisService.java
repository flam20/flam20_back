package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Pais;

public interface IPaisService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Pais> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Pais> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Pais findById(Long id);

	/**
	 * Guardar el pais
	 * 
	 * @param pais
	 * @return
	 */
	Pais save(Pais pais);

	/**
	 * Borrar pais seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
