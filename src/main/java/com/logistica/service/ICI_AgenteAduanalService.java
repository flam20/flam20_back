package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.CI_AgenteAduanal;

public interface ICI_AgenteAduanalService {
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<CI_AgenteAduanal> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<CI_AgenteAduanal> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	CI_AgenteAduanal findById(Long id);

	/**
	 * Guardar el Agente aduanal
	 * 
	 * @param cargo
	 * @return
	 */
	CI_AgenteAduanal save(CI_AgenteAduanal agenteAduanal);

	/**
	 * Borrar agente aduanal seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);
}
