package com.logistica.service;

import com.logistica.model.EmpleadoSAP;
import com.logistica.model.Servicio;


public interface IPedidoCompraService {
	
	public String creaPedidoCompra(Servicio s, EmpleadoSAP empleadoSAP);
	
	public String actualizaPedidoCompra(Servicio s, EmpleadoSAP empleadoSAP);

}
