package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.ServicioPorCliente;

public interface IServicioPorClienteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<ServicioPorCliente> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<ServicioPorCliente> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	ServicioPorCliente findById(Long id);

	/**
	 * Guardar el servicioPorCliente
	 * 
	 * @param servicioPorCliente
	 * @return
	 */
	ServicioPorCliente save(ServicioPorCliente servicioPorCliente);

	/**
	 * Borrar servicioPorCliente seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
