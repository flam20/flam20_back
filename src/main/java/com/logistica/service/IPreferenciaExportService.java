package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.PreferenciaExport;

public interface IPreferenciaExportService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<PreferenciaExport> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<PreferenciaExport> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	PreferenciaExport findById(Long id);

	/**
	 * Buscar por usuario id.
	 * 
	 * @param id
	 * @return
	 */
	PreferenciaExport findByIdUsuarioAndTipo(Long idUsuario, String tipo);

	/**
	 * Guardar la PreferenciaExport
	 * 
	 * @param PreferenciaExport
	 * @return
	 */
	PreferenciaExport save(PreferenciaExport preferenciaExport);

	/**
	 * Borrar PreferenciaExport seleccionada
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
