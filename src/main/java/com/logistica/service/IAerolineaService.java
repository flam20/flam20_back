package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Aerolinea;

public interface IAerolineaService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Aerolinea> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Aerolinea> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Aerolinea findById(Long id);

	/**
	 * Guardar el Aerolinea
	 * 
	 * @param aerolinea
	 * @return
	 */
	Aerolinea save(Aerolinea aerolinea);

	/**
	 * Borrar Aerolinea seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
