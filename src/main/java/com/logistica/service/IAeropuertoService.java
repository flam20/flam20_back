package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Aeropuerto;

public interface IAeropuertoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Aeropuerto> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Aeropuerto> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Aeropuerto findById(Long id);

	/**
	 * Guardar el Aeropuerto
	 * 
	 * @param aeropuerto
	 * @return
	 */
	Aeropuerto save(Aeropuerto aeropuerto);

	/**
	 * Borrar Aeropuerto seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
