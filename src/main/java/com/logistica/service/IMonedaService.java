package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Moneda;

public interface IMonedaService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Moneda> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Moneda> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Moneda findById(Long id);

	/**
	 * Guardar la moneda
	 * 
	 * @param moneda
	 * @return
	 */
	Moneda save(Moneda moneda);

	/**
	 * Borrar moneda seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
