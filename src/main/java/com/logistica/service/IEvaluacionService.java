package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Evaluacion;

public interface IEvaluacionService {

	/**
	 * Obtener todas paginadas
	 * 
	 * @return
	 */
	Page<Evaluacion> pageAll(Pageable pageable);

	/**
	 * Obtener todas
	 * 
	 * @return
	 */
	List<Evaluacion> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Evaluacion findById(Long id);

	/**
	 * Guardar el evaluacion
	 * 
	 * @param evaluacion
	 * @return
	 */
	Evaluacion save(Evaluacion evaluacion);

	/**
	 * Borrar evaluacion seleccionada
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
