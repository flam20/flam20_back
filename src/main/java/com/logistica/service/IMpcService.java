package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Mpc;

public interface IMpcService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Mpc> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Mpc> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Mpc findById(Long id);

	/**
	 * Guardar el mpc
	 * 
	 * @param mpc
	 * @return
	 */
	Mpc save(Mpc mpc);

	/**
	 * Borrar mpc seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
