package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Proveedor;

public interface IProveedorService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Proveedor> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Proveedor> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Proveedor findById(Long id);

	/**
	 * Guardar el proveedor
	 * 
	 * @param proveedor
	 * @return
	 */
	Proveedor save(Proveedor proveedor);

	/**
	 * Borrar proveedor seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	/**
	 * Obtener todos los proveedores disponibles
	 * 
	 * @return
	 */
	List<Proveedor> findProveedoresDisponibles();

	/**
	 * Obtener los proveedores no bloqueados.
	 * 
	 * @return
	 */
	List<Proveedor> findAllUnlocked();

	/**
	 * Obtener la lista de proveedores filtrados
	 * 
	 * @return
	 */
	// List<Proveedor> findVendorByFilter(ProveedorFilter filter);

	/**
	 * Obtener los proveedores no bloqueados y aereos
	 * 
	 * @return
	 */
	List<Proveedor> findAllUnlockedAereos();

	List<Proveedor> findFirst10ByRazonComercial(String razonComercial);

	List<Proveedor> search(String[] giros, String[] sociosComerciales, String[] coberturas, String[] patios,
			String[] codigosHazmat, String[] servicios, String[] unidadesAutAero, String[] puertos, String[] fronteras,
			String[] recursos, String[] certificacionesEmpresa, String[] certificacionesOperador, String[] equipos);

}
