package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Ejecutivo;

public interface IEjecutivoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Ejecutivo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Ejecutivo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Ejecutivo findById(Long id);
	
	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Ejecutivo findByIdTeams(String idTeams);

	/**
	 * Guardar el Ejecutivo
	 * 
	 * @param ejecutivo
	 * @return
	 */
	Ejecutivo save(Ejecutivo ejecutivo);

	/**
	 * Borrar Ejecutivo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
