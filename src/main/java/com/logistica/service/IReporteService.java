package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Reporte;

public interface IReporteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Reporte> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Reporte> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Reporte findById(Long id);

	/**
	 * Guardar el Reporte
	 * 
	 * @param Reporte
	 * @return
	 */
	Reporte save(Reporte reporte);

	/**
	 * Borrar Reporte seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);
}
