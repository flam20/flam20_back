package com.logistica.service.impl;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;

import com.logistica.dao.IPropiedadDao;
import com.logistica.dao.IServicioDao;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.EmpleadoSAP;
import com.logistica.model.PedidoCompraRequest;
import com.logistica.model.Propiedad;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioPorCliente;
import com.logistica.sap.handler.PedidoCompraHandlerResolver;
import com.logistica.sap.pedidoCompra.ManagePurchaseOrderIn;
import com.logistica.sap.pedidoCompra.PurchaseOrderBundleMaintainRequestMessageSync;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainConfirmationBundleMessageSync;
import com.logistica.sap.pedidoCompra.Service;
import com.logistica.service.IPedidoCompraService;
import com.logistica.service.impl.util.PedidoCompraUtil;
import com.logistica.util.ActionCode;
import com.sun.xml.ws.policy.privateutil.PolicyLogger;

@org.springframework.stereotype.Service
public class PedidoCompraServiceImpl implements IPedidoCompraService {
	static Logger logger = Logger.getLogger(PedidoCompraServiceImpl.class.getName());

	@Autowired
	IPropiedadDao propiedadDao;

	String pwd;

	ManagePurchaseOrderIn binding;

	PurchaseOrderBundleMaintainRequestMessageSync puRequest = null;

	PedidoCompraRequest pcr = null;

	@Autowired
	IServicioDao servicioDao;

	@Autowired
	private PedidoCompraUtil utility;

	@Autowired
	private TaskExecutor taskExecutor;

	public String creaPedidoCompra(Servicio s, EmpleadoSAP empleadoSAP) {

		logger.info("[" + s.getIdServicioStr() + "] =========== CREA PEDIDO DE COMPRA ===========");

		try {
			Set<Long> proveedores = getProveedoresOnService(s);
			for (Long proveedor : proveedores) {
				// if (supplierContainsCurrency(s, "MXN", proveedor)) {
				if (supplierContainsCurrency(s, 5, proveedor)) {
					logger.info("CREAR PEDIDO DE COMPRA PROV " + proveedor + " MXN");
					ejecutaServicioSAP(s, ActionCode.CREATE, empleadoSAP, false, proveedor, 5);

				}
				// if (supplierContainsCurrency(s, "USD", proveedor)) {
				if (supplierContainsCurrency(s, 1, proveedor)) {
					logger.info("CREAR PEDIDO DE COMPRA PROV " + proveedor + " USD");
					ejecutaServicioSAP(s, ActionCode.CREATE, empleadoSAP, false, proveedor, 1);
				}
			}

		} catch (Exception e) {
			logger.error("[" + s.getIdServicioStr() + "] Error al ejecutar el servicio de SAP:" + e.getMessage(), e);
			return null;
		}

		return null;

	}

	public String actualizaPedidoCompra(Servicio s, EmpleadoSAP empleadoSAP) {

		return null;
	}

//	private boolean validaServicio(LocalDate fecha) {
//		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
//		java.util.Date d1;
//
//		try {
//			d1 = sdformat.parse("2020-02-05");
//
//			if (fecha.compareTo(compareTo(d1) > 0) {
//				// si ejecuta
//				return true;
//			} else {
//				return false;
//			}
//		} catch (Exception e) {
//			log.error("Error al validar la fecha:" + e.getMessage(), e);
//			return false;
//		}
//	}

	private void ejecutaServicioSAP(Servicio s, ActionCode action, EmpleadoSAP empleadoSAP, boolean cancela,
			Long idProveedor, int currency) throws Exception {

		PolicyLogger pLog = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
		pLog.setLevel(Level.OFF);
		logger.info("[" + s.getIdServicioStr() + "] PEDIDO COMPRA  - ACCION [" + action.getActionCode() + "] ["
				+ currency + "] [" + idProveedor + "]");

		binding = obtieneBinding();
		String pSalesUnit = empleadoSAP.getUnidadVentas() + "-1";
		String numeroEquipo = "";

		try {

			if (getRuta(s, 3).getTipoMovimiento() != null) {
//				log.info("---> " + getRuta(s, 3).getTipoMovimiento());
				if (getRuta(s, 3).getTipoMovimiento().getIdTipoMovimiento() == 1L) {
					// Es transbordo, se manda el de descarga
//					log.info("---> " + getRuta(s, 3).getNumEquipoTipoMovimiento());
					numeroEquipo = getRuta(s, 3).getNumEquipoTipoMovimiento();
				} else {
					// Es directo, se manda el de carga
					numeroEquipo = getRuta(s, 3).getNumEquipoTipoEquipo();
				}
			} else {
				numeroEquipo = getRuta(s, 3).getNumEquipoTipoEquipo();
			}

			// Long podId = getRuta(s, 3).getPodId();

			ZoneId defaultZoneId = ZoneId.systemDefault();
			Date fechaCarga = Date.from(getRuta(s, 1).getFechaCarga().atStartOfDay(defaultZoneId).toInstant());
			Date fechaDestino = Date.from(getRuta(s, 3).getFechaDestino().atStartOfDay(defaultZoneId).toInstant());

			pcr = PedidoCompraRequest.builder().itemActionCode(action.getActionCode())
					.poActionCode(action.getActionCode()).itemServicioFLAM(s.getIdServicioStr() + "")
					.itemConsignatarioDestino(getRuta(s, 3).getConsignatario()).itemFechaCarga(fechaCarga)
					.itemFechaCruce(getRuta(s, 2) != null
							? Date.from(getRuta(s, 2).getFechaCruce().atStartOfDay(defaultZoneId).toInstant())
							: null)
					.itemFechaDestino(fechaDestino)
					.itemFechaAgenteAduanal(getRuta(s, 2) != null
							? Date.from(getRuta(s, 2).getFechaAgenteAduanal().atStartOfDay(defaultZoneId).toInstant())
							: null)
					.itemNumeroEquipoCarga(numeroEquipo).itemRequierePOD("")
					.itemTipoEquipo(getRuta(s, 3).getTipoEquipo().getDescripcionCombinada())
					.itemUbicacionCruce(getRuta(s, 2) != null ? getRuta(s, 2).getUbicacion().getLabel() : "N/A")
					.itemUnidadVentas(pSalesUnit).itemUbicacionDestino(getRuta(s, 3).getUbicacion().getLabel())
					.itemTextValue(pSalesUnit).itemUbicacionOrigen(getRuta(s, 1).getUbicacion().getLabel())
					.cancelItem(cancela).build();

			puRequest = utility.getTerrestreRequest(pcr, s, idProveedor, currency);

			/****** UserName & Password *********************/
			Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
			reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
			reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
			/************************************************/
			taskExecutor.execute(new Runnable() {
				public void run() {
					PurchaseOrderMaintainConfirmationBundleMessageSync result = null;
					try {
						result = binding.managePurchaseOrderInMaintainBundle(puRequest);
						String ordenCompra = utility.leeOrdenCompra(s.getIdServicioStr() + "", result);
						s.setOrdenCompra(ordenCompra);
						servicioDao.save(s);

					} catch (Exception e) {
						logger.error("Error al ejecutar el servicio de OC en SAP::" + e, e);
					}

				}
			});

		} catch (Exception e) {
//					ordenCompra = null;
			logger.error("[" + s.getIdServicioStr() + "] Error al ejecutar la orden de compra:: " + e.getMessage(), e);
		}

//				return ordenCompra;

	}

	private ManagePurchaseOrderIn obtieneBinding() {
		ManagePurchaseOrderIn binding = null;
		// Propiedad prop = propiedadDao.getOne(1L);
//		if (prop != null && prop.getValor().equals("PRD")) {
//			ServicePRD service = new ServicePRD();
//			PolicyLogger logger = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
//			logger.setLevel(Level.OFF);
//			OrdenCompraHandlerResolver myHanlderResolver = new OrdenCompraHandlerResolver();
//			service.setHandlerResolver(myHanlderResolver);
//			binding = service.getBinding();
//			Propiedad pass = propiedadDao.findByProp("prd_pass");
//			pwd = pass.getValor();
////			log.info("EJECUCION A PRD");
//
//		} else {
		Service service = new Service();
		logger.info("EJECUCION A DEV");
		PolicyLogger logger = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
		logger.setLevel(Level.OFF);
		PedidoCompraHandlerResolver myHanlderResolver = new PedidoCompraHandlerResolver();
		service.setHandlerResolver(myHanlderResolver);
		binding = service.getBinding();
		Propiedad pass = propiedadDao.findByProp("dev_pass");
		pwd = pass.getValor();
//			log.info("EJECUCION A DEV");
//		}
		return binding;
	}

	private ServicioPorCliente getRuta(Servicio s, int tipo) {
		ServicioPorCliente result = null;
		for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {
			if (sxc.getTipoRuta().getIdTipoRuta() == tipo) {
				result = sxc;
				break;
			}
		}
		return result;
	}

	private Set<Long> getProveedoresOnService(Servicio s) {
		List<Long> proveedores = new ArrayList<Long>();

		for (ServicioPorCliente ruta : s.getServiciosPorCliente()) {
			proveedores.add(ruta.getProveedor().getIdProveedor());
		}

		if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0)
			for (CargoPorServicio cargo : s.getCargosPorServicio()) {
				proveedores.add(cargo.getProveedor().getIdProveedor());
			}

		Set<Long> aSet = new HashSet<Long>(proveedores);
		logger.info("== PROVEEDORES EN EL SERVICIO ==");
		for (Long l : aSet) {
			logger.info("  [" + l + "]");
		}
		return aSet;

	}

	private boolean supplierContainsCurrency(Servicio s, int currency, Long idProveedor) {
		logger.info("== supplierContainsCurrency == " + idProveedor);
		logger.info("== currency == " + currency);

		boolean contains = false;
		for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {

			if (sxc.getVenta().compareTo(BigDecimal.ZERO) > 0) {
//				logger.info("===== sxc.getMonedaVenta() == " + sxc.getMonedaVenta());
//				logger.info("===== sxc.getMonedaVenta().getSimbolo() == " + sxc.getMonedaVenta().getIdMoneda());
				if (sxc.getMonedaVenta().getIdMoneda() == currency) {
					if (sxc.getProveedor().getIdProveedor().compareTo(idProveedor) == 0) {
						logger.info("===== sxc.getProveedor() == " + sxc.getProveedor());
						contains = true;
						break;
					}
				}
			}

		}

		if (!contains) {
			if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0) {
				for (CargoPorServicio c : s.getCargosPorServicio()) {
					if (c.getVenta().compareTo(BigDecimal.ZERO) > 0 && c.getMonedaVenta().getIdMoneda() == currency
							&& c.getProveedor().getIdProveedor().compareTo(idProveedor) == 0) {
						contains = true;
						break;
					}
				}
			}
		}
		return contains;

	}

}
