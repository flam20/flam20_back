package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITipoRutaDao;
import com.logistica.model.TipoRuta;
import com.logistica.service.ITipoRutaService;

@Service
public class TipoRutaServiceImpl implements ITipoRutaService {

	@Autowired
	ITipoRutaDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TipoRuta> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoRuta> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoRuta findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TipoRuta save(TipoRuta tipoRuta) {
		return dao.save(tipoRuta);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TipoRuta tipoRuta = dao.findById(id).orElse(null);
		if (tipoRuta != null) {
			dao.delete(tipoRuta);
		}
	}

}
