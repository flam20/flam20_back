package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ISeguroDao;
import com.logistica.model.cartaporte.Seguro;
import com.logistica.service.ISeguroService;

@Service
public class SeguroServiceImpl implements ISeguroService {

	@Autowired
	ISeguroDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Seguro> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Seguro> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Seguro findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Seguro save(Seguro seguro) {
		return dao.save(seguro);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Seguro seguro = dao.findById(id).orElse(null);
		if (seguro != null) {
			dao.delete(seguro);
		}
	}

}
