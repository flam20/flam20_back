package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IAeropuertoDao;
import com.logistica.model.Aeropuerto;
import com.logistica.service.IAeropuertoService;

@Service
public class AeropuertoServiceImpl implements IAeropuertoService {

	@Autowired
	IAeropuertoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Aeropuerto> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Aeropuerto> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Aeropuerto findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Aeropuerto save(Aeropuerto aeropuerto) {
		return dao.save(aeropuerto);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Aeropuerto aeropuerto = dao.findById(id).orElse(null);
		if (aeropuerto != null) {
			dao.delete(aeropuerto);
		}
	}

}
