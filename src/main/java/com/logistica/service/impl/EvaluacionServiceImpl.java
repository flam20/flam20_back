package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IEvaluacionDao;
import com.logistica.model.Evaluacion;
import com.logistica.service.IEvaluacionService;

@Service
public class EvaluacionServiceImpl implements IEvaluacionService {

	@Autowired
	private IEvaluacionDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Evaluacion> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Evaluacion> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Evaluacion findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Evaluacion save(Evaluacion evaluacion) {
		return dao.save(evaluacion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Evaluacion evaluacion = dao.findById(id).orElse(null);
		if (evaluacion != null) {
			dao.delete(evaluacion);
		}
	}

}
