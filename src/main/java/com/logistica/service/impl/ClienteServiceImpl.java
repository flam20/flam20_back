package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IClienteDao;
import com.logistica.model.Cliente;
import com.logistica.model.Ejecutivo;
import com.logistica.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService {

//	static Logger logger = Logger.getLogger(ClienteServiceImpl.class.getName());

	@Autowired
	private IClienteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Cliente> pageAll(Pageable pageable) {
//		logger.info("pageAll");
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() {
//		logger.info("findAll");
//		return dao.findAll();
		return dao.findByClientePotencial(Boolean.FALSE);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Cliente save(Cliente cliente) {
		return dao.save(cliente);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Cliente cliente = dao.findById(id).orElse(null);
		if (cliente != null) {
			dao.delete(cliente);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAllUnlocked() {
//		logger.info("findAllUnlocked");
		return dao.findByBloqueado(Boolean.FALSE);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findFirst10ByBusqueda(String busqueda) {
//		logger.info("findFirst10ByBusqueda");
//		return dao.findFirst10ByTerminoBusquedaContainingIgnoreCase(busqueda);
//		return dao.findFirst10ByTerminoBusquedaContainingIgnoreCaseAndBloqueado(busqueda, Boolean.FALSE);
//		String busquedaEscape = busqueda.replace("&","\\&");
//		logger.info("Busqueda init ["+busqueda+"]");
		busqueda = busqueda.replace("(amp)", "&");
//		logger.info("Busqueda replace ["+busqueda+"]");
		return dao.findFirst10ByTerminoBusquedaContainingIgnoreCaseAndBloqueadoAndClientePotencial(busqueda, Boolean.FALSE,Boolean.FALSE);
	}

	@Override
	public List<Ejecutivo> findEjecutivosByIdCliente(Long id) {
//		log.info("Buscando ejecutivos del cliente: " + id);
		Cliente cliente = dao.findById(id).orElse(null);
		if (cliente != null) {
//			log.info("Ejecutivos encontrados [" + cliente.getEjecutivos().size() + "]");
			return cliente.getEjecutivos();
		} else {
//			log.error("El cliente es nulo");
			return null;
		}
	}

}
