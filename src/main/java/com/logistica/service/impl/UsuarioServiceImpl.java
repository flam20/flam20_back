package com.logistica.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IUsuarioDao;
import com.logistica.model.Usuario;
import com.logistica.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioDao usuarioDao;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	@Transactional(readOnly = true)
	public Usuario findByUsername(String username) {
		return usuarioDao.findByUsername(username);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Usuario> pageAll(Pageable pageable) {
		return usuarioDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return usuarioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findById(Long id) {
		return usuarioDao.findById(id).orElse(null);
	}

	@Override
	@Transactional()
	public Usuario save(Usuario usuario) {
		if (usuario != null) {
			if (usuario.getPassword() == null || usuario.getPassword().isEmpty()) {
				usuario.setPassword(passwordEncoder.encode("temporal123"));
			}

			if (usuario.getEnabled() == null) {
				usuario.setEnabled(Boolean.TRUE);
			}
		}
		return usuarioDao.save(usuario);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findCuentasPorPagarAndAdministracion() {
		List<Long> ids = new ArrayList<>();
		ids.add(1L);
		ids.add(4L);
		return usuarioDao.findByRolIdRolIn(ids);
	}

}
