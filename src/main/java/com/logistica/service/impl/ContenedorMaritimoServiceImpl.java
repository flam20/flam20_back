package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IContenedorMaritimoDao;
import com.logistica.model.cartaporte.ContenedorMaritimo;
import com.logistica.service.IContenedorMaritimoService;

@Service
public class ContenedorMaritimoServiceImpl implements IContenedorMaritimoService {

	@Autowired
	IContenedorMaritimoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<ContenedorMaritimo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ContenedorMaritimo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ContenedorMaritimo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ContenedorMaritimo save(ContenedorMaritimo contenedorMaritimo) {
		return dao.save(contenedorMaritimo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ContenedorMaritimo contenedorMaritimo = dao.findById(id).orElse(null);
		if (contenedorMaritimo != null) {
			dao.delete(contenedorMaritimo);
		}
	}

}
