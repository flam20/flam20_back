package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITipoCambioDao;
import com.logistica.model.AnioMes;
import com.logistica.model.EnumMes;
import com.logistica.model.Mes;
import com.logistica.model.Moneda;
import com.logistica.model.TipoCambio;
import com.logistica.service.ITipoCambioService;

@Service
public class TipoCambioServiceImpl implements ITipoCambioService {

	@Autowired
	private ITipoCambioDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TipoCambio> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoCambio> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoCambio findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional()
	public TipoCambio save(TipoCambio tipoCambio) {
		return dao.save(tipoCambio);
	}

	@Override
	@Transactional()
	public void delete(Long id) {
		TipoCambio tipoCambio = dao.findById(id).orElse(null);
		if (tipoCambio != null) {
			dao.delete(tipoCambio);
		}
	}

	@Override
	public List<TipoCambio> findByAnioMes(Integer anio, Long mes) {
		return dao.findByAnioMesAnioAndAnioMesMesIdMes(anio, mes);
	}

	@Override
	public TipoCambio getByAnioMesMoneda(int anio, EnumMes mesEnum, Moneda moneda) {
		AnioMes aniomes = new AnioMes();
		aniomes.setAnio(anio);
		Mes mes = new Mes();
		mes.setIdMes(new Long(mesEnum.getIdMes()));
		aniomes.setMes(mes);
		return dao.getByAnioMesMoneda(aniomes, moneda);
	}

}
