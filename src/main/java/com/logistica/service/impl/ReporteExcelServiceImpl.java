package com.logistica.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logistica.dao.ICargoAereoDao;
import com.logistica.dao.IReporteAMDao;
import com.logistica.model.Cargo;
import com.logistica.model.CargoDestino;
import com.logistica.model.CargoOrigen;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.Cliente;
import com.logistica.model.Contacto;
import com.logistica.model.EnumMes;
import com.logistica.model.Evaluacion;
import com.logistica.model.Moneda;
import com.logistica.model.Proveedor;
import com.logistica.model.Reporte;
import com.logistica.model.ReporteAM;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;
import com.logistica.model.ServicioPorCliente;
import com.logistica.model.TipoCambio;
import com.logistica.model.TipoRuta;
import com.logistica.service.ICargoService;
import com.logistica.service.IClienteService;
import com.logistica.service.IEvaluacionService;
import com.logistica.service.IReporteExcelService;
import com.logistica.service.IReporteService;
import com.logistica.service.ITipoCambioService;

@Service
public class ReporteExcelServiceImpl implements IReporteExcelService {
	static Logger logger = Logger.getLogger("requestLogger");

	@Autowired
	IReporteService reporteService;

	@Autowired
	ICargoService cargoService;

	@Autowired
	ITipoCambioService tipoCambioService;

	@Autowired
	IClienteService clienteService;

	@Autowired
	IEvaluacionService evaluacionService;

	Cargo cargoTitulo;

	TipoCambio tipoCambioServicio;

	@Autowired
	IReporteAMDao reporteAMDao;

	@Autowired
	ICargoAereoDao cargoAereoDao;

	public File exportToExcel(List<Servicio> servicios) {

		FileOutputStream outputStream = null;
		HSSFWorkbook workBook = null;
		logger.info("======================================= INICIA EXPORTACION DEL REPORTE TERRESTRE ["+servicios.size()+"] SERVICIOS");
		SimpleDateFormat sdfMes = new SimpleDateFormat("dd/MMM/yy");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
		Reporte reporte = null;
		try {
			workBook = new HSSFWorkbook();
			HSSFSheet workSheet = workBook.createSheet("Servicios");
			Row excelRow = null;
			Cell excelCell = null;
			int rowNumber = 0;

			excelRow = workSheet.createRow(rowNumber++);
			HSSFFont font = workBook.createFont();
			HSSFCellStyle style = workBook.createCellStyle();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			style.setFont(font);

			List<Reporte> titulos = reporteService.findAll();
			List<Cargo> cargosAll = cargoService.findAll();

			long TInicio, TFin, tiempo;
			TInicio = System.currentTimeMillis();
			// Titulos Basicos Reporte
			int indexTitulos = titulos.size();

			for (int z = 0; z < titulos.size(); z++) {

				reporte = titulos.get(z);

				excelCell = excelRow.createCell(reporte.getColumnaPosicion());
				excelCell.setCellValue(reporte.getColumnaTitulo());
				excelCell.setCellStyle(style);

			}

			indexTitulos = this.pintaTitulos(excelCell, excelRow, " - COSTO DLS ", indexTitulos, style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("COSTO DE RUTAS + COSTO DE CARGOS (DLS)");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA ORIGEN - DLS");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA DESTINO - DLS");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA CRUCE - DLS");
			excelCell.setCellStyle(style);

			indexTitulos = this.pintaTitulos(excelCell, excelRow, " - VENTA DLS", indexTitulos, style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA DE RUTAS + VENTA DE CARGOS (DLS)");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("UTILIDAD TOTAL (DLS)");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("COSTO ORIGEN - MXN");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("COSTO DESTINO - MXN");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("COSTO CRUCE - MXN");
			excelCell.setCellStyle(style);

			indexTitulos = this.pintaTitulos(excelCell, excelRow, " - COSTO MXN", indexTitulos, style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("COSTO DE RUTA + COSTO DE CARGOS (MXN)");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA ORIGEN - MXN");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA DESTINO - MXN");
			excelCell.setCellStyle(style);

			excelCell = excelRow.createCell(indexTitulos++);
			excelCell.setCellValue("VENTA CRUCE - MXN");
			excelCell.setCellStyle(style);

			indexTitulos = this.pintaTitulos(excelCell, excelRow, " - VENTA MXN", indexTitulos, style);

			int i = indexTitulos++;
			logger.info("VENTA DE RUTA + VENTA DE CARGOS (MXN)-->"+i);
			excelCell = excelRow.createCell(i);
			excelCell.setCellValue("VENTA DE RUTA + VENTA DE CARGOS (MXN)");
			excelCell.setCellStyle(style);

			//excelCell = excelRow.createCell(140);
			i = indexTitulos++;
			logger.info("OBSERVACIONES-->"+i);
			excelCell = excelRow.createCell(i);
			excelCell.setCellValue("OBSERVACIONES");
			excelCell.setCellStyle(style);
			
			i = indexTitulos++;
			logger.info("FECHA CIERRE-->"+i);
			excelCell = excelRow.createCell(i);
			excelCell.setCellValue("FECHA CIERRE");
			excelCell.setCellStyle(style);

			//////////////////////////////////////////////////////////////////////////////////////
			for (Servicio item : servicios) {
				excelRow = workSheet.createRow(rowNumber++);

				List<ServicioPorCliente> lSxC = item.getServiciosPorCliente();

				BigDecimal sumaCostoCargos = new BigDecimal(0);
				BigDecimal sumaVentaCargos = new BigDecimal(0);
//				ZoneId defaultZoneId = ZoneId.systemDefault();
//				Date date = Date.from(item.getFechaServicio().atStartOfDay(defaultZoneId).toInstant());
				this.getTipoCambioServicio(localDateToDate(item.getFechaServicio()),item.getIdServicioStr());

				String errorMsgLog = "";

				for (ServicioPorCliente sxc : lSxC) {
					TipoRuta tipo = sxc.getTipoRuta();
					try {
						excelCell = excelRow.createCell(0);
						excelCell.setCellValue(
								item.getIdServicioStr() != null ? item.getIdServicioStr().toString() : "");

						excelCell = excelRow.createCell(2);
						excelCell.setCellValue(item.getFechaServicio() != null
								? sdfMes.format(localDateToDate(item.getFechaServicio())).toUpperCase()
								: "");

						excelCell = excelRow.createCell(3);
						excelCell.setCellValue(item.getMpc().getDescripcion() != null ? item.getMpc().getDescripcion()
								: "NO CONTIENE MPC");

						excelCell = excelRow.createCell(4);
						excelCell.setCellValue(
								item.getEstatus().getDescripcion() != null ? item.getEstatus().getDescripcion()
										: "NO CONTIENE ESTATUS");

						excelCell = excelRow.createCell(7);
						excelCell.setCellValue(item.getTrafico() != null ? item.getNombreTrafico() : ""); // OP

						// CLIENTE

						excelCell = excelRow.createCell(1);
						excelCell.setCellValue(
								item.getCliente().getIdCliente() != null ? item.getCliente().getIdCliente() : 0L);

						// excelCell = excelRow.createCell(16);
						// excelCell.setCellValue(item.getCliente() != null ?
						// item.getCliente().getNombre1() : "");

						excelCell = excelRow.createCell(10);
						excelCell.setCellValue(item.getCliente() != null ? item.getCliente().getNombre1() : "");

						// excelCell = excelRow.createCell(20);
						// excelCell.setCellValue(item.getCliente() != null ?
						// item.getCliente().getNombre1() : "");

					} catch (Exception e) {

						logger.error("Datos 0 - 7 servicio: " + item.getIdServicioStr() + " Error: " + e.getMessage(),
								e);

						errorMsgLog += "Este servicio contiene datos con informacion incompleta";

					}
					if (tipo.getDescripcion().equalsIgnoreCase("Origen")) {

						try {

							excelCell = excelRow.createCell(14);
							excelCell.setCellValue(sxc.getUbicacion() != null && sxc.getUbicacion().getLabel() != null
									? sxc.getUbicacion().getLabel()
									: "");

							excelCell = excelRow.createCell(15);
							excelCell.setCellValue(
									sxc.getFechaCarga() != null ? sdf.format(localDateToDate(sxc.getFechaCarga()))
											: "");

							excelCell = excelRow.createCell(22);
							excelCell.setCellValue(
									sxc.getProveedor() != null ? sxc.getProveedor().getRazonComercial() : "");

							Map<String, BigDecimal> mapa = null;

							if (sxc.getCosto() != null && sxc.getMonedaCosto().getSimbolo() != null) { // CTO
																										// DLS

								mapa = this.validaCosto(sxc);

								BigDecimal costoDolar = mapa.get("DOLAR");
								BigDecimal costoPesos = mapa.get("PESOS");

								excelCell = excelRow.createCell(25);
								excelCell.setCellValue(costoDolar != null ? costoDolar.doubleValue() : new Double(0));
								excelCell = excelRow.createCell(82);
								excelCell.setCellValue(costoPesos != null ? costoPesos.doubleValue() : new Double(0));
								mapa = null;
							}

							if (sxc.getVenta() != null && sxc.getMonedaVenta().getSimbolo() != null) {// VNT
																										// DLS

								mapa = validaVenta(sxc);

								BigDecimal ventaDolar = mapa.get("DOLAR");
								BigDecimal ventaPesos = mapa.get("PESOS");

								excelCell = excelRow.createCell(53);
								excelCell.setCellValue(ventaDolar != null ? ventaDolar.doubleValue() : new Double(0));
								excelCell = excelRow.createCell(110);
								excelCell.setCellValue(ventaPesos != null ? ventaPesos.doubleValue() : new Double(0));
								mapa = null;
							}

						} catch (Exception e) {
							logger.error(
									" Datos Origen servicio: " + item.getIdServicioStr() + " Error: " + e.getMessage(),
									e);

							errorMsgLog += "Este servicio contiene datos origen con informacion incompleta";
						}
					}
					if (tipo.getDescripcion().equalsIgnoreCase("Cruce")) {

						try {

							excelCell = excelRow.createCell(16);
							excelCell.setCellValue(
									sxc.getUbicacion().getLabelCruce() != null ? sxc.getUbicacion().getLabelCruce()
											: "");

							excelCell = excelRow.createCell(17);
							excelCell.setCellValue(sxc.getFechaAgenteAduanal() != null
									? sdf.format(localDateToDate(sxc.getFechaAgenteAduanal()))
									: "");

							excelCell = excelRow.createCell(18);
							excelCell.setCellValue(
									sxc.getFechaCruce() != null ? sdf.format(localDateToDate(sxc.getFechaCruce()))
											: "");

							excelCell = excelRow.createCell(23);
							excelCell.setCellValue(
									sxc.getProveedor() != null ? sxc.getProveedor().getRazonComercial() : "");

							Map<String, BigDecimal> mapa = null;
							if (sxc.getCosto() != null && sxc.getMonedaCosto().getSimbolo() != null) { // CTO

								mapa = this.validaCosto(sxc);

								BigDecimal costoDolar = mapa.get("DOLAR");
								BigDecimal costoPesos = mapa.get("PESOS");

								excelCell = excelRow.createCell(27);
								excelCell.setCellValue(costoDolar != null ? costoDolar.doubleValue() : new Double(0));
								excelCell = excelRow.createCell(84);
								excelCell.setCellValue(costoPesos != null ? costoPesos.doubleValue() : new Double(0));
								mapa = null;
							}

							if (sxc.getVenta() != null && sxc.getMonedaVenta().getIdMoneda() != null) {// VNT

								mapa = this.validaVenta(sxc);

								BigDecimal ventaDolar = mapa.get("DOLAR");
								BigDecimal ventaPesos = mapa.get("PESOS");

								excelCell = excelRow.createCell(55);
								excelCell.setCellValue(ventaDolar != null ? ventaDolar.doubleValue() : new Double(0));
								excelCell = excelRow.createCell(112);
								excelCell.setCellValue(ventaPesos != null ? ventaPesos.doubleValue() : new Double(0));
								mapa = null;
							}
							///////////
						} catch (Exception e) {
							logger.error(
									"Datos Cruce servicio: " + item.getIdServicioStr() + " Error: " + e.getMessage(),
									e);

							errorMsgLog += " Este servicio contiene datos de CRUCE con informacion incompleta";

						}

					}

					if (tipo.getDescripcion().equalsIgnoreCase("Destino")) {

						try {

							excelCell = excelRow.createCell(5);
							excelCell.setCellValue(sxc.getNumFactura() != null ? sxc.getNumFactura() : "");

							excelCell = excelRow.createCell(6);
							excelCell.setCellValue(
									sxc.getFechaFactura() != null ? sdf.format(localDateToDate(sxc.getFechaFactura()))
											: "");

							excelCell = excelRow.createCell(8);

							String descripcionPOD = "";
//							logger.info("POD "+sxc.getPodId());
							if (sxc.getPodId() != null) {
								if (sxc.getPodId() == 1L) {
									descripcionPOD = "No";
								} else if (sxc.getPodId() == 2L) {
									descripcionPOD = "Si/Digital";
								} else {
									descripcionPOD = "Si/Original";
								}
							}

							excelCell.setCellValue(descripcionPOD);

							Cliente tmpCli = clienteService.findById(item.getCliente().getIdCliente());
							//
							// for (Usuario ejec: tmpCli.getEjecutivos()) {
							// System.out.println("EJEC:"+ejec.getIdUsuario());
							// }

							if (tmpCli.getEjecutivos() != null && !tmpCli.getEjecutivos().isEmpty()) {
								String ejecutivos = "";

								for (int ind = 0; ind < tmpCli.getEjecutivos().size(); ind++) {
									ejecutivos += tmpCli.getEjecutivos().get(ind).getNombre() + " "
											+ tmpCli.getEjecutivos().get(ind).getApellidoPaterno() + " / ";
								}

								excelCell = excelRow.createCell(9);
								excelCell.setCellValue(ejecutivos);
							}

							excelCell = excelRow.createCell(11);
							excelCell.setCellValue(sxc.getConsignatario() != null ? sxc.getConsignatario() : "");

							excelCell = excelRow.createCell(12);
							excelCell.setCellValue(sxc.getTipoEquipo().getDescripcionCombinada() != null
									? sxc.getTipoEquipo().getDescripcionCombinada()
									: "");

							excelCell = excelRow.createCell(13);
							excelCell.setCellValue(
									sxc.getNumEquipoTipoEquipo() != null ? sxc.getNumEquipoTipoEquipo() : "");

							excelCell = excelRow.createCell(19);
							excelCell.setCellValue(sxc.getUbicacion() != null && sxc.getUbicacion().getLabel() != null
									? sxc.getUbicacion().getLabel()
									: "");

							excelCell = excelRow.createCell(20);
							excelCell.setCellValue(
									sxc.getFechaDestino() != null ? sdf.format(localDateToDate(sxc.getFechaDestino()))
											: "");

							excelCell = excelRow.createCell(21);
							excelCell
									.setCellValue(sxc.getReferenciaCliente() != null ? sxc.getReferenciaCliente() : "");

							excelCell = excelRow.createCell(24);
							excelCell.setCellValue(
									sxc.getProveedor() != null ? sxc.getProveedor().getRazonComercial() : "");

							Map<String, BigDecimal> mapa = null;
							if (sxc.getCosto() != null && sxc.getMonedaCosto().getIdMoneda() != null) { // CTO
								// DLS

								mapa = this.validaCosto(sxc);

								BigDecimal costoDolar = mapa.get("DOLAR");
								BigDecimal costoPesos = mapa.get("PESOS");

								excelCell = excelRow.createCell(26);
								excelCell.setCellValue(costoDolar != null ? costoDolar.doubleValue() : new Double(0));
								excelCell = excelRow.createCell(83);
								excelCell.setCellValue(costoPesos != null ? costoPesos.doubleValue() : new Double(0));
								mapa = null;
							}

							if (sxc.getVenta() != null && sxc.getMonedaVenta().getIdMoneda() != null) {// VNT
																										// DLS

								mapa = this.validaVenta(sxc);

								BigDecimal ventaDolar = mapa.get("DOLAR");
								BigDecimal ventaPesos = mapa.get("PESOS");

								excelCell = excelRow.createCell(54);
								excelCell.setCellValue(ventaDolar != null ? ventaDolar.doubleValue() : new Double(0));
								excelCell = excelRow.createCell(111);
								excelCell.setCellValue(ventaPesos != null ? ventaPesos.doubleValue() : new Double(0));
								mapa = null;
							}
							/////////
						} catch (Exception e) {
							logger.error(
									" Datos Destino servicio: " + item.getIdServicioStr() + " Error: " + e.getMessage(),
									e);

							errorMsgLog += " Este servicio contiene datos destino con informacion incompleta";
						}
					}

				} // FOR ListaServiciosXCLiente (RUTAS)

				// SE CALCULA UTILIDAD

				if (item.getCostoServicioUsd() != null && item.getCostoCargoUsd() != null) {

					// System.out.println("costo Servico: " +
					// item.getCostoServicioUSD());
					// System.out.println("costo Cargo: " +
					// item.getCostoServicioUSD());

					excelCell = excelRow.createCell(52); // DLS COSTO CARGOS + COSTO
															// RUTAS

					String indiceIni1 = "Z" + rowNumber;
					String indiceFin2 = "AZ" + rowNumber;
					String strFormula3 = "SUM(" + indiceIni1 + ":" + indiceFin2 + ")";
					excelCell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
					excelCell.setCellFormula(strFormula3);

					excelCell.setCellStyle(style);

					excelCell = excelRow.createCell(109);
					// // MXN COSTO CARGOS + COSTO RUTAS
					String indiceIni = "CE" + rowNumber;
					String indiceFin = "DE" + rowNumber;
					String strFormula = "SUM(" + indiceIni + ":" + indiceFin + ")";
					excelCell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
					excelCell.setCellFormula(strFormula);
					excelCell.setCellStyle(style);
				}

				if (item.getVentaCargoUsd() != null && item.getVentaServicioUsd() != null) {

					// BigDecimal ventaCargosRutasDLS =
					// item.getVentaServicioUSD().add(item.getVentaCargoUSD());

					excelCell = excelRow.createCell(80);// DLS VENTA CARGOS + VENTA
														// RUTAS DLS

					String indiceIni1 = "BB" + rowNumber;
					String indiceFin2 = "CB" + rowNumber;
					String strFormula3 = "SUM(" + indiceIni1 + ":" + indiceFin2 + ")";
					excelCell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
					excelCell.setCellFormula(strFormula3);
					excelCell.setCellStyle(style);

					excelCell = excelRow.createCell(137);
					// // MXN VENTA CARGOS + VENTA RUTAS
					String indiceIni = "DG" + rowNumber;
					String indiceFin = "EG" + rowNumber;
					String strFormula = "SUM(" + indiceIni + ":" + indiceFin + ")";
					excelCell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
					excelCell.setCellFormula(strFormula);
					excelCell.setCellStyle(style);

				}
//				log.info("CALCULO DE UTILIDAD");
				excelCell = excelRow.createCell(81);
				if (item.getCostoCargoUsd() != null && item.getCostoServicioUsd() != null) {

					// sumaCostoCargos =
					// item.getCostoCargoUSD().add(item.getCostoServicioUSD());
					// sumaVentaCargos =
					// item.getVentaCargoUSD().add(item.getVentaServicioUSD());

					String indiceIni1 = "CC" + rowNumber;
					String indiceFin2 = "BA" + rowNumber;
					String strFormula3 = "SUM(" + indiceIni1 + ",(" + indiceFin2 + "*-1))";
					excelCell.setCellType(HSSFCell.CELL_TYPE_FORMULA);
					excelCell.setCellFormula(strFormula3);

					// excelCell.setCellValue(df.format((sumaVentaCargos.subtract(sumaCostoCargos))));
					excelCell.setCellStyle(style);

				} else {
					excelCell.setCellValue("");
				}

				//////////////////////////////
				// Bloque de tratamiento de cargos INICIO
				//////////////////////////////
//				BigDecimal sumaCostoCargosIguales = new BigDecimal(0);
//				BigDecimal sumaVentaCargosIguales = new BigDecimal(0);

				BigDecimal sumaCostoCargosIgualesMXN = new BigDecimal(0);
				BigDecimal sumaVentaCargosIgualesMXN = new BigDecimal(0);

				BigDecimal sumaCostoCargosIgualesUSD = new BigDecimal(0);
				BigDecimal sumaVentaCargosIgualesUSD = new BigDecimal(0);

				CargoPorServicio cs = null;

				for (Cargo c : cargosAll) {
					// Cargo c = cargosAll.get(f);
					// List<CargoPorServicio> cxs =
					// catalogoService.findByCargoId(item, c);
					// List<CargoPorServicio> cxs = item.getCargosPorServicio();
					boolean bandera = false;
					boolean convertirNegativo = false;
					List<CargoPorServicio> cxs = findCargoPorServicio(item, c);

//					sumaCostoCargosIguales = new BigDecimal(0);
//					sumaVentaCargosIguales = new BigDecimal(0);
					sumaCostoCargosIgualesMXN = new BigDecimal(0);
					sumaVentaCargosIgualesMXN = new BigDecimal(0);

					sumaCostoCargosIgualesUSD = new BigDecimal(0);
					sumaVentaCargosIgualesUSD = new BigDecimal(0);
					for (int h = 0; h < cxs.size(); h++) {
//					for (CargoPorServicio cs : cxs) {
						cs = cxs.get(h);
						//logger.info(h + " =========== CARGO [" + cs.getIdCargoServicio() + "] " + cs.getCargo().getDescCorta());

						if (cs.getMonedaCosto().getIdMoneda() == 5L) // PESOS MEXICANOS
							sumaCostoCargosIgualesMXN = sumaCostoCargosIgualesMXN
									.add(cs.getCosto() != null ? cs.getCosto() : new BigDecimal(0));
						if (cs.getMonedaCosto().getIdMoneda() == 1L) // DOLAR USD
							sumaCostoCargosIgualesUSD = sumaCostoCargosIgualesUSD
									.add(cs.getCosto() != null ? cs.getCosto() : new BigDecimal(0));
						if (cs.getMonedaVenta().getIdMoneda() == 5L) // PESOS MEXICANOS
							sumaVentaCargosIgualesMXN = sumaVentaCargosIgualesMXN
									.add(cs.getVenta() != null ? cs.getVenta() : new BigDecimal(0));
						if (cs.getMonedaVenta().getIdMoneda() == 1L) // DOLAR USD
							sumaVentaCargosIgualesUSD = sumaVentaCargosIgualesUSD
									.add(cs.getVenta() != null ? cs.getVenta() : new BigDecimal(0));

						if (cs.getCargo().getIdCargo() == 2l || cs.getCargo().getIdCargo() == 7l) {
							convertirNegativo = true;
						}

						bandera = true;
					}

					if (convertirNegativo) {

//						sumaCostoCargosIguales = sumaCostoCargosIguales.multiply(new BigDecimal(-1));
//						sumaVentaCargosIguales = sumaVentaCargosIguales.multiply(new BigDecimal(-1));
						sumaCostoCargosIgualesMXN = sumaCostoCargosIgualesMXN.multiply(new BigDecimal(-1));
						sumaVentaCargosIgualesMXN = sumaVentaCargosIgualesMXN.multiply(new BigDecimal(-1));

						sumaCostoCargosIgualesUSD = sumaCostoCargosIgualesUSD.multiply(new BigDecimal(-1));
						sumaVentaCargosIgualesUSD = sumaVentaCargosIgualesUSD.multiply(new BigDecimal(-1));
					}

					if (bandera) {
						escribeCargo(cs, excelCell, excelRow, sumaCostoCargosIgualesMXN, sumaVentaCargosIgualesMXN,
								sumaCostoCargosIgualesUSD, sumaVentaCargosIgualesUSD);
//						escribeCargo(cs, excelCell, excelRow, sumaCostoCargosIguales, sumaVentaCargosIguales);

					}
				}

				//////////////////////////////
				// Bloque de tratamiento de cargos FIN
				//////////////////////////////
				excelCell = excelRow.createCell(138);// Errores
				excelCell.setCellValue(errorMsgLog);
				excelCell.setCellStyle(style);
				
				excelCell = excelRow.createCell(139);// Fecha Cierre
				excelCell.setCellValue(item.getFechaCierre() != null
						? sdfMes.format(localDateToDate(item.getFechaCierre())).toUpperCase()
						: "");
				excelCell.setCellStyle(style);
			} // FOR Servicios

			//////////////////////////////////////////////////////////////////////////////////////

			TFin = System.currentTimeMillis();
			tiempo = TFin - TInicio; // Calculamos los milisegundos de diferencia
			logger.info("Tiempo de ejecución en milisegundos: " + tiempo);

			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmm");
			Calendar cal = Calendar.getInstance();
			File tempFile = new File("ReporteServiciosAdmin_" + dateFormat.format(cal.getTime()) + ".xls");

			outputStream = new FileOutputStream(tempFile);
			workBook.write(outputStream);
//			workBook.g
			outputStream.close();

			return tempFile;
//			Filedownload.save(tempFile, "application/file");

		} catch (Exception e) {
			logger.error("=================================" + e.getMessage(),e);
			logger.error("Error al exportar el reporte - Error: " + e.getStackTrace(), e);
			e.printStackTrace();
			logger.error("=================================" + e.getMessage(),e);
			return null;

		}
	}

	public File exportToExcelInter(List<ServicioInter> servicios) {

		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet workSheet = workBook.createSheet("Servicios");
		Row excelRow = null;
		Cell excelCell = null;
		int rowNumber = 0;
//		List<CargoAereo> listaAllCargosOrigen;
//		List<CargoAereo> listaAllCargosDestino;
		SimpleDateFormat sdfMes = new SimpleDateFormat("dd/MM/yy");
		// DecimalFormat df = new DecimalFormat("$###,###.00");
		try {

			excelRow = workSheet.createRow(rowNumber++);
			HSSFFont font = workBook.createFont();
			HSSFFont font2 = workBook.createFont();

			HSSFCellStyle style = workBook.createCellStyle();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
			style.setFont(font);

			HSSFCellStyle style2 = workBook.createCellStyle();
			font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			style2.setFont(font2);

			// HSSFCellStyle styleDecimal = workBook.createCellStyle();
			// styleDecimal.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));

			long TInicio, TFin, tiempo;
			TInicio = System.currentTimeMillis();

			int indexTitulos = 0;
			BigDecimal totalCostoUSD = new BigDecimal(0);
			BigDecimal totalVentaUSD = new BigDecimal(0);
			List<ReporteAM> titulosBasicos = reporteAMDao.findAll();
//			listaAllCargosOrigen = cargoAereoDao.findAll();
//			listaAllCargosDestino = cargoAereoDao.findAll();

			for (ReporteAM reporte : titulosBasicos) {

				excelCell = excelRow.createCell(reporte.getColumnaPosicion());
				excelCell.setCellValue(reporte.getColumnaTitulo());
				excelCell.setCellStyle(style2);
				indexTitulos++;

			}

			int indexColumns = 0;
			for (ServicioInter servicio : servicios) {
				// -------------------------
				totalCostoUSD = new BigDecimal(0);
				totalVentaUSD = new BigDecimal(0);
				indexColumns = 0;
				excelRow = workSheet.createRow(rowNumber++);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getCliente() != null ? servicio.getCliente().getIdCliente() + "" : "");
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getIdServicioStr() != null ? servicio.getIdServicioStr() : "");
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				/*
				 * 
				 * if (servicio.getIdServicioStr().trim().endsWith("AJ")) {
				 * excelCell.setCellValue("AE"); } else if
				 * (servicio.getTipo().equalsIgnoreCase("Marítimo")) {
				 * excelCell.setCellValue("ME"); } else { excelCell.setCellValue("AE"); }
				 */
//				String tipoServicio = servicio.getIdServicioStr().trim()
//						.substring(Math.max(servicio.getIdServicioStr().trim().length() - 2, 0));

				String tipoServicio = servicio.getIdServicioStr().length() > 2
						? servicio.getIdServicioStr().substring(servicio.getIdServicioStr().length() - 2)
						: servicio.getIdServicioStr();
				excelCell.setCellValue(tipoServicio);
//				excelCell.setCellValue(servicio.getIdServicioStr());
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getFechaServicio() != null
						? sdfMes.format(localDateToDate(servicio.getFechaServicio()))
						: "");
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getEstatus() != null ? servicio.getEstatus().getDescripcion() : "");
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getNoFacturaCliente() != null ? servicio.getNoFacturaCliente() : "");
				excelCell.setCellStyle(style);

				// fecha factura
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getFechaFactura() != null
						? sdfMes.format(localDateToDate(servicio.getFechaFactura())).toUpperCase()
						: "");
				excelCell.setCellStyle(style);

				// OP
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getTrafico() != null ? servicio.getNombreTrafico() : "");
				excelCell.setCellStyle(style);

				// Solicitado por
				if (servicio.getCliente() != null) {
//				Cliente tmpCli = clienteService.findCliente(servicio.getCliente().getIdCliente());
					Cliente tmpCli = clienteService.findById(servicio.getCliente().getIdCliente());

					if (tmpCli.getEjecutivos() != null && !tmpCli.getEjecutivos().isEmpty()) {
						String ejecutivos = "";

						for (int ind = 0; ind < tmpCli.getEjecutivos().size(); ind++) {
							ejecutivos += tmpCli.getEjecutivos().get(ind).getNombreCompleto() + " / ";
						}

						excelCell = excelRow.createCell(indexColumns++);
						excelCell.setCellValue(ejecutivos);
						excelCell.setCellStyle(style);
					}
				} else {
					excelCell = excelRow.createCell(indexColumns++);
					excelCell.setCellValue("");
					excelCell.setCellStyle(style);
				}

				// Pagador
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getCliente() != null ? servicio.getCliente().getNombre1() + "" : "");
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(
						servicio.getConsignatario() != null ? servicio.getConsignatario().getNombre() : "");
				excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(
						servicio.getAeropuertoOrigen() != null
								? "(" + servicio.getAeropuertoOrigen().getIniciales() + ") "
										+ servicio.getAeropuertoOrigen().getPais().getDescLargaPais()
								: "");
				excelCell.setCellStyle(style);

				// fecha ETD
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(
						servicio.getFechaSalida() != null ? sdfMes.format(localDateToDate(servicio.getFechaSalida()))
								: "");
				excelCell.setCellStyle(style);

				// destino
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(
						servicio.getAeropuertoDestino() != null
								? "(" + servicio.getAeropuertoDestino().getIniciales() + ") "
										+ servicio.getAeropuertoDestino().getPais().getDescLargaPais()
								: "");
				excelCell.setCellStyle(style);

				// fecha eta
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(
						servicio.getFechaArribo3() != null ? sdfMes.format(localDateToDate(servicio.getFechaArribo3()))
								: "");
				excelCell.setCellStyle(style);

				// HOUSE
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getGhNumGuia() != null ? servicio.getGhNumGuia() : "");
				excelCell.setCellStyle(style);

				// MASTERS
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getGmNumGuia() != null ? servicio.getGmNumGuia() : "");
				excelCell.setCellStyle(style);

				// REFERENCIA FACTURA
				excelCell = excelRow.createCell(indexColumns++);
				excelCell.setCellValue(servicio.getRefFacturacion() != null ? servicio.getRefFacturacion() : "");
				excelCell.setCellStyle(style);

				BigDecimal sumaCostoCargosIguales = new BigDecimal(0);
				BigDecimal sumaVentaCargosIguales = new BigDecimal(0);
				CargoOrigen cao = null;

				/***************************** CARGOS ORIGEN ***************************/
				for (CargoOrigen servicioCargo : servicio.getCargosOrigen()) {

					indexTitulos = 18;
					// RUTA
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue("ORIGEN");
					excelCell.setCellStyle(style);

					// CONCEPTO
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getCargoAereo() != null ? servicioCargo.getCargoAereo().getConcepto() : "");
					excelCell.setCellStyle(style);

					// PROVEEDOR
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getProveedor() != null ? servicioCargo.getProveedor().getRazonSocial() : "");
					excelCell.setCellStyle(style);

					// COSTO
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getCompraMonto() != null ? servicioCargo.getCompraMonto().doubleValue()
									: new Double(0));
					excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
					// excelCell.setCellStyle(styleDecimal);

					// MONEDA COSTO
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getMonedaCosto() != null ? servicioCargo.getMonedaCosto().getSimbolo() : "");
					excelCell.setCellStyle(style);

					// COSTO USD
					if (servicioCargo.getMonedaCosto() != null && servicioCargo.getCompraMonto() != null) {
						BigDecimal costoUSD = calculaTC(servicioCargo.getCompraMonto(), servicioCargo.getMonedaCosto(),
								localDateToDate(servicio.getFechaServicio()));
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(costoUSD.doubleValue());
						excelCell.setCellStyle(style2);
						// excelCell.setCellStyle(styleDecimal);
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
						totalCostoUSD = totalCostoUSD.add(costoUSD);
					} else {
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(new Double(0));
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
					}
					// VENTA
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getVentaMonto() != null ? servicioCargo.getVentaMonto().doubleValue()
									: new Double(0));
					excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);

					// MONEDA VENTA
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getMonedaVenta() != null ? servicioCargo.getMonedaVenta().getSimbolo() : "");
					excelCell.setCellStyle(style);

					// VENTA USD
					if (servicioCargo.getMonedaVenta() != null && servicioCargo.getVentaMonto() != null) {
						BigDecimal ventaUSD = calculaTC(servicioCargo.getVentaMonto(), servicioCargo.getMonedaVenta(),
								localDateToDate(servicio.getFechaServicio()));
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(ventaUSD.doubleValue());
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
						excelCell.setCellStyle(style2);
						totalVentaUSD = totalVentaUSD.add(ventaUSD);
					} else {
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(new Double(0));
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
					}

					// PROFIT
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(servicioCargo.getProfit() != null ? servicioCargo.getProfit().doubleValue()
							: new Double(0));
					excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);

					excelRow = workSheet.createRow(rowNumber++);
				}

				/***************************** CARGOS DESTINO ***************************/
				for (CargoDestino servicioCargo : servicio.getCargosDestino()) {

					indexTitulos = 18;
					// RUTA
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue("DESTINO");
					excelCell.setCellStyle(style);

					// CONCEPTO
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getCargoAereo() != null ? servicioCargo.getCargoAereo().getConcepto() : "");
					excelCell.setCellStyle(style);

					// PROVEEDOR
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getProveedor() != null ? servicioCargo.getProveedor().getRazonSocial() : "");
					excelCell.setCellStyle(style);

					// COSTO
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getCompraMonto() != null ? servicioCargo.getCompraMonto().doubleValue()
									: new Double(0));
					excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);

					// MONEDA COSTO
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getMonedaCosto() != null ? servicioCargo.getMonedaCosto().getSimbolo() : "");
					excelCell.setCellStyle(style);

					// COSTO USD
					if (servicioCargo.getMonedaCosto() != null && servicioCargo.getCompraMonto() != null) {
						BigDecimal costoUSD = calculaTC(servicioCargo.getCompraMonto(), servicioCargo.getMonedaCosto(),
								localDateToDate(servicio.getFechaServicio()));
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(costoUSD.doubleValue());
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
						excelCell.setCellStyle(style2);
						totalCostoUSD = totalCostoUSD.add(costoUSD);

					} else {
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(new Double(0));
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
					}

					// VENTA
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getVentaMonto() != null ? servicioCargo.getVentaMonto().doubleValue()
									: new Double(0));
					excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);

					// MONEDA VENTA
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(
							servicioCargo.getMonedaVenta() != null ? servicioCargo.getMonedaVenta().getSimbolo() : "");
					excelCell.setCellStyle(style);

					// VENTA USD
					if (servicioCargo.getMonedaVenta() != null && servicioCargo.getVentaMonto() != null) {
						BigDecimal ventaUSD = calculaTC(servicioCargo.getVentaMonto(), servicioCargo.getMonedaVenta(),
								localDateToDate(servicio.getFechaServicio()));
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(ventaUSD.doubleValue());
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
						excelCell.setCellStyle(style2);
						totalVentaUSD = totalVentaUSD.add(ventaUSD);
					} else {
						excelCell = excelRow.createCell(indexTitulos++);
						excelCell.setCellValue(new Double(0));
						excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
					}

					// PROFIT
					excelCell = excelRow.createCell(indexTitulos++);
					excelCell.setCellValue(servicioCargo.getProfit() != null ? servicioCargo.getProfit().doubleValue()
							: new Double(0));
					excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);

					excelRow = workSheet.createRow(rowNumber++);
				}

				/***************************** FOOTER DE TOTALES ***************************/
				excelCell = excelRow.createCell(22);
				excelCell.setCellValue("TOTAL COSTO USD");
				excelCell.setCellStyle(style2);

				excelCell = excelRow.createCell(23);
				excelCell.setCellValue(totalCostoUSD != null ? totalCostoUSD.doubleValue() : new Double(0));
				excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
				excelCell.setCellStyle(style2);

				excelCell = excelRow.createCell(25);
				excelCell.setCellValue("TOTAL VENTA USD");
				excelCell.setCellStyle(style2);

				excelCell = excelRow.createCell(26);
				excelCell.setCellValue(totalVentaUSD != null ? totalVentaUSD.doubleValue() : new Double(0));
				excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
				excelCell.setCellStyle(style2);

//			excelCell = excelRow.createCell(26);
//			excelCell.setCellValue("TOTAL PROFIT");
//			excelCell.setCellStyle(style);

				excelCell = excelRow.createCell(27);
				excelCell.setCellValue(
						servicio.getTotalprofit() != null ? servicio.getTotalprofit().doubleValue() : new Double(0));
				excelCell.setCellType(Cell.CELL_TYPE_NUMERIC);
				excelCell.setCellStyle(style2);

				excelRow = workSheet.createRow(rowNumber++);

			}

			TFin = System.currentTimeMillis();
			tiempo = TFin - TInicio; // Calculamos los milisegundos de diferencia
			System.out.println("Tiempo de ejecución en milisegundos: " + tiempo);

			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmm");
			Calendar cal = Calendar.getInstance();
			File tempFile = new File("ReporteServiciosAE_" + dateFormat.format(cal.getTime()) + ".xls");

			// File tempFile = new File("Servicios.xls");
			FileOutputStream outputStream = new FileOutputStream(tempFile);
			workBook.write(outputStream);
			outputStream.close();

			return tempFile;

		} catch (Exception e) {

			logger.error("Error al exportar el reporte Inter - Error: " + e.getMessage(), e);
			e.printStackTrace();
			return null;

		}

	}

	// -----------------------------------------------------------------
	public void escribeCargo(CargoPorServicio cargo, Cell excelCell, Row excelRow, BigDecimal sumaCostoMXN,
			BigDecimal sumaVentaMXN, BigDecimal sumaCostoUSD, BigDecimal sumaVentaUSD) {

		Map<String, BigDecimal> mapa = null;
//		log.info("\n ============== ESCRIBE CARGO [" + cargo.getItemID() + "] " + cargo.getCargo().getDescCorta());
//		log.info("sumaCostoMXN [" + sumaCostoMXN + "] sumaCostoUSD [" + sumaCostoUSD + "]");
//		log.info("sumaVentaMXN [" + sumaVentaMXN + "] sumaVentaUSD [" + sumaVentaUSD + "]");

		if (cargo.getCargo() != null && cargo.getMonedaCosto().getIdMoneda() != null) {
			// Imprime costo USD
			// DOLARIZO EL TOTAL DE MXN EN USD Y SE SUMA
			BigDecimal costoDolarizado = sumaCostoUSD
					.add(sumaCostoMXN.divide(new BigDecimal(tipoCambioServicio.getValor()), 2, RoundingMode.HALF_EVEN));

			excelCell = excelRow.createCell(Integer.parseInt(cargo.getCargo().getPosCtoDls()));
			excelCell.setCellValue(costoDolarizado != null ? costoDolarizado.doubleValue() : new Double(0));

			// Imprime costo MXN
			excelCell = excelRow.createCell(Integer.parseInt(cargo.getCargo().getPosCtoMxn()));
			excelCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
			excelCell.setCellValue(sumaCostoMXN != null ? sumaCostoMXN.doubleValue() : new Double(0));
			mapa = null;

		}

		if (cargo.getCargo() != null && cargo.getCargo().getIdCargo() != 2l) {
			if (cargo.getCargo() != null && cargo.getMonedaVenta().getIdMoneda() != null) {
				// IMPRIME VENTA USD
				BigDecimal ventaDolarizada = sumaVentaUSD.add(
						sumaVentaMXN.divide(new BigDecimal(tipoCambioServicio.getValor()), 2, RoundingMode.HALF_EVEN));
				excelCell = excelRow.createCell(Integer.parseInt(cargo.getCargo().getPosVtaDls()));
				excelCell.setCellValue(ventaDolarizada != null ? ventaDolarizada.doubleValue() : new Double(0));

				// IMPRIME VENTA MXN
				excelCell = excelRow.createCell(Integer.parseInt(cargo.getCargo().getPosVtaMxn()));
				excelCell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
				excelCell.setCellValue(sumaVentaMXN != null ? sumaVentaMXN.doubleValue() : new Double(0));

			}
		}
	}

	public int pintaTitulos(Cell excelCell, Row excelRow, String rubro, int contador, HSSFCellStyle style) {

		List<Cargo> cargosAll = cargoService.findAll();

		for (int i = 0; i < cargosAll.size(); i++) {
			cargoTitulo = cargosAll.get(i);
			excelCell = excelRow.createCell(contador);
			excelCell.setCellValue(cargoTitulo.getDescripcionCombinada() + rubro);// 1
			excelCell.setCellStyle(style);
			contador++;
		}

		return contador;
	}

	public Map<String, BigDecimal> validaCosto(ServicioPorCliente sxc) {
		Map<String, BigDecimal> mapa = null;
		mapa = new HashMap<String, BigDecimal>();

		if (sxc.getMonedaCosto().getIdMoneda() == 1) { // Dolares
			mapa.put("DOLAR", sxc.getCosto());
			mapa.put("PESOS", null);
		}

		if (sxc.getMonedaCosto().getIdMoneda() == 5) { // Pesos
			mapa.put("PESOS", sxc.getCosto());
			mapa.put("DOLAR",
					sxc.getCosto().divide(new BigDecimal(tipoCambioServicio.getValor()), 2, RoundingMode.HALF_EVEN));
//			mapa.put("DOLAR", sxc.getCosto());
		}

		return mapa;
	}

	public Map<String, BigDecimal> validaVenta(CargoPorServicio cargo, BigDecimal venta) {
		logger.info("===== VALIDA VENTA [" + cargo.getCargo().getIdCargo() + "] " + cargo.getCargo().getDescCorta()
				+ " [" + venta + "][" + cargo.getMonedaVenta().getSimbolo() + "]");
		Map<String, BigDecimal> mapa = null;
		mapa = new HashMap<String, BigDecimal>();

		if (cargo.getMonedaVenta().getIdMoneda() == 1) { // Dolares
			mapa.put("DOLAR", venta);
			mapa.put("PESOS", null);
		}

		if (cargo.getMonedaVenta().getIdMoneda() == 5) { // Pesos
			mapa.put("PESOS", venta);
			mapa.put("DOLAR", venta.divide(new BigDecimal(tipoCambioServicio.getValor()), 2, RoundingMode.HALF_EVEN));
		}

		return mapa;

	}

	public Map<String, BigDecimal> validaVenta(ServicioPorCliente sxc) {

		Map<String, BigDecimal> mapa = null;
		mapa = new HashMap<String, BigDecimal>();

		if (sxc.getMonedaVenta().getIdMoneda() == 1) { // Dolares
			mapa.put("DOLAR", sxc.getVenta());
			mapa.put("PESOS", null);
		}

		if (sxc.getMonedaVenta().getIdMoneda() == 5) { // Pesos
			mapa.put("PESOS", sxc.getVenta());
			mapa.put("DOLAR",
					sxc.getVenta().divide(new BigDecimal(tipoCambioServicio.getValor()), 2, RoundingMode.HALF_EVEN));
//			mapa.put("DOLAR", sxc.getVenta());
		}

		return mapa;

	}

	private Date localDateToDate(LocalDate localDate) {
		ZoneId defaultZoneId = ZoneId.systemDefault();
		return localDate != null ? Date.from(localDate.atStartOfDay(defaultZoneId).toInstant()) : null;
	}

	private List<CargoPorServicio> findCargoPorServicio(Servicio servicio, Cargo cargo) {
		List<CargoPorServicio> cargos = new ArrayList<CargoPorServicio>();
		for (CargoPorServicio servicioCargo : servicio.getCargosPorServicio()) {
			if (servicioCargo.getCargo().getIdCargo() == cargo.getIdCargo()) {

				cargos.add(servicioCargo);
			}
		}
		// if (cargos.size() == 0) {
		// cargos = null;
		// }

		return cargos;
	}

	private void getTipoCambioServicio(java.util.Date fecha,String idServicioStr) {
		//logger.info("Tipo de cambio Servicio ["+idServicioStr+"] Fecha +[" + fecha + "]");
		Moneda moneda = new Moneda();
		moneda.setIdMoneda(1L);// 2=Dólares
		Calendar calendario = GregorianCalendar.getInstance();
		calendario.setTimeInMillis(fecha.getTime());
//		logger.info("calendario.get(Calendar.MONTH) "+calendario.get(Calendar.MONTH));
		EnumMes mes = EnumMes.getValue(calendario.get(Calendar.MONTH) + 1);
		//logger.info("mes " + mes + " - mes.getIdMes() " + mes.getIdMes());
		int year = calendario.get(Calendar.YEAR);

		tipoCambioServicio = tipoCambioService.getByAnioMesMoneda(year, mes, moneda);
		logger.info("TC Servicio ["+idServicioStr+"] ["+year + "] [" + mes + "]  ===> [" + tipoCambioServicio.getValor()+"]");
	}

	private BigDecimal calculaTC(BigDecimal monto, Moneda moneda, Date fechaServicio) {
		//logger.info("calculaTC: monto[" + monto, "] moneda [" + moneda + "] fecha servicio+[" + fechaServicio + "]");
		BigDecimal ventaMontoDls = BigDecimal.ZERO;
		// EnumMes mesActual = null;

		Calendar calendario = GregorianCalendar.getInstance();
		calendario.setTimeInMillis(fechaServicio.getTime());
//		logger.info("calendario.get(Calendar.MONTH) "+calendario.get(Calendar.MONTH));
		EnumMes mesActual = EnumMes.getValue(calendario.get(Calendar.MONTH) + 1);
//		logger.info("mesActual "+mesActual+" - mesActual.getIdMes() "+mesActual.getIdMes());
		int anioActual = calendario.get(Calendar.YEAR);

		if (moneda.getIdMoneda() != 1l && moneda.getIdMoneda() != 5l) {
			TipoCambio tc = tipoCambioService.getByAnioMesMoneda(anioActual, mesActual, moneda);
			//logger.info(anioActual + " - " + mesActual + " - " + moneda + "==" + tc.getValor());
			ventaMontoDls = monto.multiply(new BigDecimal(tc.getValor()));

		} else if (moneda.getIdMoneda() == 5l) {
			Moneda monedaUSD = new Moneda();
			monedaUSD.setIdMoneda(1l);// 2=Dólares
			TipoCambio dltc = tipoCambioService.getByAnioMesMoneda(anioActual, mesActual, monedaUSD);
			//logger.info(anioActual + " - " + mesActual + " - " + monedaUSD + "==" + dltc.getValor());
			ventaMontoDls = monto.divide(new BigDecimal(dltc.getValor()), 2, RoundingMode.HALF_EVEN);
		} else if (moneda.getIdMoneda() == 1l) {
			ventaMontoDls = monto;
		}
		return ventaMontoDls.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public File exportProveedoresToExcel(List<Proveedor> proveedores) {
//		listaProveedoresReporte = proveedorService.findAll();		
		logger.info("Se genera reporte: " + proveedores.size());
		FileOutputStream outputStream = null;
		HSSFWorkbook workBook = new HSSFWorkbook();
		HSSFSheet workSheet = workBook.createSheet("Proveedores");
		Row excelRow = null;
		Cell excelCell = null;

		int rowNumber = 0;
		HSSFFont font = workBook.createFont();
		HSSFCellStyle style = workBook.createCellStyle();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);

		excelRow = workSheet.createRow(rowNumber);

		excelCell = excelRow.createCell(0);
		excelCell.setCellValue("ID");
		excelCell.setCellStyle(style);

		excelCell = excelRow.createCell(1);
		excelCell.setCellValue("NOMBRE COMERCIAL");
		excelCell.setCellStyle(style);

		excelCell = excelRow.createCell(2);
		excelCell.setCellValue("RAZÓN SOCIAL");
		excelCell.setCellStyle(style);

		excelCell = excelRow.createCell(3);
		excelCell.setCellValue("CONTACTO");
		excelCell.setCellStyle(style);

		excelCell = excelRow.createCell(4);
		excelCell.setCellValue("CORREO");
		excelCell.setCellStyle(style);

		excelCell = excelRow.createCell(5);
		excelCell.setCellValue("TELÉFONO OFICINA");
		excelCell.setCellStyle(style);

		excelCell = excelRow.createCell(6);
		excelCell.setCellValue("TELÉFONO CELULAR");
		excelCell.setCellStyle(style);
		/*
		 * 
		 * 
		 * excelCell = excelRow.createCell(7); excelCell.setCellValue("DIRECCION");
		 * excelCell.setCellStyle(style);
		 * 
		 * excelCell = excelRow.createCell(8); excelCell.setCellValue("BLOQUEADO");
		 * excelCell.setCellStyle(style);
		 */

		for (int i = 0; i < proveedores.size(); i++) {

			rowNumber++;
			Proveedor proveedor = proveedores.get(i);

			Evaluacion evaluacion = evaluacionService.findById(proveedor.getIdEvaluacion());

			excelRow = workSheet.createRow(rowNumber);

			excelCell = excelRow.createCell(0);
			excelCell.setCellValue(proveedor.getMotivoBloqueo() != null ? proveedor.getMotivoBloqueo() : "");

			excelCell = excelRow.createCell(1);
			excelCell.setCellValue(proveedor.getRazonComercial() != null ? proveedor.getRazonComercial() : "");

			excelCell = excelRow.createCell(2);
			excelCell.setCellValue(proveedor.getRazonSocial() != null ? proveedor.getRazonSocial() : "");

			// CONTACTO
			List<Contacto> contactos = evaluacion.getContactos();

			String nombreContacto = "";
			String correo = "";
			String telefono = "";
			String celular = "";
			for (Contacto contacto : contactos) {
				nombreContacto += (contacto.getNombreContacto() != null ? contacto.getNombreContacto() : "") + " / ";
				correo += (contacto.getCorreoElectronico() != null ? contacto.getCorreoElectronico() : "") + " / ";
				telefono += (contacto.getTelefonoOficina() != null ? contacto.getTelefonoOficina() : "") + " / ";
				celular += (contacto.getTelefonoCelular() != null ? contacto.getTelefonoCelular() : "") + " / ";
			}

			excelCell = excelRow.createCell(3);
			excelCell.setCellValue(nombreContacto);

			excelCell = excelRow.createCell(4);
			excelCell.setCellValue(correo);

			excelCell = excelRow.createCell(5);
			excelCell.setCellValue(telefono);

			excelCell = excelRow.createCell(6);
			excelCell.setCellValue(celular);
			/*
			 * 
			 * 
			 * excelCell = excelRow.createCell(7);
			 * excelCell.setCellValue(proveedor.getDireccion() != null ?
			 * proveedor.getDireccion().getDireccionCompleta(false) : "");
			 * 
			 * excelCell = excelRow.createCell(8);
			 * excelCell.setCellValue(proveedor.getBloqueado() ? "SI" : "NO");
			 */
		}

		try {
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmm");
			Calendar cal = Calendar.getInstance();
			File tempFile = new File("ReporteProveeduria_" + dateFormat.format(cal.getTime()) + ".xls");

			outputStream = new FileOutputStream(tempFile);
			workBook.write(outputStream);
			outputStream.close();

			return tempFile;

		} catch (Exception e) {
			System.out.println("Error generando reporte: " + e.getMessage());
			return null;
		}
	}
}
