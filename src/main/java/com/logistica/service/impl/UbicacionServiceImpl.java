package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IUbicacionDao;
import com.logistica.model.Ubicacion;
import com.logistica.service.IUbicacionService;

@Service
public class UbicacionServiceImpl implements IUbicacionService {

	@Autowired
	IUbicacionDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Ubicacion> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Ubicacion> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Ubicacion findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Ubicacion save(Ubicacion tipoRuta) {
		return dao.save(tipoRuta);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Ubicacion ubicacion = dao.findById(id).orElse(null);
		if (ubicacion != null) {
			dao.delete(ubicacion);
		}
	}

	@Override
	public List<Ubicacion> findFirst10ByCiudad(String ciudad) {
		return dao.findFirst10ByCiudadContainingIgnoreCase(ciudad);
	}

	@Override
	public List<Ubicacion> findFirst10ByCiudadAndPais(String ciudad, String pais) {
		return dao.findFirst10ByCiudadContainingIgnoreCaseAndPaisContainingIgnoreCase(ciudad, pais);
	}

}
