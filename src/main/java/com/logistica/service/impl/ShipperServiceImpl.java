package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IShipperDao;
import com.logistica.model.Shipper;
import com.logistica.service.IShipperService;

@Service
public class ShipperServiceImpl implements IShipperService {

	@Autowired
	IShipperDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Shipper> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Shipper> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Shipper findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Shipper save(Shipper shipper) {
		return dao.save(shipper);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Shipper shipper = dao.findById(id).orElse(null);
		if (shipper != null) {
			dao.delete(shipper);
		}
	}

}
