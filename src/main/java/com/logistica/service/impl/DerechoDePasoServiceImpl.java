package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IDerechoDePasoDao;
import com.logistica.model.cartaporte.DerechoDePaso;
import com.logistica.service.IDerechoDePasoService;

@Service
public class DerechoDePasoServiceImpl implements IDerechoDePasoService {

	@Autowired
	IDerechoDePasoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<DerechoDePaso> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DerechoDePaso> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public DerechoDePaso findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public DerechoDePaso save(DerechoDePaso derechoDePaso) {
		return dao.save(derechoDePaso);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		DerechoDePaso derechoDePaso = dao.findById(id).orElse(null);
		if (derechoDePaso != null) {
			dao.delete(derechoDePaso);
		}
	}

}
