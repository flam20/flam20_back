package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICantidadTransportadaDao;
import com.logistica.model.cartaporte.CantidadTransportada;
import com.logistica.service.ICantidadTransportadaService;

@Service
public class CantidadTransportadaServiceImpl implements ICantidadTransportadaService {

	@Autowired
	ICantidadTransportadaDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<CantidadTransportada> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CantidadTransportada> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public CantidadTransportada findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public CantidadTransportada save(CantidadTransportada cantidadTransportada) {
		return dao.save(cantidadTransportada);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CantidadTransportada cantidadTransportada = dao.findById(id).orElse(null);
		if (cantidadTransportada != null) {
			dao.delete(cantidadTransportada);
		}
	}

}
