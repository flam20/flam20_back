package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITransporteAereoDao;
import com.logistica.model.cartaporte.TransporteAereo;
import com.logistica.service.ITransporteAereoService;

@Service
public class TransporteAereoServiceImpl implements ITransporteAereoService {

	@Autowired
	ITransporteAereoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TransporteAereo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TransporteAereo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TransporteAereo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TransporteAereo save(TransporteAereo transporteAereo) {
		return dao.save(transporteAereo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TransporteAereo transporteAereo = dao.findById(id).orElse(null);
		if (transporteAereo != null) {
			dao.delete(transporteAereo);
		}
	}

}
