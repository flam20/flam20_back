package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IEstatusServicioDao;
import com.logistica.model.EstatusServicio;
import com.logistica.service.IEstatusServicioService;

@Service
public class EstatusServicioServiceImpl implements IEstatusServicioService {

	@Autowired
	IEstatusServicioDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<EstatusServicio> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EstatusServicio> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public EstatusServicio findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public EstatusServicio save(EstatusServicio estatusServicio) {
		return dao.save(estatusServicio);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		EstatusServicio estatusServicio = dao.findById(id).orElse(null);
		if (estatusServicio != null) {
			dao.delete(estatusServicio);
		}
	}

}
