package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IRemolqueDao;
import com.logistica.model.cartaporte.Remolque;
import com.logistica.service.IRemolqueService;

@Service
public class RemolqueServiceImpl implements IRemolqueService {

	@Autowired
	IRemolqueDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Remolque> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Remolque> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Remolque findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Remolque save(Remolque remolque) {
		return dao.save(remolque);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Remolque remolque = dao.findById(id).orElse(null);
		if (remolque != null) {
			dao.delete(remolque);
		}
	}

}
