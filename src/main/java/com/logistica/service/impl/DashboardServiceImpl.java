package com.logistica.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.logistica.model.Dashboard;
import com.logistica.model.DashboardBar;
import com.logistica.model.DashboardBarMonth;
import com.logistica.model.EstatusServicio;
import com.logistica.service.IDashboardService;

@Service
public class DashboardServiceImpl implements IDashboardService {
	static Logger logger = Logger.getLogger(DashboardServiceImpl.class.getName());

	@PersistenceContext
	private EntityManager em;

	public Dashboard getCountByUser(String id_teams) {
		Query query = null;
		Dashboard dashboard = new Dashboard();
		String countHql = "Select s.estatus,count(s.estatus) FROM Servicio s where id_usuario = :idUsuario group by s.estatus";
		query = em.createQuery(countHql);

		query.setParameter("idUsuario", id_teams);
		List<Object[]> list = query.getResultList();
		for (Object[] obj : list) {
			EstatusServicio estatus = (EstatusServicio) obj[0];
			Long count = (Long) obj[1];
			if (estatus.getIdEstatusServicio() == 1L)
				dashboard.setTransito(count.intValue());
			if (estatus.getIdEstatusServicio() == 3L)
				dashboard.setCerrado(count.intValue());
			if (estatus.getIdEstatusServicio() == 4L)
				dashboard.setCancelado(count.intValue());
		}

		return dashboard;

	}

	@Override
	public List<DashboardBar> getCountByUserByMonth(String id_teams) {
//		logger.info("COUNT BY MONTH [" + id_teams + "]");
		Query query = null;
//		DashboardBar dashboard = new DashboardBar();

		List<DashboardBar> lista = new ArrayList<DashboardBar>();
		List<DashboardBarMonth> mesesProv = new ArrayList<DashboardBarMonth>();
		lista.add(new DashboardBar("En Tránsito"));
		lista.add(new DashboardBar("Cerrado"));
		lista.add(new DashboardBar("Cancelado"));
		lista.get(0).setMeses(new ArrayList<DashboardBarMonth>());
		lista.get(1).setMeses(new ArrayList<DashboardBarMonth>());
		lista.get(2).setMeses(new ArrayList<DashboardBarMonth>());

		String countHql = "Select s.estatus, monthname(fecha_servicio) AS MES,count(s.estatus) FROM Servicio s "
				+ "where id_usuario = :idUsuario group by s.estatus,MES";
		query = em.createQuery(countHql);

		query.setParameter("idUsuario", id_teams);
		List<Object[]> list = query.getResultList();
//		logger.info("-->"+query.getResultList().get(0));
		DashboardBarMonth month = null;
		for (Object[] obj : list) {
			EstatusServicio estatus = (EstatusServicio) obj[0];
			String mes = obj[1].toString();
//			logger.info("---> " + estatus.getDescripcion() + " -> [" + mes + "] -> " + obj[2]);
			Long count = (Long) obj[2];
			if (estatus.getDescripcion().equalsIgnoreCase("En Tránsito")) {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(0).getMeses().add(month);
			} else if (estatus.getDescripcion().equalsIgnoreCase("Cerrado")) {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(1).getMeses().add(month);
			} else {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(2).getMeses().add(month);
			}

		}

		return lista;
	}

	public Dashboard getCountToAdmin() {
		Query query = null;
		Dashboard dashboard = new Dashboard();
		String countHql = "Select s.estatus,count(s.estatus) FROM Servicio s group by s.estatus";
		query = em.createQuery(countHql);

		List<Object[]> list = query.getResultList();
		for (Object[] obj : list) {
			EstatusServicio estatus = (EstatusServicio) obj[0];
			Long count = (Long) obj[1];
			if (estatus.getIdEstatusServicio() == 1L)
				dashboard.setTransito(count.intValue());
			if (estatus.getIdEstatusServicio() == 3L)
				dashboard.setCerrado(count.intValue());
			if (estatus.getIdEstatusServicio() == 4L)
				dashboard.setCancelado(count.intValue());
		}

		return dashboard;
	}

	public List<DashboardBar> getCountToAdminByMonth() {
		Query query = null;

		List<DashboardBar> lista = new ArrayList<DashboardBar>();
		List<DashboardBarMonth> mesesProv = new ArrayList<DashboardBarMonth>();
		lista.add(new DashboardBar("En Tránsito"));
		lista.add(new DashboardBar("Cerrado"));
		lista.add(new DashboardBar("Cancelado"));
		lista.get(0).setMeses(new ArrayList<DashboardBarMonth>());
		lista.get(1).setMeses(new ArrayList<DashboardBarMonth>());
		lista.get(2).setMeses(new ArrayList<DashboardBarMonth>());

		String countHql = "Select s.estatus, monthname(s.fechaServicio) AS MES,count(s.estatus) FROM Servicio s group by s.estatus,MES";
		query = em.createQuery(countHql);

		List<Object[]> list = query.getResultList();
		DashboardBarMonth month = null;
		for (Object[] obj : list) {
			EstatusServicio estatus = (EstatusServicio) obj[0];
			String mes = obj[1].toString();
			Long count = (Long) obj[2];
			if (estatus.getDescripcion().equalsIgnoreCase("En Tránsito")) {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(0).getMeses().add(month);
			} else if (estatus.getDescripcion().equalsIgnoreCase("Cerrado")) {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(1).getMeses().add(month);
			} else {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(2).getMeses().add(month);
			}

		}
		return lista;
	}

	public Dashboard getCountToVentas(String idEjecutivo) {
		Query query = null;
		Dashboard dashboard = new Dashboard();
		String countHql = "Select s.estatus,count(s.estatus),e.idTeams FROM Servicio s  JOIN  s.cliente c  JOIN  c.ejecutivos e "
				+ "where e.idTeams = :idTeams group by s.estatus";
		query = em.createQuery(countHql);

		query.setParameter("idTeams", idEjecutivo);
		List<Object[]> list = query.getResultList();
		for (Object[] obj : list) {
			EstatusServicio estatus = (EstatusServicio) obj[0];
			Long count = (Long) obj[1];
			if (estatus.getIdEstatusServicio() == 1L)
				dashboard.setTransito(count.intValue());
			if (estatus.getIdEstatusServicio() == 3L)
				dashboard.setCerrado(count.intValue());
			if (estatus.getIdEstatusServicio() == 4L)
				dashboard.setCancelado(count.intValue());
		}

		return dashboard;
	}

	@Override
	public List<DashboardBar> getCountToVentasByMonth(String IdTeams) {
		logger.info("COUNT BY MONTH [" + IdTeams + "]");
		Query query = null;
//		DashboardBar dashboard = new DashboardBar();

		List<DashboardBar> lista = new ArrayList<DashboardBar>();
		List<DashboardBarMonth> mesesProv = new ArrayList<DashboardBarMonth>();
		lista.add(new DashboardBar("En Tránsito"));
		lista.add(new DashboardBar("Cerrado"));
		lista.add(new DashboardBar("Cancelado"));
		lista.get(0).setMeses(new ArrayList<DashboardBarMonth>());
		lista.get(1).setMeses(new ArrayList<DashboardBarMonth>());
		lista.get(2).setMeses(new ArrayList<DashboardBarMonth>());

		String countHql = "Select s.estatus, monthname(s.fechaServicio) AS MES,count(s.estatus),e.idTeams "
				+ " FROM Servicio s  JOIN  s.cliente c  JOIN  c.ejecutivos e "
				+ "where e.idTeams = :idTeams group by s.estatus,MES";
		query = em.createQuery(countHql);

		query.setParameter("idTeams", IdTeams);
		List<Object[]> list = query.getResultList();
//		logger.info("-->"+query.getResultList().get(0));
		DashboardBarMonth month = null;
		for (Object[] obj : list) {
			EstatusServicio estatus = (EstatusServicio) obj[0];
			String mes = obj[1].toString();
//			logger.info("---> " + estatus.getDescripcion() + " -> [" + mes + "] -> " + obj[2]);
			Long count = (Long) obj[2];
			if (estatus.getDescripcion().equalsIgnoreCase("En Tránsito")) {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(0).getMeses().add(month);
			} else if (estatus.getDescripcion().equalsIgnoreCase("Cerrado")) {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(1).getMeses().add(month);
			} else {
				month = new DashboardBarMonth(mes, count.intValue());
				lista.get(2).getMeses().add(month);
			}

		}

		return lista;
	}

}
