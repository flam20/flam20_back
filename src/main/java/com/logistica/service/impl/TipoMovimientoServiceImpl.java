package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITipoMovimientoDao;
import com.logistica.model.TipoMovimiento;
import com.logistica.service.ITipoMovimientoService;

@Service
public class TipoMovimientoServiceImpl implements ITipoMovimientoService {

	@Autowired
	ITipoMovimientoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TipoMovimiento> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoMovimiento> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoMovimiento findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TipoMovimiento save(TipoMovimiento tipoMovimiento) {
		return dao.save(tipoMovimiento);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TipoMovimiento tipoMovimiento = dao.findById(id).orElse(null);
		if (tipoMovimiento != null) {
			dao.delete(tipoMovimiento);
		}
	}

}
