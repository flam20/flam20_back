package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IGuiaIdentificacionDao;
import com.logistica.model.cartaporte.GuiaIdentificacion;
import com.logistica.service.IGuiaIdentificacionService;

@Service
public class GuiaIdentificacionServiceImpl implements IGuiaIdentificacionService {

	@Autowired
	IGuiaIdentificacionDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<GuiaIdentificacion> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<GuiaIdentificacion> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public GuiaIdentificacion findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public GuiaIdentificacion save(GuiaIdentificacion guiaIdentificacion) {
		return dao.save(guiaIdentificacion);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		GuiaIdentificacion guiaIdentificacion = dao.findById(id).orElse(null);
		if (guiaIdentificacion != null) {
			dao.delete(guiaIdentificacion);
		}
	}

}
