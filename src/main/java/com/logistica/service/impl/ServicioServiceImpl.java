package com.logistica.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IServicioDao;
import com.logistica.model.EstatusServicio;
import com.logistica.model.Servicio;
import com.logistica.service.IServicioService;

@Service
public class ServicioServiceImpl implements IServicioService {

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	static Logger logger = Logger.getLogger(ServicioServiceImpl.class.getName());
	@Autowired
	IServicioDao dao;

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public Page<Servicio> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Servicio> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Servicio findById(Long id) {
		Servicio servicio = dao.findById(id).orElse(null);
		if (servicio != null && servicio.getServiciosPorCliente() != null
				&& !servicio.getServiciosPorCliente().isEmpty()) {
			Collections.sort(servicio.getServiciosPorCliente());
		}
		return servicio;
	}

	@Override
	@Transactional
	public Servicio save(Servicio servicio) {
		return dao.save(servicio);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Servicio servicio = dao.findById(id).orElse(null);
		if (servicio != null) {
			dao.delete(servicio);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Integer getConsecutiveByAnio(Integer anio) {
		return dao.findConsecutivoByAnio(anio);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Servicio> searchServicios(String numeroServicio, Long idEstatus, String idTrafico, Long idCliente,
			Long idProveedor, String numeroFactura, String fechaDesde, String fechaHasta, Boolean borradores) {
		//logger.info("searchServicios");
		String sSql = "SELECT DISTINCT s FROM Servicio s LEFT JOIN s.serviciosPorCliente ruta ";

		sSql += " WHERE ";

		if (borradores != null && !borradores.booleanValue()) {
			sSql += " s.idServicioStr NOT LIKE '%Borrador%' AND";
		}
		if (numeroServicio != null) {
			sSql += " (s.idServicioStr LIKE :numeroServicio OR ruta.proveedor.razonComercial LIKE :numeroServicio ) AND";
		}
		if (idEstatus != null) {
			sSql += " s.estatus.idEstatusServicio = :idEstatus AND";
		}
		if (idTrafico != null) {
			sSql += " s.trafico = :idTrafico AND";
		}
		if (idCliente != null) {
			sSql += " s.cliente.idCliente = :idCliente AND";
		}
		if (idProveedor != null) {
			sSql += " ruta.proveedor.idProveedor = :idProveedor AND";
		}
		if (numeroFactura != null) {
			sSql += " ruta.numFactura LIKE :numeroFactura AND";
		}
		if (fechaDesde != null) {
			sSql += " s.fechaServicio >= :fechaDesde AND";
		}
		if (fechaHasta != null) {
			sSql += " s.fechaServicio <= :fechaHasta";
		}

		if (sSql.endsWith("AND")) {
			sSql = sSql.substring(0, sSql.length() - 3);
		} else if (sSql.endsWith("WHERE ")) {
			sSql = sSql.substring(0, sSql.length() - 6);
		}

		Query query = em.createQuery(sSql);

		if (numeroServicio != null) {
			query.setParameter("numeroServicio", "%" + numeroServicio.replace("(amp)", "&") + "%");
		}
		if (idEstatus != null) {
			query.setParameter("idEstatus", idEstatus);
		}
		if (idTrafico != null) {
			query.setParameter("idTrafico", idTrafico);
		}
		if (idCliente != null) {
			query.setParameter("idCliente", idCliente);
		}
		if (idProveedor != null) {
			query.setParameter("idProveedor", idProveedor);
		}
		if (numeroFactura != null) {
			query.setParameter("numeroFactura", "%" + numeroFactura + "%");
		}
		if (fechaDesde != null) {
			query.setParameter("fechaDesde", LocalDate.parse(fechaDesde, formatter));
		}
		if (fechaHasta != null) {
			query.setParameter("fechaHasta", LocalDate.parse(fechaHasta, formatter));
		}

		return query.getResultList();
	}

	public List<Servicio> searchServiciosVentas(String numeroServicio, Long idEstatus, String desde, String hasta,
			Long idCliente, String idTeams, Long idProveedor, String idTrafico) {
		logger.info("searchServiciosVentas");
		Query query = null;

		String queryStringFull = "SELECT s FROM Servicio s INNER JOIN s.cliente c INNER JOIN c.ejecutivos e LEFT JOIN s.serviciosPorCliente ruta ";
		// String queryStringFull = "SELECT s FROM Servicio s LEFT JOIN
		// s.serviciosPorCliente ruta ";

		if (numeroServicio == null && idEstatus == null && desde == null && hasta == null && idCliente == null
				&& idTrafico == null && idProveedor == null && idTeams == null) {
			logger.info("TODOS SON NULOS, SIN WHERE ");

		} else {
			// log.info("AL MENOS UN PARAMETRO NO ES NULO");
			queryStringFull = queryStringFull.concat(" WHERE ");
		}

		if (numeroServicio != null) {
			queryStringFull = queryStringFull.concat(" s.idServicioStr LIKE :numeroServicio ");
		}

		if (idEstatus != null) {
			if (queryStringFull.contains(":"))
				queryStringFull = queryStringFull.concat(" and s.estatus.idEstatusServicio = :estatus ");
			else
				queryStringFull = queryStringFull.concat(" s.estatus.idEstatusServicio = :estatus ");
		}

		if (idCliente != null) {
			if (queryStringFull.contains(":"))
				queryStringFull = queryStringFull.concat(" and s.cliente.idCliente = :cliente ");
			else
				queryStringFull = queryStringFull.concat(" s.cliente.idCliente = :cliente ");

//			logger.info("cliente: " +idCliente);
		}

		if (idProveedor != null) {
			if (queryStringFull.contains(":"))
				queryStringFull = queryStringFull.concat(" and ruta.proveedor.idProveedor = :idProveedor ");
			else
				queryStringFull = queryStringFull.concat(" ruta.proveedor.idProveedor = :idProveedor ");

//			logger.info("cliente: " +idCliente);
		}

		if (idTrafico != null) {
			if (queryStringFull.contains(":"))
				queryStringFull = queryStringFull.concat(" and s.trafico = :idTrafico ");
			else
				queryStringFull = queryStringFull.concat(" s.trafico = :idTrafico ");

//			logger.info("cliente: " +idCliente);
		}

		if (desde != null && hasta != null) {
			if (queryStringFull.contains(":"))
				queryStringFull = queryStringFull.concat(" and s.fechaServicio BETWEEN :desde AND :hasta ");
			else
				queryStringFull = queryStringFull.concat(" s.fechaServicio BETWEEN :desde AND :hasta ");

//			logger.info("fechaInicio: " + desde + "- fechaFin: " + hasta);
		}

		if (idTeams != null) {
			if (queryStringFull.contains(":"))
				// queryStringFull = queryStringFull.concat(" and s.trafico = :idTeams ");
				queryStringFull = queryStringFull.concat(" and e.idTeams = :idTeams ");
			else
				// queryStringFull = queryStringFull.concat(" s.trafico = :idTeams ");
				queryStringFull = queryStringFull.concat(" e.idTeams = :idTeams ");
		}

		queryStringFull += " group by s.idServicio ";

		logger.info("SQL " + queryStringFull);
		// em.clear();
		query = em.createQuery(queryStringFull);

		if (numeroServicio != null)
			query.setParameter("numeroServicio", "%" + numeroServicio + "%");

		if (idEstatus != null) {
//			EstatusServicio estatusServ = new EstatusServicio();
//			estatusServ.setIdEstatusServicio(idEstatus);
			query.setParameter("estatus", idEstatus);
		}
		if (desde != null && hasta != null) {
			query.setParameter("desde", LocalDate.parse(desde, formatter));
			query.setParameter("hasta", LocalDate.parse(hasta, formatter));
		}

		if (idCliente != null) {
			query.setParameter("cliente", idCliente);
		}

		if (idProveedor != null) {
			query.setParameter("idProveedor", idProveedor);
		}

		if (idTrafico != null) {
			query.setParameter("idTrafico", idTrafico);
		}

		if (idTeams != null) {
			query.setParameter("idTeams", idTeams);
		}

//		logger.info("Consulta: " + queryStringFull);

		List<Servicio> servicios = query.getResultList();
//		logger.info("1");
//		for (Servicio servicio : servicios) {
//			servicio.getServiciosPorCliente().size();
//			servicio.getCargosPorServicio().size();
//		}
//		logger.info("2");
		return servicios;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Servicio> findTraficos() {
		List<Object> result = dao.findTraficos();
		List<Servicio> servicios = new ArrayList<>();
		for (Object cdata : result) {
			Servicio s = new Servicio();
			Object[] obj = (Object[]) cdata;
			s.setTrafico((String) obj[0]);
			s.setNombreTrafico((String) obj[1]);
			servicios.add(s);
		}
		return servicios;
	}

	@Override
	public void updateChatID(String idChat, Long idServicio) {
		dao.updateChatID(idChat, idServicio);
	}

}
