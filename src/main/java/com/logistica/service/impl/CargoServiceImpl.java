package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICargoDao;
import com.logistica.model.Cargo;
import com.logistica.service.ICargoService;

@Service
public class CargoServiceImpl implements ICargoService {

	@Autowired
	ICargoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Cargo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cargo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Cargo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Cargo save(Cargo cargo) {
		return dao.save(cargo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Cargo cargo = dao.findById(id).orElse(null);
		if (cargo != null) {
			dao.delete(cargo);
		}
	}

}
