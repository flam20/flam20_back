package com.logistica.service.impl;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxConfig;
import com.box.sdk.BoxDeveloperEditionAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxSearch;
import com.box.sdk.BoxSearchParameters;
import com.box.sdk.BoxSharedLink;
import com.box.sdk.IAccessTokenCache;
import com.box.sdk.InMemoryLRUAccessTokenCache;
import com.box.sdk.PartialCollection;
import com.logistica.dao.IArchivoBoxDao;
import com.logistica.model.ArchivoBox;
import com.logistica.model.Factura;
import com.logistica.service.IBoxService;

@Service
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class BoxServiceImpl implements IBoxService {

	static Logger logger = Logger.getLogger(BoxServiceImpl.class.getName());

	private static final int MAX_CACHE_ENTRIES = 100;
	private static final String USER_ID = "1418268451";

	@Autowired
	IArchivoBoxDao dao;

	@Override
	public List<ArchivoBox> getArchivoBoxByServicio(Long idServicio) {
		return dao.findByIdServicio(idServicio);
	}

	@Override
	public void deleteArchivoBoxByServicio(Long idServicio) {
		List<ArchivoBox> listArchivosBox = getArchivoBoxByServicio(idServicio);
		if (listArchivosBox != null && !listArchivosBox.isEmpty()) {
			for (ArchivoBox archivoBox : listArchivosBox) {
				archivoBox.setIdServicio(null);
			}

			dao.deleteAll(listArchivosBox);
		}
	}

	@Override
	public List<ArchivoBox> consultaPODs(Long idServicio) {
		BoxDeveloperEditionAPIConnection api = obtieneConexionBox();
		BoxSearch bs = new BoxSearch(api);
		BoxSearchParameters bsp = new BoxSearchParameters();

		List<String> fileExtensions = new ArrayList<String>();
		fileExtensions.add("pdf");
		fileExtensions.add("png");
		fileExtensions.add("jpg");

		List<String> contentTypes = new ArrayList<String>();
		contentTypes.add("name");

		bsp.setFileExtensions(fileExtensions);
		bsp.setContentTypes(contentTypes);
		bsp.setQuery(idServicio + "_POD_V2");

		return ejecutaBusquedaEnBox(idServicio, bsp, bs, api);
	}

	@Override
	public ArchivoBox uploadArchivoPod(InputStream is, String extension) {
//		logger.info("Extension "+extension);
		ArchivoBox archivoBox = null;
		BoxDeveloperEditionAPIConnection api = obtieneConexionBox();
		try {
			DateTimeFormatter timeStampPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

			String nombre = "POD_V2_" + timeStampPattern.format(java.time.LocalDateTime.now()) + "." + extension;

			String destinationFolderID = "106422521819";
			BoxFolder destinationFolder = new BoxFolder(api, destinationFolderID);
			if (destinationFolder != null) {
				BoxFile.Info newFileInfo = destinationFolder.uploadFile(is, nombre);

				BoxSharedLink slFile = createSharedLink(api, newFileInfo.getID());
				logger.info("SE SUBIO EL ARCHIVO A BOX: " + nombre);

				archivoBox = new ArchivoBox(null, nombre, slFile.getURL(), slFile.getDownloadURL());
				dao.save(archivoBox);
			}
		} catch (Exception e) {
			logger.error("Error al subir archivo:" + e);
		}
		return archivoBox;
	}

	@Override
	public ArchivoBox uploadArchivoPod(Long idServicio, InputStream is,String extension,String idServicioStr) {
		logger.info("Extension "+extension);
		ArchivoBox archivoBox = null;
		BoxDeveloperEditionAPIConnection api = obtieneConexionBox();
		try {
			DateTimeFormatter timeStampPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

			String nombre = idServicioStr.replace("-", "_") + "_POD_V2_" + timeStampPattern.format(LocalDateTime.now()) + "."+extension;

			String destinationFolderID = "106422521819";
			BoxFolder destinationFolder = new BoxFolder(api, destinationFolderID);
			BoxFile.Info newFileInfo = destinationFolder.uploadFile(is, nombre);

			BoxSharedLink slFile = createSharedLink(api, newFileInfo.getID());
			logger.info("SE SUBIO EL ARCHIVO A BOX: " + nombre);

			archivoBox = new ArchivoBox(idServicio, nombre, slFile.getURL(), slFile.getDownloadURL());
			dao.save(archivoBox);
		} catch (Exception e) {
			logger.error("Error al subir archivo: " + e.getMessage(), e);
		}
		return archivoBox;
	}

	@Override
	public List<Factura> getFacturasByProveedor(Long idProveedor) {
		// TODO Auto-generated method stub
		return null;
	}

	private BoxDeveloperEditionAPIConnection obtieneConexionBox() {
		IAccessTokenCache accessTokenCache = new InMemoryLRUAccessTokenCache(MAX_CACHE_ENTRIES);
		BoxDeveloperEditionAPIConnection api = null;

		try {
			File fileJson = new File("c:\\config.json");
			Reader reader = new FileReader(fileJson);

			// Reader reader = new FileReader("src/main/java/config.json");
			BoxConfig boxConfig = BoxConfig.readFrom(reader);
			api = BoxDeveloperEditionAPIConnection.getUserConnection(USER_ID, boxConfig, accessTokenCache);
			logger.info("CONEXION A BOX EXITOSA");
		} catch (Exception e) {
			logger.error("Error al obtener conexión a BOX:" + e.getMessage(), e);
		}

		return api;
	}

	private static BoxSharedLink createSharedLink(BoxAPIConnection api, String fileId) {
		BoxFile file = new BoxFile(api, fileId);
		BoxSharedLink.Permissions permissions = new BoxSharedLink.Permissions();
		permissions.setCanDownload(true);
		permissions.setCanPreview(true);
		Date date = new Date();

		Calendar unshareAt = Calendar.getInstance();
		unshareAt.setTime(date);
		unshareAt.add(Calendar.DATE, 30);

		BoxSharedLink sharedLink = file.createSharedLink(BoxSharedLink.Access.OPEN, unshareAt.getTime(), permissions);
		return sharedLink;
	}

	private List<ArchivoBox> ejecutaBusquedaEnBox(Long idServicio, BoxSearchParameters bsp, BoxSearch bs,
			BoxAPIConnection api) {
		logger.info("Buscando archivos para " + idServicio);
		PartialCollection<BoxItem.Info> searchResults;
		List<ArchivoBox> archivosEncontrados = new ArrayList<ArchivoBox>();
		ArchivoBox archivo = null;
		long offset = 0;
		long limit = 1000;
		long fullSizeOfResult = 0;

		try {
			while (offset <= fullSizeOfResult) {
				searchResults = bs.searchRange(offset, limit, bsp);
				fullSizeOfResult = searchResults.fullSize();

				// SE BORRAN TODOS LOS ARCHIVOS PARA EVITAR DUPLICADOS
				deleteArchivoBoxByServicio(idServicio);

				for (BoxItem.Info itemInfo : searchResults) {
					BoxSharedLink slFile = createSharedLink(api, itemInfo.getID());
					archivo = new ArchivoBox(idServicio, itemInfo.getName(), slFile.getURL(), slFile.getDownloadURL());
					dao.save(archivo);
					archivosEncontrados.add(archivo);
					logger.info("Archivo encontrado: " + archivo.toString());
				}

				offset += limit;
			}
		} catch (Exception e) {
			logger.error("Error al buscar en box: " + e, e);
		}

		return archivosEncontrados;
	}

}
