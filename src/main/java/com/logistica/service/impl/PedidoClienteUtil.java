package com.logistica.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logistica.dao.IBitacoraSapDao;
import com.logistica.model.BitacoraSap;
import com.logistica.model.PedidoClienteItemRequest;
import com.logistica.model.PedidoClienteRequest;
import com.logistica.sap.pedidoCliente.BusinessTransactionDocumentID;
import com.logistica.sap.pedidoCliente.EXTENDEDName;
import com.logistica.sap.pedidoCliente.FulfilmentPartyCategoryCode;
import com.logistica.sap.pedidoCliente.LOCALNORMALISEDDateTime2;
import com.logistica.sap.pedidoCliente.Log;
import com.logistica.sap.pedidoCliente.LogItem;
import com.logistica.sap.pedidoCliente.PartyID;
import com.logistica.sap.pedidoCliente.ProductInternalID;
import com.logistica.sap.pedidoCliente.Rate;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainConfirmationBundle;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainConfirmationBundleMessageSync;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequest;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestBundleMessageSync;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestDeliveryTerms;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestItem;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestItemProduct;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestItemScheduleLine;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestItemServiceTerms;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestPartyIDParty;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestPartyParty;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestPriceAndTaxCalculationItem;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestPricingTerms;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestTextCollection;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestTextCollectionText;
import com.logistica.sap.pedidoCliente.TextCollectionTextTypeCode;
import com.logistica.util.ControlAccesoUtil;

@Component
public class PedidoClienteUtil {
	static Logger logger = Logger.getLogger(PedidoClienteUtil.class.getName());

	private static final String cFullfillmentCategoryCode = "2";
	private static final String cTypeCodeObservacionesItem = "10024";// OBSERVACIONES
	private static final String cTypeCodeComentariosGenerales = "10011";// COMENTARIOS
																		// GENERALES
	private static final String cTypeCodeReferenciaFactura = "10024";// REF
																		// FACTURA
	private static final String cItemServicePlannedDuration = "P1D";
	private static final String cItemScheduleLineId = "1";
	private static final String cItemScheduleLineTypeCode = "1";
	private static final int cItemScheduleLineQuantity = 1;

	@Autowired
	IBitacoraSapDao bitacoraSAPDao;

	public SalesOrderMaintainRequestBundleMessageSync getSalesOrderRequest(String idServicio, PedidoClienteRequest r,
			List<PedidoClienteItemRequest> items) {
		logger.info("[" + idServicio + "] ============ CREA REQUEST PEDIDO CLIENTE");
		String FORMATER = "yyyy-MM-dd'T'HH:mm:ss'Z'";
		DateFormat format = new SimpleDateFormat(FORMATER);
		SalesOrderMaintainRequestBundleMessageSync soRequest = new SalesOrderMaintainRequestBundleMessageSync();
		SalesOrderMaintainRequestItem item = null;
		try {
			SalesOrderMaintainRequest request = new SalesOrderMaintainRequest();

			request.setActionCode(r.getActionCode());

			if (r.getActionCode().equals("04")) {
				logger.info("[" + idServicio + "] ES ACTUALIZACION");

				BusinessTransactionDocumentID btId = new BusinessTransactionDocumentID();
				btId.setValue(r.getPedidoCliente());
				request.setID(btId);
			}

			BusinessTransactionDocumentID buyerId = new BusinessTransactionDocumentID();
			buyerId.setValue(r.getBuyerId());
			request.setBuyerID(buyerId);

			EXTENDEDName name = new EXTENDEDName();
			name.setValue(r.getRequestName());
			request.setName(name);

			Date dt2 = new Date();
			Calendar c2 = Calendar.getInstance();
			c2.setTime(dt2);
			XMLGregorianCalendar pd = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(format.format(c2.getTime()));

//			XMLGregorianCalendar postingDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			request.setPostingDate(pd);

			SalesOrderMaintainRequestPartyParty party = new SalesOrderMaintainRequestPartyParty();
			PartyID partyId = new PartyID();
			partyId.setValue(r.getBillTo());
			party.setPartyID(partyId);
			request.setBillToParty(party);

			SalesOrderMaintainRequestPartyParty accountParty = new SalesOrderMaintainRequestPartyParty();
			PartyID accountPartyId = new PartyID();
			accountPartyId.setValue(r.getAccountId());
			accountParty.setPartyID(accountPartyId);
			request.setAccountParty(accountParty);

			SalesOrderMaintainRequestPartyIDParty employee = new SalesOrderMaintainRequestPartyIDParty();
			PartyID employeeId = new PartyID();
			employeeId.setValue(r.getEmployeeId());
			employee.setPartyID(employeeId);
			request.setEmployeeResponsibleParty(employee);

			SalesOrderMaintainRequestPartyIDParty salesUnitParty = new SalesOrderMaintainRequestPartyIDParty();
			PartyID unitPartyId = new PartyID();
			unitPartyId.setValue(r.getSalesUnit());
			salesUnitParty.setPartyID(unitPartyId);
			request.setSalesUnitParty(salesUnitParty);

			SalesOrderMaintainRequestPricingTerms pricingTerms = new SalesOrderMaintainRequestPricingTerms();
			pricingTerms.setCurrencyCode(r.getPricingTermsCurrency());
			request.setPricingTerms(pricingTerms);

			SalesOrderMaintainRequestDeliveryTerms deliveryTerms = new SalesOrderMaintainRequestDeliveryTerms();
			deliveryTerms.setDeliveryPriorityCode(r.getDeliveryTermsPriority());
			request.setDeliveryTerms(deliveryTerms);

			SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms periodTerms = new SalesOrderMaintainRequestRequestedFulfillmentPeriodPeriodTerms();
			LOCALNORMALISEDDateTime2 normalisedDateTime = new LOCALNORMALISEDDateTime2();

			Date dt = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(dt);
			// c.add(Calendar.DATE, 1);
			XMLGregorianCalendar postingDate2 = DatatypeFactory.newInstance()
					.newXMLGregorianCalendar(format.format(c.getTime()));

			normalisedDateTime.setValue(postingDate2);
			periodTerms.setStartDateTime(normalisedDateTime);
			request.setRequestedFulfillmentPeriodPeriodTerms(periodTerms);

			/********* ITEMS ******/
			for (PedidoClienteItemRequest it : items) {
//				logger.info("----> " + it.getItemID() + " - " + it.getItemCurrency() + " - " + it.getItemVendorId());

				item = getItemRequest(r, it.getItemProductId(), it.getItemPrice(), it.getItemCurrency(),
						it.getItemVendorId(), it.getItemObservaciones(), it.getItemID());
				request.getItem().add(item);
			}
			/******* FIN ITEM ******/

			/**** TEXT COLLECTION ******/
			if ((r.getNotasGenerales() != null && r.getNotasGenerales().length() > 0)
					|| (r.getReferenciaFactura() != null && r.getReferenciaFactura().length() > 0)) {
				SalesOrderMaintainRequestTextCollection textCollection = new SalesOrderMaintainRequestTextCollection();

				/* NOTAS DE FLAM */
				if (r.getNotasGenerales() != null && r.getNotasGenerales().length() > 0) {
					SalesOrderMaintainRequestTextCollectionText notasGeneralesText = new SalesOrderMaintainRequestTextCollectionText();
					notasGeneralesText.setActionCode(r.getActionCode());
					TextCollectionTextTypeCode typeCode = new TextCollectionTextTypeCode();
					typeCode.setValue(cTypeCodeComentariosGenerales);
					notasGeneralesText.setTypeCode(typeCode);
					notasGeneralesText.setContentText(r.getNotasGenerales());
					textCollection.getText().add(notasGeneralesText);
				}
				if (r.getReferenciaFactura() != null && r.getReferenciaFactura().length() > 0) {
					/* REFERENCIA PARA FACTURAR */
					SalesOrderMaintainRequestTextCollectionText refFacturaText = new SalesOrderMaintainRequestTextCollectionText();
					refFacturaText.setActionCode(r.getActionCode());
					TextCollectionTextTypeCode typeCodeRef = new TextCollectionTextTypeCode();
					typeCodeRef.setValue(cTypeCodeReferenciaFactura);
					refFacturaText.setTypeCode(typeCodeRef);
					refFacturaText.setContentText(r.getReferenciaFactura());
					textCollection.getText().add(refFacturaText);
				}
				request.setTextCollection(textCollection);
			}

			/**** FIN TEXT COLLECTION ******/

//			request.setConsignatarios(r.getConsignatarioOrigen());
//			request.setConsignatarioCruce(r.getConsignatarioCruce());
//			request.setConsignatarioDestino(r.getConsignatarioDestino());
//
//			request.setUbicacinDestino(r.getUbicacionDestino());
//			request.setUbicacionOrigen(r.getUbicacionOrigen());
//			request.setUbicacinCruce(r.getUbicacionCruce());
//
//			request.setFechaCarga1(formatDate(r.getFechaCarga()));
//			request.setFechaCruce1(formatDate(r.getFechaCruce()));
//			request.setFechaDestino1(formatDate(r.getFechaDestino()));
//			request.setFechadeAgenteAduanal1(formatDate(r.getFechaAgente()));
//
//			request.setGuaHouse(r.getGuiaHouse());
//			request.setGuaMaster(r.getGuíaMaster());
//			request.setNmerodeequipodecarga(r.getNumeroEquipoCarga());
//			request.setPzsPBPC(r.getPzasPBPC());
//			request.setShipper(r.getShipper());
//			request.setTipodeequipo(r.getTipoEquipo());
//			request.setRequierePOD(r.getRequierePOD());

			request.setServicioFLAM(r.getNoServicio());
//			request.setNdeservicio1(r.getNoServicio());

			soRequest.getSalesOrder().add(request);
		} catch (Exception e) {
			logger.error("[" + idServicio + "] Error al armar request PedidoCliente:" + e.getMessage(), e);
		}

		return soRequest;
	}

	public String leePedidoCliente(Long idServicio, String idServicioStr, String currency,
			SalesOrderMaintainConfirmationBundleMessageSync r) {
		String pedidoCliente = null;
		String salida = "";
		logger.info("[" + idServicioStr + "] Pedido de Cliente");
		try {
			if (r.getSalesOrder() != null && r.getSalesOrder().size() > 0) {
				for (SalesOrderMaintainConfirmationBundle soResult : r.getSalesOrder()) {
					BusinessTransactionDocumentID id = soResult.getID();

					pedidoCliente = id.getValue();
					salida = " - Pedido a cliente [" + currency + "] - [" + pedidoCliente + "]";
				}
			} else {
				salida = " - No se generó Pedido a Cliente [" + currency + "]";
			}
			logger.info("[" + idServicioStr + "] - " + salida);
			Log loge = r.getLog();

			String notas = "";
			for (LogItem logItem : loge.getItem()) {
				logger.info(
						"[" + idServicioStr + "] \t Nota -> [" + logItem.getSeverityCode() + "] " + logItem.getNote());
				notas += "- [" + logItem.getSeverityCode() + "] " + logItem.getNote() + "\n";
			}

			BitacoraSap t = new BitacoraSap(idServicio, "PEDIDO CLIENTE [" + currency + "]", notas, pedidoCliente);
			t.setControlAcceso(ControlAccesoUtil.generarControlAccesoCreacion());
			bitacoraSAPDao.save(t);
		} catch (Exception e) {
			logger.error("[" + idServicioStr + "] Error al leer la respuesta de SAP para pedido de cliente:"
					+ e.getMessage(), e);
		}

		return pedidoCliente;
	}

	private SalesOrderMaintainRequestItem getItemRequest(PedidoClienteRequest r, String pItemProductInternalId,
			BigDecimal pItemPriceValue, String pItemCurrency, String pItemVendorId, String observaciones,
			String itemID) {
		SalesOrderMaintainRequestItem item = new SalesOrderMaintainRequestItem();
		item.setActionCode(r.getActionCode());
		try {

			item.setID(itemID);
			FulfilmentPartyCategoryCode fullCategoryCode = new FulfilmentPartyCategoryCode();
			fullCategoryCode.setValue(cFullfillmentCategoryCode);
			item.setFulfilmentPartyCategoryCode(fullCategoryCode);

			SalesOrderMaintainRequestItemProduct itemProduct = new SalesOrderMaintainRequestItemProduct();
			ProductInternalID productInternalId = new ProductInternalID();
			productInternalId.setValue(pItemProductInternalId);
			itemProduct.setProductInternalID(productInternalId);
			item.setItemProduct(itemProduct);

			SalesOrderMaintainRequestItemServiceTerms serviceTerms = new SalesOrderMaintainRequestItemServiceTerms();
			Duration plannedDuration = DatatypeFactory.newInstance().newDuration(cItemServicePlannedDuration);
			serviceTerms.setServicePlannedDuration(plannedDuration);
			item.setItemServiceTerms(serviceTerms);

			SalesOrderMaintainRequestItemScheduleLine scheduleLine = new SalesOrderMaintainRequestItemScheduleLine();
			scheduleLine.setID(cItemScheduleLineId);
			scheduleLine.setTypeCode(cItemScheduleLineTypeCode);
			com.logistica.sap.pedidoCliente.Quantity quantity = new com.logistica.sap.pedidoCliente.Quantity();
			quantity.setValue(new BigDecimal(cItemScheduleLineQuantity));
			scheduleLine.setQuantity(quantity);
			item.getItemScheduleLine().add(scheduleLine);

			SalesOrderMaintainRequestPriceAndTaxCalculationItem priceTaxCalculation = new SalesOrderMaintainRequestPriceAndTaxCalculationItem();
			SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice itemMainPrice = new SalesOrderMaintainRequestPriceAndTaxCalculationItemItemMainPrice();
			Rate itemRate = new Rate();
			itemRate.setDecimalValue(pItemPriceValue);
			itemRate.setCurrencyCode(pItemCurrency);
			itemMainPrice.setRate(itemRate);
			priceTaxCalculation.setItemMainPrice(itemMainPrice);
			item.setPriceAndTaxCalculationItem(priceTaxCalculation);

			SalesOrderMaintainRequestPartyIDParty vendorItemParty = new SalesOrderMaintainRequestPartyIDParty();
			PartyID vendorItemPartyId = new PartyID();
			vendorItemPartyId.setValue(pItemVendorId);
			vendorItemParty.setPartyID(vendorItemPartyId);
			item.setVendorItemParty(vendorItemParty);

			if (observaciones != null && observaciones.length() > 0) {
				SalesOrderMaintainRequestTextCollection textCollection = new SalesOrderMaintainRequestTextCollection();
				SalesOrderMaintainRequestTextCollectionText textCollectionText = new SalesOrderMaintainRequestTextCollectionText();
				textCollectionText.setActionCode(r.getActionCode());
				TextCollectionTextTypeCode typeCode = new TextCollectionTextTypeCode();
				typeCode.setValue(cTypeCodeObservacionesItem);
				textCollectionText.setTypeCode(typeCode);
				textCollectionText.setContentText(observaciones);
				textCollection.getText().add(textCollectionText);
				item.setItemTextCollection(textCollection);
			}

			/* MODIFICACIONES PARAMETROS FLAM */

			item.setUbicacinorigenPD(r.getUbicacionOrigen());
			item.setUbicacinCrucePD(r.getUbicacionCruce());
			item.setUbicacinDestinoPD(r.getUbicacionDestino());
			item.setFechaCargaETDPD2(formatDate(r.getFechaCarga()));
			item.setFechaDestinoETAPD2(formatDate(r.getFechaDestino()));
			item.setFechaCrucePD2(formatDate(r.getFechaCruce()));

			item.setConsignatarioDestinoPD(r.getConsignatarioDestino());
			item.setTipodeEquipoPD(r.getTipoEquipo());
			item.setNmerodeequipoPD(r.getNumeroEquipoCarga());
			item.setGuaHousePD(r.getGuiaHouse());
			item.setGuaMasterPD(r.getGuíaMaster());
			item.setRequierePODPD(r.getRequierePOD());
			item.setServicio(r.getNoServicio());

		} catch (Exception e) {
			logger.error("Error al armar Item:" + e.getMessage(), e);
		}
		return item;
	}

	private XMLGregorianCalendar formatDate(Date date) {
		String FORMATER = "yyyy-MM-dd";
		DateFormat format = new SimpleDateFormat(FORMATER);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
		} catch (Exception e) {
			return null;
		}
	}
}
