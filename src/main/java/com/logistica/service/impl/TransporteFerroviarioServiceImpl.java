package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITransporteFerroviarioDao;
import com.logistica.model.cartaporte.TransporteFerroviario;
import com.logistica.service.ITransporteFerroviarioService;

@Service
public class TransporteFerroviarioServiceImpl implements ITransporteFerroviarioService {

	@Autowired
	ITransporteFerroviarioDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TransporteFerroviario> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TransporteFerroviario> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TransporteFerroviario findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TransporteFerroviario save(TransporteFerroviario transporteFerroviario) {
		return dao.save(transporteFerroviario);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TransporteFerroviario transporteFerroviario = dao.findById(id).orElse(null);
		if (transporteFerroviario != null) {
			dao.delete(transporteFerroviario);
		}
	}

}
