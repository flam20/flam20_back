package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IMonedaDao;
import com.logistica.model.Moneda;
import com.logistica.service.IMonedaService;

@Service
public class MonedaServiceImpl implements IMonedaService {

	@Autowired
	IMonedaDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Moneda> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Moneda> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Moneda findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Moneda save(Moneda mpc) {
		return dao.save(mpc);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Moneda moneda = dao.findById(id).orElse(null);
		if (moneda != null) {
			dao.delete(moneda);
		}
	}

}
