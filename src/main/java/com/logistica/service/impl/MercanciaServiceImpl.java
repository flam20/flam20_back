package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IMercanciaDao;
import com.logistica.model.cartaporte.Mercancia;
import com.logistica.service.IMercanciaService;

@Service
public class MercanciaServiceImpl implements IMercanciaService {

	@Autowired
	IMercanciaDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Mercancia> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Mercancia> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Mercancia findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Mercancia save(Mercancia mercancia) {
		return dao.save(mercancia);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Mercancia mercancia = dao.findById(id).orElse(null);
		if (mercancia != null) {
			dao.delete(mercancia);
		}
	}

}
