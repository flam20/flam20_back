package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITipoEquipoDao;
import com.logistica.model.TipoEquipo;
import com.logistica.service.ITipoEquipoService;

@Service
public class TipoEquipoServiceImpl implements ITipoEquipoService {

	@Autowired
	ITipoEquipoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TipoEquipo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoEquipo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoEquipo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TipoEquipo save(TipoEquipo tipoEquipo) {
		return dao.save(tipoEquipo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TipoEquipo tipoEquipo = dao.findById(id).orElse(null);
		if (tipoEquipo != null) {
			dao.delete(tipoEquipo);
		}
	}

}
