package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IRolDao;
import com.logistica.model.Rol;
import com.logistica.service.IRolService;

@Service
public class RolServiceImpl implements IRolService {

	@Autowired
	IRolDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Rol> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Rol> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Rol findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Rol save(Rol rol) {
		return dao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Rol rol = dao.findById(id).orElse(null);
		if (rol != null) {
			dao.delete(rol);
		}
	}

}
