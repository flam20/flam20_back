package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IBitacoraSapDao;
import com.logistica.model.BitacoraSap;
import com.logistica.service.IBitacoraSapService;

@Service
public class BitacoraSapServiceImpl implements IBitacoraSapService {

	@Autowired
	IBitacoraSapDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<BitacoraSap> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<BitacoraSap> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public BitacoraSap findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional()
	public BitacoraSap save(BitacoraSap bitacoraSap) {
		return dao.save(bitacoraSap);
	}

	@Override
	@Transactional()
	public void delete(Long id) {
		BitacoraSap bitacoraSap = dao.findById(id).orElse(null);
		if (bitacoraSap != null) {
			dao.delete(bitacoraSap);
		}
	}

	
	public List<BitacoraSap> findByIdServicio(Long idServicio) {
		return dao.findByIdServicio(idServicio);
	}

}
