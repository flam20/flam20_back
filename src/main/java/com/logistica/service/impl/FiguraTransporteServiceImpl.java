package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IFiguraTransporteDao;
import com.logistica.model.cartaporte.FiguraTransporte;
import com.logistica.service.IFiguraTransporteService;

@Service
public class FiguraTransporteServiceImpl implements IFiguraTransporteService {

	@Autowired
	IFiguraTransporteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<FiguraTransporte> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FiguraTransporte> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public FiguraTransporte findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public FiguraTransporte save(FiguraTransporte figuraTransporte) {
		return dao.save(figuraTransporte);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		FiguraTransporte figuraTransporte = dao.findById(id).orElse(null);
		if (figuraTransporte != null) {
			dao.delete(figuraTransporte);
		}
	}

}
