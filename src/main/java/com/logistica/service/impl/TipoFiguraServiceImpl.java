package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITipoFiguraDao;
import com.logistica.model.cartaporte.TipoFigura;
import com.logistica.service.ITipoFiguraService;

@Service
public class TipoFiguraServiceImpl implements ITipoFiguraService {

	@Autowired
	ITipoFiguraDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TipoFigura> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TipoFigura> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TipoFigura findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TipoFigura save(TipoFigura tipoFigura) {
		return dao.save(tipoFigura);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TipoFigura tipoFigura = dao.findById(id).orElse(null);
		if (tipoFigura != null) {
			dao.delete(tipoFigura);
		}
	}

}
