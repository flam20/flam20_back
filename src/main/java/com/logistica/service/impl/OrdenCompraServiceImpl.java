package com.logistica.service.impl;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;

import com.logistica.dao.IPropiedadDao;
import com.logistica.dao.IServicioDao;
import com.logistica.dao.IServicioInterDao;
import com.logistica.model.EmpleadoSAP;
import com.logistica.model.OrdenCompraRequest;
import com.logistica.model.Propiedad;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;
import com.logistica.model.ServicioPorCliente;
import com.logistica.sap.handler.OrdenCompraHandlerResolver;
import com.logistica.sap.ordenCompra.BusinessTransactionDocumentID;
import com.logistica.sap.ordenCompra.Log;
import com.logistica.sap.ordenCompra.LogItem;
import com.logistica.sap.ordenCompra.ManagePurchaseRequestIn;
import com.logistica.sap.ordenCompra.ManuallyPurchaseRequest;
import com.logistica.sap.ordenCompra.PurchaseRequestItemManually;
import com.logistica.sap.ordenCompra.PurchaseRequestMessageManuallyCreate;
import com.logistica.sap.ordenCompra.PurchaseRequestResponse;
import com.logistica.sap.ordenCompra.Service;
import com.logistica.sap.ordenCompra.ServicePRD;
import com.logistica.service.IOrdenCompraService;
import com.logistica.service.impl.util.OrdenCompraUtil;
import com.logistica.util.ActionCode;
import com.sun.xml.ws.policy.privateutil.PolicyLogger;

@org.springframework.stereotype.Service
public class OrdenCompraServiceImpl implements IOrdenCompraService {
	static Logger logger = Logger.getLogger(OrdenCompraServiceImpl.class.getName());

	@Autowired
	IPropiedadDao propiedadDao;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	IServicioDao servicioDao;

	@Autowired
	IServicioInterDao servicioInterDao;

	@Autowired
	private OrdenCompraUtil utility;

	String pwd;
	PurchaseRequestMessageManuallyCreate puRequest = null;
	OrdenCompraRequest ocr = null;
	ManagePurchaseRequestIn binding;

	public String creaOrdenCompra(Servicio s, EmpleadoSAP empleadoSAP) {
//		if (validaServicio(s.getFechaServicio())) {
		logger.info("[" + s.getIdServicioStr() + "] ===== CREA ORDEN DE COMPRA ======");
		try {
			ejecutaServicioSAP(s, ActionCode.CREATE, empleadoSAP, false);
			return "";
		} catch (Exception e) {
			logger.error("[" + s.getIdServicioStr() + "] Error al ejecutar el servicio de SAP:" + e.getMessage(), e);
			return null;
		}

//		} else {
//			return null;
//		}
	}

	public String actualizaOrdenCompra(Servicio s, EmpleadoSAP empleadoSAP) {

//		if (validaServicio(s.getFechaServicio())) {
		logger.info("[" + s.getIdServicioStr() + "] ===== ACTUALIZA ORDEN DE COMPRA " + s.getOrdenCompra() + " ======");
		try {
			ejecutaServicioSAP(s, ActionCode.SAVE, empleadoSAP, false);
		} catch (Exception e) {
			logger.error("Error al ejecutar el servicio de SAP:" + e.getMessage(), e);
			return null;
		}

		return null;
//		} else {
//			return null;
//		}
	}

	public String cancelaOrdenCompra(Servicio s, EmpleadoSAP empleadoSAP) {

		logger.info("[" + s.getIdServicioStr() + "] ===== CANCELA ORDEN DE COMPRA ======");
		try {
			ejecutaServicioSAP(s, ActionCode.SAVE, empleadoSAP, true);
		} catch (Exception e) {
			logger.error("[" + s.getIdServicioStr() + "] Error al ejecutar el servicio de SAP:" + e.getMessage(), e);
			return null;
		}

		return null;
	}

	public String creaOrdenCompra_Inter(ServicioInter s, EmpleadoSAP empleadoSAP) {
//		String ordenCompra = null;
		logger.info(s.getIdServicioStr() + " ===== CREA ORDEN DE COMPRA IMPO ======");
		ejecutaServicioSAP_Inter(s, ActionCode.CREATE, empleadoSAP);
		return "";
	}

	public String actualizaOrdenCompra_Inter(ServicioInter s, EmpleadoSAP empleadoSAP) {
		String out = null;
		logger.info(s.getIdServicioStr() + "===== ACTUALIZA ORDEN DE COMPRA IMPO " + s.getOrdenCompra() + " ======");
		out = ejecutaServicioSAP_Inter(s, ActionCode.SAVE, empleadoSAP);

		return out;
	}

	public boolean eliminaItem(String idServicioStr, String ordenCompra, Long itemId, EmpleadoSAP empleadoSAP) {
		
			logger.info(idServicioStr + "============ INICIO ELIMINA ITEM DE OC ============");
			boolean salida = false;

			binding = obtieneBinding();
			PurchaseRequestMessageManuallyCreate puRequest = new PurchaseRequestMessageManuallyCreate();

			try {

				ManuallyPurchaseRequest pr = new ManuallyPurchaseRequest();
				BusinessTransactionDocumentID documentID = new BusinessTransactionDocumentID();
				documentID.setValue(ordenCompra);
				pr.setPurchaseRequestID(documentID);
				pr.setActionCode(ActionCode.UPDATE.getActionCode());

				PurchaseRequestItemManually item = new PurchaseRequestItemManually();
				item.setActionCode(ActionCode.REMOVE.getActionCode());
				item.setItemID(itemId + "");

				pr.getItem().add(item);
				// ========================SE AÑADE EL REQUEST
				puRequest.getPurchaseRequestMaintainBundle().add(pr);

				/****** UserName & Password *********************/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/
				PurchaseRequestResponse result = binding.maintainBundle(puRequest);
				// salida = parseSAPResponse(idServicio, result);
				Log loge = result.getLog();
				for (LogItem logItem : loge.getItem()) {
					logger.info(idServicioStr + "\t Nota -> " + logItem.getNote());

				}
				if (loge.getItem() != null && loge.getItem().size() > 0) {
					// HAY ERRORES
					salida = false;
				} else {
					// SIN ERRORES
					salida = true;
				}

			} catch (Exception e) {
				// salida = "Error al eliminar el item [" + e.getMessage() +
				// "]";
				logger.error("[" + idServicioStr + "] Error al eliminar el item: " + e.getMessage(), e);
				salida = false;
			}
			logger.info(idServicioStr + " ============ FIN ELIMINA ITEM DE OC ============ " + salida);
			return salida;
		
	}

	private ServicioPorCliente getRuta(Servicio s, int tipo) {
		ServicioPorCliente result = null;
		if (s.getServiciosPorCliente() != null && s.getServiciosPorCliente().size() > 0) {
			logger.info(
					"[" + s.getIdServicioStr() + "] LEE " + s.getServiciosPorCliente().size() + " RUTAS DEL SERVICIO ");
			for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {
				if (sxc.getTipoRuta().getIdTipoRuta() == tipo) {
					result = sxc;
					break;
				}
			}
		} else {
			logger.error(
					"[" + s.getIdServicioStr() + "] SE INTENTARON LEER LAS RUTAS DEL SERVICIO, PERO NO TRAE NINGUNA ");
		}
		return result;
	}

	private ManagePurchaseRequestIn obtieneBinding() {
		ManagePurchaseRequestIn binding = null;
		Propiedad prop = propiedadDao.getOne(1L);
		if (prop != null && prop.getValor().equals("PRD")) {
			ServicePRD service = new ServicePRD();
			PolicyLogger logger = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
			logger.setLevel(Level.OFF);
			OrdenCompraHandlerResolver myHanlderResolver = new OrdenCompraHandlerResolver();
			service.setHandlerResolver(myHanlderResolver);
			binding = service.getBinding();
			Propiedad pass = propiedadDao.findByProp("prd_pass");
			pwd = pass.getValor();
//			log.info("EJECUCION A PRD");

		} else {
			Service service = new Service();
			// log.info("SE CONSULTA A DEV");
			PolicyLogger logger = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
			logger.setLevel(Level.OFF);
			OrdenCompraHandlerResolver myHanlderResolver = new OrdenCompraHandlerResolver();
			service.setHandlerResolver(myHanlderResolver);
			binding = service.getBinding();
			Propiedad pass = propiedadDao.findByProp("dev_pass");
			pwd = pass.getValor();
//			log.info("EJECUCION A DEV");
		}
		return binding;
	}

	private void ejecutaServicioSAP(Servicio s, ActionCode action, EmpleadoSAP empleadoSAP, boolean cancela)
			throws Exception {

		PolicyLogger pLog = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
		pLog.setLevel(Level.OFF);
		logger.info("[" + s.getIdServicioStr() + "] ACCION [" + action.getActionCode() + "]");
		binding = obtieneBinding();
		String pSalesUnit = empleadoSAP.getUnidadVentas() + "-1";
		String numeroEquipo = "";
		ServicioPorCliente origen = null;
		ServicioPorCliente cruce = null;
		ServicioPorCliente destino = null;

		try {
			origen = getRuta(s, 1);
			cruce = getRuta(s, 2);
			destino = getRuta(s, 3);
			if (destino.getTipoMovimiento() != null) {
				if (destino.getTipoMovimiento().getIdTipoMovimiento() == 1L) {
					// Es transbordo, se manda el de descarga
					numeroEquipo = destino.getNumEquipoTipoMovimiento();
				} else {
					// Es directo, se manda el de carga
					numeroEquipo = destino.getNumEquipoTipoEquipo();
				}
			} else {
				numeroEquipo = destino.getNumEquipoTipoEquipo();
			}

			// Long podId = destino.getPodId();
			if (origen.getFechaCarga() != null) {
				logger.error("[" + s.getIdServicioStr() + "] LA FECHA DE CARGA NO ES NULA");
			} else {
				logger.error("[" + s.getIdServicioStr() + "] LA FECHA DE CARGA ES NULA");
			}
			ocr = OrdenCompraRequest.builder().itemActionCode(action.getActionCode())
					.poActionCode(action.getActionCode()).itemServicioFLAM(s.getIdServicioStr() + "")
					// .itemConsignatarioOrigen(getRuta(s,
					// 1).getConsignatario())
					// .itemConsignatarioCruce(getRuta(s, 2) != null &&
					// getRuta(s, 2).getConsignatario() != null
					// ? getRuta(s, 2).getConsignatario() : "")
					.itemConsignatarioDestino(destino.getConsignatario())
					.itemFechaCarga(localDateToDate(origen.getFechaCarga()))
					.itemFechaDestino(localDateToDate(destino.getFechaDestino())).itemNumeroEquipoCarga(numeroEquipo)
					.itemRequierePOD("").itemTipoEquipo(destino.getTipoEquipo().getDescripcionCombinada())
					.itemUnidadVentas(pSalesUnit).itemUbicacionDestino(destino.getUbicacion().getLabel())
					.itemTextValue(pSalesUnit).itemUbicacionOrigen(origen.getUbicacion().getLabel()).cancelItem(cancela)
					.build();

			if (cruce != null && cruce.getFechaCruce() != null) {
				ocr.setItemUbicacionCruce(cruce.getUbicacion() != null ? cruce.getUbicacion().getLabelCruce() : "N/A");
				ocr.setItemFechaAgenteAduanal(localDateToDate(cruce.getFechaAgenteAduanal()));
				ocr.setItemFechaCruce(localDateToDate(cruce.getFechaCruce()));
			}

			puRequest = utility.getTerrestreRequest(ocr, s);

			/****** UserName & Password *********************/
			Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
			reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
			reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
			/************************************************/
			taskExecutor.execute(new Runnable() {
				public void run() {
					PurchaseRequestResponse result = null;
					try {
						result = binding.maintainBundle(puRequest);
						String ordenCompra = utility.leeOrdenCompra(s.getIdServicio(), s.getIdServicioStr() + "",
								result);
						s.setOrdenCompra(ordenCompra);
						servicioDao.updateOrdenCompra(ordenCompra, s.getIdServicio());
						logger.info("[" + s.getIdServicioStr() + "] ===== SE GUARDO LA ORDEN DE COMPRA [" + ordenCompra
								+ "]");
//						return ordenCompra;
					} catch (Exception e) {
						logger.error("[" + s.getIdServicioStr() + "] Error al ejecutar el servicio de OC en SAP::" + e, e);
//						return null;
					}

				}
			});

		} catch (Exception e) {
//					ordenCompra = null;
			logger.error("[" + s.getIdServicioStr() + "] Error al ejecutar la orden de compra:: " + e.getMessage(), e);
//			return null;
		}

//				return ordenCompra;

	}

	private Date localDateToDate(LocalDate localDate) {
		ZoneId defaultZoneId = ZoneId.systemDefault();
		return localDate != null ? Date.from(localDate.atStartOfDay(defaultZoneId).toInstant()) : null;
	}

	private String ejecutaServicioSAP_Inter(ServicioInter s, ActionCode action, EmpleadoSAP empleadoSAP) {
		PolicyLogger pLog = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
		pLog.setLevel(Level.OFF);
		logger.info("[" + s.getIdServicioStr() + "]  ACCION [" + action.getActionCode() + "]");
		String ordenCompra = null;
		binding = obtieneBinding();
		String pSalesUnit = empleadoSAP.getUnidadVentas() + utility.calculaPrefijoUnidadVentas(s.getIdServicioStr());
		try {
			ocr = OrdenCompraRequest.builder().poActionCode(action.getActionCode())
					.itemActionCode(action.getActionCode()).idServicio(s.getIdServicioStr())
					.itemFechaCarga(localDateToDate(s.getFechaSalida()))
					.itemFechaDestino(localDateToDate(s.getFechaArribo3()))
					.itemGuiaMaster((s.getGmPrefijoAerolinea() != null ? s.getGmPrefijoAerolinea().getPrefijo() : "")
							+ "-" + s.getGmNumGuia())
					.itemGuiaHouse(s.getGhPrefijoAgente() + "-" + s.getGhNumGuia())
					.itemPzsPBPCS((s.getPesoBruto() != null ? s.getPesoBruto() : "") + " - "
							+ (s.getPesoCargable() != null ? s.getPesoCargable() : ""))
					.itemUnidadVentas(pSalesUnit).itemShipper(s.getShipper().getNombre())
					.itemUbicacionOrigen(s.getAeropuertoOrigen().getNombre())
					.itemUbicacionDestino(s.getAeropuertoDestino().getNombre()).itemServicioFLAM(s.getIdServicioStr())
					.itemTextValue(pSalesUnit).build();
			puRequest = utility.getExpoImpoRequest(ocr, s.getOrdenCompra(), s.getCargosOrigen(), s.getCargosDestino());

			Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
			reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
			reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
			/************************************************/
			PurchaseRequestResponse result = binding.maintainBundle(puRequest);
			ordenCompra = utility.leeOrdenCompra(s.getIdServicio(), s.getIdServicioStr() + "", result);
			logger.info("ACTUALIZA ORDEN COMPRA SERVICIO[" + s.getIdServicio() + "] OC[" + ordenCompra + "]");
			servicioInterDao.updateOrdenCompra(ordenCompra, s.getIdServicio());

		} catch (Exception e) {

			logger.error("[" + s.getIdServicioStr() + "] Error al generar la orden de compra Inter:: " + e.getMessage(),
					e);
		}
		logger.info(s.getIdServicioStr() + " ============ FIN CREA ORDEN DE COMPRA IMPO ============");
		return ordenCompra;
	}

}
