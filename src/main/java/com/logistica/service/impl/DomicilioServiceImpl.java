package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IDomicilioDao;
import com.logistica.model.cartaporte.Domicilio;
import com.logistica.service.IDomicilioService;

@Service
public class DomicilioServiceImpl implements IDomicilioService {

	@Autowired
	IDomicilioDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Domicilio> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Domicilio> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Domicilio findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Domicilio save(Domicilio domicilio) {
		return dao.save(domicilio);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Domicilio domicilio = dao.findById(id).orElse(null);
		if (domicilio != null) {
			dao.delete(domicilio);
		}
	}

}
