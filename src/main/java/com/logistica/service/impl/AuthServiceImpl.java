package com.logistica.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IUsuarioDao;
import com.logistica.model.Usuario;

@Service
public class AuthServiceImpl implements UserDetailsService {

	static Logger logger = Logger.getLogger(AuthServiceImpl.class.getName());

	@Autowired
	private IUsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("Usuario accediendo al sistema: " + username);
		Usuario usuario = usuarioDao.findByUsername(username);

		if (usuario == null) {
			String msg = "Error en el login: no existe el usuario '" + username + "' en el sistema!";
			logger.error(msg);
			throw new UsernameNotFoundException(msg);
		}
//		logger.info("Rol: " + usuario.getRol().getNombre());
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(usuario.getRol().getNombre()));

		return new User(usuario.getUsername(), usuario.getPassword(), usuario.getEnabled(), true, true, true,
				authorities);
	}

}