package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IPedimentoDao;
import com.logistica.model.cartaporte.Pedimento;
import com.logistica.service.IPedimentoService;

@Service
public class PedimentoServiceImpl implements IPedimentoService {

	@Autowired
	IPedimentoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Pedimento> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Pedimento> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Pedimento findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Pedimento save(Pedimento pedimento) {
		return dao.save(pedimento);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Pedimento pedimento = dao.findById(id).orElse(null);
		if (pedimento != null) {
			dao.delete(pedimento);
		}
	}

}
