package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IAgenteDao;
import com.logistica.model.Agente;
import com.logistica.service.IAgenteService;

@Service
public class AgenteServiceImpl implements IAgenteService {

	@Autowired
	IAgenteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Agente> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Agente> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Agente findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Agente save(Agente agente) {
		return dao.save(agente);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Agente agente = dao.findById(id).orElse(null);
		if (agente != null) {
			dao.delete(agente);
		}
	}

}
