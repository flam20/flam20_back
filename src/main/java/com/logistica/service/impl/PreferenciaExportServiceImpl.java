package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.logistica.dao.IPreferenciaExportDao;
import com.logistica.model.PreferenciaExport;
import com.logistica.service.IPreferenciaExportService;

@Service
public class PreferenciaExportServiceImpl implements IPreferenciaExportService {

	@Autowired
	IPreferenciaExportDao dao;

	@Override
	public Page<PreferenciaExport> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	public List<PreferenciaExport> findAll() {
		return dao.findAll();
	}

	@Override
	public PreferenciaExport findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	public PreferenciaExport save(PreferenciaExport preferenciaExport) {
		return dao.save(preferenciaExport);
	}

	@Override
	public void delete(Long id) {
		PreferenciaExport preferenciaExport = dao.findById(id).orElse(null);
		if (preferenciaExport != null) {
			dao.delete(preferenciaExport);
		}
	}

	@Override
	public PreferenciaExport findByIdUsuarioAndTipo(Long idUsuario, String tipo) {
		return dao.findByTraficoIdUsuarioAndTipo(idUsuario, tipo);
	}

}
