package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICI_ShipperDao;
import com.logistica.model.CI_Shipper;
import com.logistica.service.ICI_ShipperService;

@Service
public class CI_ShipperServiceImpl implements ICI_ShipperService {

	@Autowired
	ICI_ShipperDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<CI_Shipper> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CI_Shipper> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public CI_Shipper findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public CI_Shipper save(CI_Shipper shipper) {
		return dao.save(shipper);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CI_Shipper shipper = dao.findById(id).orElse(null);
		if (shipper != null) {
			dao.delete(shipper);
		}

	}

}
