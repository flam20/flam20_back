package com.logistica.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IServicioInterDao;
import com.logistica.model.ServicioInter;
import com.logistica.service.IServicioInterService;

@Service
public class ServicioInterServiceImpl implements IServicioInterService {

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

	@Autowired
	IServicioInterDao dao;

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public Page<ServicioInter> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ServicioInter> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ServicioInter findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ServicioInter save(ServicioInter servicio) {
		return dao.save(servicio);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ServicioInter servicio = dao.findById(id).orElse(null);
		if (servicio != null) {
			dao.delete(servicio);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Integer getConsecutiveByAnio(Integer anio) {
		return dao.findConsecutivoByAnio(anio);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<ServicioInter> searchServicios(String numeroServicio, String idTrafico, String guiaHouse,
			String guiaMaster, Long idConsignatario, Long idAgente, String fechaDesde, String fechaHasta,
			Boolean borradores, String rol) {
		String sSql = "SELECT DISTINCT s FROM ServicioInter s LEFT JOIN s.cargosOrigen origen LEFT JOIN s.cargosDestino destino";

		sSql += " WHERE ";

		if (borradores != null && !borradores.booleanValue()) {
			sSql += " s.idServicioStr NOT LIKE '%Borrador%' AND";
		}
		if (numeroServicio != null) {
			//sSql += " (s.idServicioStr LIKE :numeroServicio OR origen.proveedor.razonComercial LIKE :numeroServicio OR destino.proveedor.razonComercial LIKE :numeroServicio) AND";
			sSql += " s.idServicioStr LIKE :numeroServicio AND";
		}
		if (idTrafico != null) {
			sSql += " s.trafico = :idTrafico AND";
		}
		if (guiaHouse != null) {
			sSql += " s.ghNumGuia LIKE :guiaHouse AND";
		}
		if (guiaMaster != null) {
			sSql += " s.gmNumGuia LIKE :guiaMaster AND";
		}
		if (idConsignatario != null) {
			sSql += " s.consignatario.idConsignatario = :idConsignatario AND";
		}
		if (idAgente != null) {
			sSql += " s.ghPrefijoAgente.idAgente = :idAgente AND";
		}
		if (fechaDesde != null) {
			sSql += " s.fechaServicio >= :fechaDesde AND";
		}
		if (fechaHasta != null) {
			sSql += " s.fechaServicio <= :fechaHasta AND";
		}
		
		if(rol!=null) {
			sSql += " s.subtipo = :subtipo ";
		}

		if (sSql.endsWith("AND")) {
			sSql = sSql.substring(0, sSql.length() - 3);
		} else if (sSql.endsWith("WHERE ")) {
			sSql = sSql.substring(0, sSql.length() - 6);
		}

		System.out.println("query: " + sSql);
		Query query = em.createQuery(sSql);

		if (numeroServicio != null) {
			query.setParameter("numeroServicio", "%" + numeroServicio.replace("(amp)", "&") + "%");
		}
		if (idTrafico != null) {
			query.setParameter("idTrafico", idTrafico);
		}
		if (guiaHouse != null) {
			query.setParameter("guiaHouse", "%" + guiaHouse + "%");
		}
		if (guiaMaster != null) {
			query.setParameter("guiaMaster", "%" + guiaMaster + "%");
		}
		if (idConsignatario != null) {
			query.setParameter("idConsignatario", idConsignatario);
		}
		if (idAgente != null) {
			query.setParameter("idAgente", idAgente);
		}
		if (fechaDesde != null) {
			query.setParameter("fechaDesde", LocalDate.parse(fechaDesde, formatter));
		}
		if (fechaHasta != null) {
			query.setParameter("fechaHasta", LocalDate.parse(fechaHasta, formatter));
		}
		if (rol != null) {
			query.setParameter("subtipo",rol);
		}

		return query.getResultList();
	}

	@Override
	public void updateChatID(String idChat, Long idServicio) {
		dao.updateChatID(idChat, idServicio);

	}

	@Override
	@Transactional(readOnly = true)
	public List<ServicioInter> findTraficos() {
		List<Object> result = dao.findTraficos();
		List<ServicioInter> servicios = new ArrayList<>();
		for (Object cdata : result) {
			ServicioInter s = new ServicioInter();
			Object[] obj = (Object[]) cdata;
			s.setTrafico((String) obj[0]);
			s.setNombreTrafico((String) obj[1]);
			servicios.add(s);
		}
		return servicios;
	}

}
