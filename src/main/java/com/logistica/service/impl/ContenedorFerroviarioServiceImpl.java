package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IContenedorFerroviarioDao;
import com.logistica.model.cartaporte.ContenedorFerroviario;
import com.logistica.service.IContenedorFerroviarioService;

@Service
public class ContenedorFerroviarioServiceImpl implements IContenedorFerroviarioService {

	@Autowired
	IContenedorFerroviarioDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<ContenedorFerroviario> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ContenedorFerroviario> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ContenedorFerroviario findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ContenedorFerroviario save(ContenedorFerroviario contenedorFerroviario) {
		return dao.save(contenedorFerroviario);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ContenedorFerroviario contenedorFerroviario = dao.findById(id).orElse(null);
		if (contenedorFerroviario != null) {
			dao.delete(contenedorFerroviario);
		}
	}

}
