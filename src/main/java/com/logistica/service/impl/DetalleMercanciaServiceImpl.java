package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IDetalleMercanciaDao;
import com.logistica.model.cartaporte.DetalleMercancia;
import com.logistica.service.IDetalleMercanciaService;

@Service
public class DetalleMercanciaServiceImpl implements IDetalleMercanciaService {

	@Autowired
	IDetalleMercanciaDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<DetalleMercancia> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DetalleMercancia> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public DetalleMercancia findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public DetalleMercancia save(DetalleMercancia detalleMercancia) {
		return dao.save(detalleMercancia);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		DetalleMercancia detalleMercancia = dao.findById(id).orElse(null);
		if (detalleMercancia != null) {
			dao.delete(detalleMercancia);
		}
	}

}
