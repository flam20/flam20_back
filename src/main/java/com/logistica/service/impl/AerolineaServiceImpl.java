package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IAerolineaDao;
import com.logistica.model.Aerolinea;
import com.logistica.service.IAerolineaService;

@Service
public class AerolineaServiceImpl implements IAerolineaService {

	@Autowired
	IAerolineaDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Aerolinea> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Aerolinea> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Aerolinea findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Aerolinea save(Aerolinea aerolinea) {
		return dao.save(aerolinea);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Aerolinea aerolinea = dao.findById(id).orElse(null);
		if (aerolinea != null) {
			dao.delete(aerolinea);
		}
	}

}
