package com.logistica.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.task.TaskExecutor;

import com.logistica.dao.IPropiedadDao;
import com.logistica.dao.IServicioDao;
import com.logistica.dao.IServicioSAPDao;
import com.logistica.model.CargoDestino;
import com.logistica.model.CargoOrigen;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.EmpleadoSAP;
import com.logistica.model.PedidoClienteItemRequest;
import com.logistica.model.PedidoClienteRequest;
import com.logistica.model.Propiedad;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;
import com.logistica.model.ServicioPorCliente;
import com.logistica.model.ServicioSAP;
import com.logistica.sap.consultaPedidoCliente.BusinessTransactionDocumentID;
import com.logistica.sap.consultaPedidoCliente.QueryProcessingConditions;
import com.logistica.sap.consultaPedidoCliente.QuerySalesOrderIn;
import com.logistica.sap.consultaPedidoCliente.ResponseProcessingConditions;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsQueryMessageSync;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsQuerySelectionByElements;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsQuerySelectionByID;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsResponse;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsResponseItem;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsResponseItemStatus;
import com.logistica.sap.consultaPedidoCliente.SalesOrderByElementsResponseMessageSync;
import com.logistica.sap.handler.PedidoClienteHandlerResolver;
import com.logistica.sap.pedidoCliente.ManageSalesOrderIn;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainConfirmationBundleMessageSync;
import com.logistica.sap.pedidoCliente.SalesOrderMaintainRequestBundleMessageSync;
import com.logistica.service.IPedidoClienteService;
import com.logistica.util.ActionCode;
import com.sun.xml.ws.policy.EffectiveAlternativeSelector;
import com.sun.xml.ws.policy.privateutil.PolicyLogger;

@org.springframework.stereotype.Service
public class PedidoClienteServiceImpl implements IPedidoClienteService {
	static Logger log = Logger.getLogger(PedidoClienteServiceImpl.class.getName());

	ManageSalesOrderIn binding;

	@Autowired
	IPropiedadDao propiedadDao;

	@Autowired
	private PedidoClienteUtil utility;

//	@Autowired
//	private TaskExecutor taskExecutor;

	@Autowired
	IServicioSAPDao servicioSAPDao;

	@Autowired
	IServicioDao servicioDao;

	String pwd;
	SalesOrderMaintainConfirmationBundleMessageSync result;
	SalesOrderMaintainRequestBundleMessageSync soRequest;

	public String creaPedidoCliente(Servicio s, String currency, EmpleadoSAP empleadoSAP) {
		String pedidoCliente = null;
//		if (validaServicio(s.getFechaServicio())) {
		log.info("[" + s.getIdServicioStr() + "] ===== CREA PEDIDO CLIENTE TERRESTRE ======");
		pedidoCliente = ejecutaServicioSAP(s, currency, empleadoSAP, ActionCode.CREATE);
		return pedidoCliente;
//		} else {
//			return null;
//		}

	}

	public String creaPedidoClienteInter(ServicioInter s, String currency, EmpleadoSAP empleadoSAP) {
		String pedidoCliente = null;

		log.info("[" + s.getIdServicioStr() + "] ===== CREA PEDIDO CLIENTE INTER ======");
		pedidoCliente = ejecutaServicioSAP_Inter(s, currency, empleadoSAP, ActionCode.CREATE);
		return pedidoCliente;

	}

	public String actualizaRuta(Servicio s, ActionCode action, EmpleadoSAP empleadoSAP, ServicioPorCliente ruta) {

		String pedidoCliente = null;
		PedidoClienteRequest pcr = null;
		ServicioSAP servicioSAP = null;
		init();
		log.info("[" + s.getIdServicioStr() + "] ACTUALIZA PEDIDO DE CLIENTE -> RUTA");
		try {
			String notasGenerales = s.getNotas() != null && s.getNotas().length() > 0 ? s.getNotas() : "N/A";
			String refFactura = getRuta(s, 3).getReferenciaCliente() != null
					&& getRuta(s, 3).getReferenciaCliente().length() > 0 ? getRuta(s, 3).getReferenciaCliente() : null;
			Long podId = getRuta(s, 3).getPodId();
			String numeroEquipo = "";

			if (getRuta(s, 3).getTipoMovimiento() != null) {
//					log.info("---> " + getRuta(s, 3).getTipoMovimiento());
				if (getRuta(s, 3).getTipoMovimiento().getIdTipoMovimiento() == 1L) {
					// Es transbordo, se manda el de descarga
//						log.info("---> " + getRuta(s, 3).getNumEquipoTipoMovimiento());
					numeroEquipo = getRuta(s, 3).getNumEquipoTipoMovimiento();
				} else {
					// Es directo, se manda el de carga
					numeroEquipo = getRuta(s, 3).getNumEquipoTipoEquipo();
				}
			} else {
				numeroEquipo = getRuta(s, 3).getNumEquipoTipoEquipo();
			}

			pcr = PedidoClienteRequest.builder().buyerId(s.getCliente().getNumeroClienteSAP())
					.pedidoCliente(ruta.getPedidoCliente()).actionCode(action.getActionCode())
					.notasGenerales(notasGenerales).referenciaFactura(refFactura)
					.requestName("Servicio Terrestre FLAM " + s.getIdServicioStr())
					.billTo(s.getCliente().getNumeroClienteSAP()).accountId(s.getCliente().getNumeroClienteSAP())
					.employeeId(empleadoSAP.getIdEmpleado()).salesUnit(empleadoSAP.getUnidadVentas() + "-1")
					.pricingTermsCurrency(ruta.getMonedaVenta().getSimbolo()).deliveryTermsPriority("3")
					.consignatarioOrigen(getRuta(s, 1).getConsignatario())
					.consignatarioCruce(getRuta(s, 2) != null ? getRuta(s, 2).getConsignatario() : "N/A")
					.consignatarioDestino(getRuta(s, 3).getConsignatario())
					.ubicacionOrigen(getRuta(s, 1).getUbicacion().getLabel())
					.ubicacionCruce(getRuta(s, 2) != null ? getRuta(s, 2).getUbicacion().getLabelCruce() : "N/A")
					.ubicacionDestino(getRuta(s, 3).getUbicacion().getLabel()).requierePOD("")
					.numeroEquipoCarga(numeroEquipo).tipoEquipo(getRuta(s, 3).getTipoEquipo().getDescCorta())
					.fechaCarga(localDateToDate(getRuta(s, 1).getFechaCarga()))
					.fechaDestino(localDateToDate(getRuta(s, 3).getFechaDestino()))
					// .fechaAgente(getRuta(s, 2) != null ? getRuta(s, 2).getFechaAgenteAduanal() :
					// null)
					.fechaAgente(null)
//					.fechaCruce(getRuta(s, 2) != null ? getRuta(s, 2).getFechaCruce() : null)
					.fechaCruce(null).noServicio(s.getIdServicioStr() + "").build();

			List<PedidoClienteItemRequest> items = new ArrayList<PedidoClienteItemRequest>();
			PedidoClienteItemRequest item = null;

			// ------------------ AGREGA ITEMS DE RUTAS
			// for (ServicioPorCliente ruta : s.getServiciosPorCliente()) {
			log.info(ruta.getTipoRuta().getDescripcion() + " -> PC:" + ruta.getPedidoCliente());
			if (ruta.getVenta() != null && (ruta.getVenta().compareTo(BigDecimal.ZERO) > 0)) {
				String tipoServicio = ruta.getUbicacion().getPais().equals("MEXICO") ? "NACIONAL" : "EXPO";
				String pCargoFLAM = (ruta.getTipoRuta().getIdTipoRuta().compareTo(new Long(2)) == 0) ? "CRUCE"
						: "FLETE";
				servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio, pCargoFLAM);
//				log.info("BUSCA SERVICIO SAP:" + tipoServicio + " - " + pCargoFLAM);
				if (servicioSAP != null) {
					item = PedidoClienteItemRequest.builder().itemID(ruta.getTipoRuta().getIdTipoRuta() + "")
							.itemProductId(servicioSAP.getIdServicio()).itemPrice(ruta.getVenta())
							.itemCurrency(ruta.getMonedaVenta().getSimbolo()).itemObservaciones(null)
							.itemVendorId(ruta.getProveedor().getMotivoBloqueo() + "").build();
					items.add(item);
				} else {
					log.error("No se encontro servicioSAP:" + tipoServicio + " - " + pCargoFLAM);
				}

			}

			// }

			if (items != null && items.size() > 0) {
				soRequest = utility.getSalesOrderRequest(s.getIdServicioStr() + "", pcr, items);

				/****** UserName & Password *********************/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/

				result = binding.maintainBundle(soRequest);

				pedidoCliente = utility.leePedidoCliente(s.getIdServicio(), s.getIdServicioStr() + "",
						ruta.getMonedaVenta().getSimbolo(), result);
			} else {
				log.error("[" + s.getIdServicioStr() + "]NO HAY ITEMS PARA ENVIAR");
			}

		} catch (Exception e) {
			log.error("[" + s.getIdServicioStr() + "] Error al actualizar la ruta:: " + e.getMessage(), e);
		}
//			log.info(s.getIdServicioStr() + "", "============ FIN ACTUALIZA PEDIDO DE CLIENTE RUTA ============");
		return pedidoCliente;

	}

	public String actualizaCargo(Servicio s, ActionCode action, EmpleadoSAP empleadoSAP, CargoPorServicio cargo) {

		String pedidoCliente = null;
		PedidoClienteRequest pcr = null;
		ServicioSAP servicioSAP = null;
		init();
		log.info("[" + s.getIdServicioStr() + "] ACTUALIZA PEDIDO DE CLIENTE -> CARGO[" + cargo.getPedidoCliente()
				+ "]");
		try {
			String notasGenerales = s.getNotas() != null && s.getNotas().length() > 0 ? s.getNotas() : "N/A";
			String refFactura = getRuta(s, 3).getReferenciaCliente() != null
					&& getRuta(s, 3).getReferenciaCliente().length() > 0 ? getRuta(s, 3).getReferenciaCliente() : null;
			Long podId = getRuta(s, 3).getPodId();
			String numeroEquipo = "";

			if (getRuta(s, 3).getTipoMovimiento() != null) {
//				log.info("---> " + getRuta(s, 3).getTipoMovimiento());
				if (getRuta(s, 3).getTipoMovimiento().getIdTipoMovimiento() == 1L) {
					// Es transbordo, se manda el de descarga
//					log.info("---> " + getRuta(s, 3).getNumEquipoTipoMovimiento());
					numeroEquipo = getRuta(s, 3).getNumEquipoTipoMovimiento();
				} else {
					// Es directo, se manda el de carga
					numeroEquipo = getRuta(s, 3).getNumEquipoTipoEquipo();
				}
			} else {
				numeroEquipo = getRuta(s, 3).getNumEquipoTipoEquipo();
			}

			pcr = PedidoClienteRequest.builder().buyerId(s.getCliente().getNumeroClienteSAP())
					.pedidoCliente(cargo.getPedidoCliente()).actionCode(action.getActionCode())
					.notasGenerales(notasGenerales).referenciaFactura(refFactura)
					.requestName("Servicio Terrestre FLAM " + s.getIdServicioStr())
					.billTo(s.getCliente().getNumeroClienteSAP()).accountId(s.getCliente().getNumeroClienteSAP())
					.employeeId(empleadoSAP.getIdEmpleado()).salesUnit(empleadoSAP.getUnidadVentas() + "-1")
					.pricingTermsCurrency(cargo.getMonedaVenta().getSimbolo()).deliveryTermsPriority("3")
					.consignatarioOrigen(getRuta(s, 1).getConsignatario())
					.consignatarioCruce(getRuta(s, 2) != null ? getRuta(s, 2).getConsignatario() : "N/A")
					.consignatarioDestino(getRuta(s, 3).getConsignatario())
					.ubicacionOrigen(getRuta(s, 1).getUbicacion().getLabel())
					.ubicacionCruce(getRuta(s, 2) != null ? getRuta(s, 2).getUbicacion().getLabelCruce() : "N/A")
					.ubicacionDestino(getRuta(s, 3).getUbicacion().getLabel()).requierePOD("")
					.numeroEquipoCarga(numeroEquipo).tipoEquipo(getRuta(s, 3).getTipoEquipo().getDescCorta())
					.fechaCarga(localDateToDate(getRuta(s, 1).getFechaCarga()))
					.fechaDestino(localDateToDate(getRuta(s, 3).getFechaDestino()))
//					.fechaAgente(getRuta(s, 2) != null ? getRuta(s, 2).getFechaAgenteAduanal() : null)
					.fechaAgente(null)
//					.fechaCruce(getRuta(s, 2) != null ? getRuta(s, 2).getFechaCruce() : null)
					.fechaCruce(null).noServicio(s.getIdServicioStr() + "").build();

			List<PedidoClienteItemRequest> items = new ArrayList<PedidoClienteItemRequest>();
			PedidoClienteItemRequest item = null;

			// ------------------ AGREGA ITEMS DE CARGOS
			// for (CargoPorServicio cargo : s.getCargosPorServicio()) {
			log.info(cargo.getPosicion() + " -> PC:" + cargo.getPedidoCliente());
			// if (cargo.getPedidoCliente() == null) {
			if (cargo.getVenta() != null && (cargo.getVenta().compareTo(BigDecimal.ZERO) > 0)) {

				servicioSAP = servicioSAPDao.getServicioSAPByCargo("NACIONAL", cargo.getCargo().getDescCorta());

				if (servicioSAP != null && !servicioSAP.getIdServicio().startsWith("NOTA")) {
					item = PedidoClienteItemRequest.builder().itemProductId(servicioSAP.getIdServicio())
							.itemID(cargo.getPosicion() + "").itemPrice(cargo.getVenta())
							.itemCurrency(cargo.getMonedaVenta().getSimbolo())
							.itemObservaciones(cargo.getObservaciones())
							.itemVendorId(cargo.getProveedor().getMotivoBloqueo() + "").build();
					items.add(item);
				}

			}

			if (items != null && items.size() > 0) {
				soRequest = utility.getSalesOrderRequest(s.getIdServicioStr() + "", pcr, items);

				/****** UserName & Password *********************/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/

				result = binding.maintainBundle(soRequest);

				pedidoCliente = utility.leePedidoCliente(s.getIdServicio(), s.getIdServicioStr() + "",
						cargo.getMonedaVenta().getSimbolo(), result);
			} else {
				log.error("NO HAY ITEMS PARA ENVIAR");
			}

		} catch (Exception e) {
			log.error("[" + s.getIdServicioStr() + "] Error al crear el pedido de cliente:: " + e.getMessage(), e);
		}
//		log.info(s.getIdServicioStr() + "", "============ FIN CREA PEDIDO DE CLIENTE ============");
		return pedidoCliente;
	}

	private String ejecutaServicioSAP(Servicio s, String currency, EmpleadoSAP empleadoSAP, ActionCode action) {
//		String pedidoCliente = null;
		PedidoClienteRequest pcr = null;
//		ServicioSAP servicioSAP = null;
		ServicioPorCliente origen = null;
		ServicioPorCliente cruce = null;
		ServicioPorCliente destino = null;

		init();
		log.info("[" + s.getIdServicioStr() + "] CREA PEDIDO DE CLIENTE [" + currency + "]");
		try {

			origen = getRuta(s, 1);
			cruce = getRuta(s, 2);
			destino = getRuta(s, 3);

			String notasGenerales = s.getNotas() != null && s.getNotas().length() > 0 ? s.getNotas() : "N/A";
			String refFactura = destino.getReferenciaCliente() != null && destino.getReferenciaCliente().length() > 0
					? destino.getReferenciaCliente()
					: null;
			// Long podId = destino.getPodId();
			String numeroEquipo = "";

			if (destino.getTipoMovimiento() != null) {
//				log.info("---> " + destino.getTipoMovimiento());
				if (destino.getTipoMovimiento().getIdTipoMovimiento() == 1L) {
					// Es transbordo, se manda el de descarga
//					log.info("---> " + destino.getNumEquipoTipoMovimiento());
					numeroEquipo = destino.getNumEquipoTipoMovimiento();
				} else {
					// Es directo, se manda el de carga
					numeroEquipo = destino.getNumEquipoTipoEquipo();
				}
			} else {
				numeroEquipo = destino.getNumEquipoTipoEquipo();
			}

			pcr = PedidoClienteRequest.builder().buyerId(s.getCliente().getNumeroClienteSAP())
					.actionCode(action.getActionCode()).notasGenerales(notasGenerales).referenciaFactura(refFactura)
					.requestName("Servicio Terrestre FLAM " + s.getIdServicioStr())
					.billTo(s.getCliente().getNumeroClienteSAP()).accountId(s.getCliente().getNumeroClienteSAP())
					.employeeId(empleadoSAP.getIdEmpleado()).salesUnit(empleadoSAP.getUnidadVentas() + "-1")
					.pricingTermsCurrency(currency).deliveryTermsPriority("3")
					.consignatarioOrigen(origen.getConsignatario())
//					.consignatarioCruce(getRuta(s, 2) != null ? getRuta(s, 2).getConsignatario() : "N/A")
					.consignatarioDestino(destino.getConsignatario()).ubicacionOrigen(origen.getUbicacion().getLabel())
//					.ubicacionCruce(getRuta(s, 2) != null ? getRuta(s, 2).getUbicacion().getLabelCruce() : "N/A")
					.ubicacionDestino(destino.getUbicacion().getLabel()).requierePOD("").numeroEquipoCarga(numeroEquipo)
					.tipoEquipo(destino.getTipoEquipo().getDescCorta())
					.fechaCarga(localDateToDate(origen.getFechaCarga()))
					.fechaDestino(localDateToDate(destino.getFechaDestino()))
//					.fechaAgente(localDateToDate(getRuta(s, 2).getFechaAgenteAduanal()))
//					.fechaCruce(localDateToDate(getRuta(s, 2).getFechaCruce()))
					.noServicio(s.getIdServicioStr() + "").build();

			if (cruce != null && cruce.getFechaCruce() != null) {
				pcr.setConsignatarioCruce(cruce.getConsignatario() != null ? cruce.getConsignatario() : "N/A");
				pcr.setUbicacionCruce(cruce.getUbicacion() != null ? cruce.getUbicacion().getLabelCruce() : "N/A");
				pcr.setFechaAgente(localDateToDate(cruce.getFechaAgenteAduanal()));
				pcr.setFechaCruce(localDateToDate(cruce.getFechaCruce()));
			}

			List<PedidoClienteItemRequest> items = new ArrayList<PedidoClienteItemRequest>();
			PedidoClienteItemRequest item = null;

			// ------------------ AGREGA ITEMS DE RUTAS
			for (ServicioPorCliente ruta : s.getServiciosPorCliente()) {
				log.info(ruta.getTipoRuta().getDescripcion() + " -> PC:" + ruta.getPedidoCliente());
				if (ruta.getPedidoCliente() != null && ruta.getPedidoCliente().length() > 0) {
					log.info(ruta.getTipoRuta().getDescripcion() + " NO SE AGREGA PORQUE YA TIENE PEDIDO DE CLIENTE");

				} else {
					log.info(ruta.getTipoRuta().getDescripcion() + " SE AGREGA AL PEDIDO DE CLIENTE");
					if (ruta.getVenta() != null && (ruta.getVenta().compareTo(BigDecimal.ZERO) > 0)
							&& ruta.getMonedaVenta().getSimbolo().equals(currency)) {
						String tipoServicio = ruta.getUbicacion().getPais().equals("MEXICO") ? "NACIONAL" : "EXPO";
						String pCargoFLAM = (ruta.getTipoRuta().getIdTipoRuta().compareTo(new Long(2)) == 0) ? "CRUCE"
								: "FLETE";
						String pContentTextReferencia = ruta.getReferenciaCliente() != null
								&& ruta.getReferenciaCliente().length() > 0 ? ruta.getReferenciaCliente() : "N/A";
						ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio, pCargoFLAM);
//						log.info("BUSCA SERVICIO SAP:" + tipoServicio + " - " + pCargoFLAM);

						if (servicioSAP != null) {
							item = PedidoClienteItemRequest.builder().itemID(ruta.getTipoRuta().getIdTipoRuta() + "")
									.itemProductId(servicioSAP.getIdServicio()).itemPrice(ruta.getVenta())
									.itemCurrency(ruta.getMonedaVenta().getSimbolo())
									.itemObservaciones(pContentTextReferencia)
									.itemVendorId(ruta.getProveedor().getMotivoBloqueo() + "").build();
							items.add(item);
						} else {
							log.error("No se encontro servicioSAP:" + tipoServicio + " - " + pCargoFLAM);
						}

					}
				}
			}

			if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0) {
				// ------------------ AGREGA ITEMS DE CARGOS

				for (CargoPorServicio cargo : s.getCargosPorServicio()) {
					if (cargo.getPedidoCliente() != null && cargo.getPedidoCliente().length() > 0) {

						log.info("POSICION " + cargo.getPosicion() + " NO SE AGREGA PORQUE YA TIENE PEDIDO DE CLIENTE");

					} else {
						if (cargo.getVenta() != null && (cargo.getVenta().compareTo(BigDecimal.ZERO) > 0)
								&& cargo.getMonedaVenta().getSimbolo().equals(currency)) {

							ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo("NACIONAL",
									cargo.getCargo().getDescCorta());

							if (servicioSAP != null && !servicioSAP.getIdServicio().startsWith("NOTA")) {
								item = PedidoClienteItemRequest.builder().itemID(cargo.getPosicion() + "")
										.itemProductId(servicioSAP.getIdServicio()).itemPrice(cargo.getVenta())
										.itemCurrency(cargo.getMonedaVenta().getSimbolo())
										.itemObservaciones(cargo.getObservaciones())
										.itemVendorId(cargo.getProveedor().getMotivoBloqueo() + "").build();
								items.add(item);
							} else {
								log.error("No se encontro Servicio SAP " + "NACIONAL - "
										+ cargo.getCargo().getDescCorta());
							}

						}
					}
				}
			}
			if (items != null && items.size() > 0) {
				soRequest = utility.getSalesOrderRequest(s.getIdServicioStr() + "", pcr, items);

				/****** UserName & Password *********************/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/
//				taskExecutor.execute(new Runnable() {
//					public void run() {

				try {
					result = binding.maintainBundle(soRequest);
					String pedidoCliente = utility.leePedidoCliente(s.getIdServicio(), s.getIdServicioStr() + "",
							currency, result);
					if (pedidoCliente != null) {
						for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {
							if (sxc.getVenta().compareTo(BigDecimal.ZERO) > 0
									&& sxc.getMonedaVenta().getSimbolo().equals(currency)) {
								sxc.setPedidoCliente(pedidoCliente);
							}
						}

						if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0) {
							for (CargoPorServicio c : s.getCargosPorServicio()) {
								if (c.getVenta().compareTo(BigDecimal.ZERO) > 0
										&& c.getMonedaVenta().getSimbolo().equals(currency)) {
									c.setPedidoCliente(pedidoCliente);
								}
							}
						}
						servicioDao.save(s);
//								try {
//									if (wcService != null) {
//										wcService.creaRegistro(s);
//									} else {
//										log.info("wcService es null");
//									}
//								} catch (Exception e) {
//									log.error("Error al generar WC dentro de pedido cliente:: " + e, e);
//								}

					} else {
						log.error("NO SE GENERÓ PEDIDO A CLIENTE");
					}
				} catch (Exception e) {
					log.error("Error al ejecutar el servicio de Pedido de cliente en SAP::" + e, e);
				}

//					}
//				});

			} else {
				log.error("NO HAY ITEMS PARA ENVIAR");
			}

		} catch (Exception e) {
			log.error("[" + s.getIdServicioStr() + "] Error al generar el pedido a cliente:: " + e.getMessage(), e);
		}
		return "";

	}

	private void init() {
		binding = obtieneBinding();

	}

	/*
	 * private boolean validaServicio(Date fecha) { SimpleDateFormat sdformat = new
	 * SimpleDateFormat("yyyy-MM-dd"); java.util.Date d1;
	 * 
	 * try { d1 = sdformat.parse("2020-02-05");
	 * 
	 * if (fecha.compareTo(d1) > 0) { // si ejecuta return true; } else { return
	 * false; } } catch (Exception e) { log.error("Error al validar la fecha:" +
	 * e.getMessage(), e); return false; } }
	 */

	private ManageSalesOrderIn obtieneBinding() {
		ManageSalesOrderIn binding = null;
		Propiedad prop = propiedadDao.getOne(1L);
		if (prop != null && prop.getValor().equals("PRD")) {
			com.logistica.sap.pedidoCliente.ServicePRD service = new com.logistica.sap.pedidoCliente.ServicePRD();
			PolicyLogger logger = PolicyLogger.getLogger(EffectiveAlternativeSelector.class);
			logger.setLevel(Level.OFF);
			PedidoClienteHandlerResolver myHanlderResolver = new PedidoClienteHandlerResolver();
			service.setHandlerResolver(myHanlderResolver);
			binding = service.getBinding();
			Propiedad pass = propiedadDao.findByProp("prd_pass");
			pwd = pass.getValor();

		} else {
			com.logistica.sap.pedidoCliente.Service service = new com.logistica.sap.pedidoCliente.Service();
//			log.info("EJECUCION A DEV");
			PolicyLogger loggerSOAP = PolicyLogger.getLogger(EffectiveAlternativeSelector.class);
			loggerSOAP.setLevel(Level.OFF);
			PedidoClienteHandlerResolver myHanlderResolver = new PedidoClienteHandlerResolver();
			service.setHandlerResolver(myHanlderResolver);
			binding = service.getBinding();
			Propiedad pass = propiedadDao.findByProp("dev_pass");
			pwd = pass.getValor();

		}
		return binding;
	}

	private QuerySalesOrderIn obtieneBindingConsulta() {
		QuerySalesOrderIn binding = null;
		Propiedad prop = propiedadDao.getOne(1L);
		if (prop != null && prop.getValor().equals("PRD")) {
			com.logistica.sap.consultaPedidoCliente.ServicePRD service = new com.logistica.sap.consultaPedidoCliente.ServicePRD();

			binding = service.getBinding();
			Propiedad pass = propiedadDao.findByProp("prd_pass");
			pwd = pass.getValor();
//			log.info("SE CONSULTA A PRD " + pwd);

		} else {
			com.logistica.sap.consultaPedidoCliente.Service service = new com.logistica.sap.consultaPedidoCliente.Service();
//			log.info("SE CONSULTA A DEV");
			binding = service.getBinding();
			Propiedad pass = propiedadDao.findByProp("dev_pass");
			pwd = pass.getValor();
//			log.info("SE CONSULTA A DEV " + pwd);
		}
		return binding;
	}

	private ServicioPorCliente getRuta(Servicio s, int tipo) {
		ServicioPorCliente result = null;
		for (ServicioPorCliente sxc : s.getServiciosPorCliente()) {
			if (sxc.getTipoRuta().getIdTipoRuta() == tipo) {
				result = sxc;
				break;
			}
		}
		return result;
	}

	private Date localDateToDate(LocalDate localDate) {
		ZoneId defaultZoneId = ZoneId.systemDefault();
		return localDate != null ? Date.from(localDate.atStartOfDay(defaultZoneId).toInstant()) : null;
	}

	private String ejecutaServicioSAP_Inter(ServicioInter s, String currency, EmpleadoSAP empleadoSAP,
			ActionCode action) {
		String pedidoCliente = null;
		PedidoClienteRequest pcr = null;
//		ServicioSAP servicioSAP = null;
		init();
		String tipoServicio = calculaTipoServicio(s.getIdServicioStr());
		log.info("[" + s.getIdServicioStr() + "] CREA PEDIDO DE CLIENTE INTER [" + currency + "]");
		try {
			String pNotasGenerales = s.getNotas() != null && s.getNotas().length() > 0 ? s.getNotas() : null;
			String refFactura = s.getRefFacturacion() != null && s.getRefFacturacion().length() > 0
					? s.getRefFacturacion()
					: null;

			pcr = PedidoClienteRequest.builder().buyerId(s.getCliente().getNumeroClienteSAP())
					.actionCode(action.getActionCode()).referenciaFactura(refFactura).notasGenerales(pNotasGenerales)
					.requestName("SERVICIO " + tipoServicio + " FLAM " + s.getIdServicioStr())
					.billTo(s.getCliente().getNumeroClienteSAP()).accountId(s.getCliente().getNumeroClienteSAP())
					.employeeId(empleadoSAP.getIdEmpleado())
					.salesUnit(empleadoSAP.getUnidadVentas() + calculaPrefijoUnidadVentas(s.getIdServicioStr()))
					.pricingTermsCurrency(currency).deliveryTermsPriority("3")
					.ubicacionOrigen(s.getAeropuertoOrigen().getNombre())
					.ubicacionDestino(s.getAeropuertoDestino().getNombre()).shipper(s.getShipper().getNombre())
					.pzasPBPC((s.getPesoBruto() != null ? s.getPesoBruto() : "") + " - "
							+ (s.getPesoCargable() != null ? s.getPesoCargable() : ""))
					.fechaCarga(localDateToDate(s.getFechaSalida())).fechaDestino(localDateToDate(s.getFechaArribo3()))
					.guíaMaster(s.getGmNumGuia()).guiaHouse(s.getGhNumGuia()).noServicio(s.getIdServicioStr()).build();

			List<PedidoClienteItemRequest> items = new ArrayList<PedidoClienteItemRequest>();
			PedidoClienteItemRequest item = null;

			if (s.getCargosOrigen() != null && s.getCargosOrigen().size() > 0) {
				for (CargoOrigen cargoOrigen : s.getCargosOrigen()) {
					// ----
					log.info(cargoOrigen.getItemID() + " -> PC:" + cargoOrigen.getPedidoCliente());
					if (cargoOrigen.getPedidoCliente() != null && cargoOrigen.getPedidoCliente().length() > 0) {
						log.info(cargoOrigen.getPedidoCliente() + " NO SE AGREGA PORQUE YA TIENE PEDIDO DE CLIENTE");

					} else {
						if (cargoOrigen.getVentaMonto() != null
								&& (cargoOrigen.getVentaMonto()).compareTo(BigDecimal.ZERO) > 0
								&& cargoOrigen.getMonedaVenta().getSimbolo().equals(currency)) {
							ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio,
									cargoOrigen.getCargoAereo().getTraduccion());
							log.info("BUSCA SERVICIO SAP:" + tipoServicio + " - "
									+ cargoOrigen.getCargoAereo().getTraduccion());
							if (servicioSAP != null) {
								item = PedidoClienteItemRequest.builder().itemProductId(servicioSAP.getIdServicio())
										.itemID(cargoOrigen.getItemID() + "").itemPrice(cargoOrigen.getVentaMonto())
										.itemCurrency(cargoOrigen.getMonedaVenta().getSimbolo())
										.itemObservaciones(s.getRefFacturacion())
										.itemVendorId(cargoOrigen.getProveedor().getMotivoBloqueo()).build();
								items.add(item);
							} else {
								log.error("No se encontro servicioSAP:" + tipoServicio + " - "
										+ cargoOrigen.getCargoAereo().getTraduccion());
							}
						}
					}
					// ----
				}
			}

			if (s.getCargosDestino() != null && s.getCargosDestino().size() > 0) {
				for (CargoDestino cargoDestino : s.getCargosDestino()) {

					log.info(cargoDestino.getItemID() + " -> PC:" + cargoDestino.getPedidoCliente());
					if (cargoDestino.getPedidoCliente() != null && cargoDestino.getPedidoCliente().length() > 0) {
						log.info(cargoDestino.getPedidoCliente() + " NO SE AGREGA PORQUE YA TIENE PEDIDO DE CLIENTE");

					} else {
						if (cargoDestino.getVentaMonto() != null
								&& (cargoDestino.getVentaMonto()).compareTo(BigDecimal.ZERO) > 0
								&& cargoDestino.getMonedaVenta().getSimbolo().equals(currency)) {
							ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio,
									cargoDestino.getCargoAereo().getTraduccion());
							log.info("BUSCA SERVICIO SAP:" + tipoServicio + " - "
									+ cargoDestino.getCargoAereo().getTraduccion());
							if (servicioSAP != null) {
								item = PedidoClienteItemRequest.builder().itemProductId(servicioSAP.getIdServicio())
										.itemID(cargoDestino.getItemID() + "").itemPrice(cargoDestino.getVentaMonto())
										.itemCurrency(cargoDestino.getMonedaVenta().getSimbolo())
										.itemObservaciones(s.getRefFacturacion())
										.itemVendorId(cargoDestino.getProveedor().getMotivoBloqueo()).build();
								items.add(item);
							} else {
								log.error("No se encontro servicioSAP:" + tipoServicio + " - "
										+ cargoDestino.getCargoAereo().getTraduccion());
							}
						}
					}
				}
			}
			if (items != null && items.size() > 0) {
				soRequest = utility.getSalesOrderRequest(s.getIdServicioStr() + "", pcr, items);

				/****** UserName & Password ******/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/

				result = binding.maintainBundle(soRequest);
				pedidoCliente = utility.leePedidoCliente(s.getIdServicio(), s.getIdServicioStr() + "", currency,
						result);

			} else {
				log.error("NO HAY ITEMS QUE MOSTRAR");
			}
		} catch (

		Exception e) {
			log.error("[" + s.getIdServicioStr() + "] Exception:: " + e.getMessage(), e);
		}
//		log.info("["+s.getIdServicioStr()+"] ============ FIN CREA PEDIDO DE CLIENTE IMPO ============");
		return pedidoCliente;
	}

	private String calculaTipoServicio(String idServicioStr) {
		if (idServicioStr != null) {
			if (idServicioStr.endsWith("AE")) {
				return "AEREO EXPO";
			} else if (idServicioStr.endsWith("AI")) {
				return "AEREO IMPO";
			} else if (idServicioStr.endsWith("ME")) {
				return "MARITIMO EXPO";
			} else if (idServicioStr.endsWith("MI")) {
				return "MARITIMO IMPO";
			} else if (idServicioStr.endsWith("LE")) {
				return "LAND EXPO";
			} else if (idServicioStr.endsWith("LI")) {
				return "LAND IMPO";
			} else {
				return "NACIONAL";
			}

		} else {
			return "NACIONAL";
		}
	}

	private String calculaPrefijoUnidadVentas(String idServicioStr) {
		if (idServicioStr != null) {
			if (idServicioStr.endsWith("AE")) {
				return "-3";
			} else if (idServicioStr.endsWith("AI")) {
				return "-3";
			} else if (idServicioStr.endsWith("ME")) {
				return "-2";
			} else if (idServicioStr.endsWith("MI")) {
				return "-2";
			} else if (idServicioStr.endsWith("LE")) {
				return "-4";
			} else if (idServicioStr.endsWith("LI")) {
				return "-4";
			} else {
				return "-1";
			}

		} else {
			return "-1";
		}
	}

	@Override
	public String actualizaCargoOrigen(ServicioInter s, ActionCode action, EmpleadoSAP empleadoSAP, CargoOrigen cargo) {
		String pedidoCliente = null;
		PedidoClienteRequest pcr = null;
		ServicioSAP servicioSAP = null;
		init();
		String tipoServicio = calculaTipoServicio(s.getIdServicioStr());
		log.info("[" + s.getIdServicioStr() + "] ============ INICIO ACTUALIZACION DE PEDIDO DE CLIENTE ["
				+ cargo.getPedidoCliente() + "]");
		try {

			String pNotasGenerales = s.getNotas() != null && s.getNotas().length() > 0 ? s.getNotas() : null;
			String refFactura = s.getRefFacturacion() != null && s.getRefFacturacion().length() > 0
					? s.getRefFacturacion()
					: null;

			pcr = PedidoClienteRequest.builder().buyerId(s.getCliente().getNumeroClienteSAP())
					.pedidoCliente(cargo.getPedidoCliente()).actionCode(action.getActionCode())
					.referenciaFactura(refFactura).notasGenerales(pNotasGenerales)
					.requestName("SERVICIO " + tipoServicio + " FLAM " + s.getIdServicioStr())
					.billTo(s.getCliente().getNumeroClienteSAP()).accountId(s.getCliente().getNumeroClienteSAP())
					.employeeId(empleadoSAP.getIdEmpleado())
					.salesUnit(empleadoSAP.getUnidadVentas() + calculaPrefijoUnidadVentas(s.getIdServicioStr()))
					.pricingTermsCurrency(cargo.getMonedaVenta().getSimbolo()).deliveryTermsPriority("3")
					.ubicacionOrigen(s.getAeropuertoOrigen().getNombre())
					.ubicacionDestino(s.getAeropuertoDestino().getNombre()).shipper(s.getShipper().getNombre())
					.pzasPBPC((s.getPesoBruto() != null ? s.getPesoBruto() : "") + " - "
							+ (s.getPesoCargable() != null ? s.getPesoCargable() : ""))
					.fechaCarga(localDateToDate(s.getFechaSalida())).fechaDestino(localDateToDate(s.getFechaArribo3()))
					.guíaMaster(s.getGmNumGuia()).guiaHouse(s.getGhNumGuia()).noServicio(s.getIdServicioStr()).build();

			List<PedidoClienteItemRequest> items = new ArrayList<PedidoClienteItemRequest>();
			PedidoClienteItemRequest item = null;

			// for (CargoAereoOrigen cargoOrigen : s.getCargosOrigen()) {
			if (cargo.getVentaMonto() != null && (cargo.getVentaMonto()).compareTo(BigDecimal.ZERO) > 0) {
				servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio, cargo.getCargoAereo().getTraduccion());
				log.info("BUSCA SERVICIO SAP:" + tipoServicio + " - " + cargo.getCargoAereo().getTraduccion());
				if (servicioSAP != null) {
					item = PedidoClienteItemRequest.builder().itemProductId(servicioSAP.getIdServicio())
							.itemPrice(cargo.getVentaMonto()).itemCurrency(cargo.getMonedaVenta().getSimbolo())
							.itemObservaciones(s.getRefFacturacion())
							.itemVendorId(cargo.getProveedor().getMotivoBloqueo() + "").build();
					items.add(item);

				} else {
					log.error("No se encontro servicioSAP:" + tipoServicio + " - "
							+ cargo.getCargoAereo().getTraduccion());
				}
			}
			// }

			if (items != null && items.size() > 0) {
				soRequest = utility.getSalesOrderRequest(s.getIdServicioStr() + "", pcr, items);
				/****** UserName & Password ******/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/

				result = binding.maintainBundle(soRequest);
				pedidoCliente = utility.leePedidoCliente(s.getIdServicio(), s.getIdServicioStr() + "",
						cargo.getMonedaVenta().getSimbolo(), result);
			} else {
				log.error("NO HAY ITEMS QUE MOSTRAR");
			}
		} catch (Exception e) {
			log.error("[" + s.getIdServicioStr() + "]Exception:: " + e.getStackTrace());
			e.printStackTrace();
		}
//		log.info(s.getIdServicioStr() + "", "============ FIN CREA PEDIDO DE CLIENTE IMPO ============");
		return pedidoCliente;
	}

	@Override
	public String actualizaCargoDestino(ServicioInter s, ActionCode action, EmpleadoSAP empleadoSAP,
			CargoDestino cargo) {
		String pedidoCliente = null;
		PedidoClienteRequest pcr = null;
		ServicioSAP servicioSAP = null;
		init();
		String tipoServicio = calculaTipoServicio(s.getIdServicioStr());
		log.info("[" + s.getIdServicioStr() + "] ============ INICIO ACTUALIZACION DE PEDIDO DE CLIENTE ["
				+ cargo.getPedidoCliente() + "]");
		try {

			String pNotasGenerales = s.getNotas() != null && s.getNotas().length() > 0 ? s.getNotas() : null;
			String refFactura = s.getRefFacturacion() != null && s.getRefFacturacion().length() > 0
					? s.getRefFacturacion()
					: null;

			pcr = PedidoClienteRequest.builder().buyerId(s.getCliente().getNumeroClienteSAP())
					.pedidoCliente(cargo.getPedidoCliente()).actionCode(action.getActionCode())
					.referenciaFactura(refFactura).notasGenerales(pNotasGenerales)
					.requestName("SERVICIO " + tipoServicio + " FLAM " + s.getIdServicioStr())
					.billTo(s.getCliente().getNumeroClienteSAP()).accountId(s.getCliente().getNumeroClienteSAP())
					.employeeId(empleadoSAP.getIdEmpleado())
					.salesUnit(empleadoSAP.getUnidadVentas() + calculaPrefijoUnidadVentas(s.getIdServicioStr()))
					.pricingTermsCurrency(cargo.getMonedaVenta().getSimbolo()).deliveryTermsPriority("3")
					.ubicacionOrigen(s.getAeropuertoOrigen().getNombre())
					.ubicacionDestino(s.getAeropuertoDestino().getNombre()).shipper(s.getShipper().getNombre())
					.pzasPBPC((s.getPesoBruto() != null ? s.getPesoBruto() : "") + " - "
							+ (s.getPesoCargable() != null ? s.getPesoCargable() : ""))
					.fechaCarga(localDateToDate(s.getFechaSalida())).fechaDestino(localDateToDate(s.getFechaArribo3()))
					.guíaMaster(s.getGmNumGuia()).guiaHouse(s.getGhNumGuia()).noServicio(s.getIdServicioStr()).build();

			List<PedidoClienteItemRequest> items = new ArrayList<PedidoClienteItemRequest>();
			PedidoClienteItemRequest item = null;

			// for (CargoAereoOrigen cargoOrigen : s.getCargosOrigen()) {
			if (cargo.getVentaMonto() != null && (cargo.getVentaMonto()).compareTo(BigDecimal.ZERO) > 0) {
				servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio, cargo.getCargoAereo().getTraduccion());
				log.info("BUSCA SERVICIO SAP:" + tipoServicio + " - " + cargo.getCargoAereo().getTraduccion());
				if (servicioSAP != null) {
					item = PedidoClienteItemRequest.builder().itemProductId(servicioSAP.getIdServicio())
							.itemPrice(cargo.getVentaMonto()).itemCurrency(cargo.getMonedaVenta().getSimbolo())
							.itemObservaciones(s.getRefFacturacion())
							.itemVendorId(cargo.getProveedor().getMotivoBloqueo() + "").build();
					items.add(item);

				} else {
					log.error("No se encontro servicioSAP:" + tipoServicio + " - "
							+ cargo.getCargoAereo().getTraduccion());
				}
			}
			// }

			if (items != null && items.size() > 0) {
				soRequest = utility.getSalesOrderRequest(s.getIdServicioStr() + "", pcr, items);
				/****** UserName & Password ******/
				Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
				reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
				reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);
				/************************************************/

				result = binding.maintainBundle(soRequest);
				pedidoCliente = utility.leePedidoCliente(s.getIdServicio(), s.getIdServicioStr() + "",
						cargo.getMonedaVenta().getSimbolo(), result);
			} else {
				log.error("NO HAY ITEMS QUE MOSTRAR");
			}
		} catch (Exception e) {
			log.error("[" + s.getIdServicioStr() + "]Exception:: " + e.getStackTrace());
		}
//		log.info(s.getIdServicioStr() + "", "============ FIN CREA PEDIDO DE CLIENTE IMPO ============");
		return pedidoCliente;
	}

	public String consultarStatusPedido(String posicion, String pedidoCliente) {
		String respuesta = "No se encontro la posicion [" + posicion + "]";
		try {
			QuerySalesOrderIn bindingConsulta = obtieneBindingConsulta();
			Map<String, Object> reqContext = ((BindingProvider) bindingConsulta).getRequestContext();
			reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
			reqContext.put(BindingProvider.PASSWORD_PROPERTY, pwd);

			SalesOrderByElementsQueryMessageSync request = new SalesOrderByElementsQueryMessageSync();
			SalesOrderByElementsQuerySelectionByElements selection = new SalesOrderByElementsQuerySelectionByElements();

			SalesOrderByElementsQuerySelectionByID selectionById = new SalesOrderByElementsQuerySelectionByID();
			selectionById.setInclusionExclusionCode("I");
			selectionById.setIntervalBoundaryTypeCode("1");
			BusinessTransactionDocumentID docId = new BusinessTransactionDocumentID();
			docId.setValue(pedidoCliente);
			selectionById.setLowerBoundaryID(docId);
			selection.getSelectionByID().add(selectionById);
			request.setSalesOrderSelectionByElements(selection);

			QueryProcessingConditions processingConditions = new QueryProcessingConditions();
			processingConditions.setQueryHitsMaximumNumberValue(10);
			processingConditions.setQueryHitsUnlimitedIndicator(false);

			request.setProcessingConditions(processingConditions);

			SalesOrderByElementsResponseMessageSync response = bindingConsulta.findByElements(request);
			ResponseProcessingConditions conditions = response.getProcessingConditions();

			int resultados = conditions.getReturnedQueryHitsNumberValue();
			if (resultados > 0) {
				List<SalesOrderByElementsResponse> pedidos = response.getSalesOrder();
				for (SalesOrderByElementsResponse pedido : pedidos) {
//					System.out.log.infoln("->" + pedido.getID().getValue());

					for (SalesOrderByElementsResponseItem item : pedido.getItem()) {
//						System.out.log.infoln("ID ----->" + item.getID());
						SalesOrderByElementsResponseItemStatus status = item.getStatus();
						if (item.getID().equalsIgnoreCase(posicion)) {
//							System.out.log.infoln(
//									"InvoiceProcessingStatusCode ----->" + status.getInvoiceProcessingStatusCode());
//							System.out.log.infoln("InvoiceProcessingStatusName ----->"
//									+ status.getInvoiceProcessingStatusName().getValue());

							if (!status.getInvoiceProcessingStatusCode().equalsIgnoreCase("3")) {
								respuesta = "true";
							} else {
								respuesta = "No se puede cambiar la posicion del pedido " + pedidoCliente
										+ "\n - Estatus[" + status.getInvoiceProcessingStatusName().getValue() + "]";
							}
							break;
						}
					}

				}
			} else {
				respuesta = "No se encontraron resultados para el pedido de cliente " + pedidoCliente;
				log.error("NO SE ENCONTRARON RESULTADOS DE PEDIDO DE CLIENTE " + pedidoCliente);
			}
		} catch (Exception e) {
			log.error("Error al consultar estatus del pedido " + pedidoCliente, e);
			respuesta = "Error al consultar estatus del pedido " + pedidoCliente + " [" + e.getMessage() + "]";
		}
		return respuesta;
	}

}
