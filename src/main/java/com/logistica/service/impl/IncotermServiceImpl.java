package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IIncotermDao;
import com.logistica.model.Incoterm;
import com.logistica.service.IIncotermService;

@Service
public class IncotermServiceImpl implements IIncotermService {

	@Autowired
	IIncotermDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Incoterm> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Incoterm> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Incoterm findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Incoterm save(Incoterm incoterm) {
		return dao.save(incoterm);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Incoterm incoterm = dao.findById(id).orElse(null);
		if (incoterm != null) {
			dao.delete(incoterm);
		}
	}

}
