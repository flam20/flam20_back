package com.logistica.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IProveedorDao;
import com.logistica.model.Proveedor;
import com.logistica.service.IProveedorService;

@Service
public class ProveedorServiceImpl implements IProveedorService {

	@Autowired
	private IProveedorDao dao;

	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(readOnly = true)
	public Page<Proveedor> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Proveedor findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Proveedor save(Proveedor proveedor) {
		return dao.save(proveedor);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Proveedor proveedor = dao.findById(id).orElse(null);
		if (proveedor != null) {
			dao.delete(proveedor);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findProveedoresDisponibles() {
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findAllUnlocked() {
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findAllUnlockedAereos() {
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findFirst10ByRazonComercial(String razonComercial) {
		razonComercial = razonComercial.replace("(amp)", "&");
		return dao.findFirst10ByRazonComercialContainingIgnoreCaseAndBloqueado(razonComercial, Boolean.FALSE);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> search(String[] giros, String[] sociosComerciales, String[] coberturas, String[] patios,
			String[] codigosHazmat, String[] servicios, String[] unidadesAutAero, String[] puertos, String[] fronteras,
			String[] recursos, String[] certificacionesEmpresa, String[] certificacionesOperador, String[] equipos) {
		List<Proveedor> listaProveedores = new ArrayList<>();
		StringBuilder sSql = new StringBuilder();
		sSql.append("SELECT DISTINCT p.id_proveedor, ");
		sSql.append("p.razon_social, ");
		sSql.append("p.razon_comercial, ");
		sSql.append("p.termino_busqueda, ");
		sSql.append("p.linea_credito, ");
		sSql.append("p.plazo, ");
		sSql.append("p.id_evaluacion, ");
		sSql.append("p.motivo_bloqueo ");
		sSql.append("FROM proveedores p ");
		sSql.append("LEFT JOIN evaluacion_giro eg ON eg.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_socios_comerciales esc ON esc.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_cobertura_nacional ecn ON ecn.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_patios_operacion epo ON epo.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_codigos_hazmat ech ON ech.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_servicios es ON es.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_unidades_aut_aero euaa ON euaa.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_puertos_arrastre epa ON epa.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_fronteras_cruce efc ON efc.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluacion_recursos_seguridad ers ON ers.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append(
				"LEFT JOIN evaluacion_certificaciones_empresa ece ON ece.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append(
				"LEFT JOIN evaluacion_certificado_operadores eco ON eco.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN evaluaciones_equipos ee ON ee.evaluacion_id_evaluacion = p.id_evaluacion ");
		sSql.append("LEFT JOIN equipos e ON e.id_equipo = ee.equipos_id_equipo ");

		if ((giros != null && giros.length > 0) || (sociosComerciales != null && sociosComerciales.length > 0)
				|| (coberturas != null && coberturas.length > 0) || (patios != null && patios.length > 0)
				|| (codigosHazmat != null && codigosHazmat.length > 0) || (servicios != null && servicios.length > 0)
				|| (unidadesAutAero != null && unidadesAutAero.length > 0) || (puertos != null && puertos.length > 0)
				|| (fronteras != null && fronteras.length > 0) || (recursos != null && recursos.length > 0)
				|| (certificacionesEmpresa != null && certificacionesEmpresa.length > 0)
				|| (certificacionesOperador != null && certificacionesOperador.length > 0)
				|| (equipos != null && equipos.length > 0)) {
			sSql.append(" WHERE ");

			if (giros != null && giros.length > 0) {
				sSql.append("(");
				for (String giro : giros) {
					sSql.append("eg.giro like '%").append(giro).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (sociosComerciales != null && sociosComerciales.length > 0) {
				sSql.append("(");
				for (String socio : sociosComerciales) {
					sSql.append("esc.socios_comerciales like '%").append(socio).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (coberturas != null && coberturas.length > 0) {
				sSql.append("(");
				for (String cobertura : coberturas) {
					sSql.append("ecn.cobertura_nacional like '%").append(cobertura).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (patios != null && patios.length > 0) {
				sSql.append("(");
				for (String patio : patios) {
					sSql.append("epo.patios_operacion like '%").append(patio).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (codigosHazmat != null && codigosHazmat.length > 0) {
				sSql.append("(");
				for (String codigo : codigosHazmat) {
					sSql.append("ech.codigos_hazmat like '%").append(codigo).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (servicios != null && servicios.length > 0) {
				sSql.append("(");
				for (String servicio : servicios) {
					sSql.append("es.servicios like '%").append(servicio).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (unidadesAutAero != null && unidadesAutAero.length > 0) {
				sSql.append("(");
				for (String unidad : unidadesAutAero) {
					sSql.append("euaa.unidades_aut_aero like '%").append(unidad).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (puertos != null && puertos.length > 0) {
				sSql.append("(");
				for (String puerto : puertos) {
					sSql.append("epa.puertos_arrastre like '%").append(puerto).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (fronteras != null && fronteras.length > 0) {
				sSql.append("(");
				for (String frontera : fronteras) {
					sSql.append("efc.fronteras_cruce like '%").append(frontera).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (recursos != null && recursos.length > 0) {
				sSql.append("(");
				for (String recurso : recursos) {
					sSql.append("ers.recursos_seguridad like '%").append(recurso).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (certificacionesEmpresa != null && certificacionesEmpresa.length > 0) {
				sSql.append("(");
				for (String certificacion : certificacionesEmpresa) {
					sSql.append("ece.certificaciones_empresa like '%").append(certificacion).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (certificacionesOperador != null && certificacionesOperador.length > 0) {
				sSql.append("(");
				for (String certificado : certificacionesOperador) {
					sSql.append("eco.certificado_operadores like '%").append(certificado).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}
			
			if (equipos != null && equipos.length > 0) {
				sSql.append("(");
				for (String equipo : equipos) {
					sSql.append("e.descripcion like '%").append(equipo).append("%' or ");
				}
				if (sSql.toString().endsWith("or ")) {
					sSql = new StringBuilder(sSql.substring(0, sSql.length() - 3));
				}
				sSql.append(") and ");
			}

			if (sSql.toString().endsWith("WHERE ")) {
				sSql = new StringBuilder(sSql.substring(0, sSql.length() - 6));
			}

			if (sSql.toString().endsWith("and ")) {
				sSql = new StringBuilder(sSql.substring(0, sSql.length() - 4));
			}
		}

		Query query = em.createNativeQuery(sSql.toString());

		List<Object[]> proveedores = query.getResultList();

		for (Object[] p : proveedores) {
			Proveedor nuevoProveedor = new Proveedor();
			BigInteger idProv = (BigInteger) p[0];
			nuevoProveedor.setIdProveedor(idProv.longValue());
			nuevoProveedor.setRazonSocial((String) p[1]);
			nuevoProveedor.setRazonComercial((String) p[2]);
			nuevoProveedor.setTerminoBusqueda((String) p[3]);
			nuevoProveedor.setLineaCredito((BigDecimal) p[4]);
			nuevoProveedor.setPlazo((BigDecimal) p[5]);
			BigInteger idEval = new BigInteger( p[6].toString());
			nuevoProveedor.setIdEvaluacion(idEval.longValue());
			nuevoProveedor.setMotivoBloqueo((String) p[7]);
			listaProveedores.add(nuevoProveedor);
		}

		return listaProveedores;
	}

}
