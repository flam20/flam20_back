package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IConsignatarioDao;
import com.logistica.model.Consignatario;
import com.logistica.service.IConsignatarioService;

@Service
public class ConsignatarioServiceImpl implements IConsignatarioService {

	@Autowired
	IConsignatarioDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Consignatario> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Consignatario> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Consignatario findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Consignatario save(Consignatario consignatario) {
		return dao.save(consignatario);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Consignatario consignatario = dao.findById(id).orElse(null);
		if (consignatario != null) {
			dao.delete(consignatario);
		}
	}

}
