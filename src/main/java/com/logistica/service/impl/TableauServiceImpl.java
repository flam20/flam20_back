package com.logistica.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.logistica.service.ITableauService;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

@Service
public class TableauServiceImpl implements ITableauService {

	private String secret = "/wFs/bnfinlmKYuH6CDlryqbj+h8ygV/4mxCojQzis4=";
	private String kid = "4ef59b66-039b-4ca6-a542-2b620c39d1a8";
	private String clientId = "95d7fb0e-f2b1-402d-b256-62397dd4d56e";
	private List<String> scopes = new ArrayList<>(Arrays.asList("tableau:views:embed"));
	static Logger logger = Logger.getLogger(TableauServiceImpl.class.getName());
	@Override
	public String getToken(String username) throws JOSEException {
		logger.info("USUARIO TOKEN "+username);
		JWSSigner signer = new MACSigner(secret);
		JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.HS256).keyID(kid).customParam("iss", clientId).build();
		JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
				.issuer(clientId)
				.expirationTime(new Date(new Date().getTime() + 60 * 1000)) //expires in 1 minute
				.jwtID(UUID.randomUUID().toString())
				.audience("tableau")
				.subject(username)
				.claim("scp", scopes)
				.build();
		SignedJWT signedJWT = new SignedJWT(header, claimsSet);
		signedJWT.sign(signer);
		String token = signedJWT.serialize();
		logger.info("OBTIENE TOKEN "+token);
		return token;
	}

}
