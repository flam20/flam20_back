package com.logistica.service.impl.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logistica.controller.ServicioInterRestController;
import com.logistica.dao.IBitacoraSapDao;
import com.logistica.dao.IProveedorDao;
import com.logistica.dao.IServicioDao;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.PedidoCompraRequest;
import com.logistica.model.Proveedor;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioPorCliente;
import com.logistica.sap.pedidoCompra.Amount;
import com.logistica.sap.pedidoCompra.BusinessTransactionDocumentID;
import com.logistica.sap.pedidoCompra.BusinessTransactionDocumentTypeCode;
import com.logistica.sap.pedidoCompra.LOCALNORMALISEDDateTime;
import com.logistica.sap.pedidoCompra.LocationID;
import com.logistica.sap.pedidoCompra.Log;
import com.logistica.sap.pedidoCompra.LogItem;
import com.logistica.sap.pedidoCompra.MaintenanceAccountingCodingBlockDistribution;
import com.logistica.sap.pedidoCompra.MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment;
import com.logistica.sap.pedidoCompra.PartyID;
import com.logistica.sap.pedidoCompra.PartyKey;
import com.logistica.sap.pedidoCompra.Price;
import com.logistica.sap.pedidoCompra.ProcurementDocumentItemFollowUpDelivery;
import com.logistica.sap.pedidoCompra.ProcurementDocumentItemFollowUpInvoice;
import com.logistica.sap.pedidoCompra.ProductID;
import com.logistica.sap.pedidoCompra.ProductKey;
import com.logistica.sap.pedidoCompra.PurchaseOrderBundleMaintainRequestMessageSync;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainConfirmationBundle;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainConfirmationBundleMessageSync;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundle;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleItem;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleItemLocation;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleItemProduct;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleParty;
import com.logistica.sap.pedidoCompra.Quantity;
import com.logistica.sap.pedidoCompra.UPPEROPENLOCALNORMALISEDDateTimePeriod;

@Component
public class PedidoCompraUtil {
	static Logger log = Logger.getLogger(PedidoCompraUtil.class.getName());
	private static final String cProductTypeCode = "2";
	private static final String cProductIdentifierTypeCode = "1";
	private static final String cTypeCode = "19"; // SIEMPRE 19
	// private static final String cQuantityUnitCode = "EA";// SIEMPRE EA
	private static final String cQuantity = "1";// SIEMPRE 1
	private static final String cShipToLocation = "MLG1000"; // SIEMPRE MLG1000
	private static final String cCompanyId = "MLG1000";// SIEMPRE MLG1000
	private static final String cPurchasingId = "MLG1405";// SIEMPRE ES MLG1405
	private static final String cTextTypeCode = "10011";

//	@Autowired
//	ServicioSAPDao servicioSAPDao;

	String tipoServicio;

	@Autowired
	IServicioDao servicioDao;

	@Autowired
	IProveedorDao proveedorDao;

	@Autowired
	IBitacoraSapDao bitacoraSAPDao;

	public PurchaseOrderBundleMaintainRequestMessageSync getTerrestreRequest(PedidoCompraRequest pcr, Servicio s,
			Long idProveedor, int currency) {

//		log.info("[" + s.getIdServicio() + "] ARMA REQUEST");
		PurchaseOrderBundleMaintainRequestMessageSync puRequest = new PurchaseOrderBundleMaintainRequestMessageSync();
		PurchaseOrderMaintainRequestBundleItem item;
		try {

			PurchaseOrderMaintainRequestBundle pr = new PurchaseOrderMaintainRequestBundle();
			pr.setActionCode(pcr.getPoActionCode());
			if (pcr.getPoActionCode().equals("04")) {
				log.info("[" + s.getIdServicioStr() + "] Actualizacion de Pedido de compra " + s.getOrdenCompra());
				BusinessTransactionDocumentID documentID = new BusinessTransactionDocumentID();

				documentID.setValue(s.getOrdenCompra());
//				pr.setPurchaseRequestID(documentID);
				pr.setPurchaseOrderID(documentID);
			}

			BusinessTransactionDocumentTypeCode btTypeCode = new BusinessTransactionDocumentTypeCode();
			btTypeCode.setValue("001");
			pr.setBusinessTransactionDocumentTypeCode(btTypeCode);

			// CURRENCY
			if (currency == 1)
				pr.setCurrencyCode("USD");
			if (currency == 5)
				pr.setCurrencyCode("MXN");
			// Buyer Party
			pr.setBuyerParty(getParty("MLG1000"));

			// Seller Party (Proveedor)
//			pr.setSellerParty(getParty("T01020"));
			Proveedor p = proveedorDao.getOne(idProveedor);
			pr.setSellerParty(getParty(p.getMotivoBloqueo()));

			// Bill To
			pr.setBillToParty(getParty("MLG1000"));

			// Employee
			pr.setEmployeeResponsibleParty(getParty("1010"));

			pr.setOrderPurchaseOrderActionIndicator(true);

			// ******* RUTAS
			for (ServicioPorCliente ruta : s.getServiciosPorCliente()) {
//				if (ruta.getCosto() != null && (ruta.getCosto().compareTo(BigDecimal.ZERO) > 0) && !ruta.isDisabled()) {
				if (ruta.getCosto() != null && ruta.getCosto().compareTo(BigDecimal.ZERO) > 0
						&& ruta.getMonedaCosto().getIdMoneda() == currency
						&& ruta.getProveedor().getIdProveedor().compareTo(idProveedor) == 0) {
					String tipoServicio = ruta.getUbicacion().getPais().equals("MEXICO") ? "NACIONAL" : "EXPO";
					String pCargoFLAM = (ruta.getTipoRuta().getIdTipoRuta().compareTo(new Long(2)) == 0) ? "CRUCE"
							: "FLETE";
					String texto = (ruta.getTipoRuta().getIdTipoRuta().compareTo(new Long(3)) == 0)
							? ruta.getReferenciaCliente()
							: null;

//					ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio, pCargoFLAM,
//							s.getIdServicioAnual() + "");
//					if (servicioSAP != null) {
					pcr.setItemID(ruta.getTipoRuta().getIdTipoRuta());
//						pcr.setItemProductId(servicioSAP.getIdServicio());SERV_LOG_N
					pcr.setItemProductId("SERV_LOG_N");
					pcr.setItemDeliveryStartTime(new Date());
					pcr.setItemCurrency(ruta.getMonedaCosto().getSimbolo());
					pcr.setItemPriceValue(ruta.getCosto());
					pcr.setItemSupplierId(ruta.getProveedor().getMotivoBloqueo());// CORREGIR CAMPO MOTIVO BLOQUEO
					pcr.setItemTextValue(null);

					item = getPurchaseItem(pcr);
					pr.getItem().add(item);
//					}
				}
			}

			if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0) {
				for (CargoPorServicio cargo : s.getCargosPorServicio()) {
					if (cargo.getCosto() != null && (cargo.getCosto().compareTo(BigDecimal.ZERO) > 0)
							&& cargo.getMonedaCosto().getIdMoneda() == currency
							&& cargo.getProveedor().getIdProveedor().compareTo(idProveedor) == 0) {
//					ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo("NACIONAL",
//							cargo.getCargo().getDescCorta(), s.getIdServicioAnual() + "");
//					if (servicioSAP != null && !servicioSAP.getIdServicio().startsWith("NOTA")) {
						pcr.setItemID(cargo.getIdCargoServicio());
						pcr.setItemProductId("SERV_LOG_N");
						pcr.setItemDeliveryStartTime(new Date());
						pcr.setItemCurrency(cargo.getMonedaCosto().getSimbolo());
						pcr.setItemPriceValue(cargo.getCosto());
						pcr.setItemSupplierId(cargo.getProveedor().getMotivoBloqueo());
						pcr.setItemTextValue(cargo.getObservaciones());
						item = getPurchaseItem(pcr);
						pr.getItem().add(item);
//					}
					}
				}
			}
			// ========================SE AÑADE EL REQUEST
			puRequest.getPurchaseOrderMaintainBundle().add(pr);

		} catch (Exception e) {
			log.error("[" + pcr.getIdServicio() + "] Error al generar request:: " + e.getMessage(), e);
		}

		return puRequest;
	}

	private PurchaseOrderMaintainRequestBundleItem getPurchaseItem(PedidoCompraRequest re) {

		PurchaseOrderMaintainRequestBundleItem poItem = new PurchaseOrderMaintainRequestBundleItem();
		try {

			poItem.setActionCode(re.getItemActionCode());
			poItem.setItemID(re.getItemID() + "");

			// CANCELA ITEM
			if (re.isCancelItem()) {
//				item.setCancelOpenQuantityActionIndicator(true);
				poItem.setCancelItemPurchaseOrderActionIndicator(true);
			}

			// Quantity
			Quantity quantityValue = new Quantity();
			quantityValue.setUnitCode("Z01");
			quantityValue.setValue(new BigDecimal(1));
			poItem.setQuantity(quantityValue);

			// Delivery Period
			UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod = new UPPEROPENLOCALNORMALISEDDateTimePeriod();
			LOCALNORMALISEDDateTime startDateTime = new LOCALNORMALISEDDateTime();

			GregorianCalendar gc = new GregorianCalendar();
			// gc.setTime(re.getItemDeliveryStartTime());
			gc.setTime(new Date());
			XMLGregorianCalendar periodStartTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			startDateTime.setValue(periodStartTime);
			startDateTime.setTimeZoneCode("EST");
			deliveryPeriod.setStartDateTime(startDateTime);
			deliveryPeriod.setEndDateTime(startDateTime);
			poItem.setDeliveryPeriod(deliveryPeriod);

			// Price
			Price price = new Price();
			Amount amountValue = new Amount();
			amountValue.setCurrencyCode(re.getItemCurrency());
			amountValue.setValue(re.getItemPriceValue());
			price.setAmount(amountValue);
			poItem.setListUnitPrice(price);

			// Followed UP delivery
			ProcurementDocumentItemFollowUpDelivery fUpDelivery = new ProcurementDocumentItemFollowUpDelivery();
			fUpDelivery.setRequirementCode("01");
			fUpDelivery.setEmployeeTimeConfirmationRequiredIndicator(true);
			poItem.setFollowUpDelivery(fUpDelivery);

			// Followed UP Invoice
			ProcurementDocumentItemFollowUpInvoice fUpInvoice = new ProcurementDocumentItemFollowUpInvoice();
			fUpInvoice.setRequirementCode("01");
			fUpInvoice.setDeliveryBasedInvoiceVerificationIndicator(true);
			poItem.setFollowUpInvoice(fUpInvoice);

			// Item Product
			PurchaseOrderMaintainRequestBundleItemProduct prod = new PurchaseOrderMaintainRequestBundleItemProduct();
			prod.setActionCode(re.getItemActionCode());
			prod.setCashDiscountDeductibleIndicator(true);
			ProductKey prodKey = new ProductKey();
			prodKey.setProductTypeCode("1");
			prodKey.setProductIdentifierTypeCode("1");
			ProductID prodID = new ProductID();
			prodID.setValue(re.getItemProductId());
			prodKey.setProductID(prodID);
			prod.setProductKey(prodKey);

			poItem.setItemProduct(prod);

			// Ship To Location
			PurchaseOrderMaintainRequestBundleItemLocation shipToLocation = new PurchaseOrderMaintainRequestBundleItemLocation();
			LocationID location = new LocationID();
			location.setValue(cShipToLocation);
			shipToLocation.setLocationID(location);
			poItem.setShipToLocation(shipToLocation);

			// AccountingCodingBlockDistribution

			MaintenanceAccountingCodingBlockDistribution blockDis = new MaintenanceAccountingCodingBlockDistribution();
			blockDis.setActionCode("01");
			MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment assignment = new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment();
			assignment.setActionCode("01");
			assignment.setCostCentreID("MLGV1114-1");
			blockDis.getAccountingCodingBlockAssignment().add(assignment);
			poItem.setItemAccountingCodingBlockDistribution(blockDis);

			// ============================== FIN item

			// ProductKeyItem
//			PurchaseRequestMaintainRequestProductKeyItem productKeyItem = new PurchaseRequestMaintainRequestProductKeyItem();
//			productKeyItem.setProductTypeCode(cProductTypeCode);
//			productKeyItem.setProductIdentifierTypeCode(cProductIdentifierTypeCode);
//			ProductID productId = new ProductID();
//			productId.setValue(re.getItemProductId());
//			productKeyItem.setProductID(productId);
//			item.setProductKeyItem(productKeyItem);
//
//			// TypeCode
//			item.setTypeCode(cTypeCode);
//			
//
//			// Quantity
//			Quantity quantity = new Quantity();
//			// quantity.setUnitCode(cQuantityUnitCode);
//			quantity.setValue(new BigDecimal(cQuantity));
//			item.setQuantity(quantity);
//
//			// DeliveryPeriod
//			UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod = new UPPEROPENLOCALNORMALISEDDateTimePeriod();
//			LOCALNORMALISEDDateTime startDateTime = new LOCALNORMALISEDDateTime();
//
//			GregorianCalendar gc = new GregorianCalendar();
//			gc.setTime(re.getItemDeliveryStartTime());
//			XMLGregorianCalendar periodStartTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
//			startDateTime.setValue(periodStartTime);
//
//			deliveryPeriod.setStartDateTime(startDateTime);
//			deliveryPeriod.setEndDateTime(startDateTime);
//			item.setDeliveryPeriod(deliveryPeriod);
//
//			// ShipToLocationID
//			LocationID locationId = new LocationID();
//			locationId.setValue(cShipToLocation);
//			item.setShipToLocationID(locationId);
//
//			// ListUnitPrice
//			Price unitPrice = new Price();
//			Amount amount = new Amount();
//			amount.setCurrencyCode(re.getItemCurrency());
//			amount.setValue(re.getItemPriceValue());
//			unitPrice.setAmount(amount);
//			item.setListUnitPrice(unitPrice);
//
//			// CompanyIDParty
//			PurchaseRequestMaintainRequestBundleItemParty companyId = new PurchaseRequestMaintainRequestBundleItemParty();
//			PartyID companyIdParty = new PartyID();
//			companyIdParty.setValue(cCompanyId);
//			companyId.setPartyID(companyIdParty);
//			item.setCompanyIDParty(companyId);
//
//			// TEXT COLLECTION
//			if (re.getItemTextValue() != null && re.getItemTextValue().length() > 0) {
//				MaintenanceTextCollection textCollectionValue = new MaintenanceTextCollection();
//				MaintenanceTextCollectionText text = new MaintenanceTextCollectionText();
//				TextCollectionTextTypeCode typeCode = new TextCollectionTextTypeCode();
//				typeCode.setValue(cTextTypeCode);
//				text.setTypeCode(typeCode);
//
//				MaintenanceTextCollectionTextTextContent textContent = new MaintenanceTextCollectionTextTextContent();
//				Text value = new Text();
//				value.setValue(re.getItemTextValue());
//				textContent.setText(value);
//				text.setTextContent(textContent);
//				text.setLanguageCode("ES");
//				textCollectionValue.getText().add(text);
//				item.setTextCollection(textCollectionValue);
//			}
//
//			// BuyerResponsibleIDParty
//			// PurchaseRequestMaintainRequestBundleItemParty buyerId = new
//			// PurchaseRequestMaintainRequestBundleItemParty();
//			// PartyID buyerIdParty = new PartyID();
//			// buyerIdParty.setValue(pBuyerId);
//			// buyerId.setPartyID(buyerIdParty);
//			// item.setBuyerResponsibleIDParty(buyerId);
//
//			// PurchasingUnitIDParty
//			PurchaseRequestMaintainRequestBundleItemParty purchasingId = new PurchaseRequestMaintainRequestBundleItemParty();
//			PartyID purchasingIdParty = new PartyID();
//			purchasingIdParty.setValue(cPurchasingId);
//			purchasingId.setPartyID(purchasingIdParty);
//			item.setPurchasingUnitIDParty(purchasingId);
//
//			// SupplierIDParty
//			PurchaseRequestMaintainRequestBundleItemParty supplierId = new PurchaseRequestMaintainRequestBundleItemParty();
//			PartyID supplierIdParty = new PartyID();
//			supplierIdParty.setValue(re.getItemSupplierId());
//			supplierId.setPartyID(supplierIdParty);
//			item.setSupplierIDParty(supplierId);

			// PARAMETROS SIMPLES
			poItem.setNdeservicio2(re.getItemServicioFLAM());
			// item.setConsignatariosS(re.getItemConsignatarios());
			// SE CAMBIA
//			item.setConsignatarioCruceS(re.getItemConsignatarioCruce());
//			item.setConsignatarioDestino1(re.getItemConsignatarioDestino());
			poItem.setConsignatariosS(re.getItemConsignatarioDestino());

			poItem.setFechaCargaS(formatDate(re.getItemFechaCarga()));
			if (re.getItemFechaCruce() != null) {
				poItem.setFechaCruceS(formatDate(re.getItemFechaCruce()));
			}
			poItem.setFechaDestinoS(formatDate(re.getItemFechaDestino()));
			if (re.getItemFechaAgenteAduanal() != null) {
				poItem.setFechadeAgenteAduanalS(formatDate(re.getItemFechaAgenteAduanal()));
			}
			poItem.setGuaHouseS(re.getItemGuiaHouse());
			poItem.setGuaMasterS(re.getItemGuiaMaster());
			poItem.setNmerodeequipodecargaS(re.getItemNumeroEquipoCarga());
			poItem.setPzsPBPCS(re.getItemPzsPBPCS());
			poItem.setRequierePODS(re.getItemRequierePOD());
			poItem.setShipperS(re.getItemShipper());
			poItem.setTipodeequipoS(re.getItemTipoEquipo());

			poItem.setUbicacinCruceS(re.getItemUbicacionCruce());
			poItem.setUbicacinDestinoS(re.getItemUbicacionDestino());
			poItem.setUbicacinOrigenS(re.getItemUbicacionOrigen());

			poItem.setUnidaddeventaS(re.getItemUnidadVentas());

		} catch (Exception e) {
			log.error("Error al armar request Pedido de Compra::" + e, e);
		}

		return poItem;
	}

	public String leeOrdenCompra(String idServicio, PurchaseOrderMaintainConfirmationBundleMessageSync r) {
		String salida = "";
		String idOrdenCompra = null;
		log.info("[" + idServicio + "] Pedido de compra");
		try {
			if (r.getPurchaseOrder() != null && r.getPurchaseOrder().size() > 0) {
				for (PurchaseOrderMaintainConfirmationBundle soResult : r.getPurchaseOrder()) {
					BusinessTransactionDocumentID id = soResult.getBusinessTransactionDocumentID();
					idOrdenCompra = id.getValue();
					salida = " - Pedido de compra [" + idOrdenCompra + "]";
				}
			} else {
				salida = " - No se generó Pedido de compra";
			}
			log.info("[" + idServicio + "] - " + salida);
			Log loge = r.getLog();
			String notas = "";
			for (LogItem logItem : loge.getItem()) {
				log.info("[" + idServicio + "] \t Nota -> " + logItem.getNote());
				notas += "- " + logItem.getNote() + "\n";
			}

//			BitacoraSap t = new BitacoraSap(idServicio, "PEDIDO DE COMPRA", notas, idOrdenCompra);
//			t.setControlAcceso(ControlAccesoUtil.generarControlAccesoCreacion());
//			bitacoraSAPDao.save(t);
		} catch (Exception e) {
			log.error("[" + idServicio + "] Error al leer la respuesta de SAP para pedido de compra:" + e.getMessage(),
					e);
		}
//		log.info(idServicio + " ---------- FIN RESPUESTA SAP ORDEN DE COMPRA---------- ");
		return idOrdenCompra;
	}

	public String calculaTipoServicio(String idServicioStr) {
		if (idServicioStr != null) {
			if (idServicioStr.endsWith("AE")) {
				return "AEREO EXPO";
			} else if (idServicioStr.endsWith("AI")) {
				return "AEREO IMPO";
			} else if (idServicioStr.endsWith("ME")) {
				return "MARITIMO EXPO";
			} else if (idServicioStr.endsWith("MI")) {
				return "MARITIMO IMPO";
			} else if (idServicioStr.endsWith("LE")) {
				return "LAND EXPO";
			} else if (idServicioStr.endsWith("LI")) {
				return "LAND IMPO";
			} else {
				return "NACIONAL";
			}

		} else {
			return "NACIONAL";
		}
	}

	private static XMLGregorianCalendar formatDate(Date date) {
		String FORMATER = "yyyy-MM-dd";
		DateFormat format = new SimpleDateFormat(FORMATER);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
		} catch (Exception e) {
			return null;
		}

	}

//	public void log.info(String idServicio, String cadena) {
//		log.info("[" + idServicio + "]  " + cadena);
//	}

	public String calculaPrefijoUnidadVentas(String idServicioStr) {
		if (idServicioStr != null) {
			if (idServicioStr.endsWith("AE")) {
				return "-3";
			} else if (idServicioStr.endsWith("AI")) {
				return "-3";
			} else if (idServicioStr.endsWith("ME")) {
				return "-2";
			} else if (idServicioStr.endsWith("MI")) {
				return "-2";
			} else if (idServicioStr.endsWith("LE")) {
				return "-4";
			} else if (idServicioStr.endsWith("LI")) {
				return "-4";
			} else {
				return "-1";
			}

		} else {
			return "-1";
		}
	}

	private PurchaseOrderMaintainRequestBundleParty getParty(String value) {
		PurchaseOrderMaintainRequestBundleParty party = new PurchaseOrderMaintainRequestBundleParty();
		PartyKey partyKey = new PartyKey();
		PartyID partyID = new PartyID();
		partyID.setValue(value);
		partyKey.setPartyID(partyID);
		party.setPartyKey(partyKey);
		return party;
	}
}
