package com.logistica.service.impl.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logistica.dao.IBitacoraSapDao;
import com.logistica.dao.IServicioDao;
import com.logistica.dao.IServicioSAPDao;
import com.logistica.model.BitacoraSap;
import com.logistica.model.CargoDestino;
import com.logistica.model.CargoOrigen;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.OrdenCompraRequest;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioPorCliente;
import com.logistica.model.ServicioSAP;
import com.logistica.sap.ordenCompra.Amount;
import com.logistica.sap.ordenCompra.BusinessTransactionDocumentID;
import com.logistica.sap.ordenCompra.LOCALNORMALISEDDateTime;
import com.logistica.sap.ordenCompra.LocationID;
import com.logistica.sap.ordenCompra.Log;
import com.logistica.sap.ordenCompra.LogItem;
import com.logistica.sap.ordenCompra.MaintenanceTextCollection;
import com.logistica.sap.ordenCompra.MaintenanceTextCollectionText;
import com.logistica.sap.ordenCompra.MaintenanceTextCollectionTextTextContent;
import com.logistica.sap.ordenCompra.ManuallyPurchaseRequest;
import com.logistica.sap.ordenCompra.ManuallyPurchaseRequestResponse;
import com.logistica.sap.ordenCompra.PartyID;
import com.logistica.sap.ordenCompra.Price;
import com.logistica.sap.ordenCompra.ProductID;
import com.logistica.sap.ordenCompra.PurchaseRequestItemManually;
import com.logistica.sap.ordenCompra.PurchaseRequestMaintainRequestBundleItemParty;
import com.logistica.sap.ordenCompra.PurchaseRequestMaintainRequestProductKeyItem;
import com.logistica.sap.ordenCompra.PurchaseRequestMessageManuallyCreate;
import com.logistica.sap.ordenCompra.PurchaseRequestResponse;
import com.logistica.sap.ordenCompra.Quantity;
import com.logistica.sap.ordenCompra.Text;
import com.logistica.sap.ordenCompra.TextCollectionTextTypeCode;
import com.logistica.sap.ordenCompra.UPPEROPENLOCALNORMALISEDDateTimePeriod;
import com.logistica.util.ControlAccesoUtil;

@Component
public class OrdenCompraUtil {
	static Logger logger = Logger.getLogger(OrdenCompraUtil.class.getName());
	private static final String cProductTypeCode = "2";
	private static final String cProductIdentifierTypeCode = "1";
	private static final String cTypeCode = "19"; // SIEMPRE 19
	// private static final String cQuantityUnitCode = "EA";// SIEMPRE EA
	private static final String cQuantity = "1";// SIEMPRE 1
	private static final String cShipToLocation = "MLG1000"; // SIEMPRE MLG1000
	private static final String cCompanyId = "MLG1000";// SIEMPRE MLG1000
	private static final String cPurchasingId = "MLG1405";// SIEMPRE ES MLG1405
	private static final String cTextTypeCode = "10011";

	String tipoServicio;

	@Autowired
	IServicioSAPDao servicioSAPDao;

	@Autowired
	IServicioDao servicioDao;

	@Autowired
	IBitacoraSapDao bitacoraSAPDao;

	public PurchaseRequestMessageManuallyCreate getExpoImpoRequest(OrdenCompraRequest ocr, String ordenCompra,
			List<CargoOrigen> cargosOrigen, List<CargoDestino> cargosDestino) {
		tipoServicio = calculaTipoServicio(ocr.getIdServicio());
		logger.info(ocr.getIdServicio() + "============ CREA REQUEST ORDEN COMPRA [" + ordenCompra + "]");
		PurchaseRequestMessageManuallyCreate puRequest = new PurchaseRequestMessageManuallyCreate();

		try {
			ManuallyPurchaseRequest pr = new ManuallyPurchaseRequest();
			pr.setActionCode(ocr.getPoActionCode());
			if (ocr.getPoActionCode().equals("04")) {
				logger.info("ES ACTUALIZACION");
				BusinessTransactionDocumentID documentID = new BusinessTransactionDocumentID();
				documentID.setValue(ordenCompra);
				pr.setPurchaseRequestID(documentID);
			}

			// ******* CARGOS ORIGEN
			if (cargosOrigen != null && cargosOrigen.size() > 0) {
				for (CargoOrigen cargoOrigen : cargosOrigen) {
					if (cargoOrigen.getCompraMonto() != null
							&& (cargoOrigen.getCompraMonto()).compareTo(BigDecimal.ZERO) > 0) {
						ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio,
								cargoOrigen.getCargoAereo().getTraduccion());
//						logger.info("BUSCA SERVICIO SAP:" + tipoServicio + " - "
//								+ cargoOrigen.getCargoAereo().getTraduccion());
						if (servicioSAP != null) {

							ocr.setItemID(cargoOrigen.getItemID());
							ocr.setItemProductId(servicioSAP.getIdServicio());
							ocr.setItemDeliveryStartTime(new Date());
							ocr.setItemCurrency(cargoOrigen.getMonedaCosto().getSimbolo());
							ocr.setItemPriceValue(cargoOrigen.getCompraMonto());
							ocr.setItemSupplierId(cargoOrigen.getProveedor().getMotivoBloqueo());

							PurchaseRequestItemManually item = getPurchaseItem(ocr);
							pr.getItem().add(item);

						} else {
							logger.error("No se encontro servicioSAP:" + tipoServicio + " - "
									+ cargoOrigen.getCargoAereo().getTraduccion());
						}
					}
				}
			}
			// ******* CARGOS DESTINO
			if (cargosDestino != null && cargosDestino.size() > 0) {
				for (CargoDestino cargoDestino : cargosDestino) {
					if (cargoDestino.getCompraMonto() != null
							&& (cargoDestino.getCompraMonto()).compareTo(BigDecimal.ZERO) > 0) {
//					ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio,
//							cargoDestino.getCargosAereos().getTraduccion(), ocr.getIdServicio());
						// String pProductId = null;
						ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio,
								cargoDestino.getCargoAereo().getTraduccion());
//						logger.info("BUSCA SERVICIO SAP:" + tipoServicio + " - "
//								+ cargoDestino.getCargoAereo().getTraduccion());

						if (servicioSAP != null) {
							ocr.setItemID(cargoDestino.getItemID());
							ocr.setItemProductId(servicioSAP.getIdServicio());
							ocr.setItemDeliveryStartTime(new Date());
							ocr.setItemCurrency(cargoDestino.getMonedaCosto().getSimbolo());
							ocr.setItemPriceValue(cargoDestino.getCompraMonto());
							ocr.setItemSupplierId(cargoDestino.getProveedor().getMotivoBloqueo());

							PurchaseRequestItemManually item = getPurchaseItem(ocr);
							pr.getItem().add(item);
						} else {
							logger.error("No se encontro servicioSAP:" + tipoServicio + " - "
									+ cargoDestino.getCargoAereo().getTraduccion());
						}
					}
				}
			}
			// ========================SE AÑADE EL REQUEST
			puRequest.getPurchaseRequestMaintainBundle().add(pr);

		} catch (Exception e) {
			// salida = "Error al generar la orden de compra [" + e.getMessage()
			// + "]";
			logger.error("[" + ocr.getIdServicio() + "] Error al generar la orden de compra:: " + e.getMessage(), e);
		}
		logger.info(ocr.getIdServicio() + "============ FIN CREA ORDEN DE COMPRA IMPO ============");
		return puRequest;
	}

	public PurchaseRequestMessageManuallyCreate getTerrestreRequest(OrdenCompraRequest ocr, Servicio s) {

//		log.info("[" + s.getIdServicio() + "] ARMA REQUEST");
		PurchaseRequestMessageManuallyCreate puRequest = new PurchaseRequestMessageManuallyCreate();
		PurchaseRequestItemManually item;
		try {

			ManuallyPurchaseRequest pr = new ManuallyPurchaseRequest();
			pr.setActionCode(ocr.getPoActionCode());
			if (ocr.getPoActionCode().equals("04")) {
				logger.info("[" + s.getIdServicioStr() + "] ACTUALIZACION DE OC " + s.getOrdenCompra());
				BusinessTransactionDocumentID documentID = new BusinessTransactionDocumentID();

				documentID.setValue(s.getOrdenCompra());
				pr.setPurchaseRequestID(documentID);
			}

			// ******* RUTAS
			for (ServicioPorCliente ruta : s.getServiciosPorCliente()) {
				if (ruta.getCosto() != null && (ruta.getCosto().compareTo(BigDecimal.ZERO) > 0)) {
					String tipoServicio = ruta.getUbicacion().getPais().equals("MEXICO") ? "NACIONAL" : "EXPO";
					String pCargoFLAM = (ruta.getTipoRuta().getIdTipoRuta().compareTo(new Long(2)) == 0) ? "CRUCE"
							: "FLETE";
//					String texto = (ruta.getTipoRuta().getIdTipoRuta().compareTo(new Long(3)) == 0)
//							? ruta.getReferenciaCliente()
//							: null;
					ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo(tipoServicio, pCargoFLAM);
//					logger.info("BUSCA SERVICIO SAP:" + tipoServicio + " - " + pCargoFLAM);
					if (servicioSAP != null) {
						ocr.setItemID(ruta.getTipoRuta().getIdTipoRuta());
						ocr.setItemProductId(servicioSAP.getIdServicio());
						ocr.setItemDeliveryStartTime(new Date());
//						logger.info("MONEDA COSTO:"+ruta.getMonedaCosto().getIdMoneda()+" - "+ruta.getMonedaCosto().getSimbolo());
						ocr.setItemCurrency(ruta.getMonedaCosto().getIdMoneda() == 1L ? "USD" : "MXN");
						ocr.setItemPriceValue(ruta.getCosto());
						ocr.setItemSupplierId(ruta.getProveedor().getMotivoBloqueo());
						ocr.setItemTextValue(null);

						item = getPurchaseItem(ocr);
						pr.getItem().add(item);
					} else {
						logger.error("No se encontro servicioSAP:" + tipoServicio + " - " + pCargoFLAM);
					}
				}
			}
			if (s.getCargosPorServicio() != null && s.getCargosPorServicio().size() > 0) {
				for (CargoPorServicio cargo : s.getCargosPorServicio()) {
					if (cargo.getCosto() != null && (cargo.getCosto().compareTo(BigDecimal.ZERO) > 0)) {
//					if (cargo.getCosto() != null && (cargo.getCosto().compareTo(BigDecimal.ZERO) > 0)
//							&& !cargo.isDisabled()) {
//					
						ServicioSAP servicioSAP = servicioSAPDao.getServicioSAPByCargo("NACIONAL",
								cargo.getCargo().getDescCorta());
						if (servicioSAP != null && !servicioSAP.getIdServicio().startsWith("NOTA")) {
							ocr.setItemID(new Long(cargo.getPosicion()));
							ocr.setItemProductId(servicioSAP.getIdServicio());
							ocr.setItemDeliveryStartTime(new Date());
							ocr.setItemCurrency(cargo.getMonedaCosto().getIdMoneda() == 1L ? "USD" : "MXN");
							ocr.setItemPriceValue(cargo.getCosto());
							ocr.setItemSupplierId(cargo.getProveedor().getMotivoBloqueo());
							ocr.setItemTextValue(cargo.getObservaciones());
							item = getPurchaseItem(ocr);
							pr.getItem().add(item);
						} else {
							logger.error("No se encontro servicioSAP");
						}
					}
				}
			}
			// ========================SE AÑADE EL REQUEST
			puRequest.getPurchaseRequestMaintainBundle().add(pr);

		} catch (Exception e) {
			logger.error("[" + ocr.getIdServicio() + "] Error al generar request:: " + e.getMessage(), e);
		}

		return puRequest;
	}

	private PurchaseRequestItemManually getPurchaseItem(OrdenCompraRequest re) {

		PurchaseRequestItemManually item = new PurchaseRequestItemManually();
		try {

			item.setActionCode(re.getItemActionCode());
			item.setItemID(re.getItemID() + "");

			// CANCELA ITEM
			if (re.isCancelItem()) {
				item.setCancelOpenQuantityActionIndicator(true);
			}

			// ProductKeyItem
			PurchaseRequestMaintainRequestProductKeyItem productKeyItem = new PurchaseRequestMaintainRequestProductKeyItem();
			productKeyItem.setProductTypeCode(cProductTypeCode);
			productKeyItem.setProductIdentifierTypeCode(cProductIdentifierTypeCode);
			ProductID productId = new ProductID();
			productId.setValue(re.getItemProductId());
			productKeyItem.setProductID(productId);
			item.setProductKeyItem(productKeyItem);

			// TypeCode
			item.setTypeCode(cTypeCode);

			// Quantity
			Quantity quantity = new Quantity();
			// quantity.setUnitCode(cQuantityUnitCode);
			quantity.setValue(new BigDecimal(cQuantity));
			item.setQuantity(quantity);

			// DeliveryPeriod
			UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod = new UPPEROPENLOCALNORMALISEDDateTimePeriod();
			LOCALNORMALISEDDateTime startDateTime = new LOCALNORMALISEDDateTime();

			GregorianCalendar gc = new GregorianCalendar();
			gc.setTime(re.getItemDeliveryStartTime());
			XMLGregorianCalendar periodStartTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			startDateTime.setValue(periodStartTime);

			deliveryPeriod.setStartDateTime(startDateTime);
			deliveryPeriod.setEndDateTime(startDateTime);
			item.setDeliveryPeriod(deliveryPeriod);

			// ShipToLocationID
			LocationID locationId = new LocationID();
			locationId.setValue(cShipToLocation);
			item.setShipToLocationID(locationId);

			// ListUnitPrice
			Price unitPrice = new Price();
			Amount amount = new Amount();
			amount.setCurrencyCode(re.getItemCurrency());
			amount.setValue(re.getItemPriceValue());
			unitPrice.setAmount(amount);
			item.setListUnitPrice(unitPrice);

			// CompanyIDParty
			PurchaseRequestMaintainRequestBundleItemParty companyId = new PurchaseRequestMaintainRequestBundleItemParty();
			PartyID companyIdParty = new PartyID();
			companyIdParty.setValue(cCompanyId);
			companyId.setPartyID(companyIdParty);
			item.setCompanyIDParty(companyId);

			// TEXT COLLECTION
			if (re.getItemTextValue() != null && re.getItemTextValue().length() > 0) {
				MaintenanceTextCollection textCollectionValue = new MaintenanceTextCollection();
				MaintenanceTextCollectionText text = new MaintenanceTextCollectionText();
				TextCollectionTextTypeCode typeCode = new TextCollectionTextTypeCode();
				typeCode.setValue(cTextTypeCode);
				text.setTypeCode(typeCode);

				MaintenanceTextCollectionTextTextContent textContent = new MaintenanceTextCollectionTextTextContent();
				Text value = new Text();
				value.setValue(re.getItemTextValue());
				textContent.setText(value);
				text.setTextContent(textContent);
				text.setLanguageCode("ES");
				textCollectionValue.getText().add(text);
				item.setTextCollection(textCollectionValue);
			}

			// BuyerResponsibleIDParty
			// PurchaseRequestMaintainRequestBundleItemParty buyerId = new
			// PurchaseRequestMaintainRequestBundleItemParty();
			// PartyID buyerIdParty = new PartyID();
			// buyerIdParty.setValue(pBuyerId);
			// buyerId.setPartyID(buyerIdParty);
			// item.setBuyerResponsibleIDParty(buyerId);

			// PurchasingUnitIDParty
			PurchaseRequestMaintainRequestBundleItemParty purchasingId = new PurchaseRequestMaintainRequestBundleItemParty();
			PartyID purchasingIdParty = new PartyID();
			purchasingIdParty.setValue(cPurchasingId);
			purchasingId.setPartyID(purchasingIdParty);
			item.setPurchasingUnitIDParty(purchasingId);

			// SupplierIDParty
			PurchaseRequestMaintainRequestBundleItemParty supplierId = new PurchaseRequestMaintainRequestBundleItemParty();
			PartyID supplierIdParty = new PartyID();
			supplierIdParty.setValue(re.getItemSupplierId());
			supplierId.setPartyID(supplierIdParty);
			item.setSupplierIDParty(supplierId);

			// PARAMETROS SIMPLES
			item.setNdeservicio2(re.getItemServicioFLAM());
			// item.setConsignatariosS(re.getItemConsignatarios());
			// SE CAMBIA
//			item.setConsignatarioCruceS(re.getItemConsignatarioCruce());
//			item.setConsignatarioDestino1(re.getItemConsignatarioDestino());
			item.setConsignatariosS(re.getItemConsignatarioDestino());

			item.setFechaCargaS(formatDate(re.getItemFechaCarga()));
			if (re.getItemFechaCruce() != null) {
				item.setFechaCruceS(formatDate(re.getItemFechaCruce()));
			}
			item.setFechaDestinoS(formatDate(re.getItemFechaDestino()));
			if (re.getItemFechaAgenteAduanal() != null) {
				item.setFechadeAgenteAduanalS(formatDate(re.getItemFechaAgenteAduanal()));
			}
			item.setGuaHouseS(re.getItemGuiaHouse());
			item.setGuaMasterS(re.getItemGuiaMaster());
			item.setNmerodeequipodecargaS(re.getItemNumeroEquipoCarga());
			item.setPzsPBPCS(re.getItemPzsPBPCS());
			item.setRequierePODS(re.getItemRequierePOD());
			item.setShipperS(re.getItemShipper());
			item.setTipodeequipoS(re.getItemTipoEquipo());

			item.setUbicacinCruceS(re.getItemUbicacionCruce());
			item.setUbicacinDestinoS(re.getItemUbicacionDestino());
			item.setUbicacinOrigenS(re.getItemUbicacionOrigen());

			item.setUnidaddeventaS(re.getItemUnidadVentas());

		} catch (Exception e) {
			logger.error("Error al armar request Orden de Compra::" + e, e);
		}

		return item;
	}

	public String leeOrdenCompra(Long idServicio, String idServicioStr, PurchaseRequestResponse r) {
		String salida = "";
		String idOrdenCompra = null;
		logger.info("[" + idServicioStr + "] Orden compra");
		try {
			if (r.getPurchaseRequestResponse() != null && r.getPurchaseRequestResponse().size() > 0) {
				for (ManuallyPurchaseRequestResponse soResult : r.getPurchaseRequestResponse()) {
					BusinessTransactionDocumentID id = soResult.getPurchaseRequestID();
					idOrdenCompra = id.getValue();
					salida = "Orden de compra [" + idOrdenCompra + "]";
				}
			} else {
				salida = " - No se generó Orden de compra";
			}
			logger.info("[" + idServicioStr + "] - " + salida);
			Log loge = r.getLog();
			String notas = "";
			for (LogItem logItem : loge.getItem()) {
				logger.info(
						"[" + idServicioStr + "] \t Nota -> [" + logItem.getSeverityCode() + "] " + logItem.getNote());
				notas += "- [" + logItem.getSeverityCode() + "] " + logItem.getNote() + "\n";
			}

			BitacoraSap t = new BitacoraSap(idServicio, "ORDEN DE COMPRA", notas, idOrdenCompra);
			t.setControlAcceso(ControlAccesoUtil.generarControlAccesoCreacion());
			bitacoraSAPDao.save(t);
		} catch (Exception e) {
			logger.error(
					"[" + idServicioStr + "] Error al leer la respuesta de SAP para orden de compra:" + e.getMessage(),
					e);
		}
//		log.info(idServicio + " ---------- FIN RESPUESTA SAP ORDEN DE COMPRA---------- ");
		return idOrdenCompra;
	}

	public String calculaTipoServicio(String idServicioStr) {
		if (idServicioStr != null) {
			if (idServicioStr.endsWith("AE")) {
				return "AEREO EXPO";
			} else if (idServicioStr.endsWith("AI")) {
				return "AEREO IMPO";
			} else if (idServicioStr.endsWith("ME")) {
				return "MARITIMO EXPO";
			} else if (idServicioStr.endsWith("MI")) {
				return "MARITIMO IMPO";
			} else if (idServicioStr.endsWith("LE")) {
				return "LAND EXPO";
			} else if (idServicioStr.endsWith("LI")) {
				return "LAND IMPO";
			} else {
				return "NACIONAL";
			}

		} else {
			return "NACIONAL";
		}
	}

	private static XMLGregorianCalendar formatDate(Date date) {
		String FORMATER = "yyyy-MM-dd";
		DateFormat format = new SimpleDateFormat(FORMATER);
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
		} catch (Exception e) {
			return null;
		}

	}

//	public void log.info(String idServicio, String cadena) {
//		log.info("[" + idServicio + "]  " + cadena);
//	}

	public String calculaPrefijoUnidadVentas(String idServicioStr) {
		if (idServicioStr != null) {
			if (idServicioStr.endsWith("AE")) {
				return "-3";
			} else if (idServicioStr.endsWith("AI")) {
				return "-3";
			} else if (idServicioStr.endsWith("ME")) {
				return "-2";
			} else if (idServicioStr.endsWith("MI")) {
				return "-2";
			} else if (idServicioStr.endsWith("LE")) {
				return "-4";
			} else if (idServicioStr.endsWith("LI")) {
				return "-4";
			} else {
				return "-1";
			}

		} else {
			return "-1";
		}
	}
}
