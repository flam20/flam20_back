package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICI_AgenteAduanalDao;
import com.logistica.model.CI_AgenteAduanal;
import com.logistica.service.ICI_AgenteAduanalService;

@Service
public class CI_AgenteAduanalServiceImpl implements ICI_AgenteAduanalService {

	@Autowired
	ICI_AgenteAduanalDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<CI_AgenteAduanal> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CI_AgenteAduanal> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public CI_AgenteAduanal findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public CI_AgenteAduanal save(CI_AgenteAduanal agenteAduanal) {
		return dao.save(agenteAduanal);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CI_AgenteAduanal agenteAduanal = dao.findById(id).orElse(null);
		if (agenteAduanal != null) {
			dao.delete(agenteAduanal);
		}

	}

}
