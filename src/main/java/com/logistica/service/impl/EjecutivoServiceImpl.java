package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IEjecutivoDao;
import com.logistica.model.Ejecutivo;
import com.logistica.service.IEjecutivoService;

@Service
public class EjecutivoServiceImpl implements IEjecutivoService {

	@Autowired
	IEjecutivoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Ejecutivo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Ejecutivo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Ejecutivo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Ejecutivo save(Ejecutivo ejecutivo) {
		return dao.save(ejecutivo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Ejecutivo ejecutivo = dao.findById(id).orElse(null);
		if (ejecutivo != null) {
			dao.delete(ejecutivo);
		}
	}

	@Override
	public Ejecutivo findByIdTeams(String idTeams) {
		List<Ejecutivo> ejecutivos = dao.findByIdTeams(idTeams);
		if(ejecutivos!=null && ejecutivos.size()>0) {
				return ejecutivos.get(0);
		}else {
			return null;
		}
	}

}
