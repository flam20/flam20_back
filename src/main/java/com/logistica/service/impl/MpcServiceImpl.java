package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IMpcDao;
import com.logistica.model.Mpc;
import com.logistica.service.IMpcService;

@Service
public class MpcServiceImpl implements IMpcService {

	@Autowired
	IMpcDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Mpc> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Mpc> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Mpc findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Mpc save(Mpc mpc) {
		return dao.save(mpc);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Mpc mpc = dao.findById(id).orElse(null);
		if (mpc != null) {
			dao.delete(mpc);
		}
	}

}
