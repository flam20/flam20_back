package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IFormaPagoDao;
import com.logistica.model.FormaPago;
import com.logistica.service.IFormaPagoService;

@Service
public class FormaPagoServiceImpl implements IFormaPagoService {

	@Autowired
	IFormaPagoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<FormaPago> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<FormaPago> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public FormaPago findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public FormaPago save(FormaPago formaPago) {
		return dao.save(formaPago);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		FormaPago formaPago = dao.findById(id).orElse(null);
		if (formaPago != null) {
			dao.delete(formaPago);
		}
	}

}
