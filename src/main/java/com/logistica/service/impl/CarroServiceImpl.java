package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICarroDao;
import com.logistica.model.cartaporte.Carro;
import com.logistica.service.ICarroService;

@Service
public class CarroServiceImpl implements ICarroService {

	@Autowired
	ICarroDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Carro> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Carro> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Carro findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Carro save(Carro carro) {
		return dao.save(carro);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Carro carro = dao.findById(id).orElse(null);
		if (carro != null) {
			dao.delete(carro);
		}
	}

}
