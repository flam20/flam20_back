package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ITransporteMaritimoDao;
import com.logistica.model.cartaporte.TransporteMaritimo;
import com.logistica.service.ITransporteMaritimoService;

@Service
public class TransporteMaritimoServiceImpl implements ITransporteMaritimoService {

	@Autowired
	ITransporteMaritimoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<TransporteMaritimo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TransporteMaritimo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TransporteMaritimo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public TransporteMaritimo save(TransporteMaritimo transporteMaritimo) {
		return dao.save(transporteMaritimo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		TransporteMaritimo transporteMaritimo = dao.findById(id).orElse(null);
		if (transporteMaritimo != null) {
			dao.delete(transporteMaritimo);
		}
	}

}
