package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IAutotransporteDao;
import com.logistica.model.cartaporte.Autotransporte;
import com.logistica.service.IAutotransporteService;

@Service
public class AutotransporteServiceImpl implements IAutotransporteService {

	@Autowired
	IAutotransporteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Autotransporte> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Autotransporte> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Autotransporte findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Autotransporte save(Autotransporte autotransporte) {
		return dao.save(autotransporte);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Autotransporte autotransporte = dao.findById(id).orElse(null);
		if (autotransporte != null) {
			dao.delete(autotransporte);
		}
	}

}
