package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IPaisDao;
import com.logistica.model.Pais;
import com.logistica.service.IPaisService;

@Service
public class PaisServiceImpl implements IPaisService {

	@Autowired
	IPaisDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Pais> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Pais> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Pais findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Pais save(Pais pais) {
		return dao.save(pais);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Pais pais = dao.findById(id).orElse(null);
		if (pais != null) {
			dao.delete(pais);
		}
	}

}
