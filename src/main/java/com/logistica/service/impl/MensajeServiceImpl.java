package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logistica.dao.IMensajeDao;
import com.logistica.model.Mensaje;
import com.logistica.service.IMensajeService;

@Service
public class MensajeServiceImpl implements IMensajeService {

	@Autowired
	private IMensajeDao dao;

	@Override
	public List<Mensaje> findMensajesByUsuarioDestino(Long idUsuario) {
		return dao.findMensajesByUsuarioDestinoIdUsuario(idUsuario);
	}

	@Override
	public List<Mensaje> findMensajesByUsuarioAndTipoAndServicio(Long idUsuario, String tipo, Long idServicio) {
		return dao.findMensajesByUsuarioDestinoIdUsuarioOrUsuarioOrigenIdUsuarioAndTipoAndIdServicioOrderByFechaAsc(
				idUsuario, tipo, idServicio);
	}

	@Override
	public Mensaje findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	public Mensaje save(Mensaje mensaje) {
		return dao.save(mensaje);
	}

}
