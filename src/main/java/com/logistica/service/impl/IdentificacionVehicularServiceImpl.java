package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IIdentificacionVehicularDao;
import com.logistica.model.cartaporte.IdentificacionVehicular;
import com.logistica.service.IIdentificacionVehicularService;

@Service
public class IdentificacionVehicularServiceImpl implements IIdentificacionVehicularService {

	@Autowired
	IIdentificacionVehicularDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<IdentificacionVehicular> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<IdentificacionVehicular> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public IdentificacionVehicular findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public IdentificacionVehicular save(IdentificacionVehicular identificacionVehicular) {
		return dao.save(identificacionVehicular);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		IdentificacionVehicular identificacionVehicular = dao.findById(id).orElse(null);
		if (identificacionVehicular != null) {
			dao.delete(identificacionVehicular);
		}
	}

}
