package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICargoAereoDao;
import com.logistica.model.CargoAereo;
import com.logistica.service.ICargoAereoService;

@Service
public class CargoAereoServiceImpl implements ICargoAereoService {

	@Autowired
	ICargoAereoDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<CargoAereo> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CargoAereo> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public CargoAereo findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public CargoAereo save(CargoAereo cargo) {
		return dao.save(cargo);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CargoAereo cargo = dao.findById(id).orElse(null);
		if (cargo != null) {
			dao.delete(cargo);
		}
	}

}
