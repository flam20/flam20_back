package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IServicioPorClienteDao;
import com.logistica.model.ServicioPorCliente;
import com.logistica.service.IServicioPorClienteService;

@Service
public class ServicioPorClienteServiceImpl implements IServicioPorClienteService {

	@Autowired
	IServicioPorClienteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<ServicioPorCliente> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ServicioPorCliente> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ServicioPorCliente findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ServicioPorCliente save(ServicioPorCliente servicioPorCliente) {
		return dao.save(servicioPorCliente);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ServicioPorCliente servicioPorCliente = dao.findById(id).orElse(null);
		if (servicioPorCliente != null) {
			dao.delete(servicioPorCliente);
		}
	}

}
