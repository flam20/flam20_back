package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IUbicacionCpDao;
import com.logistica.model.cartaporte.UbicacionCp;
import com.logistica.service.IUbicacionCpService;

@Service
public class UbicacionCpServiceImpl implements IUbicacionCpService {

	@Autowired
	IUbicacionCpDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<UbicacionCp> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<UbicacionCp> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public UbicacionCp findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public UbicacionCp save(UbicacionCp ubicacionCp) {
		return dao.save(ubicacionCp);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		UbicacionCp ubicacionCp = dao.findById(id).orElse(null);
		if (ubicacionCp != null) {
			dao.delete(ubicacionCp);
		}
	}

}
