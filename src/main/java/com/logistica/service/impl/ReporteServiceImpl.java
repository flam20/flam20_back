package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IReporteDao;
import com.logistica.model.Reporte;
import com.logistica.service.IReporteService;

@Service
public class ReporteServiceImpl implements IReporteService {

	@Autowired
	IReporteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Reporte> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Reporte> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Reporte findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Reporte save(Reporte reporte) {
		return dao.save(reporte);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Reporte reporte = dao.findById(id).orElse(null);
		if (reporte != null) {
			dao.delete(reporte);
		}

	}

}
