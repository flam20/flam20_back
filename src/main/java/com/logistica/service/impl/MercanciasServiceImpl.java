package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IMercanciasDao;
import com.logistica.model.cartaporte.Mercancias;
import com.logistica.service.IMercanciasService;

@Service
public class MercanciasServiceImpl implements IMercanciasService {

	@Autowired
	IMercanciasDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<Mercancias> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Mercancias> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Mercancias findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Mercancias save(Mercancias resumenMercancia) {
		return dao.save(resumenMercancia);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Mercancias resumenMercancia = dao.findById(id).orElse(null);
		if (resumenMercancia != null) {
			dao.delete(resumenMercancia);
		}
	}

}
