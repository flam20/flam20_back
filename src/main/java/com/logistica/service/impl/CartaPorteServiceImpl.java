package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.ICartaPorteDao;
import com.logistica.model.cartaporte.CartaPorte;
import com.logistica.service.ICartaPorteService;

@Service
public class CartaPorteServiceImpl implements ICartaPorteService {

	@Autowired
	ICartaPorteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<CartaPorte> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CartaPorte> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public CartaPorte findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public CartaPorte save(CartaPorte cartaPorte) {
		return dao.save(cartaPorte);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CartaPorte cartaPorte = dao.findById(id).orElse(null);
		if (cartaPorte != null) {
			dao.delete(cartaPorte);
		}
	}

}
