package com.logistica.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.dao.IParteTransporteDao;
import com.logistica.model.cartaporte.ParteTransporte;
import com.logistica.service.IParteTransporteService;

@Service
public class ParteTransporteServiceImpl implements IParteTransporteService {

	@Autowired
	IParteTransporteDao dao;

	@Override
	@Transactional(readOnly = true)
	public Page<ParteTransporte> pageAll(Pageable pageable) {
		return dao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ParteTransporte> findAll() {
		return dao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ParteTransporte findById(Long id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ParteTransporte save(ParteTransporte parteTransporte) {
		return dao.save(parteTransporte);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ParteTransporte parteTransporte = dao.findById(id).orElse(null);
		if (parteTransporte != null) {
			dao.delete(parteTransporte);
		}
	}

}
