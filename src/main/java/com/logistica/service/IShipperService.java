package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Shipper;

public interface IShipperService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Shipper> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Shipper> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Shipper findById(Long id);

	/**
	 * Guardar el Shipper
	 * 
	 * @param cargo
	 * @return
	 */
	Shipper save(Shipper shipper);

	/**
	 * Borrar Shipper seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
