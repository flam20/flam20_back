package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Carro;

public interface ICarroService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Carro> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Carro> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Carro findById(Long id);

	/**
	 * Guardar el Carro
	 * 
	 * @param carro
	 * @return
	 */
	Carro save(Carro carro);

	/**
	 * Borrar Carro seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
