package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.CI_Shipper;

public interface ICI_ShipperService {
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<CI_Shipper> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<CI_Shipper> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	CI_Shipper findById(Long id);

	/**
	 * Guardar el Shipper
	 * 
	 * @param cargo
	 * @return
	 */
	CI_Shipper save(CI_Shipper shipper);

	/**
	 * Borrar Shipper seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);
}
