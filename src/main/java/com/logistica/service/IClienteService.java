package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Cliente;
import com.logistica.model.Ejecutivo;

public interface IClienteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Cliente> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Cliente> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Cliente findById(Long id);

	/**
	 * Guardar el cliente
	 * 
	 * @param cliente
	 * @return
	 */
	Cliente save(Cliente cliente);

	/**
	 * Borrar cliente seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	/**
	 * Obtener la lista de clientes no bloqueados.
	 * 
	 * @return
	 */
	List<Cliente> findAllUnlocked();

	List<Cliente> findFirst10ByBusqueda(String busqueda);
	
	List<Ejecutivo> findEjecutivosByIdCliente(Long id);
	

}
