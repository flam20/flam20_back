package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Remolque;

public interface IRemolqueService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Remolque> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Remolque> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Remolque findById(Long id);

	/**
	 * Guardar el remolque
	 * 
	 * @param cargo
	 * @return
	 */
	Remolque save(Remolque remolque);

	/**
	 * Borrar Remolque seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
