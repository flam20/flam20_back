package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Ubicacion;

public interface IUbicacionService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Ubicacion> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Ubicacion> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Ubicacion findById(Long id);

	/**
	 * Guardar la ubicacion
	 * 
	 * @param ubicacion
	 * @return
	 */
	Ubicacion save(Ubicacion tipoRuta);

	/**
	 * Borrar ubicacion seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	List<Ubicacion> findFirst10ByCiudad(String ciudad);

	List<Ubicacion> findFirst10ByCiudadAndPais(String ciudad, String pais);

}
