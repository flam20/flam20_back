package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.CartaPorte;

public interface ICartaPorteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<CartaPorte> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<CartaPorte> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	CartaPorte findById(Long id);

	/**
	 * Guardar el CartaPorte
	 * 
	 * @param cartaPorte
	 * @return
	 */
	CartaPorte save(CartaPorte cartaPorte);

	/**
	 * Borrar cartaPorte seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
