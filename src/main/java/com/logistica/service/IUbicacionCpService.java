package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.UbicacionCp;

public interface IUbicacionCpService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<UbicacionCp> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<UbicacionCp> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	UbicacionCp findById(Long id);

	/**
	 * Guardar el UbicacionCp
	 * 
	 * @param cargo
	 * @return
	 */
	UbicacionCp save(UbicacionCp ubicacionCp);

	/**
	 * Borrar UbicacionCp seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
