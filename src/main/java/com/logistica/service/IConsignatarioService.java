package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Consignatario;

public interface IConsignatarioService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Consignatario> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Consignatario> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Consignatario findById(Long id);

	/**
	 * Guardar el Consignatario
	 * 
	 * @param consignatario
	 * @return
	 */
	Consignatario save(Consignatario consignatario);

	/**
	 * Borrar Consignatario seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
