package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Usuario;

public interface IUsuarioService {

	Usuario findByUsername(String username);

	Page<Usuario> pageAll(Pageable pageable);

	List<Usuario> findAll();

	Usuario findById(Long id);

	Usuario save(Usuario usuario);

	List<Usuario> findCuentasPorPagarAndAdministracion();

}
