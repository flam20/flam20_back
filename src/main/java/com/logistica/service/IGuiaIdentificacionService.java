package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.GuiaIdentificacion;

public interface IGuiaIdentificacionService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<GuiaIdentificacion> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<GuiaIdentificacion> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	GuiaIdentificacion findById(Long id);

	/**
	 * Guardar el GuiaIdentificacion
	 * 
	 * @param guiaIdentificacion
	 * @return
	 */
	GuiaIdentificacion save(GuiaIdentificacion guiaIdentificacion);

	/**
	 * Borrar GuiaIdentificacion seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
