package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Pedimento;

public interface IPedimentoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Pedimento> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Pedimento> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Pedimento findById(Long id);

	/**
	 * Guardar el Pedimento
	 * 
	 * @param pedimento
	 * @return
	 */
	Pedimento save(Pedimento pedimento);

	/**
	 * Borrar Pedimento seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
