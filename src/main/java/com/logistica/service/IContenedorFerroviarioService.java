package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.ContenedorFerroviario;

public interface IContenedorFerroviarioService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<ContenedorFerroviario> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<ContenedorFerroviario> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	ContenedorFerroviario findById(Long id);

	/**
	 * Guardar el ContenedorFerroviario
	 * 
	 * @param contenedorFerroviario
	 * @return
	 */
	ContenedorFerroviario save(ContenedorFerroviario contenedorFerroviario);

	/**
	 * Borrar ContenedorFerroviario seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
