package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Servicio;

public interface IServicioService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Servicio> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Servicio> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Servicio findById(Long id);

	/**
	 * Guardar el servicio
	 * 
	 * @param servicio
	 * @return
	 */
	Servicio save(Servicio servicio);

	/**
	 * Borrar servicio seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	Integer getConsecutiveByAnio(Integer anio);

	List<Servicio> searchServicios(String numeroServicio, Long idEstatus, String idTrafico, Long idCliente,
			Long idProveedor, String numeroFactura, String fechaDesde, String fechaHasta, Boolean borradores);

	List<Servicio> searchServiciosVentas(String numeroServicio, Long idEstatus, String desde, String hasta,
			Long cliente, String idTeams, Long idProveedor, String idTrafico);

	List<Servicio> findTraficos(); 

	/**
	 * Actualiza idChat
	 * 
	 * @param servicio
	 * @return
	 */
	void updateChatID(String idChat, Long idServicio);

}
