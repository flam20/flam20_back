package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.TipoFigura;

public interface ITipoFiguraService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TipoFigura> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TipoFigura> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TipoFigura findById(Long id);

	/**
	 * Guardar el Operador
	 * 
	 * @param operador
	 * @return
	 */
	TipoFigura save(TipoFigura operador);

	/**
	 * Borrar Operador seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
