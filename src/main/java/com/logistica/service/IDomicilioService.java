package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Domicilio;

public interface IDomicilioService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Domicilio> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Domicilio> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Domicilio findById(Long id);

	/**
	 * Guardar el Aerolinea
	 * 
	 * @param domicilio
	 * @return
	 */
	Domicilio save(Domicilio domicilio);

	/**
	 * Borrar Domicilio seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
