package com.logistica.service;

import java.util.List;

import com.logistica.model.Dashboard;
import com.logistica.model.DashboardBar;

public interface IDashboardService {

	Dashboard getCountByUser(String id_teams);
	
	List<DashboardBar> getCountByUserByMonth(String id_teams);
	
	Dashboard getCountToAdmin();
	
	List<DashboardBar> getCountToAdminByMonth();
	
	Dashboard getCountToVentas(String idEjecutivo);
	
	List<DashboardBar> getCountToVentasByMonth(String IdTeams);

}
