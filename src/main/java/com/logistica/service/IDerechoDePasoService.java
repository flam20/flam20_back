package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.DerechoDePaso;

public interface IDerechoDePasoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<DerechoDePaso> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<DerechoDePaso> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	DerechoDePaso findById(Long id);

	/**
	 * Guardar el DerechoDePaso
	 * 
	 * @param derechoDePaso
	 * @return
	 */
	DerechoDePaso save(DerechoDePaso derechoDePaso);

	/**
	 * Borrar DerechoDePaso seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
