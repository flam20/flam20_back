package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.EnumMes;
import com.logistica.model.Moneda;
import com.logistica.model.TipoCambio;

public interface ITipoCambioService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TipoCambio> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TipoCambio> findAll();

	/**
	 * Obtener los tipos de cambio por año y mes
	 * 
	 * @return
	 */
	List<TipoCambio> findByAnioMes(Integer anio, Long mes);

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TipoCambio findById(Long id);

	/**
	 * Guardar el tipo de cambio
	 * 
	 * @param tipoCambio
	 * @return
	 */
	TipoCambio save(TipoCambio tipoCambio);

	/**
	 * Borrar tipoCambio seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	/**
	 * Buscar por anio, mes, moneda
	 * 
	 * @param id
	 * @return
	 */
	TipoCambio getByAnioMesMoneda(int anio, EnumMes mes, Moneda moneda);

}
