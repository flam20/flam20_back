package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Incoterm;

public interface IIncotermService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Incoterm> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Incoterm> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Incoterm findById(Long id);

	/**
	 * Guardar el Incoterm
	 * 
	 * @param incoterm
	 * @return
	 */
	Incoterm save(Incoterm incoterm);

	/**
	 * Borrar Incoterm seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
