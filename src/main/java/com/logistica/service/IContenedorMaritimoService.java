package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.ContenedorMaritimo;

public interface IContenedorMaritimoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<ContenedorMaritimo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<ContenedorMaritimo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	ContenedorMaritimo findById(Long id);

	/**
	 * Guardar el contenedorMaritimo
	 * 
	 * @param contenedorMaritimo
	 * @return
	 */
	ContenedorMaritimo save(ContenedorMaritimo contenedorMaritimo);

	/**
	 * Borrar ContenedorMaritimo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
