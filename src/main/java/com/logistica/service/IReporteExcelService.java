package com.logistica.service;

import java.io.File;
import java.util.List;

import com.logistica.model.Proveedor;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;

public interface IReporteExcelService {

	File exportToExcel(List<Servicio> servicios);
	
	File exportProveedoresToExcel(List<Proveedor> proveedores);

	File exportToExcelInter(List<ServicioInter> servicios);

}
