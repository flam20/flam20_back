package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.BitacoraSap;

public interface IBitacoraSapService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<BitacoraSap> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<BitacoraSap> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	BitacoraSap findById(Long id);

	/**
	 * Buscar por idServicio.
	 * 
	 * @param idServicio
	 * @return
	 */
	List<BitacoraSap> findByIdServicio(Long idServicio);

	/**
	 * Guardar la BitacoraSap
	 * 
	 * @param bitacoraSap
	 * @return
	 */
	BitacoraSap save(BitacoraSap bitacoraSap);

	/**
	 * Borrar BitacoraSap seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
