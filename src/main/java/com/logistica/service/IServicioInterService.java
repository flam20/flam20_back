package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.ServicioInter;

public interface IServicioInterService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<ServicioInter> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<ServicioInter> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	ServicioInter findById(Long id);

	/**
	 * Guardar el servicio
	 * 
	 * @param servicio
	 * @return
	 */
	ServicioInter save(ServicioInter servicio);

	/**
	 * Borrar servicio seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	Integer getConsecutiveByAnio(Integer anio);

	List<ServicioInter> searchServicios(String numeroServicio, String idTrafico, String guiaHouse, String guiaMaster,
			Long idConsignatario, Long idAgente, String fechaDesde, String fechaHasta, Boolean borradores, String rol);

	void updateChatID(String idChat, Long idServicio);

	List<ServicioInter> findTraficos();

}
