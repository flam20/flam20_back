package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.TipoRuta;

public interface ITipoRutaService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TipoRuta> pageAll(Pageable pageable);

	/**
	 * Obtener todos 
	 * 
	 * @return
	 */
	List<TipoRuta> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TipoRuta findById(Long id);

	/**
	 * Guardar el tipo ruta
	 * 
	 * @param tipoRuta
	 * @return
	 */
	TipoRuta save(TipoRuta tipoRuta);

	/**
	 * Borrar tipoRuta seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
