package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Mercancia;

public interface IMercanciaService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Mercancia> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Mercancia> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Mercancia findById(Long id);

	/**
	 * Guardar el Mercancia
	 * 
	 * @param mercancia
	 * @return
	 */
	Mercancia save(Mercancia mercancia);

	/**
	 * Borrar Mercancia seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
