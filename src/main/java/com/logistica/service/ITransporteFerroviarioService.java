package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.TransporteFerroviario;

public interface ITransporteFerroviarioService {
	
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TransporteFerroviario> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TransporteFerroviario> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TransporteFerroviario findById(Long id);

	/**
	 * Guardar el TransporteFerroviario
	 * 
	 * @param cargo
	 * @return
	 */
	TransporteFerroviario save(TransporteFerroviario transporteFerroviario);

	/**
	 * Borrar TransporteFerroviario seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
