package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.EstatusServicio;

public interface IEstatusServicioService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<EstatusServicio> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<EstatusServicio> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	EstatusServicio findById(Long id);

	/**
	 * Guardar el estatusServicio
	 * 
	 * @param estatusServicio
	 * @return
	 */
	EstatusServicio save(EstatusServicio estatusServicio);

	/**
	 * Borrar estatusServicio seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
