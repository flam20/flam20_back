package com.logistica.service;

import com.logistica.model.EmpleadoSAP;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;

public interface IOrdenCompraService {
	public String creaOrdenCompra(Servicio s, EmpleadoSAP empleadoSAP);

	public String actualizaOrdenCompra(Servicio s, EmpleadoSAP empleadoSAP);
	
	public String cancelaOrdenCompra(Servicio s, EmpleadoSAP empleadoSAP);

	public String creaOrdenCompra_Inter(ServicioInter s, EmpleadoSAP empleadoSAP);

	public String actualizaOrdenCompra_Inter(ServicioInter s, EmpleadoSAP empleadoSAP);

//	public String creaOrdenCompra_EXPO(ServicioAereoExpo s, EmpleadoSAP empleadoSAP);
//
//	public String actualizaOrdenCompra_EXPO(ServicioAereoExpo s, EmpleadoSAP empleadoSAP);

	public boolean eliminaItem(String idServicioStr, String ordenCompra, Long itemId, EmpleadoSAP empleadoSAP);
}
