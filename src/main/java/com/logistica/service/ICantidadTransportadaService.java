package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.logistica.model.cartaporte.CantidadTransportada;

@Service
public interface ICantidadTransportadaService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<CantidadTransportada> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<CantidadTransportada> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	CantidadTransportada findById(Long id);

	/**
	 * Guardar el CantidadTransportada
	 * 
	 * @param cantidadTransportada
	 * @return
	 */
	CantidadTransportada save(CantidadTransportada cantidadTransportada);

	/**
	 * Borrar CantidadTransportada seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
