package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Autotransporte;

public interface IAutotransporteService {
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Autotransporte> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Autotransporte> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Autotransporte findById(Long id);

	/**
	 * Guardar el Autotransporte
	 * 
	 * @param autotransporte
	 * @return
	 */
	Autotransporte save(Autotransporte autotransporte);

	/**
	 * Borrar Autotransporte seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
