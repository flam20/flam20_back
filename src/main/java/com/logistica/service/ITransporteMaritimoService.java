package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.TransporteMaritimo;

public interface ITransporteMaritimoService {
	
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TransporteMaritimo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TransporteMaritimo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TransporteMaritimo findById(Long id);

	/**
	 * Guardar el TransporteMaritimo
	 * 
	 * @param cargo
	 * @return
	 */
	TransporteMaritimo save(TransporteMaritimo transporteMaritimo);

	/**
	 * Borrar TransporteMaritimo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
