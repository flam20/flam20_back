package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Mercancias;

public interface IMercanciasService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Mercancias> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Mercancias> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Mercancias findById(Long id);

	/**
	 * Guardar el ResumenMercancia
	 * 
	 * @param resumenMercancia
	 * @return
	 */
	Mercancias save(Mercancias resumenMercancia);

	/**
	 * Borrar ResumenMercancia seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
