package com.logistica.service;

import java.io.InputStream;
import java.util.List;

import com.logistica.model.ArchivoBox;
import com.logistica.model.Factura;

public interface IBoxService {

	List<ArchivoBox> getArchivoBoxByServicio(Long idServicio);

	void deleteArchivoBoxByServicio(Long idServicio);

	List<ArchivoBox> consultaPODs(Long idServicio);

	ArchivoBox uploadArchivoPod(InputStream is,String extension);

	ArchivoBox uploadArchivoPod(Long idServicio, InputStream is,String extension,String idServicioStr);

	List<Factura> getFacturasByProveedor(Long idProveedor);

}
