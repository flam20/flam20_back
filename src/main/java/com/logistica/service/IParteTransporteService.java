package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.ParteTransporte;

public interface IParteTransporteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<ParteTransporte> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<ParteTransporte> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	ParteTransporte findById(Long id);

	/**
	 * Guardar el ParteTransporte
	 * 
	 * @param parteTransporte
	 * @return
	 */
	ParteTransporte save(ParteTransporte parteTransporte);

	/**
	 * Borrar ParteTransporte seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
