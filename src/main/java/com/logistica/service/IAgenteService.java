package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Agente;

public interface IAgenteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Agente> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Agente> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Agente findById(Long id);

	/**
	 * Guardar el Agente
	 * 
	 * @param agente
	 * @return
	 */
	Agente save(Agente agente);

	/**
	 * Borrar Agente seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
