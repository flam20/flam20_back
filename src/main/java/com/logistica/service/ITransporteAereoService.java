package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.TransporteAereo;

public interface ITransporteAereoService {
	
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TransporteAereo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TransporteAereo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TransporteAereo findById(Long id);

	/**
	 * Guardar el TransporteAereo
	 * 
	 * @param cargo
	 * @return
	 */
	TransporteAereo save(TransporteAereo transporteAereo);

	/**
	 * Borrar TransporteAereo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
