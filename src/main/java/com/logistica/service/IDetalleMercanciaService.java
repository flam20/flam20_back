package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.DetalleMercancia;

public interface IDetalleMercanciaService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<DetalleMercancia> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<DetalleMercancia> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	DetalleMercancia findById(Long id);

	/**
	 * Guardar el DetalleMercancia
	 * 
	 * @param detalleMercancia
	 * @return
	 */
	DetalleMercancia save(DetalleMercancia detalleMercancia);

	/**
	 * Borrar DetalleMercancia seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
