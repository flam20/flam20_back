package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Cargo;

public interface ICargoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Cargo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Cargo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Cargo findById(Long id);

	/**
	 * Guardar el cargo
	 * 
	 * @param cargo
	 * @return
	 */
	Cargo save(Cargo cargo);

	/**
	 * Borrar cargo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
