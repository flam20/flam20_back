package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.Seguro;

public interface ISeguroService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Seguro> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Seguro> findAll();

	/**
	 * Buscar Seguro por id.
	 * 
	 * @param id
	 * @return
	 */
	Seguro findById(Long id);

	/**
	 * Guardar el Seguro
	 * 
	 * @param seguro
	 * @return
	 */
	Seguro save(Seguro seguro);

	/**
	 * Borrar Seguro seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
