package com.logistica.service;

import com.logistica.model.CargoDestino;
import com.logistica.model.CargoOrigen;
import com.logistica.model.CargoPorServicio;
import com.logistica.model.EmpleadoSAP;
import com.logistica.model.Servicio;
import com.logistica.model.ServicioInter;
import com.logistica.model.ServicioPorCliente;
import com.logistica.util.ActionCode;

public interface IPedidoClienteService {
	public String creaPedidoCliente(Servicio s, String currency, EmpleadoSAP empleadoSAP);

	public String creaPedidoClienteInter(ServicioInter s, String currency, EmpleadoSAP empleadoSAP);

	public String actualizaRuta(Servicio s, ActionCode action, EmpleadoSAP empleadoSAP, ServicioPorCliente ruta);

	public String actualizaCargo(Servicio s, ActionCode action, EmpleadoSAP empleadoSAP, CargoPorServicio cargo);

	public String consultarStatusPedido(String posicion, String pedidoCliente);

	public String actualizaCargoOrigen(ServicioInter s, ActionCode action, EmpleadoSAP empleadoSAP,
			CargoOrigen cargo);
	
	public String actualizaCargoDestino(ServicioInter s, ActionCode action, EmpleadoSAP empleadoSAP,
			CargoDestino cargo);



}
