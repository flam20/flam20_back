package com.logistica.service;

import java.util.List;

import com.logistica.model.Mensaje;

public interface IMensajeService {

	List<Mensaje> findMensajesByUsuarioDestino(Long idUsuario);

	List<Mensaje> findMensajesByUsuarioAndTipoAndServicio(Long udUsuario, String tipo, Long idServicio);

	Mensaje findById(Long id);

	Mensaje save(Mensaje mensaje);

}
