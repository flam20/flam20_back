package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.CargoAereo;

public interface ICargoAereoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<CargoAereo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<CargoAereo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	CargoAereo findById(Long id);

	/**
	 * Guardar el CargoAereo
	 * 
	 * @param cargo
	 * @return
	 */
	CargoAereo save(CargoAereo cargo);

	/**
	 * Borrar CargoAereo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
