package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.TipoEquipo;

public interface ITipoEquipoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TipoEquipo> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TipoEquipo> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TipoEquipo findById(Long id);

	/**
	 * Guardar el tipoEquipo
	 * 
	 * @param mpc
	 * @return
	 */
	TipoEquipo save(TipoEquipo tipoEquipo);

	/**
	 * Borrar tipoEquipo seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
