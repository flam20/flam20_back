package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.FormaPago;

public interface IFormaPagoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<FormaPago> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<FormaPago> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	FormaPago findById(Long id);

	/**
	 * Guardar la forma de pago
	 * 
	 * @param formaPago
	 * @return
	 */
	FormaPago save(FormaPago formaPago);

	/**
	 * Borrar forma de pago
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
