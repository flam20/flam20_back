package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Rol;

public interface IRolService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Rol> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Rol> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Rol findById(Long id);

	/**
	 * Guardar el rol
	 * 
	 * @param cargo
	 * @return
	 */
	Rol save(Rol rol);

	/**
	 * Borrar rol seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
