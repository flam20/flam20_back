package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.FiguraTransporte;

public interface IFiguraTransporteService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<FiguraTransporte> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<FiguraTransporte> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	FiguraTransporte findById(Long id);

	/**
	 * Guardar la FiguraTransporte
	 * 
	 * @param figuraTransporte
	 * @return
	 */
	FiguraTransporte save(FiguraTransporte figuraTransporte);

	/**
	 * Borrar FiguraTransporte seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
