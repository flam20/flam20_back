package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.TipoMovimiento;

public interface ITipoMovimientoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<TipoMovimiento> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<TipoMovimiento> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	TipoMovimiento findById(Long id);

	/**
	 * Guardar el TipoMovimiento
	 * 
	 * @param tipoMovimiento
	 * @return
	 */
	TipoMovimiento save(TipoMovimiento tipoMovimiento);

	/**
	 * Borrar TipoMovimiento seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
