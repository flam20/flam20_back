package com.logistica.service;

import com.nimbusds.jose.JOSEException;

public interface ITableauService {

	String getToken(String username) throws JOSEException;
	
}
