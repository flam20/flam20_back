package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.Prospecto;

public interface IProspectoService {

	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<Prospecto> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<Prospecto> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	Prospecto findById(Long id);

	/**
	 * Guardar el Prospecto
	 * 
	 * @param Prospecto
	 * @return
	 */
	Prospecto save(Prospecto prospecto);

	/**
	 * Borrar Prospecto seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

	List<Prospecto> search(String[] giros, String[] sociosComerciales, String[] coberturas, String[] patios,
			String[] codigosHazmat, String[] servicios, String[] unidadesAutAero, String[] puertos, String[] fronteras,
			String[] recursos, String[] certificacionesEmpresa, String[] certificacionesOperador, String[] equipos);

}
