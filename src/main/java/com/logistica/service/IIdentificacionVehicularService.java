package com.logistica.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.logistica.model.cartaporte.IdentificacionVehicular;

public interface IIdentificacionVehicularService {
	
	/**
	 * Obtener todos paginados
	 * 
	 * @return
	 */
	Page<IdentificacionVehicular> pageAll(Pageable pageable);

	/**
	 * Obtener todos
	 * 
	 * @return
	 */
	List<IdentificacionVehicular> findAll();

	/**
	 * Buscar por id.
	 * 
	 * @param id
	 * @return
	 */
	IdentificacionVehicular findById(Long id);

	/**
	 * Guardar el IdentificacionVehicular
	 * 
	 * @param identificacionVehicular
	 * @return
	 */
	IdentificacionVehicular save(IdentificacionVehicular identificacionVehicular);

	/**
	 * Borrar IdentificacionVehicular seleccionado
	 * 
	 * @param id
	 * @return
	 */
	void delete(Long id);

}
