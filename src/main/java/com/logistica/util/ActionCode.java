package com.logistica.util;

public enum ActionCode {
	CREATE("01"), UPDATE("02"), REMOVE("03"), SAVE("04");

	private String actionCode;

	ActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionCode() {
		return actionCode;
	}
}