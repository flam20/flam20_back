package com.logistica.util;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

import com.logistica.model.ControlAcceso;

public class ControlAccesoUtil {
	
	public static ControlAcceso generarControlAccesoCreacion() {
//			return new ControlAcceso(new Timestamp(GregorianCalendar.getInstance().getTimeInMillis()), "flam");
		ControlAcceso ca = new ControlAcceso();
		
		ca.setFechaModificacion(null);
		ca.setUsuarioModificacion("flam");
		return ca;
	}

	public static ControlAcceso generarControlAccesoModificacion(ControlAcceso ca) {

		ca.setFechaModificacion(null);
		ca.setUsuarioModificacion("flam");

		return ca;
	}
	
	
}
