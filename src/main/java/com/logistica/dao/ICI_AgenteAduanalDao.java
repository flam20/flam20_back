package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.CI_AgenteAduanal;

public interface ICI_AgenteAduanalDao extends JpaRepository<CI_AgenteAduanal, Long> {

}
