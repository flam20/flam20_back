package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Cliente;

public interface IClienteDao extends JpaRepository<Cliente, Long> {

	List<Cliente> findByClientePotencial(Boolean bloqueado);
	
	List<Cliente> findByBloqueado(Boolean bloqueado);

	List<Cliente> findFirst10ByTerminoBusquedaContainingIgnoreCase(String busqueda);
	
	List<Cliente> findFirst10ByTerminoBusquedaContainingIgnoreCaseAndBloqueado(String busqueda,Boolean bloqueado);
	
	List<Cliente> findFirst10ByTerminoBusquedaContainingIgnoreCaseAndBloqueadoAndClientePotencial(String busqueda,Boolean bloqueado,Boolean clientePotencial);

}
