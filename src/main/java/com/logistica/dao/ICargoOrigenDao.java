package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.CargoOrigen;

public interface ICargoOrigenDao extends JpaRepository<CargoOrigen, Long> {

}
