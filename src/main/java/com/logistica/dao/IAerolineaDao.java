package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Aerolinea;

public interface IAerolineaDao extends JpaRepository<Aerolinea, Long> {

}
