package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Ejecutivo;

public interface IEjecutivoDao extends JpaRepository<Ejecutivo, Long> {

	List<Ejecutivo> findByIdTeams(String idTeams);

}
