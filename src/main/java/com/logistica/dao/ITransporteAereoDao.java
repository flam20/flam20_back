package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.TransporteAereo;

public interface ITransporteAereoDao extends JpaRepository<TransporteAereo, Long> {

}
