package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.FormaPago;

public interface IFormaPagoDao extends JpaRepository<FormaPago, Long> {

}
