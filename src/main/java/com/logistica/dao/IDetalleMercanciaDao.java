package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.DetalleMercancia;

public interface IDetalleMercanciaDao extends JpaRepository<DetalleMercancia, Long> {

}
