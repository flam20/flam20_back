package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.ReporteAM;

public interface IReporteAMDao extends JpaRepository<ReporteAM, Long> {

}

