package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.UbicacionCp;

public interface IUbicacionCpDao extends JpaRepository<UbicacionCp, Long> {

}
