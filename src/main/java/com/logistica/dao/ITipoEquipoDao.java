package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.TipoEquipo;

public interface ITipoEquipoDao extends JpaRepository<TipoEquipo, Long> {

}
