package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Seguro;

public interface ISeguroDao extends JpaRepository<Seguro, Long> {

}
