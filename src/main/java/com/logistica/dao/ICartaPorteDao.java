package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.CartaPorte;

public interface ICartaPorteDao extends JpaRepository<CartaPorte, Long> {

}
