package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Consignatario;

public interface IConsignatarioDao extends JpaRepository<Consignatario, Long> {

}
