package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Agente;

public interface IAgenteDao extends JpaRepository<Agente, Long> {

}
