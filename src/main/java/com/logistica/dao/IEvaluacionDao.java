package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Evaluacion;

public interface IEvaluacionDao extends JpaRepository<Evaluacion, Long> {

}
