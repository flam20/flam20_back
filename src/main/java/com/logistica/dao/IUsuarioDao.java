package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Usuario;

public interface IUsuarioDao extends JpaRepository<Usuario, Long> {

	Usuario findByUsername(String username);

	List<Usuario> findByRolIdRolIn(List<Long> ids);
	
}
