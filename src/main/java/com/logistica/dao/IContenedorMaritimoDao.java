package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.ContenedorMaritimo;

public interface IContenedorMaritimoDao extends JpaRepository<ContenedorMaritimo, Long> {

}
