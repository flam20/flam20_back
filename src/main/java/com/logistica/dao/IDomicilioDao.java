package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Domicilio;

public interface IDomicilioDao extends JpaRepository<Domicilio, Long> {

}
