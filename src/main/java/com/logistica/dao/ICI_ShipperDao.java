package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.CI_Shipper;

public interface ICI_ShipperDao extends JpaRepository<CI_Shipper, Long> {

}
