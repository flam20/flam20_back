package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Mpc;

public interface IMpcDao extends JpaRepository<Mpc, Long> {

}
