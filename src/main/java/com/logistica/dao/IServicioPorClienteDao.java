package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.ServicioPorCliente;

public interface IServicioPorClienteDao extends JpaRepository<ServicioPorCliente, Long> {

}
