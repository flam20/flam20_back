package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Remolque;

public interface IRemolqueDao extends JpaRepository<Remolque, Long> {

}
