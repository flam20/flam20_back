package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.ContenedorFerroviario;

public interface IContenedorFerroviarioDao extends JpaRepository<ContenedorFerroviario, Long> {

}
