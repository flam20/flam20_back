package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.TipoRuta;

public interface ITipoRutaDao extends JpaRepository<TipoRuta, Long> {

}
