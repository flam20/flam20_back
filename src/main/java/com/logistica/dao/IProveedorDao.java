package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Proveedor;

public interface IProveedorDao extends JpaRepository<Proveedor, Long> {

	List<Proveedor> findFirst10ByRazonComercialContainingIgnoreCase(String razonComercial);

	List<Proveedor> findFirst10ByRazonComercialContainingIgnoreCaseAndBloqueado(String razonComercial,Boolean bloqueado);
}
