package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Incoterm;

public interface IIncotermDao extends JpaRepository<Incoterm, Long> {

}
