package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.EstatusServicio;

public interface IEstatusServicioDao extends JpaRepository<EstatusServicio, Long> {

}
