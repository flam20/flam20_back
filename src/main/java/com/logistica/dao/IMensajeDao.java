package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.logistica.model.Mensaje;

public interface IMensajeDao extends JpaRepository<Mensaje, Long> {

	List<Mensaje> findMensajesByUsuarioDestinoIdUsuario(Long idUsuario);

	@Query(value = "Select m From Mensaje m Where (m.usuarioOrigen.idUsuario = :idUsuario or m.usuarioDestino.idUsuario = :idUsuario) and m.tipo = :tipo and m.idServicio = :idServicio Order by m.fecha ASC")
	List<Mensaje> findMensajesByUsuarioDestinoIdUsuarioOrUsuarioOrigenIdUsuarioAndTipoAndIdServicioOrderByFechaAsc(
			@Param("idUsuario") Long idUsuario, @Param("tipo") String tipo, @Param("idServicio") Long idServicio);
	
}
