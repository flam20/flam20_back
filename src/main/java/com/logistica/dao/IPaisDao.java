package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Pais;

public interface IPaisDao extends JpaRepository<Pais, Long> {

}
