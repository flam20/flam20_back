package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.CantidadTransportada;

public interface ICantidadTransportadaDao extends JpaRepository<CantidadTransportada, Long> {

}
