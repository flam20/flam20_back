package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Aeropuerto;

public interface IAeropuertoDao extends JpaRepository<Aeropuerto, Long> {

}
