package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.IdentificacionVehicular;

public interface IIdentificacionVehicularDao extends JpaRepository<IdentificacionVehicular, Long> {

}
