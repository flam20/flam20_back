package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Reporte;

public interface IReporteDao extends JpaRepository<Reporte, Long> {

}

