package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.ArchivoBox;

public interface IArchivoBoxDao extends JpaRepository<ArchivoBox, Long> {

	List<ArchivoBox> findByIdServicio(Long idServicio);

}
