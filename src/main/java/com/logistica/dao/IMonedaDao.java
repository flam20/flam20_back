package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Moneda;

public interface IMonedaDao extends JpaRepository<Moneda, Long> {

}
