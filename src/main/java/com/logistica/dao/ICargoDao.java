package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Cargo;

public interface ICargoDao extends JpaRepository<Cargo, Long> {

}
