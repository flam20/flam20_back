package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.FiguraTransporte;

public interface IFiguraTransporteDao extends JpaRepository<FiguraTransporte, Long> {

}
