package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Autotransporte;

public interface IAutotransporteDao extends JpaRepository<Autotransporte, Long> {

}
