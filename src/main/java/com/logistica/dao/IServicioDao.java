package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.logistica.model.Servicio;

public interface IServicioDao extends JpaRepository<Servicio, Long> {

	@Query(value = "SELECT max(consecutivo) FROM Servicio Where anio = :anio")
	Integer findConsecutivoByAnio(@Param("anio") Integer anio);

	@Query(value = "SELECT distinct s.trafico, s.nombreTrafico FROM Servicio s WHERE s.nombreTrafico is not null")
	List<Object> findTraficos();
	
	@Transactional
	@Modifying
	@Query("update Servicio s set s.ordenCompra = :ordenCompra where s.idServicio = :idServicio")
	void updateOrdenCompra(@Param("ordenCompra") String ordenCompra,@Param("idServicio") Long idServicio);
	
	@Transactional
	@Modifying
	@Query("update Servicio s set s.idChat = :idChat where s.idServicio = :idServicio")
	void updateChatID(@Param("idChat") String idChat,@Param("idServicio") Long idServicio);
	
}
