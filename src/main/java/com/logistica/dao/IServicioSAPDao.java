package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.logistica.model.ServicioSAP;

public interface IServicioSAPDao extends JpaRepository<ServicioSAP, Long> {
	
	@Query(value = "SELECT s FROM ServicioSAP s WHERE s.tipoServicio = :tipoServicio AND s.cargoFlam = :cargoFlam")
	ServicioSAP getServicioSAPByCargo(@Param("tipoServicio") String tipoServicio, @Param("cargoFlam") String cargoFlam);

}
