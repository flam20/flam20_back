package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Rol;

public interface IRolDao extends JpaRepository<Rol, Long> {

}
