package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Ubicacion;

public interface IUbicacionDao extends JpaRepository<Ubicacion, Long> {

	List<Ubicacion> findFirst10ByCiudadContainingIgnoreCase(String ciudad);

	List<Ubicacion> findFirst10ByCiudadContainingIgnoreCaseAndPaisContainingIgnoreCase(String ciudad, String pais);
	
}
