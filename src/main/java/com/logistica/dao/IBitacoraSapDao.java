package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.logistica.model.BitacoraSap;

public interface IBitacoraSapDao extends JpaRepository<BitacoraSap, Long> {

	List<BitacoraSap> findByIdServicio(Long idServicio);
	
//	@Query(value = "SELECT distinct bs FROM BitacoraSap bs WHERE bs.idServicio = :idServicio ")
//	List<BitacoraSap> findByIdServicioStr(@Param("idServicio") String idServicio);

}
