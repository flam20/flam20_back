package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Mercancias;

public interface IMercanciasDao extends JpaRepository<Mercancias, Long> {

}
