package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.CargoDestino;

public interface ICargoDestinoDao extends JpaRepository<CargoDestino, Long> {

}
