package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Pedimento;

public interface IPedimentoDao extends JpaRepository<Pedimento, Long> {

}
