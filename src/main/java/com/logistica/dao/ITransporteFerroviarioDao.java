package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.TransporteFerroviario;

public interface ITransporteFerroviarioDao extends JpaRepository<TransporteFerroviario, Long> {

}
