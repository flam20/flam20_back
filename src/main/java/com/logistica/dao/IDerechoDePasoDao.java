package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.DerechoDePaso;

public interface IDerechoDePasoDao extends JpaRepository<DerechoDePaso, Long> {

}
