package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.TransporteMaritimo;

public interface ITransporteMaritimoDao extends JpaRepository<TransporteMaritimo, Long> {

}
