package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Carro;

public interface ICarroDao extends JpaRepository<Carro, Long> {

}
