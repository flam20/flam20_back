package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.CargoAereo;

public interface ICargoAereoDao extends JpaRepository<CargoAereo, Long> {

}
