package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.ParteTransporte;

public interface IParteTransporteDao extends JpaRepository<ParteTransporte, Long> {

}
