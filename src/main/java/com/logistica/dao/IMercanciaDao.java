package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.Mercancia;

public interface IMercanciaDao extends JpaRepository<Mercancia, Long> {

}
