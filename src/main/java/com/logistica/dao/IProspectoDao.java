package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Prospecto;

public interface IProspectoDao extends JpaRepository<Prospecto, Long> {

}
