package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.Shipper;

public interface IShipperDao extends JpaRepository<Shipper, Long> {

}
