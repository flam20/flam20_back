package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.TipoFigura;

public interface ITipoFiguraDao extends JpaRepository<TipoFigura, Long> {

}
