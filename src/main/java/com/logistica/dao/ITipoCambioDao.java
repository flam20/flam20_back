package com.logistica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.logistica.model.AnioMes;
import com.logistica.model.EnumMes;
import com.logistica.model.Moneda;
import com.logistica.model.TipoCambio;

public interface ITipoCambioDao extends JpaRepository<TipoCambio, Long> {

	List<TipoCambio> findByAnioMesAnioAndAnioMesMesIdMes(Integer anio, Long idMes);
	
	@Query(value = "SELECT tc FROM TipoCambio tc WHERE tc.anioMes = :anioMes and tc.moneda = :moneda")
	TipoCambio getByAnioMesMoneda(@Param("anioMes") AnioMes anioMes,@Param("moneda") Moneda moneda);
	
}
