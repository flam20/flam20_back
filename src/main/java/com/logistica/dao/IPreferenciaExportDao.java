package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.PreferenciaExport;

public interface IPreferenciaExportDao extends JpaRepository<PreferenciaExport, Long> {

	PreferenciaExport findByTraficoIdUsuarioAndTipo(Long idUsuario, String tipo);
	
}
