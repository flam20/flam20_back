package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.logistica.model.Propiedad;

public interface IPropiedadDao extends JpaRepository<Propiedad, Long> {

	@Query(value = "SELECT p FROM Propiedad p Where p.propiedad = :propiedad")
	Propiedad findByProp(@Param("propiedad") String propiedad);
//
//	@Query(value = "SELECT distinct s.trafico, s.nombreTrafico FROM Servicio s WHERE s.nombreTrafico is not null")
//	List<Object> findTraficos();
	
}
