package com.logistica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logistica.model.cartaporte.GuiaIdentificacion;

public interface IGuiaIdentificacionDao extends JpaRepository<GuiaIdentificacion, Long> {

}
