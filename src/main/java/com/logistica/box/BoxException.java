package com.logistica.box;

public class BoxException extends Exception {

	private static final long serialVersionUID = 1981763405037854577L;

	private final int responseCode;
	private final String errorMessage;

	public BoxException(final int responseCode, final String errorMessage, final Throwable t) {
		super();
		this.responseCode = responseCode;
		this.errorMessage = errorMessage;
	}

	public BoxException(final int responseCode, final Throwable t) {
		super(t);
		this.responseCode = responseCode;
		this.errorMessage = null;
	}

	public BoxException(final Throwable t) {
		super(t);
		this.responseCode = 0;
		this.errorMessage = null;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public String toString() {
		return this.getResponseCode() + " -> " + this.getMessage();
	}

}
