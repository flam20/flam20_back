package com.logistica.box;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.codehaus.jackson.map.ObjectMapper;

public abstract class BoxClient {

	private static String accessToken;
	private static long expirationTimeMillis;

	protected static final String BOX_BASE_URL = "https://api.box.com/2.0/";
	protected static final String CLIENT_ID = "cvsn4of9w5szl7we11qivbv4fhteuzr9";
	protected static final String CLIENT_SECRET = "5UnKNk8obGUdkGbg6Q2xSjkUwg1p1nYg";

	public BoxClient() {
		System.setProperty("https.protocols", "TLSv1.2");
	}

	protected String getAccessToken() throws BoxException {
		if (accessToken == null || expirationTimeMillis < (System.currentTimeMillis() - 60000)) {
			updateToken();
		}
		return accessToken;
	}

	private void updateToken() throws BoxException {
		try {
			final HttpURLConnection httpURLConnection = getConnection("https://api.box.com/oauth2/token", "POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			try (final DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream())) {

				final String requestBody = "client_id={client_id}&client_secret={client_secret}&grant_type=client_credentials&box_subject_type=enterprise&box_subject_id=17258522"
						.replace("{client_id}", CLIENT_ID).replace("{client_secret}", CLIENT_SECRET);

				dataOutputStream.writeBytes(requestBody);
				dataOutputStream.flush();

				if (httpURLConnection.getResponseCode() != 200) {
					throw new BoxException(httpURLConnection.getResponseCode(), null);
				}

				final BoxLogin rs = extractResponse(httpURLConnection, BoxLogin.class);

				synchronized (this) {
					accessToken = "Bearer ".concat(rs.getAccessToken());
					expirationTimeMillis = System.currentTimeMillis() + (rs.getExpiresIn() * 1000);
				}
			}
		} catch (IOException e) {
			throw new BoxException(-1, e);
		}
	}

	protected HttpURLConnection getConnection(final String urlString, final String requestMethod)
			throws MalformedURLException, IOException {
		final HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(urlString).openConnection();
		httpURLConnection.setUseCaches(false);
		httpURLConnection.setDoInput(true);
		httpURLConnection.setDoOutput(true);
		httpURLConnection.setRequestMethod(requestMethod);
		return httpURLConnection;

	}

	protected <T> T extractResponse(final HttpURLConnection httpURLConnection, Class<T> clazz) throws IOException {

		final InputStream inputStream = httpURLConnection.getErrorStream() != null ? httpURLConnection.getErrorStream()
				: httpURLConnection.getInputStream();

		return new ObjectMapper().readValue(inputStream, clazz);
	}

}
