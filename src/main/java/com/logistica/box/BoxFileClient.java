package com.logistica.box;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.box.sdk.BoxItem;

public class BoxFileClient extends BoxClient {

	private static final String LIST_ITEMS_IN_FOLDER = String.format("%s/folders/{folderId}/items", BOX_BASE_URL);

	private Logger logger = LoggerFactory.getLogger(BoxClient.class);

	public BoxFileClient() {
		super();
	}

	public String testConnection() throws BoxException {
		return getAccessToken();
	}

	public List<BoxItem> listItemsFolder(String folderId) throws BoxException {
		try {
			final HttpURLConnection httpURLConnection = getConnection(
					LIST_ITEMS_IN_FOLDER.replace("{folderId}", folderId), "GET");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
			httpURLConnection.setRequestProperty("Authorization", getAccessToken());

			if (httpURLConnection.getResponseCode() != 200) {
				throw new BoxException(httpURLConnection.getResponseCode(), getErrorResponse(httpURLConnection), null);
			}

			return extractResponse(httpURLConnection, ListBoxItemResponse.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	protected String getErrorResponse(HttpURLConnection httpURLConnection) throws BoxException {
		BufferedReader bufferedReader = null;
		try {
			if (httpURLConnection.getResponseCode() > 299) {
				bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
			} else {
				bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			}
			String inputLine;
			StringBuilder responseBody = new StringBuilder();

			while ((inputLine = bufferedReader.readLine()) != null) {
				responseBody.append(inputLine);
			}

			bufferedReader.close();
			logger.info("Error Response: " + responseBody.toString());
			return responseBody.toString();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new BoxException(e);
		}
	}

}
