package com.logistica;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class MultilogV2Application implements CommandLineRunner {

	//@Autowired
	//private BCryptPasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(MultilogV2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//String password = "Admin2021";
		//System.out.println(passwordEncoder.encode(password));
	}

}
