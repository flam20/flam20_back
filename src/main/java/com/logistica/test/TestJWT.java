package com.logistica.test;

import java.util.*;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.*;
import com.nimbusds.jwt.*;

public class TestJWT {

	public static void main(String[] args) {
		System.out.println("== INICIO ==");
		try {
			String clientId = "62a36a42-a2bd-42a5-b37c-8429fb894b6f";
			
			String secret = "a6c7a429-398e-483f-bb1f-a2104538f77b";
			String kid = "bjF52zp2A4W8+EduNLsYHvUNGqQWf6W9BvypVYC4SCQ=";
			
			List<String> scopes = new ArrayList<>(Arrays.asList("tableau:views:embed"));
			String username = "fernando.aguirre@multilog.com.mx";
			JWSSigner signer = new MACSigner(secret);
			JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.HS256).keyID(kid).customParam("iss", clientId).build();
			JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
					.issuer(clientId)
					.expirationTime(new Date(new Date().getTime() + 60 * 1000)) //expires in 1 minute
					.jwtID(UUID.randomUUID().toString())
					.audience("tableau")
					.subject(username)
					.claim("scp", scopes)
					.build();
			SignedJWT signedJWT = new SignedJWT(header, claimsSet);
			signedJWT.sign(signer);
			//model.addAttribute("token", signedJWT.serialize());
			System.out.println("TOKEN:"+signedJWT.serialize());
			System.out.println("== FIN ==");
		} catch (Exception e) {
			System.err.println("Error::"+e);
			e.printStackTrace();
		}
		

	}

}
