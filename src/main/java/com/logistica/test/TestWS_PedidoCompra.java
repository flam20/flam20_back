package com.logistica.test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import com.logistica.sap.handler.OrdenCompraHandlerResolver;
import com.logistica.sap.pedidoCompra.Amount;
import com.logistica.sap.pedidoCompra.BusinessTransactionDocumentTypeCode;
import com.logistica.sap.pedidoCompra.LOCALNORMALISEDDateTime;
import com.logistica.sap.pedidoCompra.LocationID;
import com.logistica.sap.pedidoCompra.MaintenanceAccountingCodingBlockDistribution;
import com.logistica.sap.pedidoCompra.MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment;
import com.logistica.sap.pedidoCompra.ManagePurchaseOrderIn;
import com.logistica.sap.pedidoCompra.PartyID;
import com.logistica.sap.pedidoCompra.PartyKey;
import com.logistica.sap.pedidoCompra.Price;
import com.logistica.sap.pedidoCompra.ProcurementDocumentItemFollowUpDelivery;
import com.logistica.sap.pedidoCompra.ProcurementDocumentItemFollowUpInvoice;
import com.logistica.sap.pedidoCompra.ProductID;
import com.logistica.sap.pedidoCompra.ProductKey;
import com.logistica.sap.pedidoCompra.PurchaseOrderBundleMaintainRequestMessageSync;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainConfirmationBundle;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainConfirmationBundleMessageSync;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundle;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleItem;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleItemLocation;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleItemProduct;
import com.logistica.sap.pedidoCompra.PurchaseOrderMaintainRequestBundleParty;
import com.logistica.sap.pedidoCompra.Quantity;
import com.logistica.sap.pedidoCompra.Service;
import com.logistica.sap.pedidoCompra.UPPEROPENLOCALNORMALISEDDateTimePeriod;
import com.sun.xml.ws.policy.privateutil.PolicyLogger;


public class TestWS_PedidoCompra {
	
	static Logger log = Logger.getLogger(TestWS_PedidoCompra.class.getName());
	public static void main(String args[]) {
//		System.out.println("TEST PEDIDO/ORDEN COMPRA");
		
		TestWS_PedidoCompra t = new TestWS_PedidoCompra();
//		System.out.println("UNIDAD DE VENTAS:" + t.obtieneUnidadVentas("12121212"));
//		System.out.println("CENTRO :"+ t.creaCentroTrabajo());
		t.creaPedidoCompra();

	}

	private String creaPedidoCompra() {
		Service service = new Service();
		
		
		String unidadVentas = null;
		try {
			PolicyLogger logger = PolicyLogger.getLogger(com.sun.xml.ws.policy.EffectiveAlternativeSelector.class);
			logger.setLevel(Level.OFF);
			OrdenCompraHandlerResolver myHanlderResolver = new OrdenCompraHandlerResolver();
			service.setHandlerResolver(myHanlderResolver);
			ManagePurchaseOrderIn binding = service.getBinding();
			Map<String, Object> reqContext = ((BindingProvider) binding).getRequestContext();
			
			
		
		 	
			
			
			reqContext.put(BindingProvider.USERNAME_PROPERTY, "_FLAMSYSTEM");
			reqContext.put(BindingProvider.PASSWORD_PROPERTY, "Inicio01");

			PurchaseOrderBundleMaintainRequestMessageSync request = new PurchaseOrderBundleMaintainRequestMessageSync();

			PurchaseOrderMaintainRequestBundle poRequest = new PurchaseOrderMaintainRequestBundle();
			poRequest.setActionCode("01");

			BusinessTransactionDocumentTypeCode btTypeCode = new BusinessTransactionDocumentTypeCode();
			btTypeCode.setValue("001");
			poRequest.setBusinessTransactionDocumentTypeCode(btTypeCode);

			// CURRENCY
			poRequest.setCurrencyCode("MXN");

			// Buyer Party
			poRequest.setBuyerParty(getParty("MLG1000"));

			// Seller Party (Proveedor)
			poRequest.setSellerParty(getParty("T01020"));

			// Bill To
			poRequest.setBillToParty(getParty("MLG1000"));

			// Employee
			poRequest.setEmployeeResponsibleParty(getParty("1010"));

			// ============================== item
			PurchaseOrderMaintainRequestBundleItem poItem = new PurchaseOrderMaintainRequestBundleItem();
			poItem.setActionCode("01");

			// Quantity
			Quantity quantityValue = new Quantity();
			quantityValue.setUnitCode("Z01");
			quantityValue.setValue(new BigDecimal(1));
			poItem.setQuantity(quantityValue);

			// Delivery Period
			UPPEROPENLOCALNORMALISEDDateTimePeriod deliveryPeriod = new UPPEROPENLOCALNORMALISEDDateTimePeriod();
			LOCALNORMALISEDDateTime startDateTime = new LOCALNORMALISEDDateTime();

			GregorianCalendar gc = new GregorianCalendar();
			// gc.setTime(re.getItemDeliveryStartTime());
			gc.setTime(new Date());
			XMLGregorianCalendar periodStartTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
			startDateTime.setValue(periodStartTime);
			startDateTime.setTimeZoneCode("EST");
			deliveryPeriod.setStartDateTime(startDateTime);
			deliveryPeriod.setEndDateTime(startDateTime);
			poItem.setDeliveryPeriod(deliveryPeriod);

			// Price
			Price price = new Price();
			Amount amountValue = new Amount();
			amountValue.setCurrencyCode("MXN");
			amountValue.setValue(new BigDecimal(1200.0));
			price.setAmount(amountValue);
			poItem.setListUnitPrice(price);

			// Followed UP delivery
			ProcurementDocumentItemFollowUpDelivery fUpDelivery = new ProcurementDocumentItemFollowUpDelivery();
			fUpDelivery.setRequirementCode("01");
			fUpDelivery.setEmployeeTimeConfirmationRequiredIndicator(true);
			poItem.setFollowUpDelivery(fUpDelivery);

			// Followed UP Invoice
			ProcurementDocumentItemFollowUpInvoice fUpInvoice = new ProcurementDocumentItemFollowUpInvoice();
			fUpInvoice.setRequirementCode("01");
			fUpInvoice.setDeliveryBasedInvoiceVerificationIndicator(true);
			poItem.setFollowUpInvoice(fUpInvoice);

			// Item Product
			PurchaseOrderMaintainRequestBundleItemProduct prod = new PurchaseOrderMaintainRequestBundleItemProduct();
			prod.setActionCode("01");
			prod.setCashDiscountDeductibleIndicator(true);
			ProductKey prodKey = new ProductKey();
			prodKey.setProductTypeCode("1");
			prodKey.setProductIdentifierTypeCode("1");
			ProductID prodID = new ProductID();
			prodID.setValue("FLETE_TER_N");
			prodKey.setProductID(prodID);
			prod.setProductKey(prodKey);

			poItem.setItemProduct(prod);

			// Ship To Location
			PurchaseOrderMaintainRequestBundleItemLocation shipToLocation = new PurchaseOrderMaintainRequestBundleItemLocation();
			LocationID location = new LocationID();
			location.setValue("MLG1000");
			shipToLocation.setLocationID(location);
			poItem.setShipToLocation(shipToLocation);

			// AccountingCodingBlockDistribution

			MaintenanceAccountingCodingBlockDistribution blockDis = new MaintenanceAccountingCodingBlockDistribution();
			blockDis.setActionCode("01");
			MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment assignment = new MaintenanceAccountingCodingBlockDistributionAccountingCodingBlockAssignment();
			assignment.setActionCode("01");
			assignment.setCostCentreID("MLGV1114-1");
			blockDis.getAccountingCodingBlockAssignment().add(assignment);
			poItem.setItemAccountingCodingBlockDistribution(blockDis);
			
			

			// ============================== FIN item
			poRequest.getItem().add(poItem);

			
			
//			poRequest.setFinishDeliveryPOActionIndicator(true);
//			poRequest.setFinishInvoicePOActionIndicator(true);
			poRequest.setOrderPurchaseOrderActionIndicator(true);
			
			// 1 o mas PO's
			request.getPurchaseOrderMaintainBundle().add(poRequest);
			PurchaseOrderMaintainConfirmationBundleMessageSync response = binding
					.managePurchaseOrderInMaintainBundle(request);

			List<PurchaseOrderMaintainConfirmationBundle> pos = response.getPurchaseOrder();
			if (pos != null && pos.size() > 0) {
				for (PurchaseOrderMaintainConfirmationBundle po : pos) {
					System.out.println("PO =>" + po.getBusinessTransactionDocumentID().getValue());
				}
			}

			com.logistica.sap.pedidoCompra.Log responseLog = response.getLog();

//			System.out.println(responseLog.getItem().size());
			if (responseLog.getItem() != null && responseLog.getItem().size() > 0) {
				for (com.logistica.sap.pedidoCompra.LogItem item : responseLog.getItem()) {
					System.out.println(item.getSeverityCode() + " - " + item.getNote());
				}
			}

		} catch (Exception e) {
			System.err.println("Error:" + e);
			e.printStackTrace();
		}
		return unidadVentas;
	}

	private PurchaseOrderMaintainRequestBundleParty getParty(String value) {
		PurchaseOrderMaintainRequestBundleParty party = new PurchaseOrderMaintainRequestBundleParty();
		PartyKey partyKey = new PartyKey();
		PartyID partyID = new PartyID();
		partyID.setValue(value);
		partyKey.setPartyID(partyID);
		party.setPartyKey(partyKey);
		return party;
	}

}
