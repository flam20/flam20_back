package com.logistica.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.logistica.dto.MenuDTO;
import com.logistica.model.Menu;
import com.logistica.model.Usuario;
import com.logistica.service.IUsuarioService;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

	@Autowired
	private IUsuarioService usuarioService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		Usuario usuario = usuarioService.findByUsername(authentication.getName());
		Map<String, Object> mapInfo = new HashMap<>();
		mapInfo.put("idUsuario", usuario.getIdUsuario());
		mapInfo.put("nombre1", usuario.getNombre1());
		mapInfo.put("nombre2", usuario.getNombre2());
		mapInfo.put("nombre3", usuario.getNombre3());
		mapInfo.put("apellidoPaterno", usuario.getApellidoPaterno());
		mapInfo.put("apellidoMaterno", usuario.getApellidoMaterno());
		mapInfo.put("correoElectronico", usuario.getCorreoElectronico());
		List<MenuDTO> menus = new ArrayList<>();
		for (Menu menu : usuario.getRol().getMenus().stream().sorted().collect(Collectors.toList())) {
			MenuDTO menuDTO = new MenuDTO();
			menuDTO.setIdMenu(menu.getIdMenu());
			if (menu.getMenuPadre() != null) {
				menuDTO.setIdMenuPadre(menu.getMenuPadre().getIdMenu());
			} else {
				menuDTO.setIdMenuPadre(null);
			}
			menuDTO.setName(menu.getNombre());
			if (menu.getUrl() == null) {
				menuDTO.setHeading(Boolean.TRUE);
			} else {
				menuDTO.setLink(menu.getUrl());
				menuDTO.setHeading(null);
			}
			menuDTO.setIconclass(menu.getIcono());
			menuDTO.setOrder(menu.getOrden());
			menus.add(menuDTO);
		}
		mapInfo.put("menu", menus);
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(mapInfo);
		return accessToken;
	}

}
