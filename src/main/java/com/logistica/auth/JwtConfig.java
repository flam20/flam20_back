package com.logistica.auth;

public class JwtConfig {
	public static final String NAME_APP = "multilog-app";
	
	public static final String PASSWORD_APP = "Multilog2021";
	
	public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\r\n"
			+ "MIIEpAIBAAKCAQEApd4ZPEb35AZtmudIUpGHBLZMwMcbqdfw9VTgAHkOjIxSj7rY\r\n"
			+ "GdIjksnLAJjHbpe3PkUUxKcsA0HSM0AQbBFOaV27A8PM2iDNNFVq1Y5FAUM519ub\r\n"
			+ "b4hM/S02lg936PmLRDNIpqsa8iJTI10rXVEyMN2ub+d8TyG5v6btFaapcfAGSfnj\r\n"
			+ "/1u7yvjqZCu8DXY9G6QjR+YmgST84M22UGIUquj8i12pqt22SodH5zOpzezhYNIL\r\n"
			+ "NN+p8lmHkomGTo68kb19FpXPsz20IDHyg/fe9G4SRI2CVejXGjMtjARGRgSZrl10\r\n"
			+ "PiovItMZPMGnKALjfAsfc+qBbSgcBzaSAp+mBwIDAQABAoIBAQCHmRdWUMpNDvhB\r\n"
			+ "v7w/TBtuPb5nhx7VbeTeG2H44P6E5h6ExAHwsftFAFVGCqBXiA5VCNKepe/0RbYb\r\n"
			+ "Ec+bGVbaCrddr49j75byprXtGh7NlnDOSAk9V9dgzrqmWMT1oDQ7MBW/s3gW5PrE\r\n"
			+ "mWFfK4b1rfTyJPNaGb8pr8R4g8RYkMha7RXOqWvIk/8ihWLj01pdzH3oSALQcawE\r\n"
			+ "hfsKLD3mS8yYZeG2h7wn2lOAAJZ+z9kjC7Rv0OXvU1iqZ8XPdo7+8cKXGFUqJfFl\r\n"
			+ "QAdFDfB69KhkG/q7YnG8ZFjA28MWrQw3xRcF4p86bvGXth2tBi9xT91bLxl4HOHz\r\n"
			+ "DkUre4wpAoGBANwk2b8RHTEGSk7GtOs/60fhJU3y31bK5h4nMuvwffuJiFOMtMC2\r\n"
			+ "VMDzOWLlQlgWAmKryHPhTlXrF2qz0vyQx9VaYTnkoxEJo1z3atYArMWNMB0gCMI5\r\n"
			+ "8jNkZdaI0eAd+KDP8LoFNHIgMxkViwq6W5ku1W7yTT2k5oW+NRaYf0nFAoGBAMDi\r\n"
			+ "Ii5YR8V31fePFsIUkbLx66Xi0Hznbzy60+9LdU028prl8eB7Rvks0vSRimKwRijb\r\n"
			+ "uiC60oSbYBnBe54OlWdoSQXkUou8BumHLZTeqlDoMdHC7DQN96M2ydT/XMDEfjHe\r\n"
			+ "VlVIpfnNegJd+2rPp4bkJVcVJwfWzw8NwCsProlbAoGBAL4F6RbuNN5uaZHaH6y4\r\n"
			+ "/eSLyWGhdXs7pKr+dwwGCRkovE4yNIvsVvg+epiNAFl8lg6J7XtpTe8IAKyQEF0G\r\n"
			+ "tBfL7gTFAKTiRusmfke/SWM03gUl86mDzzbhQK6QZ2knNcfJ9ib7+ytP9AuA9wYJ\r\n"
			+ "mqt9K6qa6GV/g6SKbNTWSXlpAoGAUglcBF4j2IcJiEthoJZjCbr/QQThK967XxcP\r\n"
			+ "QP3VHEw8j0kxwQptu1knOEIWm7bhgEtENNuA4km7a/jw/N6zCNJADLN5sFj+j6Cg\r\n"
			+ "8GpsAhFf7xl6oQE3zEFnQqKB7rMkAU3RZmCJx19vf0Y9wsYjOKocf5dNjHX+hyaQ\r\n"
			+ "D0CSOXUCgYBlRI3jvSForb6TmfcDVg+TZEn7gE03f4bkC3PJeKUPfSBFebmoZVoL\r\n"
			+ "1fp4xH/ayj12d+verv1TUer4iI8H1HeUwfwBOL/idc1hKwLwJJ0IUzU88eNtXsPS\r\n"
			+ "00UrO6epEhJFCLXa+ff+AnopA2hbl+FgneeVJD+d3eSVeHTPLr2SNA==\r\n"
			+ "-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\r\n"
			+ "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApd4ZPEb35AZtmudIUpGH\r\n"
			+ "BLZMwMcbqdfw9VTgAHkOjIxSj7rYGdIjksnLAJjHbpe3PkUUxKcsA0HSM0AQbBFO\r\n"
			+ "aV27A8PM2iDNNFVq1Y5FAUM519ubb4hM/S02lg936PmLRDNIpqsa8iJTI10rXVEy\r\n"
			+ "MN2ub+d8TyG5v6btFaapcfAGSfnj/1u7yvjqZCu8DXY9G6QjR+YmgST84M22UGIU\r\n"
			+ "quj8i12pqt22SodH5zOpzezhYNILNN+p8lmHkomGTo68kb19FpXPsz20IDHyg/fe\r\n"
			+ "9G4SRI2CVejXGjMtjARGRgSZrl10PiovItMZPMGnKALjfAsfc+qBbSgcBzaSAp+m\r\n"
			+ "BwIDAQAB\r\n"
			+ "-----END PUBLIC KEY-----";
	
}
